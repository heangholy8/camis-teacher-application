// ignore_for_file: non_constant_identifier_names

class BaseUrl {
  final String _staff_base_url =
      "https://api-services.camemis-learn.com/api/v1/admin";

  String get staffBaseUrl => _staff_base_url;
}
