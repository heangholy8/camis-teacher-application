import 'dart:convert';
import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;

class SetAbilitiesStudetnApi {
    Future<bool> setInputNAttendanceApi({required String classId,required List<Map<String,dynamic>> listStudent}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/utility/set-student-leadership-tasks"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: json.encode(
        {
          "class_id":classId,
          "list_studetns":listStudent
        },
      ),
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

}