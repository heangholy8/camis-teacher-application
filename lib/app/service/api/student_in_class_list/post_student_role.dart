import 'dart:convert';

// import 'package:gallery_saver/files.dart';

import 'package:http/http.dart' as http;

import '../../../storages/get_storage.dart';
import '../../base_url.dart';

class PostStudentRole {
  final BaseUrl _baseUrl = BaseUrl();
Future postStudentRoleTask (
  {
    required int classID,
    required String student_id,
    required int take_attendance,
    required int input_score

  }
)async{
      GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
        var body={
      "class_id":classID,
      "list_students":[
        {
            "student_id" :  student_id.toString() ,
            "take_attendance" :take_attendance,
            "input_score" : input_score
        }
      ],
    };
    http.Response response = await http.post(
      Uri.parse(
          "${dataaccess.accessUrl}/utility/set-student-leadership-tasks"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type":"application/json", 
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: jsonEncode(body),
      );

    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
    } else {
      print("error ${response.body}");
      throw Exception((e) {
        print("Error:"+e);

      });
    }}
}