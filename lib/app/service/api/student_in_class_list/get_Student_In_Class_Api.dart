import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../models/student_list_model/list_student_in_class_model.dart';

class GetStudentInClassApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<StudentInClassListModel> getStudentInClassRequestApi(
      {required String? idClass}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/utility/get-list-student/$idClass"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return StudentInClassListModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e);
      });
    }
  }

  Future<StudentInClassListModel> setClassMonitorApi(
      {required String idClass,
      required List<ClassMonitorSet> listStudent}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/utility/set-class-moniter"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: json.encode(
        {
          "class_id": idClass,
          "list_students": listStudent,
        },
      ),
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Set Class Monitor ${response.body}");
      return StudentInClassListModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e.toString());
      });
    }
  }

}
