import 'dart:convert';
import 'dart:io';

import 'package:camis_teacher_application/app/models/student_list_model/update_profile_student_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../storages/get_storage.dart';

class UpdateStudentProfileApi {
  Future<UpdateStudentProfileModel> updateStudentProfileApi({
    required String classId,
    required String studentId,
    required String firstname,
    required String lastname,
    required String firstnameEn,
    required String lastnameEn,
    required int gender,
    required String phone,
    required String dob,
    required String address,
    required String email,

    required String motherfirstname,
    required String motherlastname,
    required String motherfirstnameEn,
    required String motherlastnameEn,
    required String motherphone,
    required String motheraddress,
    required String motheremail,
    required String motherjob,

    required String fatherfirstname,
    required String fatherlastname,
    required String fatherfirstnameEn,
    required String fatherlastnameEn,
    required String fatherphone,
    required String fatheraddress,
    required String fatheremail,
    required String fatherjob,

    required File? image,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;

    var request = http.MultipartRequest(
      'POST',
      Uri.parse("${dataaccess.accessUrl}/utility/update-profile-student/$classId/$studentId"),
    );
    Map<String, String> headers = {
      "Content-type": "multipart/form-data",
      "Content-Type": "application/json",
      "Authorization": "Bearer${dataaccess.accessToken}",
    };
    request.fields.addAll({
      "firstname": firstname,
      "lastname": lastname,
      "firstname_en": firstnameEn,
      "lastname_en": lastnameEn,
      "gender": gender.toString(),
      "phone": phone,
      "dob": dob,
      "email": email,
      "address": address,

      "father_firstname": fatherfirstname,
      "father_lastname": fatherlastname,
      "father_firstname_latin": fatherfirstnameEn,
      "father_lastname_latin": fatherlastnameEn,
      "father_phone": fatherphone,
      "father_job": fatherjob,
      "father_email": fatheremail,
      "father_address": fatheraddress,

      "mother_firstname": motherfirstname,
      "mother_lastname": motherlastname,
      "mother_firstname_latin": motherfirstnameEn,
      "mother_lastname_latin": motherlastnameEn,
      "mother_phone": motherphone,
      "mother_job": motherjob,
      "mother_email": motheremail,
      "mother_address": motheraddress,
    });
    if (image != null) {
      request.files.add(
        http.MultipartFile(
          'file',
          image.readAsBytes().asStream(),
          image.lengthSync(),
          filename: image.path.split('/').last,
        ),
      );
    }
    request.headers.addAll(headers);
    var res = await request.send();
    http.Response response = await http.Response.fromStream(res);
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("asdfasdf${response.body}");
      return UpdateStudentProfileModel.fromJson(jsonDecode(response.body));
    } else {
      debugPrint("asdfasdf${response.body}");
      throw Exception((e) {
        debugPrint(e);
      });
    }
  }

  // Future<UpdateUserModel> updateUserProfileApi({
  //   required String firstname,
  //   required String lastname,
  //   required String firstnameEn,
  //   required String lastnameEn,
  //   required String gender,
  //   required String phone,
  //   required String dob,
  //   required String email,
  //   required String address,
  // }) async {
  //   GetStoragePref _prefs = GetStoragePref();
  //   var dataaccess = await _prefs.getJsonToken;
  //   http.Response response = await http.post(
  //       Uri.parse("${dataaccess.accessUrl}/update-profile-information"),
  //       headers: <String, String>{
  //         "Accept": "application/json",
  //         "Content-Type": "application/json",
  //         "Authorization": "Bearer${dataaccess.accessToken}",
  //       },
  //       body: jsonEncode(<String, dynamic>{
  //         "firstname": firstname,
  //     "lastname": lastname,
  //     "firstname_en": firstnameEn,
  //     "lastname_en": lastnameEn,
  //     "gender": gender,
  //     "phone": phone,
  //     "dob": dob,
  //     "email": email,
  //     "address": address
  //       }));
  //   if (response.statusCode == 200 || response.statusCode == 201) {
  //     print(response.body);
  //     return UpdateUserModel.fromJson(jsonDecode(response.body));
  //   } else {
  //     throw Exception((e) {
  //       print(e);
  //     });
  //   }
  // }
}