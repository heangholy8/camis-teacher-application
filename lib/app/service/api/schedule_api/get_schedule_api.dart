import 'dart:convert';
import 'package:camis_teacher_application/app/models/schedule_model/schedule_model.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;

class GetScheduleApi {
  Future<ScheduleModel> getClassScheduleRequestApi({required String? date,String? classid}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/schedule/list-in-class-by-date?date=$date&class_id=$classid"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
   
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ScheduleModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
//======================  My Schedule ==========================================

  Future<ScheduleModel> getMyScheduleRequestApi({required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/schedule/list-by-date?date=$date"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ScheduleModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
      });
    }
  }
}
