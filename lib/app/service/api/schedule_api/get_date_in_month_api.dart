import 'dart:convert';
import 'package:camis_teacher_application/app/models/schedule_model/list_date_in_month_model.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../storages/user_storage.dart';

class GetListDateApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ListDateInMonthModel> getListDateRequestApi({required String? month,String? year}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/utility/get-list-day-in-month/$month/$year"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ListDateInMonthModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}