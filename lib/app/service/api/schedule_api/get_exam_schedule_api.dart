import 'dart:convert';
import 'package:camis_teacher_application/app/models/exam_model/class_exam_model.dart';
import 'package:camis_teacher_application/app/models/exam_model/my_exam_schedule.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';

class GetExamScheduleApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ClassExamScheduleModel> getClassExamScheduleRequestApi({required String month,required String semester,required String type,required String classId}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/score-enter/list-subject-exam-date-in-class?month=$month&semester=$semester&type=$type&class_id=$classId"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("list subject enter score${response.body}");
      return ClassExamScheduleModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("list subject enter score${response.body}");
        print(e);
      });
    }
  }
//======================  My Schedule ==========================================

  Future<MyExamScheduleModel> getMyExamScheduleRequestApi({required String month,required String semester,required String type}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/score-enter/list-subject-exam-date?month=$month&semester=$semester&type=$type"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return MyExamScheduleModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
      });
    }
  }
}