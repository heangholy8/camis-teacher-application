import 'dart:convert';
import 'package:camis_teacher_application/app/models/exam_model/post_exam_date.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;

class SetExamApi {
  Future<PostExamModel> setNoExamApi({required String? idClass,required String? idsubject, required String? semester, required String? isNoExam,required String? type, required String? month,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/score-enter/save-subject-exam-date".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body:{
        "class_id" : idClass,
        "subject_id" : idsubject,
        "is_no_exam" : isNoExam,
        "semester" : semester,
        "month" : month,
        "type" : type
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return PostExamModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<PostExamModel> setIsExamApi({required String? idClass,required String? idsubject,
                                      required String? date,
                                      required String? isNoExam,required String? type,
                                      required String? scheduleId,
                                      }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/score-enter/save-subject-exam-date".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body: {
        "class_id" : idClass,
        "subject_id" : idsubject,
        "is_no_exam" : isNoExam,
        "type" : type,
        "schedule_id" : scheduleId,
        "exam_date" : date,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return PostExamModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<bool> deleteExamDate({ String? id,}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.delete(
        Uri.parse("${dataaccess.accessUrl}/score-enter/delete-subject-exam-date/$id".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      return true;
    }else{
      return false;
    }
  }

  Future<bool> deleteExamDateWithSchedule({String? classid,String? type, String? subjectid,String? month,}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/score-enter/delete-subject-exam-date-by-schedule".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body:{
          "class_id" : classid,
          "subject_id" : subjectid,
          "type" : type,
          "month" : month,
        }
    );
    if (response.statusCode ==200 || response.statusCode==201) {
       return true;
    } else {
      return true;
    }
  }

  Future<bool> deleteArticleApi({String? id}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.delete(
        Uri.parse("${dataaccess.accessUrl}/schedule/delete-schedule-activity/$id".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print("Delete Success");
      return true;
    }else{
      print("Delete Fail");
      return false;
    }
  }

  Future<bool> deleteArticleBySceduleApi({String? scheduleId,String? typeActivity,String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/schedule/delete-schedule-activity-by-schedule".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body:{
          "schedule_id" : scheduleId,
          "activity_type" : typeActivity,
          "date" : date,
        }
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print("Delete Success");
      return true;
    }else{
      print("Delete Fail");
      return false;
    }
  }


  Future<bool> deleteScheduleAttachApi({required String scheduleActivityId ,required String attachmentsId}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
      http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/schedule/delete-schedule-activity-attachment"),
        headers: <String, String>{
          "Accept": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body: {
          'schedule_activity_id':scheduleActivityId,
          'attachments_id':attachmentsId
        }
    );
    if (response.statusCode ==200 || response.statusCode == 201) {
      print("Delete Success");
      return true;
    }else{
      print("Delete Fail");
      return false;
    }
  }
}