
import 'dart:convert';

import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class PostActvityApi {
  final GetStoragePref _pref = GetStoragePref();
  Future<bool> postActivityApi({ String? title,String? scheduleId,String? date,String? activityType,String? description, List<File>? attachments,String? expiredDate,List<String>? links,}) async {


  var auth = await _pref.getJsonToken;
  var url = Uri.parse("${auth.accessUrl}/schedule/create-activity");
  var headers = {
    "Accept": "application/json",
    "Authorization": "${auth.tokenType}${auth.accessToken}"
  };
  var request = http.MultipartRequest('POST', url);

  if(activityType == "1") {
    if(title!.isEmpty || title == null){
      request.fields["schedule_id"] =  scheduleId.toString();
      request.fields["date"] = date.toString();
      request.fields['activity_type'] = activityType.toString();
    }else{
      request.fields["schedule_id"] =  scheduleId.toString();
      request.fields["date"] = date.toString();
      request.fields['activity_type'] = activityType.toString();
      request.fields['title'] = title.toString();
    }
  }

  if(activityType == "5") {
    if(title!.isEmpty || title == null){
      request.fields["schedule_id"] =  scheduleId.toString();
      request.fields["date"] = date.toString();
      request.fields['activity_type'] = activityType.toString();
    }else{
      request.fields["schedule_id"] =  scheduleId.toString();
      request.fields["date"] = date.toString();
      request.fields['activity_type'] = activityType.toString();
      request.fields['title'] = title.toString();
    }
  }

  if(activityType == "2"){
      request.fields["schedule_id"] =  scheduleId.toString();
      request.fields["date"] = date.toString();
      request.fields['activity_type'] = activityType.toString();
      request.fields['title'] = title.toString();
      request.fields["description"] = description.toString();
      request.fields['expired_at'] = expiredDate.toString();  
    // print("this is type $activityType");
    // if(title!.isEmpty && description!.isEmpty){
    //   request.fields["schedule_id"] =  scheduleId.toString();
    //   request.fields["date"] = date.toString();
    //   request.fields['activity_type'] = activityType.toString();
    //   request.fields['expired_at'] = expiredDate.toString();
    // }else if(description!.isEmpty){
    //   request.fields["schedule_id"] =  scheduleId.toString();
    //   request.fields["date"] = date.toString();
    //   request.fields['activity_type'] = activityType.toString();
    //   request.fields['title'] = title.toString();
    //   request.fields['expired_at'] = expiredDate.toString();
    // }else if(title.isEmpty ){
    //   request.fields["schedule_id"] =  scheduleId.toString();
    //   request.fields["date"] = date.toString();
    //   request.fields['activity_type'] = activityType.toString();
    //   request.fields["description"] = description.toString();
    //   request.fields['expired_at'] = expiredDate.toString();
    // }else{
    //   request.fields["schedule_id"] =  scheduleId.toString();
    //   request.fields["date"] = date.toString();
    //   request.fields['activity_type'] = activityType.toString();
    //   request.fields['title'] = title.toString();
    //   request.fields["description"] = description.toString();
    //   request.fields['expired_at'] = expiredDate.toString();  
    // }
    if(links!.isNotEmpty){
      for(int i = 0;i<links.length;i++){
        request.fields["links[$i]"]=links[i];
      }
    } else{
      request.fields.addAll({
        "links":""
      });
    }
  }

  if(attachments!.isNotEmpty || attachments !=null || attachments.length!=0){
    for (var file in attachments) {
      var stream = http.ByteStream(file.openRead());
      var length = await file.length();
      var multipartFile = http.MultipartFile('file[]', stream, length, filename: file.path);
      request.files.add(multipartFile);
    }
  }
  var requestHeaders = request.headers;
  requestHeaders.addAll(headers);
  request.headers.addAll(requestHeaders);
  var response = await request.send();
  
  if (response.statusCode == 200) {
    print("Post Activitise Success");
    return true;
  } else {
    print("Post Activitise Error");
    return false;
  }
}

  Future<bool> postArticleActivityApi({ required String title,String? scheduleId,String? date,String? activityType,String? description,List<File>? listFile}) async {
    try {
      var dataaccess = await _pref.getJsonToken;
      var url = Uri.parse("${dataaccess.accessUrl}/schedule/create-activity");
      
      var headers = {
        "Accept": "application/json",
        "Authorization": "${dataaccess.tokenType} ${dataaccess.accessToken}"
      };

      var request = http.MultipartRequest('POST',url);
      request.fields['title'] = title;
      request.fields['schedule_id'] = scheduleId!;
      request.fields['date'] = date!;
      request.fields['activity_type'] = activityType!;

      for (var file in listFile!) {
        var stream = http.ByteStream(file.openRead());
        var length = await file.length();
        var multipartFile = http.MultipartFile('file[]', stream, length, filename: file.path);
        request.files.add(multipartFile);
      }

      var requestHeader = request.headers;
      requestHeader.addAll(headers);
      request.headers.addAll(requestHeader);
      var response = await request.send();
      
      if(response.statusCode == 200 || response.statusCode == 201){
        return true;
      }else{
        return false;
      }
    } catch (e) {
        print(e);
        return false;
  
    }
  }
  Future<bool> postTeacherAbsentApi({ required String title,String? scheduleId,String? date,String? activityType,}) async {
      var dataaccess = await _pref.getJsonToken;
       http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/schedule/create-activity"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body: jsonEncode(<String, dynamic>{
          "title": title,
          "schedule_id": scheduleId,
          "date": date,
          "activity_type": activityType,
        },
      ),
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print("PostArticle suucesss");
      return true;
    }else{
       print("PostArticle Error");

      return false;
    }
  }

}
