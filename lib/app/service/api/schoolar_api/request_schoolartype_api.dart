import 'dart:convert';
import 'dart:io';
import 'package:camis_teacher_application/app/models/schoolarship_model/all_schoolarship_model.dart';
import 'package:camis_teacher_application/app/models/schoolarship_model/type_schoolarship_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:http/http.dart' as http;
import '../../../models/schoolarship_model/request_schoolarship_model.dart';
import '../../../storages/get_storage.dart';
import '../../base_url.dart';

class GetSchoolarClass {
  final BaseUrl _baseUrl = BaseUrl();
  GetStoragePref _prefs = GetStoragePref();

  // Get Type of SchoolarShip
  Future<TypeofSchoolarModel> getSchoolarTypeApi() async {
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/request-company/list-type"),
      headers: <String, String> {
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201){
      return TypeofSchoolarModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) { print(e);});
    }
  }

 // Posting Request Form
  Future<RequestSchoolarModel> postAttachRequestApi({ 
    required String studentId, 
    required String classId, 
    required String schoolyearId,
    required String typeSchoolar,  
    String? description,  
    List<File>? attachment}) async {

      var auth = await _prefs.getJsonToken;
      var url = Uri.parse("${auth.accessUrl}/request-company/create-request");
      var headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "${auth.tokenType}${auth.accessToken}"
      };
      
      var request = http.MultipartRequest('POST', url);
      request.fields['student_id'] = studentId;
      request.fields["class_id"] = classId.toString();
      request.fields["schoolyear_id"] = schoolyearId.toString();
      request.fields["type"] = typeSchoolar.toString();
      request.fields["description"] = "$description ";

      for (var file in attachment!) {
        var stream = http.ByteStream(file.openRead());
        var length = await file.length();
        var multipartFile = http.MultipartFile('attachments[]', stream, length, filename: file.path);
        request.files.add(multipartFile);
      }
      
      var requestHeaders = request.headers;
      requestHeaders.addAll(headers);
      request.headers.addAll(requestHeaders);
      var responseSend = await request.send();

      http.Response response = await http.Response.fromStream(responseSend);


      /// status 1 approve, 2 rejected, null waiting
      
      var statusField = json.decode(response.body);
      var status = statusField['status'];
      var message = statusField['message'];
      print("Warning Status $status, - $message");

      if (response.statusCode == 200 || response.statusCode == 201 ) {
        return RequestSchoolarModel.fromJson(jsonDecode(response.body));
      } else {
        throw Exception((e) { print(e);});
      }
    }

  // Get All student Schoolarship
  Future<AllSchoolarModel> getAllStudentSchoolarApi()async{
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/request-company/get-all-request"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201){
      print("sdfsa${response.body}");
      return AllSchoolarModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) { 
        print(e);});
    }
  }

  Future<bool> deleteScolarshipList({ String? id,}) async {
    var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.delete(
        Uri.parse("${dataaccess.accessUrl}/request-company/delete-request/$id".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      return true;
    }else{
      return false;
    }
  }
}
