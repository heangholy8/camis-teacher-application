import 'dart:convert';
import 'package:camis_teacher_application/app/models/approved_model/approved_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class ApprovedApi {
  Future<ApprovedRespone> postApproved({
    required int? classId,
    required int? examObjectId,
    required int? isApprove,
    required String? discription,

  }) async {
    GetStoragePref prefs = GetStoragePref();
    var dataaccess = await prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/score-enter/action-approve-subject-exam/${classId.toString()}"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
        "exam_object_id": examObjectId.toString(),
        "is_approve": isApprove.toString(),
        "description":discription.toString()
      
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return ApprovedRespone.fromJson(jsonDecode(response.body));
    } else {
      debugPrint("error ${response.body}");
      throw Exception((e) {
        debugPrint("Error: $e");
      });
    }
  }
}
