import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../models/study_affaire_model/get_daily_attendance/get_daily_attendance_teacher_model.dart';

class GetTeacherAttendanceForStudyAffairApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<AttendanceTeacherListModel> getTeacherAttendanceApi({required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/staff-attendance/list-staff-attendance-by-date?date=$date"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return AttendanceTeacherListModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
  Future<AttendanceTeacherListModel> getTeacherAttendanceByPrincipleApi({required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/mobile/staff-attendance/list-staff-attendance-by-date?date=$date"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return AttendanceTeacherListModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}