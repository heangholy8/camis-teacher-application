import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;

import '../../../models/primary_school_model/list_subject_enter_score_primary_model.dart';

class GetPrimaryListSubjectApi{
  Future<ListSubjectEnterScorePrimaryModel> getListSubjectEcnterScorePrimarySchoolApi({required String? idClass,required String? term,required String? month,required String?type}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/primary-school/list-subject-exam-date-in-class?month=$month&semester=$term&type=$type&class_id=$idClass"),
      headers:<String,String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("dsfdasf${response.body}");
      return ListSubjectEnterScorePrimaryModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("$e");
      });
    }
  }
}