// ignore_for_file: depend_on_referenced_packages, avoid_print

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';
import '../../base_url.dart';

class UpdatingIsverifyApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future updateIsverifyApi({
    required String phoneNumber,
  }) async {
    GetStoragePref prefs = GetStoragePref();
    var token = await prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse(
        "${token.accessUrl}/verification/verify-teacher-number".trim(),
      ),
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer ${token.accessToken}",
      },
      body: {"phone": phoneNumber},
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("success verify phone number ${response.body}");
      return true;
    } else if (response.statusCode == 302) {
      return false;
    } else {
      throw Exception((e) {
        print("Updating isVerify $e");
      });
    }
  }
}


/// 1 instructor, 2 subject
