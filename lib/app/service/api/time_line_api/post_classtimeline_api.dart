import 'dart:convert';
import 'dart:io';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

Future<bool> posClassTimelineApi({required String postText,String? parent, List<File>? attachments,List<String>? classID}) async {
  GetStoragePref _pref = GetStoragePref();
  var auth = await _pref.getJsonToken;
  var url = Uri.parse("${auth.accessUrl}/class-time-line/post-multiple-class");
  var headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "${auth.tokenType}${auth.accessToken}"
  };
  
  var request = http.MultipartRequest('POST', url);

  request.fields['post_text'] = postText;
  request.fields["parent"] = parent.toString();

  if(classID !=null){
    for(int i=0;i< classID.length;i++){
      request.fields['class_id[$i]'] = classID[i];
    }  
  }else{
    print("Class is null");
  }

  // for(var classid in classID!) {
  //   request.fields['class_id[]'] = classid.toString();
  // }
  
  
  for (var file in attachments!) {
    var stream = http.ByteStream(file.openRead());
    var length = await file.length();
    var multipartFile = http.MultipartFile('attachments[]', stream, length, filename: file.path);
    request.files.add(multipartFile);
  }
  
  var requestHeaders = request.headers;
  requestHeaders.addAll(headers);
  request.headers.addAll(requestHeaders);
  var response = await request.send();
  
  if (response.statusCode == 200 || response.statusCode==201 ) {
   return true;
  } else {
    print("Errrrror ");
    return false;
  }
}


Future<bool> posSchoolTimelineApi({required String postText,String? parent, List<File>? attachments}) async {
  GetStoragePref _pref = GetStoragePref();
  var auth = await _pref.getJsonToken;
  var url = Uri.parse("${auth.accessUrl}/mobile/school-time-line/post");
  var headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "${auth.tokenType}${auth.accessToken}"
  };
  
  var request = http.MultipartRequest('POST', url);

  request.fields['post_text'] = postText;
  request.fields["parent"] = parent.toString();
  
  for (var file in attachments!) {
    var stream = http.ByteStream(file.openRead());
    var length = await file.length();
    var multipartFile = http.MultipartFile('attachments[]', stream, length, filename: file.path);
    request.files.add(multipartFile);
  }
  
  var requestHeaders = request.headers;
  requestHeaders.addAll(headers);
  request.headers.addAll(requestHeaders);
  var response = await request.send();
  
  if (response.statusCode == 200 || response.statusCode==201 ) {
   return true;
  } else {
    print("Errrrror ");
    return false;
  }
}

Future<bool> deleteSchoolTimeLineApi({required String postId}) async{
 
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.delete(Uri.parse("${dataaccess.accessUrl}/mobile/school-time-line/delete/$postId"),
    headers: <String, String>{
        "Accept": "application/json",
        "Content-Type":"application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    // print("object$postId");
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("delete success $postId");
      return true;
    } 
    else {
       print("delete unsuccess");
      return false;
    }

    // GetStoragePref _prefs = GetStoragePref();
    // var dataaccess = await _prefs.getJsonToken;
    // http.Response response = await http.delete(Uri.parse("${dataaccess.accessUrl}/class-time-line/delete/$postId"),
    // headers: <String, String>{
    //     "Accept": "application/json",
    //     "Content-Type":"application/json",
    //     "Authorization": "Bearer${dataaccess.accessToken}",
    //   },
    // );
    // if (response.statusCode == 200 || response.statusCode == 201) {
    //   print("delete success $postId");
    //   return true;
    // } else {
    //    print("delete unsuccess");
    //   return false;
    // }
  
  
}

Future<bool> updateSchoolTimelineApi({required String postText,String? parent, List<File>? attachments,String? postID}) async {
  GetStoragePref _pref = GetStoragePref();
  var auth = await _pref.getJsonToken;
  var url = Uri.parse("${auth.accessUrl}/mobile/school-time-line/edit/$postID");
  var headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "${auth.tokenType}${auth.accessToken}"
  };
  
  var request = http.MultipartRequest('POST', url);

  request.fields['post_text'] = postText;
  request.fields["parent"] = parent.toString();

  for (var file in attachments!) {
    var stream = http.ByteStream(file.openRead());
    var length = await file.length();
    var multipartFile = http.MultipartFile('attachments[]', stream, length, filename: file.path);
    request.files.add(multipartFile);
  }
  
  var requestHeaders = request.headers;
  requestHeaders.addAll(headers);
  request.headers.addAll(requestHeaders);
  var response = await request.send();
  
  if (response.statusCode == 200 || response.statusCode==201 ) {
   return true;
  } else {
    print("Errrrror ");
    return false;
  }
}

Future<bool> delectAttSchoolTimeLine({required List<String> idAtta}) async{
 
    GetStoragePref _pref = GetStoragePref();
  var auth = await _pref.getJsonToken;
  var url = Uri.parse("${auth.accessUrl}/mobile/school-time-line/delete-school-time-line-attachment");
  var headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "${auth.tokenType}${auth.accessToken}"
  };
  
  var request = http.MultipartRequest('POST', url);

  if(idAtta !=null){
    for(int i=0;i< idAtta.length;i++){
      request.fields['attachment_id[$i]'] = idAtta[i];
    }  
  }else{
    print("idAtta is null");
  }
  
  var requestHeaders = request.headers;
  requestHeaders.addAll(headers);
  request.headers.addAll(requestHeaders);
  var response = await request.send();
  if (response.statusCode == 200 || response.statusCode==201 ) {
   return true;
  } else {
    print("Errrrror ");
    return false;
  }
}


Future<bool> deleteClassTimeLineApi({required String postId}) async{
 
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.delete(Uri.parse("${dataaccess.accessUrl}/class-time-line/delete/$postId"),
    headers: <String, String>{
        "Accept": "application/json",
        "Content-Type":"application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    // print("object$postId");
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("delete success $postId");
      return true;
    } 
    else {
       print("delete unsuccess");
      return false;
    }

    // GetStoragePref _prefs = GetStoragePref();
    // var dataaccess = await _prefs.getJsonToken;
    // http.Response response = await http.delete(Uri.parse("${dataaccess.accessUrl}/class-time-line/delete/$postId"),
    // headers: <String, String>{
    //     "Accept": "application/json",
    //     "Content-Type":"application/json",
    //     "Authorization": "Bearer${dataaccess.accessToken}",
    //   },
    // );
    // if (response.statusCode == 200 || response.statusCode == 201) {
    //   print("delete success $postId");
    //   return true;
    // } else {
    //    print("delete unsuccess");
    //   return false;
    // }
  
  
}

Future<bool> updateClassTimelineApi({required String postText,String? parent, List<File>? attachments,String? postID}) async {
  GetStoragePref _pref = GetStoragePref();
  var auth = await _pref.getJsonToken;
  var url = Uri.parse("${auth.accessUrl}/class-time-line/edit/$postID");
  var headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "${auth.tokenType}${auth.accessToken}"
  };
  
  var request = http.MultipartRequest('POST', url);

  request.fields['post_text'] = postText;
  request.fields["parent"] = parent.toString();

  for (var file in attachments!) {
    var stream = http.ByteStream(file.openRead());
    var length = await file.length();
    var multipartFile = http.MultipartFile('attachments[]', stream, length, filename: file.path);
    request.files.add(multipartFile);
  }
  
  var requestHeaders = request.headers;
  requestHeaders.addAll(headers);
  request.headers.addAll(requestHeaders);
  var response = await request.send();
  
  if (response.statusCode == 200 || response.statusCode==201 ) {
   return true;
  } else {
    print("Errrrror ");
    return false;
  }
}

Future<bool> delectAttClassTimeLine({required List<String> idAtta}) async{
 
    GetStoragePref _pref = GetStoragePref();
  var auth = await _pref.getJsonToken;
  var url = Uri.parse("${auth.accessUrl}/class-time-line/delete-class-time-line-attachment");
  var headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "${auth.tokenType}${auth.accessToken}"
  };
  
  var request = http.MultipartRequest('POST', url);

  if(idAtta !=null){
    for(int i=0;i< idAtta.length;i++){
      request.fields['attachment_id[$i]'] = idAtta[i];
    }  
  }else{
    print("idAtta is null");
  }
  
  var requestHeaders = request.headers;
  requestHeaders.addAll(headers);
  request.headers.addAll(requestHeaders);
  var response = await request.send();
  if (response.statusCode == 200 || response.statusCode==201 ) {
   return true;
  } else {
    print("Errrrror ");
    return false;
  }
}

