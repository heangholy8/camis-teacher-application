import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';

import '../../../models/study_affaire_model/get_class_by_level_model.dart';
import '../../../models/study_affaire_model/get_current_grades/get_current_grades_affaire_model.dart';

class GetCurrentGradeAffaireApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<GetCurrentGradeAffaireModel> getCurrentGradeAffaireApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/list-all-level-classes"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("asdfdsa${response.body}");
      return GetCurrentGradeAffaireModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<GetCurrentGradeAffaireModel> getCurrentGradePrincipleApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/utility/list-all-level-classes"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return GetCurrentGradeAffaireModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<StudentAffairLevelModel> getClassStudentAffairByLevelApi({required String month, required String semester, required String type,required String level, required String page}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/score-enter/list-subject-class-enter-score?month=$month&semester=$semester&type=$type&level=$level&page=$page&limit=5"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return studentAffairLevelModelFromJson(response.body.toString());
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}