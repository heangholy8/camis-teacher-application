import 'dart:convert';
import 'package:camis_teacher_application/app/models/check_attendance_model/check_attendance_model.dart';
import 'package:camis_teacher_application/app/service/base_url.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../models/check_attendance_model/respon_attendace_model.dart';
import '../../../models/check_attendance_model/summary_attendance_model.dart';

class GetCheckStudentAttendanceList {
  final BaseUrl _baseUrl = BaseUrl();
  Future<CheckAttendanceModel> getCheckAttendanceList(
      {required String? class_id,
      required String? schedule_id,
      required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/schedule/get-list-student-attendance"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
        "class_id": class_id,
        "schedule_id": schedule_id,
        "date": date,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return CheckAttendanceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("Error:" + e);
      });
    }
  }

  Future<ResponCheckAttendanceModel> postAttendanceApi({Data? data}) async {
    GetStoragePref _prefs = GetStoragePref();
    var accessURL = await _prefs.getJsonToken;
   
    http.Response response = await http.post(
      Uri.parse("${accessURL.accessUrl}/schedule/save-list-student-attendance"),
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer ${accessURL.accessToken}",
        "Content-Type": "application/json"
      },
      body: json.encode(data),
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("alksjflaksdfj" + response.body);
      return ResponCheckAttendanceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("Errore Post Attendance : $e");
        return e.toString();
      });
    }
  }

  Future<ResponCheckAttendanceModel> postAttendancePrimaryApi({Data? data}) async {
    GetStoragePref _prefs = GetStoragePref();
    var accessURL = await _prefs.getJsonToken;
   
    http.Response response = await http.post(
      Uri.parse("${accessURL.accessUrl}/primary-school/save-list-student-attendance"),
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer ${accessURL.accessToken}",
        "Content-Type": "application/json"
      },
      body: json.encode(data),
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("alksjflaksdfj" + response.body);
      return ResponCheckAttendanceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("Errore Post Attendance : $e");
        return e.toString();
      });
    }
  }

  Future<SummaryAttendanceModel> getSummaryAttendanceList(
      {required String? class_id,
      required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/schedule/get-summary-attendance"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
        "class_id": class_id,
        "date": date,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("sadfsad${response.body}");
      return SummaryAttendanceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("Error:" + e);
      });
    }
  }
}
