import 'dart:convert';
import 'package:camis_teacher_application/app/models/schedule_model/schedule_time_in_day.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;

class GetTeachingTimeApi {
  Future<ScheduleTimeModel> getTeachingTimeRequestApi({required String? idClass,required String? idsubject,required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/schedule/get-subject-time"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body: {
        "class_id" : idClass,
        "subject_id" : idsubject,
        "date" : date,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ScheduleTimeModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}