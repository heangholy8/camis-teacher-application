import 'dart:convert';

import 'package:flutter/cupertino.dart';

import '../../../models/attachment_model/post_attachment_model.dart';
import '../../../modules/home_screen/e.homscreen.dart';
import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;
 class PostAttachmetnApi{
  Future<PostAttachmentRespone> postAttachmentApi({
    required List<File> image,
    required String subjectId,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;

    var request = http.MultipartRequest(
      'POST',
      Uri.parse("${dataaccess.accessUrl}/score-enter/save-subject-exam-attachment"),
    );
    Map<String, String> headers = {
      "Content-type": "multipart/form-data",
      "Content-Type": "application/json",
      "Authorization": "Bearer ${dataaccess.accessToken}",
    };
   request.fields.addAll({
      "subject_exam_id": subjectId,
    });
    List<http.MultipartFile> files = [];
    for(File file in image){
      var f = http.MultipartFile(
        'attachments[]',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        filename: file.path);
      files.add(f);
    }

    request.files.addAll(files);
    
    request.headers.addAll(headers);
    var res = await request.send();
    http.Response response = await http.Response.fromStream(res);

    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return PostAttachmentRespone.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e.toString());
      });
    }
  }
}