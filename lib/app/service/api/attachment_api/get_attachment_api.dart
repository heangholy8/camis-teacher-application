import 'dart:convert';

import 'package:camis_teacher_application/app/models/image_view_model/image_view_model.dart';

import '../../../storages/get_storage.dart';
import '../../base_url.dart';
import 'package:http/http.dart' as http;

class GetAttachmentApi{
  final BaseUrl _baseUrl = BaseUrl();
  Future<ImageViewModel> getAttachmentExam(
      {required String? exam_id,
     }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse(
          "${dataaccess.accessUrl}/score-enter/get-subject-exam-attachment/$exam_id"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ImageViewModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}