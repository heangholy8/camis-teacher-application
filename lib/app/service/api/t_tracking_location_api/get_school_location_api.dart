import 'package:flutter/material.dart';

import '../../../models/tracking_location_model/school_location_model.dart';
import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;

class GetSchoolLocationApi {
  Future<SchoolLocationModel> getSchoolLocationApi({required String schoolCode}) async {
      GetStoragePref prefs = GetStoragePref();
      var dataaccess = await prefs.getJsonToken;
      http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/get-school-location?school_code=/${schoolCode.toString()}"),
        headers: <String, String>{
          "Accept": "application/json",
          "Authorization": "Bearer ${dataaccess.accessToken}",
        },
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        print("fsadfsa${response.body}");
        return schoolLocationModelFromJson(response.body);
      } else {
        throw Exception((e) {
          print("Get school location is error: "+e.toString());
        });
      }
    }

}