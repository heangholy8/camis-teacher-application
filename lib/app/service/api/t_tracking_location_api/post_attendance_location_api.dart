import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;

class PostAttendanceLocationApi {
  Future<bool> postAttendanceLocationApi({required String classId,required String subjectId,required String timeTSId, required String type,required String lat,required String log}) async {
      GetStoragePref prefs = GetStoragePref();
      var dataaccess = await prefs.getJsonToken;
      http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/utility/save-staff-present-location"),
        headers: <String, String>{
          "Accept": "application/json",
          "Authorization": "Bearer ${dataaccess.accessToken}",
        },
        body: {
          "class_id":classId,
          "subject_id":subjectId,
          "time_table_setting_id":timeTSId,
          "type":type,
          "latitude":lat,
          "longitude":log
        }
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        print("holy${response.body}");
        return true;
      } else {
        throw Exception((e) {
          print("Get school location is error: "+e.toString());
        });
      }
    }

}