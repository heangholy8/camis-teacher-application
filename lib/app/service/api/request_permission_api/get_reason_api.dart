import 'dart:convert';

import 'package:http/http.dart' as http;
import '../../../models/request_absent_model/admission_model.dart';
import '../../../storages/get_storage.dart';

class GetReasonApi {
  Future<AdmissionModel> getReasonApi({String? idRequestAtt}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/request-absence-permission/get-admission-type-data"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return AdmissionModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

}
