import 'dart:convert';
import 'dart:io';
import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;

class RequestPermssionApi {

  Future updateRequestPermssionApi({
    required String startDate,
    required String endDate,
    required String requestType,
    required String admissionId,
    required List<String> studentId,
    required List<String> scheduleId,
    required List<File> listFile,
    bool isUpdate = false,
    String? id

  }) async {
    try {
      GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;
      // var body = jsonEncode();
        var request = http.MultipartRequest(
        'POST', Uri.parse(
             "${auth.accessUrl}/request-absence-permission/create-update-request-permission"),
      );
      request.fields.addAll({
          "start_date": startDate,
          "end_date": endDate,
          "request_type":requestType,
          "admission_type": admissionId,
          "description": "",
          "id":id.toString()
        });
        if(scheduleId.isNotEmpty){
          for(int i = 0;i<scheduleId.length;i++){
            request.fields["schedule_id[$i]"]=scheduleId[i];
          }
        } else{
          request.fields.addAll({
            "schedule_id":""
          });
        }

      if(listFile.isNotEmpty){
            for (int i = 0;i<listFile.length;i++) {
            var stream = http.ByteStream(listFile[i].openRead());
            var length = await listFile[i].length();
            var multipartFile = http.MultipartFile('file[$i]', stream, length, filename: listFile[i].path);
            request.files.add(multipartFile);
          }
      }

      var headers = {
        "Accept": "application/json",
        "Content-Type":"application/json",
        "Authorization":"Bearer${auth.accessToken}",
      };

      request.headers.addAll(headers);
      var res = await request.send();
      http.Response response = await http.Response.fromStream(res);
      print("${response.body}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
    } else{
      throw(e){
        print("error permission"+ e);
      };
    }
    } catch (e) {
      print("error permission request "+ e.toString());
    }
  }

  Future postRequestPermssionApi({
    required String startDate,
    required String endDate,
    required String requestType,
    required String admissionId,
    required List<String> studentId,
    required List<String> scheduleId,
    required List<File> listFile,
    bool isUpdate = false,

  }) async {
    try {
      GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;
      // var body = jsonEncode();
      var request = http.MultipartRequest(
        'POST', Uri.parse("${auth.accessUrl}/request-absence-permission/create-update-request-permission"),
      );
      request.fields.addAll({
          "start_date": startDate,
          "end_date": endDate,
          "request_type":requestType,
          "admission_type": admissionId,
          "description": "",
        });
        if(scheduleId.isNotEmpty){
          for(int i = 0;i<scheduleId.length;i++){
            request.fields["schedule_id[$i]"]=scheduleId[i];
          }
        } else{
          request.fields.addAll({
            "schedule_id":""
          });
        }

      if(listFile.isNotEmpty){
            for (int i = 0;i<listFile.length;i++) {
            var stream = http.ByteStream(listFile[i].openRead());
            var length = await listFile[i].length();
            var multipartFile = http.MultipartFile('file[$i]', stream, length, filename: listFile[i].path);
            request.files.add(multipartFile);
          }
      }

      var headers = {
        "Accept": "application/json",
        "Content-Type":"application/json",
        "Authorization":"Bearer${auth.accessToken}",
      };

      request.headers.addAll(headers);
      var res = await request.send();
      http.Response response = await http.Response.fromStream(res);
      print("${response.body}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
    } else{
      throw(e){
        print("error permission"+ e);
      };
    }
    } catch (e) {
      print("error permission request "+ e.toString());
    }
  }
}
