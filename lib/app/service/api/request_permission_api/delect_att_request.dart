import 'dart:convert';

import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class DelectPermissionApi {
  Future<bool> deleteRequestAttApi({String? idRequestAtt}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.delete(
        Uri.parse("${dataaccess.accessUrl}/request-absence-permission/delete-request-permission-attachment/$idRequestAtt".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print("Delete Success");
      return true;
    }else{
      print("Delete Fail");
      return false;
    }
  }

  Future<bool> deleteRequestApi({String? idRequest}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.delete(
        Uri.parse("${dataaccess.accessUrl}/request-absence-permission/delete-request-permission/$idRequest".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print("Delete Success");
      return true;
    }else{
      print("Delete Fail");
      return false;
    }
  }

}
