import 'dart:convert';
import 'package:http/http.dart' as http;
import '../../../models/request_absent_model/own_present_in_month_model.dart';
import '../../../storages/get_storage.dart';

class GetOwnPresentApi {
  Future<OwnPrensentInMonthModel> getOwnPresentApi({String? month,String? year,String? type}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/utility/get-own-staff-all-present-location-by-type?month=$month&year=$year&type=$type"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return OwnPrensentInMonthModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

}