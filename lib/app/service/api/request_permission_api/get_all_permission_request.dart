import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../models/request_absent_model/list_all_permission_request_model.dart';
import '../../../storages/get_storage.dart';

class GetAllPermissionRequestApi {
  Future<AllPermissionRequestModel> getAllPermissionRequestApi({String? month,String? year}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/utility/get-list-own-staff-all-present-location-by-type?month=$month&year=$year"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return AllPermissionRequestModel.fromJson(jsonDecode(response.body));
    } else {
      debugPrint(response.body);
      throw Exception((e) {
        debugPrint(e);
      });
    }
  }

}