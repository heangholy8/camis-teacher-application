// ignore_for_file: depend_on_referenced_packages

import 'dart:convert';
import 'package:camis_teacher_application/app/service/base_url.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;
import '../../../models/auth_model/auth_model.dart';

class AuthApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<AuthModel> signInRequestApi(
      {required String schoolCode,
      required String loginName,
      required String password,
      String deviceToken ="",
      String? platform}) async {
    //  await FirebaseMessaging.instance.getAPNSToken().then((value) {
    //   deviceToken = value.toString();
    // });
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.staffBaseUrl}/access-school-login".trim()),
      body: {
        "school_code": schoolCode,
        "loginname": loginName,
        "password": password,
        "device_token": deviceToken,
        "platform": platform
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
       debugPrint("dsfasd${response.body}");
      return AuthModel.fromJson(jsonDecode(response.body));
     
    } else {
      throw Exception((e) {
        debugPrint(e.toString());
      });
    }
  }

  Future<bool> changePasswordApi(
      {required String currentPass,
      required String newPass,
      required String comfirmPass,}) async {
        GetStoragePref _prefs = GetStoragePref();
         var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/change-password".trim()),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
        "current_password": currentPass,
        "password": newPass,
        "password_confirmation": comfirmPass
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
       debugPrint("changePassword${response.body}");
      return true;
     
    } else {
      debugPrint("changePassword error");
      return false;
      // throw Exception((e) {
      //   debugPrint("changePassword$e");
      //   return false;
      // });
    }
  }
}
