import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;

import '../../../models/list_month_semester/list_month_semester_model.dart';

class GetListMonthSemesterApi {
  Future<ListMonthSermesterModel> getListMonthSemester() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse(
          "${dataaccess.accessUrl}/utility/get-list-all-month"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ListMonthSermesterModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}