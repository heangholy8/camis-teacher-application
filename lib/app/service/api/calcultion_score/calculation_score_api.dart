import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../models/exam_model/calculate_score_model.dart';
import '../../../storages/get_storage.dart';

class CalculationScoreApi {
  Future<CalculateScoreModel> calculationScoreApi({
    required String? classId,
    required String? semester,
    required String? month,
    required String? type,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse(
          "${dataaccess.accessUrl}/score-management/calculation-score"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
        "month": month.toString(),
        "semester":semester.toString(),
        "type": type.toString(),
        "class_id":classId.toString(),
      
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return CalculateScoreModel.fromJson(jsonDecode(response.body));
    } else {
      debugPrint("error ${response.body}");
      throw Exception((e) {
        debugPrint("Error: $e");
      });
    }
  }
}
