import 'dart:convert';
import 'package:camis_teacher_application/app/models/contact_model/contact_model.dart';
import 'package:camis_teacher_application/app/service/base_url.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class GetContactApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ContactModel> getContactApi() async {
     GetStoragePref _prefs = GetStoragePref();
     var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.staffBaseUrl}/get-app-support-contact"),
      headers: <String, String>{
          "Accept": "application/json",
        },
        body: <String, dynamic>{
          "school_id":dataaccess.schoolCode.toString()
        }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("fas,dfnadskf${response.body}");
      return ContactModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}