// ignore_for_file: avoid_print

import 'dart:convert';
import 'package:camis_teacher_application/app/models/report_model/own_subject_model.dart';
import 'package:flutter/material.dart';
import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;

class OwnSubjectListApi{
  Future<OwnSubjectModel> getOwnSubjectApi({
    required String? classId,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
     http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/utility/get-list-own-subjects/$classId"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("All subject in class${response.body}");
      return OwnSubjectModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<OwnSubjectModel> getListSubjectApi({
    required String? classId,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
     http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/utility/list-subjects-teachers-in-class/$classId"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return OwnSubjectModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

}