import 'dart:convert';
import 'package:camis_teacher_application/app/models/grading_model/grading_model.dart';
import 'package:camis_teacher_application/app/models/grading_model/grading_respone_model.dart';
import 'package:camis_teacher_application/app/service/base_url.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;


import '../../../storages/user_storage.dart';
  List<StudentsData> listStudeng = [
        StudentsData(
          studentId:"37854342273749328133",
          score:50,
          absentExam:null,
          teacherComment:null
        ),
         StudentsData(
          studentId:"30500803934508873343",
          score:50,
          absentExam:0,
          teacherComment:"rean min kert reak kom pee"
        ),
         StudentsData(
          studentId:"71473394349038733843",
          score:50,
            absentExam:0,
            teacherComment:"rean min kert reak kom pee"
        ),
        
      ];

class PostGradingApi {
  final BaseUrl _baseUrl = BaseUrl();
Future<GradingSubjectModel> postListStudentSubjectExam (
  {
    required int classID,
    required int subjectID,
    required int type,
    required String term,
    required int month,
    required String examDate,
    required List<StudentsData> listStudentScore
  }
)async{
      GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
        var body={
       "class_id":classID,
       "subject_id":subjectID,
      "type":type,
      "term":term,
      "month":month,
      "exam_date":examDate,
      "list_students_score":listStudentScore,
    };
    print("list : $listStudentScore");
    http.Response response = await http.post(
      Uri.parse(
          "${dataaccess.accessUrl}/score-enter/student-score-enter"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type":"application/json", 
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: jsonEncode(body),
      );

    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return GradingSubjectModel.fromJson(jsonDecode(response.body));
    } else {
      print("error ${response.body}");
      throw Exception((e) {
        print("Error:"+e);

      });
    }}







}
