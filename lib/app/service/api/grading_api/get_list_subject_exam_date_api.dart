import 'dart:convert';
import 'package:camis_teacher_application/app/models/grading_subject_model/list_subject_exam.dart';
import 'package:camis_teacher_application/app/service/base_url.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../models/student_list_model/list_student_in_class_model.dart';
import '../../../storages/user_storage.dart';

class GetListSubjectExamDate {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ListSubjectExamModel> getListSubjectExamDate(
      {required String? month,
      required String? semester,
      required String? type}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse(
          "${dataaccess.accessUrl}/score-enter/list-subject-exam-date?month=$month&semester=$semester&type=$type"),
      //Uri.parse("https://api-services.camemis-learn.com/api/staff/ee9f5614-ffc2-11ea-89b1-00163c091aef/utility/get-list-student/$idClass".trim()),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ListSubjectExamModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}
