import 'dart:convert';
import 'package:camis_teacher_application/app/models/grading_model/grading_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class GradingApi {
  Future<GradingSubjectModel> getListStudentSubjectExam({
    required String? classId,
    required String? subjectId,
    required String? type,
    required String? month,
    required String? semester,
     required String? examDate,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse(
          "${dataaccess.accessUrl}/score-enter/list-student-subject-exam"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
        "class_id": classId,
        "subject_id": subjectId,
        "type": type,
        "term": semester,
        "month": month,
        "exam_date": examDate
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint( "fsadfsadf"+ response.body);
      return GradingSubjectModel.fromJson(jsonDecode(response.body));
    } else {
      debugPrint("error ${response.body}");
      throw Exception((e) {
        debugPrint("Error: $e");
      });
    }
  }

}
