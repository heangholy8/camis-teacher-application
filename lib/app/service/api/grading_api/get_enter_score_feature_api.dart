import 'dart:convert';
import 'package:camis_teacher_application/app/service/base_url.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../models/grading_model/enter_score_feature_model.dart';

class GetApiCheckConditionSwitchEnterScore {
  final BaseUrl _baseUrl = BaseUrl();
  Future<GetCheckCoditionSwitchEnterScoreScreenModel> getApiCheckConditionSwitchEnterScore() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/get-app-enter-score-feature"),
      //Uri.parse("https://api-services.camemis-learn.com/api/staff/ee9f5614-ffc2-11ea-89b1-00163c091aef/utility/get-list-student/$idClass".trim()),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return GetCheckCoditionSwitchEnterScoreScreenModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}
