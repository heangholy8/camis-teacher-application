import 'dart:convert';
import 'package:camis_teacher_application/app/models/study_affaire_model/get_daily_attendance/get_daily_attendance_model.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';

class GetDailyAtytendanceAffaireApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<GetDailyAttendanceAffaireModel> getDailyAttendanceAffaireApi({required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/schedule/list-by-date?date=$date"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return GetDailyAttendanceAffaireModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}