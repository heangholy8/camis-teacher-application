import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class PostActionTeacherApi {
  Future<bool> actionTeacherApi({String? feature,String? keyFeature}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/action-app-feature"),
        headers: <String, String>{
          "Accept": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body: {
          "feature":feature,
          "key_feature":keyFeature,
          "app_name":"teacher",
        }
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      debugPrint("post Success");
      return true;
    }else{
      debugPrint("post Fail");
      return false;
    }
  }
}
