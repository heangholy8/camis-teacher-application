import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/models/class_list_model/class_list_model.dart';
import '../../../storages/get_storage.dart';

class GetClassApi {
  Future<ClassModel> getClassRequestApi() async {
    GetStoragePref prefs = GetStoragePref();
    var dataaccess = await prefs.getJsonToken;

    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/teacher-academic/get-current"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ClassModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}
