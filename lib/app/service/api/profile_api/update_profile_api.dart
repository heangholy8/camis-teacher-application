import 'dart:convert';
import 'dart:io';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../models/profile_model/update_user_model.dart';

class UpdateProfileApi {
  Future<UpdateUserModel> updateProfileWithImageApi({
    required String firstname,
    required String lastname,
    required String firstnameEn,
    required String lastnameEn,
    required String gender,
    required String phone,
    required String dob,
    required String address,
    required String email,
    required File? image,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;

    var request = http.MultipartRequest(
      'POST',
      Uri.parse("${dataaccess.accessUrl}/update-profile-information"),
    );
    Map<String, String> headers = {
      "Content-type": "multipart/form-data",
      "Content-Type": "application/json",
      "Authorization": "Bearer${dataaccess.accessToken}",
    };
    request.fields.addAll({
      "firstname": firstname,
      "lastname": lastname,
      "firstname_en": firstnameEn,
      "lastname_en": lastnameEn,
      "gender": gender,
      "phone": phone,
      "dob": dob,
      "email": email,
      "address": address
    });
    if (image != null) {
      request.files.add(
        http.MultipartFile(
          'file',
          image.readAsBytes().asStream(),
          image.lengthSync(),
          filename: image.path.split('/').last,
        ),
      );
    }
    request.headers.addAll(headers);
    var res = await request.send();
    http.Response response = await http.Response.fromStream(res);
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return UpdateUserModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<UpdateUserModel> updateUserProfileApi({
    required String firstname,
    required String lastname,
    required String firstnameEn,
    required String lastnameEn,
    required String gender,
    required String phone,
    required String dob,
    required String email,
    required String address,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/update-profile-information"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body: jsonEncode(<String, dynamic>{
          "firstname": firstname,
      "lastname": lastname,
      "firstname_en": firstnameEn,
      "lastname_en": lastnameEn,
      "gender": gender,
      "phone": phone,
      "dob": dob,
      "email": email,
      "address": address
        }));
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return UpdateUserModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}
