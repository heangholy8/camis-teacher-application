import 'dart:convert';
import 'package:camis_teacher_application/app/models/profile_model/profile_model.dart';
import 'package:camis_teacher_application/app/modules/login_screen/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../storages/get_storage.dart';

class GetProfileUserApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ProfileModel> getUserProfileRequestApi(BuildContext context) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/get-profile"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if(response.statusCode == 404){
      Navigator.push(context,PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) => const LoginScreen(),
        transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,));
    }
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ProfileModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}