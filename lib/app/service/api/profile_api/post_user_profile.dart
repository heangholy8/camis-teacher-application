import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../models/profile_model/update_user_model.dart';
import '../../../storages/get_storage.dart';
import '../../../storages/user_storage.dart';

class UpdateProfileUserApi {
  final BaseUrl _baseUrl = BaseUrl();

  Future<UpdateUserModel> updateUserProfileImageRequestApi({
    //  String? firstname,
    //  String? lastname,
    required File image,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;

    var request = http.MultipartRequest(
      'POST',
      Uri.parse("${dataaccess.accessUrl}/update-profile-information"),
    );
    Map<String, String> headers = {
      "Content-type": "multipart/form-data",
      "Content-Type": "application/json",
      "Authorization": "Bearer${dataaccess.accessToken}",
    };
    // request.fields.addAll({
    //  "firstname":firstname,
    //  "lastname":lastname,
    // });
    if (image != null) {
      request.files.add(
        http.MultipartFile(
          'file',
          image.readAsBytes().asStream(),
          image.lengthSync(),
          filename: image.path.split('/').last,
        ),
      );
    }
    request.headers.addAll(headers);
    var res = await request.send();
    http.Response response = await http.Response.fromStream(res);
    //var responseData = json.decode(response.body);
    // http.Response response = await http.post(
    //   Uri.parse("${dataaccess.accessUrl}/update-profile-information"),
    //   //Uri.parse("https://api-services.camemis-learn.com/api/staff/ee9f5614-ffc2-11ea-89b1-00163c091aef/update-profile-information"),
    //   headers: <String, String>{
    //       "Accept": "application/json",
    //       "Content-Type":"application/json",
    //       "Authorization":"Bearer${dataaccess.accessToken}",
    //     },
    //   body: jsonEncode(<String, String>{
    //       "firstname":firstname,
    //       "lastname":lastname,
    //   })
    // );

    if (response.statusCode == 200 || response.statusCode == 201) {
      return UpdateUserModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e.toString());
      });
    }
  }

  Future<UpdateUserModel> updateUserProfileRequestApi({
    required String firstname,
    required String lastname,
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/update-profile-information"),
        //Uri.parse("https://api-services.camemis-learn.com/api/staff/ee9f5614-ffc2-11ea-89b1-00163c091aef/update-profile-information"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body: jsonEncode(<String, String>{
          "firstname": firstname,
          "lastname": lastname,
        }));
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
      return UpdateUserModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}
