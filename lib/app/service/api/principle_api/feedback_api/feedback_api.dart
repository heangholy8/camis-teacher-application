import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../../models/principle_model/feedback_model.dart';

class FeedbackApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<FeedBackModel> getFeedBackApi({required int isSeen,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/mobile/guardian-feedback/list?is_seen=$isSeen"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("Feedback ${response.body}");
      return FeedBackModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        
        print(e);
      });
    }
  }
  Future seenFeedback({required String id}) async {
    GetStoragePref prefs = GetStoragePref();
    var token = await prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse(
        "${token.accessUrl}/mobile/guardian-feedback/action-seen/$id".trim(),
      ),
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer ${token.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("seen feedback ${response.body}");
      return true;
    } else if (response.statusCode == 302) {
      return false;
    } else {
      throw Exception((e) {
        print("seen feedback  $e");
      });
    }
  }
}