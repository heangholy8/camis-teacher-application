import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../../models/principle_model/list_class_check_attendance_bu_hour_model.dart';
import '../../../../models/principle_model/summary_attendance_teacher_model.dart';

class GetClassCheckAttApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ListClassCheckAttendanceByHourModel> getClassCheckAttApi({required String data,required int index}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/mobile/staff-attendance/list-by-date?date=$data&index=$index"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("class check att${response.body}");
      return ListClassCheckAttendanceByHourModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        
        print(e);
      });
    }
  }

  Future<ListClassCheckAttendanceByHourModel> getClassCheckAttDashboardApi({required String data,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/mobile/dashboard/list-class-attendance-checking-by-date?date=$data"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("class check att${response.body}");
      return ListClassCheckAttendanceByHourModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        
        print(e);
      });
    }
  }

  Future<CheckAttendanceSummaryTeacherModel> getSummaryAttTeacherDashboardApi({required String date,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/mobile/dashboard/get-summary-attendance?date=$date"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
      body: <String, String> {
        "date":date,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("check att teacher${response.body}");
      return CheckAttendanceSummaryTeacherModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        
        print(e);
      });
    }
  }
  
  Future<bool> principleCheckAttTeacherApi({required String actionObject,staffId,classId,subjectId,timeTableSettingId,date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/mobile/staff-attendance/save-staff-attendance"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
      body: <String, String> {
        "action_object":actionObject,
        "date":date,
        "staff_id":staffId,
        "class_id":classId,
        "subject_id":subjectId,
        "time_table_setting_id":timeTableSettingId,
        "type":"1",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("check att teacher${response.body}");
      return true;
    } else {
      throw Exception((e) {
        return false;
      });
    }
  }
}