import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';

import '../../../../models/principle_model/list_all_subjects_header.dart';
import '../../../../models/principle_model/list_check_class_input_score_model.dart';

class GetCheckClassInputScoreApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ListCheckClassInputScoreModel> getListClassInputScoreApi({required String month,required String type,required String semester,required String mapDefaultSubjectId}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/mobile/score-enter/list-subject-class-enter-score-every-level?month=$month&semester=$semester&type=$type&map_default_subject_id=$mapDefaultSubjectId"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("asdfdsa${response.body}");
      return ListCheckClassInputScoreModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<AllSubjectListModel> getListAllSubjectHeaderApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/utility/list-all-subjects-header"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return AllSubjectListModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}