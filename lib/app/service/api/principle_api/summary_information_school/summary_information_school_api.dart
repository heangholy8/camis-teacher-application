import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../../models/principle_model/all_year_in_school_model.dart';
import '../../../../models/principle_model/summary_student_study_result_model.dart';
import '../../../../models/principle_model/summary_student_teacher_model.dart';

class GetSummaryInforSchoolApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<AllYearInschool> getAllYearInschoolApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/mobile/all-schoolyear"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("get all school Year${response.body}");
      return AllYearInschool.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        
        debugPrint(e);
      });
    }
  }

  Future<SrudentAndTeacherInClass> getSrudentAndTeacherInClassApi({required String idSchoolYear,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/mobile/summary/student-teacher-class"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
      body: {
        "schoolyearId":idSchoolYear
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("get summary student and techer${response.body}");
      return SrudentAndTeacherInClass.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        
        debugPrint(e);
      });
    }
  }

  Future<SummaryStudentStudyResult> getSummaryStudentStudyResultApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/mobile/summary/student-study-result"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("get summary tudent study result${response.body}");
      return SummaryStudentStudyResult.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        
        debugPrint(e);
      });
    }
  }
}