import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:camis_teacher_application/app/service/base_url.dart';
import '../../../../models/principle_model/list_hour_in_date_model.dart';

class GetHourInDateApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ListHourInDateModel> getHourInDateApi({required String data}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/utility/list-all-study-hour?date=$data"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("List Hour${response.body}");
      return ListHourInDateModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}