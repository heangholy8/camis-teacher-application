import 'dart:convert';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../models/result/list_student_monthly_result_model.dart';
import '../../../models/result/list_student_semester_result_model.dart';
import '../../../models/result/list_student_year_result_model.dart';

class GetResultApi {
  Future<StudentMonthlyResultModel> getMonthlyResultApi({required String? idClass,required String? term,int? month}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/score-management/list-student-monthly-result?term=$term&month=$month&class_id=$idClass"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return StudentMonthlyResultModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<StudentSemesterResultModel> getSemesterResultApi({required String? idClass,required String? term,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/score-management/list-student-semester-result?class_id=$idClass&term=$term"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return StudentSemesterResultModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<StudentYearResultModel> getYearResultApi({required String? idClass,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/score-management/list-student-year-result?class_id=$idClass"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return StudentYearResultModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}