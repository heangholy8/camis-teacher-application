import 'dart:convert';
import 'package:http/http.dart' as http;
import '../../../models/teacher_commission_model/get_payment_transaction_model.dart';
import '../../../storages/get_storage.dart';

class GetTransactionApi {
  Future<TransactionPaymentModel> getTransactionRequestApi({required String? tranId,String? guardianId}) async {
     GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/payment/get-transaction"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body: {
        "transaction_id" : tranId.toString(),
        "guardian_id" : guardianId.toString(),
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return TransactionPaymentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}