import 'dart:convert';
import 'package:camis_teacher_application/app/models/teacher_commission_model/commission_teacher_model.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GetCommissionReportApi {
  Future<TeacherCommissionModel> getCommissionReportRequestApi(
      {required String? month,String?year}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/payment/query-list-commission?month=$month&year=$year"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return TeacherCommissionModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e);
      });
    }
  }
}
