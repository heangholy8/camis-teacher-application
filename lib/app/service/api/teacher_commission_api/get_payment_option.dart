import 'dart:convert';
import '../../../models/teacher_commission_model/payment_option_model.dart';
import '../../../storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PaymentOptionApi{
    Future<PaymentOptionModel> getPaymentOptionRequestApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/payment/list-payment-price"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return PaymentOptionModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e);
      });
    }
  }
}
