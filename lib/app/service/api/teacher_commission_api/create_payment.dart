import 'dart:convert';
import 'package:camis_teacher_application/app/models/teacher_commission_model/create_payment_bakong_model.dart';
import 'package:camis_teacher_application/app/models/teacher_commission_model/create_payment_cash_model.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import '../../../storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CreatePaymentApi{
  Future<CreatePaymentCashModel> getPaymentCreateCashApi({String? guardianId,studentId,classId, paymentType}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/payment/create-student-commission-payment"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
         "payment_method":"cash",
         "student_id":studentId,
         "class_id":classId,
         "choose_pay_option":paymentType,
         "guardian_id":guardianId,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return CreatePaymentCashModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e);
      });
    }
  }

  Future<CreatePaymentBakongModel> getPaymentCreateBakongApi({String? guardianId,studentId,paymentType,classId}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/payment/create-student-commission-payment".tr()),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
         "payment_method":"bakong",
         "student_id":studentId,
         "class_id":classId,
         "choose_pay_option":paymentType,
         "guardian_id":guardianId,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return CreatePaymentBakongModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e);
      });
    }
  }
}
