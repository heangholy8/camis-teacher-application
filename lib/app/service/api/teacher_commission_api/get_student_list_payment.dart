import 'dart:convert';
import 'package:camis_teacher_application/app/models/teacher_commission_model/student_list_payment_free_model.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GetStudentListPaymentApi {
  Future<ListStudentPaymentModel> getStudentListPaymentRequestApi(
      {required String? idClass}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/payment/list-student-payment?class_id=$idClass"),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ListStudentPaymentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
         print(e);
        debugPrint(e);
      });
    }
  }
}
