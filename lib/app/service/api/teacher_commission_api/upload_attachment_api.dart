import 'dart:convert';

import 'package:flutter/cupertino.dart';

import '../../../models/attachment_model/post_attachment_model.dart';
import '../../../modules/home_screen/e.homscreen.dart';
import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;
 class UploadAttachmentCommissionApi{
  Future<dynamic> uploadAttachmentCommissionApi({
    required List<File> attachments,
    required String year,
    required String month
  }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;

    var request = http.MultipartRequest(
      'POST',
      Uri.parse("${dataaccess.accessUrl}/payment/upload-attachment-commission"),
    );
    Map<String, String> headers = {
      "Content-type": "multipart/form-data",
      "Content-Type": "application/json",
      "Authorization": "Bearer${dataaccess.accessToken}",
    };

    request.fields.addAll({
      "year": year,
    });

    request.fields.addAll({
      "month":month
    });

    List<http.MultipartFile> files = [];
    for(File file in attachments){
      var f = http.MultipartFile(
        'attachments[]',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        filename: file.path);
      files.add(f);
    }

    request.files.addAll(files);
    
    request.headers.addAll(headers);
    var res = await request.send();
    http.Response response = await http.Response.fromStream(res);

    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      print("Upload Successfullyt");
      // return dynamic.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint(e.toString());
      });
    }
  }
}