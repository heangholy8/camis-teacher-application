import 'package:flutter/material.dart';

import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;

class PrintReport{
  Future<String> printMonthlyResutly({ String? classid,String? semester,String? month,String? column}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/print-pdf/month-result?month=$month&term=$semester&class_id=$classid&istwocolumn=$column".trim()),
      headers: <String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return response.body;
    } else {
      return response.body;
    }
  }
  
   Future<String> printSubjectMonthltyResultApi({
    String? month,
    String? term,
    String? classId,
    String? subjectId,
    String? type,
    String? istwocolumN,
   }) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse(
          "${dataaccess.accessUrl}/print-pdf/subject-month-result?month=$month&term=$term&class_id=$classId&subject_id=$subjectId&type=$type&istwocolumn=$istwocolumN"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print("Success printSubjectMonthlyResultApi");
      return response.body;
    }else{
      print("Error printSubjectMonthlyResultApi");
      return response.body;
    }
  }

  Future<String> printReportSemesterExamApi({required String classid,required String semester}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/semester-exam-result?term=$semester&class_id=$classid&istwocolumn=0".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }
  Future<String> printReportSemesterResultApi({required String classid,required String semester}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/semester-result?term=$semester&class_id=$classid&istwocolumn=1".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      print("fjasdfladsf${response.body}");
      return response.body;
    }
  }

  Future<String> printReportAttendanceMonthApi({required String classid,required String month}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/monthly-attendance-report?month=$month&class_id=$classid".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }
  Future<String> printReportAttendanceYearApi({required String classid}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/yearly-attendance-report?class_id=$classid".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }
  Future<String> printReportResultAllSubjectYearApi({required String classid}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/landscape-yearly-subject-result?class_id=$classid".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }
  Future<String> printReportResultAllSubjectMonthApi({required String classid,required String month}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/landscape-monthly-subject-result?class_id=$classid&month=$month".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }
  Future<String> printReportResultAllSubjectSemesterApi({required String classid,required String semester}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/semester-subject-exam-result?term=$semester&class_id=$classid".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }

  Future<String> printReportResultBigBookMonthApi({required String classid,required String month}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/monthly-result?month=$month&class_id=$classid".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }
  Future<String> printReportResultBigBookSemesterApi({required String classid,required String semester}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/semester-result-v2?class_id=$classid&term=$semester".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print(response.body);
      return response.body;
    }else{
      return response.body;
    }
  }

  Future<String> printReportHonoraryListApi({required String classid,required String semester,required String month,required String type}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/student-top-rank?month=$month&term=$semester&class_id=$classid&type=$type".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      debugPrint(response.body);
      return response.body;
    }else{
      debugPrint("Honorary list Error ${response.body}");
      return response.body;
    }
  }

  Future<String> printReportTeacherAttApi({required String month,type,date}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}${type=="month"?"/print-pdf/staff-monthly-attendance-report?month=$month":"/print-pdf/staff-daily-attendance-report?date=$date"}".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      debugPrint(response.body);
      return response.body;
    }else{
      debugPrint("Attendance Teacher Error ${response.body}");
      return response.body;
    }
  }

  Future<String> printReportSECApi({required String classId}) async {
    GetStoragePref _prefs = GetStoragePref();
      var dataaccess = await _prefs.getJsonToken;
        http.Response response = await http.get(
        Uri.parse("${dataaccess.accessUrl}/print-pdf/print-sec-class?class_id=$classId".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      debugPrint(response.body);
      return response.body;
    }else{
      debugPrint("SEC table class Error ${response.body}");
      return response.body;
    }
  }
}