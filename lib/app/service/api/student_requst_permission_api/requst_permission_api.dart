
import 'dart:convert';

import 'package:http/http.dart' as http;
import '../../../models/request_permission_model/list_request_permission_model.dart';
import '../../../storages/get_storage.dart';

class RequestPermissionApi {
  final GetStoragePref _pref = GetStoragePref();
  Future<ListPermissionRequestStudentModel> getRequestPermissionApi({required String? date,String? scheduleId}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/list-request-permission"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
      },
      body: jsonEncode(<String, dynamic>{
        "date": date,
        "schedule_id": scheduleId,
      },)
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ListPermissionRequestStudentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<bool> seenRequestPermissionApi({String? scheduleId,String? date,}) async {
      var dataaccess = await _pref.getJsonToken;
       http.Response response = await http.post(
        Uri.parse("${dataaccess.accessUrl}/seen-request-permission"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer${dataaccess.accessToken}",
        },
        body: jsonEncode(<String, dynamic>{
          "date": date,
          "schedule_id": scheduleId,
        },
      ),
    );
    if (response.statusCode ==200 || response.statusCode==201) {
      print("seen suucesss");
      return true;
    }else{
       print("seen Error");

      return false;
    }
  }

}
