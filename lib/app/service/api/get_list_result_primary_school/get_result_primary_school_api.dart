import 'dart:convert';
import 'package:camis_teacher_application/app/models/primary_school_model/list-all-student-subject-result_primary_model.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:http/http.dart' as http;

import '../../../models/primary_school_model/result_detail_month_semester_model.dart';

class GetPrimaryResultApi{
  Future<ListAllStudentSubjectResultPrimaryModel> getResultPrimarySchoolApi({required String? idClass,required String? term,required String? month,required String?type}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/primary-school/list-all-student-subject-result"),
      headers:<String,String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body:{
        "class_id": idClass,
        "type": type,
        "month": month,
        "term":term,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
      return ListAllStudentSubjectResultPrimaryModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("$e");
      });
    }
  }

  Future<StudentResultMonthSemesterDetailPrimaryModel> getDetailResultPrimarySchoolApi({required String? idClass,String? studentId,required String? term,required String? month,required String?type}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/primary-school/get-learning-result"),
      headers:<String,String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body:{
        "class_id": idClass,
        "student_id": studentId,
        "type": type,
        "month": month,
        "term":term,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return StudentResultMonthSemesterDetailPrimaryModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("$e");
      });
    }
  }
}