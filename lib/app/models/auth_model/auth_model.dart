class AuthModel {
    AuthModel({
         this.accessToken,
         this.tokenType,
         this.expiresIn,
         this.expiresInDate,
         this.authUser,
         this.schoolCode,
         this.schoolGuid,
         this.schoolName,
         this.isPrimary,
         this.accessUrl,
    });

    String? accessToken;
    String? tokenType;
    int? expiresIn;
    String ?expiresInDate;
    AuthUser? authUser;
    int? schoolCode;
    String? schoolGuid;
    String? schoolName;
    bool? isPrimary;
    String? accessUrl;

    factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        accessToken: json["access_token"],
        tokenType: json["token_type"],
        expiresIn: json["expires_in"],
        expiresInDate: json["expires_in_date"],
        authUser: AuthUser.fromJson(json["auth_user"]),
        schoolCode: json["school_code"],
        schoolGuid: json["school_guid"],
        schoolName: json["school_name"],
        isPrimary: json["is_primary"],
        accessUrl: json["access_url"],
    );

    Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "token_type": tokenType,
        "expires_in": expiresIn,
        "expires_in_date": expiresInDate,
        "auth_user": authUser?.toJson(),
        "school_code": schoolCode,
        "school_guid": schoolGuid,
        "school_name": schoolName,
        "is_primary": isPrimary,
        "access_url": accessUrl,
    };
}

class AuthUser {
    AuthUser({
        required this.id,
        this.schoolCode,
        required this.code,
        required this.name,
        required this.nameEn,
        required this.firstname,
        required this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        required this.gender,
        required this.genderName,
        required this.dob,
        required this.phone,
        required this.isVerify,
        required this.email,
        this.address,
        this.framework,
        this.prokas,
        required this.role,
        required this.profileMedia,
    });

    String id;
    dynamic schoolCode;
    String code;
    String name;
    String nameEn;
    String firstname;
    String lastname;
    dynamic firstnameEn;
    dynamic lastnameEn;
    int gender;
    String genderName;
    String dob;
    String phone;
    int isVerify;
    String email;
    dynamic address;
    dynamic framework;
    dynamic prokas;
    int role;
    ProfileMedia profileMedia;

    factory AuthUser.fromJson(Map<String, dynamic> json) => AuthUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        isVerify: json["is_verify"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"],
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "is_verify": isVerify,
        "email": email,
        "address": address,
        "framework": framework,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia.toJson(),
    };
}

class ProfileMedia {
    ProfileMedia({
        required this.id,
        required this.fileName,
        required this.fileSize,
        required this.fileType,
        required this.fileIndex,
        required this.fileArea,
        required this.objectId,
        required this.schoolUrl,
        required this.postDate,
        required this.isGdrive,
        required this.fileGdriveId,
        required this.isS3,
        required this.fileShow,
        required this.fileThumbnail,
    });

    int id;
    String fileName;
    String fileSize;
    String fileType;
    String fileIndex;
    String fileArea;
    String objectId;
    String schoolUrl;
    String postDate;
    int isGdrive;
    String fileGdriveId;
    int isS3;
    String fileShow;
    String fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
