// To parse this JSON data, do
//
//     final allPermissionRequestModel = allPermissionRequestModelFromJson(jsonString);

import 'dart:convert';

AllPermissionRequestModel allPermissionRequestModelFromJson(String str) => AllPermissionRequestModel.fromJson(json.decode(str));

String allPermissionRequestModelToJson(AllPermissionRequestModel data) => json.encode(data.toJson());

class AllPermissionRequestModel {
    bool? status;
    String? asd;
    String? message;
    List<DataAllPermission>? data;

    AllPermissionRequestModel({
        this.status,
        this.asd,
        this.message,
        this.data,
    });

    factory AllPermissionRequestModel.fromJson(Map<String, dynamic> json) => AllPermissionRequestModel(
        status: json["status"],
        asd: json["asd"],
        message: json["message"],
        data: List<DataAllPermission>.from(json["data"].map((x) => DataAllPermission.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "asd": asd,
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataAllPermission {
    dynamic id;
    dynamic startDate;
    dynamic endDate;
    dynamic requestTypes;
    dynamic admissionTypes;
    dynamic admissionTypeName;
    dynamic admissionTypeNameEn;
    dynamic userType;
    dynamic userId;
    List<Schedule>? schedules;
    List<FileElement>? file;
    bool? status;

    DataAllPermission({
        this.id,
        this.startDate,
        this.endDate,
        this.requestTypes,
        this.admissionTypes,
        this.admissionTypeName,
        this.admissionTypeNameEn,
        this.userType,
        this.userId,
        this.schedules,
        this.file,
        this.status,
    });

    factory DataAllPermission.fromJson(Map<String, dynamic> json) => DataAllPermission(
        id: json["id"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        requestTypes: json["request_types"],
        admissionTypes: json["admission_types"],
        admissionTypeName: json["admission_type_name"],
        admissionTypeNameEn: json["admission_type_name_en"],
        userType: json["user_type"],
        userId: json["user_id"],
        schedules: List<Schedule>.from(json["schedules"].map((x) => Schedule.fromJson(x))),
        file: List<FileElement>.from(json["file"].map((x) => FileElement.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "start_date": startDate,
        "end_date": endDate,
        "request_types": requestTypes,
        "admission_types": admissionTypes,
        "admission_type_name": admissionTypeName,
        "admission_type_name_en": admissionTypeNameEn,
        "user_type": userType,
        "user_id": userId,
        "schedules": List<dynamic>.from(schedules!.map((x) => x.toJson())),
        "file": List<dynamic>.from(file!.map((x) => x.toJson())),
         "status": status,
    };
}

class FileElement {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    FileElement({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class Schedule {
    dynamic scheduleId;
    dynamic startTime;
    dynamic endTime;
    dynamic subjectId;
    dynamic subjectName;
    dynamic subjectNameEn;

    Schedule({
        this.scheduleId,
        this.startTime,
        this.endTime,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
    });

    factory Schedule.fromJson(Map<String, dynamic> json) => Schedule(
        scheduleId: json["schedule_id"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
    );

    Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "start_time": startTime,
        "end_time": endTime,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
    };
}
