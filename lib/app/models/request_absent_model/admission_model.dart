// To parse this JSON data, do
//
//     final admissionModel = admissionModelFromJson(jsonString);

import 'dart:convert';

AdmissionModel admissionModelFromJson(String str) => AdmissionModel.fromJson(json.decode(str));

String admissionModelToJson(AdmissionModel data) => json.encode(data.toJson());

class AdmissionModel {
    List<Datum>? data;
    String? message;

    AdmissionModel({
        this.data,
        this.message,
    });

    factory AdmissionModel.fromJson(Map<String, dynamic> json) => AdmissionModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
    };
}

class Datum {
    int? id;
    int? parentId;
    String? name;
    String? nameEn;
    dynamic sortkey;
    dynamic objectType;
    dynamic description;
   // List<dynamic>? children;

    Datum({
        this.id,
        this.parentId,
        this.name,
        this.nameEn,
        this.sortkey,
        this.objectType,
        this.description,
        //this.children,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        parentId: json["parent_id"],
        name: json["name"],
        nameEn: json["name_en"],
        sortkey: json["sortkey"],
        objectType: json["object_type"],
        description: json["description"],
        //children: List<dynamic>.from(json["children"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent_id": parentId,
        "name": name,
        "name_en": nameEn,
        "sortkey": sortkey,
        "object_type": objectType,
        "description": description,
        //"children": List<dynamic>.from(children.map((x) => x)),
    };
}
