// To parse this JSON data, do
//
//     final ownPrensentInMonthModel = ownPrensentInMonthModelFromJson(jsonString);

import 'dart:convert';

OwnPrensentInMonthModel ownPrensentInMonthModelFromJson(String str) => OwnPrensentInMonthModel.fromJson(json.decode(str));

String ownPrensentInMonthModelToJson(OwnPrensentInMonthModel data) => json.encode(data.toJson());

class OwnPrensentInMonthModel {
    bool? status;
    String? message;
    Data? data;

    OwnPrensentInMonthModel({
        this.status,
        this.message,
        this.data,
    });

    factory OwnPrensentInMonthModel.fromJson(Map<String, dynamic> json) => OwnPrensentInMonthModel(
        status: json["status"],
        message: json["message"],
        data: json["data"] == null?null:Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data==null?null:data!.toJson(),
    };
}

class Data {
    String? startDayOfMonth;
    String? endDayOfMonth;
    List<StaffPresentDatum>? staffPresentData;

    Data({
        this.startDayOfMonth,
        this.endDayOfMonth,
        this.staffPresentData,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        startDayOfMonth: json["start_day_of_month"],
        endDayOfMonth: json["end_day_of_month"],
        staffPresentData: List<StaffPresentDatum>.from(json["staff_present_data"].map((x) => StaffPresentDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "start_day_of_month": startDayOfMonth,
        "end_day_of_month": endDayOfMonth,
        "staff_present_data": List<dynamic>.from(staffPresentData!.map((x) => x.toJson())),
    };
}

class StaffPresentDatum {
    String? date;
    String? message;
    List<PresentDatum>? presentData;

    StaffPresentDatum({
        this.date,
        this.message,
        this.presentData,
    });

    factory StaffPresentDatum.fromJson(Map<String, dynamic> json) => StaffPresentDatum(
        date: json["date"],
        message: json["message"],
        presentData: List<PresentDatum>.from(json["present_data"].map((x) => PresentDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "date": date,
        "message": message,
        "present_data": List<dynamic>.from(presentData!.map((x) => x.toJson())),
    };
}

class PresentDatum {
    int? scheduleId;
    int? scheduleType;
    String? startTime;
    String? endTime;
    String? duration;
    bool? isCurrent;
    String? beforeStartTime;
    String? afterEndTime;
    bool? checkEventTime;
    String? date;
    String? shortDay;
    String? event;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectColor;
    int? classId;
    String? className;
    String? classNameEn;
    String? classObjectType;
    String? schoolyearId;
    int? schoolType;
    String? teacherName;
    String? teacherNameEn;
    String? teacherId;
    int? isInstructor;
    int? timeTableSettingId;
    int? staffPresentStatus;
    String? staffPresentStatusName;
    //StaffPresentData? staffPresentData;

    PresentDatum({
        this.scheduleId,
        this.scheduleType,
        this.startTime,
        this.endTime,
        this.duration,
        this.isCurrent,
        this.beforeStartTime,
        this.afterEndTime,
        this.checkEventTime,
        this.date,
        this.shortDay,
        this.event,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectColor,
        this.classId,
        this.className,
        this.classNameEn,
        this.classObjectType,
        this.schoolyearId,
        this.schoolType,
        this.teacherName,
        this.teacherNameEn,
        this.teacherId,
        this.isInstructor,
        this.timeTableSettingId,
        this.staffPresentStatus,
        this.staffPresentStatusName,
        //this.staffPresentData,
    });

    factory PresentDatum.fromJson(Map<String, dynamic> json) => PresentDatum(
        scheduleId: json["schedule_id"],
        scheduleType: json["schedule_type"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        duration: json["duration"],
        isCurrent: json["is_current"],
        beforeStartTime: json["before_start_time"],
        afterEndTime: json["after_end_time"],
        checkEventTime: json["check_event_time"],
        date: json["date"],
        shortDay: json["short_day"],
        event: json["event"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectColor: json["subject_color"],
        classId: json["class_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        classObjectType: json["class_object_type"],
        schoolyearId: json["schoolyear_id"],
        schoolType: json["school_type"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherId: json["teacher_id"],
        isInstructor: json["is_instructor"],
        timeTableSettingId: json["time_table_setting_id"],
        staffPresentStatus: json["staff_present_status"],
        staffPresentStatusName: json["staff_present_status_name"],
       // staffPresentData: json["staff_present_data"]==null?null: StaffPresentData.fromJson(json["staff_present_data"]),
    );

    Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "schedule_type": scheduleType,
        "start_time": startTime,
        "end_time": endTime,
        "duration": duration,
        "is_current": isCurrent,
        "before_start_time": beforeStartTime,
        "after_end_time": afterEndTime,
        "check_event_time": checkEventTime,
        "date": date,
        "short_day": shortDay,
        "event": event,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_color": subjectColor,
        "class_id": classId,
        "class_name": className,
        "class_name_en": classNameEn,
        "class_object_type": classObjectType,
        "schoolyear_id": schoolyearId,
        "school_type": schoolType,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_id": teacherId,
        "is_instructor": isInstructor,
        "time_table_setting_id": timeTableSettingId,
        "staff_present_status": staffPresentStatus,
        "staff_present_status_name": staffPresentStatusName,
       // "staff_present_data": staffPresentData == null?null: staffPresentData!.toJson(),
    };
}

class StaffPresentData {
    int? id;
    String? staffId;
    int? classId;
    int? scheduleId;
    int? timeshift;
    int? late;
    int? leave;
    int? isAdminStaff;
    String? latitude;
    String? longitude;
    String? createdDatetime;
    int? type;
    int? disactive;
    int? duration;
    int? subjectId;
    int? timeTableSettingId;
    int? isPermission;
    dynamic staffAttendancePermissionId;

    StaffPresentData({
        this.id,
        this.staffId,
        this.classId,
        this.scheduleId,
        this.timeshift,
        this.late,
        this.leave,
        this.isAdminStaff,
        this.latitude,
        this.longitude,
        this.createdDatetime,
        this.type,
        this.disactive,
        this.duration,
        this.subjectId,
        this.timeTableSettingId,
        this.isPermission,
        this.staffAttendancePermissionId,
    });

    factory StaffPresentData.fromJson(Map<String, dynamic> json) => StaffPresentData(
        id: json["ID"],
        staffId: json["STAFF_ID"],
        classId: json["CLASS_ID"],
        scheduleId: json["SCHEDULE_ID"],
        timeshift: json["TIMESHIFT"],
        late: json["LATE"],
        leave: json["LEAVE"],
        isAdminStaff: json["IS_ADMIN_STAFF"],
        latitude: json["LATITUDE"],
        longitude: json["LONGITUDE"],
        createdDatetime: json["CREATED_DATETIME"],
        type: json["TYPE"],
        disactive: json["DISACTIVE"],
        duration: json["DURATION"],
        subjectId: json["SUBJECT_ID"],
        timeTableSettingId: json["TIME_TABLE_SETTING_ID"],
        isPermission: json["IS_PERMISSION"],
        staffAttendancePermissionId: json["STAFF_ATTENDANCE_PERMISSION_ID"],
    );

    Map<String, dynamic> toJson() => {
        "ID": id,
        "STAFF_ID": staffId,
        "CLASS_ID": classId,
        "SCHEDULE_ID": scheduleId,
        "TIMESHIFT": timeshift,
        "LATE": late,
        "LEAVE": leave,
        "IS_ADMIN_STAFF": isAdminStaff,
        "LATITUDE": latitude,
        "LONGITUDE": longitude,
        "CREATED_DATETIME": createdDatetime,
        "TYPE": type,
        "DISACTIVE": disactive,
        "DURATION": duration,
        "SUBJECT_ID": subjectId,
        "TIME_TABLE_SETTING_ID": timeTableSettingId,
        "IS_PERMISSION": isPermission,
        "STAFF_ATTENDANCE_PERMISSION_ID": staffAttendancePermissionId,
    };
}
