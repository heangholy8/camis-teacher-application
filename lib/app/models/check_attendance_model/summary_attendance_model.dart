// To parse this JSON data, do
//
//     final summaryAttendanceModel = summaryAttendanceModelFromJson(jsonString);

import 'dart:convert';

SummaryAttendanceModel summaryAttendanceModelFromJson(String str) => SummaryAttendanceModel.fromJson(json.decode(str));

String summaryAttendanceModelToJson(SummaryAttendanceModel data) => json.encode(data.toJson());

class SummaryAttendanceModel {
    bool? status;
    DataSummaryAtt? data;
    String? message;

    SummaryAttendanceModel({
        this.status,
        this.data,
        this.message,
    });

    factory SummaryAttendanceModel.fromJson(Map<String, dynamic> json) => SummaryAttendanceModel(
        status: json["status"],
        data: DataSummaryAtt.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data!.toJson(),
        "message": message,
    };
}

class DataSummaryAtt {
    String? classId;
    String? className;
    String? date;
    Attendance? attendance;
    List<ListAttendance>? listAttendance;

    DataSummaryAtt({
        this.classId,
        this.className,
        this.date,
        this.attendance,
        this.listAttendance,
    });

    factory DataSummaryAtt.fromJson(Map<String, dynamic> json) => DataSummaryAtt(
        classId: json["class_id"],
        className: json["class_name"],
        date: json["date"],
        attendance: Attendance.fromJson(json["attendance"]),
        listAttendance: List<ListAttendance>.from(json["list_attendance"].map((x) => ListAttendance.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "class_id": classId,
        "class_name": className,
        "date": date,
        "attendance": attendance!.toJson(),
        "list_attendance": List<dynamic>.from(listAttendance!.map((x) => x.toJson())),
    };
}

class Attendance {
    int? totalStudent;
    int? present;
    int? absent;
    int? late;
    int? permission;

    Attendance({
        this.totalStudent,
        this.present,
        this.absent,
        this.late,
        this.permission,
    });

    factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        totalStudent: json["total_student"],
        present: json["present"],
        absent: json["absent"],
        late: json["late"],
        permission: json["permission"],
    );

    Map<String, dynamic> toJson() => {
        "total_student": totalStudent,
        "present": present,
        "absent": absent,
        "late": late,
        "permission": permission,
    };
}

class ListAttendance {
    String? studentId;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    dynamic gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    int? attendanceStatus;
    ProfileMedia? profileMedia;

    ListAttendance({
        this.studentId,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.attendanceStatus,
        this.profileMedia,
    });

    factory ListAttendance.fromJson(Map<String, dynamic> json) => ListAttendance(
        studentId: json["student_id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        attendanceStatus: json["attendance_status"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "attendance_status": attendanceStatus,
        "profileMedia": profileMedia!.toJson(),
    };
}

class ProfileMedia {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;
    String? fileShow;
    String? fileThumbnail;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
