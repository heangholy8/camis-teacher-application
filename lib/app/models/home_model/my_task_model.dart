// To parse this JSON data, do
//
//     final myTaskDashboardModel = myTaskDashboardModelFromJson(jsonString);

import 'dart:convert';

MyTaskDashboardModel myTaskDashboardModelFromJson(String str) => MyTaskDashboardModel.fromJson(json.decode(str));

String myTaskDashboardModelToJson(MyTaskDashboardModel data) => json.encode(data.toJson());

class MyTaskDashboardModel {
    bool? status;
    List<Datum>? data;
    String? message;

    MyTaskDashboardModel({
        this.status,
        this.data,
        this.message,
    });

    factory MyTaskDashboardModel.fromJson(Map<String, dynamic> json) => MyTaskDashboardModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
    };
}

class Datum {
    int? classId;
    String? objectType;
    String? name;
    String? nameEn;
    String? schoolyear;
    int? schoolType;
    int? isInstructor;
    int? scheduleId;
    int? scheduleType;
    String? startTime;
    String? endTime;
    String? duration;
    bool? isCurrent;
    String? date;
    String? shortDay;
    String? event;
    dynamic subjectGroupTagId;
    String? subjectGroupTagName;
    String? subjectGroupTagNameEn;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectColor;
    bool? isSubSubject;
    Attendance? attendance;
    int? id;
    String? examDate;
    int? type;
    int? month;
    String? monthName;
    String? monthNameEn;
    String? semester;
    String? description;
    int? isApprove;
    String? approvedBy;
    String? approvedAt;
    int? isCheckScoreEnter;
    String? scoreEnterStatusName;
    int? totalStudentsScoreEntered;
    double? totalStudentsScoreEnteredPercentage;
    String? scoreEnterExpireDate;
    int? taskStatus;
    String? taskStatusName;
    int? countdownDays;
    int? timeTableSettingId;
    int? staffPresentStatus;
    String? staffPresentStatusName;
    bool? receiveRequestPermission;
    int? countRequestPermissions;
    List<dynamic>? queryRequestPermissions;

    Datum({
        this.classId,
        this.objectType,
        this.name,
        this.nameEn,
        this.schoolyear,
        this.schoolType,
        this.isInstructor,
        this.scheduleId,
        this.scheduleType,
        this.startTime,
        this.endTime,
        this.duration,
        this.isCurrent,
        this.date,
        this.shortDay,
        this.event,
        this.subjectGroupTagId,
        this.subjectGroupTagName,
        this.subjectGroupTagNameEn,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectColor,
        this.isSubSubject,
        this.attendance,
        this.id,
        this.examDate,
        this.type,
        this.month,
        this.monthName,
        this.monthNameEn,
        this.semester,
        this.description,
        this.isApprove,
        this.approvedBy,
        this.approvedAt,
        this.isCheckScoreEnter,
        this.scoreEnterStatusName,
        this.totalStudentsScoreEntered,
        this.totalStudentsScoreEnteredPercentage,
        this.scoreEnterExpireDate,
        this.taskStatus,
        this.taskStatusName,
        this.countdownDays,
        this.timeTableSettingId,
        this.staffPresentStatus,
        this.staffPresentStatusName,
        this.receiveRequestPermission,
        this.countRequestPermissions,
        this.queryRequestPermissions,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        classId: json["class_id"],
        objectType: json["object_type"],
        name: json["name"],
        nameEn: json["name_en"],
        schoolyear: json["schoolyear"],
        schoolType: json["school_type"],
        isInstructor: json["is_instructor"],
        scheduleId: json["schedule_id"],
        scheduleType: json["schedule_type"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        duration: json["duration"],
        isCurrent: json["is_current"],
        date: json["date"],
        shortDay: json["short_day"],
        event: json["event"],
        subjectGroupTagId: json["subject_group_tag_id"],
        subjectGroupTagName: json["subject_group_tag_name"],
        subjectGroupTagNameEn: json["subject_group_tag_name_en"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectColor: json["subject_color"],
        isSubSubject: json["is_sub_subject"],
        attendance: Attendance.fromJson(json["attendance"]),
        id: json["id"],
        examDate: json["exam_date"],
        type: json["type"],
        month: json["month"],
        monthName: json["month_name"],
        monthNameEn: json["month_name_en"],
        semester: json["semester"],
        description: json["description"],
        isApprove: json["is_approve"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
        isCheckScoreEnter: json["is_check_score_enter"],
        scoreEnterStatusName: json["score_enter_status_name"],
        totalStudentsScoreEntered: json["total_students_score_entered"],
        totalStudentsScoreEnteredPercentage: json["total_students_score_entered_percentage"].toDouble(),
        scoreEnterExpireDate: json["score_enter_expire_date"],
        taskStatus: json["task_status"],
        taskStatusName: json["task_status_name"],
        countdownDays: json["countdown_days"],
        timeTableSettingId: json["time_table_setting_id"],
        staffPresentStatus: json["staff_present_status"],
        staffPresentStatusName: json["staff_present_status_name"],
        receiveRequestPermission: json["receive_request_permission"],
        countRequestPermissions: json["count_request_permissions"],
        queryRequestPermissions: List<dynamic>.from(json["query_request_permissions"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "class_id": classId,
        "object_type": objectType,
        "name": name,
        "name_en": nameEn,
        "schoolyear": schoolyear,
        "school_type": schoolType,
        "is_instructor": isInstructor,
        "schedule_id": scheduleId,
        "schedule_type": scheduleType,
        "start_time": startTime,
        "end_time": endTime,
        "duration": duration,
        "is_current": isCurrent,
        "date": date,
        "short_day": shortDay,
        "event": event,
        "subject_group_tag_id": subjectGroupTagId,
        "subject_group_tag_name": subjectGroupTagName,
        "subject_group_tag_name_en": subjectGroupTagNameEn,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_color": subjectColor,
        "is_sub_subject": isSubSubject,
        "attendance": attendance?.toJson(),
        "id": id,
        "exam_date": examDate,
        "type": type,
        "month": month,
        "month_name": monthName,
        "month_name_en": monthNameEn,
        "semester": semester,
        "description": description,
        "is_approve": isApprove,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
        "is_check_score_enter": isCheckScoreEnter,
        "score_enter_status_name": scoreEnterStatusName,
        "total_students_score_entered": totalStudentsScoreEntered,
        "total_students_score_entered_percentage": totalStudentsScoreEnteredPercentage,
        "score_enter_expire_date": scoreEnterExpireDate,
        "task_status": taskStatus,
        "task_status_name": taskStatusName,
        "countdown_days": countdownDays,
        "time_table_setting_id": timeTableSettingId,
        "staff_present_status": staffPresentStatus,
        "staff_present_status_name": staffPresentStatusName,
        "receive_request_permission": receiveRequestPermission,
        "count_request_permissions": countRequestPermissions,
        "query_request_permissions": List<dynamic>.from(queryRequestPermissions!.map((x) => x)),
    };
}

class Attendance {
    int? isCheckAttendance;
    String? attendanceTaker;
    int? totalStudent;
    int? present;
    int? absent;
    int? late;
    int? permission;

    Attendance({
        this.isCheckAttendance,
        this.attendanceTaker,
        this.totalStudent,
        this.present,
        this.absent,
        this.late,
        this.permission,
    });

    factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        isCheckAttendance: json["is_check_attendance"],
        attendanceTaker: json["attendance_taker"],
        totalStudent: json["total_student"],
        present: json["present"],
        absent: json["absent"],
        late: json["late"],
        permission: json["permission"],
    );

    Map<String, dynamic> toJson() => {
        "is_check_attendance": isCheckAttendance,
        "attendance_taker": attendanceTaker,
        "total_student": totalStudent,
        "present": present,
        "absent": absent,
        "late": late,
        "permission": permission,
    };
}
