class GradingSubjectModel {
  bool? status;
  GradingData? data;

  GradingSubjectModel({this.status, this.data});

  GradingSubjectModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new GradingData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class GradingData {
  int? classId;
  dynamic className;
  dynamic classNameEn;
  int? subjectId;
  dynamic subjectName;
  dynamic subjectNameEn;
  dynamic subjectColor;
  int? scheduleId;
  dynamic startTime;
  dynamic endTime;
  Attendance? attendance;
  int? id;
  dynamic examDate;
  int? type;
  dynamic month;
  dynamic semester;
  dynamic description;
  int? isApprove;
  dynamic approvedBy;
  dynamic approvedAt;
  int? totalStudentsInClass;
  List<StudentsData>? studentsData;
  int? totalStudentsScoreEntered;
  dynamic isNoExam;
  int? isAdminApprove;

  GradingData(
      {this.classId,
      this.className,
      this.classNameEn,
      this.subjectId,
      this.subjectName,
      this.subjectNameEn,
      this.subjectColor,
      this.scheduleId,
      this.startTime,
      this.endTime,
      this.attendance,
      this.id,
      this.examDate,
      this.type,
      this.month,
      this.semester,
      this.description,
      this.isApprove,
      this.approvedBy,
      this.approvedAt,
      this.totalStudentsInClass,
      this.studentsData,
      this.totalStudentsScoreEntered,
      this.isNoExam,
      this.isAdminApprove,
      });

  GradingData.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    className = json['class_name'];
    classNameEn = json['class_name_en'];
    subjectId = json['subject_id'];
    subjectName = json['subject_name'];
    subjectNameEn = json['subject_name_en'];
    subjectColor = json['subject_color'];
    scheduleId = json['schedule_id'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    attendance = json['attendance'] != null
        ? new Attendance.fromJson(json['attendance'])
        : null;
    id = json['id'];
    examDate = json['exam_date'];
    type = json['type'];
    month = json['month'];
    semester = json['semester'];
    description = json['description'];
    isApprove = json['is_approve'];
    approvedBy = json['approved_by'];
    approvedAt = json['approved_at'];
    isNoExam = json['is_no_exam'];
    totalStudentsInClass = json['total_students_in_class'];
    if (json['students_data'] != null) {
      studentsData = <StudentsData>[];
      json['students_data'].forEach((v) {
        studentsData!.add(new StudentsData.fromJson(v));
      });
    }
    totalStudentsScoreEntered = json['total_students_score_entered'];
    isAdminApprove = json['is_admin_approve'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['class_id'] = this.classId;
    data['class_name'] = this.className;
    data['class_name_en'] = this.classNameEn;
    data['subject_id'] = this.subjectId;
    data['subject_name'] = this.subjectName;
    data['subject_name_en'] = this.subjectNameEn;
    data['subject_color'] = this.subjectColor;
    data['schedule_id'] = this.scheduleId;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    if (this.attendance != null) {
      data['attendance'] = this.attendance!.toJson();
    }
    data['id'] = this.id;
    data['exam_date'] = this.examDate;
    data['type'] = this.type;
    data['month'] = this.month;
    data['semester'] = this.semester;
    data['description'] = this.description;
    data['is_approve'] = this.isApprove;
    data['approved_by'] = this.approvedBy;
    data['approved_at'] = this.approvedAt;
    data['total_students_in_class'] = this.totalStudentsInClass;
    data['is_no_exam'] = this.isNoExam;
    if (this.studentsData != null) {
      data['students_data'] =
          this.studentsData!.map((v) => v.toJson()).toList();
    }
    data['total_students_score_entered'] = this.totalStudentsScoreEntered;
    data['is_admin_approve'] = this.isAdminApprove;
    return data;
  }
}

class Attendance {
  int? totalStudent;
  int? present;
  int? absent;
  int? late;
  int? permission;

  Attendance(
      {this.totalStudent,
      this.present,
      this.absent,
      this.late,
      this.permission});

  Attendance.fromJson(Map<String, dynamic> json) {
    totalStudent = json['total_student'];
    present = json['present'];
    absent = json['absent'];
    late = json['late'];
    permission = json['permission'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_student'] = this.totalStudent;
    data['present'] = this.present;
    data['absent'] = this.absent;
    data['late'] = this.late;
    data['permission'] = this.permission;
    return data;
  }
}

 class ChildrenSubject{
  
  dynamic id;
  dynamic name;
  dynamic nameEn;
  dynamic short;
  dynamic maxScore;
  dynamic avg;
  dynamic  absentExam;
  dynamic rank;
  dynamic score;
  dynamic grading;
  dynamic gradingEn;
  dynamic letterGrade;
  dynamic teacherComment;
  dynamic isFail;
 

  ChildrenSubject(
      {
        this.id,
        this.name,
        this.nameEn,
        this.short,
        this.maxScore,
        this.avg,
        this.absentExam,
        this.rank,
        this.score,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.isFail,
        this.teacherComment,

      });

  ChildrenSubject.fromJson(Map<String, dynamic> json) {
    id=json['id'];
    name=json['name'];
    nameEn=json['name_en'];
    short=json['short'];
    maxScore=json['max_score'];
    avg=json['avg'];
    score = json['score'];
    absentExam = json['absent_exam'];
    teacherComment = json['teacher_comment'];
    rank = json['rank'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    isFail = json['is_fail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name']=this.name;
    data['name_en']=this.nameEn;
    data['short']=this.short;
    data['max_score'] = this.maxScore;
    data['avg']=this.avg;
    data['score'] = this.score;
    data['absent_exam'] = this.absentExam;
    data['teacher_comment'] = this.teacherComment;
    data['rank'] = this.rank;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['is_fail'] = this.isFail;
    return data;
  }
}


class StudentsData {
  dynamic studentId;
  dynamic studentName;
  dynamic studentNameEn;
  dynamic gender;
  dynamic dateOfBirth;
  dynamic phone;
  ProfileMedia? profileMedia;
  dynamic subjectShort;
  dynamic subjectScoreMin;
  dynamic subjectScoreMax;
  dynamic score;
  dynamic absentExam;
  dynamic teacherComment;
  dynamic complementaryScore;
  dynamic includeInEvaluation;
  dynamic rank;
  dynamic month;
  dynamic term;
  dynamic grading;
  dynamic gradingEn;
  dynamic letterGrade;
  dynamic gradePoints;
  dynamic isFail;
  List<ChildrenSubject>? childrenSubject;
   bool showChild  = false;

  StudentsData(
      {this.studentId,
      this.studentName,
      this.studentNameEn,
      this.gender,
      this.dateOfBirth,
      this.phone,
      this.profileMedia,
      this.subjectShort,
      this.subjectScoreMin,
      this.subjectScoreMax,
      this.score,
      this.absentExam,
      this.teacherComment,
      this.complementaryScore,
      this.includeInEvaluation,
      this.rank,
      this.month,
      this.term,
      this.grading,
      this.gradingEn,
      this.letterGrade,
      this.gradePoints,
      this.isFail,
      this.childrenSubject,
      this.showChild = false,
      });

  StudentsData.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    studentName = json['student_name'];
    studentNameEn = json['student_name_en'];
    gender = json['gender'];
    dateOfBirth = json['date_of_birth'];
    phone = json['phone'];
    profileMedia = json['profileMedia'] != null
        ? new ProfileMedia.fromJson(json['profileMedia'])
        : null;
    subjectShort = json['subject_short'];
    subjectScoreMin = json['subject_score_min'];
    subjectScoreMax = json['subject_score_max'];
    score = json['score'];
    absentExam = json['absent_exam'];
    teacherComment = json['teacher_comment'];
    complementaryScore = json['complementary_score'];
    includeInEvaluation = json['include_in_evaluation'];
    rank = json['rank'];
    month = json['month'];
    term = json['term'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    gradePoints = json['grade_points'];
    isFail = json['is_fail'];
    if (json['children'] != null) {
      childrenSubject = <ChildrenSubject>[];
      json['children'].forEach((v) {
        childrenSubject!.add(new ChildrenSubject.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['student_name'] = this.studentName;
    data['student_name_en'] = this.studentNameEn;
    data['gender'] = this.gender;
    data['date_of_birth'] = this.dateOfBirth;
    data['phone'] = this.phone;
    if (this.profileMedia != null) {
      data['profileMedia'] = this.profileMedia!.toJson();
    }
    data['subject_short'] = this.subjectShort;
    data['subject_score_min'] = this.subjectScoreMin;
    data['subject_score_max'] = this.subjectScoreMax;
    data['score'] = this.score;
    data['absent_exam'] = this.absentExam;
    data['teacher_comment'] = this.teacherComment;
    data['complementary_score'] = this.complementaryScore;
    data['include_in_evaluation'] = this.includeInEvaluation;
    data['rank'] = this.rank;
    data['month'] = this.month;
    data['term'] = this.term;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['grade_points'] = this.gradePoints;
    data['is_fail'] = this.isFail;
    if (this.childrenSubject != null) {
      data['children'] =
          this.childrenSubject!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProfileMedia {
  int? id;
  dynamic fileName;
  dynamic fileSize;
  dynamic fileType;
  dynamic fileShow;
  dynamic fileThumbnail;
  dynamic fileIndex;
  dynamic fileArea;
  dynamic objectId;
  dynamic schoolUrl;
  dynamic postDate;
  int? isGdrive;
  dynamic fileGdriveId;
  int? isS3;

  ProfileMedia(
      {this.id,
      this.fileName,
      this.fileSize,
      this.fileType,
      this.fileShow,
      this.fileThumbnail,
      this.fileIndex,
      this.fileArea,
      this.objectId,
      this.schoolUrl,
      this.postDate,
      this.isGdrive,
      this.fileGdriveId,
      this.isS3});

  ProfileMedia.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fileName = json['file_name'];
    fileSize = json['file_size'];
    fileType = json['file_type'];
    fileShow = json['file_show'];
    fileThumbnail = json['file_thumbnail'];
    fileIndex = json['file_index'];
    fileArea = json['file_area'];
    objectId = json['object_id'];
    schoolUrl = json['school_url'];
    postDate = json['post_date'];
    isGdrive = json['is_gdrive'];
    fileGdriveId = json['file_gdrive_id'];
    isS3 = json['is_s3'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['file_name'] = this.fileName;
    data['file_size'] = this.fileSize;
    data['file_type'] = this.fileType;
    data['file_show'] = this.fileShow;
    data['file_thumbnail'] = this.fileThumbnail;
    data['file_index'] = this.fileIndex;
    data['file_area'] = this.fileArea;
    data['object_id'] = this.objectId;
    data['school_url'] = this.schoolUrl;
    data['post_date'] = this.postDate;
    data['is_gdrive'] = this.isGdrive;
    data['file_gdrive_id'] = this.fileGdriveId;
    data['is_s3'] = this.isS3;
    return data;
  }
}
