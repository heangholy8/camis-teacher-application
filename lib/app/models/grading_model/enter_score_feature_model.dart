// To parse this JSON data, do
//
//     final getCheckCoditionSwitchEnterScoreScreenModel = getCheckCoditionSwitchEnterScoreScreenModelFromJson(jsonString);

import 'dart:convert';

GetCheckCoditionSwitchEnterScoreScreenModel getCheckCoditionSwitchEnterScoreScreenModelFromJson(String str) => GetCheckCoditionSwitchEnterScoreScreenModel.fromJson(json.decode(str));

String getCheckCoditionSwitchEnterScoreScreenModelToJson(GetCheckCoditionSwitchEnterScoreScreenModel data) => json.encode(data.toJson());

class GetCheckCoditionSwitchEnterScoreScreenModel {
    Data? data;
    bool? success;

    GetCheckCoditionSwitchEnterScoreScreenModel({
        this.data,
        this.success,
    });

    factory GetCheckCoditionSwitchEnterScoreScreenModel.fromJson(Map<String, dynamic> json) => GetCheckCoditionSwitchEnterScoreScreenModel(
        data: Data.fromJson(json["data"]),
        success: json["success"],
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
        "success": success,
    };
}

class Data {
    int? appEnterScoreSwitchType;

    Data({
        this.appEnterScoreSwitchType,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        appEnterScoreSwitchType: json["APP_ENTER_SCORE_SWITCH_TYPE"],
    );

    Map<String, dynamic> toJson() => {
        "APP_ENTER_SCORE_SWITCH_TYPE": appEnterScoreSwitchType,
    };
}
