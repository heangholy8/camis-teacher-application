// To parse this JSON data, do
//
//     final graedingResponeModel = graedingResponeModelFromJson(jsonString);

import 'dart:convert';

GraedingResponeModel graedingResponeModelFromJson(String str) => GraedingResponeModel.fromJson(json.decode(str));

String graedingResponeModelToJson(GraedingResponeModel data) => json.encode(data.toJson());

class GraedingResponeModel {
    GraedingResponeModel({
        this.status,
        this.data,
    });

    bool? status;
    List<Datum>? data;

    factory GraedingResponeModel.fromJson(Map<String, dynamic> json) => GraedingResponeModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.studentId,
        this.studentName,
        this.studentNameEn,
        this.gender,
        this.dateOfBirth,
        this.phone,
        this.profileMedia,
        this.subjectShort,
        this.subjectScoreMin,
        this.subjectScoreMax,
        this.score,
        this.absentExam,
        this.teacherComment,
        this.complementaryScore,
        this.includeInEvaluation,
        this.rank,
        this.month,
        this.term,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
    });

    String? studentId;
    String? studentName;
    String? studentNameEn;
    dynamic gender;
    String? dateOfBirth;
    String? phone;
    ProfileMedia? profileMedia;
    String? subjectShort;
    dynamic subjectScoreMin;
    dynamic subjectScoreMax;
    dynamic score;
    int? absentExam;
    String? teacherComment;
    dynamic complementaryScore;
    int ?includeInEvaluation;
    int ?rank;
    int ?month;
    String? term;
    String ?grading;
    String ?gradingEn;
    String ?letterGrade;
    int? gradePoints;
    int? isFail;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        studentId: json["student_id"],
        studentName: json["student_name"],
        studentNameEn: json["student_name_en"],
        gender: json["gender"],
        dateOfBirth: json["date_of_birth"],
        phone: json["phone"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        subjectShort: json["subject_short"],
        subjectScoreMin: json["subject_score_min"],
        subjectScoreMax: json["subject_score_max"],
        score: json["score"],
        absentExam: json["absent_exam"],
        teacherComment: json["teacher_comment"],
        complementaryScore: json["complementary_score"],
        includeInEvaluation: json["include_in_evaluation"],
        rank: json["rank"],
        month: json["month"],
        term: json["term"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "student_name": studentName,
        "student_name_en": studentNameEn,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "phone": phone,
        "profileMedia": profileMedia!.toJson(),
        "subject_short": subjectShort,
        "subject_score_min": subjectScoreMin,
        "subject_score_max": subjectScoreMax,
        "score": score,
        "absent_exam": absentExam,
        "teacher_comment": teacherComment,
        "complementary_score": complementaryScore,
        "include_in_evaluation": includeInEvaluation,
        "rank": rank,
        "month": month,
        "term": term,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
    };
}

class ProfileMedia {
    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;
    String? fileShow;
    String? fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
