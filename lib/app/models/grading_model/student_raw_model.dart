
class StudentScoreRawModel {
    StudentScoreRawModel({
        this.studentId,
        this.score,
        this.absentExam,
        this.teacherComment,
    });

    String ?studentId;
    int? score;
    int? absentExam;
    String? teacherComment;

    factory StudentScoreRawModel.fromJson(Map<String, dynamic> json) => StudentScoreRawModel(
        studentId: json["student_id"],
        score: json["score"],
        absentExam: json["absent_exam"],
        teacherComment: json["teacher_comment"],
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "score": score,
        "absent_exam": absentExam,
        "teacher_comment": teacherComment,
    };
}
