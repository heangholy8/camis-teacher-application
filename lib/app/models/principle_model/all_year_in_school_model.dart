// To parse this JSON data, do
//
//     final allYearInschool = allYearInschoolFromJson(jsonString);

import 'dart:convert';

AllYearInschool allYearInschoolFromJson(String str) => AllYearInschool.fromJson(json.decode(str));

String allYearInschoolToJson(AllYearInschool data) => json.encode(data.toJson());

class AllYearInschool {
    bool? status;
    String? message;
    List<DataAllYear>? data;

    AllYearInschool({
        this.status,
        this.message,
        this.data,
    });

    factory AllYearInschool.fromJson(Map<String, dynamic> json) => AllYearInschool(
        status: json["status"],
        message: json["message"],
        data: List<DataAllYear>.from(json["data"].map((x) => DataAllYear.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataAllYear {
    String? id;
    String? name;
    String? nameEn;
    int? currentActive;

    DataAllYear({
        this.id,
        this.name,
        this.nameEn,
        this.currentActive,
    });

    factory DataAllYear.fromJson(Map<String, dynamic> json) => DataAllYear(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        currentActive: json["current_active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "current_active": currentActive,
    };
}
