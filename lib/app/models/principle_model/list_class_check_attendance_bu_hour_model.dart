// To parse this JSON data, do
//
//     final listClassCheckAttendanceByHourModel = listClassCheckAttendanceByHourModelFromJson(jsonString);

import 'dart:convert';

ListClassCheckAttendanceByHourModel listClassCheckAttendanceByHourModelFromJson(String str) => ListClassCheckAttendanceByHourModel.fromJson(json.decode(str));

String listClassCheckAttendanceByHourModelToJson(ListClassCheckAttendanceByHourModel data) => json.encode(data.toJson());

class ListClassCheckAttendanceByHourModel {
    bool? status;
    String? message;
    Data? data;

    ListClassCheckAttendanceByHourModel({
        this.status,
        this.message,
        this.data,
    });

    factory ListClassCheckAttendanceByHourModel.fromJson(Map<String, dynamic> json) => ListClassCheckAttendanceByHourModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    int? index;
    dynamic time;
    dynamic startTime;
    dynamic endTime;
    dynamic shortDay;
    dynamic date;
    bool? isCurrent;
    List<Grade>? grade;

    Data({
        this.index,
        this.time,
        this.startTime,
        this.endTime,
        this.shortDay,
        this.date,
        this.isCurrent,
        this.grade,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        index: json["index"],
        time: json["time"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        shortDay: json["shortDay"],
        date: json["date"],
        isCurrent: json["is_current"],
        grade: List<Grade>.from(json["grade"].map((x) => Grade.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "index": index,
        "time": time,
        "start_time": startTime,
        "end_time": endTime,
        "shortDay": shortDay,
        "date": date,
        "is_current": isCurrent,
        "grade": List<dynamic>.from(grade!.map((x) => x.toJson())),
    };
}

class Grade {
    dynamic grade;
    List<Class>? classes;

    Grade({
        this.grade,
        this.classes,
    });

    factory Grade.fromJson(Map<String, dynamic> json) => Grade(
        grade: json["grade"],
        classes: List<Class>.from(json["classes"].map((x) => Class.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "grade": grade,
        "classes": List<dynamic>.from(classes!.map((x) => x.toJson())),
    };
}

class Class {
    dynamic id;
    dynamic name;
    dynamic subjectName;
    dynamic scheduleId;
    bool? isTakeAttendance;
    dynamic teacherId;
    dynamic teacherName;
    dynamic teacherNameEn;
    dynamic teacherPhone;

    Class({
        this.id,
        this.name,
        this.subjectName,
        this.scheduleId,
        this.isTakeAttendance,
        this.teacherId,
        this.teacherName,
        this.teacherNameEn,
        this.teacherPhone,
    });

    factory Class.fromJson(Map<String, dynamic> json) => Class(
        id: json["id"],
        name: json["name"],
        subjectName: json["subject_name"],
        scheduleId: json["schedule_id"],
        isTakeAttendance: json["is_take_attendance"],
        teacherId: json["teacher_id"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherPhone: json["teacher_phone"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "subject_name": subjectName,
        "schedule_id": scheduleId,
        "is_take_attendance": isTakeAttendance,
        "teacher_id": name,
        "teacher_name": subjectName,
        "teacher_name_en": scheduleId,
        "teacher_phone": isTakeAttendance,
    };
}
