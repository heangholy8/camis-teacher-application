// To parse this JSON data, do
//
//     final summaryStudentStudyResult = summaryStudentStudyResultFromJson(jsonString);

import 'dart:convert';

SummaryStudentStudyResult summaryStudentStudyResultFromJson(String str) => SummaryStudentStudyResult.fromJson(json.decode(str));

String summaryStudentStudyResultToJson(SummaryStudentStudyResult data) => json.encode(data.toJson());

class SummaryStudentStudyResult {
    bool? status;
    String? message;
    List<DataStudyStudentResult>? data;

    SummaryStudentStudyResult({
        this.status,
        this.message,
        this.data,
    });

    factory SummaryStudentStudyResult.fromJson(Map<String, dynamic> json) => SummaryStudentStudyResult(
        status: json["status"],
        message: json["message"],
        data: List<DataStudyStudentResult>.from(json["data"].map((x) => DataStudyStudentResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataStudyStudentResult {
    int? level;
    List<Result>? result;

    DataStudyStudentResult({
        this.level,
        this.result,
    });

    factory DataStudyStudentResult.fromJson(Map<String, dynamic> json) => DataStudyStudentResult(
        level: json["level"],
        result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "level": level,
        "result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    String? name;
    String? month;
    String? semester;
    String? year;
    Fail? pass;
    Fail? fail;

    Result({
        this.name,
        this.month,
        this.semester,
        this.year,
        this.pass,
        this.fail,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        name: json["name"],
        month: json["month"],
        semester: json["semester"],
        year: json["year"],
        pass: Fail.fromJson(json["pass"]),
        fail: Fail.fromJson(json["fail"]),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "month": month,
        "semester": semester,
        "year": year,
        "pass": pass!.toJson(),
        "fail": fail!.toJson(),
    };
}

class Fail {
    int? total;
    double? percent;
    int? male;
    int? female;

    Fail({
        this.total,
        this.percent,
        this.male,
        this.female,
    });

    factory Fail.fromJson(Map<String, dynamic> json) => Fail(
        total: json["total"],
        percent: json["percent"].toDouble(),
        male: json["male"],
        female: json["female"],
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "percent": percent,
        "male": male,
        "female": female,
    };
}
