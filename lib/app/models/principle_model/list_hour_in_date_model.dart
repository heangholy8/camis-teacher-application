// To parse this JSON data, do
//
//     final listHourInDateModel = listHourInDateModelFromJson(jsonString);

import 'dart:convert';

ListHourInDateModel listHourInDateModelFromJson(String str) => ListHourInDateModel.fromJson(json.decode(str));

String listHourInDateModelToJson(ListHourInDateModel data) => json.encode(data.toJson());

class ListHourInDateModel {
    Data? data;

    ListHourInDateModel({
        this.data,
    });

    factory ListHourInDateModel.fromJson(Map<String, dynamic> json) => ListHourInDateModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}

class Data {
    dynamic date;
    dynamic shortDay;
    bool? isCurrent;
    List<HourDatum>? hourData;

    Data({
        this.date,
        this.shortDay,
        this.isCurrent,
        this.hourData,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        date: json["date"],
        shortDay: json["shortDay"],
        isCurrent: json["is_current"],
        hourData: List<HourDatum>.from(json["hour_data"].map((x) => HourDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "date": date,
        "shortDay": shortDay,
        "is_current": isCurrent,
        "hour_data": List<dynamic>.from(hourData!.map((x) => x.toJson())),
    };
}

class HourDatum {
    int? index;
    dynamic time;
    dynamic startTime;
    dynamic endTime;
    bool? isActive;
    bool? isCurrent;

    HourDatum({
        this.index,
        this.time,
        this.startTime,
        this.endTime,
        this.isActive,
        this.isCurrent = false,
    });

    factory HourDatum.fromJson(Map<String, dynamic> json) => HourDatum(
        index: json["index"],
        time: json["time"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        isActive: json["is_active"],
    );

    Map<String, dynamic> toJson() => {
        "index": index,
        "time": time,
        "start_time": startTime,
        "end_time": endTime,
        "is_active": isActive,
    };
}
