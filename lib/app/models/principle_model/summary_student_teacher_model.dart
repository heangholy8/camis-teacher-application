// To parse this JSON data, do
//
//     final srudentAndTeacherInClass = srudentAndTeacherInClassFromJson(jsonString);

import 'dart:convert';

SrudentAndTeacherInClass srudentAndTeacherInClassFromJson(String str) => SrudentAndTeacherInClass.fromJson(json.decode(str));

String srudentAndTeacherInClassToJson(SrudentAndTeacherInClass data) => json.encode(data.toJson());

class SrudentAndTeacherInClass {
    bool? status;
    String? message;
    Data? data;

    SrudentAndTeacherInClass({
        this.status,
        this.message,
        this.data,
    });

    factory SrudentAndTeacherInClass.fromJson(Map<String, dynamic> json) => SrudentAndTeacherInClass(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    Student? student;
    Student? teacher;
    AcademicStructure? academicStructure;

    Data({
        this.student,
        this.teacher,
        this.academicStructure,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        student: Student.fromJson(json["student"]),
        teacher: Student.fromJson(json["teacher"]),
        academicStructure: AcademicStructure.fromJson(json["academic_structure"]),
    );

    Map<String, dynamic> toJson() => {
        "student": student!.toJson(),
        "teacher": teacher!.toJson(),
        "academic_structure": academicStructure!.toJson(),
    };
}

class AcademicStructure {
    int? totalClass;
    List<AcademicStructureEducationStage>? educationStage;
    List<TotalClassLevel>? totalClassLevel;

    AcademicStructure({
        this.totalClass,
        this.educationStage,
        this.totalClassLevel,
    });

    factory AcademicStructure.fromJson(Map<String, dynamic> json) => AcademicStructure(
        totalClass: json["total_class"],
        educationStage: List<AcademicStructureEducationStage>.from(json["education_stage"].map((x) => AcademicStructureEducationStage.fromJson(x))),
        totalClassLevel: List<TotalClassLevel>.from(json["total_class_level"].map((x) => TotalClassLevel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total_class": totalClass,
        "education_stage": List<dynamic>.from(educationStage!.map((x) => x.toJson())),
        "total_class_level": List<dynamic>.from(totalClassLevel!.map((x) => x.toJson())),
    };
}

class AcademicStructureEducationStage {
    String? name;
    int? total;

    AcademicStructureEducationStage({
        this.name,
        this.total,
    });

    factory AcademicStructureEducationStage.fromJson(Map<String, dynamic> json) => AcademicStructureEducationStage(
        name: json["name"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "total": total,
    };
}

class TotalClassLevel {
    int? level;
    int? totalClass;

    TotalClassLevel({
        this.level,
        this.totalClass,
    });

    factory TotalClassLevel.fromJson(Map<String, dynamic> json) => TotalClassLevel(
        level: json["level"],
        totalClass: json["total_class"],
    );

    Map<String, dynamic> toJson() => {
        "level": level,
        "total_class": totalClass,
    };
}

class Student {
    int? total;
    int? female;
    int? male;
    List<LevelElement>? educationStage;
    List<DataGradeStage>? dataGradeStage;

    Student({
        this.total,
        this.female,
        this.male,
        this.educationStage,
        this.dataGradeStage,
    });

    factory Student.fromJson(Map<String, dynamic> json) => Student(
        total: json["total"],
        female: json["female"],
        male: json["male"],
        educationStage: List<LevelElement>.from(json["education_stage"].map((x) => LevelElement.fromJson(x))),
        dataGradeStage: List<DataGradeStage>.from(json["data_grade_stage"].map((x) => DataGradeStage.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "female": female,
        "male": male,
        "education_stage": List<dynamic>.from(educationStage!.map((x) => x.toJson())),
        "data_grade_stage": List<dynamic>.from(dataGradeStage!.map((x) => x.toJson())),
    };
}

class DataGradeStage {
    String? name;
    List<LevelElement>? level;

    DataGradeStage({
        this.name,
        this.level,
    });

    factory DataGradeStage.fromJson(Map<String, dynamic> json) => DataGradeStage(
        name: json["name"],
        level: List<LevelElement>.from(json["level"].map((x) => LevelElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "level": List<dynamic>.from(level!.map((x) => x.toJson())),
    };
}

class LevelElement {
    int? level;
    int? total;
    int? female;
    int? male;
    String? name;

    LevelElement({
        this.level,
        this.total,
        this.female,
        this.male,
        this.name,
    });

    factory LevelElement.fromJson(Map<String, dynamic> json) => LevelElement(
        level: json["level"],
        total: json["total"],
        female: json["female"],
        male: json["male"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "level": level,
        "total": total,
        "female": female,
        "male": male,
        "name": name,
    };
}
