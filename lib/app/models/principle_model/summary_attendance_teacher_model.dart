// To parse this JSON data, do
//
//     final checkAttendanceSummaryTeacherModel = checkAttendanceSummaryTeacherModelFromJson(jsonString);

import 'dart:convert';

CheckAttendanceSummaryTeacherModel checkAttendanceSummaryTeacherModelFromJson(String str) => CheckAttendanceSummaryTeacherModel.fromJson(json.decode(str));

String checkAttendanceSummaryTeacherModelToJson(CheckAttendanceSummaryTeacherModel data) => json.encode(data.toJson());

class CheckAttendanceSummaryTeacherModel {
    bool? status;
    String? message;
    Data? data;

    CheckAttendanceSummaryTeacherModel({
        this.status,
        this.message,
        this.data,
    });

    factory CheckAttendanceSummaryTeacherModel.fromJson(Map<String, dynamic> json) => CheckAttendanceSummaryTeacherModel(
        status: json["status"],
        message: json["message"],
        data: json["data"] == null?null:Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data==null?null: data!.toJson(),
    };
}

class Data {
    dynamic date;
    dynamic shortDay;
    dynamic isCurrent;
    dynamic totalSchedule;
    dynamic totalCount;
    dynamic currentDayCount;
    dynamic dayAfterCount;

    Data({
        this.date,
        this.shortDay,
        this.isCurrent,
        this.totalSchedule,
        this.totalCount,
        this.currentDayCount,
        this.dayAfterCount,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        date: json["date"],
        shortDay: json["shortDay"],
        isCurrent: json["is_current"],
        totalSchedule: json["total_schedule"],
        totalCount: json["total_count"],
        currentDayCount: json["current_day_count"],
        dayAfterCount: json["day_after_count"],
    );

    Map<String, dynamic> toJson() => {
        "date": date,
        "shortDay": shortDay,
        "is_current": isCurrent,
        "total_schedule": totalSchedule,
        "total_count": totalCount,
        "current_day_count": currentDayCount,
        "day_after_count": dayAfterCount,
    };
}
