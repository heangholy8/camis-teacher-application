// To parse this JSON data, do
//
//     final listCheckClassInputScoreModel = listCheckClassInputScoreModelFromJson(jsonString);

import 'dart:convert';

ListCheckClassInputScoreModel listCheckClassInputScoreModelFromJson(String str) => ListCheckClassInputScoreModel.fromJson(json.decode(str));

String listCheckClassInputScoreModelToJson(ListCheckClassInputScoreModel data) => json.encode(data.toJson());

class ListCheckClassInputScoreModel {
    Data? data;

    ListCheckClassInputScoreModel({
        this.data,
    });

    factory ListCheckClassInputScoreModel.fromJson(Map<String, dynamic> json) => ListCheckClassInputScoreModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}

class Data {
    dynamic mapDefaultSubjectId;
    String? name;
    String? nameEn;
    dynamic code;
    List<Grade>? grades;

    Data({
        this.mapDefaultSubjectId,
        this.name,
        this.nameEn,
        this.code,
        this.grades,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        mapDefaultSubjectId: json["map_default_subject_id"],
        name:json["name"],
        nameEn: json["name_en"],
        code: json["code"],
        grades: List<Grade>.from(json["grades"].map((x) => Grade.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "map_default_subject_id": mapDefaultSubjectId,
        "name": name,
        "name_en": nameEn,
        "code": code,
        "grades": List<dynamic>.from(grades!.map((x) => x.toJson())),
    };
}

class Grade {
    dynamic level;
    List<Class>? classes;

    Grade({
        this.level,
        this.classes,
    });

    factory Grade.fromJson(Map<String, dynamic> json) => Grade(
        level: json["level"],
        classes: List<Class>.from(json["classes"].map((x) => Class.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "level": level,
        "classes": List<dynamic>.from(classes!.map((x) => x.toJson())),
    };
}

class Class {
    dynamic id;
    String? name;
    dynamic subjectId;
    dynamic subjectName;
    dynamic mapDefaultSubjectId;
    dynamic totalStudents;
    dynamic totalScoreEntredStudents;
    dynamic isCheckScoreEnter;
    dynamic scoreEnterStatusName;
    dynamic teacherId;
    dynamic teacherName;
    dynamic teacherNameEn;
    dynamic teacherPhone;
    String? month;
    dynamic semester;
    String? type;
    SubjectExam? subjectExam;

    Class({
        this.id,
        this.name,
        this.subjectId,
        this.subjectName,
        this.mapDefaultSubjectId,
        this.totalStudents,
        this.totalScoreEntredStudents,
        this.isCheckScoreEnter,
        this.scoreEnterStatusName,
        this.month,
        this.teacherId,
        this.teacherName,
        this.teacherNameEn,
        this.teacherPhone,
        this.semester,
        this.type,
        this.subjectExam,
    });

    factory Class.fromJson(Map<String, dynamic> json) => Class(
        id: json["id"],
        name: json["name"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        mapDefaultSubjectId: json["map_default_subject_id"],
        teacherId: json["teacher_id"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherPhone: json["teacher_phone"],
        totalStudents: json["total_students"],
        totalScoreEntredStudents: json["total_score_entred_students"],
        isCheckScoreEnter: json["is_check_score_enter"],
        scoreEnterStatusName: json["score_enter_status_name"],
        month: json["month"],
        semester: json["semester"],
        type: json["type"],
        subjectExam: SubjectExam.fromJson(json["subject_exam"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "map_default_subject_id": mapDefaultSubjectId,
        "teacher_id": teacherId,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_phone": teacherPhone,
        "total_students": totalStudents,
        "total_score_entred_students": totalScoreEntredStudents,
        "is_check_score_enter": isCheckScoreEnter,
        "score_enter_status_name": scoreEnterStatusName,
        "month": month,
        "semester": semester,
        "type": type,
        "subject_exam": subjectExam!.toJson(),
    };
}

class SubjectExam {
    dynamic id;
    dynamic examDate;
    dynamic type;
    dynamic month;
    String? semester;
    dynamic isNoExam;
    dynamic isApprove;
    dynamic approvedBy;
    dynamic approvedAt;

    SubjectExam({
        this.id,
        this.examDate,
        this.type,
        this.month,
        this.semester,
        this.isNoExam,
        this.isApprove,
        this.approvedBy,
        this.approvedAt,
    });

    factory SubjectExam.fromJson(Map<String, dynamic> json) => SubjectExam(
        id: json["id"],
        examDate: json["exam_date"],
        type: json["type"],
        month: json["month"],
        semester:json["semester"],
        isNoExam: json["is_no_exam"],
        isApprove: json["is_approve"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "exam_date": examDate,
        "type": type,
        "month": month,
        "semester": semester,
        "is_no_exam": isNoExam,
        "is_approve": isApprove,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
    };
}


