// To parse this JSON data, do
//
//     final allSubjectListModel = allSubjectListModelFromJson(jsonString);

import 'dart:convert';

AllSubjectListModel allSubjectListModelFromJson(String str) => AllSubjectListModel.fromJson(json.decode(str));

String allSubjectListModelToJson(AllSubjectListModel data) => json.encode(data.toJson());

class AllSubjectListModel {
    bool? status;
    List<Datum>? data;

    AllSubjectListModel({
        this.status,
        this.data,
    });

    factory AllSubjectListModel.fromJson(Map<String, dynamic> json) => AllSubjectListModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    int? index;
    int? mapDefaultSubjectId;
    String? name;
    String? nameEn;
    dynamic code;

    Datum({
        this.index,
        this.mapDefaultSubjectId,
        this.name,
        this.nameEn,
        this.code,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        index: json["index"],
        mapDefaultSubjectId: json["map_default_subject_id"],
        name: json["name"],
        nameEn: json["name_en"],
        code: json["code"],
    );

    Map<String, dynamic> toJson() => {
        "index": index,
        "map_default_subject_id": mapDefaultSubjectId,
        "name": name,
        "name_en": nameEn,
        "code": code,
    };
}
