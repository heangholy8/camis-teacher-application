// To parse this JSON data, do
//
//     final feedBackModel = feedBackModelFromJson(jsonString);

import 'dart:convert';

FeedBackModel feedBackModelFromJson(String str) => FeedBackModel.fromJson(json.decode(str));

String feedBackModelToJson(FeedBackModel data) => json.encode(data.toJson());

class FeedBackModel {
    bool? status;
    dynamic error;
    Data? data;

    FeedBackModel({
        this.status,
        this.error,
        this.data,
    });

    factory FeedBackModel.fromJson(Map<String, dynamic> json) => FeedBackModel(
        status: json["status"],
        error: json["error"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": data!.toJson(),
    };
}

class Data {
    dynamic total;
    dynamic currentPage;
    dynamic hasMorePages;
    List<DataFeedback>? data;

    Data({
        this.total,
        this.currentPage,
        this.hasMorePages,
        this.data,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        currentPage: json["current_page"],
        hasMorePages: json["hasMorePages"],
        data: List<DataFeedback>.from(json["data"].map((x) => DataFeedback.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "current_page": currentPage,
        "hasMorePages": hasMorePages,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataFeedback {
    dynamic id;
    dynamic customerId;
    dynamic guardianId;
    dynamic classId;
    dynamic className;
    dynamic comment;
    dynamic isSeen;
    dynamic seenBy;
    dynamic seenAt;
    dynamic createdAt;
    dynamic updatedAt;
    dynamic hasFile;
    List<FileElement>? file;

    DataFeedback({
        this.id,
        this.customerId,
        this.guardianId,
        this.classId,
        this.className,
        this.comment,
        this.isSeen,
        this.seenBy,
        this.seenAt,
        this.createdAt,
        this.updatedAt,
        this.hasFile,
        this.file,
    });

    factory DataFeedback.fromJson(Map<String, dynamic> json) => DataFeedback(
        id: json["id"],
        customerId: json["customer_id"],
        guardianId: json["guardian_id"],
        classId: json["class_id"],
        className: json["class_name"],
        comment: json["comment"],
        isSeen: json["is_seen"],
        seenBy: json["seen_by"],
        seenAt: json["seen_at"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        hasFile: json["has_file"],
        file: List<FileElement>.from(json["file"].map((x) => FileElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "customer_id": customerId,
        "guardian_id": guardianId,
        "class_id": classId,
        "class_name": className,
        "comment": comment,
        "is_seen": isSeen,
        "seen_by": seenBy,
        "seen_at": seenAt,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "has_file": hasFile,
        "file": List<dynamic>.from(file!.map((x) => x.toJson())),
    };
}

class FileElement {
    dynamic id;
    dynamic fileName;
    dynamic fileSize;
    dynamic fileType;
    dynamic fileShow;
    dynamic fileThumbnail;
    dynamic fileIndex;
    dynamic fileArea;
    dynamic objectId;
    dynamic schoolUrl;
    dynamic postDate;
    dynamic isGdrive;
    dynamic fileGdriveId;
    dynamic isS3;

    FileElement({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}
