// To parse this JSON data, do
//
//     final listSubjectExamModel = listSubjectExamModelFromJson(jsonString);

import 'dart:convert';

List<ListSubjectExamModel> listSubjectExamModelFromJson(String str) =>
    List<ListSubjectExamModel>.from(
        json.decode(str).map((x) => ListSubjectExamModel.fromJson(x)));

String listSubjectExamModelToJson(List<ListSubjectExamModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ListSubjectExamModel {
  ListSubjectExamModel({
    this.id,
    this.objectType,
    this.name,
    this.nameEn,
    this.schoolyear,
    this.schoolType,
    this.isInstructor,
    this.teachingSubjects,
  });

  int? id;
  String? objectType;
  String? name;
  String? nameEn;
  String? schoolyear;
  int? schoolType;
  int? isInstructor;
  List<TeachingSubject>? teachingSubjects;

  factory ListSubjectExamModel.fromJson(Map<String, dynamic> json) =>
      ListSubjectExamModel(
        id: json["id"],
        objectType: json["object_type"],
        name: json["name"],
        nameEn: json["name_en"],
        schoolyear: json["schoolyear"],
        schoolType: json["school_type"],
        isInstructor: json["is_instructor"],
        teachingSubjects: List<TeachingSubject>.from(
            json["teaching_subjects"].map((x) => TeachingSubject.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object_type": objectType,
        "name": name,
        "name_en": nameEn,
        "schoolyear": schoolyear,
        "school_type": schoolType,
        "is_instructor": isInstructor,
        "teaching_subjects":
            List<dynamic>.from(teachingSubjects!.map((x) => x.toJson())),
      };
}

class TeachingSubject {
  TeachingSubject({
    this.scheduleId,
    this.subjectId,
    this.subjectName,
    this.subjectNameEn,
    this.subjectExam,
    this.examDate,
    this.type,
    this.month,
    this.semester,
    this.description,
    this.isApprove,
    this.approvedBy,
    this.approvedAt,
  });

  int? scheduleId;
  int? subjectId;
  String? subjectName;
  String? subjectNameEn;
  dynamic subjectExam;
  String? examDate;
  String? type;
  String? month;
  String? semester;
  String? description;
  String? isApprove;
  String? approvedBy;
  String? approvedAt;

  factory TeachingSubject.fromJson(Map<String, dynamic> json) =>
      TeachingSubject(
        scheduleId: json["schedule_id"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectExam: json["subject_exam"],
        examDate: json["exam_date"],
        type: json["type"],
        month: json["month"],
        semester: json["semester"],
        description: json["description"],
        isApprove: json["is_approve"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
      );

  Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_exam": subjectExam,
        "exam_date": examDate,
        "type": type,
        "month": month,
        "semester": semester,
        "description": description,
        "is_approve": isApprove,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
      };
}
