class MonthModel{
  bool? isActive;
  int? month;
  MonthModel({required this.isActive,required this.month,});

  static List<MonthModel> generate(){
  return[
     MonthModel(month: 1,isActive: false),
     MonthModel(month: 2,isActive: false), 
     MonthModel(month: 3,isActive: false), 
     MonthModel(month: 4,isActive: false), 
     MonthModel(month: 5,isActive: false), 
     MonthModel(month: 6,isActive: false), 
     MonthModel(month: 7,isActive: false), 
     MonthModel(month: 8,isActive: false), 
     MonthModel(month: 9,isActive: false), 
     MonthModel(month: 10,isActive: false), 
     MonthModel(month: 11,isActive: false), 
     MonthModel(month: 12,isActive: false),           
    ];
  }
}