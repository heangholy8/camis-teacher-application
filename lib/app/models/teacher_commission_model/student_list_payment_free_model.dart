// To parse this JSON data, do
//
//     final listStudentPaymentModel = listStudentPaymentModelFromJson(jsonString);

import 'dart:convert';

ListStudentPaymentModel listStudentPaymentModelFromJson(String str) => ListStudentPaymentModel.fromJson(json.decode(str));

String listStudentPaymentModelToJson(ListStudentPaymentModel data) => json.encode(data.toJson());

class ListStudentPaymentModel {
    bool? status;
    List<StudentListPayment>? data;
    String? message;

    ListStudentPaymentModel({
        this.status,
        this.data,
        this.message,
    });

    factory ListStudentPaymentModel.fromJson(Map<String, dynamic> json) => ListStudentPaymentModel(
        status: json["status"],
        data: List<StudentListPayment>.from(json["data"].map((x) => StudentListPayment.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
    };
}

class StudentListPayment {
    String? guid;
    String? studentId;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    String? gender;
    dynamic genderNumber;
    String? dob;
    String? birthPlace;
    String? phone;
    String? email;
    String? address;
    String? guardianId;
    String? guardianName;
    String? guardianNameEn;
    String? guardianPhone;
    ProfileMedia? profileMedia;
    dynamic appPaymentExpire;

    StudentListPayment({
        this.guid,
        this.studentId,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderNumber,
        this.dob,
        this.birthPlace,
        this.phone,
        this.email,
        this.address,
        this.guardianId,
        this.guardianName,
        this.guardianNameEn,
        this.guardianPhone,
        this.profileMedia,
        this.appPaymentExpire,
    });

    factory StudentListPayment.fromJson(Map<String, dynamic> json) => StudentListPayment(
        guid: json["guid"],
        studentId: json["student_id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderNumber: json["gender_number"],
        dob: json["dob"],
        birthPlace: json["birth_place"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        guardianId: json["guardian_id"],
        guardianName: json["guardian_name"],
        guardianNameEn: json["guardian_name_en"],
        guardianPhone: json["guardian_phone"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        appPaymentExpire: json["app_payment_expire"],
    );

    Map<String, dynamic> toJson() => {
        "guid": guid,
        "student_id": studentId,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_number": genderNumber,
        "dob": dob,
        "birth_place": birthPlace,
        "phone": phone,
        "email": email,
        "address": address,
        "guardian_id": guardianId,
        "guardian_name": guardianName,
        "guardian_name_en": guardianNameEn,
        "guardian_phone": guardianPhone,
        "profileMedia": profileMedia!.toJson(),
        "app_payment_expire": appPaymentExpire,
    };
}

class ProfileMedia {
    dynamic id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    dynamic isGdrive;
    String? fileGdriveId;
    dynamic isS3;
    String? fileShow;
    String? fileThumbnail;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
