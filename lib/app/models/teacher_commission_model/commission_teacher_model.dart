// To parse this JSON data, do
//
//     final teacherCommissionModel = teacherCommissionModelFromJson(jsonString);

import 'dart:convert';

TeacherCommissionModel teacherCommissionModelFromJson(String str) => TeacherCommissionModel.fromJson(json.decode(str));

String teacherCommissionModelToJson(TeacherCommissionModel data) => json.encode(data.toJson());

class TeacherCommissionModel {
    bool? status;
    DataCommission? data;
    String? message;

    TeacherCommissionModel({
        this.status,
        this.data,
        this.message,
    });

    factory TeacherCommissionModel.fromJson(Map<String, dynamic> json) => TeacherCommissionModel(
        status: json["status"],
        data: DataCommission.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data!.toJson(),
        "message": message,
    };
}

class DataCommission {
    dynamic reportId;
    dynamic month;
    dynamic year;
    dynamic totalIncome;
    dynamic totalCashIncome;
    dynamic totalCommission;
    dynamic currency;
    List<Attachment>? attachments;
    dynamic status;
    dynamic paidAt;
    dynamic commissionPercentage;
    List<Item>? items;

    DataCommission({
        this.reportId,
        this.month,
        this.year,
        this.totalIncome,
        this.totalCashIncome,
        this.totalCommission,
        this.currency,
        this.attachments,
        this.status,
        this.paidAt,
        this.commissionPercentage,
        this.items,
    });

    factory DataCommission.fromJson(Map<String, dynamic> json) => DataCommission(
        reportId: json["report_id"],
        month: json["month"],
        year: json["year"],
        totalIncome: json["total_income"],
        totalCashIncome: json["total_cash_income"],
        totalCommission: json["total_commission"],
        currency: json["currency"],
        attachments: List<Attachment>.from(json["attachments"].map((x) => Attachment.fromJson(x))),
        status: json["status"],
        paidAt: json["paid_at"],
        commissionPercentage: json["commission_percentage"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "report_id": reportId,
        "month": month,
        "year": year,
        "total_income": totalIncome,
        "total_cash_income": totalCashIncome,
        "total_commission": totalCommission,
        "currency": currency,
        "attachments": List<dynamic>.from(attachments!.map((x) => x.toJson())),
        "status": status,
        "paid_at": paidAt,
        "commission_percentage": commissionPercentage,
        "items": List<dynamic>.from(items!.map((x) => x.toJson())),
    };
}

class Attachment {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    Attachment({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class Item {
    dynamic id;
    dynamic staffId;
    dynamic staffName;
    dynamic studentId;
    dynamic studentName;
    int? classId;
    String? className;
    String? paymentMethod;
    int? choosePayOption;
    dynamic amount;
    dynamic currency;
    dynamic status;
    dynamic successAt;
    dynamic createdBy;
    dynamic guardianPaymentId;
    GuardianPayment? guardianPayment;
    int? isPaid;
    dynamic paidAt;
    dynamic commission;
    dynamic paidBy;
    dynamic commissionReportId;
    dynamic attachment;
    dynamic createdAt;
    dynamic updatedAt;
    String? studentQrUrl;
    String? guardianQrUrl;

    Item({
        this.id,
        this.staffId,
        this.staffName,
        this.studentId,
        this.studentName,
        this.classId,
        this.className,
        this.paymentMethod,
        this.choosePayOption,
        this.amount,
        this.currency,
        this.status,
        this.successAt,
        this.createdBy,
        this.guardianPaymentId,
        this.guardianPayment,
        this.isPaid,
        this.paidAt,
        this.commission,
        this.paidBy,
        this.commissionReportId,
        this.attachment,
        this.createdAt,
        this.updatedAt,
        this.studentQrUrl,
        this.guardianQrUrl,
    });

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        staffId: json["staff_id"],
        staffName: json["staff_name"],
        studentId: json["student_id"],
        studentName: json["student_name"],
        classId: json["class_id"],
        className: json["class_name"],
        paymentMethod: json["payment_method"],
        choosePayOption: json["choose_pay_option"],
        amount: json["amount"],
        currency: json["currency"],
        status: json["status"],
        successAt: DateTime.parse(json["success_at"]),
        createdBy: json["created_by"],
        guardianPaymentId: json["guardian_payment_id"],
        guardianPayment: GuardianPayment.fromJson(json["guardianPayment"]),
        isPaid: json["is_paid"],
        paidAt: json["paid_at"],
        commission: json["commission"],
        paidBy: json["paid_by"],
        commissionReportId: json["commission_report_id"],
        attachment: json["attachment"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        studentQrUrl: json["student_qr_url"],
        guardianQrUrl: json["guardian_qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "staff_id": staffId,
        "staff_name": staffName,
        "student_id": studentId,
        "student_name": studentName,
        "class_id": classId,
        "class_name": className,
        "payment_method": paymentMethod,
        "choose_pay_option": choosePayOption,
        "amount": amount,
        "currency": currency,
        "status": status,
        "success_at": successAt.toIso8601String(),
        "created_by": createdBy,
        "guardian_payment_id": guardianPaymentId,
        "guardianPayment": guardianPayment!.toJson(),
        "is_paid": isPaid,
        "paid_at": paidAt,
        "commission": commission,
        "paid_by": paidBy,
        "commission_report_id": commissionReportId,
        "attachment": attachment,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "student_qr_url": studentQrUrl,
        "guardian_qr_url": guardianQrUrl,
    };
}

class GuardianPayment {
    dynamic id;
    dynamic invoiceNumber;
    dynamic guardianId;
    dynamic amount;
    String? currency;
    int? choosePayOption;
    dynamic choosePayOptionName;
    dynamic choosePayOptionNameEn;
    dynamic transactionId;
    dynamic status;
    dynamic month;
    dynamic semester;
    dynamic piadfor;
    dynamic paymentMethod;
    dynamic expiredAt;
    dynamic paidAt;
    dynamic createdAt;
    dynamic updatedAt;
    dynamic createdBy;
    dynamic createdByName;
    Guardian? guardian;

    GuardianPayment({
        this.id,
        this.invoiceNumber,
        this.guardianId,
        this.amount,
        this.currency,
        this.choosePayOption,
        this.choosePayOptionName,
        this.choosePayOptionNameEn,
        this.transactionId,
        this.status,
        this.month,
        this.semester,
        this.piadfor,
        this.paymentMethod,
        this.expiredAt,
        this.paidAt,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.createdByName,
        this.guardian,
    });

    factory GuardianPayment.fromJson(Map<String, dynamic> json) => GuardianPayment(
        id: json["id"],
        invoiceNumber: json["invoice_number"],
        guardianId: json["guardian_id"],
        amount: json["amount"],
        currency: json["currency"],
        choosePayOption: json["choose_pay_option"],
        choosePayOptionName: json["choose_pay_option_name"],
        choosePayOptionNameEn: json["choose_pay_option_name_en"],
        transactionId: json["transaction_id"],
        status: json["status"],
        month: json["month"],
        semester: json["semester"],
        piadfor: json["piadfor"],
        paymentMethod: json["payment_method"],
        expiredAt: json["expired_at"],
        paidAt: json["paid_at"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        createdBy: json["created_by"],
        createdByName: json["created_by_name"],
        guardian: Guardian.fromJson(json["guardian"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "invoice_number": invoiceNumber,
        "guardian_id": guardianId,
        "amount": amount,
        "currency": currency,
        "choose_pay_option": choosePayOption,
        "choose_pay_option_name": choosePayOptionName,
        "choose_pay_option_name_en": choosePayOptionNameEn,
        "transaction_id": transactionId,
        "status": status,
        "month": month,
        "semester": semester,
        "piadfor": piadfor,
        "payment_method": paymentMethod,
        "expired_at": expiredAt,
        "paid_at": paidAt,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "created_by": createdBy,
        "created_by_name": createdByName,
        "guardian": guardian!.toJson(),
    };
}

class Guardian {
    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? role;
    dynamic isVerify;
    Attachment? profileMedia;
    String? qrUrl;

    Guardian({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.isVerify,
        this.profileMedia,
        this.qrUrl,
    });

    factory Guardian.fromJson(Map<String, dynamic> json) => Guardian(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        isVerify: json["is_verify"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "is_verify": isVerify,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}
