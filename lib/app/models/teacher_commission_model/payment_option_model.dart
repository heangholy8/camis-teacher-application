// To parse this JSON data, do
//
//     final paymentOptionModel = paymentOptionModelFromJson(jsonString);

import 'dart:convert';

PaymentOptionModel paymentOptionModelFromJson(String str) => PaymentOptionModel.fromJson(json.decode(str));

String paymentOptionModelToJson(PaymentOptionModel data) => json.encode(data.toJson());

class PaymentOptionModel {
    List<DataPaymentOption>? data;

    PaymentOptionModel({
        this.data,
    });

    factory PaymentOptionModel.fromJson(Map<String, dynamic> json) => PaymentOptionModel(
        data: List<DataPaymentOption>.from(json["data"].map((x) => DataPaymentOption.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataPaymentOption {
    String? name;
    String? nameEn;
    int? amount;
    int? type;
    String? currency;
    int? amountOfMonth;
    dynamic duration;

    DataPaymentOption({
        this.name,
        this.nameEn,
        this.amount,
        this.type,
        this.currency,
        this.amountOfMonth,
        this.duration,
    });

    factory DataPaymentOption.fromJson(Map<String, dynamic> json) => DataPaymentOption(
        name: json["name"],
        nameEn: json["name_en"],
        amount: json["amount"],
        type: json["type"],
        currency: json["currency"],
        amountOfMonth: json["amount_of_month"],
        duration: json["duration"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "name_en": nameEn,
        "amount": amount,
        "type": type,
        "currency": currency,
        "amount_of_month": amountOfMonth,
        "duration": duration,
    };
}
