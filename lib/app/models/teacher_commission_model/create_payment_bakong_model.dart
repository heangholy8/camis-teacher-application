// To parse this JSON data, do
//
//     final createPaymentBakongModel = createPaymentBakongModelFromJson(jsonString);

import 'dart:convert';

CreatePaymentBakongModel createPaymentBakongModelFromJson(String str) => CreatePaymentBakongModel.fromJson(json.decode(str));

String createPaymentBakongModelToJson(CreatePaymentBakongModel data) => json.encode(data.toJson());

class CreatePaymentBakongModel {
    DataPaymentBakong? data;
    Link? link;
    bool? status;
    String? message;

    CreatePaymentBakongModel({
        this.data,
        this.link,
        this.status,
        this.message,
    });

    factory CreatePaymentBakongModel.fromJson(Map<String, dynamic> json) => CreatePaymentBakongModel(
        data: DataPaymentBakong.fromJson(json["data"]),
        link: Link.fromJson(json["link"]),
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
        "link": link!.toJson(),
        "status": status,
        "message": message,
    };
}

class DataPaymentBakong {
    int? id;
    String? staffId;
    String? staffName;
    String? studentId;
    String? studentName;
    String? classId;
    String? className;
    dynamic paymentMethod;
    dynamic choosePayOption;
    dynamic amount;
    String? currency;
    dynamic status;
    dynamic successAt;
    dynamic createdBy;
    int? guardianPaymentId;
    GuardianPayment? guardianPayment;
    dynamic isPaid;
    dynamic paidAt;
    dynamic commission;
    dynamic paidBy;
    dynamic commissionReportId;
    dynamic attachment;
    dynamic createdAt;
    dynamic updatedAt;
    String? studentQrUrl;
    String? guardianQrUrl;

    DataPaymentBakong({
        this.id,
        this.staffId,
        this.staffName,
        this.studentId,
        this.studentName,
        this.classId,
        this.className,
        this.paymentMethod,
        this.choosePayOption,
        this.amount,
        this.currency,
        this.status,
        this.successAt,
        this.createdBy,
        this.guardianPaymentId,
        this.guardianPayment,
        this.isPaid,
        this.paidAt,
        this.commission,
        this.paidBy,
        this.commissionReportId,
        this.attachment,
        this.createdAt,
        this.updatedAt,
        this.studentQrUrl,
        this.guardianQrUrl,
    });

    factory DataPaymentBakong.fromJson(Map<String, dynamic> json) => DataPaymentBakong(
        id: json["id"],
        staffId: json["staff_id"],
        staffName: json["staff_name"],
        studentId: json["student_id"],
        studentName: json["student_name"],
        classId: json["class_id"],
        className: json["class_name"],
        paymentMethod: json["payment_method"],
        choosePayOption: json["choose_pay_option"],
        amount: json["amount"],
        currency: json["currency"],
        status: json["status"],
        successAt: json["success_at"],
        createdBy: json["created_by"],
        guardianPaymentId: json["guardian_payment_id"],
        guardianPayment: GuardianPayment.fromJson(json["guardianPayment"]),
        isPaid: json["is_paid"],
        paidAt: json["paid_at"],
        commission: json["commission"],
        paidBy: json["paid_by"],
        commissionReportId: json["commission_report_id"],
        attachment: json["attachment"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        studentQrUrl: json["student_qr_url"],
        guardianQrUrl: json["guardian_qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "staff_id": staffId,
        "staff_name": staffName,
        "student_id": studentId,
        "student_name": studentName,
        "class_id": classId,
        "class_name": className,
        "payment_method": paymentMethod,
        "choose_pay_option": choosePayOption,
        "amount": amount,
        "currency": currency,
        "status": status,
        "success_at": successAt,
        "created_by": createdBy,
        "guardian_payment_id": guardianPaymentId,
        "guardianPayment": guardianPayment!.toJson(),
        "is_paid": isPaid,
        "paid_at": paidAt,
        "commission": commission,
        "paid_by": paidBy,
        "commission_report_id": commissionReportId,
        "attachment": attachment,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "student_qr_url": studentQrUrl,
        "guardian_qr_url": guardianQrUrl,
    };
}

class GuardianPayment {
    int? id;
    String? invoiceNumber;
    String? guardianId;
    dynamic amount;
    String? currency;
    dynamic choosePayOption;
    String? choosePayOptionName;
    String? choosePayOptionNameEn;
    String? transactionId;
    dynamic status;
    dynamic month;
    dynamic semester;
    dynamic piadfor;
    String? paymentMethod;
    dynamic expiredAt;
    dynamic paidAt;
    dynamic createdAt;
    String? updatedAt;
    String? createdBy;
    String? createdByName;
    Guardian? guardian;

    GuardianPayment({
        this.id,
        this.invoiceNumber,
        this.guardianId,
        this.amount,
        this.currency,
        this.choosePayOption,
        this.choosePayOptionName,
        this.choosePayOptionNameEn,
        this.transactionId,
        this.status,
        this.month,
        this.semester,
        this.piadfor,
        this.paymentMethod,
        this.expiredAt,
        this.paidAt,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.createdByName,
        this.guardian,
    });

    factory GuardianPayment.fromJson(Map<String, dynamic> json) => GuardianPayment(
        id: json["id"],
        invoiceNumber: json["invoice_number"],
        guardianId: json["guardian_id"],
        amount: json["amount"],
        currency: json["currency"],
        choosePayOption: json["choose_pay_option"],
        choosePayOptionName: json["choose_pay_option_name"],
        choosePayOptionNameEn: json["choose_pay_option_name_en"],
        transactionId: json["transaction_id"],
        status: json["status"],
        month: json["month"],
        semester: json["semester"],
        piadfor: json["piadfor"],
        paymentMethod: json["payment_method"],
        expiredAt: json["expired_at"],
        paidAt: json["paid_at"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        createdBy: json["created_by"],
        createdByName: json["created_by_name"],
        guardian: Guardian.fromJson(json["guardian"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "invoice_number": invoiceNumber,
        "guardian_id": guardianId,
        "amount": amount,
        "currency": currency,
        "choose_pay_option": choosePayOption,
        "choose_pay_option_name": choosePayOptionName,
        "choose_pay_option_name_en": choosePayOptionNameEn,
        "transaction_id": transactionId,
        "status": status,
        "month": month,
        "semester": semester,
        "piadfor": piadfor,
        "payment_method": paymentMethod,
        "expired_at": expiredAt,
        "paid_at": paidAt,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "created_by": createdBy,
        "created_by_name": createdByName,
        "guardian": guardian!.toJson(),
    };
}

class Guardian {
    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String? genderName;
    dynamic dob;
    dynamic phone;
    String? email;
    String? address;
    dynamic role;
    dynamic isVerify;
    ProfileMedia? profileMedia;
    String? qrUrl;

    Guardian({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.isVerify,
        this.profileMedia,
        this.qrUrl,
    });

    factory Guardian.fromJson(Map<String, dynamic> json) => Guardian(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        isVerify: json["is_verify"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "is_verify": isVerify,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class ProfileMedia {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    dynamic postDate;
    dynamic isGdrive;
    String? fileGdriveId;
    dynamic isS3;
    String? fileShow;
    String? fileThumbnail;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}

class Link {
    int? transactionId;
    String? location;

    Link({
        this.transactionId,
        this.location,
    });

    factory Link.fromJson(Map<String, dynamic> json) => Link(
        transactionId: json["transaction_id"],
        location: json["location"],
    );

    Map<String, dynamic> toJson() => {
        "transaction_id": transactionId,
        "location": location,
    };
}
