// To parse this JSON data, do
//
//     final postExamModel = postExamModelFromJson(jsonString);

import 'dart:convert';

PostExamModel postExamModelFromJson(String str) => PostExamModel.fromJson(json.decode(str));

String postExamModelToJson(PostExamModel data) => json.encode(data.toJson());

class PostExamModel {
    PostExamModel({
        this.status,
        this.data,
        this.message,
    });

    bool? status;
    Data? data;
    String? message;

    factory PostExamModel.fromJson(Map<String, dynamic> json) => PostExamModel(
        status: json["status"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data!.toJson(),
        "message": message,
    };
}

class Data {
    Data({
        this.id,
        this.subjectId,
        this.classId,
        this.scheduleId,
        this.type,
        this.examDate,
        this.month,
        this.semester,
        this.description,
        this.isNoExam,
        this.isApprove,
        this.approvedBy,
        this.approvedAt,
        this.createdBy,
        this.createdAt,
    });

    int? id;
    String? subjectId;
    String? classId;
    dynamic scheduleId;
    dynamic type;
    dynamic examDate;
    int? month;
    String? semester;
    String? description;
    int? isNoExam;
    int? isApprove;
    String? approvedBy;
    String? approvedAt;
    String? createdBy;
    String? createdAt;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        subjectId: json["subject_id"],
        classId: json["class_id"],
        scheduleId: json["schedule_id"],
        type: json["type"],
        examDate: json["exam_date"],
        month: json["month"],
        semester: json["semester"],
        description: json["description"],
        isNoExam: json["is_no_exam"],
        isApprove: json["is_approve"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
        createdBy: json["created_by"],
        createdAt: json["created_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "subject_id": subjectId,
        "class_id": classId,
        "schedule_id": scheduleId,
        "type": type,
        "exam_date": examDate,
        "month": month,
        "semester": semester,
        "description": description,
        "is_no_exam": isNoExam,
        "is_approve": isApprove,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
        "created_by": createdBy,
        "created_at": createdAt,
    };
}
