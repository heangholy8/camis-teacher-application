// To parse this JSON data, do
//
//     final calculateScoreModel = calculateScoreModelFromJson(jsonString);

import 'dart:convert';

CalculateScoreModel calculateScoreModelFromJson(String str) => CalculateScoreModel.fromJson(json.decode(str));

String calculateScoreModelToJson(CalculateScoreModel data) => json.encode(data.toJson());

class CalculateScoreModel {
    CalculateScoreModel({
        this.status,
        this.message,
    });

    bool? status;
    String? message;

    factory CalculateScoreModel.fromJson(Map<String, dynamic> json) => CalculateScoreModel(
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
    };
}
