// To parse this JSON data, do
//
//     final myExamScheduleModel = myExamScheduleModelFromJson(jsonString);

import 'dart:convert';

MyExamScheduleModel myExamScheduleModelFromJson(String str) => MyExamScheduleModel.fromJson(json.decode(str));

String myExamScheduleModelToJson(MyExamScheduleModel data) => json.encode(data.toJson());

class MyExamScheduleModel {
    MyExamScheduleModel({
        this.status,
        this.data,
        this.message,
    });

    bool? status;
    List<DataMyExam>? data;
    String? message;

    factory MyExamScheduleModel.fromJson(Map<String, dynamic> json) => MyExamScheduleModel(
        status: json["status"],
        data: List<DataMyExam>.from(json["data"].map((x) => DataMyExam.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
    };
}

class DataMyExam {
    DataMyExam({
        this.classId,
        this.objectType,
        this.name,
        this.nameEn,
        this.schoolyear,
        this.schoolType,
        this.isInstructor,
        this.teachingSubjects,
    });

    int? classId;
    String? objectType;
    String? name;
    String? nameEn;
    String? schoolyear;
    int? schoolType;
    int? isInstructor;
    List<TeachingSubject>? teachingSubjects;

    factory DataMyExam.fromJson(Map<String, dynamic> json) => DataMyExam(
        classId: json["class_id"],
        objectType: json["object_type"],
        name: json["name"],
        nameEn: json["name_en"],
        schoolyear: json["schoolyear"],
        schoolType: json["school_type"],
        isInstructor: json["is_instructor"],
        teachingSubjects: List<TeachingSubject>.from(json["teaching_subjects"].map((x) => TeachingSubject.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "class_id": classId,
        "object_type": objectType,
        "name": name,
        "name_en": nameEn,
        "schoolyear": schoolyear,
        "school_type": schoolType,
        "is_instructor": isInstructor,
        "teaching_subjects": List<dynamic>.from(teachingSubjects!.map((x) => x.toJson())),
    };
}

class TeachingSubject {
    TeachingSubject({
        this.scheduleId,
        this.stratTime,
        this.endTime,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectColor,
        this.attendance,
        this.examDate,
        this.id,
        this.type,
        this.month,
        this.monthName,
        this.semester,
        this.description,
        this.isNoExam,
        this.isApprove,
        this.approvedBy,
        this.approvedAt,
        this.isCheckScoreEnter,
    });

    int? scheduleId;
    String? stratTime;
    String? endTime;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectColor;
    Attendance? attendance;
    String? examDate;
    int? id;
    int? type;
    int? month;
    String? monthName;
    String? semester;
    String? description;
    int? isNoExam;
    int? isApprove;
    String? approvedBy;
    String? approvedAt;
    int? isCheckScoreEnter;

    factory TeachingSubject.fromJson(Map<String, dynamic> json) => TeachingSubject(
        scheduleId: json["schedule_id"],
        stratTime: json["start_time"],
        endTime: json["end_time"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectColor: json["subject_color"],
        attendance: Attendance.fromJson(json["attendance"]),
        examDate: json["exam_date"],
        id: json["id"],
        type: json["type"],
        month: json["month"],
        monthName: json["month_name"],
        semester: json["semester"],
        description: json["description"],
        isNoExam: json["is_no_exam"],
        isApprove: json["is_approve"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
        isCheckScoreEnter: json["is_check_score_enter"],
    );

    Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "start_time": stratTime,
        "end_time": endTime,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_color": subjectColor,
        "attendance": attendance!.toJson(),
        "exam_date": examDate,
        "id": id,
        "type": type,
        "month": month,
        "month_name": monthName,
        "semester": semester,
        "description": description,
        "is_no_exam": isNoExam,
        "is_approve": isApprove,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
        "is_check_score_enter": isCheckScoreEnter,
    };
}

class Attendance {
    Attendance({
        this.isCheckAttendance,
        this.attendanceTaker,
        this.totalStudent,
        this.present,
        this.absent,
        this.late,
        this.permission,
    });

    int? isCheckAttendance;
    String? attendanceTaker;
    int? totalStudent;
    int? present;
    int? absent;
    int? late;
    int? permission;

    factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        isCheckAttendance: json["is_check_attendance"],
        attendanceTaker: json["attendance_taker"],
        totalStudent: json["total_student"],
        present: json["present"],
        absent: json["absent"],
        late: json["late"],
        permission: json["permission"],
    );

    Map<String, dynamic> toJson() => {
        "is_check_attendance": isCheckAttendance,
        "attendance_taker": attendanceTaker,
        "total_student": totalStudent,
        "present": present,
        "absent": absent,
        "late": late,
        "permission": permission,
    };
}
