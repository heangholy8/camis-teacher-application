// To parse this JSON data, do
//
//     final studentAffairLevelModel = studentAffairLevelModelFromJson(jsonString);

import 'dart:convert';

StudentAffairLevelModel studentAffairLevelModelFromJson(String str) => StudentAffairLevelModel.fromJson(json.decode(str));

String studentAffairLevelModelToJson(StudentAffairLevelModel data) => json.encode(data.toJson());

class StudentAffairLevelModel {
    int? total;
    int? currentPage;
    bool? hasMorePages;
    List<Datum>? data;

    StudentAffairLevelModel({
        this.total,
        this.currentPage,
        this.hasMorePages,
        this.data,
    });

    factory StudentAffairLevelModel.fromJson(Map<String, dynamic> json) => StudentAffairLevelModel(
        total: json["total"],
        currentPage: json["current_page"],
        hasMorePages: json["hasMorePages"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "current_page": currentPage,
        "hasMorePages": hasMorePages,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    int? subjectId;
    String? subjectName;
    List<Class>? classes;

    Datum({
        this.subjectId,
        this.subjectName,
        this.classes,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        classes: List<Class>.from(json["classes"].map((x) => Class.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "subject_id": subjectId,
        "subject_name": subjectName,
        "classes": List<dynamic>.from(classes!.map((x) => x.toJson())),
    };
}

class Class {
    int? id;
    String ?className;
    int? isCheckScoreEnter;
    String? scoreEnterStatusName;
    String? month;
    String? semester;
    String? type;
    SubjectExam? subjectExam;

    Class({
        this.id,
        this.className,
        this.isCheckScoreEnter,
        this.scoreEnterStatusName,
        this.month,
        this.semester,
        this.type,
        this.subjectExam,
    });

    factory Class.fromJson(Map<String, dynamic> json) => Class(
        id: json["id"],
        className: json["class_name"],
        isCheckScoreEnter: json["is_check_score_enter"],
        scoreEnterStatusName: json["score_enter_status_name"],
        month: json["month"],
        semester: json["semester"],
        type: json["type"],
        subjectExam: SubjectExam.fromJson(json["subject_exam"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "class_name": className,
        "is_check_score_enter": isCheckScoreEnter,
        "score_enter_status_name": scoreEnterStatusName,
        "month": month,
        "semester": semester,
        "type": type,
        "subject_exam": subjectExam?.toJson(),
    };
}

class SubjectExam {
    int? id;
    String? examDate;
    int? type;
    dynamic month;
    String? semester;
    int? isNoExam;
    int? isApprove;
    String? approvedBy;
    String? approvedAt;

    SubjectExam({
        this.id,
        this.examDate,
        this.type,
        this.month,
        this.semester,
        this.isNoExam,
        this.isApprove,
        this.approvedBy,
        this.approvedAt,
    });

    factory SubjectExam.fromJson(Map<String, dynamic> json) => SubjectExam(
        id: json["id"],
        examDate: json["exam_date"],
        type: json["type"],
        month: json["month"],
        semester: json["semester"],
        isNoExam: json["is_no_exam"],
        isApprove: json["is_approve"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "exam_date": examDate,
        "type": type,
        "month": month,
        "semester": semester,
        "is_no_exam": isNoExam,
        "is_approve": isApprove,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
    };
}
