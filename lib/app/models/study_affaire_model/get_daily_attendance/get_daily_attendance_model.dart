// To parse this JSON data, do
//
//     final getDailyAttendanceAffaireModel = getDailyAttendanceAffaireModelFromJson(jsonString);

import 'dart:convert';

GetDailyAttendanceAffaireModel getDailyAttendanceAffaireModelFromJson(String str) => GetDailyAttendanceAffaireModel.fromJson(json.decode(str));

String getDailyAttendanceAffaireModelToJson(GetDailyAttendanceAffaireModel data) => json.encode(data.toJson());

class GetDailyAttendanceAffaireModel {
    List<DataDailyAttendanceAffaire>? data;
    bool? status;

    GetDailyAttendanceAffaireModel({
        this.data,
        this.status,
    });

    factory GetDailyAttendanceAffaireModel.fromJson(Map<String, dynamic> json) => GetDailyAttendanceAffaireModel(
        data: List<DataDailyAttendanceAffaire>.from(json["data"].map((x) => DataDailyAttendanceAffaire.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "status": status,
    };
}

class DataDailyAttendanceAffaire {
    String? time;
    String? startTime;
    String? endTime;
    String? shortDay;
    dynamic date;
    bool? isCurrent;
    List<Grade>? grade;

    DataDailyAttendanceAffaire({
        this.time,
        this.startTime,
        this.endTime,
        this.shortDay,
        this.date,
        this.isCurrent,
        this.grade,
    });

    factory DataDailyAttendanceAffaire.fromJson(Map<String, dynamic> json) => DataDailyAttendanceAffaire(
        time: json["time"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        shortDay: json["shortDay"],
        date: json["date"],
        isCurrent: json["is_current"],
        grade: List<Grade>.from(json["grade"].map((x) => Grade.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "time": time,
        "start_time": startTime,
        "end_time": endTime,
        "shortDay": shortDay,
        "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "is_current": isCurrent,
        "grade": List<dynamic>.from(grade!.map((x) => x.toJson())),
    };
}

class Grade {
    String? grade;
    List<Class>? classes;

    Grade({
        this.grade,
        this.classes,
    });

    factory Grade.fromJson(Map<String, dynamic> json) => Grade(
        grade: json["grade"],
        classes: List<Class>.from(json["classes"].map((x) => Class.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "grade": grade,
        "classes": List<dynamic>.from(classes!.map((x) => x.toJson())),
    };
}

class Class {
    int? id;
    dynamic name;
    dynamic subjectName;
    dynamic scheduleId;
    bool? isTakeAttendance;

    Class({
        this.id,
        this.name,
        this.subjectName,
        this.scheduleId,
        this.isTakeAttendance,
    });

    factory Class.fromJson(Map<String, dynamic> json) => Class(
        id: json["id"],
        name: json["name"],
        subjectName: json["subject_name"],
        scheduleId: json["schedule_id"],
        isTakeAttendance: json["is_take_attendance"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "subject_name": subjectName,
        "schedule_id": scheduleId,
        "is_take_attendance": isTakeAttendance,
    };
}
