// To parse this JSON data, do
//
//     final attendanceTeacherListModel = attendanceTeacherListModelFromJson(jsonString);

import 'dart:convert';

AttendanceTeacherListModel attendanceTeacherListModelFromJson(String str) => AttendanceTeacherListModel.fromJson(json.decode(str));

String attendanceTeacherListModelToJson(AttendanceTeacherListModel data) => json.encode(data.toJson());

class AttendanceTeacherListModel {
    bool? status;
    String? message;
    List<DataTeacherAttebdabce>? data;

    AttendanceTeacherListModel({
        this.status,
        this.message,
        this.data,
    });

    factory AttendanceTeacherListModel.fromJson(Map<String, dynamic> json) => AttendanceTeacherListModel(
        status: json["status"],
        message: json["message"],
        data: List<DataTeacherAttebdabce>.from(json["data"].map((x) => DataTeacherAttebdabce.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataTeacherAttebdabce {
    String? time;
    String? startTime;
    String? endTime;
    String? shortDay;
    String? date;
    bool? isCurrent;
    List<Grade>? grade;

    DataTeacherAttebdabce({
        this.time,
        this.startTime,
        this.endTime,
        this.shortDay,
        this.date,
        this.isCurrent,
        this.grade,
    });

    factory DataTeacherAttebdabce.fromJson(Map<String, dynamic> json) => DataTeacherAttebdabce(
        time: json["time"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        shortDay: json["shortDay"],
        date: json["date"],
        isCurrent: json["is_current"],
        grade: List<Grade>.from(json["grade"].map((x) => Grade.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "time": time,
        "start_time": startTime,
        "end_time": endTime,
        "shortDay": shortDay,
        "date": date,
        "is_current": isCurrent,
        "grade": List<dynamic>.from(grade!.map((x) => x.toJson())),
    };
}

class Grade {
    dynamic grade;
    List<Class>? classes;

    Grade({
        this.grade,
        this.classes,
    });

    factory Grade.fromJson(Map<String, dynamic> json) => Grade(
        grade: json["grade"],
        classes: List<Class>.from(json["classes"].map((x) => Class.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "grade": grade,
        "classes": List<dynamic>.from(classes!.map((x) => x.toJson())),
    };
}

class Class {
    int? id;
    String? name;
    String? subjectName;
    dynamic subjectId;
    int? scheduleId;
    dynamic timeTableSettingId;
    String? teacherId;
    String? teacherName;
    String? teacherNameEn;
    String? teacherPhone;
    String? staffPresentStatus;
    dynamic staffPresentData;
    dynamic staffPermissionData;

    Class({
        this.id,
        this.name,
        this.subjectName,
        this.scheduleId,
        this.timeTableSettingId,
        this.teacherId,
        this.teacherName,
        this.teacherNameEn,
        this.teacherPhone,
        this.staffPresentStatus,
        this.staffPresentData,
        this.staffPermissionData,
        this.subjectId,
    });

    factory Class.fromJson(Map<String, dynamic> json) => Class(
        id: json["id"],
        name: json["name"],
        subjectName: json["subject_name"],
        subjectId: json["subject_id"],
        scheduleId: json["schedule_id"],
        timeTableSettingId: json["time_table_setting_id"],
        teacherId: json["teacher_id"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherPhone: json["teacher_phone"],
        staffPresentStatus: json["staff_present_status"],
        staffPresentData: json["staff_present_data"],
        staffPermissionData: json["staff_permission_data"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "subject_name": subjectName,
        "subject_id": subjectId,
        "schedule_id": scheduleId,
        "time_table_setting_id": timeTableSettingId,
        "teacher_id": teacherId,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_phone": teacherPhone,
        "staff_present_status": staffPresentStatus,
        "staff_present_data": staffPresentData,
        "staff_permission_data": staffPermissionData,
    };
}