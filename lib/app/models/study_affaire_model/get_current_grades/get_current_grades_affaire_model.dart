// To parse this JSON data, do
//
//     final getCurrentGradeAffaireModel = getCurrentGradeAffaireModelFromJson(jsonString);

import 'dart:convert';

GetCurrentGradeAffaireModel getCurrentGradeAffaireModelFromJson(String str) => GetCurrentGradeAffaireModel.fromJson(json.decode(str));

String getCurrentGradeAffaireModelToJson(GetCurrentGradeAffaireModel data) => json.encode(data.toJson());

class GetCurrentGradeAffaireModel {
    List<DataAllGradeAffaire>? data;
    bool? status;

    GetCurrentGradeAffaireModel({
        this.data,
        this.status,
    });

    factory GetCurrentGradeAffaireModel.fromJson(Map<String, dynamic> json) => GetCurrentGradeAffaireModel(
        data: List<DataAllGradeAffaire>.from(json["data"].map((x) => DataAllGradeAffaire.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "status": status,
    };
}

class DataAllGradeAffaire {
    dynamic level;
    List<Class>? classes;

    DataAllGradeAffaire({
        this.level,
        this.classes,
    });

    factory DataAllGradeAffaire.fromJson(Map<String, dynamic> json) => DataAllGradeAffaire(
        level: json["level"],
        classes: List<Class>.from(json["classes"].map((x) => Class.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "level": level,
        "classes": List<dynamic>.from(classes!.map((x) => x.toJson())),
    };
}

class Class {
    int? classId;
    String? className;

    Class({
        this.classId,
        this.className,
    });

    factory Class.fromJson(Map<String, dynamic> json) => Class(
        classId: json["class_id"],
        className: json["class_name"],
    );

    Map<String, dynamic> toJson() => {
        "class_id": classId,
        "class_name": className,
    };
}
