class ApprovedRespone {
  bool? status;
  ApprovedData? data;
  String? message;

  ApprovedRespone({this.status, this.data, this.message});

  ApprovedRespone.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new ApprovedData.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class ApprovedData {
  int? id;
  int? subjectId;
  String? subjectName;
  String? subjectNameEn;
  String? subjectColor;
  int? classId;
  String? objectType;
  String? name;
  String? nameEn;
  int? scheduleId;
  int? type;
  String? examDate;
  int? month;
  String? monthName;
  String? semester;
  String? description;
  int? isApprove;
  String? approvedBy;
  String? approvedAt;
  List<Medias>? medias;

  ApprovedData(
      {this.id,
      this.subjectId,
      this.subjectName,
      this.subjectNameEn,
      this.subjectColor,
      this.classId,
      this.objectType,
      this.name,
      this.nameEn,
      this.scheduleId,
      this.type,
      this.examDate,
      this.month,
      this.monthName,
      this.semester,
      this.description,
      this.isApprove,
      this.approvedBy,
      this.approvedAt,
      this.medias});

  ApprovedData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    subjectId = json['subject_id'];
    subjectName = json['subject_name'];
    subjectNameEn = json['subject_name_en'];
    subjectColor = json['subject_color'];
    classId = json['class_id'];
    objectType = json['object_type'];
    name = json['name'];
    nameEn = json['name_en'];
    scheduleId = json['schedule_id'];
    type = json['type'];
    examDate = json['exam_date'];
    month = json['month'];
    monthName = json['month_name'];
    semester = json['semester'];
    description = json['description'];
    isApprove = json['is_approve'];
    approvedBy = json['approved_by'];
    approvedAt = json['approved_at'];
    if (json['medias'] != null) {
      medias = <Medias>[];
      json['medias'].forEach((v) {
        medias!.add(new Medias.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['subject_id'] = this.subjectId;
    data['subject_name'] = this.subjectName;
    data['subject_name_en'] = this.subjectNameEn;
    data['subject_color'] = this.subjectColor;
    data['class_id'] = this.classId;
    data['object_type'] = this.objectType;
    data['name'] = this.name;
    data['name_en'] = this.nameEn;
    data['schedule_id'] = this.scheduleId;
    data['type'] = this.type;
    data['exam_date'] = this.examDate;
    data['month'] = this.month;
    data['month_name'] = this.monthName;
    data['semester'] = this.semester;
    data['description'] = this.description;
    data['is_approve'] = this.isApprove;
    data['approved_by'] = this.approvedBy;
    data['approved_at'] = this.approvedAt;
    if (this.medias != null) {
      data['medias'] = this.medias!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Medias {
  int? id;
  String? fileName;
  String? fileSize;
  String? fileType;
  String? fileShow;
  String? fileThumbnail;
  String? fileIndex;
  String? fileArea;
  String? objectId;
  String? schoolUrl;
  String? postDate;
  int? isGdrive;
  String? fileGdriveId;
  int? isS3;

  Medias(
      {this.id,
      this.fileName,
      this.fileSize,
      this.fileType,
      this.fileShow,
      this.fileThumbnail,
      this.fileIndex,
      this.fileArea,
      this.objectId,
      this.schoolUrl,
      this.postDate,
      this.isGdrive,
      this.fileGdriveId,
      this.isS3});

  Medias.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fileName = json['file_name'];
    fileSize = json['file_size'];
    fileType = json['file_type'];
    fileShow = json['file_show'];
    fileThumbnail = json['file_thumbnail'];
    fileIndex = json['file_index'];
    fileArea = json['file_area'];
    objectId = json['object_id'];
    schoolUrl = json['school_url'];
    postDate = json['post_date'];
    isGdrive = json['is_gdrive'];
    fileGdriveId = json['file_gdrive_id'];
    isS3 = json['is_s3'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['file_name'] = this.fileName;
    data['file_size'] = this.fileSize;
    data['file_type'] = this.fileType;
    data['file_show'] = this.fileShow;
    data['file_thumbnail'] = this.fileThumbnail;
    data['file_index'] = this.fileIndex;
    data['file_area'] = this.fileArea;
    data['object_id'] = this.objectId;
    data['school_url'] = this.schoolUrl;
    data['post_date'] = this.postDate;
    data['is_gdrive'] = this.isGdrive;
    data['file_gdrive_id'] = this.fileGdriveId;
    data['is_s3'] = this.isS3;
    return data;
  }
}
