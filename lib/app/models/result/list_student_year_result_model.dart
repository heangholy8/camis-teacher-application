// To parse this JSON data, do
//
//     final studentYearResultModel = studentYearResultModelFromJson(jsonString);

import 'dart:convert';

StudentYearResultModel studentYearResultModelFromJson(String str) => StudentYearResultModel.fromJson(json.decode(str));

String studentYearResultModelToJson(StudentYearResultModel data) => json.encode(data.toJson());

class StudentYearResultModel {
    List<DataYear>? data;
    bool? status;

    StudentYearResultModel({
        this.data,
        this.status,
    });

    factory StudentYearResultModel.fromJson(Map<String, dynamic> json) => StudentYearResultModel(
        data: List<DataYear>.from(json["data"].map((x) => DataYear.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "status": status,
    };
}

class DataYear {
    String? studentId;
    dynamic studentSchoolyearId;
    String? studentSchoolId;
    int? classId;
    String? className;
    String? classNameEn;
    Instructor? instructor;
    String? schoolyearId;
    String? name;
    String? nameEn;
    dynamic gender;
    dynamic dateOfBirth;
    dynamic phone;
    ProfileMedia? profileMedia;
    ResultYearly? resultYearly;

    DataYear({
        this.studentId,
        this.studentSchoolyearId,
        this.studentSchoolId,
        this.classId,
        this.className,
        this.classNameEn,
        this.instructor,
        this.schoolyearId,
        this.name,
        this.nameEn,
        this.gender,
        this.dateOfBirth,
        this.phone,
        this.profileMedia,
        this.resultYearly,
    });

    factory DataYear.fromJson(Map<String, dynamic> json) => DataYear(
        studentId: json["student_id"],
        studentSchoolyearId: json["student_schoolyear_id"],
        studentSchoolId: json["student_school_id"],
        classId: json["class_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        instructor: Instructor.fromJson(json["instructor"]),
        schoolyearId: json["schoolyear_id"],
        name: json["name"],
        nameEn: json["name_en"],
        gender: json["gender"],
        dateOfBirth: json["date_of_birth"],
        phone: json["phone"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        resultYearly: json["result_yearly"] == null?null:ResultYearly.fromJson(json["result_yearly"]),
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "student_schoolyear_id": studentSchoolyearId,
        "student_school_id": studentSchoolId,
        "class_id": classId,
        "class_name": className,
        "class_name_en": classNameEn,
        "instructor": instructor!.toJson(),
        "schoolyear_id": schoolyearId,
        "name": name,
        "name_en": nameEn,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "phone": phone,
        "profileMedia": profileMedia!.toJson(),
        "result_yearly": resultYearly==null?null:resultYearly!.toJson(),
    };
}

class Instructor {
    String? id;
    String? schoolCode;
    dynamic code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    dynamic dob;
    dynamic phone;
    dynamic isVerify;
    String? email;
    String? address;
    String? framework;
    dynamic prokas;
    dynamic role;
    ProfileMedia? profileMedia;
    String? qrUrl;

    Instructor({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.isVerify,
        this.email,
        this.address,
        this.framework,
        this.prokas,
        this.role,
        this.profileMedia,
        this.qrUrl,
    });

    factory Instructor.fromJson(Map<String, dynamic> json) => Instructor(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        isVerify: json["is_verify"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"],
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "is_verify": isVerify,
        "email": email,
        "address": address,
        "framework": framework,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class ProfileMedia {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class ResultYearly {
    dynamic id;
    dynamic maxAvgScore;
    dynamic avgScore;
    dynamic rank;
    dynamic teacherComment;
    dynamic recommendation;
    dynamic behavior;
    dynamic absenceTotal;
    dynamic absenceWithPermission;
    dynamic absenceWithoutPermission;
    dynamic absenceWithoutLate;
    dynamic grading;
    dynamic gradingEn;
    dynamic letterGrade;
    dynamic gradePoints;
    dynamic isFail;
    dynamic morality;
    dynamic bangkeunPhal;
    dynamic health;
    dynamic firstSemesterTotalExamScore;
    dynamic firstSemesterAvgExamScore;
    dynamic firstSemesterMonthAvgScore;
    dynamic firstSemesterAvg;
    dynamic firstSemesterRank;
    dynamic firstSemesterLetterGrade;
    dynamic secondSemesterTotalExamScore;
    dynamic secondSemesterAvgExamScore;
    dynamic secondSemesterMonthAvgScore;
    dynamic secondSemesterAvg;
    dynamic secondSemesterRank;
    dynamic secondSemesterLetterGrade;

    ResultYearly({
        this.id,
        this.maxAvgScore,
        this.avgScore,
        this.rank,
        this.teacherComment,
        this.recommendation,
        this.behavior,
        this.absenceTotal,
        this.absenceWithPermission,
        this.absenceWithoutPermission,
        this.absenceWithoutLate,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
        this.morality,
        this.bangkeunPhal,
        this.health,
        this.firstSemesterTotalExamScore,
        this.firstSemesterAvgExamScore,
        this.firstSemesterMonthAvgScore,
        this.firstSemesterAvg,
        this.firstSemesterRank,
        this.firstSemesterLetterGrade,
        this.secondSemesterTotalExamScore,
        this.secondSemesterAvgExamScore,
        this.secondSemesterMonthAvgScore,
        this.secondSemesterAvg,
        this.secondSemesterRank,
        this.secondSemesterLetterGrade,
    });

    factory ResultYearly.fromJson(Map<String, dynamic> json) => ResultYearly(
        id: json["id"],
        maxAvgScore: json["max_avg_score"],
        avgScore: json["avg_score"],
        rank: json["rank"],
        teacherComment: json["teacher_comment"],
        recommendation: json["recommendation"],
        behavior: json["behavior"],
        absenceTotal: json["absence_total"],
        absenceWithPermission: json["absence_with_permission"],
        absenceWithoutPermission: json["absence_without_permission"],
        absenceWithoutLate: json["absence_without_late"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
        morality: json["morality"],
        bangkeunPhal: json["bangkeun_phal"],
        health: json["health"],
        firstSemesterTotalExamScore: json["first_semester_total_exam_score"],
        firstSemesterAvgExamScore: json["first_semester_avg_exam_score"],
        firstSemesterMonthAvgScore: json["first_semester_month_avg_score"],
        firstSemesterAvg: json["first_semester_avg"],
        firstSemesterRank: json["first_semester_rank"],
        firstSemesterLetterGrade: json["first_semester_letter_grade"],
        secondSemesterTotalExamScore: json["second_semester_total_exam_score"],
        secondSemesterAvgExamScore: json["second_semester_avg_exam_score"],
        secondSemesterMonthAvgScore: json["second_semester_month_avg_score"],
        secondSemesterAvg: json["second_semester_avg"],
        secondSemesterRank: json["second_semester_rank"],
        secondSemesterLetterGrade: json["second_semester_letter_grade"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "max_avg_score": maxAvgScore,
        "avg_score": avgScore,
        "rank": rank,
        "teacher_comment": teacherComment,
        "recommendation": recommendation,
        "behavior": behavior,
        "absence_total": absenceTotal,
        "absence_with_permission": absenceWithPermission,
        "absence_without_permission": absenceWithoutPermission,
        "absence_without_late": absenceWithoutLate,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
        "morality": morality,
        "bangkeun_phal": bangkeunPhal,
        "health": health,
        "first_semester_total_exam_score": firstSemesterTotalExamScore,
        "first_semester_avg_exam_score": firstSemesterAvgExamScore,
        "first_semester_month_avg_score": firstSemesterMonthAvgScore,
        "first_semester_avg": firstSemesterAvg,
        "first_semester_rank": firstSemesterRank,
        "first_semester_letter_grade": firstSemesterLetterGrade,
        "second_semester_total_exam_score": secondSemesterTotalExamScore,
        "second_semester_avg_exam_score": secondSemesterAvgExamScore,
        "second_semester_month_avg_score": secondSemesterMonthAvgScore,
        "second_semester_avg": secondSemesterAvg,
        "second_semester_rank": secondSemesterRank,
        "second_semester_letter_grade": secondSemesterLetterGrade,
    };
}
