import 'dart:convert';

StudentSemesterResultModel studentSemesterResultModelFromJson(String str) => StudentSemesterResultModel.fromJson(json.decode(str));

String studentSemesterResultModelToJson(StudentSemesterResultModel data) => json.encode(data.toJson());

class StudentSemesterResultModel {
    List<DataSemesterResult>? data;
    bool? status;

    StudentSemesterResultModel({
        this.data,
        this.status,
    });

    factory StudentSemesterResultModel.fromJson(Map<String, dynamic> json) => StudentSemesterResultModel(
        data: List<DataSemesterResult>.from(json["data"].map((x) => DataSemesterResult.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "status": status,
    };
}

class DataSemesterResult {
    String? studentId;
    dynamic studentSchoolyearId;
    String? studentSchoolId;
    int? classId;
    String? className;
    String? classNameEn;
    Instructor? instructor;
    String? schoolyearId;
    String? name;
    String? nameEn;
    dynamic gender;
    dynamic dateOfBirth;
    dynamic phone;
    ProfileMedia? profileMedia;
    ResultSemester? resultSemester;
    ResultSemesterExam? resultSemesterExam;
    List<AllSubjectSemesterExamResult>? allSubjectSemesterExamResult;

    DataSemesterResult({
        this.studentId,
        this.studentSchoolyearId,
        this.studentSchoolId,
        this.classId,
        this.className,
        this.classNameEn,
        this.instructor,
        this.schoolyearId,
        this.name,
        this.nameEn,
        this.gender,
        this.dateOfBirth,
        this.phone,
        this.profileMedia,
        this.resultSemester,
        this.resultSemesterExam,
        this.allSubjectSemesterExamResult,
    });

    factory DataSemesterResult.fromJson(Map<String, dynamic> json) => DataSemesterResult(
        studentId: json["student_id"],
        studentSchoolyearId: json["student_schoolyear_id"].toDouble(),
        studentSchoolId: json["student_school_id"],
        classId: json["class_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        instructor: Instructor.fromJson(json["instructor"]),
        schoolyearId: json["schoolyear_id"],
        name: json["name"],
        nameEn: json["name_en"],
        gender: json["gender"],
        dateOfBirth: json["date_of_birth"],
        phone: json["phone"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        resultSemester: json["result_semester"] ==null?null:ResultSemester.fromJson(json["result_semester"]),
        resultSemesterExam:json["result_semester_exam"]==null?null:ResultSemesterExam.fromJson(json["result_semester_exam"]),
        allSubjectSemesterExamResult: List<AllSubjectSemesterExamResult>.from(json["all_subject_semester_exam_result"].map((x) => AllSubjectSemesterExamResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "student_schoolyear_id": studentSchoolyearId,
        "student_school_id": studentSchoolId,
        "class_id": classId,
        "class_name": className,
        "class_name_en": classNameEn,
        "instructor": instructor!.toJson(),
        "schoolyear_id": schoolyearId,
        "name": name,
        "name_en": nameEn,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "phone": phone,
        "profileMedia": profileMedia!.toJson(),
        "result_semester": resultSemester==null?null:resultSemester!.toJson(),
        "result_semester_exam":resultSemesterExam==null?null:resultSemesterExam!.toJson(),
        "all_subject_semester_exam_result": List<dynamic>.from(allSubjectSemesterExamResult!.map((x) => x.toJson())),
    };
}

class AllSubjectSemesterExamResult {
    String? id;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectShort;
    dynamic subjectScoreMin;
    dynamic subjectScoreMax;
    dynamic score;
    dynamic absentExam;
    dynamic teacherComment;
    dynamic complementaryScore;
    dynamic includeInEvaluation;
    dynamic rank;
    int? month;
    String? term;
    dynamic grading;
    dynamic gradingEn;
    dynamic letterGrade;
    dynamic gradePoints;
    dynamic isFail;

    AllSubjectSemesterExamResult({
        this.id,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectShort,
        this.subjectScoreMin,
        this.subjectScoreMax,
        this.score,
        this.absentExam,
        this.teacherComment,
        this.complementaryScore,
        this.includeInEvaluation,
        this.rank,
        this.month,
        this.term,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
    });

    factory AllSubjectSemesterExamResult.fromJson(Map<String, dynamic> json) => AllSubjectSemesterExamResult(
        id: json["id"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectShort: json["subject_short"],
        subjectScoreMin: json["subject_score_min"],
        subjectScoreMax: json["subject_score_max"],
        score: json["score"],
        absentExam: json["absent_exam"],
        teacherComment: json["teacher_comment"],
        complementaryScore: json["complementary_score"],
        includeInEvaluation: json["include_in_evaluation"],
        rank: json["rank"],
        month: json["month"],
        term: json["term"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_short": subjectShort,
        "subject_score_min": subjectScoreMin,
        "subject_score_max": subjectScoreMax,
        "score": score,
        "absent_exam": absentExam,
        "teacher_comment": teacherComment,
        "complementary_score": complementaryScore,
        "include_in_evaluation": includeInEvaluation,
        "rank": rank,
        "month": month,
        "term": term,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
    };
}

class Instructor {
    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String? genderName;
    dynamic dob;
    dynamic phone;
    dynamic isVerify;
    String? email;
    String? address;
    dynamic framework;
    dynamic prokas;
    dynamic role;
    ProfileMedia? profileMedia;
    String? qrUrl;

    Instructor({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.isVerify,
        this.email,
        this.address,
        this.framework,
        this.prokas,
        this.role,
        this.profileMedia,
        this.qrUrl,
    });

    factory Instructor.fromJson(Map<String, dynamic> json) => Instructor(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        isVerify: json["is_verify"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"],
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "is_verify": isVerify,
        "email": email,
        "address": address,
        "framework": framework,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class ProfileMedia {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    dynamic postDate;
    dynamic isGdrive;
    String? fileGdriveId;
    dynamic isS3;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class ResultSemester {
    int? id;
    dynamic maxAvgScore;
    dynamic avgScore;
    dynamic examAvg;
    dynamic monthAvg;
    dynamic rank;
    String? term;
    dynamic teacherComment;
    dynamic recommendation;
    dynamic behavior;
    dynamic absenceTotal;
    dynamic absenceWithPermission;
    dynamic absenceWithoutPermission;
    dynamic absenceWithLate;
    dynamic grading;
    dynamic gradingEn;
    dynamic letterGrade;
    dynamic gradePoints;
    dynamic isFail;
    dynamic morality;
    dynamic bangkeunPhal;
    dynamic health;

    ResultSemester({
        this.id,
        this.maxAvgScore,
        this.avgScore,
        this.examAvg,
        this.monthAvg,
        this.rank,
        this.term,
        this.teacherComment,
        this.recommendation,
        this.behavior,
        this.absenceTotal,
        this.absenceWithPermission,
        this.absenceWithoutPermission,
        this.absenceWithLate,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
        this.morality,
        this.bangkeunPhal,
        this.health,
    });

    factory ResultSemester.fromJson(Map<String, dynamic> json) => ResultSemester(
        id: json["id"],
        maxAvgScore: json["max_avg_score"],
        avgScore: json["avg_score"].toDouble(),
        examAvg: json["exam_avg"].toDouble(),
        monthAvg: json["month_avg"].toDouble(),
        rank: json["rank"],
        term: json["term"],
        teacherComment: json["teacher_comment"],
        recommendation: json["recommendation"],
        behavior: json["behavior"],
        absenceTotal: json["absence_total"],
        absenceWithPermission: json["absence_with_permission"],
        absenceWithoutPermission: json["absence_without_permission"],
        absenceWithLate: json["absence_with_late"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
        morality: json["morality"],
        bangkeunPhal: json["bangkeun_phal"],
        health: json["health"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "max_avg_score": maxAvgScore,
        "avg_score": avgScore,
        "exam_avg": examAvg,
        "month_avg": monthAvg,
        "rank": rank,
        "term": term,
        "teacher_comment": teacherComment,
        "recommendation": recommendation,
        "behavior": behavior,
        "absence_total": absenceTotal,
        "absence_with_permission": absenceWithPermission,
        "absence_without_permission": absenceWithoutPermission,
        "absence_with_late": absenceWithLate,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
        "morality": morality,
        "bangkeun_phal": bangkeunPhal,
        "health": health,
    };
}

class ResultSemesterExam {
    int? id;
    dynamic totalScore;
    dynamic totalCoeff;
    dynamic maxAvgScore;
    dynamic avgScore;
    dynamic rank;
    String? term;
    dynamic teacherComment;
    dynamic grading;
    dynamic gradingEn;
    dynamic letterGrade;
    dynamic gradePoints;
    dynamic isFail;

    ResultSemesterExam({
        this.id,
        this.totalScore,
        this.totalCoeff,
        this.maxAvgScore,
        this.avgScore,
        this.rank,
        this.term,
        this.teacherComment,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
    });

    factory ResultSemesterExam.fromJson(Map<String, dynamic> json) => ResultSemesterExam(
        id: json["id"],
        totalScore: json["total_score"],
        totalCoeff: json["total_coeff"],
        maxAvgScore: json["max_avg_score"],
        avgScore: json["avg_score"].toDouble(),
        rank: json["rank"],
        term: json["term"],
        teacherComment: json["teacher_comment"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "total_score": totalScore,
        "total_coeff": totalCoeff,
        "max_avg_score": maxAvgScore,
        "avg_score": avgScore,
        "rank": rank,
        "term": term,
        "teacher_comment": teacherComment,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
    };
}
