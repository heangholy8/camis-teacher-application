// To parse this JSON data, do
//
//     final studentMonthlyResultModel = studentMonthlyResultModelFromJson(jsonString);

import 'dart:convert';

StudentMonthlyResultModel studentMonthlyResultModelFromJson(String str) => StudentMonthlyResultModel.fromJson(json.decode(str));

String studentMonthlyResultModelToJson(StudentMonthlyResultModel data) => json.encode(data.toJson());

class StudentMonthlyResultModel {
    List<DataMonthlyResult>? data;
    bool? status;

    StudentMonthlyResultModel({
        this.data,
        this.status,
    });

    factory StudentMonthlyResultModel.fromJson(Map<String, dynamic> json) => StudentMonthlyResultModel(
        data: List<DataMonthlyResult>.from(json["data"].map((x) => DataMonthlyResult.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "status": status,
    };
}

class DataMonthlyResult {
    String? studentId;
    double? studentSchoolyearId;
    String? studentSchoolId;
    int? classId;
    String? className;
    String? classNameEn;
    Instructor? instructor;
    String? schoolyearId;
    String? name;
    String? nameEn;
    int? gender;
    String? dateOfBirth;
    dynamic phone;
    ProfileMedia? profileMedia;
    ResultMonthly? resultMonthly;
    List<AllSubjectResult>? allSubjectResult;

    DataMonthlyResult({
        this.studentId,
        this.studentSchoolyearId,
        this.studentSchoolId,
        this.classId,
        this.className,
        this.classNameEn,
        this.instructor,
        this.schoolyearId,
        this.name,
        this.nameEn,
        this.gender,
        this.dateOfBirth,
        this.phone,
        this.profileMedia,
        this.resultMonthly,
        this.allSubjectResult,
    });

    factory DataMonthlyResult.fromJson(Map<String, dynamic> json) => DataMonthlyResult(
        studentId: json["student_id"],
        studentSchoolyearId: json["student_schoolyear_id"].toDouble(),
        studentSchoolId: json["student_school_id"],
        classId: json["class_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        instructor: Instructor.fromJson(json["instructor"]),
        schoolyearId: json["schoolyear_id"],
        name: json["name"],
        nameEn: json["name_en"],
        gender: json["gender"],
        dateOfBirth: json["date_of_birth"],
        phone: json["phone"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        resultMonthly: json["result_monthly"] ==null?null: ResultMonthly.fromJson(json["result_monthly"]),
        allSubjectResult: List<AllSubjectResult>.from(json["all_subject_result"].map((x) => AllSubjectResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "student_schoolyear_id": studentSchoolyearId,
        "student_school_id": studentSchoolId,
        "class_id": classId,
        "class_name": className,
        "class_name_en": classNameEn,
        "instructor": instructor!.toJson(),
        "schoolyear_id": schoolyearId,
        "name": name,
        "name_en": nameEn,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "phone": phone,
        "profileMedia": profileMedia!.toJson(),
        "result_monthly": resultMonthly == null?null:resultMonthly!.toJson(),
        "all_subject_result": List<dynamic>.from(allSubjectResult!.map((x) => x.toJson())),
    };
}

class AllSubjectResult {
    String? id;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectShort;
    dynamic subjectScoreMin;
    dynamic subjectScoreMax;
    dynamic score;
    dynamic absentExam;
    dynamic teacherComment;
    dynamic complementaryScore;
    dynamic includeInEvaluation;
    dynamic rank;
    int? month;
    String? term;
    dynamic grading;
    dynamic gradingEn;
    dynamic letterGrade;
    dynamic gradePoints;
    dynamic isFail;

    AllSubjectResult({
        this.id,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectShort,
        this.subjectScoreMin,
        this.subjectScoreMax,
        this.score,
        this.absentExam,
        this.teacherComment,
        this.complementaryScore,
        this.includeInEvaluation,
        this.rank,
        this.month,
        this.term,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
    });

    factory AllSubjectResult.fromJson(Map<String, dynamic> json) => AllSubjectResult(
        id: json["id"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectShort: json["subject_short"],
        subjectScoreMin: json["subject_score_min"],
        subjectScoreMax: json["subject_score_max"],
        score: json["score"],
        absentExam: json["absent_exam"],
        teacherComment: json["teacher_comment"],
        complementaryScore: json["complementary_score"],
        includeInEvaluation: json["include_in_evaluation"],
        rank: json["rank"],
        month: json["month"],
        term: json["term"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_short": subjectShort,
        "subject_score_min": subjectScoreMin,
        "subject_score_max": subjectScoreMax,
        "score": score,
        "absent_exam": absentExam,
        "teacher_comment": teacherComment,
        "complementary_score": complementaryScore,
        "include_in_evaluation": includeInEvaluation,
        "rank": rank,
        "month": month,
        "term": term,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
    };
}

class Instructor {
    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String? genderName;
    dynamic dob;
    dynamic phone;
    dynamic isVerify;
    String? email;
    String? address;
    dynamic framework;
    dynamic prokas;
    dynamic role;
    ProfileMedia? profileMedia;

    Instructor({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.isVerify,
        this.email,
        this.address,
        this.framework,
        this.prokas,
        this.role,
        this.profileMedia,
    });

    factory Instructor.fromJson(Map<String, dynamic> json) => Instructor(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        isVerify: json["is_verify"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"],
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "is_verify": isVerify,
        "email": email,
        "address": address,
        "framework": framework,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
    };
}

class ProfileMedia {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    dynamic isGdrive;
    dynamic fileGdriveId;
    dynamic isS3;
    String? fileShow;
    String? fileThumbnail;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}

class ResultMonthly {
    String? id;
    dynamic performancePercentage;
    dynamic avgScore;
    dynamic maxAvgScore;
    dynamic totalScore;
    dynamic totalCoeff;
    dynamic rank;
    int? month;
    dynamic teacherComment;
    dynamic recommendation;
    dynamic behavior;
    dynamic absenceTotal;
    dynamic absenceWithPermission;
    dynamic absenceWithoutPermission;
    dynamic absenceExam;
    String? grading;
    String? gradingEn;
    dynamic letterGrade;
    dynamic gradePoints;
    dynamic isFail;

    ResultMonthly({
        this.id,
        this.performancePercentage,
        this.avgScore,
        this.maxAvgScore,
        this.totalScore,
        this.totalCoeff,
        this.rank,
        this.month,
        this.teacherComment,
        this.recommendation,
        this.behavior,
        this.absenceTotal,
        this.absenceWithPermission,
        this.absenceWithoutPermission,
        this.absenceExam,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
    });

    factory ResultMonthly.fromJson(Map<String, dynamic> json) => ResultMonthly(
        id: json["id"],
        performancePercentage: json["performance_percentage"],
        avgScore: json["avg_score"],
        maxAvgScore: json["max_avg_score"],
        totalScore: json["total_score"],
        totalCoeff: json["total_coeff"],
        rank: json["rank"],
        month: json["month"],
        teacherComment: json["teacher_comment"],
        recommendation: json["recommendation"],
        behavior: json["behavior"],
        absenceTotal: json["absence_total"],
        absenceWithPermission: json["absence_with_permission"],
        absenceWithoutPermission: json["absence_without_permission"],
        absenceExam: json["absence_exam"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "performance_percentage": performancePercentage,
        "avg_score": avgScore,
        "max_avg_score": maxAvgScore,
        "total_score": totalScore,
        "total_coeff": totalCoeff,
        "rank": rank,
        "month": month,
        "teacher_comment": teacherComment,
        "recommendation": recommendation,
        "behavior": behavior,
        "absence_total": absenceTotal,
        "absence_with_permission": absenceWithPermission,
        "absence_without_permission": absenceWithoutPermission,
        "absence_exam": absenceExam,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
    };
}
