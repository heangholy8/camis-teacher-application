// To parse this JSON data, do
//
//     final listPermissionRequestStudentModel = listPermissionRequestStudentModelFromJson(jsonString);

import 'dart:convert';

ListPermissionRequestStudentModel listPermissionRequestStudentModelFromJson(String str) => ListPermissionRequestStudentModel.fromJson(json.decode(str));

String listPermissionRequestStudentModelToJson(ListPermissionRequestStudentModel data) => json.encode(data.toJson());

class ListPermissionRequestStudentModel {
    bool? status;
    List<DataRequestPermission>? data;
    String? error;

    ListPermissionRequestStudentModel({
        this.status,
        this.data,
        this.error,
    });

    factory ListPermissionRequestStudentModel.fromJson(Map<String, dynamic> json) => ListPermissionRequestStudentModel(
        status: json["status"],
        data: List<DataRequestPermission>.from(json["data"].map((x) => DataRequestPermission.fromJson(x))),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "error": error,
    };
}

class DataRequestPermission {
    int? id;
    dynamic description;
    String? startDate;
    String? endDate;
    int? requestTypes;
    int? admissionType;
    String? admissionTypeName;
    String? admissionTypeNameEn;
    String? schoolyearId;
    String? createdBy;
    int? role;
    DateTime? createdAt;
    DateTime? updatedAt;
    int? approved;
    dynamic approvedBy;
    dynamic approvedAt;
    dynamic approvedRole;
    List<StudentId>? studentId;
    List<Schedule>? schedules;
    List<FileElement>? file;

    DataRequestPermission({
        this.id,
        this.description,
        this.startDate,
        this.endDate,
        this.requestTypes,
        this.admissionType,
        this.admissionTypeName,
        this.admissionTypeNameEn,
        this.schoolyearId,
        this.createdBy,
        this.role,
        this.createdAt,
        this.updatedAt,
        this.approved,
        this.approvedBy,
        this.approvedAt,
        this.approvedRole,
        this.studentId,
        this.schedules,
        this.file,
    });

    factory DataRequestPermission.fromJson(Map<String, dynamic> json) => DataRequestPermission(
        id: json["id"],
        description: json["description"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        requestTypes: json["request_types"],
        admissionType: json["admission_type"],
        admissionTypeName: json["admission_type_name"],
        admissionTypeNameEn: json["admission_type_name_en"],
        schoolyearId: json["schoolyear_id"],
        createdBy: json["created_by"],
        role: json["role"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        approved: json["approved"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
        approvedRole: json["approved_role"],
        studentId: List<StudentId>.from(json["student_id"].map((x) => StudentId.fromJson(x))),
        schedules: List<Schedule>.from(json["schedules"].map((x) => Schedule.fromJson(x))),
        file: List<FileElement>.from(json["file"].map((x) => FileElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "description": description,
        "start_date": startDate,
        "end_date": endDate,
        "request_types": requestTypes,
        "admission_type": admissionType,
        "admission_type_name": admissionTypeName,
        "admission_type_name_en": admissionTypeNameEn,
        "schoolyear_id": schoolyearId,
        "created_by": createdBy,
        "role": role,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "approved": approved,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
        "approved_role": approvedRole,
        "student_id": List<dynamic>.from(studentId!.map((x) => x.toJson())),
        "schedules": List<dynamic>.from(schedules!.map((x) => x.toJson())),
        "file": List<dynamic>.from(file!.map((x) => x.toJson())),
    };
}

class FileElement {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    FileElement({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class Schedule {
    int? scheduleId;
    String? startTime;
    String? endTime;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;

    Schedule({
        this.scheduleId,
        this.startTime,
        this.endTime,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
    });

    factory Schedule.fromJson(Map<String, dynamic> json) => Schedule(
        scheduleId: json["schedule_id"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
    );

    Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "start_time": startTime,
        "end_time": endTime,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
    };
}

class StudentId {
    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    int? role;
    String? fatherName;
    String? fatherPhone;
    String? motherName;
    String? motherPhone;
    String? guardianName;
    String? guardianPhone;
    FileElement? profileMedia;

    StudentId({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.fatherName,
        this.fatherPhone,
        this.motherName,
        this.motherPhone,
        this.guardianName,
        this.guardianPhone,
        this.profileMedia,
    });

    factory StudentId.fromJson(Map<String, dynamic> json) => StudentId(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        fatherName: json["father_name"],
        fatherPhone: json["father_phone"],
        motherName: json["mother_name"],
        motherPhone: json["mother_phone"],
        guardianName: json["guardian_name"],
        guardianPhone: json["guardian_phone"],
        profileMedia: FileElement.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "father_name": fatherName,
        "father_phone": fatherPhone,
        "mother_name": motherName,
        "mother_phone": motherPhone,
        "guardian_name": guardianName,
        "guardian_phone": guardianPhone,
        "profileMedia": profileMedia!.toJson(),
    };
}
