// To parse this JSON data, do
//
//     final listMonthSermesterModel = listMonthSermesterModelFromJson(jsonString);

import 'dart:convert';

ListMonthSermesterModel listMonthSermesterModelFromJson(String str) => ListMonthSermesterModel.fromJson(json.decode(str));

String listMonthSermesterModelToJson(ListMonthSermesterModel data) => json.encode(data.toJson());

class ListMonthSermesterModel {
    ListMonthSermesterModel({
        this.data,
    });

    List<Datum>? data;

    factory ListMonthSermesterModel.fromJson(Map<String, dynamic> json) => ListMonthSermesterModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.month,
        this.displayMonth,
        this.name,
        this.year,
        this.semester,
        this.active = false,
    });

    dynamic month;
    String? displayMonth;
    String? name;
    dynamic year;
    String? semester;
    bool? active;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        month: json["month"],
        displayMonth: json["displayMonth"],
        name: json["name"],
        year: json["year"],
        semester: json["semester"],
    );

    Map<String, dynamic> toJson() => {
        "month": month,
        "displayMonth": displayMonth,
        "name": name,
        "year": year,
        "semester": semester,
    };
}
