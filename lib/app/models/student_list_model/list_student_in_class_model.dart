// To parse this JSON data, do
//
//     final studentInClassListModel = studentInClassListModelFromJson(jsonString);

import 'dart:convert';
StudentInClassListModel studentInClassListModelFromJson(String str) => StudentInClassListModel.fromJson(json.decode(str));

String studentInClassListModelToJson(StudentInClassListModel data) => json.encode(data.toJson());

class StudentInClassListModel {
    StudentInClassListModel({
        this.data,
    });

    List<ClassMonitorSet>? data;

    factory StudentInClassListModel.fromJson(Map<String, dynamic> json) => StudentInClassListModel(
        data: List<ClassMonitorSet>.from(json["data"].map((x) => ClassMonitorSet.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class ClassMonitorSet {
    ClassMonitorSet({
        this.id,
        this.guid,
        this.studentId,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.dob,
        this.birthPlace,
        this.phone,
        this.email,
        this.address,
        this.sortkey,
        this.classId,
        this.gradeId,
        this.schoolyear,
        this.leadership,
        this.passFail,
        this.comeFromClass,
        this.comeFromSchool,
        this.goToClass,
        this.goToSchool,
        this.goToClassId,
        this.reExam,
        this.giveUp,
        this.giveUpDate,
        this.fatherLastname,
        this.fatherFirstname,
        this.fatherLastnameEn,
        this.fatherFirstnameEn,
        this.fatherName,
        this.fatherNameEn,
        this.fatherPhone,
        this.fatherJob,
        this.fatherDob,
        this.fatherEmail,
        this.fatherAddress,
        this.motherLastname,
        this.motherFirstname,
        this.motherLastnameEn,
        this.motherFirstnameEn,
        this.motherName,
        this.motherNameEn,
        this.motherPhone,
        this.motherJob,
        this.motherDob,
        this.motherEmail,
        this.motherAddress,
        this.guardianName,
        this.guardianNameEn,
        this.guardianPhone,
        this.academicAvatarImage,
        this.profileMedia,
        this.takeAttendance,
        this.inputScore
    });

    dynamic id;
    String? guid;
    String? studentId;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    String? gender;
    String? dob;
    String? birthPlace;
    String? phone;
    String? email;
    String? address;
    int? sortkey;
    int? classId;
    int? gradeId;
    String? schoolyear;
    int? leadership;
    int? takeAttendance;
    int? inputScore;
    int? passFail;
    String? comeFromClass;
    String? comeFromSchool;
    String? goToClass;
    String? goToSchool;
    int? goToClassId;
    int? reExam;
    int? giveUp;
    String? giveUpDate;
    String? fatherLastname;
    String? fatherFirstname;
    String? fatherLastnameEn;
    String? fatherFirstnameEn;
    String? fatherName;
    String? fatherNameEn;
    String? fatherPhone;
    String? fatherJob;
    String? fatherDob;
    String? fatherEmail;
    String? fatherAddress;
    String? motherLastname;
    String? motherFirstname;
    String? motherLastnameEn;
    String? motherFirstnameEn;
    String? motherName;
    String? motherNameEn;
    String? motherPhone;
    String? motherJob;
    String? motherDob;
    String? motherEmail;
    String? motherAddress;
    String? guardianName;
    String? guardianNameEn;
    String? guardianPhone;
    dynamic academicAvatarImage;
    ProfileMedia? profileMedia;

    factory ClassMonitorSet.fromJson(Map<String, dynamic> json) => ClassMonitorSet(
        id: json["id"].toDouble(),
        guid: json["guid"],
        studentId: json["student_id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        dob: json["dob"],
        birthPlace: json["birth_place"],
        phone: json["phone"],
        email: json["email"],
        takeAttendance:json["take_attendance"],
        inputScore: json["input_score"],
        address: json["address"],
        sortkey: json["sortkey"],
        classId: json["class_id"],
        gradeId: json["grade_id"],
        schoolyear: json["schoolyear"],
        leadership: json["leadership"],
        passFail: json["pass_fail"],
        comeFromClass: json["come_from_class"],
        comeFromSchool: json["come_from_school"],
        goToClass: json["go_to_class"],
        goToSchool: json["go_to_school"],
        goToClassId: json["go_to_class_id"],
        reExam: json["re_exam"],
        giveUp: json["give_up"],
        giveUpDate: json["give_up_date"],
        fatherLastname: json["father_lastname"],
        fatherFirstname: json["father_firstname"],
        fatherLastnameEn: json["father_lastname_en"],
        fatherFirstnameEn: json["father_firstname_en"],
        fatherName: json["father_name"],
        fatherNameEn: json["father_name_en"],
        fatherPhone: json["father_phone"],
        fatherJob: json["father_job"],
        fatherDob: json["father_dob"],
        fatherEmail: json["father_email"],
        fatherAddress: json["father_address"],
        motherLastname: json["mother_lastname"],
        motherFirstname: json["mother_firstname"],
        motherLastnameEn: json["mother_lastname_en"],
        motherFirstnameEn: json["mother_firstname_en"],
        motherName: json["mother_name"],
        motherNameEn: json["mother_name_en"],
        motherPhone: json["mother_phone"],
        motherJob: json["mother_job"],
        motherDob: json["mother_dob"],
        motherEmail: json["mother_email"],
        motherAddress: json["mother_address"],
        guardianName: json["guardian_name"],
        guardianNameEn: json["guardian_name_en"],
        guardianPhone: json["guardian_phone"],
        academicAvatarImage: json["academicAvatarImage"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "guid": guid,
        "student_id": studentId,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "dob": dob,
        "birth_place": birthPlace,
        "phone": phone,
        "email": email,
        "address": address,
        "sortkey": sortkey,
        "class_id": classId,
        "grade_id": gradeId,
        "schoolyear": schoolyear,
        "leadership": leadership,
        "pass_fail": passFail,
        "come_from_class": comeFromClass,
        "come_from_school": comeFromSchool,
        "go_to_class": goToClass,
        "go_to_school": goToSchool,
        "go_to_class_id": goToClassId,
        "re_exam": reExam,
        "give_up": giveUp,
        "give_up_date": giveUpDate,
        "father_lastname": fatherLastname,
        "father_firstname": fatherFirstname,
        "father_lastname_en": fatherLastnameEn,
        "father_firstname_en": fatherFirstnameEn,
        "father_name": fatherName,
        "father_name_en": fatherNameEn,
        "father_phone": fatherPhone,
        "father_job": fatherJob,
        "father_dob": fatherDob,
        "father_email": fatherEmail,
        "father_address": fatherAddress,
        "mother_lastname": motherLastname,
        "mother_firstname": motherFirstname,
        "mother_lastname_en": motherLastnameEn,
        "mother_firstname_en": motherFirstnameEn,
        "mother_name": motherName,
        "mother_name_en": motherNameEn,
        "mother_phone": motherPhone,
        "mother_job": motherJob,
        "mother_dob": motherDob,
        "mother_email": motherEmail,
        "mother_address": motherAddress,
        "guardian_name": guardianName,
        "guardian_name_en": guardianNameEn,
        "guardian_phone": guardianPhone,
        "academicAvatarImage": academicAvatarImage,
        "profileMedia": profileMedia!.toJson(),
        "input_score":inputScore,
        "take_attendance":takeAttendance
    };
}

class ProfileMedia {
    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;
    String? fileShow;
    String? fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
