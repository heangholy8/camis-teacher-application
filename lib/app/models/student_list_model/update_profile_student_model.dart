// To parse this JSON data, do
//
//     final updateStudentProfileModel = updateStudentProfileModelFromJson(jsonString);

import 'dart:convert';

UpdateStudentProfileModel updateStudentProfileModelFromJson(String str) => UpdateStudentProfileModel.fromJson(json.decode(str));

String updateStudentProfileModelToJson(UpdateStudentProfileModel data) => json.encode(data.toJson());

class UpdateStudentProfileModel {
    UpdateStudentProfileModel({
        this.status,
        this.data,
        this.message,
    });

    bool? status;
    Data? data;
    String? message;

    factory UpdateStudentProfileModel.fromJson(Map<String, dynamic> json) => UpdateStudentProfileModel(
        status: json["status"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data!.toJson(),
        "message": message,
    };
}

class Data {
    Data({
        this.id,
        this.guid,
        this.studentId,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.dob,
        this.birthPlace,
        this.phone,
        this.email,
        this.address,
        this.sortkey,
        this.classId,
        this.gradeId,
        this.schoolyear,
        this.leadership,
        this.passFail,
        this.comeFromClass,
        this.comeFromSchool,
        this.goToClass,
        this.goToSchool,
        this.goToClassId,
        this.reExam,
        this.giveUp,
        this.giveUpDate,
        this.fatherName,
        this.fatherNameEn,
        this.fatherPhone,
        this.motherName,
        this.motherNameEn,
        this.motherPhone,
        this.guardianName,
        this.guardianNameEn,
        this.guardianPhone,
        this.academicAvatarImage,
        this.profileMedia,
    });

    int? id;
    String? guid;
    String? studentId;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    String? gender;
    String? dob;
    String? birthPlace;
    String? phone;
    String? email;
    String? address;
    int? sortkey;
    int? classId;
    int? gradeId;
    String? schoolyear;
    int? leadership;
    int? passFail;
    String? comeFromClass;
    String? comeFromSchool;
    String? goToClass;
    String? goToSchool;
    int? goToClassId;
    int? reExam;
    int? giveUp;
    String? giveUpDate;
    String? fatherName;
    String? fatherNameEn;
    String? fatherPhone;
    String? motherName;
    String? motherNameEn;
    String? motherPhone;
    String? guardianName;
    String? guardianNameEn;
    String? guardianPhone;
    dynamic academicAvatarImage;
    ProfileMedia? profileMedia;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        guid: json["guid"],
        studentId: json["student_id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        dob: json["dob"],
        birthPlace: json["birth_place"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        sortkey: json["sortkey"],
        classId: json["class_id"],
        gradeId: json["grade_id"],
        schoolyear: json["schoolyear"],
        leadership: json["leadership"],
        passFail: json["pass_fail"],
        comeFromClass: json["come_from_class"],
        comeFromSchool: json["come_from_school"],
        goToClass: json["go_to_class"],
        goToSchool: json["go_to_school"],
        goToClassId: json["go_to_class_id"],
        reExam: json["re_exam"],
        giveUp: json["give_up"],
        giveUpDate: json["give_up_date"],
        fatherName: json["father_name"],
        fatherNameEn: json["father_name_en"],
        fatherPhone: json["father_phone"],
        motherName: json["mother_name"],
        motherNameEn: json["mother_name_en"],
        motherPhone: json["mother_phone"],
        guardianName: json["guardian_name"],
        guardianNameEn: json["guardian_name_en"],
        guardianPhone: json["guardian_phone"],
        academicAvatarImage: json["academicAvatarImage"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "guid": guid,
        "student_id": studentId,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "dob": dob,
        "birth_place": birthPlace,
        "phone": phone,
        "email": email,
        "address": address,
        "sortkey": sortkey,
        "class_id": classId,
        "grade_id": gradeId,
        "schoolyear": schoolyear,
        "leadership": leadership,
        "pass_fail": passFail,
        "come_from_class": comeFromClass,
        "come_from_school": comeFromSchool,
        "go_to_class": goToClass,
        "go_to_school": goToSchool,
        "go_to_class_id": goToClassId,
        "re_exam": reExam,
        "give_up": giveUp,
        "give_up_date": giveUpDate,
        "father_name": fatherName,
        "father_name_en": fatherNameEn,
        "father_phone": fatherPhone,
        "mother_name": motherName,
        "mother_name_en": motherNameEn,
        "mother_phone": motherPhone,
        "guardian_name": guardianName,
        "guardian_name_en": guardianNameEn,
        "guardian_phone": guardianPhone,
        "academicAvatarImage": academicAvatarImage,
        "profileMedia": profileMedia!.toJson(),
    };
}

class ProfileMedia {
    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;
    String? fileShow;
    String? fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
