// To parse this JSON data, do
//
//     final scheduleModel = scheduleModelFromJson(jsonString);

import 'dart:convert';

ScheduleModel scheduleModelFromJson(String str) => ScheduleModel.fromJson(json.decode(str));

String scheduleModelToJson(ScheduleModel data) => json.encode(data.toJson());

class ScheduleModel {
    ScheduleModel({
        this.status,
        this.data,
        this.expire,
        this.message,
    });

    bool? status;
    List<Datum>? data;
    bool? expire;
    String? message;

    factory ScheduleModel.fromJson(Map<String, dynamic> json) => ScheduleModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        expire: json["expire"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "expire": expire,
        "message": message,
    };
}

class Datum {
    Datum({
        this.scheduleId,
        this.scheduleType,
        this.startTime,
        this.endTime,
        this.duration,
        this.isCurrent,
        this.date,
        this.shortDay,
        this.event,
        this.subjectGroupTagId,
        this.subjectGroupTagName,
        this.subjectGroupTagNameEn,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectColor,
        this.classId,
        this.className,
        this.teacherName,
        this.teacherNameEn,
        this.teacherId,
        this.isInstructor,
        this.isSubSubject,
        this.attendance,
        this.isExam,
        this.isCheckScoreEnter,
        this.scoreEnterStatusName,
        this.totalStudentsScoreEntered,
        this.totalStudentsScoreEnteredPercentage,
        this.scoreEnterExpireDate,
        this.taskStatus,
        this.taskStatusName,
        this.countdownDays,
        this.examItem,
        this.activities,
        this.countEequestPermissions,
        this.isSelected=false,
        this.staffPresentStatus,
        this.timeTableSettingId,
        this.staffPresentStatusName
    });

    int? scheduleId;
    int? scheduleType;
    String? startTime;
    String? endTime;
    String? duration;
    bool? isCurrent;
    String? date;
    String? shortDay;
    String? event;
    dynamic subjectGroupTagId;
    dynamic subjectGroupTagName;
    dynamic subjectGroupTagNameEn;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectColor;
    int? classId;
    String? className;
    String? teacherName;
    String? teacherNameEn;
    String? teacherId;
    dynamic isInstructor;
    bool? isSubSubject;
    Attendance? attendance;
    bool? isExam;
    int? isCheckScoreEnter;
    String? scoreEnterStatusName;
    dynamic totalStudentsScoreEntered;
    dynamic totalStudentsScoreEnteredPercentage;
    String? scoreEnterExpireDate;
    int? taskStatus;
    String? taskStatusName;
    int? countdownDays;
    ExamItem? examItem;
    List<Activity>? activities;
    int? staffPresentStatus;
    String? staffPresentStatusName;
    int? timeTableSettingId;
    int? countEequestPermissions;
    bool isSelected = false;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
  
        scheduleId: json["schedule_id"],
        scheduleType: json["schedule_type"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        duration: json["duration"],
        isCurrent: json["is_current"],
        date: json["date"],
        shortDay: json["short_day"],
        event: json["event"],
        subjectGroupTagId: json["subject_group_tag_id"],
        subjectGroupTagName: json["subject_group_tag_name"],
        subjectGroupTagNameEn: json["subject_group_tag_name_en"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectColor: json["subject_color"],
        classId: json["class_id"],
        className: json["class_name"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherId: json["teacher_id"],
        isInstructor:json["is_instructor"],
        isSubSubject:json["is_sub_subject"],
        attendance: Attendance.fromJson(json["attendance"]),
        isExam: json["is_exam"],
        isCheckScoreEnter: json["is_check_score_enter"],
        scoreEnterStatusName: json["score_enter_status_name"],
        totalStudentsScoreEntered: json["total_students_score_entered"],
        totalStudentsScoreEnteredPercentage: json["total_students_score_entered_percentage"],
        scoreEnterExpireDate: json["score_enter_expire_date"],
        taskStatus: json["task_status"],
        taskStatusName: json["task_status_name"],
        countdownDays: json["countdown_days"],
        examItem: json["exam_item"] == null?null:ExamItem.fromJson(json["exam_item"]),
        activities: List<Activity>.from(json["activities"].map((x) => Activity.fromJson(x))),
        staffPresentStatus:json["staff_present_status"],
        staffPresentStatusName: json["staff_present_status_name"],
        timeTableSettingId: json["time_table_setting_id"],
        countEequestPermissions: json["count_request_permissions"],
    );

    Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "schedule_type": scheduleType,
        "start_time": startTime,
        "end_time": endTime,
        "duration": duration,
        "is_current": isCurrent,
        "date": date,
        "short_day": shortDay,
        "event": event,
        "subject_group_tag_id": subjectGroupTagId,
        "subject_group_tag_name": subjectGroupTagName,
        "subject_group_tag_name_en": subjectGroupTagNameEn,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_color": subjectColor,
        "class_id": classId,
        "class_name": className,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_id": teacherId,
        "is_instructor":isInstructor,
        "is_sub_subject":isSubSubject,
        "attendance": attendance!.toJson(),
        "is_exam": isExam,
        "is_check_score_enter": isCheckScoreEnter,
        "score_enter_status_name": scoreEnterStatusName,
        "total_students_score_entered": totalStudentsScoreEntered,
        "total_students_score_entered_percentage": totalStudentsScoreEnteredPercentage,
        "score_enter_expire_date": scoreEnterExpireDate,
        "task_status": taskStatus,
        "task_status_name": taskStatusName,
        "countdown_days": countdownDays,
        "exam_item": examItem==null?null:examItem!.toJson(),
        "activities": List<dynamic>.from(activities!.map((x) => x.toJson())),
        "count_request_permissions":countEequestPermissions,
        "staff_present_status":staffPresentStatus,
        "time_table_setting_id":timeTableSettingId,
        "staff_present_status_name":staffPresentStatusName
    };
}

class Activity {
    Activity({
        this.id,
        this.activityType,
        this.title,
        this.description,
        this.expiredAt,
        this.file,
        this.link,
    });

    int? id;
    int? activityType;
    String? title;
    dynamic description;
    String? expiredAt;
    List<FileElement>? file;
    List<LinkElement>? link;

    factory Activity.fromJson(Map<String, dynamic> json) => Activity(
        id: json["id"],
        activityType: json["activity_type"],
        title: json["title"],
        description: json["description"],
        expiredAt: json["expired_at"],
        file: List<FileElement>.from(json["file"].map((x) => FileElement.fromJson(x))),
        link: List<LinkElement>.from(json["link"].map((x) => LinkElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "activity_type": activityType,
        "title": title,
        "description": description,
        "expired_at": expiredAt,
        "file": List<dynamic>.from(file!.map((x) => x.toJson())),
        "link": List<dynamic>.from(link!.map((x) => x.toJson())),
    };
}

class LinkElement {
    LinkElement({
        this.id,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
    });

    int? id;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;

    factory LinkElement.fromJson(Map<String, dynamic> json) => LinkElement(
        id: json["id"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}

class FileElement {
    FileElement({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class Attendance {
    Attendance({
        this.isCheckAttendance,
        this.attendanceTaker,
        this.totalStudent,
        this.present,
        this.absent,
        this.late,
        this.permission,
    });

    int? isCheckAttendance;
    String? attendanceTaker;
    int? totalStudent;
    int? present;
    int? absent;
    int? late;
    int? permission;

    factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        isCheckAttendance: json["is_check_attendance"],
        attendanceTaker: json["attendance_taker"],
        totalStudent: json["total_student"],
        present: json["present"],
        absent: json["absent"],
        late: json["late"],
        permission: json["permission"],
    );

    Map<String, dynamic> toJson() => {
        "is_check_attendance": isCheckAttendance,
        "attendance_taker": attendanceTaker,
        "total_student": totalStudent,
        "present": present,
        "absent": absent,
        "late": late,
        "permission": permission,
    };
}

class ExamItem {
    ExamItem({
        this.id,
        this.subjectId,
        this.classId,
        this.schedule,
        this.type,
        this.examDate,
        this.month,
        this.semester,
        this.description,
        this.isApprove,
        this.medias,
    });

    int? id;
    int? subjectId;
    int? classId;
    int? schedule;
    int? type;
    String? examDate;
    String? month;
    String? semester;
    String? description;
    int? isApprove;
    List<dynamic>? medias;

    factory ExamItem.fromJson(Map<String, dynamic> json) => ExamItem(
        id: json["id"],
        subjectId: json["subject_id"],
        classId: json["class_id"],
        schedule: json["schedule"],
        type: json["type"],
        examDate: json["exam_date"],
        month: json["month"],
        semester: json["semester"],
        description: json["description"],
        isApprove: json["is_approve"],
        medias: List<dynamic>.from(json["medias"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "subject_id": subjectId,
        "class_id": classId,
        "schedule": schedule,
        "type": type,
        "exam_date": examDate,
        "month": month,
        "semester": semester,
        "description": description,
        "is_approve": isApprove,
        "medias": List<dynamic>.from(medias!.map((x) => x)),
    };
}
