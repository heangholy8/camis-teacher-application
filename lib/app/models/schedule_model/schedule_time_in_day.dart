// To parse this JSON data, do
//
//     final scheduleTimeModel = scheduleTimeModelFromJson(jsonString);

import 'dart:convert';

ScheduleTimeModel scheduleTimeModelFromJson(String str) => ScheduleTimeModel.fromJson(json.decode(str));

String scheduleTimeModelToJson(ScheduleTimeModel data) => json.encode(data.toJson());

class ScheduleTimeModel {
    ScheduleTimeModel({
        this.status,
        this.data,
        this.message,
    });

    bool? status;
    List<Datum>? data;
    String? message;

    factory ScheduleTimeModel.fromJson(Map<String, dynamic> json) => ScheduleTimeModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
    };
}

class Datum {
    Datum({
        this.isSelect,
        this.id,
        this.scheduleType,
        this.code,
        this.subjectName,
        this.subjectNameEn,
        this.subjectId,
        this.sessionPeriod,
        this.academicId,
        this.gradeId,
        this.schoolyearId,
        this.description,
        this.shortDay,
        this.startTime,
        this.endTime,
        this.duration,
        this.teacherId,
        this.teacherName,
        this.teacherNameEn,
        this.teacherFirstname,
        this.teacherLastname,
        this.teacherFirstnameEn,
        this.teacherLastnameEn,
    });
    bool? isSelect;
    int? id;
    int? scheduleType;
    String? code;
    String? subjectName;
    String? subjectNameEn;
    int? subjectId;
    int? sessionPeriod;
    int? academicId;
    int? gradeId;
    String? schoolyearId;
    String? description;
    String? shortDay;
    String? startTime;
    String? endTime;
    String? duration;
    String? teacherId;
    String? teacherName;
    String? teacherNameEn;
    String? teacherFirstname;
    String? teacherLastname;
    String? teacherFirstnameEn;
    String? teacherLastnameEn;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        isSelect: false,
        id: json["id"],
        scheduleType: json["schedule_type"],
        code: json["code"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectId: json["subject_id"],
        sessionPeriod: json["session_period"],
        academicId: json["academic_id"],
        gradeId: json["grade_id"],
        schoolyearId: json["schoolyear_id"],
        description: json["description"],
        shortDay: json["short_day"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        duration: json["duration"],
        teacherId: json["teacher_id"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherFirstname: json["teacher_firstname"],
        teacherLastname: json["teacher_lastname"],
        teacherFirstnameEn: json["teacher_firstname_en"],
        teacherLastnameEn: json["teacher_lastname_en"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "schedule_type": scheduleType,
        "code": code,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_id": subjectId,
        "session_period": sessionPeriod,
        "academic_id": academicId,
        "grade_id": gradeId,
        "schoolyear_id": schoolyearId,
        "description": description,
        "short_day": shortDay,
        "start_time": startTime,
        "end_time": endTime,
        "duration": duration,
        "teacher_id": teacherId,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_firstname": teacherFirstname,
        "teacher_lastname": teacherLastname,
        "teacher_firstname_en": teacherFirstnameEn,
        "teacher_lastname_en": teacherLastnameEn,
    };
}
