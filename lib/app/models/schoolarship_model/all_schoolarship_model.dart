
class AllSchoolarModel {
    List<Datum>? data;
    bool? status;

    AllSchoolarModel({
        this.data,
        this.status,
    });

    factory AllSchoolarModel.fromJson(Map<String, dynamic> json) => AllSchoolarModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "status": status,
    };
}

class Datum {
    int? id;
    String? studentId;
    String? studentName;
    String? studentNameEn;
    int? classId;
    String? className;
    String? schoolyearId;
    int? type;
    String? typeName;
    String? guardianId;
    String? description;
    String? requestedBy;
    int? requestedRole;
    int? instructorStatus;
    String? instructorBy;
    DateTime? instructorStatusDate;
    dynamic adminStatus;
    dynamic adminStatusAt;
    dynamic adminBy;
    String? adminByName;
    dynamic adminComment;
    DateTime? createdAt;
    DateTime? updatedAt;
    List<Media>? medias;

    Datum({
        this.id,
        this.studentId,
        this.studentName,
        this.studentNameEn,
        this.classId,
        this.className,
        this.schoolyearId,
        this.type,
        this.typeName,
        this.guardianId,
        this.description,
        this.requestedBy,
        this.requestedRole,
        this.instructorStatus,
        this.instructorBy,
        this.instructorStatusDate,
        this.adminStatus,
        this.adminStatusAt,
        this.adminBy,
        this.adminByName,
        this.adminComment,
        this.createdAt,
        this.updatedAt,
        this.medias,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        studentId: json["student_id"],
        studentName: json["student_name"],
        studentNameEn: json["student_name_en"],
        classId: json["class_id"],
        className: json["class_name"],
        schoolyearId: json["schoolyear_id"],
        type: json["type"],
        typeName: json["type_name"],
        guardianId: json["guardian_id"],
        description: json["description"],
        requestedBy: json["requested_by"],
        requestedRole: json["requested_role"],
        instructorStatus: json["instructor_status"],
        instructorBy: json["instructor_by"],
        instructorStatusDate: DateTime.parse(json["instructor_status_date"]),
        adminStatus: json["admin_status"],
        adminStatusAt: json["admin_status_at"],
        adminBy: json["admin_by"],
        adminByName: json["admin_by_name"],
        adminComment: json["admin_comment"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        medias: List<Media>.from(json["medias"].map((x) => Media.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "student_id": studentId,
        "student_name": studentName,
        "student_name_en": studentNameEn,
        "class_id": classId,
        "class_name": className,
        "schoolyear_id": schoolyearId,
        "type": type,
        "type_name": typeName,
        "guardian_id": guardianId,
        "description": description,
        "requested_by": requestedBy,
        "requested_role": requestedRole,
        "instructor_status": instructorStatus,
        "instructor_by": instructorBy,
        "instructor_status_date": instructorStatusDate?.toIso8601String(),
        "admin_status": adminStatus,
        "admin_status_at": adminStatusAt,
        "admin_by": adminBy,
        "admin_by_name": adminByName,
        "admin_comment": adminComment,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "medias": List<dynamic>.from(medias!.map((x) => x.toJson())),
    };
}

class Media {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    Media({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory Media.fromJson(Map<String, dynamic> json) => Media(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}
