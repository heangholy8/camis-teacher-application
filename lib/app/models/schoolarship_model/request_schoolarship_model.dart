class RequestSchoolarModel {
    String? message;
    bool? status;

    RequestSchoolarModel({
        this.message,
        this.status,
    });

    factory RequestSchoolarModel.fromJson(Map<String, dynamic> json) => RequestSchoolarModel(
        message: json["message"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status": status,
    };
}
