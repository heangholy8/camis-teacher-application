import 'dart:convert';

SchoolLocationModel schoolLocationModelFromJson(String str) => SchoolLocationModel.fromJson(json.decode(str));

String schoolLocationModelToJson(SchoolLocationModel data) => json.encode(data.toJson());

class SchoolLocationModel {
    bool? status;
    String? message;
    Data? data;

    SchoolLocationModel({
        this.status,
        this.message,
        this.data,
    });

    factory SchoolLocationModel.fromJson(Map<String, dynamic> json) => SchoolLocationModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    String? schoolCode;
    String? schoolName;
    String? schoolLatitude;
    String? schoolLongitude;
    int? schoolRadiusDistance;
    String? measurementUnit;

    Data({
        this.schoolCode,
        this.schoolName,
        this.schoolLatitude,
        this.schoolLongitude,
        this.schoolRadiusDistance,
        this.measurementUnit,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        schoolCode: json["school_code"],
        schoolName: json["school_name"],
        schoolLatitude: json["school_latitude"],
        schoolLongitude: json["school_longitude"],
        schoolRadiusDistance: json["school_radius_distance"],
        measurementUnit: json["measurement_unit"],
    );

    Map<String, dynamic> toJson() => {
        "school_code": schoolCode,
        "school_name": schoolName,
        "school_latitude": schoolLatitude,
        "school_longitude": schoolLongitude,
        "school_radius_distance": schoolRadiusDistance,
        "measurement_unit": measurementUnit,
    };
}
