// To parse this JSON data, do
//
//     final contactModel = contactModelFromJson(jsonString);

import 'dart:convert';

String contactModelToJson(ContactModel data) => json.encode(data.toJson());

class ContactModel {
    List<Datum>? data;

    ContactModel({
        this.data,
    });

    factory ContactModel.fromJson(Map<String, dynamic> json) => ContactModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    int? id;
    dynamic customerId;
    dynamic type;
    dynamic telecomeName;
    dynamic link;
    dynamic createdAt;
    dynamic updatedAt;

    Datum({
        this.id,
        this.customerId,
        this.type,
        this.telecomeName,
        this.link,
        this.createdAt,
        this.updatedAt,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        customerId: json["customer_id"],
        type: json["type"],
        telecomeName: json["telecome_name"],
        link: json["link"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "customer_id": customerId,
        "type": type,
        "telecome_name": telecomeName,
        "link": link,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
