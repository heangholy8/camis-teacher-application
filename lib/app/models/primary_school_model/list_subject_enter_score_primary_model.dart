// To parse this JSON DataSubjectPrimaryEnterScore, do
//
//     final listSubjectEnterScorePrimaryModel = listSubjectEnterScorePrimaryModelFromJson(jsonString);

import 'dart:convert';

ListSubjectEnterScorePrimaryModel listSubjectEnterScorePrimaryModelFromJson(String str) => ListSubjectEnterScorePrimaryModel.fromJson(json.decode(str));

String listSubjectEnterScorePrimaryModelToJson(ListSubjectEnterScorePrimaryModel dataSubjectPrimaryEnterScore) => json.encode(dataSubjectPrimaryEnterScore.toJson());

class ListSubjectEnterScorePrimaryModel {
    bool? status;
    DataSubjectPrimaryEnterScore? dataSubjectPrimaryEnterScore;
    String? message;

    ListSubjectEnterScorePrimaryModel({
        this.status,
        this.dataSubjectPrimaryEnterScore,
        this.message,
    });

    factory ListSubjectEnterScorePrimaryModel.fromJson(Map<String, dynamic> json) => ListSubjectEnterScorePrimaryModel(
        status: json["status"],
        dataSubjectPrimaryEnterScore: json["data"] == null?null:DataSubjectPrimaryEnterScore.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": dataSubjectPrimaryEnterScore==null?null:dataSubjectPrimaryEnterScore!.toJson(),
        "message": message,
    };
}

class DataSubjectPrimaryEnterScore {
    int? classId;
    String? objectType;
    String? name;
    String? nameEn;
    String? schoolyear;
    int? schoolType;
    int? isInstructor;
    List<SubjectsGroupTag>? subjectsGroupTag;

    DataSubjectPrimaryEnterScore({
        this.classId,
        this.objectType,
        this.name,
        this.nameEn,
        this.schoolyear,
        this.schoolType,
        this.isInstructor,
        this.subjectsGroupTag,
    });

    factory DataSubjectPrimaryEnterScore.fromJson(Map<String, dynamic> json) => DataSubjectPrimaryEnterScore(
        classId: json["class_id"],
        objectType: json["object_type"],
        name: json["name"],
        nameEn: json["name_en"],
        schoolyear: json["schoolyear"],
        schoolType: json["school_type"],
        isInstructor: json["is_instructor"],
        subjectsGroupTag: List<SubjectsGroupTag>.from(json["subjects_group_tag"].map((x) => SubjectsGroupTag.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "class_id": classId,
        "object_type": objectType,
        "name": name,
        "name_en": nameEn,
        "schoolyear": schoolyear,
        "school_type": schoolType,
        "is_instructor": isInstructor,
        "subjects_group_tag": List<dynamic>.from(subjectsGroupTag!.map((x) => x.toJson())),
    };
}

class SubjectsGroupTag {
    int? subjectGroupTagId;
    String? subjectGroupTagName;
    String? subjectGroupTagNameEn;
    List<TeachingSubject>? teachingSubjects;

    SubjectsGroupTag({
        this.subjectGroupTagId,
        this.subjectGroupTagName,
        this.subjectGroupTagNameEn,
        this.teachingSubjects,
    });

    factory SubjectsGroupTag.fromJson(Map<String, dynamic> json) => SubjectsGroupTag(
        subjectGroupTagId: json["subject_group_tag_id"],
        subjectGroupTagName: json["subject_group_tag_name"],
        subjectGroupTagNameEn: json["subject_group_tag_name_en"],
        teachingSubjects: List<TeachingSubject>.from(json["teaching_subjects"].map((x) => TeachingSubject.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "subject_group_tag_id": subjectGroupTagId,
        "subject_group_tag_name": subjectGroupTagName,
        "subject_group_tag_name_en": subjectGroupTagNameEn,
        "teaching_subjects": List<dynamic>.from(teachingSubjects!.map((x) => x.toJson())),
    };
}

class TeachingSubject {
    int? scheduleId;
    String? startTime;
    String? endTime;
    int? subjectGroupTagId;
    String? subjectGroupTagName;
    String? subjectGroupTagNameEn;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectColor;
    Attendance? attendance;
    int? id;
    String? examDate;
    int? type;
    int? month;
    String? monthName;
    String? monthNameEn;
    String? semester;
    String? description;
    int? isNoExam;
    int? isApprove;
    String? approvedBy;
    String? approvedAt;
    int? isCheckScoreEnter;
    String? scoreEnterStatusName;
    int? totalStudentsScoreEntered;
    dynamic totalStudentsScoreEnteredPercentage;

    TeachingSubject({
        this.scheduleId,
        this.startTime,
        this.endTime,
        this.subjectGroupTagId,
        this.subjectGroupTagName,
        this.subjectGroupTagNameEn,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectColor,
        this.attendance,
        this.id,
        this.examDate,
        this.type,
        this.month,
        this.monthName,
        this.monthNameEn,
        this.semester,
        this.description,
        this.isNoExam,
        this.isApprove,
        this.approvedBy,
        this.approvedAt,
        this.isCheckScoreEnter,
        this.scoreEnterStatusName,
        this.totalStudentsScoreEntered,
        this.totalStudentsScoreEnteredPercentage,
    });

    factory TeachingSubject.fromJson(Map<String, dynamic> json) => TeachingSubject(
        scheduleId: json["schedule_id"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        subjectGroupTagId: json["subject_group_tag_id"],
        subjectGroupTagName: json["subject_group_tag_name"],
        subjectGroupTagNameEn: json["subject_group_tag_name_en"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectColor: json["subject_color"],
        attendance: Attendance.fromJson(json["attendance"]),
        id: json["id"],
        examDate: json["exam_date"],
        type: json["type"],
        month: json["month"],
        monthName: json["month_name"],
        monthNameEn: json["month_name_en"],
        semester: json["semester"],
        description: json["description"],
        isNoExam: json["is_no_exam"],
        isApprove: json["is_approve"],
        approvedBy: json["approved_by"],
        approvedAt: json["approved_at"],
        isCheckScoreEnter: json["is_check_score_enter"],
        scoreEnterStatusName: json["score_enter_status_name"],
        totalStudentsScoreEntered: json["total_students_score_entered"],
        totalStudentsScoreEnteredPercentage: json["total_students_score_entered_percentage"],
    );

    Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "start_time": startTime,
        "end_time": endTime,
        "subject_group_tag_id": subjectGroupTagId,
        "subject_group_tag_name": subjectGroupTagName,
        "subject_group_tag_name_en": subjectGroupTagNameEn,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_color": subjectColor,
        "attendance": attendance!.toJson(),
        "id": id,
        "exam_date": examDate,
        "type": type,
        "month": month,
        "month_name": monthName,
        "month_name_en": monthNameEn,
        "semester": semester,
        "description": description,
        "is_no_exam": isNoExam,
        "is_approve": isApprove,
        "approved_by": approvedBy,
        "approved_at": approvedAt,
        "is_check_score_enter": isCheckScoreEnter,
        "score_enter_status_name": scoreEnterStatusName,
        "total_students_score_entered": totalStudentsScoreEntered,
        "total_students_score_entered_percentage": totalStudentsScoreEnteredPercentage,
    };
}

class Attendance {
    int? isCheckAttendance;
    String? attendanceTaker;
    int? totalStudent;
    int? present;
    int? absent;
    int? late;
    int? permission;

    Attendance({
        this.isCheckAttendance,
        this.attendanceTaker,
        this.totalStudent,
        this.present,
        this.absent,
        this.late,
        this.permission,
    });

    factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        isCheckAttendance: json["is_check_attendance"],
        attendanceTaker: json["attendance_taker"],
        totalStudent: json["total_student"],
        present: json["present"],
        absent: json["absent"],
        late: json["late"],
        permission: json["permission"],
    );

    Map<String, dynamic> toJson() => {
        "is_check_attendance": isCheckAttendance,
        "attendance_taker": attendanceTaker,
        "total_student": totalStudent,
        "present": present,
        "absent": absent,
        "late": late,
        "permission": permission,
    };
}
