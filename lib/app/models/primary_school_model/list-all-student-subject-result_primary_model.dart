// To parse this JSON data, do
//
//     final listAllStudentSubjectResultPrimaryModel = listAllStudentSubjectResultPrimaryModelFromJson(jsonString);
import 'dart:convert';
ListAllStudentSubjectResultPrimaryModel listAllStudentSubjectResultPrimaryModelFromJson(String str) => ListAllStudentSubjectResultPrimaryModel.fromJson(json.decode(str));

String listAllStudentSubjectResultPrimaryModelToJson(ListAllStudentSubjectResultPrimaryModel data) => json.encode(data.toJson());

class ListAllStudentSubjectResultPrimaryModel {
    Data? data;
    bool? status;

    ListAllStudentSubjectResultPrimaryModel({
        this.data,
        this.status,
    });

    factory ListAllStudentSubjectResultPrimaryModel.fromJson(Map<String, dynamic> json) => ListAllStudentSubjectResultPrimaryModel(
        data: Data.fromJson(json["data"]),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
        "status": status,
    };
}

class Data {
    List<Header>? header;
    List<TotalAvg>? totalAvg;
    List<Datum>? data;

    Data({
        this.header,
        this.totalAvg,
        this.data,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        header: List<Header>.from(json["header"].map((x) => Header.fromJson(x))),
        totalAvg: List<TotalAvg>.from(json["total_avg"].map((x) => TotalAvg.fromJson(x))),
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "header": List<dynamic>.from(header!.map((x) => x.toJson())),
        "total_avg": List<dynamic>.from(totalAvg!.map((x) => x.toJson())),
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    String? studentId;
    double? studentSchoolyearId;
    String? studentSchoolId;
    dynamic classId;
    String? className;
    String? classNameEn;
    String? name;
    String? nameEn;
    dynamic gender;
    dynamic ranking;
    List<Result>? result;

    Datum({
        this.studentId,
        this.studentSchoolyearId,
        this.studentSchoolId,
        this.classId,
        this.className,
        this.classNameEn,
        this.name,
        this.nameEn,
        this.gender,
        this.ranking,
        this.result,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        studentId: json["student_id"],
        studentSchoolyearId: json["student_schoolyear_id"].toDouble(),
        studentSchoolId: json["student_school_id"],
        classId: json["class_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        name: json["name"],
        nameEn: json["name_en"],
        gender: json["gender"],
        ranking: json["ranking"],
        result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "student_schoolyear_id": studentSchoolyearId,
        "student_school_id": studentSchoolId,
        "class_id": classId,
        "class_name": className,
        "class_name_en": classNameEn,
        "name": name,
        "name_en": nameEn,
        "gender": gender,
        "ranking": ranking,
        "result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    dynamic subjectId;
    String? subjectName;
    String? color;
    dynamic display;
    bool? notEvaluation;
    dynamic opacity;

    Result({
        this.subjectId,
        this.subjectName,
        this.color,
        this.display,
        this.notEvaluation,
        this.opacity,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        color: json["color"],
        display: json["display"],
        notEvaluation: json["not_evaluation"],
        opacity: json["opacity"],
    );

    Map<String, dynamic> toJson() => {
        "subject_id": subjectId,
        "subject_name": subjectName,
        "color": color,
        "display": display,
        "not_evaluation": notEvaluation,
        "opacity": opacity,
    };
}

class Header {
    dynamic id;
    String? name;
    String? nameEn;
    dynamic sort;
    String? color;
    dynamic notEvaluation;
    List<Subject>? subject;

    Header({
        this.id,
        this.name,
        this.nameEn,
        this.sort,
        this.color,
        this.notEvaluation,
        this.subject,
    });

    factory Header.fromJson(Map<String, dynamic> json) => Header(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        sort: json["sort"],
        color: json["color"],
        notEvaluation: json["not_evaluation"],
        subject: List<Subject>.from(json["subject"].map((x) => Subject.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "sort": sort,
        "color": color,
        "not_evaluation": notEvaluation,
        "subject": List<dynamic>.from(subject!.map((x) => x.toJson())),
    };
}

class Subject {
    dynamic subjectId;
    String? subjectName;
    String? subjectNameEn;
    dynamic sort;

    Subject({
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.sort,
    });

    factory Subject.fromJson(Map<String, dynamic> json) => Subject(
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "sort": sort,
    };
}
class TotalAvg {
    String? name;
    dynamic total;
    dynamic female;

    TotalAvg({
        this.name,
        this.total,
        this.female,
    });

    factory TotalAvg.fromJson(Map<String, dynamic> json) => TotalAvg(
        name: json["name"],
        total: json["total"],
        female: json["female"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "total": total,
        "female": female,
    };
}
