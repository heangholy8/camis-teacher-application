// To parse this JSON data, do
//
//     final studentResultMonthSemesterDetailPrimaryModel = studentResultMonthSemesterDetailPrimaryModelFromJson(jsonString);

import 'dart:convert';

StudentResultMonthSemesterDetailPrimaryModel studentResultMonthSemesterDetailPrimaryModelFromJson(String str) => StudentResultMonthSemesterDetailPrimaryModel.fromJson(json.decode(str));

String studentResultMonthSemesterDetailPrimaryModelToJson(StudentResultMonthSemesterDetailPrimaryModel data) => json.encode(data.toJson());

class StudentResultMonthSemesterDetailPrimaryModel {
    bool? status;
    DataResultStudentDetail? dataResultStudentDetail;
    String? message;

    StudentResultMonthSemesterDetailPrimaryModel({
        this.status,
        this.dataResultStudentDetail,
        this.message,
    });

    factory StudentResultMonthSemesterDetailPrimaryModel.fromJson(Map<String, dynamic> json) => StudentResultMonthSemesterDetailPrimaryModel(
        status: json["status"],
        dataResultStudentDetail: DataResultStudentDetail.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": dataResultStudentDetail!.toJson(),
        "message": message,
    };
}

class DataResultStudentDetail {
    Result? result;
    List<SubjectTag>? subjectTag;
    int? totalStudent;
    List<GradingSystem>? gradingSystem;

    DataResultStudentDetail({
        this.result,
        this.subjectTag,
        this.totalStudent,
        this.gradingSystem,
    });

    factory DataResultStudentDetail.fromJson(Map<String, dynamic> json) => DataResultStudentDetail(
        result: json["result"] == null?null: Result.fromJson(json["result"]),
        subjectTag: List<SubjectTag>.from(json["subject_tag"].map((x) => SubjectTag.fromJson(x))),
        totalStudent: json["total_student"],
        gradingSystem: List<GradingSystem>.from(json["grading_system"].map((x) => GradingSystem.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "result": result == null?null:result!.toJson(),
        "subject_tag": List<dynamic>.from(subjectTag!.map((x) => x.toJson())),
        "total_student": totalStudent,
        "grading_system": List<dynamic>.from(gradingSystem!.map((x) => x.toJson())),
    };
}

class GradingSystem {
    int? id;
    dynamic letterGrade;
    dynamic description;
    dynamic descriptionEn;
    int? sortkey;
    dynamic scoreMin;
    dynamic scoreMax;
    int? isFail;

    GradingSystem({
        this.id,
        this.letterGrade,
        this.description,
        this.descriptionEn,
        this.sortkey,
        this.scoreMin,
        this.scoreMax,
        this.isFail,
    });

    factory GradingSystem.fromJson(Map<String, dynamic> json) => GradingSystem(
        id: json["id"],
        letterGrade: json["letter_grade"],
        description: json["description"],
        descriptionEn: json["description_en"],
        sortkey: json["sortkey"],
        scoreMin: json["score_min"],
        scoreMax: json["score_max"],
        isFail: json["is_fail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "letter_grade": letterGrade,
        "description": description,
        "description_en": descriptionEn,
        "sortkey": sortkey,
        "score_min": scoreMin,
        "score_max": scoreMax,
        "is_fail": isFail,
    };
}

class Result {
    dynamic id;
    dynamic avgScore;
    dynamic maxAvgScore;
    dynamic rank;
    dynamic totalRank;
    dynamic teacherComment;
    dynamic recommendation;
    dynamic behavior;
    dynamic absenceTotal;
    dynamic absenceWithPermission;
    dynamic absenceWithoutPermission;
    dynamic grading;
    dynamic gradingEn;
    dynamic letterGrade;
    dynamic gradePoints;
    dynamic isFail;
    dynamic teacherName;

    Result({
        this.id,
        this.avgScore,
        this.maxAvgScore,
        this.rank,
        this.totalRank,
        this.teacherComment,
        this.recommendation,
        this.behavior,
        this.absenceTotal,
        this.absenceWithPermission,
        this.absenceWithoutPermission,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.gradePoints,
        this.isFail,
        this.teacherName,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        avgScore: json["avg_score"].toDouble(),
        maxAvgScore: json["max_avg_score"],
        rank: json["rank"],
        totalRank: json["total_rank"],
        teacherComment: json["teacher_comment"],
        recommendation: json["recommendation"],
        behavior: json["behavior"],
        absenceTotal: json["absence_total"],
        absenceWithPermission: json["absence_with_permission"],
        absenceWithoutPermission: json["absence_without_permission"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        gradePoints: json["grade_points"],
        isFail: json["is_fail"],
        teacherName: json["teacher_name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "avg_score": avgScore,
        "max_avg_score": maxAvgScore,
        "rank": rank,
        "total_rank": totalRank,
        "teacher_comment": teacherComment,
        "recommendation": recommendation,
        "behavior": behavior,
        "absence_total": absenceTotal,
        "absence_with_permission": absenceWithPermission,
        "absence_without_permission": absenceWithoutPermission,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "grade_points": gradePoints,
        "is_fail": isFail,
        "teacher_name": teacherName,
    };
}

class SubjectTag {
    dynamic id;
    String? name;
    String? nameEn;
    List<Subject>? subject;

    SubjectTag({
        this.id,
        this.name,
        this.nameEn,
        this.subject,
    });

    factory SubjectTag.fromJson(Map<String, dynamic> json) => SubjectTag(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        subject: List<Subject>.from(json["subject"].map((x) => Subject.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "subject": List<dynamic>.from(subject!.map((x) => x.toJson())),
    };
}

class Subject {
    dynamic subjectTagId;
    String? name;
    String? nameEn;
    dynamic short;
    dynamic sort;
    dynamic maxScore;
    dynamic absentExam;
    dynamic score;
    dynamic rank;
    String? grading;
    String? gradingEn;
    String? letterGrade;
    dynamic teacherComment;
    //List<dynamic>? childrens;

    Subject({
        this.subjectTagId,
        this.name,
        this.nameEn,
        this.short,
        this.sort,
        this.maxScore,
        this.absentExam,
        this.score,
        this.rank,
        this.grading,
        this.gradingEn,
        this.letterGrade,
        this.teacherComment,
        //this.childrens,
    });

    factory Subject.fromJson(Map<String, dynamic> json) => Subject(
        subjectTagId: json["subject_tag_id"],
        name: json["name"],
        nameEn: json["name_en"],
        short: json["short"],
        sort: json["sort"],
        maxScore: json["max_score"],
        absentExam: json["absent_exam"],
        score: json["score"],
        rank: json["rank"],
        grading: json["grading"],
        gradingEn: json["grading_en"],
        letterGrade: json["letter_grade"],
        teacherComment: json["teacher_comment"],
        //childrens: List<dynamic>.from(json["childrens"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "subject_tag_id": subjectTagId,
        "name": name,
        "name_en": nameEn,
        "short": short,
        "sort": sort,
        "max_score": maxScore,
        "absent_exam": absentExam,
        "score": score,
        "rank": rank,
        "grading": grading,
        "grading_en": gradingEn,
        "letter_grade": letterGrade,
        "teacher_comment": teacherComment,
        //"childrens": List<dynamic>.from(childrens.map((x) => x)),
    };
}
