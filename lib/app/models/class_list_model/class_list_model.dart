import 'dart:convert';

ClassModel classModelFromJson(String str) => ClassModel.fromJson(json.decode(str));

String classModelToJson(ClassModel data) => json.encode(data.toJson());

class ClassModel {
    ClassModel({
        this.data,
    });

    Data? data;

    factory ClassModel.fromJson(Map<String, dynamic> json) => ClassModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.id,
        this.objectType,
        this.name,
        this.nameEn,
        this.schoolyear,
        this.schoolType,
        this.isCurrent,
        this.calculateAdditionalSubjectMonthly,
        this.teachingClasses,

    });

    int? id;
  
    String? objectType;
    String? name;
    String? nameEn;
    String? schoolyear;
    int? schoolType;
    bool? isCurrent;
    int? calculateAdditionalSubjectMonthly;
    List<TeachingClass>? teachingClasses;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        objectType: json["object_type"],
        name: json["name"],
        nameEn: json["name_en"],
        schoolyear: json["schoolyear"],
        schoolType: json["school_type"],
        isCurrent: json["is_current"],
        calculateAdditionalSubjectMonthly: json["calculate_additional_subject_monthly"],
        teachingClasses: List<TeachingClass>.from(json["teaching_classes"].map((x) => TeachingClass.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "object_type": objectType,
        "name": name,
        "name_en": nameEn,
        "schoolyear": schoolyear,
        "school_type": schoolType,
        "is_current": isCurrent,
        "calculate_additional_subject_monthly": calculateAdditionalSubjectMonthly,
        "teaching_classes": List<dynamic>.from(teachingClasses!.map((x) => x.toJson())),
    };
}

class TeachingClass {
    TeachingClass({
        this.id,
        this.objectType,
        this.name,
        this.nameEn,
        this.schoolyear,
        this.schoolType,
        this.isInstructor,
        this.isSelected=false
          
    });

    int? id;
    String? objectType;
    String? name;
    String? nameEn;
    String? schoolyear;
    int? schoolType;
    int? isInstructor;
    bool? isSelected;

    factory TeachingClass.fromJson(Map<String, dynamic> json) => TeachingClass(
        id: json["id"],
        objectType: json["object_type"],
        name: json["name"],
        nameEn: json["name_en"],
        schoolyear: json["schoolyear"],
        schoolType: json["school_type"],
        isInstructor: json["is_instructor"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "object_type": objectType,
        "name": name,
        "name_en": nameEn,
        "schoolyear": schoolyear,
        "school_type": schoolType,
        "is_instructor": isInstructor,
    };
}