// To parse this JSON data, do
//
//     final teachingDayModel = teachingDayModelFromJson(jsonString);

import 'dart:convert';

TeachingDayModel teachingDayModelFromJson(String str) => TeachingDayModel.fromJson(json.decode(str));

String teachingDayModelToJson(TeachingDayModel data) => json.encode(data.toJson());

class TeachingDayModel {
    TeachingDayModel({
        this.status,
        this.data,
        this.message,
    });

    bool? status;
    List<DataTeachingDay>? data;
    String? message;

    factory TeachingDayModel.fromJson(Map<String, dynamic> json) => TeachingDayModel(
        status: json["status"],
        data: List<DataTeachingDay>.from(json["data"].map((x) => DataTeachingDay.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
    };
}

class DataTeachingDay {
    DataTeachingDay({
        this.month,
        this.displayMonth,
        this.year,
        this.studyDays,
    });

    String? month;
    String? displayMonth;
    String? year;
    List<StudyDay>? studyDays;

    factory DataTeachingDay.fromJson(Map<String, dynamic> json) => DataTeachingDay(
        month: json["month"],
        displayMonth: json["displayMonth"],
        year: json["year"],
        studyDays: List<StudyDay>.from(json["study_days"].map((x) => StudyDay.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "month": month,
        "displayMonth": displayMonth,
        "year": year,
        "study_days": List<dynamic>.from(studyDays!.map((x) => x.toJson())),
    };
}

class StudyDay {
    StudyDay({
        this.day,
        this.shortName,
        this.date,
        this.isActive,
        this.disable,
    });

    int? day;
    String? shortName;
    String? date;
    bool? isActive;
    bool? disable;

    factory StudyDay.fromJson(Map<String, dynamic> json) => StudyDay(
        day: json["day"],
        shortName: json["short_name"],
        date: json["date"],
        isActive: json["is_active"],
        disable: json["disable"],
    );

    Map<String, dynamic> toJson() => {
        "day": day,
        "short_name": shortName,
        "date": date,
        "is_active": isActive,
        "disable": disable,
    };
}
