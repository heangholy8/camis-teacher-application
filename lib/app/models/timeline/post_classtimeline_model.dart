// // To parse this JSON data, do
// //
// //     final postingCommentModel = postingCommentModelFromJson(jsonString);

// import 'dart:convert';

// PostingCommentModel postingCommentModelFromJson(String str) => PostingCommentModel.fromJson(json.decode(str));

// String postingCommentModelToJson(PostingCommentModel data) => json.encode(data.toJson());

// class PostingCommentModel {
//     PostingCommentModel({
//         this.data,
//     });

//     Data? data;

//     factory PostingCommentModel.fromJson(Map<String, dynamic> json) => PostingCommentModel(
//         data: Data.fromJson(json["data"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "data": data!.toJson(),
//     };
// }

// class Data {
//     Data({
//         this.id,
//         this.parent,
//         this.postText,
//         this.user,
//         this.comments,
//         this.classId,
//         this.isLike,
//         this.likeCount,
//         this.isReact,
//         this.reactionType,
//         this.totalReact,
//         this.totalReactType,
//         this.commentCount,
//         this.attachments,
//         this.youtubes,
//         this.createdDate,
//         this.updatedDate,
//     });

//     int? id;
//     int? parent;
//     String? postText;
//     User? user;
//     List<dynamic>? comments;
//     int? classId;
//     int? isLike;
//     int? likeCount;
//     int? isReact;
//     dynamic reactionType;
//     int ?totalReact;
//     List<dynamic> ?totalReactType;
//     int? commentCount;
//     List<Attachment> ?attachments;
//     List<dynamic> ?youtubes;
//     String? createdDate;
//     String? updatedDate;

//     factory Data.fromJson(Map<String, dynamic> json) => Data(
//         id: json["id"],
//         parent: json["parent"],
//         postText: json["post_text"],
//         user: User.fromJson(json["user"]),
//         comments: List<dynamic>.from(json["comments"].map((x) => x)),
//         classId: json["class_id"],
//         isLike: json["is_like"],
//         likeCount: json["like_count"],
//         isReact: json["is_react"],
//         reactionType: json["reaction_type"],
//         totalReact: json["total_react"],
//         totalReactType: List<dynamic>.from(json["total_react_type"].map((x) => x)),
//         commentCount: json["comment_count"],
//         attachments: List<Attachment>.from(json["attachments"].map((x) => Attachment.fromJson(x))),
//         youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
//         createdDate: json["created_date"],
//         updatedDate: json["updated_date"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "parent": parent,
//         "post_text": postText,
//         "user": user!.toJson(),
//         "comments": List<dynamic>.from(comments!.map((x) => x)),
//         "class_id": classId,
//         "is_like": isLike,
//         "like_count": likeCount,
//         "is_react": isReact,
//         "reaction_type": reactionType,
//         "total_react": totalReact,
//         "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x)),
//         "comment_count": commentCount,
//         "attachments": List<dynamic>.from(attachments!.map((x) => x.toJson())),
//         "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
//         "created_date": createdDate,
//         "updated_date": updatedDate,
//     };
// }

// class Attachment {
//     Attachment({
//         this.id,
//         this.fileName,
//         this.fileSize,
//         this.fileType,
//         this.fileShow,
//         this.fileThumbnail,
//         this.fileIndex,
//         this.fileArea,
//         this.objectId,
//         this.schoolUrl,
//         this.postDate,
//         this.isGdrive,
//         this.fileGdriveId,
//         this.isS3,
//     });

//     int id;
//     String fileName;
//     String fileSize;
//     String fileType;
//     String fileShow;
//     String fileThumbnail;
//     String fileIndex;
//     String fileArea;
//     String objectId;
//     String schoolUrl;
//     String postDate;
//     int isGdrive;
//     String fileGdriveId;
//     int isS3;

//     factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
//         id: json["id"],
//         fileName: json["file_name"],
//         fileSize: json["file_size"],
//         fileType: json["file_type"],
//         fileShow: json["file_show"],
//         fileThumbnail: json["file_thumbnail"],
//         fileIndex: json["file_index"],
//         fileArea: json["file_area"],
//         objectId: json["object_id"],
//         schoolUrl: json["school_url"],
//         postDate: json["post_date"],
//         isGdrive: json["is_gdrive"],
//         fileGdriveId: json["file_gdrive_id"],
//         isS3: json["is_s3"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "file_name": fileName,
//         "file_size": fileSize,
//         "file_type": fileType,
//         "file_show": fileShow,
//         "file_thumbnail": fileThumbnail,
//         "file_index": fileIndex,
//         "file_area": fileArea,
//         "object_id": objectId,
//         "school_url": schoolUrl,
//         "post_date": postDate,
//         "is_gdrive": isGdrive,
//         "file_gdrive_id": fileGdriveId,
//         "is_s3": isS3,
//     };
// }

// class User {
//     User({
//         this.id,
//         this.schoolCode,
//         this.code,
//         this.name,
//         this.nameEn,
//         this.firstname,
//         this.lastname,
//         this.firstnameEn,
//         this.lastnameEn,
//         this.gender,
//         this.genderName,
//         this.dob,
//         this.phone,
//         this.isVerify,
//         this.email,
//         this.address,
//         this.framework,
//         this.prokas,
//         this.role,
//         this.profileMedia,
//     });

//     String id;
//     String schoolCode;
//     String code;
//     String name;
//     String nameEn;
//     String firstname;
//     String lastname;
//     String firstnameEn;
//     String lastnameEn;
//     int gender;
//     String genderName;
//     String dob;
//     String phone;
//     dynamic isVerify;
//     String email;
//     String address;
//     String framework;
//     String prokas;
//     int role;
//     Attachment profileMedia;

//     factory User.fromJson(Map<String, dynamic> json) => User(
//         id: json["id"],
//         schoolCode: json["school_code"],
//         code: json["code"],
//         name: json["name"],
//         nameEn: json["name_en"],
//         firstname: json["firstname"],
//         lastname: json["lastname"],
//         firstnameEn: json["firstname_en"],
//         lastnameEn: json["lastname_en"],
//         gender: json["gender"],
//         genderName: json["gender_name"],
//         dob: json["dob"],
//         phone: json["phone"],
//         isVerify: json["is_verify"],
//         email: json["email"],
//         address: json["address"],
//         framework: json["framework"],
//         prokas: json["prokas"],
//         role: json["role"],
//         profileMedia: Attachment.fromJson(json["profileMedia"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "school_code": schoolCode,
//         "code": code,
//         "name": name,
//         "name_en": nameEn,
//         "firstname": firstname,
//         "lastname": lastname,
//         "firstname_en": firstnameEn,
//         "lastname_en": lastnameEn,
//         "gender": gender,
//         "gender_name": genderName,
//         "dob": dob,
//         "phone": phone,
//         "is_verify": isVerify,
//         "email": email,
//         "address": address,
//         "framework": framework,
//         "prokas": prokas,
//         "role": role,
//         "profileMedia": profileMedia.toJson(),
//     };
// }
