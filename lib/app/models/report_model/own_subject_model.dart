
class OwnSubjectModel {
    List<Datum>? data;

    OwnSubjectModel({
        this.data,
    });

    factory OwnSubjectModel.fromJson(Map<String, dynamic> json) => OwnSubjectModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    String? teacherId;
    String? teacherName;
    String? teacherNameEn;
    int? teacherGender;
    String? teacherPhone;
    ProfileMedia? profileMedia;
    int? subjectId;
    int? gradeSubjectId;
    String ?subjectShort;
    String? subjectName;
    String? subjectNameEn;
    dynamic description;
    int? scoreMin;
    int? scoreMax;
    dynamic isYourSubject;
    dynamic isInstructor;

    Datum({
        this.teacherId,
        this.teacherName,
        this.teacherNameEn,
        this.teacherGender,
        this.teacherPhone,
        this.profileMedia,
        this.subjectId,
        this.gradeSubjectId,
        this.subjectShort,
        this.subjectName,
        this.subjectNameEn,
        this.description,
        this.scoreMin,
        this.scoreMax,
        this.isYourSubject,
        this.isInstructor,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        teacherId: json["teacher_id"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherGender: json["teacher_gender"],
        teacherPhone: json["teacher_phone"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        subjectId: json["subject_id"],
        gradeSubjectId: json["grade_subject_id"],
        subjectShort: json["subject_short"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        description: json["description"],
        scoreMin: json["score_min"],
        scoreMax: json["score_max"],
        isYourSubject: json["is_your_subject"],
        isInstructor: json["is_instructor"],
    );

    Map<String, dynamic> toJson() => {
        "teacher_id": teacherId,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_gender": teacherGender,
        "teacher_phone": teacherPhone,
        "profileMedia": profileMedia?.toJson(),
        "subject_id": subjectId,
        "grade_subject_id": gradeSubjectId,
        "subject_short": subjectShort,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "description": description,
        "score_min": scoreMin,
        "score_max": scoreMax,
        "is_your_subject": isYourSubject,
        "is_instructor": isInstructor,
    };
}

class ProfileMedia {
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}
