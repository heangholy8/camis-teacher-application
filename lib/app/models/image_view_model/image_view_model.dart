class ImageViewModel {
  bool? status;
  ImageData? data;

  ImageViewModel({this.status, this.data});

  ImageViewModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ?  ImageData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class ImageData {
  int? id;
  int? subjectId;
  int? classId;
  int? schedule;
  int? type;
  String? examDate;
  String? month;
  String? semester;
  String? description;
  int? isApprove;
  List<Medias>? medias;

  ImageData(
      {this.id,
      this.subjectId,
      this.classId,
      this.schedule,
      this.type,
      this.examDate,
      this.month,
      this.semester,
      this.description,
      this.isApprove,
      this.medias});

  ImageData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    subjectId = json['subject_id'];
    classId = json['class_id'];
    schedule = json['schedule'];
    type = json['type'];
    examDate = json['exam_date'];
    month = json['month'];
    semester = json['semester'];
    description = json['description'];
    isApprove = json['is_approve'];
    if (json['medias'] != null) {
      medias = <Medias>[];
      json['medias'].forEach((v) {
        medias!.add( Medias.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    data['id'] = this.id;
    data['subject_id'] = this.subjectId;
    data['class_id'] = this.classId;
    data['schedule'] = this.schedule;
    data['type'] = this.type;
    data['exam_date'] = this.examDate;
    data['month'] = this.month;
    data['semester'] = this.semester;
    data['description'] = this.description;
    data['is_approve'] = this.isApprove;
    if (this.medias != null) {
      data['medias'] = this.medias!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Medias {
  int? id;
  String? fileName;
  String? fileSize;
  String? fileType;
  String? fileShow;
  String? fileThumbnail;
  String? fileIndex;
  String? fileArea;
  String? objectId;
  String? schoolUrl;
  String? postDate;
  int? isGdrive;
  String? fileGdriveId;
  int? isS3;

  Medias(
      {this.id,
      this.fileName,
      this.fileSize,
      this.fileType,
      this.fileShow,
      this.fileThumbnail,
      this.fileIndex,
      this.fileArea,
      this.objectId,
      this.schoolUrl,
      this.postDate,
      this.isGdrive,
      this.fileGdriveId,
      this.isS3});

  Medias.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fileName = json['file_name'];
    fileSize = json['file_size'];
    fileType = json['file_type'];
    fileShow = json['file_show'];
    fileThumbnail = json['file_thumbnail'];
    fileIndex = json['file_index'];
    fileArea = json['file_area'];
    objectId = json['object_id'];
    schoolUrl = json['school_url'];
    postDate = json['post_date'];
    isGdrive = json['is_gdrive'];
    fileGdriveId = json['file_gdrive_id'];
    isS3 = json['is_s3'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    data['id'] = this.id;
    data['file_name'] = this.fileName;
    data['file_size'] = this.fileSize;
    data['file_type'] = this.fileType;
    data['file_show'] = this.fileShow;
    data['file_thumbnail'] = this.fileThumbnail;
    data['file_index'] = this.fileIndex;
    data['file_area'] = this.fileArea;
    data['object_id'] = this.objectId;
    data['school_url'] = this.schoolUrl;
    data['post_date'] = this.postDate;
    data['is_gdrive'] = this.isGdrive;
    data['file_gdrive_id'] = this.fileGdriveId;
    data['is_s3'] = this.isS3;
    return data;
  }
}
