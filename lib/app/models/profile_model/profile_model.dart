// To parse this JSON data, do
//
//     final profileModel = profileModelFromJson(jsonString);

import 'dart:convert';

ProfileModel profileModelFromJson(String str) => ProfileModel.fromJson(json.decode(str));

String profileModelToJson(ProfileModel data) => json.encode(data.toJson());

class ProfileModel {
    ProfileModel({
        this.data,
    });

    Data? data;

    factory ProfileModel.fromJson(Map<String, dynamic> json) => ProfileModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.framework,
        this.prokas,
        this.role,
        this.profileMedia,
        this.qrCode,
        this.teachingSubject,
        this.instructorClass,
        this.monitorGrades,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? framework;
    String? prokas;
    int? role;
    ProfileMedia? profileMedia;
    dynamic qrCode;
    List<TeachingSubject>? teachingSubject;
    List<InstructorClass>? instructorClass;
    String? monitorGrades;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"],
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrCode: json["qr_url"],
        teachingSubject: List<TeachingSubject>.from(json["teaching_subject"].map((x) => TeachingSubject.fromJson(x))),
        instructorClass: List<InstructorClass>.from(json["instructor_class"].map((x) => InstructorClass.fromJson(x))),
        monitorGrades: json["monitor_grades"],
    );
    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "framework": framework,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
        "qr_url":qrCode,
        "teaching_subject": List<TeachingSubject>.from(teachingSubject!.map((x) => x.toJson())),
        "instructor_class": List<InstructorClass>.from(instructorClass!.map((x) => x.toJson())),
        "monitor_grades": monitorGrades,
    };
}
class ProfileMedia {
    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class TeachingSubject {
    TeachingSubject({
        this.id,
        this.name,
        this.nameEn,
        this.code,
    });

    int? id;
    String? name;
    String? nameEn;
    String? code;

    factory TeachingSubject.fromJson(Map<String, dynamic> json) => TeachingSubject(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        code: json["code"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "code": code,
    };
}

class InstructorClass {
  dynamic classId;
  String? className;

  InstructorClass({this.classId, this.className});

  InstructorClass.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    className = json['class_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    data['class_id'] = classId;
    data['class_name'] = className;
    return data;
  }
}
