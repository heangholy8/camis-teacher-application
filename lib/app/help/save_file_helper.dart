// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gal/gal.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';

Future<void> savePDFToDevice({required Uint8List pdfBytes,required BuildContext context,required String filename}) async {
  try{
    var file = File('');
    if (Platform.isIOS) {
      final dir = await getApplicationDocumentsDirectory();
      file = File("${dir.path}/$filename${DateTime.now().second}.pdf");

    }
    if (Platform.isAndroid) {
      var status = await Permission.storage.status;
      if (status != PermissionStatus.granted) {
          status = await Permission.storage.request();
      }
      if (status.isGranted) {
          const downloadsFolderPath = '/storage/emulated/0/Download/';
          Directory dir = Directory(downloadsFolderPath);
          file = File("${dir.path}/$filename${DateTime.now().second}.pdf");
      }
    }
    try {
      await file.writeAsBytes(pdfBytes);
    } on FileSystemException catch (err) {
    // handle error
  }
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(backgroundColor: Colorconstand.alertsPositive,
        content: Text("${"SAVEPDF".tr()} ${'SUCCESSFULLY'.tr()}"),
      ),
    );
  }catch(e){ 
      print("Error: $e");
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colorconstand.alertsDecline,
          content: Text("${"SAVEPDF".tr()} ${'FAIL'.tr()}"),
        ),
      );
  }

}
Future<void> sharePDF({required Uint8List pdfBytes,required BuildContext context,required String filename}) async {
  try{
    var file = File('');
    if (Platform.isIOS) {
      final dir = await getApplicationDocumentsDirectory();
      file = File("${dir.path}" + "/" + "$filename" + ".pdf");
    }
    if (Platform.isAndroid) {
      var status = await Permission.storage.status;
      if (status != PermissionStatus.granted) {
          status = await Permission.storage.request();
      }
      if (status.isGranted) {
          final dir = await getApplicationDocumentsDirectory();
          file = File("${dir.path}" + "/" + "$filename" + ".pdf");
          if (!file.existsSync()) {
            file.create(recursive: true);
          }
      }
      final dir = await getApplicationDocumentsDirectory();
      file = File("${dir.path}" + "/" + "$filename" + ".pdf");
      if (!file.existsSync()) {
        file.create(recursive: true);
      }
    }
    try {
      await file.writeAsBytes(pdfBytes).then((value){
        Share.shareXFiles([XFile(file.path)],text: "CAMEMIS Report");
      });
    } on FileSystemException catch (err) {
    // handle error
  }
    // ScaffoldMessenger.of(context).showSnackBar(
    //   const SnackBar(backgroundColor: Colorconstand.alertsPositive,
    //     content: Text('PDF saved successfully'),
    //   ),
    // );
  }catch(e){
      // print("Error: $e");
      // ScaffoldMessenger.of(context).showSnackBar(
      //   const SnackBar(
      //     backgroundColor: Colorconstand.alertsDecline,
      //     content: Text('Failed to save PDF'),
      //   ),
      // );
  }

}

Future<void> saveImgae({required String image}) async {
  final path = '${Directory.systemTemp.path}/image.jpg';
  await Dio().download(image, path,);
  await Gal.putImage(path, album: "Camemis App Teacher");
}

