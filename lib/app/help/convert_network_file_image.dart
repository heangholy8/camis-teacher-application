// ignore_for_file: depend_on_referenced_packages

import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';

Future<File> convertNetworkImageToImageFile(String imageUrl) async {
  final response = await http.get(Uri.parse(imageUrl));
  final documentDirectory = await getApplicationDocumentsDirectory();
  final datetime = DateTime.now();
  final file = File('${documentDirectory.path}/$datetime image.jpg');
  await file.writeAsBytes(response.bodyBytes);
  return file;
}


// Future<File> convertImageUrlToFile(String imageUrl) async {
//   try {
//     final response = await http.get(Uri.parse(imageUrl));
//     final bytes = response.bodyBytes;
//     final file = File.fromRawPath(Uint8List.fromList(bytes));
//     print(file.path);
//     return file;
//   } catch (e) {
//     throw Exception('Failed to convert image URL to file: $e');
//   }
// }

Future<File> convertImageUrlToFile(String imageUrl) async {
  try {
    
    final response = await http.get(Uri.parse(imageUrl));
    final bytes = response.bodyBytes;
  final datetime = DateTime.now().millisecond;
    if (bytes.isNotEmpty) {
      final directory = Directory.systemTemp;
      final file = File('${directory.path}/$datetime image.png');
      await file.writeAsBytes(bytes);
      print(file.path);
      return file;
    } else {
      throw Exception('Empty or invalid image URL');
    }
  } catch (e) {
    throw Exception('Failed to convert image URL to file: $e');
  }
}