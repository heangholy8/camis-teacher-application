  import 'package:easy_localization/easy_localization.dart';

String convertMinutes(String minutesString) {
  int minutes = int.parse(minutesString.replaceAll('min','').trim());
  int hours = minutes ~/ 60;
  int remainingMinutes = minutes % 60;

  if(minutes < 60){
    return "$minutes ${"MINUTE".tr()}";
  }
  else if(minutes == 60) {
    return '${hours.toString()} ${"HOUR".tr()}';
  }else{
    return '${hours.toString()} ${"HOUR".tr()} :${remainingMinutes.toString().padLeft(2, '0')} ${"MINUTE".tr()}';
  }
}