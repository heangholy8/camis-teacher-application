import 'package:camis_teacher_application/app/bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/calculate_score/bloc/calculate_score_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/set_exam/bloc/set_exam_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/state_attendance/bloc/get_date_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/state_attendance/bloc/get_schedule_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/state_month/bloc/month_exam_set_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/state_set_time_exam/bloc/schedule_subject_time_in_day_bloc.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/State/get_summary_att/bloc/summary_attendance_day_bloc.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/cubit_attach/cubit_attachment.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/cubit/select_media_cubit.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/bloc/view_image_bloc.dart';
import 'package:camis_teacher_application/app/modules/help_screen/state/bloc/contact_bloc.dart';
import 'package:camis_teacher_application/app/modules/home_screen/bloc/daskboard_screen_bloc.dart';
import 'package:camis_teacher_application/app/modules/primary_school_screen/list_enter_score_primary/state/list_all_subject_primary/bloc/list_subject_enter_score_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/list_check_class_input_score_screen/state/list_all_subject_header/bloc/list_all_subject_header_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/list_class_check_attendance_screen/state/list_hour_in_date/bloc/hour_in_date_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/principle_home_screen/state/bloc/get_teacher_attendance_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/state/all_year_in_school/get_year_school_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/state/summary_study_student/summary_student_result_bloc.dart';
import 'package:camis_teacher_application/app/modules/profile_user/bloc/bloc/update_profile_bloc.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/bloc/post_attendance_bloc.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/cubit/image_selected_cubit.dart';
import 'package:camis_teacher_application/app/modules/report_screen/state/attendance_print_state/bloc/attendance_print_bloc.dart';
import 'package:camis_teacher_application/app/modules/report_screen/state/honorary_list_print_state/bloc/honorary_list_bloc.dart';
import 'package:camis_teacher_application/app/modules/report_screen/state/sec_table_inclass/bloc/sec_table_in_class_bloc.dart';
import 'package:camis_teacher_application/app/modules/report_screen/state/teacher_attendance/teacher_attendance_bloc.dart';
import 'package:camis_teacher_application/app/modules/request_absent_screen/bloc/list_permission_request/list_all_permission_request_bloc.dart';
import 'package:camis_teacher_application/app/modules/request_absent_screen/bloc/own_present_in_month/get_own_present_bloc.dart';
import 'package:camis_teacher_application/app/modules/result_screen/state/bloc/monthly_result_bloc.dart';
import 'package:camis_teacher_application/app/modules/scholarship_screen/bloc/schoolarship_bloc.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/update_student_profile_bloc.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/state/current_grade/bloc/current_grade_bloc.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/state/daily_attendace/bloc/daily_attendance_affaire_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/api/teacher_unlock_api.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/check_school_pachage/check_school_use_package_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/list_guardian_request_unlock/list_guardian_request_unlock_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/list_student_to_unlock/list_student_to_unlock_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/create_payment/bloc/create_payment_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/payment_option/bloc/payment_option_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/student_list_payment/bloc/student_list_payment_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/teacher_commission/bloc/teacher_commission_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_list_screen/bloc/teacher_list_bloc.dart';
import 'package:camis_teacher_application/app/modules/time_line/school_time_line/bloc/post_school_timeline/post_school_timeline_bloc.dart';
import 'package:camis_teacher_application/app/service/api/calcultion_score/calculation_score_api.dart';
import 'package:camis_teacher_application/app/service/api/class_api/get_class_api.dart';
import 'package:camis_teacher_application/app/service/api/get_contact_api/get_contect_api.dart';
import 'package:camis_teacher_application/app/service/api/get_current_grades_affaire_api/current_grades_affaire.dart';
import 'package:camis_teacher_application/app/service/api/get_daily_attendance_affaire_api/daily_attendance_affaire.dart';
import 'package:camis_teacher_application/app/service/api/get_list_enter_score_primary_school_api/list_enter_score_primary_api.dart';
import 'package:camis_teacher_application/app/service/api/get_list_month_with_semester/get_list_month_semester_api.dart';
import 'package:camis_teacher_application/app/service/api/get_list_result_primary_school/get_result_primary_school_api.dart';
import 'package:camis_teacher_application/app/service/api/grading_api/get_enter_score_feature_api.dart';
import 'package:camis_teacher_application/app/service/api/grading_api/grading_api.dart';
import 'package:camis_teacher_application/app/service/api/principle_api/feedback_api/feedback_api.dart';
import 'package:camis_teacher_application/app/service/api/principle_api/summary_information_school/summary_information_school_api.dart';
import 'package:camis_teacher_application/app/service/api/profile_api/get_user_profile_api.dart';
import 'package:camis_teacher_application/app/service/api/profile_api/update_profile_api.dart';
import 'package:camis_teacher_application/app/service/api/report_api/own_subject_api.dart';
import 'package:camis_teacher_application/app/service/api/request_permission_api/get_all_permission_request.dart';
import 'package:camis_teacher_application/app/service/api/request_permission_api/get_own_present_in_month.dart';
import 'package:camis_teacher_application/app/service/api/schedule_api/get_date_in_month_api.dart';
import 'package:camis_teacher_application/app/service/api/schedule_api/set_exam_api.dart';
import 'package:camis_teacher_application/app/service/api/schoolar_api/request_schoolartype_api.dart';
import 'package:camis_teacher_application/app/service/api/student_in_class_list/get_Student_In_Class_Api.dart';
import 'package:camis_teacher_application/app/service/api/student_in_class_list/update_sutdent_api.dart';
import 'package:camis_teacher_application/app/service/api/teacher_commission_api/create_payment.dart';
import 'package:camis_teacher_application/app/service/api/teacher_commission_api/get_payment_option.dart';
import 'package:camis_teacher_application/app/service/api/teacher_commission_api/get_student_list_payment.dart';
import 'package:camis_teacher_application/app/service/api/teacher_commission_api/get_teacher_commission.dart';
import 'package:camis_teacher_application/app/service/api/teacher_commission_api/upload_attachment_api.dart';
import 'package:camis_teacher_application/app/service/api/teacher_list/teacher_list_api.dart';
import 'package:camis_teacher_application/app/service/api/time_line_api/get_time_line_api.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/check_update_infor/bloc/update_user_info_bloc.dart';
import '../bloc/check_update_version/bloc/check_version_update_bloc.dart';
import '../modules/account_confirm_screen/bloc/profile_user_bloc.dart';
import '../modules/attendance_schedule_screen/state_management/state_exam/bloc/exam_schedule_bloc.dart';
import '../modules/check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../modules/grading_screen/bloc/bloc/check_switch_enter_score_screen_bloc.dart';
import '../modules/grading_screen/bloc/grading_bloc.dart';
import '../modules/home_screen/bloc/my_task_daskboard_bloc.dart';
import '../modules/login_screen/bloc/auth_bloc.dart';
import '../modules/login_screen/bloc/bloc/list_school_bloc.dart';
import '../modules/primary_school_screen/result_primary_school/state/result_primary_state/bloc/result_primary_school_bloc.dart';
import '../modules/primary_school_screen/result_student_primary_detail/state/result_detail_term_month/bloc/result_term_month_detail_bloc.dart';
import '../modules/principle/feadback_screen/state/get_feedback/bloc/get_feedback_guardian_bloc.dart';
import '../modules/principle/list_check_class_input_score_screen/state/list_check_input_score/bloc/list_check_input_score_bloc.dart';
import '../modules/principle/list_class_check_attendance_screen/state/list_check_attendance_in_class/bloc/get_class_check_attendance_bloc.dart';
import '../modules/principle/list_class_check_attendance_screen/state/list_check_attendance_in_class_dashboard/bloc/get_list_attendance_dashboard_bloc.dart';
import '../modules/principle/summary_info_school_screen/state/summary_student_teacher_in_class/summary_student_teacher_in_class_bloc.dart';
import '../modules/report_screen/state/monthly_print_state/print_bloc_bloc.dart';
import '../modules/report_screen/state/own_subject_state/bloc/ownsubject_bloc.dart';
import '../modules/report_screen/state/report_for_big_book_state/bloc/print_report_big_book_bloc.dart';
import '../modules/report_screen/state/result_all_subject_print_state/bloc/result_all_subject_bloc.dart';
import '../modules/report_screen/state/semester_print_state/bloc/semester_print_report_bloc.dart';
import '../modules/request_absent_screen/bloc/reason_bloc/request_absent_bloc.dart';
import '../modules/scholarship_screen/bloc/get_all_request_bloc/get_all_request_bloc.dart';
import '../modules/scholarship_screen/cubit/height_bts_cubit.dart';
import '../modules/scholarship_screen/cubit/listing_exemption_cubit.dart';
import '../modules/scholarship_screen/cubit/upload_attach_cubit.dart';
import '../modules/set_mazer_screen/bloc/assignmonitor_bloc.dart';
import '../modules/set_mazer_screen/bloc/bloc/set_class_monitor_bloc.dart';
import '../modules/student_list_screen/bloc/student_in_class_bloc.dart';
import '../modules/study_affaire_screen/enter_score_screen/state/bloc/student_affair_level_bloc.dart';
import '../modules/study_affaire_screen/list_attendance_teacher/state/list_attendance_teacher_state/bloc/attendance_teacher_bloc.dart';
import '../modules/study_affaire_screen/report_study_affaire/state/list_subject_in_class/bloc/list_subject_in_class_bloc.dart';
import '../modules/teacher_commission_screen/states/check_transaction_payment/bloc/check_transaction_bloc.dart';
import '../modules/teacher_commission_screen/states/teacher_commission _report/bloc/teacher_commission_report_bloc.dart';
import '../modules/teacher_commission_screen/states/upload_attachment_commission/bloc/post_attachment_bloc.dart';
import '../modules/time_line/class_time_line/bloc/class_time_line_bloc.dart';
import '../modules/time_line/class_time_line/post_bloc/bloc/post_classtimeline_bloc_bloc.dart';
import '../modules/time_line/school_time_line/bloc/school_time_line_bloc.dart';
import '../modules/tracking_location_teacher/bloc/tracking_location_bloc.dart';
import '../modules/view_request_permission_screen/bloc/get_list_permission_request_bloc.dart';
import '../service/api/attachment_api/get_attachment_api.dart';
import '../service/api/auth_api/auth_api.dart';
import '../service/api/auth_api/get_list_school.dart';
import '../service/api/check_attendance_api/get_list_student_attendance.dart';
import '../service/api/check_update_api/check_update.dart';
import '../service/api/daskboard_api/daskboard_api.dart';
import '../service/api/principle_api/check_class_input_score_principle/get_list_check_input_score_api.dart';
import '../service/api/principle_api/get_hour_in_date_api/get_hour_in_date_api.dart';
import '../service/api/principle_api/get_list_class_check_attendance/get_class_check_attendance_api.dart';
import '../service/api/print_api/print_report_api.dart';
import '../service/api/profile_api/post_user_profile.dart';
import '../service/api/request_permission_api/get_reason_api.dart';
import '../service/api/result_api/list_student_monthly_result.dart';
import '../service/api/schedule_api/get_exam_schedule_api.dart';
import '../service/api/schedule_api/get_schedule_api.dart';
import '../service/api/student_requst_permission_api/requst_permission_api.dart';
import '../service/api/teacher_attendance_api/get_teacher_attendance_for_study_affair_api.dart';
import '../service/api/teacher_commission_api/get_transaction_api.dart';
import '../service/api/teaching_day_api/get_teaching_day_api.dart';
import '../service/api/teaching_time/teaching_time_api.dart';

List<BlocProvider> _listBlocProvider = [
  BlocProvider<AuthBloc>(create: (context) => AuthBloc(authApi: AuthApi(),),),
  BlocProvider<GetClassBloc>(create: (context) => GetClassBloc(getclass: GetClassApi(),),),
  BlocProvider<GetStudentInClassBloc>(create: (context) => GetStudentInClassBloc(getstudentinclass: GetStudentInClassApi(),),),
  BlocProvider<TeacherListBloc>(create: (context) => TeacherListBloc(teacherListApi: GetTeacherListApi(),),),
  BlocProvider<ProfileUserBloc>(create: (context) => ProfileUserBloc(profileUserApi: GetProfileUserApi(),),),
  BlocProvider<UpdateUserInfoBloc>(create: (context) =>UpdateUserInfoBloc(userUpadeData: UpdateProfileUserApi()),),
  BlocProvider<GetDateBloc>(create: (context) => GetDateBloc(listDate: GetListDateApi()),),
  BlocProvider<GetScheduleBloc>(create: (context) => GetScheduleBloc(classSchedule: GetScheduleApi(), mySchedule: GetScheduleApi()),),
  BlocProvider<UpdateProfileBloc>(create: (context) =>UpdateProfileBloc(updateProfileApi: UpdateProfileApi())),
  BlocProvider<ListSchoolBloc>(create: (context) => ListSchoolBloc(getListSchool: GetListSchoolApi()),),
  BlocProvider<AssignmonitorBloc>(create: (context) => AssignmonitorBloc(getClassStudent: GetStudentInClassApi()),),
  BlocProvider<CheckAttendanceBloc>(create: (context) => CheckAttendanceBloc(getCheckStudentAttendanceList: GetCheckStudentAttendanceList(),),),
  BlocProvider<GradingBloc>(create: (context) => GradingBloc(getGradingData: GradingApi(),),),
  BlocProvider<ListMonthSemesterBloc>(create: (context) => ListMonthSemesterBloc(getListMonthExamApi: GetListMonthSemesterApi(),),),
  BlocProvider<ExamScheduleBloc>(create: (context) => ExamScheduleBloc(classExamSchedule: GetExamScheduleApi(),myExamSchedule: GetExamScheduleApi()),),
  BlocProvider<PostAttendanceBloc>(create: (context) => PostAttendanceBloc(),),
  BlocProvider<SetClassMonitorBloc>(create: (context) => SetClassMonitorBloc(setClassMonitorApi: GetStudentInClassApi(),),),
  BlocProvider<MonthExamSetBloc>(create: (context) => MonthExamSetBloc(dayApi: GetTeachingDayApi(),),),
  BlocProvider<ScheduleSubjectTimeInDayBloc>(create: (context) => ScheduleSubjectTimeInDayBloc(timeScheduleApi: GetTeachingTimeApi(),),),
  BlocProvider<DaskboardScreenBloc>(create: (context) => DaskboardScreenBloc(daskboardApi: GetDaskboardApi(),),),
  BlocProvider<MyTaskDaskboardBloc>(create: (context) => MyTaskDaskboardBloc(daskboardApi: GetDaskboardApi(),),),
  BlocProvider<ImageSelectImageBloc>( create: ((context) => ImageSelectImageBloc()),),
  BlocProvider<AttachSelectImageBloc>( create: ((context) => AttachSelectImageBloc()),),
  BlocProvider<MediaCubit>(create: (context) => MediaCubit(),),
  BlocProvider<SetExamBloc>( create: ((context) => SetExamBloc(setExam: SetExamApi())),),
  BlocProvider<CalculateScoreBloc>( create: ((context) => CalculateScoreBloc(calculateScore: CalculationScoreApi())),),
  BlocProvider<UpdateStudentProfileBloc>( create: ((context) => UpdateStudentProfileBloc(updateStudentInfo: UpdateStudentProfileApi())),),
  BlocProvider<MonthlyResultBloc>( create: ((context) => MonthlyResultBloc(resultApi: GetResultApi())),),
  BlocProvider<StudentListPaymentBloc>( create: ((context) => StudentListPaymentBloc(dataStudentListPayment: GetStudentListPaymentApi())),),
  BlocProvider<CheckVersionUpdateBloc>(create: (context) => CheckVersionUpdateBloc(checkUpdateVersion: CheckUpdateVersionApi(),),),
  BlocProvider<AuthBloc>(
    create: (context) => AuthBloc(
      authApi: AuthApi(),
    ),
  ),
  BlocProvider<GetClassBloc>(
    create: (context) => GetClassBloc(
      getclass: GetClassApi(),
    ),
  ),
  BlocProvider<GetStudentInClassBloc>(
    create: (context) => GetStudentInClassBloc(
      getstudentinclass: GetStudentInClassApi(),
    ),
  ),
  BlocProvider<TeacherListBloc>(
    create: (context) => TeacherListBloc(
      teacherListApi: GetTeacherListApi(),
    ),
  ),
  BlocProvider<ProfileUserBloc>(
    create: (context) => ProfileUserBloc(
      profileUserApi: GetProfileUserApi(),
    ),
  ),
  BlocProvider<UpdateUserInfoBloc>(
    create: (context) =>
        UpdateUserInfoBloc(userUpadeData: UpdateProfileUserApi()),
  ),
  BlocProvider<GetDateBloc>(
    create: (context) => GetDateBloc(listDate: GetListDateApi()),
  ),
  BlocProvider<GetScheduleBloc>(
    create: (context) => GetScheduleBloc(
        classSchedule: GetScheduleApi(), mySchedule: GetScheduleApi()),
  ),
  BlocProvider<UpdateProfileBloc>(
      create: (context) =>
          UpdateProfileBloc(updateProfileApi: UpdateProfileApi())),
  BlocProvider<ListSchoolBloc>(
    create: (context) => ListSchoolBloc(getListSchool: GetListSchoolApi()),
  ),
  BlocProvider<AssignmonitorBloc>(
    create: (context) =>
        AssignmonitorBloc(getClassStudent: GetStudentInClassApi()),
  ),
  BlocProvider<CheckAttendanceBloc>(
    create: (context) => CheckAttendanceBloc(
      getCheckStudentAttendanceList: GetCheckStudentAttendanceList(),
    ),
  ),
  BlocProvider<GradingBloc>(
    create: (context) => GradingBloc(
      getGradingData: GradingApi(),
    ),
  ),
  BlocProvider<ListMonthSemesterBloc>(
    create: (context) => ListMonthSemesterBloc(
      getListMonthExamApi: GetListMonthSemesterApi(),
    ),
  ),
  BlocProvider<ExamScheduleBloc>(
    create: (context) => ExamScheduleBloc(
        classExamSchedule: GetExamScheduleApi(),
        myExamSchedule: GetExamScheduleApi()),
  ),
  BlocProvider<PostAttendanceBloc>(
    create: (context) => PostAttendanceBloc(),
  ),
  BlocProvider<SetClassMonitorBloc>(
    create: (context) => SetClassMonitorBloc(
      setClassMonitorApi: GetStudentInClassApi(),
    ),
  ),
  BlocProvider<MonthExamSetBloc>(
    create: (context) => MonthExamSetBloc(
      dayApi: GetTeachingDayApi(),
    ),
  ),
  BlocProvider<DaskboardScreenBloc>(
    create: (context) => DaskboardScreenBloc(
      daskboardApi: GetDaskboardApi(),
    ),
  ),
  BlocProvider<ImageSelectImageBloc>(
    create: ((context) => ImageSelectImageBloc()),
  ),
   BlocProvider<ViewImageBloc>(
    create: ((context) => ViewImageBloc(getViewImageData: GetAttachmentApi())),
  ),
  BlocProvider<MediaCubit>(
    create: (context) => MediaCubit(),
  ),

  BlocProvider<SchoolSocialBloc>(create: (context)=> SchoolSocialBloc(GetSchoolTimeLineApi())),
  BlocProvider<SchoolTimeLineDetailBloc>(create: (context)=>SchoolTimeLineDetailBloc(getSchooltimelinedetailapi: GetSchoolTimeLineApi())),
  BlocProvider<ClassTimeLineBloc>(create: (context)=>ClassTimeLineBloc(getSchooltimelineapi: GetSchoolTimeLineApi())),
  BlocProvider<ClassTimeLineDetailBloc>(create: (context)=>ClassTimeLineDetailBloc(getClasstimelinedetailapi: GetSchoolTimeLineApi())),
  BlocProvider<AttachCubit>(create: (context)=> AttachCubit()),
  BlocProvider<PostClasstimelineBlocBloc>(create: (context)=>PostClasstimelineBlocBloc()),
  BlocProvider<PostSchoolTimelineBloc>(create: (context)=>PostSchoolTimelineBloc()),
  BlocProvider<heightBtsCubit>(create: (context)=>heightBtsCubit()),
  BlocProvider<ExemptionCubit>(create: (context)=>ExemptionCubit()),
  BlocProvider<SchoolarshipBloc>(create: (context)=>SchoolarshipBloc(schoolarShipApi: GetSchoolarClass())),
  BlocProvider<GetAllRequestBloc>(create: (context)=>GetAllRequestBloc(schoolarShipApi: GetSchoolarClass())),
  BlocProvider<PrintBlocBloc>(create: (context)=>PrintBlocBloc()),
  BlocProvider<TeacherCommissionBloc>(create: (context)=>TeacherCommissionBloc(dataCommission:GetCommissionApi())),
  BlocProvider<TeacherCommissionReportBloc>(create: (context)=>TeacherCommissionReportBloc(dataCommissionReport:GetCommissionApi())),
  BlocProvider<PaymentOptionBloc>(create: (context)=>PaymentOptionBloc(currenApi:PaymentOptionApi())),
  BlocProvider<CreatePaymentBloc>(create: (context)=>CreatePaymentBloc(createPayment:CreatePaymentApi())),
  BlocProvider<CheckTransactionBloc>(create: (context) => CheckTransactionBloc(transactionApi: GetTransactionApi(),),),
  BlocProvider<SummaryAttendanceDayBloc>(create: (context) => SummaryAttendanceDayBloc(getSummaryStudentAttendanceList: GetCheckStudentAttendanceList(),),),
  BlocProvider<PostAttachmentBloc>(create: (context) => PostAttachmentBloc(uploadAttachment: UploadAttachmentCommissionApi(),),),
  BlocProvider<AttendancePrintBloc>(create: (context)=>AttendancePrintBloc(printReportApi: PrintReport())),
  BlocProvider<OwnsubjectBloc>(create: (context)=>OwnsubjectBloc(ownSubjectListApi: OwnSubjectListApi())),
  BlocProvider<SemesterPrintReportBloc>(create: (context)=>SemesterPrintReportBloc(printSemesterReport: PrintReport())),
  BlocProvider<ResultAllSubjectBloc>(create: (context)=>ResultAllSubjectBloc(printSemesterReport: PrintReport())),
  BlocProvider<HonoraryListBloc>(create: (context)=>HonoraryListBloc(printHonoraryList: PrintReport())),
  BlocProvider<SecTableInClassBloc>(create: (context)=>SecTableInClassBloc(printSECTableReport: PrintReport())),
  BlocProvider<PrintReportBigBookBloc>(create: (context)=>PrintReportBigBookBloc(printSemesterReport: PrintReport())),
  BlocProvider<CheckSwitchEnterScoreScreenBloc>(create: (context)=>CheckSwitchEnterScoreScreenBloc(getCheckSwitcheScreen: GetApiCheckConditionSwitchEnterScore())),
  BlocProvider<GetListPermissionRequestBloc>(create: (context)=>GetListPermissionRequestBloc(requestPermissionApi: RequestPermissionApi())),
  BlocProvider<RequestReasonBloc>(create: (context)=>RequestReasonBloc(getReasonApi: GetReasonApi())),
  BlocProvider<GetOwnPresentBloc>(create: (context)=>GetOwnPresentBloc(getOwnPresentApi: GetOwnPresentApi())),
  BlocProvider<ListAllPermissionRequestBloc>(create: (context)=>ListAllPermissionRequestBloc(getAllPermissionRequest: GetAllPermissionRequestApi())),
  BlocProvider<ContactBloc>(create: (context) => ContactBloc(getContactApi: GetContactApi()),),
  BlocProvider<CheckSchoolUsePackageBloc>(create: (context) => CheckSchoolUsePackageBloc(teacherUnlockGuardianUseAppApi: TeacherUnlockGuardianUseAppApi()),),
  BlocProvider<ListStudentToUnlockBloc>(create: (context) => ListStudentToUnlockBloc(teacherUnlockGuardianUseAppApi: TeacherUnlockGuardianUseAppApi()),),
  BlocProvider<ListGuardianRequestUnlockBloc>(create: (context) => ListGuardianRequestUnlockBloc(teacherUnlockGuardianUseAppApi: TeacherUnlockGuardianUseAppApi()),),
  //========================= Study Affaire ========================================
  BlocProvider<ListSubjectInClassBloc>(create: (context)=>ListSubjectInClassBloc(ownSubjectListApi: OwnSubjectListApi())),
  BlocProvider<DailyAttendanceAffaireBloc>(create: (context)=>DailyAttendanceAffaireBloc(getDailyAttendanceAffaire: GetDailyAtytendanceAffaireApi())),
  BlocProvider<CurrentGradeBloc>(create: (context)=>CurrentGradeBloc(currentGradeData: GetCurrentGradeAffaireApi())),
  BlocProvider<StudentAffairLevelBloc>(create: (context)=>StudentAffairLevelBloc(getclassByLevelApi: GetCurrentGradeAffaireApi())),
  BlocProvider<TrackingLocationBloc>(create: (context) => TrackingLocationBloc()),
  BlocProvider<AttendanceTeacherBloc>(create: (context) => AttendanceTeacherBloc(getTeacherAttendanceForStudyAffairApi:GetTeacherAttendanceForStudyAffairApi())),

  //=======================Primary School ======================= 
  BlocProvider<ResultPrimarySchoolBloc>(create: (context)=>ResultPrimarySchoolBloc(resultAllSubjectPrimaryApi: GetPrimaryResultApi())),
  BlocProvider<ListSubjectEnterScoreBloc>(create: (context)=>ListSubjectEnterScoreBloc(listAllSubjectPrimaryApi: GetPrimaryListSubjectApi())),
  BlocProvider<ResultTermMonthDetailBloc>(create: (context)=>ResultTermMonthDetailBloc(resultDetailStudentMonthSemesterPrimaryApi: GetPrimaryResultApi())),

  //=========================== Principle ===========================
  BlocProvider<ListCheckInputScoreBloc>(create: (context)=>ListCheckInputScoreBloc(getCheckClassInputScoreApi: GetCheckClassInputScoreApi())),
  BlocProvider<ListAllSubjectHeaderBloc>(create: (context)=>ListAllSubjectHeaderBloc(getListAllSubjectHeaderApi: GetCheckClassInputScoreApi())),
  BlocProvider<HourInDateBloc>(create: (context)=>HourInDateBloc(getHourInDateApi: GetHourInDateApi())),
  BlocProvider<GetClassCheckAttendanceBloc>(create: (context)=>GetClassCheckAttendanceBloc(getClassCheckAttApi: GetClassCheckAttApi())),
  BlocProvider<GetTeacherAttendanceBloc>(create: (context)=>GetTeacherAttendanceBloc(getClassCheckAttApi: GetClassCheckAttApi())),
  BlocProvider<GetListAttendanceDashboardBloc>(create: (context)=>GetListAttendanceDashboardBloc(getClassCheckAttApi: GetClassCheckAttApi())),
  BlocProvider<GetFeedbackGuardianBloc>(create: (context)=>GetFeedbackGuardianBloc(feedbackapi: FeedbackApi())),
  BlocProvider<GetYearSchoolBloc>(create: (context)=>GetYearSchoolBloc(getSummaryInforSchoolApi: GetSummaryInforSchoolApi())),
  BlocProvider<SummaryStudentTeacherInClassBloc>(create: (context)=>SummaryStudentTeacherInClassBloc(getSummaryInforSchoolApi: GetSummaryInforSchoolApi())),
  BlocProvider<SummaryStudentResultBloc>(create: (context)=>SummaryStudentResultBloc(getSummaryInforSchoolApi: GetSummaryInforSchoolApi())),
  BlocProvider<TeacherAttendanceBloc>(create: (context)=>TeacherAttendanceBloc(printAttTeacher: PrintReport())),
  
];

List<BlocProvider> get listBlocProvider => _listBlocProvider;
