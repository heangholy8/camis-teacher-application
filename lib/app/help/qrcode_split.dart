import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../modules/login_screen/bloc/auth_bloc.dart';

void qrCodeSplit(String code, context) {
  var newCode = code.split(";");
  var schoolId = newCode[0].split(":");
  var loginName = newCode[1].split(":");
  var password = newCode[2].split(":");
  debugPrint("QR Code Translator => ${schoolId[1]} ${loginName[1]} ${password[1]}",
  );
  BlocProvider.of<AuthBloc>(context).add(
    SignInRequestedEvent(
      schoolCode: schoolId[1].toString(),
      loginName: loginName[1].toString(),
      password: password[1].toString(),
    ),
  );
}
