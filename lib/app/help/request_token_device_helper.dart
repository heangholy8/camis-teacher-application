// // ignore_for_file: unused_local_variable
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';

// class RequestPermissionFirebaseMessaging {
//   // RequestPermission Firebase Messaging
//   void requestPermission() async {
//     FirebaseMessaging messaging = FirebaseMessaging.instance;
//     NotificationSettings settings = await messaging.requestPermission(
//       alert: true,
//       announcement: false,
//       badge: true,
//       carPlay: false,
//       criticalAlert: false,
//       provisional: false,
//       sound: true,
//     );
//   }

//   //Get Device Token
//   getDeviceToken() async {
//     String token = await FirebaseMessaging.instance.getToken().then((value) {
//       // debugPrint("Device token=> $value");
//       return value.toString();
//     });
//   }

//   // RequestMessaging BackgroundHandler
//   Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//     await Firebase.initializeApp();
//   }
// }
