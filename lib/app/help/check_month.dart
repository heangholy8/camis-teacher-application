
import 'package:easy_localization/easy_localization.dart';

checkMonth( int month) {
  if (month == 1) {
    return "JANUARY".tr();
  } else if (month == 2) {
    return "FEBRUARY".tr();
  }else if (month == 3) {
    return "MARCH".tr();
  }else if (month == 4) {
    return "APRIL".tr();
  }else if (month == 5) {
    return "MAY".tr();
  }else if (month == 6) {
    return "JUNE".tr();
  }else if (month == 7) {
    return "JULY".tr();
  }else if (month == 8) {
    return "AUGUST".tr();
  }else if (month == 9) {
    return "SEPTEMBER".tr();
  }else if (month == 10) {
    return "OCTOBER".tr();
  }else if (month == 11) {
    return "NOVEMBER".tr();
  }else if (month == 12) {
    return "DECEMBER".tr();
  }
}
