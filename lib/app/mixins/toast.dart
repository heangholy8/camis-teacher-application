import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../widget/alert_widget/custom_alertdialog.dart';
import '../core/thems/thems_constands.dart';

mixin Toast {
  Future<T> showSuccessDialog<T>(
   {required Function() callback,
    String? title,
    String? discription,
    context,
   }) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          blurColor: Colorconstand.primaryColor.withOpacity(0.1),
          title: title ?? "SUCCESSFULLY".tr(),
          subtitle:discription ?? "SUCCESS_SUBTITLE".tr(),
          onPressed: callback,
          // () {
          //   Navigator.push(
          //     context,
          //     PageTransition(
          //       child: const HomeScreen(),
          //       type: PageTransitionType.rightToLeft,
          //     ),
          //   );
          //},
          titleButton: 'OKAY'.tr(),
          buttonColor: Colorconstand.primaryColor,
          titlebuttonColor: Colorconstand.neutralWhite,
          child: Icon(
            Icons.check_circle,
            color: Colorconstand.primaryColor,
            size: MediaQuery.of(context).size.width / 6,
          ),
        );
      },
    );
  }

  Future<T> showDialogFailImage<T>(
    Function() callback,
    context,
  ) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          child: Icon(
            Icons.warning_rounded,
            color: Colors.red,
            size: MediaQuery.of(context).size.width / 6,
          ),
          title: "INVALID_CODE".tr(),
          subtitle: "INVALID_IMAGE".tr(),
          onPressed: callback,
          titleButton: "AGAIN".tr(),
          buttonColor: Colors.red,
          titlebuttonColor: Colorconstand.neutralWhite,
        );
      },
    );
  }

  Future<T> showErrorDialog<T>(Function() callback, context,
      {String? title, String? description}) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          title: title ?? "INVALID_CODE".tr(),
          subtitle: description ?? "INVALID_SUBTITLE".tr(),
          onPressed: callback,
          titleButton: 'TRYAGAIN'.tr(),
          buttonColor: Colors.red,
          titlebuttonColor: Colorconstand.neutralWhite,
          child: Icon(
            Icons.cancel,
            color: Colors.red,
            size: MediaQuery.of(context).size.width / 6,
          ),
        );
      },
    );
  }

  Future<T> showAlertLogout<T>(
      {required String title,
      void Function()? onSubmit, 
      void Function()? onCancel,
      String? onSubmitTitle,
      Color? bgColorSubmitTitle,
      Color? bgColoricon,
      String? onCancelTitle,
      Widget? icon,
      required BuildContext context}) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return Dialog(
          elevation: 5,
          backgroundColor: Colorconstand.neutralWhite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 25),
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width / 6,
                  height: MediaQuery.of(context).size.width / 6,
                  decoration: BoxDecoration(
                      color:bgColoricon??Colorconstand.alertsNotifications.withOpacity(0.1),
                      shape: BoxShape.circle),
                  child: Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: icon ?? Icon(
                      Icons.info,
                      color: Colors.green,
                      size: MediaQuery.of(context).size.width / 6,
                    ),
                  ),
                ),
                Container(
                    margin:
                        const EdgeInsets.only(top: 20, left: 17.0, right: 17.0),
                    child: Align(
                      child: Text(
                        title,
                        style: ThemsConstands.button_semibold_16.copyWith(
                            color: Colorconstand.lightBulma,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.center,
                      ),
                    )),
                Container(
                  margin: const EdgeInsets.only(top: 25, bottom: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Button_Custom(
                          onPressed: onCancel ?? (){
                            Navigator.pop(context);
                          },
                          buttonColor: Colorconstand.neutralGrey,
                          hightButton: 45,
                          radiusButton: 12,
                          widthButton: double.maxFinite,
                          maginleft: 17.0,
                          titleButton:onCancelTitle?? "NO".tr(),
                          titlebuttonColor: Colorconstand.lightBlack,
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Button_Custom(
                          onPressed: onSubmit,
                          buttonColor: bgColorSubmitTitle ?? Colorconstand.mainColorSecondary,
                          hightButton: 45,
                          radiusButton: 12,
                          maginRight: 17.0,
                          titleButton:onSubmitTitle?? "LEAVE".tr(),
                          titlebuttonColor: Colorconstand.neutralWhite,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }


    Future childNoClass(context, String name, String profile) async {
    Future.delayed(Duration.zero, () {
      showDialog(
        barrierDismissible: true,
        barrierColor: Colors.black38,
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5,
            backgroundColor: Colorconstand.neutralWhite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Container(
              //width: MediaQuery.of(context).size.width / 1.5,
              height: 350,
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(22),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          child: CircleAvatar(
                        radius: 30, // Image radius
                        backgroundImage: NetworkImage(profile),
                      )),
                      const SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          name,
                          style: ThemsConstands.headline4_regular_18
                              .copyWith(color: Colorconstand.neutralDarkGrey),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Column(
                        children: [
                          Container(
                            child: Text(
                              "CHILD_NOT_CLASS".tr(),
                              style: ThemsConstands.headline_5_semibold_16.copyWith(
                                  color: Colorconstand.darkTextsPlaceholder),textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            child: Text(
                              "CHILD_NOT_CLASS_DES".tr(),
                              style: ThemsConstands.headline_5_semibold_16.copyWith(
                                  color: Colorconstand.darkTextsPlaceholder),textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: MaterialButton(
                          color: Colorconstand.primaryColor,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12))),
                          padding: const EdgeInsets.symmetric(
                              vertical: 6, horizontal: 28),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            "យល់ព្រម",
                            style: ThemsConstands.headline_6_semibold_14
                                .copyWith(color: Colorconstand.neutralWhite),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    });
  }


  void showMessage(
      {required BuildContext context,
      String? title,
      double? width,
      double? hight,
      TextStyle? textStyle,
      Color? color}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      backgroundColor: color ?? Colors.transparent,
      duration: const Duration(milliseconds: 1000),
      width: width,
      elevation: 2.0,
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      content: Wrap(
        children: [
          SizedBox(
            height: hight,
            child: Center(
              child: Text(
                title ?? "",
                style: textStyle ??
                    ThemsConstands.headline6_regular_14_24height
                        .copyWith(color: Colorconstand.neutralWhite),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
