import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/State/get_summary_att/bloc/summary_attendance_day_bloc.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../../widget/shimmer/shimmer_style.dart';
import '../../../models/profile_model/profile_model.dart';
import '../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../attendance_schedule_screen/state_management/state_attendance/bloc/get_date_bloc.dart';
import '../../attendance_schedule_screen/state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../../attendance_schedule_screen/widgets/date_picker_screen.dart';
import '../../check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../../check_attendance_screen/view/attendance_entry_screen.dart';
import '../../check_attendance_screen/view/attendance_summary_screen.dart';
import '../../home_screen/widget/card_schedule_subject_widget.dart';
import '../../primary_school_screen/widget/card_schedule_subject_primary_widget.dart';
import '../../schedule_expanded_screen/view/schedule_expanded_screen.dart';
import '../../tracking_location_teacher/tracking_location_teacher.dart';

class ScheduleScreen extends StatefulWidget {
  final bool? isPrimary;
  const ScheduleScreen({super.key,required this.isPrimary});

  @override
  State<ScheduleScreen> createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  bool isCheckClass = false;
  TabController? controller;
  String optionClassName = "SUBJECT".tr();
  String optionClassNameSelect = "MY_SCEDULE".tr();
  bool isInstructor = false;
  bool activeMySchedule = true;
  String classIdTeacher = "";
  bool showCheckSummaryAttendance = false;
  int inStuctorLenght = 0;
  int indexInStuctorLenght = 0;
  Data? dataTeacherprofile;
  bool checkDisplayCheckAttTeacher = false;

  //=============== End Varible main Tap =====================


  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================

  //============= Calculate Score ===========
    bool isLoadingCalculate = false;
    int calculateSuccess = 0;
    String statusCalculate  = "";
  //================================

  //============== Varible scedule Tap ==================
  int myIndex = 0;
  PageController? pagec;
  int? monthselected;
  int? monthlocalfirst;
  int? daylocalfirst;
  int? yearchecklocalfirst;
  int daySelectNoCurrentMonth = 1;
  double offset = 0.0;
  List lessList = [];
  List homeList = [];
  List researchList = [];
  List teacherAbsentList = [];
  bool showListDay = false;
  String? date;
  int activeIndexDay=0;
  bool firstLoadOnlyIndexDay = true;
  ScrollController? controllerSchedule;

 //============== End Varible scedule Tap ==================

   String present= "";
   String absent= "";
   String late= "";
   String permission= "";

    StreamSubscription? internetconnection;
    bool isoffline = true;

  @override
  void initState() {
    setState(() {
      TrackingLocation.shared.reUseContext(context);
      TrackingLocation.shared.requestLocationPermission();
    // =============== condition Primary school =================
      optionClassName = widget.isPrimary == true? "":"SUBJECT".tr();
      showCheckSummaryAttendance = widget.isPrimary == true? true:false;
    //=============Check internet====================

    
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

    //============= controller class change option ========
      _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      date = "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal";
    });
    monthlocalfirst = monthlocal;
    daylocalfirst = daylocal;
    yearchecklocalfirst = yearchecklocal;
    //============== Event call Data =================
    BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
    BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent(month: monthlocal.toString(),year:yearchecklocal.toString()));
    if(widget.isPrimary != true){
      BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
    }
    //============== End Event call Data =================
    super.initState();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
    //========= controllerSchedule offset set ========
    controllerSchedule = ScrollController(initialScrollOffset: offset);
    controllerSchedule!.addListener(() {
      setState(() {
        offset = controllerSchedule!.offset;
      });
    });
    //========= controllerSchedule offset set =========
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      bottomNavigationBar: isoffline == false ? Container(height: 0) : const BottomNavigateBar(isActive: 2),
      // bottomNavigationBar:Container(height: 0),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: Container(
                  margin:const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      //========== Button widget Change class==========
                      GestureDetector(
                        onTap: isInstructor == false ? null :() {
                          if(indexInStuctorLenght == inStuctorLenght){

                          // =============== condition Primary school =================
                            if(widget.isPrimary == true){
                              setState(() {
                                activeMySchedule = false;
                                optionClassName = "${"".tr()}${dataTeacherprofile!.instructorClass![0].className}";
                                classIdTeacher = dataTeacherprofile!.instructorClass![0].classId.toString();
                                BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: dataTeacherprofile!.instructorClass![0].classId.toString()));
                                indexInStuctorLenght = 0;
                              });
                            }
                            else{
                              setState(() {
                                optionClassName = "SUBJECT".tr();
                                activeMySchedule = true;
                                BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                indexInStuctorLenght = 0;
                              });
                            }
                          }
                          else{
                            setState(() {
                              activeMySchedule = false;
                              optionClassName = "${widget.isPrimary!?"":"".tr()}${dataTeacherprofile!.instructorClass![indexInStuctorLenght].className}";
                              classIdTeacher = dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString();
                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString()));
                              indexInStuctorLenght = indexInStuctorLenght+1;
                            });
                          }
                        },
                        child: Container(
                          margin: const EdgeInsets.only(left: 28),
                          child: Row(
                            children: [
                              Container(
                                height: 30,width: 30,
                                decoration: BoxDecoration(
                                  color: Colorconstand.primaryColor,
                                  borderRadius: BorderRadius.circular(8)
                                ),
                                child: SvgPicture.asset(ImageAssets.switch_icon),
                              ),
                              const SizedBox(width: 12,),
                              Expanded(
                                child: Row(
                                  children: [
                                    Text("${ widget.isPrimary==true?"SCHEDULE".tr(): "MY_SCEDULE".tr()}៖ ",style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.neutralWhite)),
                                    Expanded(child: Text(optionClassName,style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite))),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 15,),
                      Expanded(
                        child: Container(
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Colorconstand.neutralWhite),
                          child: Column(
                            children: [
                              Expanded(
                                child: BlocBuilder<GetDateBloc, GetDateState>(
                                  builder: (context, state) {
                                    if (state is GetDateListLoading) {
                                      return Container(
                                        decoration: const BoxDecoration(
                                          color: Color(0xFFF1F9FF),
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(12),
                                            topRight: Radius.circular(12),
                                          ),
                                        ),
                                        child: Column(
                                          children: [
                                            Container(
                                              color: const Color(0x622195F3),
                                              height: 165,
                                            ),
                                            Expanded(
                                              child: ListView.builder(
                                                padding: const EdgeInsets.all(0),
                                                shrinkWrap: true,
                                                itemBuilder: (BuildContext contex, index) {
                                                  return const ShimmerTimeTable();
                                                },
                                                itemCount: 5,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    } else if (state is GetDateListLoaded) {
                                      var selectDay;
                                      var datamonth = state.listDateModel!.data;
                                      var listMonthData = datamonth!.monthData;
                                      if(datamonth.month.toString() == monthlocalfirst.toString() && yearchecklocal==yearchecklocalfirst){
                                        daySelectNoCurrentMonth = 100;
                                      }
                                  //===================== loop list ======================
                                      var listDayWeek1 = listMonthData!.where((element) {
                                        return element.index! < 7;
                                      },).toList();
                                      var listDayWeek2 = listMonthData.where((element) {
                                        return element.index! >= 7 && element.index! <= 13;
                                      },).toList();
                                      var listDayWeek3 = listMonthData.where((element) {
                                        return element.index! >= 14 && element.index! <=20;
                                      },).toList();
                                      var listDayWeek4 = listMonthData.where((element) {
                                        return element.index! >= 21 && element.index! <=27;
                                      },).toList();
                                      var listDayWeek6 = listMonthData.where((element) {
                                        return element.index! > listMonthData.length-8;
                                      },).toList();
                                      var listDayWeek5 = listMonthData.length<=35?listDayWeek6:listMonthData.where((element) {
                                          return element.index! >= 28 && element.index! <= 34;
                                        },).toList();
                                  //===================== End loop list ======================
                                      return Container(
                                        child: Column(
                                          children: [
                                            Container(
                                              width: double.maxFinite,
                                              decoration: BoxDecoration(
                                                  borderRadius:BorderRadius.circular(8.0),
                                                  color: Colorconstand.neutralSecondBackground,
                                              ),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                              //===========  Button Show List Month ===========================
                                                  GestureDetector(
                                                    onTap:() {
                                                      setState(() {
                                                          if(showListDay==true){
                                                              showListDay =false;
                                                          }
                                                          else{
                                                            showListDay = true;
                                                          }
                                                      });
                                                    },
                                                    child:Container(
                                                      width: MediaQuery.of(context).size.width,
                                                      height: 45,
                                                        child: Row(
                                                          children: [
                                                            // showListDay==false ?Container():Container(
                                                            //   margin:const EdgeInsets.only(left: 8),
                                                            //   child: IconButton(
                                                            //     onPressed: (){
                                                            //       setState(() {
                                                            //         showCheckSummaryAttendance = false;
                                                            //         if(yearchecklocal == (yearchecklocalfirst! -1)){}
                                                            //         else{
                                                            //           setState(() {
                                                            //             daylocal = 1;
                                                            //             monthlocal = 1;
                                                            //             daySelectNoCurrentMonth = 1;
                                                            //             yearchecklocal = yearchecklocalfirst! - 1;
                                                            //             showListDay = false;
                                                            //             date = "1/01/$yearchecklocal";
                                                            //           });
                                                            //           BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocal.toString(), year: yearchecklocal.toString()));
                                                            //           if(activeMySchedule==true){
                                                            //             BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                            //           }
                                                            //           else{
                                                            //             BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: classIdTeacher));
                                                            //           }
                                                            //         }
                                                            //       });
                                                            //     }, 
                                                            //     icon: Icon(Icons.arrow_back_ios_new_rounded,size: 18,color: yearchecklocal == yearchecklocalfirst! - 1?Colors.transparent:Colorconstand.primaryColor,))
                                                            // ),
                                                            Expanded(
                                                              child: Row(
                                                              mainAxisAlignment:MainAxisAlignment.center,
                                                              children: [
                                                                offset > 130
                                                                    ? Container(
                                                                        height: 25,width: 25,
                                                                        margin: const EdgeInsets.only(right: 5),
                                                                        padding: const EdgeInsets.all(3),
                                                                        decoration: BoxDecoration(color: Colorconstand.primaryColor, borderRadius: BorderRadius.circular(6)),
                                                                        child: Center(
                                                                          child: Text(
                                                                            daylocal.toString(),
                                                                            style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralWhite),
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : Container(),
                                                                Text(
                                                                  checkMonth(int.parse(datamonth.month!)),
                                                                  style:
                                                                      ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),
                                                                ),
                                                                const SizedBox(width: 5,),
                                                                Text(datamonth.year.toString(),
                                                                    style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor)),
                                                                const SizedBox(
                                                                    width: 8),
                                                              showListDay==true?Container(): Icon(
                                                                    showListDay ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                    size: 24,
                                                                    color: Colorconstand.primaryColor),
                                                                                                                                      ],
                                                                                                                                    ),
                                                            ),
                                                            // showListDay==false?Container():Container(
                                                            //   margin:const EdgeInsets.only(right: 8),
                                                            //   child: IconButton(
                                                            //     onPressed: (){
                                                            //       setState(() {
                                                            //         showCheckSummaryAttendance = false;
                                                            //         if(yearchecklocal == yearchecklocalfirst!){}
                                                            //         else{
                                                            //           yearchecklocal = yearchecklocalfirst;
                                                            //           daySelectNoCurrentMonth = 100;
                                                            //           daylocal = daylocalfirst;
                                                            //           monthlocal = monthlocalfirst;
                                                            //           showListDay = false;
                                                            //         BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                            //         if(activeMySchedule==true){
                                                            //           BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                            //         }
                                                            //         else{
                                                            //           BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: classIdTeacher));
                                                            //         }
                                                            //         }
                                                            //       });
                                                            //     }, 
                                                            //     icon:Icon(Icons.arrow_forward_ios_rounded,size: 18,color:yearchecklocal==yearchecklocalfirst?Colors.transparent: Colorconstand.primaryColor,))
                                                            // ),
                                                          ],
                                                        ),
                                                        ),
                                                  ),
                                              //=========== End Button Show List Month ===========================
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Stack(
                                                children: [
                                                  Column(
                                                    children: [
                                                      Container(
                                                        height: 0,
                                                        child: Stack(
                                                          children:List.generate(listMonthData.length,(subindex) {
                                                            if(firstLoadOnlyIndexDay==true){
                                                              if(listMonthData[subindex].isActive==true){
                                                                activeIndexDay = listMonthData[subindex].index!;
                                                                firstLoadOnlyIndexDay = false;
                                                              }
                                                            }
                                                              return Container();
                                                            }
                                                          )
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: SingleChildScrollView(
                                                          controller: controllerSchedule,
                                                          child: BlocConsumer<GetScheduleBloc, GetScheduleState>(
                                                            listener: (context, state) {
                                                              if(state is GetClassScheduleLoaded){
                                                                setState(() {
                                                                  if(state.classScheduleModel!.expire==true){
                                                                    BlocProvider.of<SummaryAttendanceDayBloc>(context).add( GetSummaryStudentAttendanceEvent(classIdTeacher.toString(),"${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",));
                                                                  }
                                                                  else if(widget.isPrimary == true&& daylocal ==daylocalfirst && monthlocal == monthlocalfirst && yearchecklocal == yearchecklocalfirst){
                                                                    BlocProvider.of<SummaryAttendanceDayBloc>(context).add( GetSummaryStudentAttendanceEvent(classIdTeacher.toString(),"${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",));
                                                                  }
                                                                });
                                                              }
                                                            },
                                                            builder: (context, state) {
                                                              if(state is GetScheduleLoading){
                                                                return Container(
                                                                  decoration: const BoxDecoration(
                                                                    color: Color(0xFFF1F9FF),
                                                                    borderRadius: BorderRadius.only(
                                                                      topLeft: Radius.circular(12),
                                                                      topRight: Radius.circular(12),
                                                                    ),
                                                                  ),
                                                                  child: Column(
                                                                    children: [
                                                                      ListView.builder(
                                                                        padding: const EdgeInsets.all(0),
                                                                        shrinkWrap: true,
                                                                        itemBuilder: (BuildContext contex, index) {
                                                                        return const ShimmerTimeTable();
                                                                        },
                                                                        itemCount: 5,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                );
                                                              }
                                                      //===========  My Schedule =========================
                                                              else if(state is GetMyScheduleLoaded){
                                                                var dataMySchedule = state.myScheduleModel!.data;
                                                                String dataMyScheduleMessage = state.myScheduleModel!.message.toString();
                                                                return dataMySchedule!.isEmpty? 
                                                                Container( margin: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                  child: ScheduleEmptyWidget(
                                                                    title: dataMyScheduleMessage.tr(),
                                                                    subTitle: dataMyScheduleMessage=="HOLIDAY"?"HOLIDAY_DES".tr():dataMyScheduleMessage=="DATE_NOT_VALID_ACADEMIC"?"DATE_NOT_VALID_ACADEMIC_DES".tr():"SCHEDULE_NOT_SET_DES".tr(),
                                                                      )
                                                                ):Container(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                  alignment: Alignment.center,
                                                                  child: ListView.builder(
                                                                    shrinkWrap: true,
                                                                    physics: const ScrollPhysics(),
                                                                    itemCount:dataMySchedule.length,
                                                                    padding: const EdgeInsets.only(top: 10),
                                                                    itemBuilder: (context, indexmyschedule) {
                                                                      if(dataMySchedule.isNotEmpty){
                                                                        homeList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                          return element.activityType==2;
                                                                        }).toList();
                                                                        teacherAbsentList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                                return element.activityType==4;
                                                                        }).toList();
                                                                        researchList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                          return element.activityType==5;
                                                                        }).toList();
                                                                        lessList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                          return element.activityType==1;
                                                                        }).toList();
                                                                      }
                                                                      return CardScheduleSubjectWidget(
                                                                            teacherAbsentList: teacherAbsentList,
                                                                            className: dataMySchedule[indexmyschedule].className.toString(),
                                                                            titleSubject:translate=="km"?dataMySchedule[indexmyschedule].subjectName.toString():dataMySchedule[indexmyschedule].subjectNameEn.toString(),
                                                                            isActive: dataMySchedule[indexmyschedule].isCurrent,
                                                                            subjectColor:dataMySchedule[indexmyschedule].isExam==false? teacherAbsentList.isNotEmpty?"0xff1b1c1e":"0xFF${dataMySchedule[indexmyschedule].subjectColor}":"0xFFEE964B",
                                                                            scheduleType: dataMySchedule[indexmyschedule].isExam==true?1:2,
                                                                            absent: dataMySchedule[indexmyschedule].attendance!.absent==0 || dataMySchedule[indexmyschedule].attendance!.isCheckAttendance==0?"-":dataMySchedule[indexmyschedule].attendance!.absent.toString(),
                                                                            present: dataMySchedule[indexmyschedule].attendance!.isCheckAttendance==0?"-":dataMySchedule[indexmyschedule].attendance!.present.toString(),
                                                                            examDate:dataMySchedule[indexmyschedule].isExam==true?dataMySchedule[indexmyschedule].examItem!.examDate.toString():"",
                                                                            homeworkExDate:homeList.isEmpty?"":homeList[0].expiredAt.toString(),
                                                                            lessonAbout: lessList.isEmpty?"":lessList[0].title.toString(),
                                                                            homeList: homeList,
                                                                            lessList: lessList,
                                                                            researchAbout: researchList.isEmpty?"":researchList[0].title.toString(),
                                                                            researchList: researchList,
                                                                            isApprove: dataMySchedule[indexmyschedule].isExam==true?dataMySchedule[indexmyschedule].examItem!.isApprove! :0,
                                                                            isCheckScoreEnter: dataMySchedule[indexmyschedule].isCheckScoreEnter!,
                                                                            durationSubject: dataMySchedule[indexmyschedule].duration.toString(),
                                                                            timeSubject:"${dataMySchedule[indexmyschedule].startTime} - ${dataMySchedule[indexmyschedule].endTime}",
                                                                            onPressed: (){
                                                                            setState(() {
                                                                              TrackingLocation.shared.checkStaffStatus(status:dataMySchedule[indexmyschedule].staffPresentStatus!);
                                                                              // TrackingLocation.shared.saveStaffPresentLocation(subjectID:dataMySchedule[indexmyschedule].subjectId.toString(), classID: dataMySchedule[indexmyschedule].classId.toString(), timeTSettingID: dataMySchedule[indexmyschedule].timeTableSettingId.toString()
                                                                              // );
                                                                            });
                                                                          //================ Check condition display check attendance teacher ================
                                                                            if(yearchecklocal! == yearchecklocalfirst!){
                                                                              if(monthlocal == monthlocalfirst){
                                                                                if(daylocal! <= daylocalfirst!){
                                                                                  setState(() {
                                                                                    checkDisplayCheckAttTeacher = true;
                                                                                  });
                                                                                }
                                                                                else{
                                                                                  setState(() {
                                                                                    checkDisplayCheckAttTeacher = false;
                                                                                  });
                                                                                }
                                                                              }
                                                                              else if(monthlocal! < monthlocalfirst!){
                                                                                setState(() {
                                                                                  checkDisplayCheckAttTeacher = true;
                                                                                });
                                                                              }
                                                                              else{
                                                                                setState(() {
                                                                                  checkDisplayCheckAttTeacher = false;
                                                                                });
                                                                              }
                                                                            }
                                                                            else if(yearchecklocal! < yearchecklocalfirst!){
                                                                              setState(() {
                                                                                checkDisplayCheckAttTeacher = true;
                                                                              });
                                                                            }
                                                                            else{
                                                                              setState(() {
                                                                                checkDisplayCheckAttTeacher = false;
                                                                              });
                                                                            }
                                                                            //================ End Check condition display check attendance teacher ================
                                                                            Navigator.push(context,
                                                                                PageTransition(type:PageTransitionType.bottomToTop,child: ScheduleExpandedScreen(isPrimary: widget.isPrimary!,routFromScreen: 2,data: dataMySchedule[indexmyschedule],activeMySchedule: true, month:monthlocal.toString(), type: '1',displayCheckAttendance: checkDisplayCheckAttTeacher,),
                                                                                ),
                                                                              );
                                                                            },
                                                                      );
                                                                    },
                                                                  ),
                                                                );
                                                              }
                                                        //===========  End My Schedule =========================
                        
                                                        //===========  Class Schedule =========================
                                                              else if( state is GetClassScheduleLoaded){
                                                                var dataClassSchedule = state.classScheduleModel!.data;
                                                                String dataMyScheduleMessage = state.classScheduleModel!.message.toString();
                                                                return dataClassSchedule!.isEmpty? 
                                                                Container( margin: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                  child: ScheduleEmptyWidget(
                                                                    title: dataMyScheduleMessage.tr(),
                                                                    subTitle: dataMyScheduleMessage=="HOLIDAY"?"HOLIDAY_DES".tr():dataMyScheduleMessage=="DATE_NOT_VALID_IN_ACADEMIC"?"DATE_NOT_VALID_ACADEMIC_DES".tr():"SCHEDULE_NOT_SET_DES".tr(),
                                                                      )
                                                                ): BlocListener<SummaryAttendanceDayBloc, SummaryAttendanceDayState>(
                                                                  listener: (context, state) {
                                                                    if(state is GetSummaryStudentAttendanceListLoaded){
                                                                      var data = state.summaryAttendanceList!.data;
                                                                      setState(() {
                                                                        present = data!.attendance!.present.toString();
                                                                        absent = data.attendance!.absent.toString();
                                                                        late = data.attendance!.late.toString();
                                                                        permission = data.attendance!.permission.toString();
                                                                        showCheckSummaryAttendance = true;
                                                                      });
                                                                    }
                                                                  },
                                                                  child: Container(
                                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                    alignment: Alignment.center,
                                                                    child: Column(
                                                                      children: [
                                                                        widget.isPrimary == true?AnimatedContainer(
                                                                          duration:const Duration(milliseconds: 200),
                                                                          height: showCheckSummaryAttendance == true? 60:0,
                                                                          decoration: BoxDecoration(
                                                                            borderRadius: BorderRadius.circular(12),
                                                                            color: Colorconstand.primaryColor.withOpacity(dataClassSchedule[0].attendance!.isCheckAttendance==0?1.0:0.08)
                                                                          ),
                                                                          margin:showCheckSummaryAttendance == true?const EdgeInsets.all(12):const EdgeInsets.all(0),
                                                                          padding:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                                                                          child: GestureDetector(
                                                                            onTap: (){
                                                                              BlocProvider.of<CheckAttendanceBloc>(context).add(GetCheckStudentAttendanceEvent(classIdTeacher.toString(), dataClassSchedule[0].scheduleId.toString(),dataClassSchedule[0].date.toString()));
                                                                              Navigator.push(context, MaterialPageRoute( builder: (context) =>  const AttendanceEntryScreen( routFromScreen:2,activeMySchedule: false,isPrimary: true,)));
                                                                            },
                                                                            child:Row(
                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                              children: [
                                                                                SvgPicture.asset(ImageAssets.people_icon,width: 25,color:dataClassSchedule[0].attendance!.isCheckAttendance==0?Colorconstand.neutralWhite:Colorconstand.primaryColor),
                                                                                const SizedBox(width: 8,),
                                                                                Expanded(child: Text("ATTENDANCE_DAY".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: dataClassSchedule[0].attendance!.isCheckAttendance==0?Colorconstand.neutralWhite:Colorconstand.neutralDarkGrey),)),
                                                                                dataClassSchedule[0].attendance!.isCheckAttendance==0?Container(
                                                                                  child: Row(
                                                                                    children: [
                                                                                      Text("PRESENCE".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralWhite),),
                                                                                      const Icon(Icons.arrow_forward,size: 20,color: Colorconstand.neutralWhite,)
                                                                                    ],
                                                                                  ),
                                                                                ):Row(
                                                                                  children: [
                                                                                    customBoxWidget(
                                                                                      background: Colorconstand.alertsPositive,
                                                                                      title: present,
                                                                                    ),
                                                                                    const SizedBox(
                                                                                      width: 8,
                                                                                    ),
                                                                                    customBoxWidget(
                                                                                      background: Colorconstand.alertsDecline,
                                                                                      title: absent,
                                                                                    ),
                                                                                    const SizedBox(
                                                                                      width: 8,
                                                                                    ),
                                                                                    customBoxWidget(
                                                                                      background: Colorconstand.alertsAwaitingText,
                                                                                      title: late,
                                                                                    ),
                                                                                    const SizedBox(
                                                                                      width: 8,
                                                                                    ),
                                                                                    customBoxWidget(
                                                                                      background: Colorconstand.mainColorForecolor,
                                                                                      title: permission,
                                                                                    ),
                                                                                    const SizedBox(width: 8,),
                                                                                    showCheckSummaryAttendance == true? const Icon(Icons.arrow_forward_ios_rounded,size: 18,color: Colorconstand.primaryColor,):const SizedBox(),
                                                                                  ],
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ):Container(height: 0,),
                                                                        ListView.builder(
                                                                          shrinkWrap: true,
                                                                          physics: const ScrollPhysics(),
                                                                          itemCount:dataClassSchedule.length,
                                                                          padding: const EdgeInsets.only(top: 10),
                                                                          itemBuilder: (context, indexclassschedule) {
                                                                            if(dataClassSchedule.isNotEmpty){
                                                                              homeList = dataClassSchedule[indexclassschedule].activities!.where((element) {
                                                                                return element.activityType==2;
                                                                              }).toList();
                                                                              teacherAbsentList = dataClassSchedule[indexclassschedule].activities!.where((element) {
                                                                                return element.activityType==4;
                                                                              }).toList();
                                                                              lessList = dataClassSchedule[indexclassschedule].activities!.where((element) {
                                                                                return element.activityType==1;
                                                                              }).toList();
                                                                              researchList = dataClassSchedule[indexclassschedule].activities!.where((element) {
                                                                                return element.activityType==5;
                                                                              }).toList();
                                                                            }
                                                                            return
                                                                             widget.isPrimary == true?
                                                                             CardScheduleSubjectPrimaryWidget(
                                                                                onPressed: (){
                                                                                  setState(() {
                                                                                    TrackingLocation.shared.checkStaffStatus(status:dataClassSchedule[indexclassschedule].staffPresentStatus!);
                                                                                  });
                                                                                  Navigator.push(context,
                                                                                          PageTransition(type:PageTransitionType.bottomToTop,child: ScheduleExpandedScreen(isPrimary: widget.isPrimary!, routFromScreen: 2,data: dataClassSchedule[indexclassschedule],activeMySchedule: false, month:monthlocal.toString(), type: '1',displayCheckAttendance: showCheckSummaryAttendance,),
                                                                                          ),
                                                                                        );
                                                                                },
                                                                                researchAbout: researchList.isEmpty?"":researchList[0].title.toString(),
                                                                                researchList: researchList,
                                                                                subjectColor:dataClassSchedule[indexclassschedule].isExam==false?  teacherAbsentList.isNotEmpty?"0xff1b1c1e":"0xff1855ab":"0xFFEE964B",
                                                                                isSubSubject: dataClassSchedule[indexclassschedule].isSubSubject!,
                                                                                tagSubject: translate=="km"?dataClassSchedule[indexclassschedule].subjectGroupTagName.toString():dataClassSchedule[indexclassschedule].subjectGroupTagNameEn.toString(),
                                                                                teacherAbsentList: teacherAbsentList,
                                                                                className: dataClassSchedule[indexclassschedule].className.toString(),
                                                                                titleSubject: translate=="km"?dataClassSchedule[indexclassschedule].subjectName.toString():dataClassSchedule[indexclassschedule].subjectNameEn.toString(),
                                                                                isActive: dataClassSchedule[indexclassschedule].isCurrent,
                                                                                scheduleType: dataClassSchedule[indexclassschedule].isExam==true?1:2,
                                                                                absent: dataClassSchedule[indexclassschedule].attendance!.absent==0 || dataClassSchedule[indexclassschedule].attendance!.isCheckAttendance==0?"-":dataClassSchedule[indexclassschedule].attendance!.absent.toString(),
                                                                                present: dataClassSchedule[indexclassschedule].attendance!.isCheckAttendance==0?"-":dataClassSchedule[indexclassschedule].attendance!.present.toString(),
                                                                                examDate:dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.examDate.toString():"",
                                                                                homeworkExDate:homeList.isEmpty?"":homeList[0].expiredAt.toString(),
                                                                                lessonAbout: lessList.isEmpty?"":lessList[0].title.toString(),
                                                                                homeList: homeList,
                                                                                lessList: lessList,
                                                                                isApprove: dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.isApprove! :0,
                                                                                isCheckScoreEnter: dataClassSchedule[indexclassschedule].isCheckScoreEnter!,
                                                                                durationSubject: dataClassSchedule[indexclassschedule].duration.toString(),
                                                                                timeSubject:"${dataClassSchedule[indexclassschedule].startTime} - ${dataClassSchedule[indexclassschedule].endTime}",
                                                                             )
                                                                             :CardScheduleSubjectWidget(
                                                                                onPressed: (){
                                                                                  setState(() {
                                                                                    TrackingLocation.shared.checkStaffStatus(status:dataClassSchedule[indexclassschedule].staffPresentStatus!);
                                                                                    // TrackingLocation.shared.saveStaffPresentLocation(subjectID:dataClassSchedule[indexclassschedule].subjectId.toString(), classID: dataClassSchedule[indexclassschedule].classId.toString(), timeTSettingID: dataClassSchedule[indexclassschedule].timeTableSettingId.toString()
                                                                                    // );
                                                                                  });
                                                                                  Navigator.push(context,
                                                                                          PageTransition(type:PageTransitionType.bottomToTop,child: ScheduleExpandedScreen(isPrimary: widget.isPrimary!, routFromScreen: 2,data: dataClassSchedule[indexclassschedule],activeMySchedule: false, month:monthlocal.toString(), type: '1',displayCheckAttendance: showCheckSummaryAttendance,),
                                                                                          ),
                                                                                        );
                                                                                },
                                                                                teacherAbsentList: teacherAbsentList,
                                                                                className: dataClassSchedule[indexclassschedule].className.toString(),
                                                                                titleSubject: translate=="km"?dataClassSchedule[indexclassschedule].subjectName.toString():dataClassSchedule[indexclassschedule].subjectNameEn.toString(),
                                                                                isActive: dataClassSchedule[indexclassschedule].isCurrent,
                                                                                subjectColor:dataClassSchedule[indexclassschedule].isExam==false?  teacherAbsentList.isNotEmpty?"0xff1b1c1e":"0xFF${dataClassSchedule[indexclassschedule].subjectColor}":"0xFFEE964B",
                                                                                scheduleType: dataClassSchedule[indexclassschedule].isExam==true?1:2,
                                                                                absent: dataClassSchedule[indexclassschedule].attendance!.absent==0 || dataClassSchedule[indexclassschedule].attendance!.isCheckAttendance==0?"-":dataClassSchedule[indexclassschedule].attendance!.absent.toString(),
                                                                                present: dataClassSchedule[indexclassschedule].attendance!.isCheckAttendance==0?"-":dataClassSchedule[indexclassschedule].attendance!.present.toString(),
                                                                                examDate:dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.examDate.toString():"",
                                                                                homeworkExDate:homeList.isEmpty?"":homeList[0].expiredAt.toString(),
                                                                                lessonAbout: lessList.isEmpty?"":lessList[0].title.toString(),
                                                                                homeList: homeList,
                                                                                lessList: lessList,
                                                                                researchAbout: researchList.isEmpty?"":researchList[0].title.toString(),
                                                                                researchList: researchList,
                                                                                isApprove: dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.isApprove! :0,
                                                                                isCheckScoreEnter: dataClassSchedule[indexclassschedule].isCheckScoreEnter!,
                                                                                durationSubject: dataClassSchedule[indexclassschedule].duration.toString(),
                                                                                timeSubject:"${dataClassSchedule[indexclassschedule].startTime} - ${dataClassSchedule[indexclassschedule].endTime}",
                                                                            );
                                                                          },
                                                                        ),
                                                                        Container(
                                                                          height:showCheckSummaryAttendance == false || widget.isPrimary ==true? 10:120,
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                );
                                                              }
                        
                                                        //=========== End Class Schedule =========================
                                                              else {
                                                                return Center(
                                                                  child: EmptyWidget(
                                                                    title: "WE_DETECT".tr(),
                                                                    subtitle: "WE_DETECT_DES".tr(),
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                        
                                                  //======================= widget Date List =======================================
                                                  AnimatedPositioned(
                                                    top: offset>130?-110:0,
                                                        left: 0,
                                                        right: 0,
                                                        duration: const Duration(milliseconds:300),
                                                    child: Container(
                                                        decoration:
                                                            const BoxDecoration(boxShadow: <BoxShadow>[
                                                          BoxShadow(
                                                              color: Colorconstand.neutralGrey,
                                                              blurRadius: 10.0,
                                                              offset: Offset(5.0, 5.7))
                                                        ], color: Colorconstand.neutralSecondBackground),
                                                        width: MediaQuery.of(context).size.width,
                                                        height:MediaQuery.of(context).size.width>800? 205:97,
                                                        padding: const EdgeInsets.symmetric(vertical:10.0,horizontal:0.0),
                                                        child: PageView.builder(
                                                            controller: PageController(initialPage:monthlocalfirst != monthlocal || yearchecklocal != yearchecklocalfirst ?0:activeIndexDay <=6? 0:activeIndexDay >=7&&activeIndexDay <=13?1:activeIndexDay >=14&&activeIndexDay <=20?2:activeIndexDay >=21&&activeIndexDay <=27?3:activeIndexDay >=28&&activeIndexDay <=34?4:5),
                                                            scrollDirection: Axis.horizontal,
                                                            itemCount:listMonthData.length<=35?5:6,
                                                            itemBuilder: (context, indexDate) {
                                                              return GridView.builder(
                                                                physics:const NeverScrollableScrollPhysics(),
                                                                padding:const EdgeInsets.all(0),
                                                                itemCount: indexDate==0?listDayWeek1.length:indexDate==1?listDayWeek2.length:indexDate==2?listDayWeek3.length:indexDate==3?listDayWeek4.length:indexDate==4?listDayWeek5.length:listDayWeek6.length,
                                                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 7,childAspectRatio: 0.8),
                                                                itemBuilder: (context, index) {
                                                                  if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){
                                                                    selectDay = true;
                                                                  }
                                                                  else{
                                                                    selectDay = false;
                                                                  }
                                                                  return indexDate==0? DatePickerWidget(
                                                                    disable: listDayWeek1[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday:listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth?selectDay:listDayWeek1[index].isActive!,
                                                                    day:listDayWeek1[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek1[index].day.toString().tr(),
                                                                    onPressed: () {
                                                                      if(listDayWeek1[index].disable==true){}
                                                                      else{
                                                                        setState(() {
                                                                          showCheckSummaryAttendance = false;
                                                                          if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){}
                                                                          else{
                                                                            daySelectNoCurrentMonth = 100;
                                                                          }
                                                                          daylocal = listDayWeek1[index].day;
                                                                          for (var element in listMonthData) {
                                                                            element.isActive = false;
                                                                          }
                                                                          date = listDayWeek1[index].date;
                                                                          listMonthData[listDayWeek1[index].index!.toInt()].isActive = true;                                                                                               
                                                                          if(activeMySchedule==true){                                                                                                
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek1[index].date));
                                                                          }
                                                                          else{
                                                                            BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek1[index].date,classid: classIdTeacher));
                                                                          }
                                                                        });
                                                                      }
                                                                    },
                                                                  ):indexDate==1? DatePickerWidget(
                                                                    disable: listDayWeek2[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek2[index].isActive!,
                                                                    day:listDayWeek2[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek2[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        showCheckSummaryAttendance = false;
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek2[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek2[index].date;
                                                                        listMonthData[listDayWeek2[index].index!.toInt()].isActive = true;                                                                                                
                                                                        if(activeMySchedule==true){                                                                                                
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek2[index].date));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek2[index].date,classid: classIdTeacher));
                                                                        }
                                                                  
                                                                      });
                                                                    },
                                                                  ):indexDate==2? DatePickerWidget(
                                                                    disable: listDayWeek3[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek3[index].isActive!,
                                                                    day:listDayWeek3[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek3[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        showCheckSummaryAttendance = false;
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek3[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek3[index].date;
                                                                        listMonthData[listDayWeek3[index].index!.toInt()].isActive = true;                                                                                                
                                                                        if(activeMySchedule==true){                                                                                                
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek3[index].date));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek3[index].date,classid: classIdTeacher));
                                                                        }
                                                                  
                                                                      });
                                                                    },
                                                                  ):indexDate==3? DatePickerWidget(
                                                                    disable: listDayWeek4[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek4[index].isActive!,
                                                                    day:listDayWeek4[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek4[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        showCheckSummaryAttendance = false;
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek4[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek4[index].date;
                                                                        listMonthData[listDayWeek4[index].index!.toInt()].isActive = true;                                                                                                
                                                                        if(activeMySchedule==true){                                                                                                
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek4[index].date));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek4[index].date,classid: classIdTeacher));
                                                                        }
                                                                  
                                                                      });
                                                                    },
                                                                  ):indexDate==4? DatePickerWidget(
                                                                    disable: listDayWeek5[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek5[index].isActive!,
                                                                    day:listDayWeek5[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek5[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        showCheckSummaryAttendance = false;
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek5[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek5[index].date;
                                                                        listMonthData[listDayWeek5[index].index!.toInt()].isActive = true;                                                                                                
                                                                        if(activeMySchedule==true){                                                                                                
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek5[index].date));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek5[index].date,classid: classIdTeacher));
                                                                        }
                                                                      });
                                                                    },
                                                                  ): DatePickerWidget(
                                                                    disable: listDayWeek6[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek6[index].isActive!,
                                                                    day:listDayWeek6[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek6[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        showCheckSummaryAttendance = false;
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek6[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek6[index].date;
                                                                        listMonthData[listDayWeek6[index].index!.toInt()].isActive = true;
                                                                        if(activeMySchedule==true){                                                                                                
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek6[index].date));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek6[index].date,classid: classIdTeacher));
                                                                        }
                                                                      });
                                                                    },
                                                                  );
                                                                },);
                                                            }),
                                                      ),
                                                  ),
                                                  /// ===================End widget List Day================================
                                                  /// 
                                                  /// =============== Baground over when drop month =============
                                                  showListDay == false
                                                  ? Container()
                                                  : Positioned(
                                                      bottom: 0,
                                                      left: 0,
                                                      right: 0,
                                                      top: 0,
                                                      child:
                                                          GestureDetector(
                                                        onTap: (() {
                                                          setState(
                                                              () {
                                                            showListDay =
                                                                false;
                                                          });
                                                        }),
                                                        child:
                                                            Container(
                                                          color: const Color(
                                                              0x7B9C9595),
                                                        ),
                                                      ),
                                                    ),
                                            /// =============== Baground over when drop month =============
                                            /// 
                                            /// =============== Widget List Change Month =============
                                                  AnimatedPositioned(
                                                      top: showListDay ==
                                                              false
                                                          ? wieght > 800? -500: -270
                                                          : 0,
                                                      left: 0,
                                                      right: 0,
                                                      duration: const Duration(milliseconds:300),
                                                      child:Container(
                                                        color: Colorconstand.neutralWhite,
                                                        child:
                                                            GestureDetector(
                                                          onTap:() {
                                                            setState(() {
                                                              showListDay = false;
                                                            });
                                                          },
                                                          child:Container(
                                                            margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                            child: GridView.builder(
                                                              shrinkWrap: true,
                                                              physics:const ScrollPhysics(),
                                                              padding:const EdgeInsets.all(.0),
                                                                itemCount: 12,
                                                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5),
                                                                itemBuilder: (BuildContext context, int index) {
                                                                  return GestureDetector(
                                                                    onTap: (() {
                                                                      setState(() {
                                                                        showCheckSummaryAttendance = false;
                                                                        if(monthlocal == index+1){}
                                                                        else{
                                                                          if(index+1==monthlocalfirst && yearchecklocal == yearchecklocalfirst){
                                                                            setState(() {
                                                                              daylocal = daylocalfirst;
                                                                            });
                                                                          }
                                                                          else{
                                                                            setState(() {
                                                                              daylocal = 1;
                                                                            });
                                                                          }
                                                                          daySelectNoCurrentMonth = 1;
                                                                          showListDay = false;
                                                                          monthlocal = (index+1);
                                                                          date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                          BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: (index+1).toString(), year: yearchecklocal.toString()));
                                                                          if(activeMySchedule==true){
                                                                            BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                                          }
                                                                          else{
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: classIdTeacher));
                                                                          }
                                                                        }
                                                                      });
                                                                    }),
                                                                    child: Container(
                                                                      margin:const EdgeInsets.all(6),
                                                                      decoration:BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(12),
                                                                        color: monthlocal == (index+1)? Colorconstand.primaryColor:Colors.transparent,
                                                                      ),
                                                                      height: 20,width: 20,
                                                                      child:  Center(child: Text(((index+1) == 1 ? "JANUARY" : (index+1) == 2 ? "FEBRUARY" : (index+1) == 3 ? "MARCH" : (index+1) == 4 ? "APRIL" : (index+1) == 5 ? "MAY" : (index+1) == 6 ? "JUNE" : (index+1) == 7 ? "JULY" : (index+1) == 8 ? "AUGUST" : (index+1) == 9 ? "SEPTEMBER" : (index+1) == 10 ? "OCTOBER" : (index+1) == 11 ? "NOVEMBER" : "DECEMBER").tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthlocal==(index+1)?Colorconstand.neutralWhite:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                                    ),
                                                                  );
                                                                },
                                                              ),
                                                          )
                                                        ),
                                                      ),
                                                    ),
                                                /// =============== End Widget List Change Month =============
                        
                                              //======================= Icon Widget redirect to active date =======================
                                                AnimatedPositioned(
                                                  bottom:monthlocal == monthlocalfirst && yearchecklocal == yearchecklocalfirst && daylocal == daylocalfirst || showListDay==true ? -60:showCheckSummaryAttendance==false || widget.isPrimary ==true? 40:130,right: 30,
                                                  duration: const Duration(milliseconds:300),
                                                  child: Container(
                                                    height: 55,width: 55,
                                                    decoration: BoxDecoration(
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colorconstand.neutralGrey.withOpacity(0.7),
                                                          spreadRadius: 4,
                                                          blurRadius: 4,
                                                          offset:const Offset(0, 3), // changes position of shadow
                                                        ),
                                                      ],
                                                      color: Colorconstand.neutralWhite,
                                                      shape: BoxShape.circle
                                                    ),
                                                    child: MaterialButton(
                                                      elevation: 8,
                                                      onPressed: (){
                                                        setState(() {
                                                          showCheckSummaryAttendance = false;
                                                            yearchecklocal = yearchecklocalfirst;
                                                            daySelectNoCurrentMonth = 100;
                                                            daylocal = daylocalfirst;
                                                            monthlocal = monthlocalfirst;
                                                            showListDay = false;
                                                        });
                                                        BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                        if(activeMySchedule==true){
                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocalfirst/${monthlocalfirst! <=9?"0$monthlocalfirst":monthlocalfirst}/$yearchecklocalfirst"));
                                                        }
                                                        else{
                                                        BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocalfirst/${monthlocalfirst! <=9?"0$monthlocalfirst":monthlocalfirst}/$yearchecklocalfirst",classid: classIdTeacher));
                                                        }
                                                      },
                                                      shape:const CircleBorder(),
                                                      child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                                    ),
                                                  ),
                                                ),
                        
                                          //======================= End Icon Widget redirect to active date =======================
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    } else {
                                      return Center(
                                        child: EmptyWidget(
                                          title: "WE_DETECT".tr(),
                                          subtitle: "WE_DETECT_DES".tr(),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
        //================= background when pop change class ============
        isCheckClass==true ? AnimatedPositioned(
          duration:const Duration(milliseconds: 4000),
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        child: GestureDetector(
          onTap: () {
          setState(() {
              isCheckClass = false;
              _arrowAnimationController!.isCompleted
            ? _arrowAnimationController!.reverse()
            : _arrowAnimationController!.forward();
          });
        },child: Container(color: Colors.transparent,width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,),
        )):Positioned(top: 0,child: Container(height: 0,)),
        //================= baground when pop change class ============

        //================ Popup Change Class ========================
            Positioned(
              top: 100,left: 12,
              child: BlocConsumer<ProfileUserBloc, ProfileUserState>(
                    listener: (context, state) {
                      if(state is GetUserProfileLoaded){
                        dataTeacherprofile = state.profileModel!.data;
                        inStuctorLenght = dataTeacherprofile!.instructorClass!.length;
                        if(dataTeacherprofile!.instructorClass!.isNotEmpty){
                          setState(() {
                            isInstructor = true;
                    // =============== condition Primary school =================
                            if(widget.isPrimary == true){
                              activeMySchedule = false;
                              classIdTeacher = dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString();
                              optionClassName = dataTeacherprofile!.instructorClass![indexInStuctorLenght].className.toString();
                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString()));
                              indexInStuctorLenght = indexInStuctorLenght+1;
                            }
                          });
                        }
                      }
                    },
                    builder: (context, state) {
                      return Container(height: 0,);
                    },
                  ),
              
            ),
        //================ End Popup Change Class ========================

      //=========== summary attendance from Day ======================

      activeMySchedule == true || widget.isPrimary == true ?Container(height: 0,)
      : AnimatedPositioned(
              bottom:showCheckSummaryAttendance==true? -5:-150,left: 0,right: 0,
               duration:const Duration(milliseconds: 500),
               child: Container(
                padding:const EdgeInsets.symmetric(vertical: 18),
                decoration: BoxDecoration(
                  color: Colorconstand.neutralWhite,
                  borderRadius:const BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 5.5,
                      blurRadius: 9,
                      offset: const Offset(2, 2),
                    )
                  ],
                ),
                
                child: Column(
                  children: [
                    Container(
                      margin:const EdgeInsets.only(left: 35,right: 35,top: 0,bottom: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("ATTENDANCE_DAY".tr()),
                          Row(
                            children: [
                              customBoxWidget(
                                background: Colorconstand.alertsPositive,
                                title: present,
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              customBoxWidget(
                                background: Colorconstand.alertsDecline,
                                title: absent,
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              customBoxWidget(
                                background: Colorconstand.alertsAwaitingText,
                                title: late,
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              customBoxWidget(
                                background: Colorconstand.mainColorForecolor,
                                title: permission,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Button_Custom(
                      hightButton: 45,
                      maginRight: 25,
                      maginleft: 25,
                      titleButton: "CHECK".tr(),
                      buttonColor: Colorconstand.primaryColor,
                      radiusButton: 8,
                      titlebuttonColor: Colorconstand.neutralWhite,
                      onPressed: (){
                        BlocProvider.of<SummaryAttendanceDayBloc>(context).add( GetSummaryStudentAttendanceEvent(classIdTeacher.toString(),"${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",));
                        Navigator.push(context, MaterialPageRoute( builder: (context) => const AttendanceSummaryScreen()));
                      },
                    )
                  ],
                ),
               ),      
        
      ),

      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
          ],
        ),
      ));

  }
  Widget customBoxWidget({String? title, Color? background}) {
    return Container(
      width: 25,
      padding: const EdgeInsets.symmetric(
        vertical: 3,
      ),
      decoration: BoxDecoration(
        color: background ?? Colorconstand.alertsDecline,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        title.toString(),
        style: ThemsConstands.headline_6_semibold_14
            .copyWith(color: Colorconstand.neutralWhite),
        textAlign: TextAlign.center,
      ),
    );
  }
}
