import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/modules/tracking_location_teacher/bloc/tracking_location_bloc.dart';
import 'package:camis_teacher_application/app/modules/tracking_location_teacher/widgets/custom_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

/// Should be Backend Manage on absent in 30 minute

enum TeacherStatus{
  insideZoon,
  outsideZoon,
  absentZoon,
  presentZoon,
  ischeckeZoon
}

class TrackingLocation{
  TrackingLocation._internal();
  static TrackingLocation? _singleton = TrackingLocation._internal();
  static TrackingLocation get shared {
    _singleton ??= TrackingLocation._internal();
    return _singleton!;
  }

  Color statusColor = Colorconstand.primaryColor;
  BuildContext? context;

  CameraPosition? cameraPosition;
  Completer<GoogleMapController>? googleMapController;
  Location? location;
  LocationData? liveLocation;
  StreamSubscription<Position>? _positionStreamSubscription;
  TeacherStatus teacherStatus = TeacherStatus.insideZoon;


  // Current LatLog
  double currentLat = 0.0;
  double currentLog = 0.0;

  // School LatLog
  double schoolLatt = 0.0;
  double schoolLogg = 0.0;

  // Start-End time
  int? startTime;
  int? endTime;
  DateTime currentTime = DateTime.now();
  bool isCheckAttendance = false;
  bool isSubmittedAttendance = false;
  int radiusDistance = 0 ;
  BitmapDescriptor? userMarkerIcon;
  BitmapDescriptor? schoolMarkerIcon;
  bool isLoadingFetchingCurrentLocation = false;
  BitmapDescriptor? schoolMarker;
  int? statusAttendance;

  String? classId;
  String? subjectId;
  String? timeTSettingId;
  String? type;
  String? lat;
  String? log;

  /// This function need to initialize first in route Screen (Home and Detail Screen)
  reUseContext(BuildContext contextt){
    context = contextt;
  }

  void timePeriodic(){
    // Timer.periodic(const Duration(seconds: 1), (timer) { 
    //   currentTime = DateTime.now();
    //   print("sdfsdfdsf ${currentTime.second}");
    // });
  }

  Future<bool> requestLocationPermission() async {
    assert(context != null, 'Call reUseContext before determinePosition');
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    LocationPermission permission = await Geolocator.checkPermission();

    if (!serviceEnabled) {
      showLocationServiceDisabledSnackBar(context);
      return false;
    }
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) { 
        // Location permission denied
        print("Permission Denied"); 

        CustomDialogWidget().customDeniedDialog(context!);
        // showLocationPermissionDeniedSnackBar(context);
        return false;
      }
    }
    print("permission is allowed");
    return true; 
  }

  void saveStaffPresentLocation({required String classID,required String subjectID,required String timeTSettingID}){
    classId = classID;
    subjectId = subjectID;
    timeTSettingId = timeTSettingID;
  }

  /// This function run in second after above function
  void determinePosition({required String schoolLat, required String schoolLog,required int rDistance,required String subjectId,required String timeTableSettingID, required String classId,required int redirectFrom}) async {
    bool granted = await requestLocationPermission();
    if(granted == true){
      try {
        createCustomMarker(assetDir: "assets/icons/png/school_marker.png");
        radiusDistance = rDistance;
        // 11.575628, 104.88779
        schoolLatt =  double.parse(schoolLat.toString());
          // 11.5746642;
        schoolLogg = double.parse(schoolLog.toString());
        // 104.8930087;
        print("coverting string to double $schoolLatt $schoolLogg");
        await Geolocator.getCurrentPosition().then((value) {
          print("Get current location success");
          currentLat = value.latitude;
          //11.5749117;
          //value.latitude;
          // 11.5746642;
          currentLog = value.longitude;
          //104.8929017;
          //value.longitude;
          //  104.8930087;
          calculateDistance(schoolLat: schoolLatt,schoolLog: schoolLogg,radius: radiusDistance,classId: classId,subjectId: subjectId,timeTableSettingID: timeTableSettingID,redirectFrom: redirectFrom);
          // timePeriodic();
          // in5minutes();
        });
      } catch (e) {
        if (e is TimeoutException) {
          isLoadingFetchingCurrentLocation = false;
          print('Location request timed out.');
        } else {
          print('Error fetching location: $e');
        }
      }
    }else{
      ScaffoldMessenger.of(context!).showSnackBar(const SnackBar(content: Text('Location permission denied.')));
    }
  }

  // Calculation Distance Between Current Location with School Location
  void calculateDistance({required double schoolLat, required double schoolLog,required int radius,required String subjectId,required String timeTableSettingID, required String classId,required int redirectFrom}) async {
    double distanceInMeters = Geolocator.distanceBetween(currentLat, currentLog, schoolLat, schoolLog);
    // double.parse(radius.toString()) ;
    // Geolocator.distanceBetween(currentLat, currentLog, schoolLat, schoolLog);
    debugPrint("distanceInMeter: $distanceInMeters - radiusMeter: $radiusDistance");
    if(teacherStatus != TeacherStatus.absentZoon) {
      if (distanceInMeters > radiusDistance) {
        changeColorState(color: Colors.red);
        changeStatus(TeacherStatus.outsideZoon);
        type = "2";
        debugPrint("User outside Zone: $distanceInMeters");
        CustomDialogWidget().customDialogBox(context!, classId: classId, subjectId: subjectId,timeTableSettingID: timeTableSettingID,redirectFrom: redirectFrom);
      } else {
        // calculateDuration(startT: "13:00",endT: "",statusNo: statusAttendance!);]
        checkStaffStatus(status: statusAttendance!);
        CustomDialogWidget().customDialogBox(context!, classId: classId, subjectId: subjectId,timeTableSettingID: timeTableSettingID,redirectFrom: redirectFrom);
        type = "1";
        debugPrint("User Inside Zone $distanceInMeters");
      }
    }else{
      print("You are abset state");
    }
  }

  void checkStaffStatus({required int status}){
    if(status == 0) {
      // Not Check Yet
      statusAttendance = 0;
      changeStatus(TeacherStatus.insideZoon);
      isCheckAttendance = true;
      changeColorState(color: Colorconstand.primaryColor);
    }else if(status == 1){
      statusAttendance=1;
      isCheckAttendance =false;
      changeColorState(color: Colors.green);
      changeStatus(TeacherStatus.presentZoon);
   
    }else if(status == 2){
      statusAttendance=2;
      // Permission
    }else if(status == 3){
      // Absent
      statusAttendance=3;
      isCheckAttendance = false;
      changeColorState(color: Colors.grey);
      changeStatus(TeacherStatus.absentZoon);
    }else {
      // Submitted
      statusAttendance =4;
      isCheckAttendance = false;
      changeColorState(color: Colorconstand.mainColorForecolor);
      changeStatus(TeacherStatus.ischeckeZoon);
    }
  }

  void disposeStreamSubscription() async {
    if(_positionStreamSubscription !=null) {
      await Future.delayed(const Duration(minutes: 5),(){
        print("pause stream location");
        _positionStreamSubscription!.pause();
      });
    }
  }

  Future<BitmapDescriptor> customizeMarker({required String assetImage}) async {
    var marker = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(18, 18)), 
      assetImage, 
    );
    schoolMarker = marker;
    return marker;
  }   

  void changeStatus(TeacherStatus status) {
    if (teacherStatus != status) {
      teacherStatus = status;
    }
  }


  void showLocationServiceDisabledSnackBar(context) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Please enable location services.'),
    ));
  }

  void showLocationPermissionDeniedSnackBar(context) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Location permissions are denied.'),
    ));
  }

  void showLocationPermissionDeniedForeverSnackBar(context) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Location permissions are permanently denied.'),
    ));
  }

  void showGeneralErrorSnackBar(String message,context) {
    ScaffoldMessenger.of(context!).showSnackBar(SnackBar(
      content: Text('An error occurred: $message'),
    ));
  }

  void changeColorState({required Color color}){
    statusColor = color;
    BlocProvider.of<TrackingLocationBloc>(context!).add(ChangeColorLocationEvent(colorStatus: color));
  }

  void createCustomMarker({required String assetDir}) async {
    final ImageConfiguration imageConfiguration = createLocalImageConfiguration(context!, size: const Size.square(84));
    BitmapDescriptor.fromAssetImage(imageConfiguration,assetDir).then((value) =>  schoolMarkerIcon = value);
  }
}
