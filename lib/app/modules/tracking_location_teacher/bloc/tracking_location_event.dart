part of 'tracking_location_bloc.dart';

 class TrackingLocationEvent extends Equatable {
  const TrackingLocationEvent();

  @override
  List<Object> get props => [];
}

class GetSchoolLocationEvent extends TrackingLocationEvent{
 const GetSchoolLocationEvent();
}

class ChangeColorLocationEvent extends TrackingLocationEvent{
  final Color colorStatus;
  const ChangeColorLocationEvent({required this.colorStatus});
}

class PostAttendanceLocationEvent extends TrackingLocationEvent{
  final String classID;
  final String subjectID;
  final String timeTableSettingID;
  final String type;
  final String latitude;
  final String longtitude;

  const PostAttendanceLocationEvent({required this.classID,required this.subjectID,required this.timeTableSettingID,required this.type,required this.latitude,required this.longtitude});


}