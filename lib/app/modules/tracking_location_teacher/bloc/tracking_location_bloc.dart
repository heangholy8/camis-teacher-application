import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/modules/tracking_location_teacher/tracking_location_teacher.dart';
import 'package:camis_teacher_application/app/service/api/t_tracking_location_api/get_school_location_api.dart';
import 'package:camis_teacher_application/app/storages/user_storage.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../core/constands/color_constands.dart';
import '../../../models/tracking_location_model/school_location_model.dart';
import '../../../service/api/t_tracking_location_api/post_attendance_location_api.dart';
import '../../../storages/get_storage.dart';
part 'tracking_location_event.dart';
part 'tracking_location_state.dart';

class TrackingLocationBloc extends Bloc<TrackingLocationEvent, TrackingLocationState> {
  final GetSchoolLocationApi getSchoolLocationApi = GetSchoolLocationApi();
  final PostAttendanceLocationApi postAttLocationApi = PostAttendanceLocationApi();
  // bool getlocationLoading = false;
  Color statusColor = Colorconstand.primaryColor;
  String schoolLatitude = "";
  String schoolLogtitude = "";
  int distanceRadius = 0;
  SchoolLocationModel schoolLocationModel = SchoolLocationModel();

  TrackingLocationBloc() : super(TrackingLocationInitial()) {
    on<GetSchoolLocationEvent>((event, emit)async {
      GetStoragePref pref = GetStoragePref();
      UserSecureStroage saveStoragePref = UserSecureStroage();
      var schoolCode = await pref.getJsonToken;
      emit(GettingSchoolLocationStateLoading());
      try {
        await getSchoolLocationApi.getSchoolLocationApi(schoolCode:"${schoolCode.schoolCode}").then((value) {
          schoolLocationModel = value;
          distanceRadius = value.data!.schoolRadiusDistance!;
          schoolLatitude = value.data!.schoolLatitude!;
          schoolLogtitude = value.data!.schoolLongitude!;
          // print("gmsdl;gadskl${value.data!.schoolLatitude.toString()}");
          // print("gmsdl;gadskl${value.data!.schoolLongitude.toString()}");
          // print("gmsdl;gadskl${value.data!.schoolRadiusDistance.toString()}");
          saveStoragePref.saveSchoolLat(schoolLat:value.data!.schoolLatitude.toString(),);
          saveStoragePref.saveSchoolLog(schoolLog:value.data!.schoolLongitude.toString(),);
          saveStoragePref.saveSchoolRadius(schoolRadius:value.data!.schoolRadiusDistance.toString(),);
          emit(GettingLocationStateSuccess(schoolLocationModel: value));
        }); 
      } catch (e) {
        print("fsadfsa$e");
        emit(GettingLocationStateError());
      }
    });

    on<ChangeColorLocationEvent>((event,emit){
      try {
        emit(ChangeColorLocationLoadingState());
        statusColor = event.colorStatus;
        emit(ChangeColorLocationSuccessState());
      } catch (e) {
        print("Change Color is Error");
      }
    });
    
    on<PostAttendanceLocationEvent>((event,emit)async{
      try {
        emit(PostAttendanceLocationLoadingState());
        var data = await postAttLocationApi.postAttendanceLocationApi(classId: event.classID,subjectId: event.subjectID,timeTSId: event.timeTableSettingID,type: event.type,lat: event.latitude,log: event.longtitude);
        emit(PostAttendanceLocationSuccessState(isSuccess: data));
        TrackingLocation.shared.checkStaffStatus(status: 1);
        print("post der hx $data");
      } catch (e) {
        emit(PostAttendanceLocationStateError());
        print("Post Attendance Location is Error $e");
      }
    });
  }
}
