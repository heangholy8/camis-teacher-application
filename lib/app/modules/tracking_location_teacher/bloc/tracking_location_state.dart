part of 'tracking_location_bloc.dart';

class TrackingLocationState extends Equatable {
  const TrackingLocationState();
  
  @override
  List<Object> get props => [];
}

class TrackingLocationInitial extends TrackingLocationState {}

class GettingSchoolLocationStateLoading extends TrackingLocationState {}

class GettingLocationStateSuccess extends TrackingLocationState {
  final SchoolLocationModel schoolLocationModel;
  const GettingLocationStateSuccess({required this.schoolLocationModel});
}

class GettingLocationStateError extends TrackingLocationState {}
class ChangeColorLocationLoadingState extends TrackingLocationState{}
class ChangeColorLocationSuccessState extends TrackingLocationState{}

class PostAttendanceLocationStateError extends TrackingLocationState {}
class PostAttendanceLocationLoadingState extends TrackingLocationState{}
class PostAttendanceLocationSuccessState extends TrackingLocationState{
  final bool isSuccess;
  const PostAttendanceLocationSuccessState({required this.isSuccess});
}
