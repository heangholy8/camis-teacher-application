import 'dart:async';

import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/modules/home_screen/helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/thems/thems_constands.dart';
import '../../attendance_schedule_screen/state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../../home_screen/bloc/daskboard_screen_bloc.dart';
import '../../home_screen/bloc/my_task_daskboard_bloc.dart';
import '../bloc/tracking_location_bloc.dart';
import '../tracking_location_teacher.dart';

class CustomDialogWidget{
  Future<dynamic> customDeniedDialog(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible:false,
      builder: (ctxDialog) => SizedBox(
        height: 100,
        width: MediaQuery.of(context).size.width,
        child: AlertDialog(
          iconPadding: const EdgeInsets.all(0),
          titlePadding: const EdgeInsets.all(0),
          contentPadding: const EdgeInsets.only(left:14,right:14,bottom: 18),
          insetPadding: const EdgeInsets.symmetric(horizontal:14),
          backgroundColor: Colors.white,
          clipBehavior: Clip.antiAlias,
          shape:RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0), // Set the desired radius here
          ),
          // title: headerAlertDialogWidget(context,color:TrackingLocation.shared.statusColor),
          content: Container(
            height: 100,
            width: 300,
            color: Colors.black,
          )
        ),
      ),
    );
  }

  Future<dynamic> customDialogBox(BuildContext context,{required String subjectId,required String timeTableSettingID, required String classId,required int redirectFrom}) {
    return showDialog(
      context: context,
      barrierDismissible:false,
      builder: (ctxDialog) => SizedBox(
        height: 100,
        width: MediaQuery.of(context).size.width,
        child: AlertDialog(
          iconPadding: const EdgeInsets.all(0),
          titlePadding: const EdgeInsets.all(0),
          contentPadding: const EdgeInsets.only(left:14,right:14,bottom: 18),
          insetPadding: const EdgeInsets.symmetric(horizontal:14),
          backgroundColor: Colors.white,
          clipBehavior: Clip.antiAlias,
          shape:RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0), // Set the desired radius here
          ),
          // title: headerAlertDialogWidget(context,color:TrackingLocation.shared.statusColor),
          content: bodyAlertDialogWidget(context,color: TrackingLocation.shared.statusColor,subjectId: subjectId,classId: classId,timeTableSettingID:timeTableSettingID,redirectFrom: redirectFrom),
        ),
      ),
    );
  }
  Widget bodyAlertDialogWidget(BuildContext context,{required Color color,required String classId ,required String subjectId,required String timeTableSettingID,required int redirectFrom}) {
    int monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    int daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
    int yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState){
        return  BlocListener<TrackingLocationBloc, TrackingLocationState>(
          listener: (context, state) {
              if(state is ChangeColorLocationLoadingState){
                print("Loading");
              }else if(state is ChangeColorLocationSuccessState){
                print("Change success");
                setState((){ 
                 TrackingLocation.shared.statusColor = BlocProvider.of<TrackingLocationBloc>(context).statusColor;
                  //  TrackingLocation.shared.checkStaffStatus(status:1);
                });              
              }else{
                print("Change color error");
              }
            },
          child: SizedBox(
            height: TrackingLocation.shared.teacherStatus == TeacherStatus.ischeckeZoon? 200 : MediaQuery.of(context).size.height/1.8,
            child: Column(
              children: [
                headerAlertDialogWidget(context,(){
                  Navigator.pop(context);
                }),
                const SizedBox(height: 18,),
                TrackingLocation.shared.teacherStatus == TeacherStatus.ischeckeZoon? Container(
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(18),
                    color: TrackingLocation.shared.teacherStatus == TeacherStatus.ischeckeZoon? TrackingLocation.shared.statusColor.withOpacity(.4):TrackingLocation.shared.statusColor, 
                  ),
                  child: Text("CLICE_TO_SHOW_VALID_PRESENCE".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.primaryColor),),
                 ):  mapWidget(context),
                TrackingLocation.shared.teacherStatus != TeacherStatus.ischeckeZoon? const SizedBox(height: 14):Container(),
                TrackingLocation.shared.teacherStatus != TeacherStatus.ischeckeZoon? Container(
                  padding:const EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                    color: TrackingLocation.shared.statusColor.withOpacity(.25)
                  ),
                  child: Text(
                    TrackingLocation.shared.teacherStatus == TeacherStatus.insideZoon ? "YOU_CAN_CLICK_BELOW_TO_PRESENCE".tr() : TrackingLocation.shared.teacherStatus == TeacherStatus.outsideZoon ? "ដើម្បីអាចចុចបង្ហាញវត្តមាន ឬបន្តការចុះវត្តមានបាន":TrackingLocation.shared.teacherStatus == TeacherStatus.presentZoon?"CLICE_TO_SHOW_PRESENCE".tr():"យោងតាមការមិននៅទីតាំងសាលាហួស ៣០ នាទី",
                    maxLines: null,
                    textAlign: TextAlign.center,
                    style:ThemsConstands.subtitle1_regular_16.copyWith(color: TrackingLocation.shared.statusColor,fontWeight: FontWeight.w700) ,
                  ),
                ):Container(),
                const SizedBox(height: 14),
                TrackingLocation.shared.teacherStatus != TeacherStatus.ischeckeZoon ? 
                  BlocBuilder<TrackingLocationBloc, TrackingLocationState>(
                    builder: (context, state) {
                      if(state is PostAttendanceLocationLoadingState){
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if(state is PostAttendanceLocationStateError){
                        return InkWell(
                          onTap: (){
                            BlocProvider.of<TrackingLocationBloc>(context).add(
                              PostAttendanceLocationEvent(
                                classID: classId,
                                subjectID: subjectId,
                                timeTableSettingID: timeTableSettingID.toString(),
                                type: "1",
                                latitude: "${TrackingLocation.shared.currentLat}",
                                longtitude: "${TrackingLocation.shared.currentLog}",
                            ));
                            Future.delayed(const Duration(seconds: 2),(){
                              debugPrint("holy$redirectFrom");
                              if(redirectFrom==1){
                                BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: "$daylocal/$monthlocal/$yearchecklocal"));
                                BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"$daylocal/$monthlocal/$yearchecklocal"));
                              }
                              else{
                                BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                              }
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.all(8),
                            height: 68,
                            width: MediaQuery.of(context).size.width,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(80),
                              color: Colors.red.withOpacity(.5),
                            ),
                            child: Text("TRYAGAIN".tr(),style: ThemsConstands.headline3_medium_20_26height.copyWith(fontWeight: FontWeight.w700),),
                          ),
                        );
                      }
                      return InkWell(
                        onTap: TrackingLocation.shared.isCheckAttendance == false || TrackingLocation.shared.teacherStatus == TeacherStatus.outsideZoon ? null : (){
                          BlocProvider.of<TrackingLocationBloc>(context).add(
                            PostAttendanceLocationEvent(
                              classID: classId,
                              subjectID: subjectId,
                              timeTableSettingID: timeTableSettingID.toString(),
                              type: "1",
                              latitude: "${TrackingLocation.shared.currentLat}",
                              longtitude: "${TrackingLocation.shared.currentLog}",
                          ));
                          Future.delayed(const Duration(seconds: 2),(){
                            debugPrint("holy$redirectFrom");
                             if(redirectFrom==1){
                                BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: "$daylocal/$monthlocal/$yearchecklocal"));
                                BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"$daylocal/$monthlocal/$yearchecklocal"));
                              }
                              else{
                                BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                              }
                          });
                        },
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          height: 68,
                          width: MediaQuery.of(context).size.width,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(80),
                            color: TrackingLocation.shared.teacherStatus == TeacherStatus.outsideZoon?TrackingLocation.shared.statusColor.withOpacity(.3):TrackingLocation.shared.statusColor
                          ),
                          child: TrackingLocation.shared.teacherStatus == TeacherStatus.insideZoon? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("STARTATTENDANCE".tr(),style:ThemsConstands.headline_2_semibold_24.copyWith(color:Colorconstand.neutralWhite) ,),
                              const SizedBox(width:18),
                              SvgPicture.asset(ImageAssets.time_starter,width:42)
                            ],
                          ):TrackingLocation.shared.teacherStatus == TeacherStatus.presentZoon? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(ImageAssets.time_starter,width: 42,),
                              const SizedBox(width: 18),
                              Text("RECORDINGATTENDANCE".tr(),style:ThemsConstands.headline_2_semibold_24.copyWith(color:Colorconstand.neutralWhite) ,),
                            ],
                          ):TrackingLocation.shared.teacherStatus == TeacherStatus.outsideZoon ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(ImageAssets.cross_pin,width: 42,),
                              const SizedBox(width: 18,),
                              Text("LOCATIONZOON".tr(),style:ThemsConstands.headline_2_semibold_24.copyWith(color:TrackingLocation.shared.statusColor) ,),
                            ],
                          ):Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(ImageAssets.time_starter,width: 38,),
                              const SizedBox(width:18),
                              Expanded(
                                child: Text("ABSENTLOCATION".tr(),style:ThemsConstands.headline3_semibold_20.copyWith(color:Colorconstand.neutralWhite),
                                  maxLines: null, 
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                     
                    },
                ): Container(
                   padding: const EdgeInsets.all(8),
                    height: 68,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(80),
                      color: TrackingLocation.shared.teacherStatus == TeacherStatus.ischeckeZoon?TrackingLocation.shared.statusColor.withOpacity(.3):TrackingLocation.shared.statusColor
                    ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ImageAssets.tick_circle_icon,width: 38,),
                      Text("PRESENTLOCATION".tr(),style:ThemsConstands.headline_2_semibold_24.copyWith(color:Colorconstand.primaryColor),
                        maxLines: null,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }


  Widget mapWidget(BuildContext context) {
    return Expanded(
      child: 
        GoogleMap(
          mapType: MapType.normal,
          myLocationEnabled:true,
          indoorViewEnabled:true,
          myLocationButtonEnabled:true,
          compassEnabled:true,
          zoomControlsEnabled: true,
          zoomGesturesEnabled: true,
          mapToolbarEnabled:true,
          circles:{
            Circle(
              circleId: const CircleId("schoolCircle"),
              radius: TrackingLocation.shared.radiusDistance.toDouble(),
              center:LatLng(TrackingLocation.shared.schoolLatt, TrackingLocation.shared.schoolLogg),
              strokeWidth: 2,
              strokeColor: TrackingLocation.shared.statusColor,
              fillColor: TrackingLocation.shared.statusColor.withOpacity(0.3)
            ),
          },
          initialCameraPosition: CameraPosition(
            bearing: 192.8334901395799,
            target: LatLng(TrackingLocation.shared.currentLat, TrackingLocation.shared.currentLog),
            tilt: 0.0,
            zoom: 17.151926040649414,
          ),
          markers: {  
            Marker(
              markerId: const MarkerId("anotherCircle"),
              infoWindow:const InfoWindow(title: "School Location"),
              position: LatLng(TrackingLocation.shared.schoolLatt, TrackingLocation.shared.schoolLogg),
              icon: TrackingLocation.shared.schoolMarkerIcon!,
              // BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure),
            ),
          },
        )
    );
  }

  Widget headerAlertDialogWidget(context,Function()? onPressed) {
    return StatefulBuilder(
      builder:(context,setState) {
        return Row (
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            IconButton (
              onPressed: onPressed,
              icon: const Icon(Icons.cancel_outlined)
            ),
            Expanded(
              child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SvgPicture.asset(ImageAssets.calendar_tick_icon,color:TrackingLocation.shared.statusColor,width: 26),
                    const SizedBox(width: 8),
                    Text("${TrackingLocation.shared.currentTime.day} ${monthNamesKh[TrackingLocation.shared.currentTime.month]}",style: ThemsConstands.headline_4_medium_18.copyWith(color: TrackingLocation.shared.statusColor))
                  ],
                ),
                // Row(
                //   children: [
                //     SvgPicture.asset(ImageAssets.hour_time_icon,color: TrackingLocation.shared.statusColor,width: 26),
                //     const SizedBox(width: 8),
                //     Text("${currentTime.hour}:${currentTime.minute} ${currentTime.hour>=12?"រសៀល":"ព្រឹក"}",style: ThemsConstands.headline_4_medium_18.copyWith(color: TrackingLocation.shared.statusColor)),
                //   ],
                // ),
              ],
            ),
          ),
          ],
        );
      },
    );
  }
}

