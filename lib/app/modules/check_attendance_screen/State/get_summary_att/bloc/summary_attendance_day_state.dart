part of 'summary_attendance_day_bloc.dart';

abstract class SummaryAttendanceDayState extends Equatable {
  const SummaryAttendanceDayState();
  
  @override
  List<Object> get props => [];
}

class SummaryAttendanceDayInitial extends SummaryAttendanceDayState {}

class GetSummaryStudentAttendanceListLoading extends SummaryAttendanceDayState{}

class GetSummaryStudentAttendanceListLoaded extends SummaryAttendanceDayState{
  final SummaryAttendanceModel? summaryAttendanceList;
  const GetSummaryStudentAttendanceListLoaded({required this.summaryAttendanceList});
}

class GetSummaryStudentAttendanceListListError extends SummaryAttendanceDayState{}
