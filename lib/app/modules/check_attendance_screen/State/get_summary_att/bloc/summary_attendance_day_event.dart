part of 'summary_attendance_day_bloc.dart';

abstract class SummaryAttendanceDayEvent extends Equatable {
  const SummaryAttendanceDayEvent();

  @override
  List<Object> get props => [];
}

class GetSummaryStudentAttendanceEvent extends SummaryAttendanceDayEvent{
  final String idClass;
  final String date;
  const GetSummaryStudentAttendanceEvent(this.idClass, this.date);
}
