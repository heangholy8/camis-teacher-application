import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/check_attendance_model/summary_attendance_model.dart';
import '../../../../../service/api/check_attendance_api/get_list_student_attendance.dart';

part 'summary_attendance_day_event.dart';
part 'summary_attendance_day_state.dart';

class SummaryAttendanceDayBloc extends Bloc<SummaryAttendanceDayEvent, SummaryAttendanceDayState> {
  final GetCheckStudentAttendanceList getSummaryStudentAttendanceList;
  List<ListAttendance>? studentList;
  SummaryAttendanceDayBloc({required this.getSummaryStudentAttendanceList}) : super(SummaryAttendanceDayInitial()) {
    on<GetSummaryStudentAttendanceEvent>((event, emit) async {
      emit(GetSummaryStudentAttendanceListLoading());
      try {
        var dataSummaryAttendance = await getSummaryStudentAttendanceList.getSummaryAttendanceList(class_id: event.idClass,date: event.date);
        studentList = dataSummaryAttendance.data!.listAttendance;
        emit(GetSummaryStudentAttendanceListLoaded(summaryAttendanceList: dataSummaryAttendance));
      } catch (e) {
        emit(GetSummaryStudentAttendanceListListError());
      }
    });
  }
}
