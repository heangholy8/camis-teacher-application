part of 'post_attendance_bloc.dart';

abstract class PostAttendanceEvent extends Equatable {
  const PostAttendanceEvent();

  @override
  List<Object> get props => [];
}

class PostAttDEvent extends PostAttendanceEvent {
  final Data data;
  const PostAttDEvent ( {required this.data});
}
class PostAttPrimaryEvent extends PostAttendanceEvent {
  final Data data;
  const PostAttPrimaryEvent ({required this.data});
}
