part of 'check_attendance_bloc.dart';

abstract class CheckStudentAttendanceEvent extends Equatable {
  const CheckStudentAttendanceEvent();

  @override
  List<Object> get props => [];
}

class GetCheckStudentAttendanceEvent extends CheckStudentAttendanceEvent{
  final String idClass;
  final String idSchedule;
  final String date;
  const GetCheckStudentAttendanceEvent(this.idClass, this.idSchedule, this.date);
}