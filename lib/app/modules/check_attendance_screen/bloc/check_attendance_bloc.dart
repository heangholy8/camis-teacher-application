import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/check_attendance_api/get_list_student_attendance.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '../../../models/check_attendance_model/check_attendance_model.dart';

part 'check_attendance_event.dart';
part 'check_attendance_state.dart';

class CheckAttendanceBloc extends Bloc<CheckStudentAttendanceEvent, CheckAttendanceState> {
  final GetCheckStudentAttendanceList getCheckStudentAttendanceList;
  List<ListAttendance>? studentList;
  CheckAttendanceBloc({required this.getCheckStudentAttendanceList})
      : super(CheckAttendanceInitial()) {
    on<GetCheckStudentAttendanceEvent>((event, emit) async {
      emit(GetCheckStudentAttendanceListLoading());
      try {
        var dataCheckAttendance = await getCheckStudentAttendanceList.getCheckAttendanceList(class_id: event.idClass,date: event.date,schedule_id: event.idSchedule);
         studentList = dataCheckAttendance.data!.listAttendance;
        emit(GetCheckStudentAttendanceListLoaded(checkAttendanceList: dataCheckAttendance));
      } catch (e) {
      
        debugPrint("Error get student attendance $e.toString()");
        emit(GetCheckStudentAttendanceListListError());
      }
    });
  }
}
