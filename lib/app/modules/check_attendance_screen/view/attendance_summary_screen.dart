import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/State/get_summary_att/bloc/summary_attendance_day_bloc.dart';
import 'package:camis_teacher_application/widget/custom_appbar_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../models/check_attendance_model/summary_attendance_model.dart';

class AttendanceSummaryScreen extends StatefulWidget {
  const AttendanceSummaryScreen({Key? key}) : super(key: key);

  @override
  State<AttendanceSummaryScreen> createState() => _AttendanceSummaryScreenState();
}

class _AttendanceSummaryScreenState extends State<AttendanceSummaryScreen> with Toast {
  ScrollController scrollController = ScrollController();
  int isAcitve = 0;

  bool connection = true;
  bool _isNotFound = false;
  StreamSubscription? sub;
  final TextEditingController searchController = TextEditingController();
  List<ListAttendance>? _filterStudent;

  @override
  void initState() {
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {} 
        else {}
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  int absent = 0, present = 0, late = 0;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var translate = context.locale.toString();
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            children: [
              CustomAppBarWidget(
                width: width,
                title: "CHECKATTENDANCE".tr(),
              ),
              Expanded(
                child: BlocConsumer<SummaryAttendanceDayBloc, SummaryAttendanceDayState>(
                  listener: (context, state) {
                    if(state is GetSummaryStudentAttendanceListLoaded){
                      var datacheckStu = state.summaryAttendanceList!.data;
                      _filterStudent = BlocProvider.of<SummaryAttendanceDayBloc>(context).studentList!;
                    }
                  },
                  builder: (context, state) {
                    if (state is GetSummaryStudentAttendanceListLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is GetSummaryStudentAttendanceListLoaded) {
                      final dataCheckAttendance = state.summaryAttendanceList!.data;
                      return dataCheckAttendance!.listAttendance!.isEmpty? 
                      Container(
                        alignment: Alignment.center,
                        child: EmptyWidget(title: "NO_STUDENT".tr()
                        , subtitle: ""),
                      )
                      :Stack(
                        children: [
                           Column(
                            children: [
                              _buildTextFormField(
                                  controller: searchController,
                                  attendanceModel: state.summaryAttendanceList!),
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 14),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SvgPicture.asset(ImageAssets.location_icon),
                                        const SizedBox(
                                          width: 4,
                                        ),
                                        Text(
                                          dataCheckAttendance.className!,
                                          style:ThemsConstands.headline_5_semibold_16,
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        SvgPicture.asset(ImageAssets.calendar_icon),
                                        const SizedBox(width:4),
                                        Row(
                                          children: [
                                            Text(
                                              dataCheckAttendance.date!,
                                              style: ThemsConstands.headline_5_semibold_16,
                                            ),
                                            const CircleAvatar(
                                              radius: 2,
                                              backgroundColor: Colors.black,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: NotificationListener(
                                  child: SingleChildScrollView(
                                    controller: scrollController,
                                    child: !_isNotFound
                                        ? Column(
                                            children: [
                                              _filterStudent!.where((element) => element.attendanceStatus == 2).isNotEmpty
                                                  ? Container(
                                                      width: width,
                                                      padding: const EdgeInsets.symmetric(
                                                          vertical: 8,
                                                          horizontal: 18),
                                                      decoration:const BoxDecoration(
                                                        color: Colorconstand.neutralSecondBackground,
                                                        border: Border(top: BorderSide(color: Colorconstand.darkTextsDisabled),),
                                                      ),
                                                      child: Text(
                                                        "${"ABSENT".tr()} (${_filterStudent!.where((element) => element.attendanceStatus == 2).length})",
                                                        style: ThemsConstands.headline_6_semibold_14
                                                            .copyWith(color: Colorconstand.alertsDecline),
                                                      ),
                                                    )
                                                  : Container(),
                                              ListView.separated(
                                                 padding:const EdgeInsets.all(0),
                                                physics: const ScrollPhysics(),
                                                shrinkWrap: true,
                                                itemCount: _filterStudent!.where((element) => element.attendanceStatus ==2).length,
                                                itemBuilder: (context, indexabsent) {
                                                  return Container(
                                                    padding:const EdgeInsets.symmetric(
                                                            horizontal: 18,
                                                            vertical: 8),
                                                    width: width,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            CircleAvatar(
                                                                radius: 30,
                                                                backgroundImage: NetworkImage(_filterStudent!.where((element) => element.attendanceStatus ==2) .toList()[
                                                                        indexabsent]
                                                                    .profileMedia!
                                                                    .fileShow
                                                                    .toString())),
                                                            const SizedBox(
                                                              width: 10,
                                                            ),
                                                            SizedBox(
                                                              width: width / 1.8,
                                                              child: Text(
                                                              translate=="km"?  _filterStudent! .where((element) => element.attendanceStatus == 2).toList()[indexabsent].name.toString():
                                                              _filterStudent!.where((element) => element.attendanceStatus == 2).toList()[indexabsent].nameEn==" "?"No Name":
                                                               _filterStudent!.where((element) => element.attendanceStatus == 2).toList()[indexabsent].nameEn.toString(),
                                                                maxLines: null,
                                                                style: ThemsConstands.headline_4_medium_18,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            setState(() {
                                                            });
                                                          },
                                                          child: Container(
                                                            width: 60,
                                                            height: 60,
                                                            decoration:
                                                                BoxDecoration(
                                                              color:  Colorconstand.alertsDecline,
                                                              borderRadius: BorderRadius.circular(18),
                                                            ),
                                                            child:const Icon( Icons.close_rounded,
                                                              size: 40,
                                                              color: Colors.white,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  );
                                                },
                                                separatorBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return const Padding(
                                                    padding:
                                                        EdgeInsets.only(left: 100),
                                                    child: Divider(
                                                      thickness: 0.5,
                                                      height: 1,
                                                    ),
                                                  );
                                                },
                                              ),
                                              //------------------------------------------------
              
                                              //---------------- Late -----------------------
                                              _filterStudent!.where((element) => element.attendanceStatus == 3).toList().isNotEmpty
                                                  ? Container(
                                                      width: width,
                                                      padding: const EdgeInsets.symmetric(
                                                          vertical: 8,horizontal: 18),
                                                      decoration:const BoxDecoration(
                                                        color: Colorconstand.neutralSecondBackground,
                                                        border: Border(
                                                          top: BorderSide(
                                                            color: Colorconstand.darkTextsDisabled,
                                                          ),
                                                        ),
                                                      ),
                                                      child: Text(
                                                        "${"LATE1".tr()} (${_filterStudent!.where((element) => element.attendanceStatus == 3).length})",
                                                        style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.alertsAwaitingText),
                                                      ),
                                                    )
                                                  : Container(),
                                                    _filterStudent!.where((element) => element.attendanceStatus == 3).toList().isEmpty
                                                  ? Container()
                                                  : ListView.separated(
                                                    padding:const EdgeInsets.all(0),
                                                      physics:const ScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount: _filterStudent!.where((element) => element.attendanceStatus == 3).toList().length,
                                                      itemBuilder:
                                                          (context, indexabsent) {
                                                        return Container(
                                                          padding: const EdgeInsets
                                                                  .symmetric(
                                                              horizontal: 18,
                                                              vertical: 8),
                                                          width: width,
                                                          child: Row(
                                                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Row(
                                                                children: [
                                                                  CircleAvatar(
                                                                    radius: 30,
                                                                    backgroundImage:
                                                                        NetworkImage(
                                                                      _filterStudent!.where((element) =>element.attendanceStatus ==3).toList()[indexabsent].profileMedia!.fileShow.toString(),
                                                                    ),
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 10,
                                                                  ),
                                                                  SizedBox(
                                                                    width:
                                                                        width / 1.8,
                                                                    child: Text(
                                                                    translate=="km"?  _filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].name.toString():
                                                                    _filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].nameEn==" "?
                                                                   "No Name" :_filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].name.toString(),
                                                                      maxLines:
                                                                          null,
                                                                      style: ThemsConstands.headline_4_medium_18,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              GestureDetector(
                                                                onTap: () {
                                                                  setState(() {
                                                                  });
                                                                },
                                                                child: Container(
                                                                  width: 60,
                                                                  height: 60,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color:  Colorconstand.alertsAwaitingText,
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                18),
                                                                  ),
                                                                  child:const Icon(Icons.access_time,
                                                                      size: 40,
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                      separatorBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return const Padding(
                                                          padding: EdgeInsets.only(
                                                              left: 100),
                                                          child: Divider(
                                                            thickness: 0.5,
                                                            height: 1,
                                                          ),
                                                        );
                                                      },
                                                    ),
                                              //------------------------------------------------
                                              //================== PERMESTION REQUEST ============
                                              _filterStudent!.where((element) =>element.attendanceStatus == 4).toList().isNotEmpty
                                                  ? Container(
                                                      width: width,
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 8,
                                                          horizontal: 18),
                                                      decoration:
                                                          const BoxDecoration(
                                                        color: Colorconstand
                                                            .neutralSecondBackground,
                                                        border: Border(
                                                          top: BorderSide(
                                                            color: Colorconstand
                                                                .darkTextsDisabled,
                                                          ),
                                                        ),
                                                      ),
                                                      child: Text(
                                                        "${"PERMISSION".tr()} (${_filterStudent!.where((element) => element.attendanceStatus == 4).length})",
                                                        style: ThemsConstands.headline_6_semibold_14
                                                            .copyWith(color: Colorconstand.mainColorForecolor),
                                                      ),
                                                    )
                                                  : Container(),
              
                                              _filterStudent!.where((element) =>element.attendanceStatus == 4).toList().isEmpty
                                                  ? Container()
                                                  : ListView.separated(
                                                    padding:const EdgeInsets.all(0),
                                                      physics:const ScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount: _filterStudent!.where((element) =>element.attendanceStatus == 4).toList().length,
                                                      itemBuilder:
                                                          (context, indexabsent) {
                                                        return Container(
                                                          padding: const EdgeInsets.symmetric(horizontal:18, vertical:8),
                                                          width: width,
                                                          child: Row(
                                                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Row(
                                                                children: [
                                                                  CircleAvatar(
                                                                    radius: 30,
                                                                    backgroundImage:
                                                                        NetworkImage(
                                                                      _filterStudent!.where((element) =>element.attendanceStatus == 4).toList()[indexabsent].profileMedia!.fileShow.toString(),
                                                                    ),
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 10,
                                                                  ),
                                                                  SizedBox(
                                                                    width:
                                                                        width / 1.8,
                                                                    child: Text(
                                                                    translate=="km"?  _filterStudent!.where((element) => element.attendanceStatus == 4).toList()[indexabsent].name.toString():
                                                                    _filterStudent!.where((element) => element.attendanceStatus == 4).toList()[indexabsent].nameEn==" "?
                                                                   "No Name" :_filterStudent!.where((element) => element.attendanceStatus == 4).toList()[indexabsent].name.toString(),
                                                                      maxLines:
                                                                          null,
                                                                      style: ThemsConstands.headline_4_medium_18,
                                                                          
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              GestureDetector(
                                                                onTap: () {
                                                                  setState(() {
                                                                  });
                                                                },
                                                                child: Container(
                                                                  width: 60,
                                                                  height: 60,
                                                                  decoration:BoxDecoration(color: Colorconstand.mainColorForecolor,
                                                                    borderRadius:BorderRadius.circular(18),
                                                                  ),
                                                                  child:const Icon(
                                                                    Icons.check_rounded,size: 40,color: Colors.white),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                      separatorBuilder:
                                                          (BuildContext context,int index) {
                                                        return const Padding(
                                                          padding: EdgeInsets.only(left: 100),
                                                          child: Divider(
                                                            thickness: 0.5,
                                                            height: 1,
                                                          ),
                                                        );
                                                      },
                                                    ),
                                              //------------------- End Permistion-----------------------------
                                              _filterStudent!
                                                      .where((element) =>
                                                          element
                                                              .attendanceStatus ==
                                                          1)
                                                      .toList()
                                                      .isNotEmpty
                                                  ? Column(
                                                      children: [
                                                        Container(
                                                          width: width,
                                                          padding: const EdgeInsets
                                                                  .symmetric(
                                                              vertical: 8,
                                                              horizontal: 18),
                                                          decoration:
                                                              const BoxDecoration(
                                                            color: Colorconstand
                                                                .neutralSecondBackground,
                                                          ),
                                                          child: Text(
                                                            "${"PRESENT".tr()} (${_filterStudent!.where((element) => element.attendanceStatus == 1).length})",
                                                            style: ThemsConstands.headline_6_semibold_14.copyWith(
                                                                    color: Colorconstand
                                                                        .alertsPositive),
                                                          ),
                                                        ),
                                                        ListView.separated(
                                                          padding:const EdgeInsets.only(bottom: 130,left: 0,right: 0,top: 0),
                                                          physics:const ScrollPhysics(),
                                                          shrinkWrap: true,
                                                          itemCount: _filterStudent!
                                                              .where((element) =>
                                                                  element
                                                                      .attendanceStatus ==
                                                                  1)
                                                              .toList()
                                                              .length,
                                                          itemBuilder:
                                                              (context, index) {
                                                            return Container(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal:
                                                                          18,
                                                                      vertical: 8),
                                                              width: width,
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  Row(
                                                                    children: [
                                                                      CircleAvatar(
                                                                          radius:
                                                                              30,
                                                                          backgroundImage: NetworkImage(_filterStudent!
                                                                              .where((element) =>
                                                                                  element.attendanceStatus ==
                                                                                  1)
                                                                              .toList()[
                                                                                  index]
                                                                              .profileMedia!
                                                                              .fileShow
                                                                              .toString())),
                                                                      const SizedBox(
                                                                        width: 10,
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            width /
                                                                                1.8,
                                                                        child: Text(
                                                                        translate=="km"?  _filterStudent! .where((element) => element.attendanceStatus == 1).toList()[index].name.toString():
                                                                        _filterStudent! .where((element) => element.attendanceStatus == 1).toList()[index].nameEn==" "?"No Name":
                                                                        _filterStudent! .where((element) => element.attendanceStatus == 1).toList()[index].nameEn.toString(),
                                                                          maxLines:
                                                                              null,
                                                                          style: ThemsConstands.headline_4_medium_18,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  Container(
                                                                      width: 60,
                                                                      height: 60,
                                                                      decoration:
                                                                    BoxDecoration(
                                                                  color:Colorconstand.alertsPositive,
                                                                  borderRadius:BorderRadius.circular(18),
                                                                      ),
                                                                      child:const Icon( Icons.check_rounded,size: 40,
                                                                    color: Colors.white),
                                                                    ),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                          separatorBuilder:
                                                              (BuildContext context,
                                                                  int index) {
                                                            return const Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      left: 100),
                                                              child: Divider(
                                                                thickness: 0.5,
                                                                height: 1,
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ],
                                                    )
                                                  : Container(),
                                            ],
                                          )
                                        : Container(
                                            alignment: Alignment.center,
                                            width: width,
                                            height: height / 1.6,
                                            child: Text(
                                              "NODDATA".tr(),
                                              style: ThemsConstands
                                                  .headline3_semibold_20,
                                            ),
                                          ),
                                  ),
                                ),
                              ),
                              // customeBottomBoxWidget(dataCheckAttendance),
                            ],
                          ),
                          AnimatedPositioned(
                            duration: const Duration(milliseconds: 15),
                            bottom:  0,
                            left: 0,
                            right: 0,
                            child: customeBottomBoxWidget(dataCheckAttendance),
                          ),
                        ],
                      );
                    } else {
                      return Center(
                        child: EmptyWidget(
                      title: "WE_DETECT".tr(),
                      subtitle: "WE_DETECT_DES".tr(),
                    ),
                      ); 
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget customeBottomBoxWidget(DataSummaryAtt dataCheckAttendance) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 15),
      padding: const EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: Colorconstand.neutralWhite,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x919E9E9E),
            offset: Offset(0, 0),
            blurRadius: 3.0,
            spreadRadius: 1.0,
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 22),
                  child: Text(
                    "ATTENDANCE".tr(),
                    style: ThemsConstands.headline_5_semibold_16
                        .copyWith(color: Colorconstand.neutralDarkGrey),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(right: 22,left: 22,top: 5,bottom: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        width: 25,
                        padding: const EdgeInsets.symmetric(
                          vertical: 3,
                        ),
                        decoration: BoxDecoration(
                          color: Colorconstand.alertsPositive,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Text(
                          dataCheckAttendance.attendance!.present.toString(),
                          style: ThemsConstands.headline_5_semibold_16
                              .copyWith(color: Colorconstand.neutralWhite),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      customBoxWidget(
                        background: Colorconstand.alertsDecline,
                        title: dataCheckAttendance.attendance!.absent.toString(),
                      ),
                      
                      const SizedBox(
                        width: 8,
                      ),
                      Container(
                        width: 25,
                        padding: const EdgeInsets.symmetric(
                          vertical: 3,
                        ),
                        decoration: BoxDecoration(
                          color: Colorconstand.alertsAwaitingText,
                          borderRadius: BorderRadius.circular(2),
                        ),
                        child: Text(dataCheckAttendance.attendance!.late.toString(),
                          style: ThemsConstands.headline_6_semibold_14
                              .copyWith(color: Colorconstand.neutralWhite),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      customBoxWidget(
                        background: Colorconstand.mainColorForecolor,
                        title: dataCheckAttendance.attendance!.permission.toString(),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget customBoxWidget({String? title, Color? background}) {
    return Container(
      width: 25,
      padding: const EdgeInsets.symmetric(
        vertical: 3,
      ),
      decoration: BoxDecoration(
        color: background ?? Colorconstand.alertsDecline,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        title.toString(),
        style: ThemsConstands.headline_6_semibold_14
            .copyWith(color: Colorconstand.neutralWhite),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTextFormField(
      {required TextEditingController controller,
      required SummaryAttendanceModel attendanceModel}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: TextFormField(
        onChanged: ((value) {
          _filter(
            query: value,
            allResult: attendanceModel.data!.listAttendance,
          );
        }),
        controller: controller,
        maxLines: 1,
        style: ThemsConstands.subtitle1_regular_16,
        decoration: InputDecoration(
          prefixIcon: Padding(
            padding: const EdgeInsets.all(14.0),
            child: SvgPicture.asset(
              ImageAssets.search_icon,
              width: 18,
              height: 18,
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(color: Color(0xFFE5E5E5)),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(color: Color(0xFFE5E5E5)),
          ),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
          hintText: "SEACHSTUDENT".tr(),
          hintStyle: ThemsConstands.subtitle1_regular_16.copyWith(
            color: const Color(0xFFE4E4E4),
          ),
        ),
      ),
    );
  }

  void _filter({
    required String query,
    required List<ListAttendance>? allResult,
  }) {
    List<ListAttendance>? results = [];

    if (query.isEmpty || query == " ") {
      setState(() {
        results = allResult;
      });
    } else {
      results = allResult!
          .where((element) =>
              element.name!.toLowerCase().contains(query.toLowerCase()))
          .toList();
      if (results.isNotEmpty) {
        setState(() {
          _isNotFound = false;
        });
        debugPrint("NotFound: $_isNotFound");
        // for (var element in results) {
        //   debugPrint("Student list: ${element.name}");
        // }
      } else {
        setState(() {
          _isNotFound = true;
          debugPrint("NotFound: $_isNotFound");
        });
      }
    }
    setState(() {
      _filterStudent = results;
      // for (var element in _filterStudent!) {
      //   debugPrint("Student listtttttttt: ${element.name} ${_filterStudent!.length}");
      // }
    });
  }
}
