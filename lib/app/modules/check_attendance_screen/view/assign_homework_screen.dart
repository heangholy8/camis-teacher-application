import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/view/preview_attachfile_widget.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/widgets/type_announcement_create.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:images_picker/images_picker.dart'; 
import '../../../service/api/schedule_api/post_schedule_activity_api.dart';
import '../../schedule_expanded_screen/attendance_widget/date_time_widget.dart';
import '../cubit_attach/cubit_attachment.dart';

class AssignHomeWorkScreen extends StatefulWidget {
  final bool? hideTitle;
  final bool activeMySchedule;
  final String date;
  final String scheduleId;
  final String classId;
  String? title;
  String? description;
  List<String>? attachment ;

  AssignHomeWorkScreen({super.key,this.title,this.description, this.attachment,this.hideTitle=false, required this.date, required this.scheduleId, required this.activeMySchedule, required this.classId,});

  @override
  State<AssignHomeWorkScreen> createState() => _AssignHomeWorkScreenState();
}

class _AssignHomeWorkScreenState extends State<AssignHomeWorkScreen> with Toast {
  final TextEditingController titleAnnouncement = TextEditingController();
  final TextEditingController subtitleAnnouncement =TextEditingController();
  List<File> listMultiMedia = [];
  bool isNoTitle = false;
  bool isPoptitle = false;

  final ImagePicker _picker = ImagePicker();
  
  @override
  void initState() {
    super.initState();  
    // checkTitleDes();
    titleAnnouncement.addListener((){
      if(titleAnnouncement.text.trim().isEmpty){
        setState(() {
          isNoTitle = false;
        });
      }else{
        setState(() {
          isNoTitle=true;
        });
      }
    });
    subtitleAnnouncement.addListener((){
     
    });
  }

  void checkTitleDes(){
    if(widget.title!.isNotEmpty && widget.description!.isNotEmpty){
      setState(() {
        titleAnnouncement.text = widget.title.toString();
        subtitleAnnouncement.text = widget.description.toString();
      });
    }else if(widget.title!.isNotEmpty){
      setState(() {
         titleAnnouncement.text = widget.title.toString();
      });
    }else{
      setState(() {
         subtitleAnnouncement.text = widget.description.toString();
      });
    }

    // if(BlocProvider.of<AttachCubit>(context).title.isNotEmpty ||BlocProvider.of<AttachCubit>(context).description.isNotEmpty ){
    //   titleAnnouncement.text = BlocProvider.of<AttachCubit>(context).title.toString();
    //   subtitleAnnouncement.text = BlocProvider.of<AttachCubit>(context).description.toString();
    // }else{
    //   titleAnnouncement.text = "";
    //   subtitleAnnouncement.text= "";
    // }
  }

  @override
  void dispose() {
    titleAnnouncement.dispose();
    subtitleAnnouncement.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      backgroundColor:Colorconstand.screenWhite,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor:Colorconstand.screenWhite,
        leading: IconButton(onPressed: (){
           Navigator.pop(context);
        }, icon: SvgPicture.asset(ImageAssets.chevron_left,width: 32,)),
        title: Text('HOMEWORK'.tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colors.black),),
        actions:[
          GestureDetector(
            onTap:() {},
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: SvgPicture.asset(
                ImageAssets.trash_icon,
                width: 28,
                color: Colors.grey,
              ),
            ),
          ),    
        ]
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Padding(
            //   padding: const EdgeInsets.only(top: 16.0, bottom: 20),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: [
            //         GestureDetector(
            //           onTap:(){
            //             Navigator.pop(context);
            //           },
            //           child: SvgPicture.asset(ImageAssets.chevron_left,color: Colors.black,width: 32,),
            //         ),
            //         Text(
            //           'ការជូនដំណឹង',
            //             style: ThemsConstands.headline3_semibold_20.copyWith(color: Colors.black),
            //         ),
            //         GestureDetector(
            //           onTap:() {     
            //           },
            //           child: Padding(
            //             padding: const EdgeInsets.symmetric(horizontal: 18.0),
            //             child: SvgPicture.asset(
            //               ImageAssets.send_outline_icon,
            //               width: 28,
            //               color: Colors.grey,
            //             ),
            //           ),
            //         ),
            //     ],
            //   )
            //   // HeaderWidget(
            //   //   disabled: !isNoTitle,
            //   //   onBack: (){
            //   //     Navigator.pop(context,true);
            //   //     // if(BlocProvider.of<AttachCubit>(context).filesMedia.isNotEmpty|| BlocProvider.of<AttachCubit>(context).title.isNotEmpty){
            //   //     //   showAlertLogout(
            //   //     //     icon:SvgPicture.asset(ImageAssets.warning_icon,width: 48,color: Colors.red,),
            //   //     //     onSubmit: () {  
            //   //     //       setState(() {
            //   //     //           BlocProvider.of<AttachCubit>(context).removeAllFile();
            //   //     //       });
            //   //     //       Navigator.pop(context);
            //   //     //       Navigator.pop(context);
            //   //     //     },
            //   //     //     title: "DISCARD".tr(),
            //   //     //     onCancelTitle: "NO".tr(),
            //   //     //     onSubmitTitle: "OKAY".tr(),
            //   //     //     context: context
            //   //     //   );
            //   //     // }else{
            //   //     //   
            //   //     // }
            //   //   },
            //   //   screenTitle: "HOMEWORK".tr(),
            //   //   onSend: (){
            //   //     if(titleAnnouncement.text.trim().isNotEmpty){
            //   //        PostActvityApi().postActivityApi(
            //   //         title: titleAnnouncement.text,
            //   //         date: widget.date.toString(),
            //   //         activityType: "2",
            //   //         description: subtitleAnnouncement.text,
            //   //         scheduleId: widget.scheduleId.toString(),
            //   //         expiredDate: "29/07/2023" ,
            //   //         attachments: BlocProvider.of<AttachCubit>(context).filesMedia
            //   //       ).then((value) {
            //   //         if(value){
            //   //           if(widget.activeMySchedule == true ){                                                                                                
            //   //             BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: widget.date));
            //   //           }
            //   //           else{
            //   //             BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: widget.date, classid: widget.classId.toString()));
            //   //           }
            //   //           Navigator.pop(context);
            //   //         }else{
            //   //           print("fail");
            //   //         }
            //   //       });
            //   //     }else{
            //   //       setState(() {
            //   //         isPoptitle=true;
            //   //       }); 
            //   //       Future.delayed(const Duration(seconds: 4),(){
            //   //         setState(() {
            //   //            isPoptitle = false;
            //   //         });
            //   //       });
            //   //     } 
            //   //   },
            //   // ),
            // ),
         
            Expanded(
              child: Stack(
                children: [
                  Container(
                    alignment: Alignment.topCenter,
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 26.0, right: 20),
                    width: double.maxFinite,
                    decoration: const BoxDecoration(
                      color: Colorconstand.screenWhite,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                      ),
                    ),
                    child: Column(
                      children: [
                        Column(
                          children: [
                            widget.hideTitle == false ? 
                              TextField(
                                maxLines: null,
                                controller: titleAnnouncement,
                                keyboardType: TextInputType.text,
                                style: ThemsConstands.headline3_semibold_20,
                                decoration: InputDecoration(
                                  hintText: "HINTANN".tr(),
                                  hintStyle:ThemsConstands.headline3_semibold_20,
                                  border:  const OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    borderSide: BorderSide(color: Colorconstand.neutralGrey,)
                                  ),
                                ),
                              ) : Container(),
                            const Divider(
                              color: Colorconstand.neutralGrey,
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height*.4,
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.symmetric(horizontal:12,vertical: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(color: Colorconstand.neutralGrey)
                              ),
                              child: TextField(
                                maxLines: null,
                                controller: subtitleAnnouncement,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: "សូមឆ្លើយសំណួរខាងក្រោម".tr(),
                                  hintStyle: ThemsConstands.headline_4_medium_18
                                      .copyWith(
                                    color: Colorconstand.neutralDarkGrey,
                                  ),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 18,
                        ),
                      //   Align(
                      //     alignment: Alignment.topLeft,
                      //     child: BlocBuilder<AttachCubit, AttachSelecteStatus>(
                      //       builder: (context, state) {
                      //         if (state == AttachSelecteStatus.loading) {
                      //           return const Center(
                      //             child: CircularProgressIndicator(),
                      //           );
                      //         } else if (state == AttachSelecteStatus.success) {
                      //           var data = BlocProvider.of<AttachCubit>(context).filesMedia;
                      //           return 
                      //           Column(
                      //             children: [
                      //               SizedBox(
                      //                 width: MediaQuery.of(context).size.width,
                      //                 child: Row(
                      //                   children: [
                      //                   SizedBox(
                      //                     width: MediaQuery.of(context).size.width* .61,
                      //                     height: 80,
                      //                     child: ListView.builder(
                      //                       shrinkWrap: true,
                      //                       itemCount:data.length,
                      //                       scrollDirection: Axis.horizontal,
                      //                       physics: const BouncingScrollPhysics(),
                      //                       itemBuilder: (context,fileIndex){
                      //                         return Stack(
                      //                           children: [
                      //                             InkWell(
                      //                               onTap: (){
                      //                                 // Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: fileIndex,fileImage: listFileLesson[fileIndex],)))
                      //                                 // .then((removed){
                      //                                 //    if(removed == true){
                      //                                 //       setState(() {
                      //                                 //         listFileLesson.removeAt(fileIndex);  
                      //                                 //       }); 
                      //                                 //       print("removed");
                      //                                 //     }else{
                      //                                 //       print("not removed");
                      //                                 //     }
                      //                                 // });
                      //                               },
                      //                               child: Container(
                      //                                 width: MediaQuery.of(context).size.width *.16,
                      //                                 height: MediaQuery.of(context).size.width *.16,
                      //                                 margin: const EdgeInsets.only(right:8,bottom: 8),
                      //                                 decoration: BoxDecoration(
                      //                                   borderRadius: BorderRadius.circular(12),
                      //                                   // image: DecorationImage(
                      //                                   //   image: FileImage(File(listFileLesson[fileIndex].path)),
                      //                                   //   fit: BoxFit.cover,
                      //                                   // ),
                      //                                 ),
                      //                               ),
                      //                             ),
                      //                           ],
                      //                         );
                      //                       },
                      //                     ),
                      //                   ),
                      //                     IconButton(onPressed: (){
                      //                       // getImageFromCamera(title:titleController.text,activityType: "1" );
                      //                     }, icon: SvgPicture.asset(ImageAssets.camera,color: Colorconstand.primaryColor,)),
                      //                     IconButton(onPressed: () {
                      //                       // getMediaFromDevice(title:titleController.text,activityType: "1" );                  
                      //                     }, icon: SvgPicture.asset(ImageAssets.gallery,color: Colorconstand.primaryColor,)),
                      //                   ],
                      //                 ),
                      //               ),            
                      //             ],
                      //           );                         
                      //         } else if (state == AttachSelecteStatus.failure) {
                      //           return const Text("Error");
                      //         }
                      //         return Container();
                      //       },
                      //     ),
                      //   ) 
                      //  , 
                      BlocProvider.of<AttachCubit>(context).filesMedia.isNotEmpty?    
                        Column(
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width* .56,
                                    height: 80,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: 0 + BlocProvider.of<AttachCubit>(context).filesMedia.length,
                                      scrollDirection: Axis.horizontal,
                                      physics: const BouncingScrollPhysics(),
                                      itemBuilder: (context,fileIndex){
                                        if(fileIndex < listMultiMedia.length){
                                            return InkWell(
                                              onTap: (){
                                                // Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: fileIndex,fileImage: listFileLesson[fileIndex],)))
                                                // .then((removed){
                                                //    if(removed == true){
                                                //       setState(() {
                                                //         listFileLesson.removeAt(fileIndex);  
                                                //       }); 
                                                //       print("removed");
                                                //     }else{
                                                //       print("not removed");
                                                //     }
                                                // });
                                              },
                                              child: Container(
                                                width: MediaQuery.of(context).size.width *.16,
                                                height: MediaQuery.of(context).size.width *.16,
                                                margin: const EdgeInsets.only(right:8,bottom: 8),
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(12),
                                                  image: DecorationImage(
                                                    image: NetworkImage(widget.attachment![fileIndex]),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                            );
                                    
                                        }else{

                                        }
                                        return InkWell(
                                          onTap: (){
                                            // Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: fileIndex,fileImage: listFileLesson[fileIndex],)))
                                            // .then((removed){
                                            //    if(removed == true){
                                            //       setState(() {
                                            //         listFileLesson.removeAt(fileIndex);  
                                            //       }); 
                                            //       print("removed");
                                            //     }else{
                                            //       print("not removed");
                                            //     }
                                            // });
                                          },
                                          child: Container(
                                            width: MediaQuery.of(context).size.width *.16,
                                            height: MediaQuery.of(context).size.width *.16,
                                            margin: const EdgeInsets.only(right:8,bottom: 8),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(12),
                                              image: DecorationImage(
                                                image: FileImage(File(BlocProvider.of<AttachCubit>(context).filesMedia[fileIndex].path)),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        );
                                    
                                      },
                                    ),
                                  ),
                                  IconButton(onPressed: (){
                                      //getImageFromCamera(title:titleController.text,activityType: "1" );
                                  }, icon: SvgPicture.asset(ImageAssets.camera,color: Colorconstand.primaryColor,)),
                                  IconButton(onPressed: () {
                                    // getMediaFromDevice(title:titleController.text,activityType: "1" );

                                  }, icon: SvgPicture.asset(ImageAssets.gallery,color: Colorconstand.primaryColor,)),
                                  IconButton(onPressed: () {
                                    // getMediaFromDevice(title:titleController.text,activityType: "1" );
                                    
                                  }, icon: SvgPicture.asset(ImageAssets.document,color: Colorconstand.primaryColor,)),
                                ],
                              ),
                            ),
                          ],
                        ): Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            customSmallBtn(
                              onTap: (){

                              },
                              title: "ថតរូបភ្ជាប់", iconPath: ImageAssets.camera,
                            ),
                            customSmallBtn(
                              onTap: (){},
                              title: "ភ្ជាប់រូបភាព", iconPath: ImageAssets.gallery,
                            ),
                            customSmallBtn(
                              onTap: (){},
                              title: "ថតភ្ជាប់ឯកសា", iconPath: ImageAssets.document,
                            ),
                          ],
                        ),
                      
                        const SizedBox(height: 24,),
                        Text("HOMEWORK_DEADLINE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colors.black),),
                         const SizedBox(height: 24,),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: DateTimeWidget(
                            date: "22/07/2023",
                            onPressed: pickTimeDate,
                          ),
                        ),
                      ],
                    ),
                  ),
                  buttomKeyboardOpenWidget(!isKeyboard),
                  Positioned(
                    bottom: 100,
                    left: 0,
                    right: 0,
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 100),
                      padding: const EdgeInsets.only(left: 30),
                      height:isPoptitle ==true? 60:0,
                      margin: const EdgeInsets.symmetric(horizontal: 18),
                      decoration: BoxDecoration(
                        color:const Color(0xFF494B5E),
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImageAssets.ep_icon),
                          const SizedBox(width: 8,),
                          Text("TITLEREQ".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: Colors.white),)
                        ],
                      ),
                    ),
                  ),
               
                ],
              ),
            ),
          ],
        ),
      ),

      bottomSheet: Container(
        padding:  const EdgeInsets.symmetric(vertical: 18,horizontal: 18),
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8))
        ),
        child: MaterialButton(
          color: Colorconstand.primaryColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12),
            ),
          ),
          padding: const EdgeInsets.symmetric( vertical: 18, horizontal: 28),
          onPressed: (){
            PostActvityApi().postActivityApi(
                title: titleAnnouncement.text,
                activityType: '2',
                date: widget.date,
                description: subtitleAnnouncement.text,
                scheduleId: widget.scheduleId,
                expiredDate: "23/07/2023",
                attachments: []
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("SAVE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite)),
            ],
          ),
        ),
      )
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceAround,
        //   children: [
        //     // Pick up Image and Video
        //     Expanded(
        //       child: TypeAnnouncementCreate(
        //         svgAsset: ImageAssets.GALLERY_ICON,
        //         title: "IMAGEANDVDO".tr(),
        //         onPressed: () {
        //           getMediaFromDevice();
        //         },
        //       ),
        //     ),
        //     const SizedBox(
        //       width: 16.0,
        //     ),
        //     /// Pick up Document
        //     Expanded(
        //       child: TypeAnnouncementCreate(
        //         title: 'DOC'.tr(),
        //         svgAsset: ImageAssets.DOCUMENT_ICON,
        //         onPressed: () async {
        //           await getFileFromDevice();
        //         },
        //       ),
        //     ),
        //   ],
        // ),
     
      
    );
  }

  Widget customSmallBtn({required Function() onTap,required String iconPath,required String title}) {
    return InkWell(
      onTap:onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            SvgPicture.asset(iconPath,color: Colorconstand.primaryColor,),
            const SizedBox(width: 8,),
            Text(title,style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor,fontSize: 18),)
          ],
        ),
      ),
    );
  }

  Widget _buildMediaItem(BuildContext context, int index) {
    final mediaPath = BlocProvider.of<AttachCubit>(context).filesMedia[index];
    final extension = mediaPath.path.toLowerCase();
    if (extension.contains('.jpg') ||
        extension.contains('.png') ||
        extension.contains('.jpeg')) {
      return InkWell(
        onTap: () {
          Navigator.push( context, MaterialPageRoute( builder: (context) => PreviewHomeWorkAttachScreen(imageIndex: index)));
        },
        child: Stack(
          children:[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                image: DecorationImage(
                  image: FileImage(File(mediaPath.path)),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              right: 2,
              top: 2,
              child: InkWell(
                onTap: (){
                  BlocProvider.of<AttachCubit>(context).removeFile(index: index);
                  print("${   BlocProvider.of<AttachCubit>(context).filesMedia.length}");
                },
                child: const CircleAvatar(
                  radius: 12,
                  child: Icon(Icons.close,size: 18)
                ),
              ),
            ),
          ],
        ),
     
      );
    } else if (extension.contains('.mov') || extension.contains('.mp4')) {
      return InkWell(
        onTap: () {
          Navigator.push(context,MaterialPageRoute( builder: (context) => PreviewHomeWorkAttachScreen(imageIndex: index)));
        },
        child: Container(
          padding: const EdgeInsets.all(38),
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(2),
          ),
          child: SvgPicture.asset(
            ImageAssets.play_icon,
          ),
        ),
      );
    } else if (extension.contains('.pdf') || extension.contains('.mp3')) {
      return InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PreviewHomeWorkAttachScreen(imageIndex: index)));
        },
        child: Container(
          padding: const EdgeInsets.all(42),
          decoration: BoxDecoration(
            color: const Color(0xFFE5EBF4),
            borderRadius: BorderRadius.circular(2),
          ),
          child: SvgPicture.asset(
            ImageAssets.filldoc_icon,
          ),
        ),
      );
    } else {
      return Container(
        color: Colors.green,
      );
    }
  }

  Widget buttomKeyboardOpenWidget(bool isKeyboard) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 000),
      bottom:0,
      left: 0,
      right: 0,
      child: !isKeyboard ? Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Divider(
            color: Colorconstand.neutralGrey,
          ),
          // Center(
          //   child: Container(
          //     padding: const EdgeInsets.all(1),
          //     decoration: BoxDecoration(
          //       border: Border.all(color: Colorconstand.neutralGrey)
          //     ),
          //     child: Row(
          //       children: [
          //         Icon(Icons.keyboard),
          //         Text("data"),
          //       ],
          //     ),
          //   ),
          // )
          Container(
            color: Colorconstand.screenWhite,
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 8.0, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      getMediaFromDevice();
                      setState(() {
                        isKeyboard = true;
                      });
                    },
                    child: SvgPicture.asset(
                      ImageAssets.keyboard_icon,
                      height: 28,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                    child: GestureDetector(
                      onTap: () async {
                        await getFileFromDevice();
                      },
                      child: Text("OFFKEYBOARD".tr())
                    ),
                  ),
                
                ],
              ),
            ),
          ) 

        ],
      ):Container(),
    );
  }

  Widget bottomPicAndDocWidget() {
    return Positioned(
        bottom: 0,
        right: 0,
        left: 0,
        child: Container(
          height: 120,
          padding: const EdgeInsets.only(
            left: 19.0,
            right: 19.0,
          ),
          decoration: const BoxDecoration(
            color: Colorconstand.neutralBtnBg,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0)),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: TypeAnnouncementCreate(
                  svgAsset: ImageAssets.GALLERY_ICON,
                  title: "IMAGE".tr(),
                  onPressed: () {
                    getMediaFromDevice();
                  },
                ),
              ),
              const SizedBox(
                width: 16.0,
              ),
              Expanded(
                child: TypeAnnouncementCreate(
                  title: 'DOCUMENT'.tr(),
                  svgAsset: ImageAssets.DOCUMENT_ICON,
                  onPressed: () {},
                ),
              )
            ],
          ),
        ));
  }
    void pickTimeDate() async {
    DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime(2100),
    );
    if (newDate == null) return;
    setState(() {
      // date = newDate;
      // String expired = DateFormat('dd/MM/yyyy').format(date);
      // postActivitiesService(title: '',des: '',file: []  ,expiredDate: expired, scheduleID: widget.data.scheduleId.toString(),type: '2');
    });
  }

 Future<void> getMediaFromDevice() async {
    // final listMedia = await ImagesPicker.pick(
    //   count: 10,
    //   gif: true,
    //   pickType: PickType.all,
    //   language: Language.System,
    //   maxTime: 3600,
    //   quality: 0.8,
    //   cropOpt: CropOption(aspectRatio: CropAspectRatio.wh16x9),
    // );
    List<XFile>? listMedia = await _picker.pickMultiImage();
    if (listMedia!.isNotEmpty) {
      for (var item in listMedia) {
        setState(() {
          listMultiMedia.add(File(item.path));
          BlocProvider.of<AttachCubit>(context).pickImgVdoFileCubit(listMedia: listMultiMedia);
        });
      }
    }
  }

  Future<void> getFileFromDevice() async {
    final result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'cvs']);
    if (result != null) {
      List<File> files = result.paths.map((e) => File(e.toString())).toList();
      for (var item in files) {
        setState(() {
          listMultiMedia.add(File(item.path));
          BlocProvider.of<AttachCubit>(context).pickImgVdoFileCubit(listMedia: listMultiMedia);
        });
      }
    }
  }

}