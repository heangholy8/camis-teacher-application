import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/models/check_attendance_model/check_attendance_model.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/bloc/post_attendance_bloc.dart';
import 'package:camis_teacher_application/app/service/api/tracking_teacher_api/tracking_teacher_api.dart';
import 'package:camis_teacher_application/widget/custom_appbar_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:no_screenshot/no_screenshot.dart';
import 'package:no_screenshot/screenshot_snapshot.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../attendance_schedule_screen/state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../../home_screen/bloc/daskboard_screen_bloc.dart';
import '../../study_affaire_screen/affaire_home_screen/state/daily_attendace/bloc/daily_attendance_affaire_bloc.dart';
import '../bloc/check_attendance_bloc.dart';

class AttendanceEntryScreen extends StatefulWidget {
  final bool activeMySchedule;
  final bool isPrimary;
  //=========== routFromScreen = 1? HomeScreen : routFromScreen = 2? ScheduleScreen
  final int routFromScreen;
  const AttendanceEntryScreen({Key? key,this.isPrimary = false, this.routFromScreen = 2,required this.activeMySchedule}) : super(key: key);

  @override
  State<AttendanceEntryScreen> createState() => _AttendanceEntryScreenState();
}

class _AttendanceEntryScreenState extends State<AttendanceEntryScreen> with Toast {
  ScrollController scrollController = ScrollController();
  int isAcitve = 0;

  var classId;
  var date;

  bool connection = true;
  bool _isLoading = false;
  bool _isLast = false;
  bool _isNotFound = false;
  bool _isFirstLoading = true;
  StreamSubscription? sub;
  final TextEditingController searchController = TextEditingController();
  List<ListAttendance>? _filterStudentAbsent;
  List<ListAttendance>? _filterStudentPermission;
  List<ListAttendance>? _filterStudentLate;
  List<ListAttendance>? _filterStudentPressen;

  final _noScreenshot = NoScreenshot.instance;

  void enableScreenshot() async {
    bool result = await _noScreenshot.screenshotOn();
    debugPrint('Enable Screenshot: $result');
  }

  void disableScreenshot() async {
    bool result = await _noScreenshot.screenshotOff();
    debugPrint('Screenshot Off: $result');
  }

  @override
  void initState() {
    // disableScreenshot();
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {} 
        else {}
      });
    });
    // addSecureScreen();
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
    // enableScreenshot();
  }

  int absent = 0, present = 0, late = 0;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var translate = context.locale.toString();
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: BlocListener<PostAttendanceBloc, PostAttendanceState>(
          listener: (context, state) {
            if (state is PostAttendanceLoading) {
              setState(() {
                _isLoading = true;
              });
            }
            else if (state is PostAttendanceSuccess) {
              setState(() {
                _isLoading = false;
                showSuccessDialog(
                  title: "SUCCESSFULLY".tr(),
                  discription: "SUCCESS_SAVE_ATTENDANCE".tr(),
                  context: context,
                  callback: () {
                    setState(() {
                    if(widget.routFromScreen == 2){
                      if(widget.activeMySchedule==true){
                        BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: date));
                      }
                      else{
                        BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date:  date,classid: classId));
                      }
                    }
                    if(widget.routFromScreen == 1){
                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: date));
                    }
                    if(widget.routFromScreen == 6 ){
                      BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: date));
                    }
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                    });
                  },
                );
              });
            }
            else{
              setState(() {
                _isLoading = false;
              });
              showErrorDialog(() {
                Navigator.of(context).pop();
              }, context,title:"FAIL".tr(),description: "FAIL_SAVE_ATTENDANCE".tr());
            }
          },
          child: Container(
            margin:const EdgeInsets.only(top: 0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: [
                  CustomAppBarWidget(
                    width: width,
                    title: "CHECKATTENDANCE".tr(),
                  ),
                  Expanded(
                    child: BlocConsumer<CheckAttendanceBloc, CheckAttendanceState>(
                      listener: (context, state) {
                        if(state is GetCheckStudentAttendanceListLoaded){
                          var datacheckStu = state.checkAttendanceList!.data;
                          setState(() {
                            date = datacheckStu!.date.toString();
                            classId = datacheckStu.classId.toString();
                          });
                          // _filterStudentPermission = BlocProvider.of<CheckAttendanceBloc>(context).studentList!;
                          // _filterStudentPressen = BlocProvider.of<CheckAttendanceBloc>(context).studentList!;
                          // _filterStudentAbsent = BlocProvider.of<CheckAttendanceBloc>(context).studentList!;
                          // _filterStudentLate = BlocProvider.of<CheckAttendanceBloc>(context).studentList!;
                        }
                      },
                      builder: (context, state) {
                        if (state is GetCheckStudentAttendanceListLoading) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        } else if (state is GetCheckStudentAttendanceListLoaded) {
                          final dataCheckAttendance = state.checkAttendanceList!.data;
                          if(dataCheckAttendance!.listAttendance!.isNotEmpty){
                            if(_isFirstLoading==true){
                              _filterStudentPermission = dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 4).toList();
                              _filterStudentPressen = dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 1).toList();
                              _filterStudentAbsent = dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 2).toList();
                              _filterStudentLate = dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 3).toList();
                            }
                            _isFirstLoading = false;
                          }
                          return dataCheckAttendance.listAttendance!.isEmpty? 
                          Container(
                            alignment: Alignment.center,
                            child: Column(
                              children: [
                                Expanded(child: Container()),
                                ScheduleEmptyWidget(
                                    title: "NO_STUDENT".tr(),
                                    subTitle: "EMPTHY_CONTENT_DES".tr(),
                                  ),
                                Expanded(child: Container()),
                              ],
                            )
                          )
                          :Stack(
                            children: [
                               Container(
                                margin:const EdgeInsets.only(bottom: 20),
                                 child: Column(
                                  children: [
                                    widget. isPrimary == true? Container():Container(
                                      child:Text(
                                      translate=="km"?  dataCheckAttendance.subjectName!:dataCheckAttendance.subjectNameEn!,
                                        style:ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.mainColorForecolor),
                                      ),
                                    ),
                                    // _buildTextFormField(
                                    //     controller: searchController,
                                    //     attendanceModel: state.checkAttendanceList!),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 14),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            children: [
                                              SvgPicture.asset(ImageAssets.location_icon),
                                              const SizedBox(
                                                width: 4,
                                              ),
                                              Text(translate == "km"? dataCheckAttendance.className!:dataCheckAttendance.className!.replaceAll("ថ្នាក់ទី", "Class"),
                                                style:ThemsConstands.headline_5_semibold_16,
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(ImageAssets.calendar_icon),
                                              const SizedBox(width:4),
                                              Row(
                                                children: [
                                                  Text(dataCheckAttendance.date.toString(),
                                                    style: ThemsConstands.headline_5_semibold_16,
                                                  ),
                                                  const CircleAvatar(
                                                    radius: 2,
                                                    backgroundColor: Colors.black,
                                                  ),
                                                  Text(
                                                    " ${dataCheckAttendance.startTime!}-${dataCheckAttendance.endTime!}",
                                                    style: ThemsConstands.headline_5_semibold_16,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: NotificationListener(
                                        child: SingleChildScrollView(
                                          controller: scrollController,
                                          child: !_isNotFound
                                              ? Column(
                                                  children: [
                                                    //---------------- ABSENT -----------------------
                                                    _filterStudentAbsent!.isNotEmpty
                                                        ? Container(
                                                            width: width,
                                                            padding: const EdgeInsets.symmetric(
                                                                vertical: 8,
                                                                horizontal: 18),
                                                            decoration:const BoxDecoration(
                                                              color: Colorconstand.neutralSecondBackground,
                                                              border: Border(top: BorderSide(color: Colorconstand.darkTextsDisabled),),
                                                            ),
                                                            child: Text(
                                                              "${"ABSENT".tr()} (${_filterStudentAbsent!.length})",
                                                              style: ThemsConstands.headline_6_semibold_14
                                                                  .copyWith(color: Colorconstand.alertsDecline),
                                                            ),
                                                          )
                                                        : Container(),
                                                    ListView.separated(
                                                       padding:const EdgeInsets.all(0),
                                                      physics: const ScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount: _filterStudentAbsent!.length,
                                                      itemBuilder: (context, indexabsent) {
                                                        return MaterialButton(
                                                          padding:const EdgeInsets.all(0),
                                                          onPressed: () {
                                                            setState(() {
                                                              if(_filterStudentAbsent![indexabsent].attendanceStatus == 1){
                                                              _filterStudentAbsent![indexabsent].attendanceStatus = 2;
                                                              }
                                                              else if(_filterStudentAbsent![indexabsent].attendanceStatus == 2){
                                                                _filterStudentAbsent![indexabsent].attendanceStatus = 3;
                                                              }
                                                              else if(_filterStudentAbsent![indexabsent].attendanceStatus == 3){
                                                                _filterStudentAbsent![indexabsent].attendanceStatus = 4;
                                                              }
                                                              else{
                                                                _filterStudentAbsent![indexabsent].attendanceStatus = 1;
                                                              }
                                                            });
                                                          },
                                                          child: Container(
                                                            padding:const EdgeInsets.symmetric(
                                                                    horizontal: 18,
                                                                    vertical: 8),
                                                            width: width,
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                Expanded(
                                                                  child: Row(
                                                                    children: [
                                                                      CircleAvatar(
                                                                          radius: 30,
                                                                          backgroundImage: NetworkImage(_filterStudentAbsent![indexabsent].profileMedia!.fileShow.toString())),
                                                                      const SizedBox(
                                                                        width: 10,
                                                                      ),
                                                                      Expanded(
                                                                        //width: width / 1.8,
                                                                        child: Column(
                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                          children: [
                                                                            Text(
                                                                            translate=="km"? _filterStudentAbsent![indexabsent].name.toString():
                                                                             _filterStudentAbsent![indexabsent].nameEn==" "?"No Name":
                                                                             _filterStudentAbsent![indexabsent].nameEn.toString(),
                                                                              maxLines: null,
                                                                              style: ThemsConstands.headline_4_medium_18,
                                                                            ),
                                                                            Text(
                                                                              _filterStudentAbsent![indexabsent].gender.toString()=="1"?"${"GENDER".tr()}: ${"MALE".tr()}":"${"GENDER".tr()}: ${"FEMALE".tr()}",
                                                                                style: ThemsConstands
                                                                                    .caption_regular_12,
                                                                              ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  width: 60,
                                                                  height: 60,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: _filterStudentAbsent![indexabsent].attendanceStatus == 2?Colorconstand.alertsDecline:_filterStudentAbsent![indexabsent].attendanceStatus == 1?Colorconstand.alertsPositive:_filterStudentAbsent![indexabsent].attendanceStatus == 3?Colorconstand.alertsAwaitingText:Colorconstand.mainColorForecolor,
                                                                    borderRadius: BorderRadius.circular(18),
                                                                  ),
                                                                  child: Icon( _filterStudentAbsent![indexabsent].attendanceStatus == 3? Icons.access_time:_filterStudentAbsent![indexabsent].attendanceStatus == 1?Icons.check_rounded:Icons.close,
                                                                    size: 40,
                                                                    color: Colors.white,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                      separatorBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return const Padding(
                                                          padding:
                                                              EdgeInsets.only(left: 100),
                                                          child: Divider(
                                                            thickness: 0.5,
                                                            height: 1,
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                    //------------------------------------------------
                                                 
                                                    //---------------- Late -----------------------
                                                    _filterStudentLate!.isNotEmpty
                                                        ? Container(
                                                            width: width,
                                                            padding: const EdgeInsets.symmetric(
                                                                vertical: 8,horizontal: 18),
                                                            decoration:const BoxDecoration(
                                                              color: Colorconstand.neutralSecondBackground,
                                                              border: Border(
                                                                top: BorderSide(
                                                                  color: Colorconstand.darkTextsDisabled,
                                                                ),
                                                              ),
                                                            ),
                                                            child: Text(
                                                              "${"LATE1".tr()} (${_filterStudentLate!.length})",
                                                              style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.alertsAwaitingText),
                                                            ),
                                                          )
                                                        : Container(),
                                                          _filterStudentLate!.isEmpty
                                                        ? Container()
                                                        : ListView.separated(
                                                          padding:const EdgeInsets.all(0),
                                                            physics:const ScrollPhysics(),
                                                            shrinkWrap: true,
                                                            itemCount: _filterStudentLate!.length,
                                                            itemBuilder:
                                                                (context, indexabsent) {
                                                              return MaterialButton(
                                                                  padding:const EdgeInsets.all(0),
                                                                  onPressed: () {
                                                                  setState(() {
                                                                      if(_filterStudentLate![indexabsent].attendanceStatus == 1){
                                                                      _filterStudentLate![indexabsent].attendanceStatus = 2;
                                                                      }
                                                                      else if(_filterStudentLate![indexabsent].attendanceStatus == 2){
                                                                        _filterStudentLate![indexabsent].attendanceStatus = 3;
                                                                      }
                                                                      else if(_filterStudentLate![indexabsent].attendanceStatus == 3){
                                                                        _filterStudentLate![indexabsent].attendanceStatus = 4;
                                                                      }
                                                                      else{
                                                                        _filterStudentLate![indexabsent].attendanceStatus = 1;
                                                                      }
                                                                  });
                                                                },
                                                                child: Container(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal: 18,
                                                                      vertical: 8),
                                                                  width: width,
                                                                  child: Row(
                                                                    mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                    children: [
                                                                      Expanded(
                                                                        child: Row(
                                                                          children: [
                                                                            CircleAvatar(
                                                                              radius: 30,
                                                                              backgroundImage:
                                                                                  NetworkImage(
                                                                                _filterStudentLate![indexabsent].profileMedia!.fileShow.toString(),
                                                                              ),
                                                                            ),
                                                                            const SizedBox(
                                                                              width: 10,
                                                                            ),
                                                                            Expanded(
                                                                              
                                                                              child: Column(
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: [
                                                                                  Text(
                                                                                    translate=="km"?  _filterStudentLate![indexabsent].name.toString():
                                                                                    _filterStudentLate![indexabsent].nameEn==" "?
                                                                                      "No Name" :_filterStudentLate![indexabsent].name.toString(),
                                                                                      maxLines:
                                                                                          null,
                                                                                      style: ThemsConstands.headline_4_medium_18,
                                                                                  ),
                                                                                  Text(
                                                                                    _filterStudentLate![indexabsent].gender.toString()=="1"?"${"GENDER".tr()}: ${"MALE".tr()}":"${"GENDER".tr()}: ${"FEMALE".tr()}",
                                                                                      style: ThemsConstands
                                                                                          .caption_regular_12,
                                                                                    ),
                                                                                  
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        width: 60,
                                                                        height: 60,
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          color:  _filterStudentLate![indexabsent].attendanceStatus == 2?Colorconstand.alertsDecline:_filterStudentLate![indexabsent].attendanceStatus == 1?Colorconstand.alertsPositive:_filterStudentLate![indexabsent].attendanceStatus == 3?Colorconstand.alertsAwaitingText:Colorconstand.mainColorForecolor,
                                                                          borderRadius:
                                                                              BorderRadius
                                                                                  .circular(
                                                                                      18),
                                                                        ),
                                                                        child:Icon(_filterStudentLate![indexabsent].attendanceStatus == 3? Icons.access_time:_filterStudentLate![indexabsent].attendanceStatus == 1?Icons.check_rounded:Icons.close,
                                                                            size: 40,
                                                                            color: Colors
                                                                                .white),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                            separatorBuilder:
                                                                (BuildContext context,
                                                                    int index) {
                                                              return const Padding(
                                                                padding: EdgeInsets.only(
                                                                    left: 100),
                                                                child: Divider(
                                                                  thickness: 0.5,
                                                                  height: 1,
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                    //------------------------------------------------
                                                    //================== PERMESTION REQUEST ============
                                                    _filterStudentPermission!.isNotEmpty
                                                        ? Container(
                                                            width: width,
                                                            padding: const EdgeInsets
                                                                    .symmetric(
                                                                vertical: 8,
                                                                horizontal: 18),
                                                            decoration:
                                                                const BoxDecoration(
                                                              color: Colorconstand
                                                                  .neutralSecondBackground,
                                                              border: Border(
                                                                top: BorderSide(
                                                                  color: Colorconstand
                                                                      .darkTextsDisabled,
                                                                ),
                                                              ),
                                                            ),
                                                            child: Text(
                                                              "${"PERMISSION".tr()} (${_filterStudentPermission!.length})",
                                                              style: ThemsConstands.headline_6_semibold_14
                                                                  .copyWith(color: Colorconstand.mainColorForecolor),
                                                            ),
                                                          )
                                                        : Container(),
                                                    _filterStudentPermission!.isEmpty
                                                        ? Container()
                                                        : ListView.separated(
                                                          padding:const EdgeInsets.all(0),
                                                            physics:const ScrollPhysics(),
                                                            shrinkWrap: true,
                                                            itemCount: _filterStudentPermission!.length,
                                                            itemBuilder:(context, indexabsent) {
                                                              return MaterialButton(
                                                                padding:const EdgeInsets.all(0),
                                                                  onPressed:_filterStudentPermission![indexabsent].editAble==1? () {
                                                                      setState(() {
                                                                      if(_filterStudentPermission![indexabsent].attendanceStatus == 1){
                                                                        _filterStudentPermission![indexabsent].attendanceStatus = 2;
                                                                      }
                                                                      else if(_filterStudentPermission![indexabsent].attendanceStatus == 2){
                                                                        _filterStudentPermission![indexabsent].attendanceStatus = 3;
                                                                      }
                                                                      else if(_filterStudentPermission![indexabsent].attendanceStatus == 3){
                                                                        _filterStudentPermission![indexabsent].attendanceStatus = 4;
                                                                      }
                                                                      else{
                                                                        _filterStudentPermission![indexabsent].attendanceStatus = 1;
                                                                      }
                                                                    });
                                                                  }:null,
                                                                child: Container(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal: 18,
                                                                      vertical: 8),
                                                                  width: width,
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    children: [
                                                                      Expanded(
                                                                        child: Row(
                                                                          children: [
                                                                            Container(
                                                                              height: 60,
                                                                              width: 60,
                                                                              child: Stack(
                                                                                children: [
                                                                                  CircleAvatar(
                                                                                    radius: 30,
                                                                                    backgroundImage:
                                                                                        NetworkImage(
                                                                                      _filterStudentPermission![indexabsent].profileMedia!.fileShow.toString(),
                                                                                    ),
                                                                                  ),
                                                                                  Positioned(
                                                                                    bottom: 0,
                                                                                    right: 0,
                                                                                    child: _filterStudentPermission![indexabsent].editAble == 1? Container():Container(
                                                                                      padding: const EdgeInsets.all(4),
                                                                                      decoration: const BoxDecoration(
                                                                                        shape: BoxShape.circle,
                                                                                        color: Colorconstand.mainColorForecolor
                                                                                      ),
                                                                                      child:const Icon(Icons.email_outlined,size: 22,color: Colorconstand.neutralWhite,),
                                                                                    ),
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ),
                                                                            const SizedBox(
                                                                              width: 10,
                                                                            ),
                                                                            Expanded(
                                                                              child: Column(
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: [
                                                                                  Text(
                                                                                  translate=="km"?  _filterStudentPermission![indexabsent].name.toString():
                                                                                  _filterStudentPermission![indexabsent].nameEn==" "?
                                                                             "No Name" :_filterStudentPermission![indexabsent].name.toString(),
                                                                                    maxLines:
                                                                                        null,
                                                                                    style: ThemsConstands.headline_4_medium_18,
                                                                                        
                                                                                  ),
                                                                                  Text(
                                                                                    _filterStudentPermission![indexabsent].gender.toString()=="1"?"${"GENDER".tr()}: ${"MALE".tr()}":"${"GENDER".tr()}: ${"FEMALE".tr()}",
                                                                                      style: ThemsConstands
                                                                                          .caption_regular_12,
                                                                                    ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        width: 60,
                                                                        height: 60,
                                                                        decoration:BoxDecoration(color: _filterStudentPermission![indexabsent].attendanceStatus == 2?Colorconstand.alertsDecline:_filterStudentPermission![indexabsent].attendanceStatus == 1?Colorconstand.alertsPositive:_filterStudentPermission![indexabsent].attendanceStatus == 3?Colorconstand.alertsAwaitingText:Colorconstand.mainColorForecolor,
                                                                          borderRadius:BorderRadius.circular(18),
                                                                        ),
                                                                        child: Icon(
                                                                          _filterStudentPermission![indexabsent].attendanceStatus == 3? Icons.access_time:_filterStudentPermission![indexabsent].attendanceStatus == 1?Icons.check_rounded:Icons.close,size: 40,color: Colors.white),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                            separatorBuilder:
                                                                (BuildContext context,int index) {
                                                              return const Padding(
                                                                padding: EdgeInsets.only(left: 100),
                                                                child: Divider(
                                                                  thickness: 0.5,
                                                                  height: 1,
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                    //------------------- End Permistion-----------------------------
                                                    _filterStudentPressen!
                                                            .isNotEmpty
                                                        ? Column(
                                                            children: [
                                                              Container(
                                                                width: width,
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    vertical: 8,
                                                                    horizontal: 18),
                                                                decoration:
                                                                    const BoxDecoration(
                                                                  color: Colorconstand
                                                                      .neutralSecondBackground,
                                                                ),
                                                                child: Text(
                                                                  "${"PRESENT".tr()} (${_filterStudentPressen!.length})",
                                                                  style: ThemsConstands.headline_6_semibold_14.copyWith(
                                                                          color: Colorconstand
                                                                              .alertsPositive),
                                                                ),
                                                              ),
                                                              ListView.separated(
                                                                padding:const EdgeInsets.only(bottom: 130,left: 0,right: 0,top: 0),
                                                                physics:const ScrollPhysics(),
                                                                shrinkWrap: true,
                                                                itemCount: _filterStudentPressen!.length,
                                                                itemBuilder:(context, index) {
                                                                  return MaterialButton(
                                                                        padding:const EdgeInsets.all(0),
                                                                        onPressed: () {
                                                                        setState(() {
                                                                          if(_filterStudentPressen![index].attendanceStatus == 1){
                                                                            _filterStudentPressen![index].attendanceStatus = 2;
                                                                          }
                                                                          else if(_filterStudentPressen![index].attendanceStatus == 2){
                                                                            _filterStudentPressen![index].attendanceStatus = 3;
                                                                          }
                                                                          else if(_filterStudentPressen![index].attendanceStatus == 3){
                                                                            _filterStudentPressen![index].attendanceStatus = 4;
                                                                          }
                                                                          else{
                                                                            _filterStudentPressen![index].attendanceStatus = 1;
                                                                          }
                                                                        });                                                                
                                                                      },
                                                                    child: Container(
                                                                      padding:
                                                                          const EdgeInsets
                                                                                  .symmetric(
                                                                              horizontal:
                                                                                  18,
                                                                              vertical: 8),
                                                                      width: width,
                                                                      child: Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment
                                                                                .spaceBetween,
                                                                        children: [
                                                                          Expanded(
                                                                            child: Row(
                                                                              children: [
                                                                                CircleAvatar(radius:30,
                                                                                    backgroundImage: NetworkImage(_filterStudentPressen![index].profileMedia!.fileShow.toString())),
                                                                                const SizedBox(
                                                                                  width: 10,
                                                                                ),
                                                                                Expanded(
                                                                                  
                                                                                  child: Column(
                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                    children: [
                                                                                      Text(
                                                                                      translate=="km"?  _filterStudentPressen![index].name.toString():
                                                                                      _filterStudentPressen![index].nameEn==" "?"No Name":
                                                                                      _filterStudentPressen![index].nameEn.toString(),
                                                                                        maxLines:
                                                                                            null,
                                                                                        style: ThemsConstands.headline_4_medium_18,
                                                                                      ),
                                                                                      Text(
                                                                                    _filterStudentPressen![index].gender.toString()=="1"?"${"GENDER".tr()}: ${"MALE".tr()}":"${"GENDER".tr()}: ${"FEMALE".tr()}",
                                                                                      style: ThemsConstands
                                                                                          .caption_regular_12,
                                                                                    ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                              width: 60,
                                                                              height: 60,
                                                                              decoration:
                                                                            BoxDecoration(
                                                                          color:_filterStudentPressen![index].attendanceStatus == 2?Colorconstand.alertsDecline:_filterStudentPressen![index].attendanceStatus == 1?Colorconstand.alertsPositive:_filterStudentPressen![index].attendanceStatus == 3?Colorconstand.alertsAwaitingText:Colorconstand.mainColorForecolor,
                                                                          borderRadius:BorderRadius.circular(18),
                                                                              ),
                                                                              child: Icon( _filterStudentPressen![index].attendanceStatus == 3? Icons.access_time:_filterStudentPressen![index].attendanceStatus == 1?Icons.check_rounded:Icons.close,size: 40,
                                                                            color: Colors.white),
                                                                            ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  );
                                                                },
                                                                separatorBuilder:
                                                                    (BuildContext context,
                                                                        int index) {
                                                                  return const Padding(
                                                                    padding:
                                                                        EdgeInsets.only(
                                                                            left: 100),
                                                                    child: Divider(
                                                                      thickness: 0.5,
                                                                      height: 1,
                                                                    ),
                                                                  );
                                                                },
                                                              ),
                                                              _isLast
                                                                  ? SizedBox(
                                                                      width: width,
                                                                      height: 120,
                                                                    )
                                                                  : Container(),
                                                            ],
                                                          )
                                                        : Container(),
                                                  ],
                                                )
                                              : Container(
                                                  alignment: Alignment.center,
                                                  width: width,
                                                  height: height / 1.6,
                                                  child: Text(
                                                    "NODDATA".tr(),
                                                    style: ThemsConstands
                                                        .headline3_semibold_20,
                                                  ),
                                                ),
                                        ),
                                      ),
                                    ),
                                    // customeBottomBoxWidget(dataCheckAttendance),
                                  ],
                                                             ),
                               ),
                              AnimatedPositioned(
                                duration: const Duration(milliseconds: 15),
                                bottom:  0,
                                left: 0,
                                right: 0,
                                child: customeBottomBoxWidget(dataCheckAttendance),
                              ),
                              _isLoading
                                  ? Positioned(
                                      child: Container(
                                        color:
                                            Colorconstand.primaryColor.withOpacity(.6),
                                        width: width,
                                        height: height,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              "RECORDEDATT".tr(),
                                              style: ThemsConstands
                                                  .headline_2_semibold_24
                                                  .copyWith(color: Colors.white),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "DESRECORDEDATT".tr(),
                                              textAlign: TextAlign.center,
                                              style: ThemsConstands.headline_5_medium_16
                                                  .copyWith(color: Colors.white),
                                            ),
                                            const SizedBox(
                                              height: 20,
                                            ),
                                            const CircularProgressIndicator(
                                              color: Colors.white,
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : Container(),
                            ],
                          );
                        } else {
                          return Center(
                            child: EmptyWidget(
                          title: "WE_DETECT".tr(),
                          subtitle: "WE_DETECT_DES".tr(),
                        ),
                          ); 
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget customeBottomBoxWidget(Data dataCheckAttendance) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 15),
      padding: const EdgeInsets.only(top: 10),
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: Colorconstand.neutralWhite,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x919E9E9E),
            offset: Offset(0, 0),
            blurRadius: 3.0,
            spreadRadius: 1.0,
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 22),
                  child: Text(
                    "ATTENDANCE".tr(),
                    style: ThemsConstands.headline_5_semibold_16
                        .copyWith(color: Colorconstand.neutralDarkGrey,overflow: TextOverflow.ellipsis,),maxLines: 2,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 22),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    customBoxWidget(
                      status: "PRESENCES".tr(),
                      background: Colorconstand.alertsPositive,
                      title: dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 1).isEmpty && dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 3).isEmpty
                          ? "0"
                          : "${dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 1 || element.attendanceStatus == 3).length}",
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    customBoxWidget(
                      status: "ABSENT".tr(),
                      background: Colorconstand.alertsDecline,
                      title: dataCheckAttendance.listAttendance!
                              .where(
                                  (element) => element.attendanceStatus == 2)
                              .isEmpty
                          ? "0"
                          : "${dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 2).length}",
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    customBoxWidget(
                      status: "PERMISSION".tr(),
                      background: Colorconstand.mainColorForecolor,
                      title: dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 4).isEmpty
                          ? "0"
                          : "${dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 4).length}",
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    customBoxWidget(
                      status: "LATE1".tr(),
                      background: Colorconstand.alertsAwaitingText,
                      title: dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 3).isEmpty
                          ? "0"
                          : "${dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 3).length}",
                    ),
                  ],
                ),
              ),
            ],
          ),
          AnimatedContainer(
            duration: const Duration(milliseconds: 30),
            child: GestureDetector(
                onTap: () {
                  if(widget.isPrimary == false){
                    BlocProvider.of<PostAttendanceBloc>(context).add(PostAttDEvent(data: dataCheckAttendance),);
                  }
                  else{
                    BlocProvider.of<PostAttendanceBloc>(context).add(PostAttPrimaryEvent(data: dataCheckAttendance),);
                  }
                  PostActionTeacherApi().actionTeacherApi(feature: "SCHEDULE_SCREEN" ,keyFeature: "CHECK_ATTENDANCE_STUDENT");
                },
                child: Container(
                  height: 48,
                  margin: const EdgeInsets.all(16),
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(9),
                      ),
                      color: Colorconstand.primaryColor),
                  child: Center(
                    child: Text(
                      "SAVE".tr(),
                      style: ThemsConstands.headline_4_semibold_18.copyWith(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )),
          ),
        ],
      ),
    );
  }

  Widget customBoxWidget({String? title, Color? background,String? status}) {
    return Column(
      children: [
        Container(
          width: 25,
          padding: const EdgeInsets.symmetric(
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: background ?? Colorconstand.alertsDecline,
            borderRadius: BorderRadius.circular(4),
          ),
          child: Text(
            title.toString(),
            style: ThemsConstands.headline_6_semibold_14
                .copyWith(color: Colorconstand.neutralWhite),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 5,),
        Text(status!,style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: background),)
      ],
    );
  }

  Widget _buildTextFormField(
      {required TextEditingController controller,
      required CheckAttendanceModel attendanceModel}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: TextFormField(
        onChanged: ((value) {
          _filter(
            query: value,
            allResult: attendanceModel.data!.listAttendance,
          );
        }),
        controller: controller,
        maxLines: 1,
        style: ThemsConstands.subtitle1_regular_16,
        decoration: InputDecoration(
          prefixIcon: Padding(
            padding: const EdgeInsets.all(14.0),
            child: SvgPicture.asset(
              ImageAssets.search_icon,
              width: 18,
              height: 18,
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(color: Color(0xFFE5E5E5)),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(color: Color(0xFFE5E5E5)),
          ),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
          hintText: "SEACHSTUDENT".tr(),
          hintStyle: ThemsConstands.subtitle1_regular_16.copyWith(
            color: const Color(0xFFE4E4E4),
          ),
        ),
      ),
    );
  }

  void _filter({
    required String query,
    required List<ListAttendance>? allResult,
  }) {
    List<ListAttendance>? results = [];

    if (query.isEmpty || query == " ") {
      setState(() {
        results = allResult;
      });
    } else {
      results = allResult!
          .where((element) =>
              element.name!.toLowerCase().contains(query.toLowerCase()))
          .toList();
      if (results.isNotEmpty) {
        setState(() {
          _isNotFound = false;
        });
        debugPrint("NotFound: $_isNotFound");
        // for (var element in results) {
        //   debugPrint("Student list: ${element.name}");
        // }
      } else {
        setState(() {
          _isNotFound = true;
          debugPrint("NotFound: $_isNotFound");
        });
      }
    }
    setState(() {
     // _filterStudent = results;
      _filterStudentAbsent = results;
      _filterStudentLate = results;
      _filterStudentPermission = results;
      _filterStudentPressen = results;
      // for (var element in _filterStudent!) {
      //   debugPrint("Student listtttttttt: ${element.name} ${_filterStudent!.length}");
      // }
    });
  }
}
