import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';

enum ImageSelectStatus { initial, loading, success, failure }

class ImageSelectImageBloc extends Cubit<ImageSelectStatus> {
  ImageSelectImageBloc() : super(ImageSelectStatus.initial);

  List<File> listImage = [];

  Future<void> listImagePickerCubit({required List<File> filepicker}) async {
    try {
      emit(ImageSelectStatus.loading);
      listImage = filepicker;
      emit(ImageSelectStatus.success);
    } catch (e) {
      emit(ImageSelectStatus.failure);
    }
  }

  Future<void> addMoreImagePickerCubit(
      {required List<File> moreFilePicker}) async {
    try {
      emit(ImageSelectStatus.loading);
      listImage.addAll(moreFilePicker);
      emit(ImageSelectStatus.success);
    } catch (e) {
      emit(ImageSelectStatus.failure);
    }
  }

  void removeImageItem({required int index}) async {
    try {
      emit(ImageSelectStatus.loading);
      listImage.removeAt(index);
      emit(ImageSelectStatus.success);
    } catch (e) {
      emit(ImageSelectStatus.failure);
    }
  }
  void removeAllFile(){ 
     emit(ImageSelectStatus.loading);
    try {
        listImage = [];
       emit(ImageSelectStatus.success);
    } catch (e) {
      emit(ImageSelectStatus.failure);
    }
  }
}
