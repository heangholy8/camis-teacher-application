import 'dart:io';
import 'package:camis_teacher_application/app/modules/grading_screen/cubit/image_selected_cubit.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/view/result_screen.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/widget/custom_attach_widget.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/widget/preview_image_widget.dart';
import 'package:camis_teacher_application/app/service/api/attachment_api/post_attachment_api.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../bloc/grading_bloc.dart';
import '../bloc/view_image_bloc.dart';

class AttachFileBottomSheetWidget extends StatefulWidget {
  final int classID,subjectID,type,month,id;
  final String semester;
  final bool isInstructor;
  final VoidCallback? onTapisPreview;
  const AttachFileBottomSheetWidget({Key? key,required this.isInstructor,this.onTapisPreview, required this.classID, required this.subjectID, required this.type, required this.month, required this.semester, required this.id})
    : super(key: key);
  @override
  State<AttachFileBottomSheetWidget> createState() => _AttachFileBottomSheetWidgetState();
}

class _AttachFileBottomSheetWidgetState extends State<AttachFileBottomSheetWidget> {
  final ImagePicker _picker = ImagePicker();
  List<File> listImage = [];
  bool _isLoading = false;

  void getImageFromCamera() async {
    final PickedFile = await _picker.pickImage(source: ImageSource.camera);
    if (PickedFile != null) {
      setState(() {
        listImage.add(File(PickedFile.path));
        BlocProvider.of<ImageSelectImageBloc>(context).listImagePickerCubit(filepicker: listImage);
      });
    }
  }

  Future getImageFromGallery() async {
    final pickedFiles = await _picker.pickMultiImage(imageQuality: 50);
    if (pickedFiles != null) {
      setState(() {
        pickedFiles.forEach((pickedFile) {
          listImage.add(File(pickedFile.path));
          BlocProvider.of<ImageSelectImageBloc>(context)
              .listImagePickerCubit(filepicker: listImage);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                  icon: const Icon(Icons.close),
                ),
                Text(
                  "ATTACHMENT".tr(),
                  style: ThemsConstands.headline4_regular_18.copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(width: 40),
              ],
            ),
            Text(
              "ATTACHMENTTYPE".tr(),
              style: ThemsConstands.headline4_regular_18.copyWith(fontWeight: FontWeight.w600,color: Colorconstand.darkTextsDisabled),
            ),
            const SizedBox(
              height: 20,
            ),
            BlocBuilder<ImageSelectImageBloc, ImageSelectStatus>(
              builder: (context, state) {
                if (state == ImageSelectStatus.loading) {
                  return const Center(
                    child: CircularProgressIndicator()
                  );
                } else if (state == ImageSelectStatus.success) {
                  var image =
                      BlocProvider.of<ImageSelectImageBloc>(context).listImage;
                  return image.isNotEmpty
                      ? Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          height: 265,
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: image.length + 1,
                            itemBuilder: (context, index) {
                              return index != image.length
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                      child: DottedBorder(
                                        color: Colorconstand.neutralGrey,
                                        dashPattern: const <double>[8, 8],
                                        padding: const EdgeInsets.all(5),
                                        radius: const Radius.circular(12),
                                        borderType: BorderType.RRect,
                                        strokeWidth: 1,
                                        borderPadding: const EdgeInsets.all(1),
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.push( context,
                                              animtedRouteBottomToTop(
                                                topage: PreviewImageWidget(imageIndex: index),
                                              ),
                                            );
                                          },
                                          child: Stack(
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:BorderRadius.circular(20),
                                                ),
                                                child: Image.file(
                                                  image[index],
                                                  width: 150,
                                                  height: 250,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                              Container(
                                                height: 250,
                                                width: 150,
                                                color: Colors.black.withOpacity(0.4),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment:CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset( ImageAssets.eye_icon, color: Colors.white,
                                                    ),
                                                    const SizedBox( height: 10),
                                                    Text(
                                                      "PREVIEW".tr(),
                                                      style: ThemsConstands.headline6_medium_14.copyWith(
                                                        color: Colorconstand.lightBackgroundsWhite,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  : imageOptoinWidget(
                                      onCamera: () {
                                        getImageFromCamera();
                                      },
                                      onGallery: () {
                                        getImageFromGallery();
                                      },
                                    );
                            },
                          ),
                        )
                      : imageOptoinWidget(
                          onCamera: () {
                            getImageFromCamera();
                          },
                          onGallery: () {
                            getImageFromGallery();
                          },
                        );
                } else {
                  return imageOptoinWidget(
                    onCamera: () {
                      getImageFromCamera();
                    },
                    onGallery: () {
                      getImageFromGallery();
                    },
                  );
                }
              },
            ),
      
           BlocProvider.of<ImageSelectImageBloc>(context)
                .listImage
                .isNotEmpty? 
                MaterialButton(
                height: 57.18,
                minWidth: MediaQuery.of(context).size.width,
                onPressed: BlocProvider.of<ImageSelectImageBloc>(context)
                        .listImage
                        .isNotEmpty
                    ? () async {
                     setState(() {
                        _isLoading = true;
                     });
                        PostAttachmetnApi().postAttachmentApi(image: BlocProvider.of<ImageSelectImageBloc>(context)
                        .listImage, subjectId: widget.id.toString()).then((value){
                           setState(() {
                          _isLoading = false;
                          Navigator.of(context).pop();
                          BlocProvider.of<ViewImageBloc>(context).add(GetViewImageEvent(widget.id.toString()),);
                          BlocProvider.of<GradingBloc>(context).add( 
                            GetGradingEvent(
                              idClass:widget.classID.toString(),subjectId: widget.subjectID.toString(),type: widget.type.toString(),month: widget.month.toString(),semester: widget.semester, examDate: ""));
                           Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> ResultScreen(
                                isInstructor: widget.isInstructor
                                )));
                     });
                        });
                      }
                    : null,
              color: Colorconstand.primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.5)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                _isLoading?  const SizedBox(
                    height: 25,
                    width: 25,
                    child: CircularProgressIndicator(color: Colorconstand.neutralWhite,strokeWidth: 3)):Container(),
                  const SizedBox(width: 15,),
                  Text(
                    "CONTINUE".tr(),
                    style: ThemsConstands.button_semibold_16
                        .copyWith(color: Colorconstand.lightGoten),
                  ),
                ],
              ),
            ):const SizedBox(
              height: 10,
            ),
            const SizedBox(
              height: 1,
            ),
            MaterialButton(
              height: 57.18,
              minWidth: MediaQuery.of(context).size.width,
              onPressed: () {
                Navigator.of(context).pop();
                BlocProvider.of<GradingBloc>(context).add( GetGradingEvent(
                  idClass:  widget.classID.toString(),subjectId: widget.subjectID.toString(),type: widget.type.toString(),month: widget.month.toString(),semester: widget.semester,examDate: ""));
                 Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> ResultScreen(
                                isInstructor: widget.isInstructor
                                )));
              },
              color: Colorconstand.neutralWhite,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.5)),
              child: Text(
                "SKIP&SEND".tr(),
                style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBlack),
              ),
            ),
          ],
        ),
      ),
    );

  }

  Widget imageOptoinWidget(
      {required Function()? onCamera, required Function()? onGallery}) {
    return DottedBorder(
      color: Colorconstand.neutralGrey,
      dashPattern: const <double>[8, 4],
      padding: const EdgeInsets.all(10),
      radius: const Radius.circular(12),
      borderType: BorderType.RRect,
      strokeWidth: 1.5,
      borderPadding: const EdgeInsets.all(1),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: onCamera,
            // () {
            //   _imgFromCamera();
            // },
            child: CustomAttachListWidget(
              title: "CAMERA".tr(),
              svg: ImageAssets.camera,
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          InkWell(
            onTap: onGallery,
            // () {
            //   getImageFromGallery();
            // },
            child: CustomAttachListWidget(
              title: "GALLERY".tr(),
              svg: ImageAssets.gallery,
            ),
          ),
        ],
      ),
    );
  }

}

Route animtedRouteBottomToTop({required Widget topage}) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => topage,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = const Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}