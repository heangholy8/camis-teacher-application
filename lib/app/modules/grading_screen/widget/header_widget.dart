import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import 'header_item_wiget.dart';

class HeaderGradingWidget extends StatelessWidget {
  final String? subjectName;
  final String? gradingname;
  final String? academicYear;
  final String? acedemicMonth;
  final Widget? child;
  final Color? colorIcon;
  final Color? colorTitle;
  final VoidCallback? onTapsetExam;
  final VoidCallback? onTapNoExam;
  final VoidCallback? onTapNoExamDate;
  final VoidCallback? onTapBack;
  bool offline = true;

  HeaderGradingWidget(
      {super.key,
      this.subjectName,
      this.gradingname,
      this.academicYear,
      this.acedemicMonth,
      required this.colorIcon,
      required this.colorTitle,
      required this.onTapBack,
       this.onTapsetExam,
       this.onTapNoExam,
       this.onTapNoExamDate,
      this.child,
      this.offline=true});
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap: onTapBack,
                    child: SvgPicture.asset(
                      ImageAssets.chevron_left,
                      height: 28,
                      width: 28,
                    ),
                  ),
                  Expanded(
                    child: Text(
                    "${"GRADING".tr()} - $subjectName",
                      style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.lightBlack),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    width: 28,
                    child: PopupMenuButton<int>(
                      padding:const EdgeInsets.all(0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    offset:const Offset(-40, 4),
                    icon: SvgPicture.asset(
                      ImageAssets.more,
                      height: 28,
                      width: 28,
                    ),
                    itemBuilder: (BuildContext context) => [
                          PopupMenuItem(
                            onTap: onTapsetExam,
                            child: Container(
                              padding:const EdgeInsets.symmetric(horizontal: 2,vertical: 1),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                  children: [
                                    Container(width: 35, child: SvgPicture.asset(ImageAssets.calendar_add,color: Colorconstand.primaryColor,)),
                                    Expanded(child: Text("SET_EXAM_DATE".tr(),style: ThemsConstands.headline6_regular_14_24height.copyWith(color: Colorconstand.primaryColor),))
                                  ]),
                            ),
                          ),
                          PopupMenuItem(
                          onTap: onTapNoExam,
                          child: Container(
                            padding:const EdgeInsets.symmetric(horizontal: 2,vertical: 1),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                children: [
                                  Container(width: 35, child: SvgPicture.asset(ImageAssets.note_remove,color: Colors.red,)),
                                  Expanded(child: Text("NO_EXAM".tr(),style: ThemsConstands.headline6_regular_14_24height.copyWith(color: Colorconstand.alertsDecline),))
                                ]),
                          ),
                          ),
                          PopupMenuItem(
                          onTap:onTapNoExamDate,
                          child: Container(
                            padding:const EdgeInsets.symmetric(horizontal: 2,vertical: 1),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                children: [
                                  Container(width: 35, child: SvgPicture.asset(ImageAssets.note_remove,color: Colors.red,)),
                                  Expanded(child: Text("NOT_SET_EXAM_DAY".tr(),style: ThemsConstands.headline6_regular_14_24height.copyWith(color: Colorconstand.alertsDecline),))
                                ]),
                          ),
                          ),
                        ]
                    ),
                  )
               
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top:15),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      HeaderItemWidget(
                        title: gradingname ?? "Empty",
                        svg: ImageAssets.location,
                        colorIcons: Colorconstand.primaryColor,
                        colorTitle: Colorconstand.darkTextsRegular,
                      ),
                      HeaderItemWidget(
                        title: academicYear ?? "Empty",
                        svg: ImageAssets.clipboard,
                        colorIcons: Colorconstand.primaryColor,
                        colorTitle: Colorconstand.darkTextsRegular,
                      ),
                      HeaderItemWidget(
                        title: acedemicMonth ?? "Empty",
                        svg: ImageAssets.current_date,
                        colorIcons: colorIcon!,
                        colorTitle: colorTitle!,
                      ),
                    ],
                  ),
                ),
              ),
              child ?? Container(),
            ],
          ),
        );
      },
    );
  }
}
