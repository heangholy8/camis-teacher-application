import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shimmer/shimmer.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';


class GradingShimmerWidget extends StatelessWidget {
  const GradingShimmerWidget({super.key});

  // baseColor: Colorconstand.neutralGrey,
  //       highlightColor: Colorconstand.neutralDarkGrey.withOpacity(0.1),

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 18),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: SvgPicture.asset(
                      ImageAssets.chevron_left,
                      height: 28,
                      width: 28,
                    ),
                  ),
                  SizedBox(
                    width: width / 2,
                    height: 40,
                    child: Shimmer.fromColors(
                      baseColor: Colorconstand.neutralGrey,
                      highlightColor:
                          Colorconstand.neutralGrey.withOpacity(0.01),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(4),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 40,
                    height: 40,
                    child: Shimmer.fromColors(
                      baseColor: Colorconstand.neutralGrey,
                      highlightColor:
                          Colorconstand.neutralDarkGrey.withOpacity(0.1),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(4),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 18,
              ),
              SizedBox(
                width: width,
                height: 40,
                child: Shimmer.fromColors(
                  baseColor: Colorconstand.neutralGrey,
                  highlightColor:
                      Colorconstand.neutralDarkGrey.withOpacity(0.1),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: const Padding(
                      padding: EdgeInsets.symmetric(vertical: 31),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        const Expanded(
          child: Center(child: CircularProgressIndicator()),
        ),
        const SizedBox(
          height: 80,
        )
      ],
    );
  }
}
