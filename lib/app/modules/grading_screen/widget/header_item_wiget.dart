import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/thems/thems_constands.dart';

class HeaderItemWidget extends StatelessWidget {
  String title, svg;
  Color colorIcons;
  Color colorTitle;
  HeaderItemWidget({
    Key? key,
    required this.title,
    required this.svg,
    required this.colorIcons,
    required this.colorTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SvgPicture.asset(
          svg,
          height: 24,
          width: 24,
          color:colorIcons,
        ),
        const SizedBox(
          width: 5,
        ),
        Text(
          title,
          style: ThemsConstands.headline_5_medium_16.copyWith(color: colorTitle),
        )
      ],
    );
  }
}
