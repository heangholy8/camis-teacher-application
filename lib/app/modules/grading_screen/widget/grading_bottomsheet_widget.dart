// ignore_for_file: must_be_immutable

import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import '../../../../widget/button_widget/button_widget.dart';
import '../../../models/grading_model/grading_model.dart';
import '../../../service/api/grading_api/post_graeding_data_api.dart';

class GradingBottomSheetWidget extends StatefulWidget {
  final int progressStep;
  bool? isEnd;
  bool? edit;
  VoidCallback finishEnterScore;
  VoidCallback closeSaveScore;
  final List<StudentsData> data;
  GradingBottomSheetWidget({
    Key? key,
    required this.progressStep,
    required this.isEnd,
    required this.data,
    required this.edit,
    required this.finishEnterScore,
    required this.closeSaveScore
  }) : super(key: key);

  @override
  State<GradingBottomSheetWidget> createState() =>
      _GradingBottomSheetWidgetState();
}

class _GradingBottomSheetWidgetState extends State<GradingBottomSheetWidget> {
  int index = 0;
  bool isFocus = true;
  bool isNext  =true,isNext1=true;
  bool isChange = false, _isComment = false;
  FocusNode focusNode = FocusNode();
  FocusNode commentFocusNode = FocusNode();

  TextEditingController controller = TextEditingController();
  TextEditingController controller1 = TextEditingController();
  TextEditingController controller2 = TextEditingController();
  TextEditingController controllerComment = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  RegExp regex = RegExp(r'([.]*0)(?!.*\d)');
  @override
  void initState() {
    super.initState();
    print("absent: ${widget.data[index].absentExam}");
    index = widget.progressStep-1;
    if(widget.data[index].childrenSubject!.isEmpty){
      if(widget.data[index].absentExam == 1){
        controller.text = "A";
        setState(() {
          widget.data[index].score = 0;
        });
      }
      else if(widget.data[index].absentExam == 0){
        controller.text =  widget.data[index].score.toString()=="null"?"":widget.data[index].score.toString();
      }
    }
    else{
        controller1.text =  widget.data[index].absentExam.toString()=="1"?"A" : widget.data[index].childrenSubject![0].score.toString()=="null"||widget.data[index].childrenSubject![0].score.toString()==""?"":widget.data[index].childrenSubject![0].score.toString();
        controller2.text = widget.data[index].absentExam.toString()=="1"?"A":widget.data[index].childrenSubject![1].score.toString()=="null"||widget.data[index].childrenSubject![1].score.toString()==""?"":widget.data[index].childrenSubject![1].score.toString();
        if(widget.data[index].absentExam == 1){
          setState(() {
            widget.data[index].childrenSubject![0].score = 0;
            widget.data[index].childrenSubject![1].score = 0;
          });
        }
    }
    controllerComment.text = widget.data[index].teacherComment==null?"":widget.data[index].teacherComment.toString();
  }

  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) {
        var oldValue = widget.data[index].score;
        return GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Container(
            height: MediaQuery.of(context).size.height - 65,
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(width: 80,),
                    // IconButton(
                    //     onPressed: () {
                    //       setState(() {
                    //         widget.isEnd = false;
                    //       });
                    //       Navigator.pop(context, widget.isEnd);
                    //     },
                    //     icon: const Icon(Icons.close)),
                     Expanded(
                      child: Center(
                        child: Text(
                          "SCORING".tr(),
                          style: ThemsConstands.headline3_medium_20_26height,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 40,
                    ),
                    PopupMenuButton<int>(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    offset: const Offset(-40, 4),
                    icon: SvgPicture.asset(
                      ImageAssets.more,
                    ),
                    itemBuilder: (BuildContext context) => [
                          PopupMenuItem(
                            onTap: () {
                               setState((){
                                 if(widget.data[index].childrenSubject!.isEmpty){
                                  if(widget.data[index].absentExam==1){
                                    widget.data[index].absentExam=0;
                                     widget.data[index].score=null;
                                     controller.text = "";
                                     isFocus = true;
                                  }
                                  else{
                                    widget.data[index].score=0;
                                    widget.data[index].absentExam=1;
                                    controller.text = "";
                                    isFocus = true;
                                  }
                                 }
                                 else{
                                    if(widget.data[index].absentExam==1){
                                      widget.data[index].absentExam=0;
                                      widget.data[index].score=null;
                                      widget.data[index].childrenSubject![0].score = null;
                                      widget.data[index].childrenSubject![1].score = null;
                                      controller1.text = "";
                                      controller2.text = "";
                                    }
                                    else{
                                      widget.data[index].absentExam=1;
                                      widget.data[index].score=0;
                                      widget.data[index].childrenSubject![0].score = 0;
                                      widget.data[index].childrenSubject![1].score = 0;
                                      controller1.text = "";
                                      controller2.text = "";
                                    }
                                 }
                               });
                            },
                            child: Container(
                              child: Row(
                                  children: [
                                    widget.data[index].absentExam==0?
                                    SvgPicture.asset(
                                      ImageAssets.absent,
                                      height: 20,
                                      width: 20,
                                    ):const Icon(Icons.check_circle_sharp,color: Colorconstand.alertsPositive,),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    widget.data[index].absentExam==0? Text("ABSENT".tr()):Text("PRESENCES".tr())
                                  ]),
                            ),
                          ),
                        ])
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 17, top: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ImageAssets.profile_2users),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: StepProgressIndicator(
                            totalSteps: widget.data.length,
                            currentStep: widget.data.where((element) => element.score!=null).length,
                            size: 12,
                            padding: 0.5,
                            roundedEdges: const Radius.circular(20),
                            unselectedColor: Colorconstand.lightGoten,
                            selectedGradientColor: const LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              colors: [
                                Colorconstand.mainColorSecondary,
                                Colorconstand.primaryColor
                              ],
                            ),
                          ),
                        ),
                      ),
                      Text(
                        "${widget.data.where((element) => element.score!=null).length}/${widget.data.length}",
                        style: ThemsConstands.button_semibold_16.copyWith(
                          color: Colorconstand.primaryColor,
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Scaffold(
                    resizeToAvoidBottomInset: false,
                    body: SingleChildScrollView(
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Center(
                                child: CircleAvatar(
                                  radius: 48.0,
                                  backgroundImage: NetworkImage(
                                      widget.data[index].profileMedia!.fileShow!),
                                  backgroundColor:
                                      const Color.fromARGB(0, 249, 233, 233),
                                ),
                              ),
                             widget.data[index].absentExam.toString()=="1"
                                  ? Positioned(
                                      bottom: 0,
                                      right:
                                          MediaQuery.of(context).size.width / 3,
                                      child: SizedBox(
                                        height: 23.23,
                                        child: SvgPicture.asset(
                                          ImageAssets.absent,
                                        ),
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Text(
                             translate=="km"? widget.data[index].studentName!:widget.data[index].studentNameEn!,
                              style: ThemsConstands.headline3_semibold_20
                                  .copyWith(),
                            ),
                          ),
                          // const SizedBox(height: 3,),
                          //  Text("SCORING".tr()),
                          const SizedBox(
                            height: 20,
                          ),
                          widget.data[index].childrenSubject!.isEmpty || widget.data[index].childrenSubject!.length == 1? 
                          Form(
                            key: _formKey,
                            child: 
                            TextFormField(
                              onChanged: (value) {
                                //try {
                                  if(value == ""){
                                    setState(() {
                                      widget.data[index].score = null;
                                    },);
                                  }
                                  else{
                                    if (double.parse(value) <= widget.data[index].subjectScoreMax!) {
                                        setState(() {
                                          isNext = true;
                                          widget.data[index].score = value;
                                        },);
                                    } else {
                                      setState(() {
                                            isNext = false;
                                            widget.data[index].score = null;
                                        },);
                                    }
                                  }
                                // } catch (e) {
                                //   setState(() {
                                //           isNext = false;
                                //            widget.data[index].score = null;
                                //         },);
                                // }
                              },
                              controller: controller,
                              enabled: widget.data[index].absentExam.toString()=="1"
                                  ? false
                                  : true,
                              maxLength: widget.data[index].subjectScoreMax.toString().length+2,
                              cursorHeight: 25,
                              style: ThemsConstands.headline_2_semibold_24.copyWith(color:widget.data[index].absentExam == 1?Colorconstand.alertsDecline:Colorconstand.neutralDarkGrey),
                              autofocus: isFocus, 
                              focusNode: focusNode,
                              textInputAction: TextInputAction.next,
                              keyboardType:const TextInputType.numberWithOptions(decimal: true),
                              textAlignVertical: TextAlignVertical.center,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                counterText: "",
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(Radius.circular(14)),
                                    borderSide: BorderSide(width: 1,color: !isNext?Colorconstand.alertsDecline:Colorconstand.primaryColor),
                                  ),
                                  suffixText:widget.data[index].absentExam == 1?"":"/${widget.data[index].subjectScoreMax}",
                                  suffixStyle:
                                      ThemsConstands.headline3_semibold_20,
                                  border: OutlineInputBorder(
                                   
                                    borderRadius: BorderRadius.circular(14.0),
                                  ),
                                  filled: true,
                                  hintText: widget.data[index].absentExam.toString()=="1"
                                      ? "A".tr()
                                      : "",
                                  hintStyle: const TextStyle(
                                      color: Colorconstand.alertsNotifications),
                                  fillColor: widget.data[index].absentExam.toString()=="1"
                                      ? Colorconstand.alertsDeclinedBg
                                      : Colors.white70),
                            ),
                          
                          )
                          //Two TextFiled
                          :Column(
                            children: [
                              Row(children: [
                              Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top: 0,bottom: 5),
                                        child: Text(
                                        translate=="km"? widget.data[index].childrenSubject![0].name!:widget.data[index].childrenSubject![0].nameEn!,
                                          style: ThemsConstands.headline3_medium_20_26height.copyWith(),
                                        ),
                                      ),
                                      TextFormField(
                                      onChanged: (value) {
                                          //try {
                                            if(value == ""){
                                              widget.data[index].childrenSubject![0].score = null;
                                                if(widget.data[index].childrenSubject![1].score==null){
                                                  setState(() {
                                                    widget.data[index].score = null;
                                                  },);
                                                }
                                                else{
                                                  setState(() {
                                                    widget.data[index].score = (double.parse(widget.data[index].childrenSubject![1].score.toString()) + 0).toString().replaceAll(regex, "");
                                                  },);
                                                }
                                            }
                                            else{
                                                if (double.parse(value) <= widget.data[index].childrenSubject![0].maxScore) {
                                                  setState(() {
                                                    isNext = true;
                                                  },);
                                                  widget.data[index].childrenSubject![0].score = value;
                                                  if(widget.data[index].childrenSubject![1].score==null){
                                                    setState(() {
                                                      widget.data[index].score = widget.data[index].childrenSubject![0].score;
                                                    },);
                                                  }
                                                  else{
                                                    setState(() {
                                                      widget.data[index].score = (double.parse(widget.data[index].childrenSubject![1].score.toString()) + double.parse(widget.data[index].childrenSubject![0].score.toString())).toString().replaceAll(regex, "");
                                                    },);
                                                  }
                                                } else {
                                                  setState(() {
                                                    isNext = false;
                                                  },);
                                                  widget.data[index].childrenSubject![0].score = null;
                                                  if(widget.data[index].childrenSubject![1].score==null){
                                                    setState(() {
                                                      widget.data[index].score =null;
                                                    },);
                                                  }
                                                  else{
                                                    setState(() {
                                                      widget.data[index].score = double.parse(widget.data[index].childrenSubject![1].score.toString()).toString().replaceAll(regex, "");
                                                    },);
                                                  }
                                                }
                                            }
                                      },
                                      controller: controller1,
                                      enabled: widget.data[index].absentExam.toString()=="1"
                                      ? false
                                      : true,
                                      maxLength: widget.data[index].childrenSubject![0].maxScore.toString().length+2,
                                      cursorHeight: 25,
                                      
                                      style: ThemsConstands.headline_2_semibold_24.copyWith(color:widget.data[index].absentExam == 1?Colorconstand.alertsDecline:Colorconstand.neutralDarkGrey),
                                      autofocus: isFocus, 
                                      focusNode: focusNode,
                                      textInputAction: TextInputAction.next,
                                      keyboardType:const TextInputType.numberWithOptions(decimal: true),
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        counterText: "",
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: const BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(width: 1,color: !isNext?Colorconstand.alertsDecline:Colorconstand.primaryColor),
                                        ),
                                        suffixText:widget.data[index].absentExam == 1?"":
                                            "/${widget.data[index].childrenSubject![0].maxScore}",
                                        suffixStyle:
                                            ThemsConstands.headline3_semibold_20,
                                        border: OutlineInputBorder(
                                          
                                          borderRadius: BorderRadius.circular(14.0),
                                        ),
                                        filled: true,
                                        hintText: widget.data[index].absentExam.toString()=="1"
                                            ? "A".tr()
                                            : "",
                                        hintStyle: const TextStyle(
                                            color: Colorconstand.alertsNotifications),
                                        fillColor: widget.data[index].absentExam.toString()=="1"
                                            ? Colorconstand.alertsDeclinedBg
                                            : Colors.white70),
                                      ),
                                    ],
                                  ),
                                ),
                              
                                const SizedBox(width: 20,),
        
                              Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top: 0,bottom: 5),
                                        child: Text(
                                        translate=="km"? widget.data[index].childrenSubject![1].name!:widget.data[index].childrenSubject![1].nameEn!,
                                          style: ThemsConstands.headline3_medium_20_26height.copyWith(),
                                        ),
                                      ),
                                      TextFormField(
                                      onChanged: (value) {
                                        if(value == ""){
                                          widget.data[index].childrenSubject![1].score = null;
                                            if(widget.data[index].childrenSubject![0].score==null){
                                              setState(() {
                                                widget.data[index].score = null;
                                              },);
                                            }
                                            else{
                                              setState(() {
                                                widget.data[index].score = (double.parse(widget.data[index].childrenSubject![0].score.toString()) + 0).toString().replaceAll(regex, "");
                                              },);
                                            }
                                        }
                                        else{
                                            if (double.parse(value) <= widget.data[index].childrenSubject![1].maxScore) {
                                              setState(() {
                                                isNext1 = true;
                                              },);
                                              widget.data[index].childrenSubject![1].score = value;
                                              if(widget.data[index].childrenSubject![0].score==null){
                                                setState(() {
                                                  widget.data[index].score = widget.data[index].childrenSubject![1].score;
                                                },);
                                              }
                                              else{
                                                setState(() {
                                                  widget.data[index].score = (double.parse(widget.data[index].childrenSubject![0].score.toString()) + double.parse(widget.data[index].childrenSubject![1].score.toString())).toString().replaceAll(regex, "");
                                                },);
                                              }
                                            } else {
                                              setState(() {
                                                isNext1 = false;
                                              },);
                                              widget.data[index].childrenSubject![1].score = null;
                                              if(widget.data[index].childrenSubject![0].score==null){
                                                setState(() {
                                                  widget.data[index].score =null;
                                                },);
                                              }
                                              else{
                                                setState(() {
                                                  widget.data[index].score = double.parse(widget.data[index].childrenSubject![0].score.toString()).toString().replaceAll(regex, "");
                                                },);
                                              }
                                            }
                                        }
                                          // } catch (e) {
                                          //   setState(() {
                                          //           isNext1 = false;
                                          //           widget.data[index].childrenSubject![1].score = 0 ;
                                          //           widget.data[index].score = (widget.data[index].childrenSubject![0].score.toString()=="null"?0:widget.data[index].childrenSubject![0].score)+widget.data[index].childrenSubject![1].score;
                                          //         },);
                                          // }
                                      },
                                      controller: controller2,
                                      enabled: widget.data[index].absentExam.toString()=="1"
                                      ? false
                                      : true,
                                      maxLength: widget.data[index].childrenSubject![1].maxScore.toString().length+2,
                                      cursorHeight: 25,
                                      style: ThemsConstands.headline_2_semibold_24.copyWith(color:widget.data[index].absentExam == 1?Colorconstand.alertsDecline:Colorconstand.neutralDarkGrey),
                                      // autofocus: isFocus, 
                                      // focusNode: focusNode,
                                      textInputAction: TextInputAction.next,
                                      keyboardType:const TextInputType.numberWithOptions(decimal: true),
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                      counterText: "",
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: const BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(width: 1,color: !isNext1?Colorconstand.alertsDecline:Colorconstand.primaryColor),
                                        ),
                                        suffixText:widget.data[index].absentExam == 1?"":
                                            "/${widget.data[index].childrenSubject![1].maxScore}",
                                        suffixStyle:
                                            ThemsConstands.headline3_semibold_20,
                                        border: OutlineInputBorder(
                                          
                                          borderRadius: BorderRadius.circular(14.0),
                                        ),
                                        filled: true,
                                        hintText: widget.data[index].absentExam.toString()=="1"
                                            ? "A".tr()
                                            : "",
                                        hintStyle: const TextStyle(
                                            color: Colorconstand.alertsNotifications),
                                        fillColor: widget.data[index].absentExam.toString()=="1"
                                            ? Colorconstand.alertsDeclinedBg
                                            : Colors.white70),
                                      ),
                                    ],
                                  ),
                                ),
                                ],
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(vertical: 20,horizontal: 0),
                                padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                                decoration: BoxDecoration(color:widget.data[index].absentExam.toString()=="1"?Colorconstand.alertsDeclinedBg: Colorconstand.subject13.withOpacity(0.3),borderRadius: const BorderRadius.all(Radius.circular(15)) ),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("${"TOTAL".tr()}៖",style: ThemsConstands.headline4_regular_18.copyWith(color: Colorconstand.subject13),),
                                    Expanded
                                    (
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(widget.data[index].absentExam.toString()=="1"? "A":widget.data[index].score.toString()=="null"||widget.data[index].score.toString()==""?"-":widget.data[index].score.toString(),style: ThemsConstands.headline_1_semibold_32.copyWith(color: widget.data[index].score==0&&widget.data[index].absentExam == 1?Colorconstand.alertsDecline:Colorconstand.subject13),),
                                            const SizedBox(width: 10,),
                                            Text(widget.data[index].absentExam == 1?"     ":"/${widget.data[index].subjectScoreMax}",style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.subject13),),
                                          ],
                                        ),
                                      ),
                                    ),
                                ],
                              ),)
                            ],
                          ),
                          isNext && isNext1?Container(): Text(
                            'WRONGFORMAT'.tr(),
                            style: const TextStyle(
                              color: Colorconstand.alertsDecline,
                            ),
                          ),
                          const SizedBox(height: 10,),
                          widget.data[index].absentExam.toString()=="1"
                              ? InkWell(
                                  onTap: () {
                                    setState(() {
                                      if(widget.data[index].childrenSubject!.isEmpty){
                                         widget.data[index].absentExam = 0;
                                          widget.data[index].score = null;
                                          controller.text = "";
                                          isFocus = true;
                                          }
                                      else{
                                        widget.data[index].absentExam=0;
                                        widget.data[index].score=null;
                                        widget.data[index].childrenSubject![0].score = null;
                                        widget.data[index].childrenSubject![1].score = null;
                                        controller1.text = "";
                                        controller2.text = "";
                                      }
                                    });
                                  },
                                  child: Text(
                                    'SCORING'.tr(),
                                    style: const TextStyle(
                                        decoration: TextDecoration.underline,
                                        color: Colorconstand.alertsPositive),
                                  ),
                                )
                              : !_isComment
                                  ? InkWell(
                                      onTap: () {
                                        setState(() {
                                          _isComment = true;
                                          commentFocusNode.requestFocus();
                                        });
                                      },
                                      child:  Text(
                                        'COMMENT'.tr(),
                                        style:ThemsConstands.button_semibold_16.copyWith(decoration: TextDecoration.underline,),
                                      ),
                                    )
                                  : SizedBox(
                                      width: MediaQuery.of(context).size.width,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: ConstrainedBox(
                                                constraints: BoxConstraints(
                                                  maxHeight: MediaQuery.of(context)
                                                      .size
                                                      .height,
                                                  maxWidth: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                ),
                                                child: TextFormField(
                                                  onChanged: (value) {
                                                    try {
                                                      widget.data[index]
                                                          .teacherComment = value;
                                                    } catch (e) {}
                                                  },
                                                  controller: controllerComment,
                                                  maxLines: 1,
                                                  minLines: 1,
                                                  focusNode: commentFocusNode,
                                                  style: ThemsConstands
                                                      .headline_6_regular_14_20height,
                                                  keyboardType:
                                                      TextInputType.multiline,
                                                  textAlignVertical:
                                                      TextAlignVertical.center,
                                                  textAlign: TextAlign.left,
                                                  decoration: InputDecoration(
                                                      border: OutlineInputBorder(
                                                        gapPadding: 0,
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                8.0),
                                                      ),
                                                      filled: true,
                                                      fillColor: Colors.white70),
                                                ),
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  _isComment = false;
                                                });
                                              },
                                              icon: const Icon(Icons.close))
                                        ],
                                      ),
                                    ),
                          const SizedBox(
                            height: 39,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                  widget.edit == true ?Container(): CustomMaterialButton(
                                  padding:const EdgeInsets.symmetric(horizontal: 5,),
                                  borderColor: Colorconstand.primaryColor,
                                  titleButton: "${"SAVE".tr()} & ${"CLOSE".tr()}", 
                                  hight: 60,
                                  styleText: ThemsConstands.headline4_regular_18.copyWith(color: Colorconstand.primaryColor),
                                  onPressed: widget.closeSaveScore,
                                ),
                                index == 0 || widget.edit == true?Container():Container(
                                  padding:const EdgeInsets.all(4),
                                  decoration:const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colorconstand.primaryColor
                                  ),
                                  child: InkWell(
                                    radius: 12,
                                    onTap:!isNext?null : () {
                                      focusNode.requestFocus();
                                      if (index <= 0) {
                                        setState(() {
                                          index = widget.data.length - 1;
                                          controller.text = widget.data[index].absentExam == 1?"A": widget.data[index].score.toString() == "null"? "": widget.data[index].score.toString();
                                          controllerComment.text = widget.data[index].teacherComment.toString() =="null"? "": widget.data[index].teacherComment.toString();
                                          if(widget.data[index].childrenSubject!.isNotEmpty){
                                            if(widget.data[index].absentExam == 1){
                                              controller1.text = "A";
                                              controller2.text = "A";
                                            }
                                            else{
                                              controller1.text = widget.data[index].childrenSubject![0].score.toString()=="null"||widget.data[index].childrenSubject![0].score==""?"":widget.data[index].childrenSubject![0].score.toString();
                                              controller2.text = widget.data[index].childrenSubject![1].score.toString()=="null"||widget.data[index].childrenSubject![1].score==""?"":widget.data[index].childrenSubject![1].score.toString();
                                            }
                                          }      
                                        });
                                      } else {
                                        setState(() {
                                          isFocus = true;
                                          index--;
                                          controller.text = widget.data[index].absentExam == 1?"A":widget.data[index].score.toString() =="null"? "": widget.data[index].score.toString();
                                          controllerComment.text = widget.data[index].teacherComment.toString() == "null"? "": widget.data[index].teacherComment.toString();
                                           if(widget.data[index].childrenSubject!.isNotEmpty){
                                            if(widget.data[index].absentExam == 1){
                                              controller1.text = "A";
                                              controller2.text = "A";
                                            }
                                            else{
                                              controller1.text = widget.data[index].childrenSubject![0].score.toString()=="null"||widget.data[index].childrenSubject![0].score==""?"":widget.data[index].childrenSubject![0].score.toString();
                                              controller2.text = widget.data[index].childrenSubject![1].score.toString()=="null"||widget.data[index].childrenSubject![1].score==""?"":widget.data[index].childrenSubject![1].score.toString();
                                            }
                                          }        
                                        });
                                      }
                                    },
                                    child: SvgPicture.asset(ImageAssets.arrow_left,width: 40,color: isNext?Colorconstand.neutralWhite:Colorconstand.darkTextsDisabled,),
                                  ),
                                ),
                                widget.edit == true? 
                                MaterialButton(
                                    height: 57.18,
                                    onPressed: widget.closeSaveScore,
                                    color:isNext? Colorconstand.primaryColor:Colorconstand.darkBackgroundsDisabled,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8.5)),
                                    child: Center(
                                      child: Row(
                                        children: [
                                          Text("${"SAVE".tr()} & ${"CLOSE".tr()}",
                                            style: ThemsConstands
                                                .headline_4_semibold_18
                                                .copyWith(
                                                    color:isNext?
                                                        Colorconstand.lightGoten:Colorconstand.darkTextsDisabled),
                                          ),
        
                                          Container(margin:const EdgeInsets.only(left: 5),child: const Icon(Icons.save_outlined,size: 23,color: Colorconstand.neutralWhite,))
                                        ],
                                      ),
                                    ))
                                :MaterialButton(
                                    height: 57.18,
                                    onPressed:!isNext? null: index == widget.data.length - 1?widget.finishEnterScore:() {
                                      focusNode.unfocus();
                                      if (index == widget.data.length - 1) {
                                        setState(() {
                                          widget.isEnd = false;
                                        });
                                        Navigator.pop(context, widget.isEnd);
                                      } else {
                                        setState(() {
                                          index++;
                                          controller.text = widget.data[index].absentExam == 1?"A":widget.data[index].score.toString() == "null"? "": widget.data[index].score.toString();
                                          controllerComment.text = widget.data[index].teacherComment.toString() == "null" ? "": widget.data[index].teacherComment.toString();
                                          if(widget.data[index].childrenSubject!.isNotEmpty){
                                            if(widget.data[index].absentExam == 1){
                                              controller1.text = "A";
                                              controller2.text = "A";
                                            }
                                            else{
                                              controller1.text = widget.data[index].childrenSubject![0].score.toString()=="null"||widget.data[index].childrenSubject![0].score==""?"":widget.data[index].childrenSubject![0].score.toString();
                                              controller2.text = widget.data[index].childrenSubject![1].score.toString()=="null"||widget.data[index].childrenSubject![1].score==""?"":widget.data[index].childrenSubject![1].score.toString();
                                            }
                                          }
                                        });
                                        Future.delayed(
                                            const Duration(milliseconds: 50), () {
                                          focusNode.requestFocus();
                                        });
                                      }
                                      setState(() {
                                        _isComment = false;
                                      },);
                                    },
                                    color:isNext? Colorconstand.primaryColor:Colorconstand.darkBackgroundsDisabled,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8.5)),
                                    child: Center(
                                      child: Row(
                                        children: [
                                          Text(
                                            widget.data.length - 1 == index
                                                ? "FINISH".tr()
                                                :"NEXTSTU".tr(),
                                                // ? "${"SAVE".tr()} & ${"FINISH".tr()}"
                                                // :"${"SAVE".tr()} & ${"NEXTSTU".tr()}",
                                            style: ThemsConstands
                                                .headline_4_semibold_18
                                                .copyWith(
                                                    color:isNext?
                                                        Colorconstand.lightGoten:Colorconstand.darkTextsDisabled,),
                                          ),
                                           widget.data.length - 1 == index?
                                           Container(margin:const EdgeInsets.only(left: 5),child: const Icon(Icons.save_outlined,size: 23,color: Colorconstand.neutralWhite,))
                                           :SvgPicture.asset(
                                              ImageAssets.arrow_right)
                                        ],
                                      ),
                                    ))
                              ]),
                          const SizedBox(
                            height: 50,
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  bottom:
                                      MediaQuery.of(context).viewInsets.bottom))
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
