import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../core/constands/color_constands.dart';

class CustomAttachListWidget extends StatelessWidget {
  final String title, svg;

  const CustomAttachListWidget({
    Key? key,
    required this.title,
    required this.svg,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      color: Colorconstand.neutralGrey,
      dashPattern: const <double>[21, 10],
      radius: const Radius.circular(10),
      borderType: BorderType.RRect,
      strokeWidth: 1,
      borderPadding: const EdgeInsets.all(1),
      child: Container(
        height: 115,
        width: 155,
        decoration: const BoxDecoration(
          color: Colorconstand.neutralSecondBackground,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              svg,
              height: 33.91,
              width: 33.96,
              color: Colors.black,
            ),
            const SizedBox(
              height: 6.75,
            ),
            Text(
              title,
              style: const TextStyle(color: Colorconstand.neutralDarkGrey),
            )
          ],
        ),
      ),
    );
  }
}
