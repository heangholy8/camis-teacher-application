// ignore_for_file: must_be_immutable, non_constant_identifier_names

import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import '../../../core/thems/thems_constands.dart';
import '../bloc/view_image_bloc.dart';

class PreviewFileWidget extends StatefulWidget {
  String examId = "";
  PreviewFileWidget({
    super.key,this.examId = ""
  });

  @override
  State<PreviewFileWidget> createState() => _PreviewFileWidgetState();
}

class _PreviewFileWidgetState extends State<PreviewFileWidget> {
  int index = 0;
  late PageController pageController;
  bool connection = true;
  StreamSubscription? sub;
  final ImagePicker _picker = ImagePicker();
  List<File> listImage = [];
  int imageIndex = 0;
  

  @override
  void initState() {
    print("hi");
    // setState(() {  
    //   sub = Connectivity().onConnectivityChanged.listen((event) {
    //     setState(() {
    //       connection = (event != ConnectivityResult.none);
    //       if (connection == true) {
    //         debugPrint("Internet connection");
           
    //       } else {
    //         debugPrint("No internet connection");
    //       }
    //     });
    //   });
    // });
    super.initState();
    pageController = PageController(
      initialPage: 0,
      keepPage: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: BlocBuilder<ViewImageBloc, ViewImageState>(
        builder: (context, state) {
          if(state is GetViewImageDataLoading){
            return const Center(child:  SizedBox( height: 50,width: 50,child:  CircularProgressIndicator(color: Colors.white,)));
          }
          else if(state is GetViewImageDataLoaded){
            var image = state.viewImageList!.data!.medias!;
            return Column(
                    children: [
                      const SizedBox(
                        height: 50,
                      ),
                      Container(
                        height: 80,
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 12,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Text(
                                    "${imageIndex+1} of ${image.length.toString()}",
                                    style: ThemsConstands.headline3_medium_20_26height
                                        .copyWith(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: const Icon(
                                  Icons.close,
                                  color: Colors.white,
                                ))
                          ],
                        ),
                      ),
                      Expanded(
                        child: PageView.builder(
                                controller: pageController,
                                itemCount: image.length,
                                itemBuilder: ((context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                    child: Container(
                                      padding:
                                          const EdgeInsets.only(top: 20, bottom: 20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                          20,
                                        ),
                                      ),
                                      child: InteractiveViewer(
                                        boundaryMargin: const EdgeInsets.all(20.0),
                                        minScale: 0.1,
                                        maxScale: 4.0,
                                        child: 
                                        Image.network(
                                          image[index].fileShow!,
                                         
                                          fit: BoxFit.cover,
                                        ),
                                       
                                      ),
                                    ),
                                  );
                                }),
                                onPageChanged: (value) {
                                  setState(() {
                                    index = value;
                                    imageIndex = value;
                                    print("value $value");
                                  });
                                }),
                      )
                        
                    ],
                  );
          }
          else{
            return Container(color: Colors.black,);
          }
        },
      ),
    );
  }
}
