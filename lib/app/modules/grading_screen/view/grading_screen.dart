// ignore_for_file: unnecessary_null_comparison

import 'dart:async';
import 'dart:convert';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/models/grading_model/grading_model.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/cubit/image_selected_cubit.dart';
import 'package:camis_teacher_application/app/modules/primary_school_screen/list_enter_score_primary/state/list_all_subject_primary/bloc/list_subject_enter_score_bloc.dart';
import 'package:camis_teacher_application/app/service/api/tracking_teacher_api/tracking_teacher_api.dart';
import 'package:camis_teacher_application/app/storages/user_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../service/api/approved_api/approved_api.dart';
import '../../../service/api/grading_api/post_graeding_data_api.dart';
import '../../../service/api/schedule_api/set_exam_api.dart';
import '../../../storages/key_storage.dart';
import '../../../storages/save_storage.dart';
import '../../attendance_schedule_screen/state_management/state_exam/bloc/exam_schedule_bloc.dart';
import '../../attendance_schedule_screen/state_management/state_month/bloc/month_exam_set_bloc.dart';
import '../../attendance_schedule_screen/view/pop_set_exam_day.dart';
import '../../home_screen/bloc/my_task_daskboard_bloc.dart';
import '../bloc/bloc/check_switch_enter_score_screen_bloc.dart';
import '../bloc/grading_bloc.dart';
import '../bloc/view_image_bloc.dart';
import '../widget/attachfile_bottom_widget.dart';
import '../widget/grading_bottomsheet_widget.dart';
import '../widget/grading_shimmer_widget.dart';
import '../widget/header_widget.dart';

class GradingScreen extends StatefulWidget {
  bool isInstructor;
  // redirectFormScreen == 3?From HomeScreen:redirectFormScreen==2?Schedule Screen:redirectFormScreen==1? From Exam Screen
  int? redirectFormScreen;
  bool? myclass;
  bool? isPrimary;
  GradingScreen({Key? key, this.isPrimary,this.myclass, this.redirectFormScreen,this.isInstructor = false} ) : super(key: key);

  @override
  State<GradingScreen> createState() => _GradingScreenState();
}

class _GradingScreenState extends State<GradingScreen> with Toast {

  UserSecureStroage saveStoragePref = UserSecureStroage();
  int _progressStep = 0;
  bool _isEnd = true;
  bool _showScrollGuide = false;
  bool openSwitchScreen = false;
  ScrollController listScrollController = ScrollController(); 
  List<TextEditingController> controller = [];
  List<TextEditingController> controller1 = [];
  List<TextEditingController> controller2 = [];
  bool connection = true;
  StreamSubscription? sub;
  bool _isLast = false;
  bool isPreviewImage = false;
  bool isFirstLoaded = true;
  PageController pageController =
  PageController(initialPage: 1, keepPage: true);
  StreamSubscription? internetconnection;
  bool isoffline = true;
  bool test = false;
  GradingData? dataScore;
  List<StudentsData>? studentDataPost;

  int? monthlocal;
  String? monthlocalCheck;
  int? daylocal;
  int? yearchecklocal;

  bool postLoading = false;
  bool isScrolling = true;
  bool checkScore = false;

  int startIndex = 0;
  bool startLoop = true;
  bool switchStatus = false;
  var isKeyboard;
  String changeUi="";
  RegExp regex = RegExp(r'([.]*0)(?!.*\d)');
  final KeyStoragePref keyPref = KeyStoragePref();
  Future checkUiEnterScore() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    changeUi = pref.getString(keyPref.keyChangeEnterScorePref) ?? "";
    setState(() {
      if(changeUi == "True"){
        setState(() {
          switchStatus = true;
          _isEnd = true;
        });
      }
      else{
        setState(() {
          switchStatus = false;
           _isEnd = false;
        });
      }
    });
  }
  @override
  void initState() {
    setState(() {
      checkUiEnterScore();
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      if(monthlocal.toString().length == 1){
        monthlocalCheck = "0$monthlocal";
      }
      else{
        monthlocalCheck = monthlocal.toString();
      }
    });
    // setState(() {  
    //   sub = Connectivity().onConnectivityChanged.listen((event) {
    //     setState(() {
    //       connection = (event != ConnectivityResult.none);
    //       if (connection == true) {
    //         if (isFirstLoaded == true) {
    //         }
    //       } else {
    //       }
    //     });
    //   });
    // });
      internetconnection = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      // whenevery connection status is changed.
      if (result == ConnectivityResult.none) {
        //there is no any connection
        setState(() {
          isoffline = false;
        });
      } else if (result == ConnectivityResult.mobile) {
        //connection is mobile data network
        setState(() {
          isoffline = true;
        });
      } else if (result == ConnectivityResult.wifi) {
        //connection is from wifi
        setState(() {
          isoffline = true;
        });
      }
    });
    super.initState();
    // setState(() {
    //   _isEnd = true;
    // });
    BlocProvider.of<CheckSwitchEnterScoreScreenBloc>(context).add(GetCheckSwitchEnterScoreEvent());
  }

  @override
  Widget build(BuildContext context) {
    isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: const Color.fromRGBO(243, 250, 255, 1),
      body: SafeArea(
        bottom: false,
        child: WillPopScope(
          onWillPop: () async{
            return false;
          },
          child: BlocListener<CheckSwitchEnterScoreScreenBloc, CheckSwitchEnterScoreScreenState>(
            listener:(context, state) {
              if(state is GetCheckSwitchEnterScoreLoaded){
                var data = state.checkEnterScoreSwitchScreen!.data;
                if(data!.appEnterScoreSwitchType == 1){
                  setState(() {
                    openSwitchScreen = true;
                  });
                }
              }
            },
            child: GestureDetector(
                onTap: () async {
                  setState(() {
                    FocusScope.of(context).unfocus();
                  });
                },
                child: BlocConsumer<GradingBloc, GradingState>(
                listener: (context, state) {
                  if(state is GetGradingDataLoaded){
                    var data = state.GradingList!.data;
                    var studentData = data!.studentsData;
                    var scoreInputDone = studentData!.where((element) => element.score ==null && element.absentExam == 0,).toList();
                    dataScore = data;
                    studentDataPost = studentData;
                    for(int i = 0;i<=data.studentsData!.length-1;i++){
                      if(startLoop == true){
                        if(studentData[i].score == null){
                          setState(() {
                            startLoop = false;
                            startIndex = i;
                          });
                        }
                        else{}
                      }
                      else{}
                    }
                    if(scoreInputDone.isEmpty){
                      setState(() {
                        _isEnd = true;
                      });
                    }
                    else{
                      setState(() {
                        if(changeUi == "True"){
                          setState(() {
                            _isEnd = true;
                          });
                        }
                        else{
                           setState(() {
                            _isEnd = false;
                          });
                        }
                      });
                    }
                  }
                },
                builder: (context, state) {
                  if (state is GetGradingDataLoading) {
                    return const Padding(
                      padding:  EdgeInsets.only(top: 0),
                      child:  GradingShimmerWidget(),
                    );
                  } else if (state is GetGradingDataLoaded) {
                    var data = state.GradingList!.data;
                    var studentData = data!.studentsData;
                    _progressStep = studentData!.where((element) => element.score != null).length;
                    isFirstLoaded = false;
              // ============== Not student condition ==================
                    return studentData.isEmpty? 
                    SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          HeaderGradingWidget(
                            onTapBack: (){
                              Navigator.of(context).pop();
                            },
                                subjectName: translate=="km"?data.subjectName.toString():data.subjectNameEn.toString(),
                                gradingname: data.className.toString() == null ||
                                        data.className.toString() == ""
                                    ? "- - -"
                                    : data.className.toString(),
                                academicYear: data.type==2?data.semester.toString() == null ||
                                        data.semester.toString() == ""
                                    ? "- - -"
                                    : data.semester.toString().tr(): "${"MONTH".tr()} ${checkMonth(int.parse(data.month.toString()))}",
                                acedemicMonth: data.isNoExam==0?"${data.examDate} - ${data.startTime}":data.isNoExam==1?"NO_EXAM".tr():"NOT_SET_EXAM_DAY".tr(),
                                colorIcon: Colorconstand.primaryColor,
                                colorTitle: Colorconstand.darkTextsRegular,
                              ),
                          const Expanded(child: SizedBox()),
                          EmptyWidget(
                                title: "NO_STUDENT".tr(),
                                subtitle: "EMPTHY_CONTENT_DES".tr(),
                              ),
                          const Expanded(child: SizedBox()),
                        ],
                      ),
                    )
                // ============== Not student condition End ==================
                    :Stack(
                      children: [
                        Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: Column(
                            children: [
                              //------Header---------
                              HeaderGradingWidget(
                              ///============= event pop set exam  ==============
                                onTapBack: (){
                                    if(data.studentsData![0].score!=null && checkScore == true){
                                      if(widget.isPrimary == true){
                                        if(widget.redirectFormScreen==3){
                                            BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"$daylocal/$monthlocalCheck/$yearchecklocal"));
                                          }
                                          if(widget.redirectFormScreen==1){
                                            BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString(),idClass: data.classId.toString()));                                                 
                                          }
                                      }
                                      else{
                                        if(widget.redirectFormScreen==3){
                                          BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"$daylocal/$monthlocalCheck/$yearchecklocal"));
                                        }
                                        if(widget.redirectFormScreen==1){
                                          if(widget.myclass==true){
                                            BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString()));
                                          }
                                          else{
                                            BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString(),classid: data.classId.toString()));
                                          }
                                        }
                                      }
                                    showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Dialog(
                                          backgroundColor: Colors.transparent,
                                          elevation: 5,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(18),
                                              color: Colorconstand.neutralWhite
                                            ),
                                            padding:const EdgeInsets.symmetric(vertical: 20,horizontal: 25),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Container(
                                                  padding:const EdgeInsets.all(10),
                                                  decoration:const BoxDecoration(
                                                    color: Colorconstand.alertsAwaitingText,
                                                    shape: BoxShape.circle
                                                  ),
                                                  alignment: Alignment.center,
                                                  child: Text("!",style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),),
                                                ),
                                                const SizedBox(height: 20,),
                                                Text("YOUR_SCORE_IS_SAVE".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.lightBulma),),
                                                const SizedBox(height: 25,),
                                                GestureDetector(
                                                  onTap: (){
                                                    Navigator.of(context).pop();
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    height: 45,
                                                    width: 150,
                                                    decoration: BoxDecoration(
                                                      color: Colorconstand.primaryColor,
                                                      borderRadius: BorderRadius.circular(12)
                                                    ),
                                                    child: Text("OK".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,))),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                    var api = PostGradingApi();
                                    api.postListStudentSubjectExam(
                                      classID: data.classId!,
                                      subjectID: data.subjectId!,
                                      term: data.semester,
                                      examDate: "",
                                      type: data.type!,
                                      month: data.month,
                                      listStudentScore: studentData,
                                    );
                                  }
                                  else{
                                    Navigator.of(context).pop();
                                  }
                                },
                                onTapsetExam: (){
                                  BlocProvider.of<MonthExamSetBloc>(context).add(MonthExamSet(idClass:data.classId.toString(), idsubject: data.subjectId.toString()));
                                  setState(() {
                                     Future.delayed(const Duration(milliseconds: 500),(){
                                        setState(() {
                                          showModalBottomSheet(
                                            isScrollControlled:true,
                                            shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(16.0),
                                                topRight: Radius.circular(16.0),
                                              ),
                                            ),
                                            context: context,
                                            builder: (context) {
                                              return PopMonthForSelect(isPrimary:widget.isPrimary!,studentDataScore: data.studentsData, date: data.examDate.toString(),typeSemesterOrMonth: data.type!,classId: data.classId.toString(),subjectId: data.subjectId.toString(),activeMySchedule: widget.myclass!,month: data.month,semester: data.semester,);
                                            },
                                          );
                                        });
                                      });
                                      
                                  });
                                  // BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                                  //     idClass:  data.classId.toString(), 
                                  //     subjectId: data.subjectId.toString(), 
                                  //     type: data.type.toString(), 
                                  //     month:  data.month.toString(), 
                                  //     semester:  data.semester.toString(),
                                  //     examDate: ""),
                                  // );
                                },
                                onTapNoExam: (){
                                  setState(() {
                                    data.isNoExam = 1;
                                    SetExamApi().setNoExamApi(idClass: data.classId.toString(), idsubject:  data.subjectId.toString(), semester: data.semester.toString(), isNoExam: "1", type:data.type.toString(),  month: data.month.toString(),);
                                   // BlocProvider.of<SetExamBloc>(context).add(SetNoExam(idClass: data.classId.toString(), idsubject:  data.subjectId.toString(), semester: data.semester.toString(), isNoExam: "1", type:data.type.toString(),  month: data.month.toString(),));
                                  });
                                },
                                onTapNoExamDate: (){
                                  setState(() {
                                    data.isNoExam = 2;
                                    SetExamApi().setNoExamApi(idClass: data.classId.toString(), idsubject:  data.subjectId.toString(), semester: data.semester.toString(), isNoExam: "2", type:data.type.toString(),  month: data.month.toString(),);
                                  });
                                },
                          ///============= end event pop set exam  ==============
                                colorIcon:data.isNoExam == 0?Colorconstand.primaryColor:Colorconstand.alertsDecline,
                                colorTitle:data.isNoExam == 0?Colorconstand.darkTextsRegular:Colorconstand.alertsDecline,
                                subjectName: translate == "km"
                                    ? "${data.subjectName}"
                                    : "${data.subjectNameEn}",
                                gradingname: data.className.toString() == null ||
                                        data.className.toString() == ""
                                    ? "- - -"
                                    : data.className.toString(),
                                academicYear: data.type==2?data.semester.toString() == null ||
                                        data.semester.toString() == ""
                                    ? "- - -"
                                    : data.semester.toString().tr(): "${"MONTH".tr()} ${checkMonth(int.parse(data.month.toString()))}",
                                acedemicMonth: data.isNoExam==0?"${data.examDate} - ${data.startTime}":data.isNoExam==1?"NO_EXAM".tr():"NOT_SET_EXAM_DAY".tr(),
                              ),
                              
                              studentData.isEmpty?Container(height: 100,):Padding(
                                padding: const EdgeInsets.only(top: 10,bottom: 0,left: 10,right: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center, 
                                  children: [
                                    SvgPicture.asset(ImageAssets.profile_2users),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 5),
                                        child: StepProgressIndicator(
                                          totalSteps: studentData.length,
                                          currentStep: _progressStep,
                                          size: 12,
                                          padding: 0.5,
                                          roundedEdges: const Radius.circular(20),
                                          unselectedColor: Colorconstand.lightGoten,
                                          selectedGradientColor: const LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                            colors: [
                                              Colorconstand.mainColorSecondary,
                                              Colorconstand.primaryColor
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "${studentData.where((element) => element.score!=null).length}/${studentData.length} ${"PEOPLE".tr()}",
                                      style: ThemsConstands.button_semibold_16.copyWith(
                                        color: Colorconstand.primaryColor,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              openSwitchScreen == false?Container(height: 10,):switchButton (),
                              AnimatedContainer(
                                duration: const Duration(milliseconds: 500),
                                height: isoffline?0:80,
                                child: Container(
                                  padding: const EdgeInsets.only(left: 20,right: 20,top: 5),
                                  width: MediaQuery.of(context).size.width,
                                  decoration: const BoxDecoration(color: Colorconstand.alertsDecline),
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text("NOCONNECTIVITY".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),),
                                        const SizedBox(height: 10,),
                                        Text("STORE_TASK".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralWhite),),
                                      ],
                                    ),
                                  ),),
                              ),
                              //-------------------------------------------------------------------
                              studentData.isEmpty?Container(
                                child:Column(
                                  children: [
                                    Expanded(child: Container()),
                                    ScheduleEmptyWidget(
                                        title: "NO_STUDENT".tr(),
                                        subTitle: "EMPTHY_CONTENT_DES".tr(),
                                      ),
                                    Expanded(child: Container()),
                                  ],
                                ),
                              ):bodyWidget(
                                studentData: studentData,
                                data: data,
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const SizedBox(height: 30,),
                          HeaderGradingWidget(
                            onTapBack: (){
                              Navigator.of(context).pop();
                            },
                                subjectName: "- - -",
                                gradingname: "- - -",
                                academicYear: "- - -",
                                acedemicMonth: "- - -",
                                colorIcon: Colorconstand.primaryColor,
                                colorTitle: Colorconstand.darkTextsRegular,
                              ),
                          Expanded(
                            child: EmptyWidget(
                                  title: "WE_DETECT".tr(),
                                  subtitle: "WE_DETECT_DES".tr(),
                                ),
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  } 
  Widget switchButton (){
    return Container(
      margin:const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Transform.scale(
            scale: 0.8,
            child: CupertinoSwitch(
              onChanged: (value) {
                setState(() {
                  switchStatus = value;
                    if(value == true){
                      setState(() {
                        _isEnd = true;
                        saveStoragePref.saveChangeUIEnterScore(keyChangeUiEnterScore: "True");
                      });
                    }
                    else{
                      setState(() {
                        _isEnd = false;
                        saveStoragePref.saveChangeUIEnterScore(keyChangeUiEnterScore: "false");
                      });
                    }
                });
              },
              activeColor: Colorconstand.primaryColor, 
              value: switchStatus,
            ),
          ),
          Expanded(
            child: Container(
              margin:const EdgeInsets.only(left: 8),
              child: Text(switchStatus==true?"SWITCH_ENTER_SCORE_TO_STUDENT_LIST".tr():"SWITCH_ENTER_SCORE_TO_TABLE".tr(),style: ThemsConstands.headline_5_medium_16,),
            ),
          )
        ],
      ),
    );
  }

  Widget bodyWidget( {required List<StudentsData> studentData, required GradingData data,
      VoidCallback? onTapPreview}) {
    return StatefulBuilder(
      builder: (context, setState) {
        final translate = context.locale.toString();
        var scoreInputDone = studentData.where((element) => element.score ==null && element.absentExam == 0,).toList();
        return Expanded(
          child: Container(
            decoration: const BoxDecoration(
                    color: Colorconstand.neutralWhite,
                    boxShadow: [
                      BoxShadow(
                        color: Colorconstand.neutralGrey,
                        offset: Offset(0, 5),
                        blurRadius: 20,
                      )
                    ],
                  ),
            child: Stack(
              children: [
                _showScrollGuide == true && isScrolling == true?Positioned(
                  left: 120,
                  bottom: 100,
                  right: 120,
                  child: Container(
                    child: Lottie.asset('assets/images/gifs/animation_llerns45.json',),
                  ),
                ):Container(height: 0,),
                NotificationListener(
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView.separated(
                          controller: listScrollController,
                          padding: const EdgeInsets.only(top: 0,left: 0,right: 0,bottom: 60),
                          itemCount: studentData.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            controller.add(TextEditingController(text: ""));
                            controller1.add(TextEditingController(text: ""));
                            controller2.add(TextEditingController(text: ""));
                           
                          return switchStatus == true? 
                            Column(
                              children: [
                                Container(
                                  padding:const EdgeInsets.symmetric(vertical: 12,horizontal: 8),
                                  height: 75,
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            children: [
                                              Container(margin:const EdgeInsets.only(right: 5),child: Text("${index+1}",style: ThemsConstands.headline_5_semibold_16,),),
                                              Container(
                                                alignment: Alignment.center,
                                                child: Stack(
                                                  children: [
                                                    CircleAvatar(
                                                      radius: 30.0,
                                                      backgroundImage: NetworkImage(
                                                          studentData[index]
                                                              .profileMedia!
                                                              .fileShow!),
                                                      backgroundColor: Colors.transparent,
                                                    ),
                                                    studentData[index].absentExam == 1
                                                        ? Positioned(
                                                            bottom: 0,
                                                            right: 0,
                                                            child: SizedBox(
                                                              height: 18.13,
                                                              child: SvgPicture.asset(
                                                                ImageAssets.absent,
                                                              ),
                                                            ),
                                                          )
                                                        : Container()
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 8,
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                    translate=="km"?studentData[index].studentName.toString():studentData[index].studentNameEn.toString()==" "?"NONAME".tr():studentData[index].studentNameEn,
                                                      style: ThemsConstands
                                                          .headline_4_medium_18,
                                                    ),
                                                    Text(
                                                      studentData[index].gender.toString()=="1"?"${"GENDER".tr()}: ${"MALE".tr()}":"${"GENDER".tr()}: ${"FEMALE".tr()}",
                                                        style: ThemsConstands
                                                            .caption_regular_12,
                                                      ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5,),
                                      GestureDetector(
                                        onTap: (){
                                          checkScore = true;
                                          if(studentData[index].absentExam == 1){
                                            setState(() {
                                              studentData[index].absentExam = 0;
                                              controller[index].text = studentData[index].score == null?"":studentData[index].score.toString();
                                              if(studentData[index].childrenSubject!.length == 2){
                                                studentData[index].childrenSubject![0].score = 0;
                                                    studentData[index].childrenSubject![1].score = 0;
                                                controller1[index].text = studentData[index].childrenSubject![0].score == null?"":studentData[index].childrenSubject![0].score.toString();
                                                controller2[index].text =studentData[index].childrenSubject![1].score == null?"":studentData[index].childrenSubject![1].score.toString();
                                              }
                                            },);
                                          }
                                          else{
                                            setState(() {
                                              studentData[index].absentExam = 1;
                                              controller[index].text = "A";
                                              studentData[index].score = 0;
                                              if(studentData[index].childrenSubject!.length==2){
                                                  if(studentData[index].absentExam == 1){
                                                    controller1[index].text = "A";
                                                    controller2[index].text = "A";
                                                    studentData[index].childrenSubject![0].score = 0;
                                                    studentData[index].childrenSubject![1].score = 0;
                                                  }
                                                  else{
                                                    controller1[index].text = studentData[index].childrenSubject![0].score.toString()=="null"||studentData[index].childrenSubject![0].score==""?"":studentData[index].childrenSubject![0].score.toString();
                                                    controller2[index].text = studentData[index].childrenSubject![1].score.toString()=="null"||studentData[index].childrenSubject![1].score==""?"":studentData[index].childrenSubject![1].score.toString();
                                                  }
                                                }     
                                            },);
                                          }
                                        },
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text(studentData[index].absentExam == 1?"ABSENT".tr():"PRESENT".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color:  studentData[index].absentExam == 1? Colorconstand.alertsDecline:Colorconstand.primaryColor),),
                                              const SizedBox(height: 5,),
                                              studentData[index].absentExam == 1? SvgPicture.asset(ImageAssets.absent,width: 19,): const Icon(Icons.check_circle_outline_rounded,size: 23,color:Colorconstand.primaryColor,)
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5,),
                                      Container(
                                        width: 110,
                                        child: TextFormField(
                                          onTap: () {
                                            setState(() {
                                              checkScore = true;
                                              controller[index].text = studentData[index].score == null?"":studentData[index].score.toString();
                                            },);
                                          },
                                          onChanged: (value) {
                                            setState(() {
                                              if(value == ""){
                                                setState(() {
                                                  studentData[index].score = null;
                                                },);
                                              }
                                              else{
                                                if (double.parse(value) <= studentData[index].subjectScoreMax!) {
                                                  setState(() {
                                                    studentData[index].score = value;
                                                    if(studentData[index].childrenSubject!.length==2){
                                                      setState(() {
                                                        studentData[index].childrenSubject![0].score = null;
                                                         studentData[index].childrenSubject![1].score = null;
                                                      },);
                                                    }
                                                  });
                                                } else {
                                                  setState(() {
                                                    studentData[index].score = null;
                                                    controller[index].text = "";
                                                  },);
                                                    
                                                }
                                              } 
                                            },);
                                          },
                                          controller: controller[index],
                                         // controller: TextEditingController(text: studentData[index].absentExam.toString() == "1"? "A":studentData[index].score == null || studentData[index].score == "" ?"".tr():studentData[index].score.toString()),
                                          enabled: studentData[index].absentExam.toString()=="1"? false: studentData[index].childrenSubject!.length == 2?((studentData[index].childrenSubject![0].score==null || studentData[index].childrenSubject![0].score==0) && (studentData[index].childrenSubject![1].score==null || studentData[index].childrenSubject![1].score==0))? true:false:true,
                                          maxLength: studentData[index].subjectScoreMax.toString().length+2,
                                          cursorHeight: 20,
                                          style: ThemsConstands.headline3_semibold_20.copyWith(color:studentData[index].absentExam.toString() == "1"?Colorconstand.alertsDecline: Colorconstand.neutralDarkGrey),
                                          textInputAction: TextInputAction.next,
                                          keyboardType:const TextInputType.numberWithOptions(decimal: true),
                                          textAlignVertical: TextAlignVertical.center,
                                          textAlign: TextAlign.center,
                                          decoration: InputDecoration(
                                            contentPadding:const EdgeInsets.symmetric(vertical: 10,horizontal: 5),
                                            counterText: "",
                                            focusedBorder:const OutlineInputBorder(
                                                borderRadius:  BorderRadius.all(Radius.circular(6)),
                                                borderSide: BorderSide(width: 1,color:Colorconstand.primaryColor),
                                              ),
                                              suffixText:controller[index].text == ""?"":studentData[index].absentExam.toString()=="1"?"":"/${studentData[index].subjectScoreMax}",
                                              suffixStyle:ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralGrey),
                                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0),borderSide:const BorderSide(width: 1)),
                                              filled: true,
                                              hintText: studentData[index].absentExam.toString()=="1"
                                                  ? "A".tr()
                                                  : studentData[index].score == null?"SCORING".tr():studentData[index].score.toString(),
                                              hintStyle:studentData[index].absentExam == 0 && studentData[index].score != null ?ThemsConstands.headline3_semibold_20.copyWith(color:Colorconstand.neutralDarkGrey):ThemsConstands.headline_5_semibold_16.copyWith(color: studentData[index].absentExam.toString()=="1"? Colorconstand.alertsNotifications :Colorconstand.neutralGrey),
                                              fillColor: studentData[index].absentExam.toString()=="1"? Colorconstand.alertsDeclinedBg: Colors.white70),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                studentData[index].childrenSubject!.length != 2? Container()
                                :((studentData[index].childrenSubject![0].score == null||studentData[index].childrenSubject![0].score == 0) && (studentData[index].childrenSubject![1].score == null||studentData[index].childrenSubject![1].score == 0)) && (studentData[index].score !=null)?Container():Container(
                                  child: Column(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          color: Colorconstand.mainColorUnderlayer.withOpacity(0.2),
                                          //borderRadius: BorderRadius.circular(8)
                                        ),
                                        //margin:const EdgeInsets.symmetric(vertical: 3),
                                        padding:const EdgeInsets.symmetric(vertical: 5,horizontal: 8),
                                        height: 60,
                                        child: Row(
                                          children: [
                                            Container(width: 65,),
                                            Expanded(
                                              child: Text(
                                              translate=="km"?studentData[index].childrenSubject![0].name.toString():studentData[index].childrenSubject![0].nameEn.toString()==" "?"NONAME".tr():studentData[index].studentNameEn,
                                                style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),
                                              ),
                                            ),
                                            Container(
                                              width: 110,
                                              child: TextFormField(
                                                onTap: (){
                                                  setState(() {
                                                    checkScore = true;
                                                    controller1[index].text = studentData[index].childrenSubject![0].score == null?"":studentData[index].childrenSubject![0].score.toString();
                                                  });
                                                },
                                                onChanged: (value) {
                                                  if(value == ""){
                                                    setState(() {
                                                      studentData[index].childrenSubject![0].score = null; 
                                                      studentData[index].childrenSubject![1].score == null || studentData[index].childrenSubject![1].score==0?studentData[index].score=null:studentData[index].score = studentData[index].childrenSubject![1].score;
                                                      
                                                    },);
                                                  }
                                                  else{
                                                    setState(() {
                                                      if (double.parse(value) <= studentData[index].childrenSubject![0].maxScore!) {
                                                        setState(() {
                                                          studentData[index].childrenSubject![0].score = value;
                                                          if(studentData[index].childrenSubject![1].score==null){
                                                            setState(() {
                                                              studentData[index].score = studentData[index].childrenSubject![0].score;
                                                            },);
                                                          }
                                                          else{
                                                            setState(() {
                                                              studentData[index].score = (double.parse(studentData[index].childrenSubject![1].score.toString()) + double.parse(studentData[index].childrenSubject![0].score.toString())).toString().replaceAll(regex, "");
                                                            },);
                                                          }
                                                          
                                                        },);
                                                      } else {
                                                        setState(() {
                                                          controller1[index].text = "";
                                                          studentData[index].childrenSubject![0].score = null;
                                                          studentData[index].childrenSubject![1].score == null || studentData[index].childrenSubject![1].score ==0?studentData[index].score=null:studentData[index].score = studentData[index].childrenSubject![1].score;
                                                          
                                                        },);
                                                          
                                                      }
                                                    },);
                                                  }
                                                },
                                                controller: controller1[index],
                                                //controller: TextEditingController(text: studentData[index].absentExam.toString() == "1"? "A":studentData[index].childrenSubject![0].score == null || studentData[index].childrenSubject![0].score == "" ?"".tr():studentData[index].childrenSubject![0].score.toString()),
                                                enabled: studentData[index].absentExam.toString()=="1"? false: true,
                                                maxLength: studentData[index].subjectScoreMax.toString().length+2,
                                                cursorHeight: 20,
                                                style: ThemsConstands.headline3_semibold_20.copyWith(color:studentData[index].absentExam.toString() == "1"?Colorconstand.alertsDecline: Colorconstand.neutralDarkGrey),
                                                keyboardType:const TextInputType.numberWithOptions(decimal: true),
                                                textAlignVertical: TextAlignVertical.center,
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  contentPadding:const EdgeInsets.symmetric(vertical: 10,horizontal: 5),
                                                  counterText: "",
                                                  focusedBorder:const OutlineInputBorder(
                                                      borderRadius:  BorderRadius.all(Radius.circular(6)),
                                                      borderSide: BorderSide(width: 1,color:Colorconstand.primaryColor),
                                                    ),
                                                    suffixText:controller1[index].text==""?"":studentData[index].absentExam.toString()=="1"?"":"/${studentData[index].childrenSubject![0].maxScore.toString()}",
                                                    suffixStyle:ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralGrey),
                                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0),borderSide:const BorderSide(width: 1)),
                                                    filled: true,
                                                    hintText: studentData[index].absentExam.toString()=="1"
                                                      ? "A".tr()
                                                      : studentData[index].childrenSubject![0].score == null ?"SCORING".tr(): studentData[index].childrenSubject![0].score.toString(),
                                                    hintStyle:studentData[index].absentExam == 0 && studentData[index].childrenSubject![0].score != null ?ThemsConstands.headline3_semibold_20.copyWith(color:Colorconstand.neutralDarkGrey):ThemsConstands.headline_5_semibold_16.copyWith(color: studentData[index].absentExam.toString()=="1"? Colorconstand.alertsNotifications :Colorconstand.neutralGrey),
                                                    fillColor: studentData[index].absentExam.toString()=="1"? Colorconstand.alertsDeclinedBg: Colors.white70),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          color: Colorconstand.mainColorUnderlayer.withOpacity(0.2),
                                          //borderRadius: BorderRadius.circular(8)
                                        ),
                                        //margin:const EdgeInsets.symmetric(vertical: 3),
                                        padding:const EdgeInsets.symmetric(vertical: 5,horizontal: 8),
                                        height: 60,
                                        child: Row(
                                          children: [
                                            Container(width: 65,),
                                            Expanded(
                                              child: Text(
                                              translate=="km"?studentData[index].childrenSubject![1].name.toString():studentData[index].childrenSubject![1].nameEn.toString()==" "?"NONAME".tr():studentData[index].studentNameEn,
                                                style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),
                                              ),
                                            ),
                                            Container(
                                              width: 110,
                                              child: TextFormField(
                                                onTap: (){
                                                  setState(() {
                                                    checkScore = true;
                                                    controller2[index].text = studentData[index].childrenSubject![1].score == null?"":studentData[index].childrenSubject![1].score.toString();
                                                  });
                                                },
                                                onChanged: (value) {
                                                  if(value == ""){
                                                    setState(() {
                                                      studentData[index].childrenSubject![1].score = null;
                                                      studentData[index].childrenSubject![0].score == null||studentData[index].childrenSubject![0].score ==0?studentData[index].score=null:studentData[index].score = studentData[index].childrenSubject![0].score;
                                                      
                                                    },);
                                                  }
                                                  else{
                                                    if (double.parse(value) <= studentData[index].childrenSubject![1].maxScore!) {
                                                      setState(() {
                                                        studentData[index].childrenSubject![1].score = value;
                                                        if(studentData[index].childrenSubject![0].score==null){
                                                          setState(() {
                                                            studentData[index].score = studentData[index].childrenSubject![1].score;
                                                          },);
                                                        }
                                                        else{
                                                          setState(() {
                                                            studentData[index].score = (double.parse(studentData[index].childrenSubject![1].score.toString()) + double.parse(studentData[index].childrenSubject![0].score.toString())).toString().replaceAll(regex, "");
                                                          },);
                                                        }
                                                        
                                                      },);
                                                    } else {
                                                      setState(() {
                                                        controller2[index].text = "";
                                                        studentData[index].childrenSubject![1].score = null;
                                                        studentData[index].childrenSubject![0].score == null?studentData[index].score=null:studentData[index].score = studentData[index].childrenSubject![0].score;
                                                        
                                                      },);
                                                        
                                                    }
                                                  }
                                                },
                                               controller: controller2[index],
                                                //controller: TextEditingController(text: studentData[index].absentExam.toString() == "1"? "A":studentData[index].childrenSubject![1].score == null || studentData[index].childrenSubject![1].score == "" ?"".tr():studentData[index].childrenSubject![1].score.toString()),
                                                enabled: studentData[index].absentExam.toString()=="1"? false: true,
                                                maxLength: studentData[index].subjectScoreMax.toString().length+2,
                                                cursorHeight: 20,
                                                style: ThemsConstands.headline3_semibold_20.copyWith(color:studentData[index].absentExam.toString() == "1"?Colorconstand.alertsDecline: Colorconstand.neutralDarkGrey),
                                                textInputAction: TextInputAction.next,
                                                keyboardType:const TextInputType.numberWithOptions(decimal: true),
                                                textAlignVertical: TextAlignVertical.center,
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  contentPadding:const EdgeInsets.symmetric(vertical: 10,horizontal: 5),
                                                  counterText: "",
                                                  focusedBorder:const OutlineInputBorder(
                                                      borderRadius:  BorderRadius.all(Radius.circular(6)),
                                                      borderSide: BorderSide(width: 1,color:Colorconstand.primaryColor),
                                                    ),
                                                    suffixText:controller1[index].text==""?"":studentData[index].absentExam.toString()=="1"?"":"/${studentData[index].childrenSubject![1].maxScore.toString()}",
                                                    suffixStyle:ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralGrey),
                                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0),borderSide:const BorderSide(width: 1)),
                                                    filled: true,
                                                    hintText: studentData[index].absentExam.toString()=="1"
                                                      ? "A".tr()
                                                      : studentData[index].childrenSubject![1].score == null?"SCORING".tr():studentData[index].childrenSubject![1].score.toString(),
                                                    hintStyle:studentData[index].absentExam == 0 && studentData[index].childrenSubject![1].score != null ?ThemsConstands.headline3_semibold_20.copyWith(color:Colorconstand.neutralDarkGrey):ThemsConstands.headline_5_semibold_16.copyWith(color: studentData[index].absentExam.toString()=="1"? Colorconstand.alertsNotifications :Colorconstand.neutralGrey),
                                                    fillColor: studentData[index].absentExam.toString()=="1"? Colorconstand.alertsDeclinedBg: Colors.white70),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                            :InkWell(
                              onTap: () {
                                setState(() {
                                  checkScore = true;
                                },);
                                inputScoreByIndexPopUpWidget(context, data, studentData,index, studentData.where((element) => element.score!=null).length == index +1 ?false:studentData[index].score==null ||  studentData[index + index == studentData.length - 1?0:1].score==null?false:true)
                                  .then((value) {
                                    setState(() {
                                      if(studentData.where((element) => element.score!=null).isNotEmpty){
                                        setState(() {
                                          _isEnd = true;
                                        },);                                      
                                      }
                                      else{
                                         setState(() {
                                          _isEnd = false;
                                        },); 
                                      }
                                    },);
                                });
                              },
                              child: Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                                  child: Row(
                                    mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(margin:const EdgeInsets.only(right: 5),child: Text("${index+1}",style: ThemsConstands.headline_5_semibold_16,),),
                                      Stack(
                                        children: [
                                          CircleAvatar(
                                            radius: 30.0,
                                            backgroundImage: NetworkImage(
                                                studentData[index]
                                                    .profileMedia!
                                                    .fileShow!),
                                            backgroundColor: Colors.transparent,
                                          ),
                                          studentData[index].absentExam == 1
                                              ? Positioned(
                                                  bottom: 0,
                                                  right: 0,
                                                  child: SizedBox(
                                                    height: 18.13,
                                                    child: SvgPicture.asset(
                                                      ImageAssets.absent,
                                                    ),
                                                  ),
                                                )
                                              : Container()
                                        ],
                                      ),
                                      const SizedBox(
                                        width: 12,
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                            translate=="km"?studentData[index].studentName.toString():studentData[index].studentNameEn.toString()==" "?"NONAME".tr():studentData[index].studentNameEn,
                                              style: ThemsConstands
                                                  .headline_4_medium_18,
                                            ),
                                            Text(
                                               studentData[index].gender.toString()=="1"?"${"GENDER".tr()}: ${"MALE".tr()}":"${"GENDER".tr()}: ${"FEMALE".tr()}",
                                                style: ThemsConstands
                                                    .caption_regular_12,
                                              ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        height: 46,
                                        width: 96,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color:studentData[index].absentExam == 1?Colorconstand.alertsDecline : studentData[index].letterGrade == null && studentData[index].score == null || studentData[index].letterGrade != null && studentData[index].score == null
                                                  ? Colorconstand.neutralGrey
                                                  : studentData[index].letterGrade ==  "F" || double.parse(studentData[index].score.toString()) < (studentData[index].subjectScoreMax/2) ? Colorconstand.alertsDecline
                                                      : studentData[index].letterGrade == "A" || double.parse(studentData[index].score.toString()) >= (studentData[index].subjectScoreMax/1.25)
                                                          ? Colorconstand.alertsPositive
                                                          : Colorconstand.alertsAwaitingText),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(6),
                                          ),
                                          color:studentData[index].absentExam == 1?Colorconstand.alertsDeclinedBg : studentData[index].letterGrade == null && studentData[index].score == null || studentData[index].letterGrade != null && studentData[index].score == null
                                              ? Colorconstand .neutralSecondBackground
                                              :  studentData[index].letterGrade =="F" || double.parse(studentData[index].score.toString()) < (studentData[index].subjectScoreMax/2)
                                              ? Colorconstand.alertsDeclinedBg
                                              : studentData[index].letterGrade =="A" || double.parse(studentData[index].score.toString()) >= (studentData[index].subjectScoreMax/1.25)
                                              ? Colorconstand .alertsPositiveBg
                                              :Colorconstand.alertsAwaitingBg,
                                        ),
                                        child: Center(
                                            child: Text(studentData[index].score == null && studentData[index].absentExam.toString() != "1"
                                              ? "SCORING".tr() : studentData[index].absentExam.toString() == "1" ||
                                                      studentData[index].absentExam == null
                                                  ? "A".tr()
                                                  : "${studentData[index].score.toString()}/${studentData[index].subjectScoreMax}",textAlign: TextAlign.center,
                                          style: ThemsConstands
                                              .headline3_semibold_20
                                              .copyWith(
                                                  color: studentData[index].absentExam == 1 ?Colorconstand.alertsDecline : studentData[index].letterGrade == null && studentData[index].score == null || studentData[index].letterGrade != null && studentData[index].score == null
                                                      ? Colorconstand.lightBlack.withOpacity(0.2)
                                                      : studentData[index] .letterGrade == "F" || double.parse(studentData[index].score.toString()) < (studentData[index].subjectScoreMax/2)
                                                          ? Colorconstand.alertsDecline
                                                          : studentData[index].letterGrade == "A" || double.parse(studentData[index].score.toString()) >= (studentData[index].subjectScoreMax/1.25)
                                                              ? Colorconstand .alertsPositive
                                                              : Colorconstand.alertsAwaitingText),
                                        )),
                                      )
                                    ],
                                  )),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return const Divider(
                              height: 1,
                              thickness: 0.5,
                            );
                          },
                        ),
                      ),
                      Container(
                        height: 30,
                      )
                    ],
                  ),
                  onNotification: (t) {
                      if(_showScrollGuide == true){
                        listScrollController.position.isScrollingNotifier.addListener(() { 
                          if(!listScrollController.position.isScrollingNotifier.value) {
                            setState(() {
                              isScrolling = true;
                            });
                            
                          } else {
                            setState(() {
                              isScrolling = false;
                            });
                          
                          }
                        });
                      }
                    if (t is ScrollEndNotification) {
                      if (listScrollController.position.pixels > 0 &&
                          listScrollController.position.atEdge && _isEnd == true) {
                        setState(() {
                          _isLast = true;
                          _showScrollGuide = false;
                        });
                      }
                      return true;
                    } else {
                      return false;
                    }
                  },
                ),
                isKeyboard == true? Container(): Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: AnimatedContainer(
                    duration:const Duration(milliseconds: 400),
                    decoration:const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colorconstand.lightGoten,
                          blurRadius: 25,
                        )
                      ],
                    ),
                    height: _isEnd == true && _isLast == true?80:0,
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                      left: 20,right: 20,bottom: 23
                      //vertical: 23
                    ),
                    child: MaterialButton(
                            height: 57.18,
                            onPressed:isoffline? () async {
                              setState((){
                                postLoading = true;
                              });
                              var api = PostGradingApi();
                              api.postListStudentSubjectExam(
                                classID: data.classId!,
                                subjectID: data.subjectId!,
                                term: data.semester,
                                examDate: "",
                                type: data.type!,
                                month: data.month,
                                listStudentScore: studentData,
                              ).then((value) async{
                                showModalBottomSheet(
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
                                  ),
                                  isScrollControlled: false,
                                  context: context,
                                  builder: (BuildContext context) {
                                    BlocProvider.of<ViewImageBloc>(context).add(GetViewImageEvent(data.id!.toString()),);
                                    return AttachFileBottomSheetWidget(isInstructor: widget.isInstructor, classID: data.classId!,subjectID: data.subjectId!,month: data.month,semester: data.semester,type: data.type!,id: data.id!,);
                                  }).then((value) {
                                    setState((){
                                      BlocProvider.of<ImageSelectImageBloc>(context).listImage =[];
                                      BlocProvider.of<ImageSelectImageBloc>(context).removeAllFile();
                                    });
                                  });
                                      // ==========  ==== Refesh Event =======================================
                                setState(() {
                                  postLoading = false;

                                  if(widget.isPrimary == true){
                                    if(widget.redirectFormScreen==3){
                                        BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"$daylocal/$monthlocalCheck/$yearchecklocal"));
                                      }
                                      if(widget.redirectFormScreen==1){
                                        BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString(),idClass: data.classId.toString()));                                                 
                                      }
                                  }
                                  else{
                                      if(widget.redirectFormScreen==3){
                                        BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"$daylocal/$monthlocalCheck/$yearchecklocal"));
                                      }
                                      if(widget.redirectFormScreen==1){
                                        if(widget.myclass==true){
                                          BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString()));
                                        }
                                        else{
                                          BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString(),classid: data.classId.toString()));
                                        }
                                      }
                                  }
                                  // if(widget.redirectFormScreen==2){
                                  //   if(widget.myclass==true){
                                  //     BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString()));
                                  //     BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: data.examDate));
                                  //   }
                                  //   else{
                                  //     BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: data.examDate,classid: data.classId.toString()));
                                  //     BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString(),classid: data.classId.toString()));
                                  //   }
                                  // }
                                },);
                                  // =============== Refesh Event =======================================
                  
                                  if(widget.isInstructor==true){
                                   ApprovedApi().postApproved(classId: data.classId, examObjectId: data.id, isApprove: 1, discription: "");
                                  }
                                SharedPreferences pref = await SharedPreferences.getInstance();
                                pref.remove("Data Score");
                                pref.remove("Instructor");
                              }).onError((error, stackTrace) {
                                setState(() {
                                  postLoading = false;
                                  showErrorDialog((
                                  ) {
                                    Navigator.of(context).pop();
                                  }, context,title:"FAIL".tr(),description: "SAVE_SCORE_FAIL".tr());
                                },);
                              });
                              PostActionTeacherApi().actionTeacherApi(feature: "SCORE" ,keyFeature: "INPUT_SCORE");
                            }:() {
                              saveDataInLocal(json.encode(data));
                              saveInstructor(widget.isInstructor?"1":"0");
                              Navigator.pop(context);
                            },
                            color: Colorconstand.primaryColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.5)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                isoffline? SvgPicture.asset(
                                  ImageAssets.attach_circlle,
                                  color: Colorconstand.lightGohan,
                                ):const Icon(Icons.save_alt_outlined,color: Colorconstand.neutralWhite,),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  isoffline? "ADDATTACHMENT".tr():"SAVE".tr(),
                                  style: ThemsConstands.button_semibold_16
                                      .copyWith(
                                          color: Colorconstand.lightGoten),
                                ),
                                const SizedBox(
                                  width: 12,
                                ),
                              ],
                            ),
                          ),
                  
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: AnimatedContainer(
                    duration:const Duration(milliseconds: 200),
                    decoration:const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colorconstand.lightGoten,
                          blurRadius: 25,
                        )
                      ],
                    ),
                    height:_isEnd == false && scoreInputDone.isNotEmpty?80:0,
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                      left: 20,right: 20,bottom: 23
                      //vertical: 23
                    ),
                    child: MaterialButton(
                            height: 57.18,
                            onPressed: () {
                              setState(() {
                                checkScore = true;
                              },);
                              inputScoreByIndexPopUpWidget(context, data, studentData,startIndex, studentData.where((element) => element.score!=null).length == startIndex +1 ?false:studentData[startIndex].score==null || studentData[startIndex+1].score==null?false:true)
                                .then((value) {
                                  setState(() {
                                    if(studentData.where((element) => element.score!=null).isNotEmpty){
                                      setState(() {
                                        _isEnd = true;
                                      },);                                      
                                    }
                                    else{
                                        setState(() {
                                        _isEnd = false;
                                      },); 
                                    }
                                  },);
                              });                          },
                            color: Colorconstand.primaryColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.5)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                   startIndex == 0?"STARTINPUT".tr():"${"CONTINUE".tr()}${translate=="km"?"":" "}${"SCORING".tr()}",
                                  style: ThemsConstands.button_semibold_16
                                      .copyWith(color: Colorconstand.lightGoten),
                                ),
                                const SizedBox(
                                  width: 12,
                                ),
                                SvgPicture.asset(ImageAssets.arrow_right)
                              ],
                            ),
                          )
                  ),
                ),
                isKeyboard == true? Container():Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: AnimatedContainer(
                    duration:const Duration(milliseconds: 200),
                    decoration:const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colorconstand.lightGoten,
                          blurRadius: 25,
                        )
                      ],
                    ),
                    height: _isEnd == true && _isLast == false&& studentData.where((element) => element.score!=null).isNotEmpty?80:switchStatus==true && _isLast==false?80:0,
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                      left: 20,right: 20,bottom: 23
                      //vertical: 23
                    ),
                    child: Center(
                      child: Text(
                        switchStatus==false?"MAKESURESCORING".tr():"MAKESURESCORINGMENUE".tr(),
                        style: ThemsConstands.headline_5_semibold_16
                            .copyWith(
                                color: Colorconstand.primaryColor),textAlign: TextAlign.center,
                      ),
                    )
                  ),
                ),
                
                postLoading == false?Container():Positioned(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colorconstand.neutralDarkGrey.withOpacity(0.4),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "SEND_SCORE".tr(),
                            style: ThemsConstands
                                .headline_2_semibold_24
                                .copyWith(color: Colors.white),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "IN_PROGRESS_SEND_SCORE".tr(),
                            textAlign: TextAlign.center,
                            style: ThemsConstands.headline_5_medium_16
                                .copyWith(color: Colors.white),
                          ),
                          const SizedBox(height: 20,),
                          Container(
                            height: 30,
                            width: 30,
                            child:const CircularProgressIndicator(color: Colorconstand.primaryColor),
                          )
                        ],
                      ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
  inputScoreByIndexPopUpWidget(
      BuildContext context, GradingData data, List<StudentsData> studentData,int index,bool edit) {
    return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
        ),
        isScrollControlled: true,
        enableDrag: false,
        context: context,
        builder: (BuildContext context) {
          return GradingBottomSheetWidget(
            progressStep: index+1,
            isEnd: _isEnd,
            data: studentData,
            edit: edit,
            closeSaveScore: (){
              setState(() {
                Navigator.of(context).pop();
                var api = PostGradingApi();
                api.postListStudentSubjectExam(
                  classID: data.classId!,
                  subjectID: data.subjectId!,
                  term: data.semester,
                  examDate: "",
                  type: data.type!,
                  month: data.month,
                  listStudentScore: studentData,
                );
              });
              setState(() {
                _isEnd = false;
              });
            },
            finishEnterScore: (){
              setState(() {
                _isLast = false;
                listScrollController.jumpTo(0);
                _showScrollGuide = true;
                Navigator.of(context).pop();
              },);
            },
          );
        });
  }
}

