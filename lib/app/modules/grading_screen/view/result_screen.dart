// ignore_for_file: unrelated_type_equality_checks

import 'dart:async';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/widget/header_item_wiget.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/routes/app_route.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../widget/Popup_Print_widget/view_pdf.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../models/grading_model/grading_model.dart';
import '../../../service/api/approved_api/approved_api.dart';
import '../../attendance_schedule_screen/state_management/state_exam/bloc/exam_schedule_bloc.dart';
import '../../report_screen/state/monthly_print_state/print_bloc_bloc.dart';
import '../bloc/grading_bloc.dart';
import '../bloc/view_image_bloc.dart';

class ResultScreen extends StatefulWidget {
  final bool isInstructor;
  final String routFromScreen;
  const ResultScreen({Key? key,this.isInstructor = true,this.routFromScreen =""}) : super(key: key);
  @override
  State<ResultScreen> createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  ScrollController scrollController = ScrollController();
  List<String> titleDataKh = [
    "លរ",
    "គោតនាម",
    "ពិន្ទុ",
    "និទ្ទេស",
    "ចំណាត់ថ្នាក់",
  ];
   List<String> titleDataEN = [
    "N.o",
    "Fullname",
    "Score",
    "Level",
    "Grade",
  ];

  bool connection = true;
  double _hight = 0;
  StreamSubscription? sub;
  bool _isLast = false;
  bool _isLoading = false;
  bool isSaveLoading = false;
  String fileName = "";
  List<StudentsData> checkDoneEnterScore=[];
  @override
  void initState() {
     setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
          //  Future.delayed(Duration.zero,(() {
          //     BlocProvider.of<GradingBloc>(context).add(
          //        const GetGradingEvent("557", "199", "1", "1", "FIRST_SEMESTER"));
          //  }));
          } else {
          }
        });
      });
      //=============Eend Check internet====================
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      body: SafeArea(
          bottom: false,
          child: BlocBuilder<GradingBloc, GradingState>(
            builder: (context, state) {
              if(state is GetGradingDataLoading){
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              else if(state is GetGradingDataLoaded){
                var data = state.GradingList!.data;
                var studentData = state.GradingList!.data!.studentsData!;
                // try{
                //   studentData.sort((a, b) => a.rank!.compareTo(b.rank!),);
                // }catch(e){}
                checkDoneEnterScore = studentData.where((element) => element.score == null && element.absentExam == 0,).toList();
                return Stack(
                  children: [
                    Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                 Navigator.of(context).pop();
                              },
                              child: SvgPicture.asset(
                                ImageAssets.chevron_left,
                                height: 28,
                                width: 28,
                              ),
                            ),
                            Expanded(
                              child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(9),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(20),
                                          gradient:const LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                Colorconstand.mainColorSecondary,
                                                Colorconstand.primaryColor,
                                              ])),
                                      child: Text(translate=="km"?data!.className!.toString().replaceAll("ថ្នាក់ទី", ""):data!.classNameEn!.toString().replaceAll("ថ្នាក់ទី", "").replaceAll("Class", ""),
                                          style: ThemsConstands.headline6_medium_14
                                              .copyWith(
                                            color: Colorconstand.lightTextsRegular,
                                          )),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      "${"MONTHLYSCORE".tr()}  ${data.type==1?"${translate=="km"?"MONTH".tr():""}${checkMonth(int.parse(data.month.toString()))}":data.semester.toString().tr()}",
                                      style: ThemsConstands
                                          .headline3_semibold_20,
                                    )
                                  ]),
                            ),
                           
                            PopupMenuButton(
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
                            padding:const EdgeInsets.symmetric(horizontal: 8),
                          
                            icon:  const Icon(Icons.more_vert, size: 22, color: Color.fromARGB(255, 139, 173, 240)),
                            itemBuilder: (ctx) => [
                              data.isApprove == 1 && widget.isInstructor == true && data.isAdminApprove == 0? _buildPopupMenuItem(iconData: Icons.sync_lock_rounded,title: "បោះបង់ការអនុម័ត".tr(),checkDoneEnterScore: checkDoneEnterScore,isInstructor: widget.isInstructor, aproved: data.isApprove!,isadminaproved: data.isAdminApprove!, onTap: () { 
                                Future.delayed(const Duration(milliseconds: 10),() async{
                                  setState(() {
                                    _isLoading=true;
                                  });
                                  ApprovedApi().postApproved(classId: data.classId, examObjectId: data.id, isApprove: 0, discription: "").then((value){
                                  setState(() {
                                    _isLoading=false;
                                    if(widget.routFromScreen == "MY_Schedule"){
                                      BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString()));
                                    }
                                    else{
                                      BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString(),classid: data.classId.toString()));
                                    }
                                  });
                                  Navigator.of(context).pop();
                                });
                               });
                              }):PopupMenuItem(height: 0,child: Container(height: 0,)),
                              _buildPopupMenuItem(iconData: Icons.edit,title: "EDIT".tr(),checkDoneEnterScore: checkDoneEnterScore,isInstructor: widget.isInstructor, aproved: data.isApprove!,isadminaproved: data.isAdminApprove!, onTap: () { 
                                Future.delayed(const Duration(milliseconds: 10),()=>
                                 Navigator.of(context).pushReplacementNamed(Routes.GRADINGSCREEN));
                                },),
                              _buildPopupMenuItem(
                                isadminaproved: 0,
                                isInstructor: true,
                                checkDoneEnterScore: checkDoneEnterScore,
                                iconData: Icons.print_outlined,title: "DOWNLOAD_DUC".tr(),aproved: 0, onTap: () {  
                                  Future.delayed(const Duration(milliseconds: 100),() => 
                                  BlocProvider.of<PrintBlocBloc>(context).add(
                                    GetSubjectResultPDFEvent(
                                      classId: "${data.classId}", 
                                      istwocolumn: '2', 
                                      month: "${data.month}", 
                                      subjectId: "${data.subjectId}", 
                                      term: "${data.semester}", 
                                      type: "${data.type}",
                                    )
                                  ));
                              },)
                            ],
                          ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 18,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 26),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            HeaderItemWidget(
                               colorIcons: Colorconstand.primaryColor,colorTitle: Colorconstand.darkTextsRegular,  title: translate=="km"? data.subjectName!:data.subjectNameEn, svg: ImageAssets.book),
                            HeaderItemWidget(
                              colorIcons:data.isNoExam == 0?Colorconstand.primaryColor:Colorconstand.alertsDecline,colorTitle:data.isNoExam == 0?Colorconstand.darkTextsRegular:Colorconstand.alertsDecline,  title: data.isNoExam==0?"${data.examDate} - ${data.startTime}":data.isNoExam==1?"NO_EXAM".tr():"NOT_SET_EXAM_DAY".tr(),
                                svg: ImageAssets.current_date),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 13,
                      ),
                      Expanded(
                        child: NotificationListener(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Container(
                                //   width: MediaQuery.of(context).size.width,
                                //   child: DataTable(
                                //       dividerThickness: 0.5,
                                //       columnSpacing: 32,
                                //       horizontalMargin: 15,
                                //       headingRowColor:
                                //           MaterialStateColor.resolveWith(
                                //               (states) =>
                                //                   Colorconstand.neutralBtnBg),
                                //       columns: 
                                // List.generate(titleDataKh.length,
                                //           (index) {
                                //         return DataColumn(
                                //           label: Expanded(
                                //             child: Text(
                                //             translate=="km"? titleDataKh[index]:titleDataEN[index],
                                //               style: ThemsConstands
                                //                   .headline_4_medium_18,
                                //               textAlign: TextAlign.left,
                                //             ),
                                //           ),
                                //         );
                                //       }),
                                //       rows: List.generate(studentData.length,
                                //           (index) {
                                //         return DataRow(
                                //           cells: <DataCell>[
                                //             DataCell(
                                // Center(
                                //               child: Text(
                                //                 "${index + 1}",
                                //                 style: ThemsConstands
                                //                     .headline_5_medium_16,
                                //               ),
                                //             )),
                                //             DataCell(
                                // Text(
                                //             translate=="km"?  "${studentData[index].studentName}":"${studentData[index].studentNameEn.toString()== " "?"No Name":studentData[index].studentNameEn}",
                                //               style: ThemsConstands.headline_5_medium_16,
                                //             )),
                                //             DataCell(Center(
                                //               child: 
                                //               Text(
                                //                 studentData[index].score.toString()!="null"? "${studentData[index].score}":"---",
                                //                 style: ThemsConstands.headline_5_medium_16.copyWith(color: studentData[index].absentExam ==1
                                //                             ? Colorconstand
                                //                                 .alertsNotifications
                                //                             : Colorconstand
                                //                                 .lightBlack,
                                //                         fontWeight:
                                //                             FontWeight.bold),
                                //               ),
                                //             )),
                                //             DataCell(Center(
                                //                 child: 
                                //            Text(
                                //               studentData[index].grading.toString()!="null"? "${studentData[index].grading}":"---",
                                //               style: ThemsConstands
                                //                   .headline_5_medium_16
                                //                   .copyWith(
                                //                       fontWeight:
                                //                           FontWeight.normal,
                                //                       color: studentData[
                                //                                       index]
                                //                                   .letterGrade ==
                                //                               "F"
                                //                           ? Colorconstand
                                //                               .alertsNotifications
                                //                           : studentData[
                                //                                           index]
                                //                                       .letterGrade ==
                                //                                   "B"
                                //                               ? Colorconstand
                                //                                   .alertsAwaitingText : studentData[
                                //                                           index]
                                //                                       .grading.toString() ==
                                //                                   "null"?Colorconstand.lightBlack
                                //                               : Colorconstand
                                //                                   .alertsPositive),
                                //             )
                                // )),
                                //             DataCell(Center(
                                //                 child: Row(
                                //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //                   children: [
                                //                     Expanded(
                                //                       child: Center(
                                //                         child: Text(
                                //                           studentData[index].rank.toString()!="null"? "${studentData[index].rank}":"---",
                                //                           style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.mainColorForecolor),
                                                                          
                                //                         ),
                                //                       ),
                                //                     ),
                                //                     studentData[index].childrenSubject!.isNotEmpty? IconButton(
                                //                       onPressed: () {},
                                //                       icon: const Icon(Icons.keyboard_arrow_down_rounded,color: Colorconstand.lightBlack,),
                                //                     ):Container(),
                                //                   ],
                                //                 ))),
                                           
                                //           ],
                                //         );
                                        
                                //       })),
                                // ),
                                Container(
                                  height: 50,
                                  decoration: const BoxDecoration(color:  Colorconstand.neutralBtnBg),
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: Text("N.O".tr(),style: ThemsConstands.button_semibold_16,),
                                      ),
                                      Container(
                                        width: 135,
                                        alignment: Alignment.centerLeft,
                                        child: Text("FULL_NAME".tr(),style: ThemsConstands.button_semibold_16,),
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Text("POINT".tr(),style: ThemsConstands.button_semibold_16,),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Text("RANK".tr(),style: ThemsConstands.button_semibold_16,),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Text("GRADE_POINT".tr(),style: ThemsConstands.button_semibold_16,),
                                        ),
                                      ),
                                      
                                    ],
                                  ),
                                ),
                               Expanded(
                                 child: ListView.separated(
                                  controller: scrollController,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                   return Container(
                                    padding: const EdgeInsets.only(top: 7),
                                    child: Column(
                                      children: [
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                           SizedBox(
                                            width: 50 ,
                                            child: Center(
                                              child: Text(
                                                 "${index + 1}",
                                                 textAlign: TextAlign.left,
                                                 style: ThemsConstands
                                                     .headline_5_medium_16,
                                              ),
                                            ),
                                           ),
                                           SizedBox(
                                            width: 135,
                                            child: Text(
                                              translate=="km"?  "${studentData[index].studentName}":"${studentData[index].studentNameEn.toString()== " "?"${studentData[index].studentName}":studentData[index].studentNameEn}",
                                              style: ThemsConstands.headline_5_medium_16,
                                              )
                                            ),
                                           Expanded(
                                            child: Center(
                                              child: Text(
                                                studentData[index].absentExam==1?"A":studentData[index].score.toString()!="null"? "${studentData[index].score.toString()}/${studentData[index].subjectScoreMax}":"---",
                                                textAlign: TextAlign.center,
                                                style: ThemsConstands.headline_5_semibold_16.copyWith(color: studentData[index].absentExam ==1? Colorconstand.alertsNotifications: Colorconstand.lightBlack,),
                                              ),
                                            ),
                                            ),
                                          Expanded(
                                            child: Center(
                                              child: Row(
                                                mainAxisAlignment:studentData[index].childrenSubject!.isNotEmpty? MainAxisAlignment.end:MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    studentData[index].rank.toString()!="null"? "${studentData[index].rank}":"---",
                                                    style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.mainColorForecolor),
                                                                    
                                                  ),
                                                  const SizedBox(width: 10,),
                                                  studentData[index].childrenSubject!.isNotEmpty? InkWell(
                                                    onTap: () {
                                                    setState(() {
                                                       studentData[index].showChild= !studentData[index].showChild;
                                                    });
                                                    },
                                                    child: Icon(studentData[index].showChild==false? Icons.keyboard_arrow_down_rounded:Icons.keyboard_arrow_up_rounded,color: Colorconstand.primaryColor,))
                                                    :Container()
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                          child: Center(
                                            child: Text(
                                              studentData[index].grading.toString()!="null"? "${translate =="km"? studentData[index].grading:""} (${studentData[index].letterGrade})":"---",
                                              textAlign: TextAlign.center,
                                              style: ThemsConstands
                                                  .headline_5_medium_16
                                                  .copyWith(
                                                      color: studentData[index].letterGrade =="A" || studentData[index].letterGrade == "B"
                                                          ? Colorconstand.alertsPositive
                                                          : studentData[index].letterGrade.toString() == "null"
                                                          ?Colorconstand.lightBlack
                                                          : studentData[index].letterGrade.toString()=="F"
                                                          ? Colorconstand.alertsDecline
                                                          :Colorconstand.alertsAwaitingText ),),
                                          )
                                          ),
                                        ]),
                                        AnimatedContainer(
                                          height: studentData[index].showChild==true?25 * double.parse(studentData[index].childrenSubject!.length.toString()):0,
                                          duration: const Duration(milliseconds: 300),
                                          child: studentData[index].childrenSubject!.isNotEmpty? Container( padding: const EdgeInsets.only(top: 0,left: 25),
                                            child: ListView.builder(itemCount: studentData[index].childrenSubject!.length,
                                          itemBuilder: (context, indexs) {
                                            return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: [
                                              Container(width: 150,child: Text(". ${studentData[index].childrenSubject![indexs].name}", style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor, height: 1.5,))),
                                              const SizedBox(width: 10,),
                                              Expanded(child: Center(
                                                child: Text("${studentData[index].absentExam.toString()=="1"?"A":studentData[index].childrenSubject![indexs].score.toString()=="null"||studentData[index].childrenSubject![indexs].score.toString()==""?"---":studentData[index].childrenSubject![indexs].score}/${studentData[index].childrenSubject![indexs].maxScore}", textAlign: TextAlign.right,style: ThemsConstands.headline_5_medium_16.copyWith(color:studentData[index].absentExam.toString()=="1"?Colorconstand.alertsDecline: Colorconstand.primaryColor, height: 1.5,)),
                                              )),
                                              const Expanded(child: Center(
                                              )),
                                              const Expanded(child: Center(
                                              )),
                                            ],);
                                          },),):Container(),
                                        )
                                      ],
                                    ),
                                   );
                                 }, separatorBuilder: (context, index) {
                                   return const Divider(indent: 15,endIndent: 15,);
                                 }, itemCount: studentData.length),
                               ),
                                Container(
                                 // height: 90,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          padding:const EdgeInsets.only(left: 18),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children:  [
                                              BottomTitleResultWidget(
                                                title: "TOTAL_STUDENT".tr(),
                                                subtitle: data.totalStudentsInClass.toString(),
                                              ),
                                              BottomTitleResultWidget(
                                                title: "PASS".tr(),
                                                subtitle: studentData.where((element) => element.letterGrade!="F").length.toString(),
                                              ),
                                              BottomTitleResultWidget(
                                                title: "FAIL_EXAM".tr(),
                                                subtitle: studentData.where((element) => element.letterGrade=="F").length.toString(),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      BlocBuilder<ViewImageBloc, ViewImageState>(
                                        builder: (context, state) {
                                          if(state is GetViewImageDataLoading){
                                            return const CircularProgressIndicator();
                                          }
                                          else if(state is GetViewImageDataLoaded){
                                            var value = state.viewImageList;
                                            return value!.data!.medias!.isNotEmpty? 
                                            Expanded(
                                              child: Container(
                                                margin:const EdgeInsets.only(right: 18),
                                                height: 45,
                                                child: MaterialButton(
                                                  padding:const EdgeInsets.all(0),
                                                  onPressed: () {
                                                      BlocProvider.of<ViewImageBloc>(context).add(
                                                          GetViewImageEvent(data.id!.toString()),
                                                      );
                                                    Navigator.pushNamed(context, Routes.PREVIEWFILE);
                                                  },
                                                  color: Colorconstand.lightGoten,
                                                  child: Container(
                                                    child: Row(
                                                      mainAxisAlignment:MainAxisAlignment.center,
                                                      crossAxisAlignment:CrossAxisAlignment.center,
                                                      children: [
                                                        SvgPicture.asset(ImageAssets.document),
                                                        const SizedBox(
                                                          width: 5,
                                                        ),
                                                        Text(
                                                          "ATTACHMENT".tr(),
                                                          style: ThemsConstands
                                                              .button_semibold_16
                                                              .copyWith(
                                                                  color:
                                                                      Colorconstand.lightBlack),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ):Container();
                                          }
                                          else{
                                            return const CircularProgressIndicator();
                                          }
                                          
                                        },
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            onNotification: (t) {
                              if (t is ScrollEndNotification) {
                                if (scrollController.position.pixels > 0 &&
                                    scrollController.position.atEdge) {
                                  setState(() {
                                    _isLast = true;
                                  });
                                }
                                return true;
                              } else {
                                return false;
                              }
                            }
                            ),
                      ),
                      Container(
                        color: Colorconstand.neutralWhite,
                        child: Column(
                          children: [
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.center,
                            //   children: [
                                // SizedBox(
                                //   width: 10,
                                // ),
                                // Expanded(
                                //   child: MaterialButton(
                                //     height: 57.18,
                                //     onPressed: () {},
                                //     color: Colorconstand.lightGoten,
                                //     shape: RoundedRectangleBorder(
                                //         borderRadius: BorderRadius.circular(8.5)),
                                //     child: Container(
                                //       child: Row(
                                //         mainAxisAlignment:
                                //             MainAxisAlignment.center,
                                //         crossAxisAlignment:
                                //             CrossAxisAlignment.center,
                                //         children: [
                                //           SvgPicture.asset(ImageAssets.print),
                                //           const SizedBox(
                                //             width: 5,
                                //           ),
                                //           Text(
                                //             "បោះពុម្ភ",
                                //             style: ThemsConstands
                                //                 .button_semibold_16
                                //                 .copyWith(
                                //                     color:
                                //                         Colorconstand.lightBlack),
                                //           ),
                                //         ],
                                //       ),
                                //     ),
                                //   ),
                                // ),
                            //   ],
                            // ),
                            // const SizedBox(
                            //   height: 13,
                            // ),
                            
                          checkDoneEnterScore.isEmpty?AnimatedContainer(
                            height:69,
                            duration: const Duration(milliseconds: 300),
                             child: widget.isInstructor == true && data.isApprove==0? Container(
                              margin:const EdgeInsets.only(bottom: 22,left: 28,right: 28),
                               child: MaterialButton(
                                  height: 69,
                                  onPressed: 
                                  () async{
                                      setState(() {
                                        _isLoading=true;
                                      });
                                      ApprovedApi().postApproved(classId: data.classId, examObjectId: data.id, isApprove: 1, discription: "").then((value){
                                      setState(() {
                                        _isLoading=false;
                                       if(widget.routFromScreen == "MY_Schedule"){
                                        BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString()));
                                       }
                                       else{
                                        BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: data.semester,type: data.type.toString(),month: data.month.toString(),classid: data.classId.toString()));
                                       }
                                      });
                                      Navigator.of(context).pop();
                                   });
                                  },
                                  textColor: Colorconstand.neutralWhite,
                                  color:Colorconstand.primaryColor,
                                  child:  Center(
                                    child: Text("${"CHECK".tr()} & ${"APPROVE".tr()}",
                                      style: ThemsConstands.button_semibold_16,
                                    ),
                                  ),
                                ),
                             ):Container(
                                //color: Colorconstand.neutralWhite,
                                width: MediaQuery.of(context).size.width,
                                child: Center(child: Text( data.isApprove==1? "${"APPROVED".tr()} & ${"CHECK".tr()}${"FROM".tr()}${"ISINSTRUCTOR".tr()}":"${"AWAIT_APPROVE".tr()} & ${"CHECK".tr()}${"FROM".tr()}${"ISINSTRUCTOR".tr()}",style: ThemsConstands.headline_5_medium_16.copyWith(color: data.isApprove == 1?Colorconstand.primaryColor: Colorconstand.alertsAwaitingText),textAlign: TextAlign.center,)),
                              ), 
                           ):Container(height: 0,),
                            Container(height: 8,)
                          ],
                        ),
                      )
                    ],
                  ),
                  !_isLoading? Container() : Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(color: Colorconstand.primaryColor.withOpacity(0.7)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children:[
                        Text("GRADING_ARE_BEING_SUBMIT".tr(),style:const TextStyle(fontSize: 23,color: Colorconstand.neutralWhite,fontWeight: FontWeight.bold),),
                        const SizedBox(height: 15),
                        Text("YOUR_GRADE_ENTRY_WILL_SUBMITED".tr(),textAlign: TextAlign.center,style:const TextStyle(fontSize: 23,color: Colorconstand.neutralWhite,fontWeight: FontWeight.bold,fontStyle: FontStyle.italic),),
                        const SizedBox(height: 15),
                        const CircularProgressIndicator(color: Colorconstand.neutralWhite),
                      ]
                      ),
                  ),
                  BlocListener<PrintBlocBloc, PrintBlocState>(
                    listener: (context, state) {
                      if(state is PrintPDFLoadingState){
                        setState(() {
                          isSaveLoading = true;
                        });
                      }else if(state is PrintPDFSuccessState){
                        setState(() {
                          isSaveLoading =false;
                        });
                        showModalBottomSheet(
                          isScrollControlled: true,
                          enableDrag:false,
                          isDismissible: true,
                          context: context, builder:(context) {
                          return viewPDFReportWidget( state: state.pdfBodyString,fileName: fileName,);
                        });
                    
                      }else if(state is PrintPDFSubjectSuccessState){
                        setState(() {
                          isSaveLoading =false;
                        });
                        showModalBottomSheet(
                          isScrollControlled: true,
                          enableDrag:false,
                          isDismissible: true,
                          context: context, builder:(context) {
                          return viewPDFReportWidget( state: state.pdfBodySubjectResult,fileName: fileName,);
                        });
                    
                      }
                      else{
                        setState(() {
                          isSaveLoading = false;
                        });
                      }
                    },
                    child: isSaveLoading == true ? 
                      Positioned(
                        child: Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey.withOpacity(.5),
                          child: const Center(child: CircularProgressIndicator(),),
                        ),
                      ):Container(),
                  )
                
                  ],
                );
              }
              else{
                return SizedBox(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(height: 1,),
                  Expanded(
                    child: EmptyWidget(
                          title: "WE_DETECT".tr(),
                          subtitle: "WE_DETECT_DES".tr(),
                        ),
                  ),
                ],
              ),
            );
              }
              
            },
          )),
    );
  }
}

PopupMenuItem _buildPopupMenuItem(
      {required String title,
      required IconData iconData,
      required int aproved,
      required int isadminaproved,
      required bool isInstructor,
      required List<StudentsData> checkDoneEnterScore,
      required Function() onTap}) {
    return PopupMenuItem(
      height:aproved == 1 && isInstructor == false&&checkDoneEnterScore.isEmpty?0: isadminaproved == 0?kMinInteractiveDimension:0,
      onTap:onTap,
      mouseCursor: MouseCursor.uncontrolled,
      padding:const EdgeInsets.symmetric(horizontal: 4,vertical:4),
      child: aproved == 1 && isInstructor == false&&checkDoneEnterScore.isEmpty?Container(height: 0,): isadminaproved == 1?Container(height: 0,): Container(
        decoration: BoxDecoration(
          color: Colorconstand.gray300.withOpacity(0.4),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
        ),
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 18,horizontal: 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              iconData,
              color: Colorconstand.primaryColor,
              size: 20,
            ),
            const SizedBox(
              width: 8.0,
            ),
            Expanded(
              child: Text(
                title,
                style: ThemsConstands.headline_6_regular_14_20height,
              ),
            ),
          ],
        ),
      ),
    );
  }

class BottomTitleResultWidget extends StatelessWidget {
  final String title, subtitle;
  const BottomTitleResultWidget({
    Key? key,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Row(
        children: [
          Text(
            title,
            style: ThemsConstands.headline_5_medium_16,
          ),
          Text(
            ":",
            style: ThemsConstands.headline_5_medium_16,
            textAlign: TextAlign.center,
          ),
          Text(
            subtitle,
            style: ThemsConstands.headline_5_medium_16,
            textAlign: TextAlign.right,
          ),
          Text(
          " ${"PEOPLE".tr()}",
            style: ThemsConstands.headline_5_medium_16,
            textAlign: TextAlign.right,
          )
        ],
      ),
    );
  }
}
