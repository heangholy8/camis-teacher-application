class ResultModel {
  String name, totalScore, grade, rank, gradEn;
  bool isAbsent;

  ResultModel(this.name, this.totalScore, this.grade, this.rank, this.isAbsent,
      this.gradEn);
}
