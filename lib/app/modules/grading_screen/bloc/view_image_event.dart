part of 'view_image_bloc.dart';


abstract class ViewImageEvent extends Equatable {
  const ViewImageEvent();

  @override
  List<Object> get props => [];
}

class GetViewImageEvent extends ViewImageEvent{
  final String examId;
  const GetViewImageEvent(this.examId);
}