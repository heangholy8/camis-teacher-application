part of 'check_switch_enter_score_screen_bloc.dart';

abstract class CheckSwitchEnterScoreScreenEvent extends Equatable {
  const CheckSwitchEnterScoreScreenEvent();

  @override
  List<Object> get props => [];
}

class GetCheckSwitchEnterScoreEvent extends CheckSwitchEnterScoreScreenEvent{}
