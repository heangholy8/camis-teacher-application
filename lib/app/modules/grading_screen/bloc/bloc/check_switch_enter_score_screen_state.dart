part of 'check_switch_enter_score_screen_bloc.dart';

abstract class CheckSwitchEnterScoreScreenState extends Equatable {
  const CheckSwitchEnterScoreScreenState();
  
  @override
  List<Object> get props => [];
}

class CheckSwitchEnterScoreScreenInitial extends CheckSwitchEnterScoreScreenState {}

class GetCheckSwitchEnterScoreLoading extends CheckSwitchEnterScoreScreenState{}

class GetCheckSwitchEnterScoreLoaded extends CheckSwitchEnterScoreScreenState{
  final GetCheckCoditionSwitchEnterScoreScreenModel? checkEnterScoreSwitchScreen;
  const GetCheckSwitchEnterScoreLoaded({required this.checkEnterScoreSwitchScreen});
}
class GetCheckSwitchEnterScoreError extends CheckSwitchEnterScoreScreenState{}
