import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../models/grading_model/enter_score_feature_model.dart';
import '../../../../service/api/grading_api/get_enter_score_feature_api.dart';

part 'check_switch_enter_score_screen_event.dart';
part 'check_switch_enter_score_screen_state.dart';

class CheckSwitchEnterScoreScreenBloc extends Bloc<CheckSwitchEnterScoreScreenEvent, CheckSwitchEnterScoreScreenState> {
  final GetApiCheckConditionSwitchEnterScore getCheckSwitcheScreen;
  CheckSwitchEnterScoreScreenBloc({required this.getCheckSwitcheScreen}) : super(CheckSwitchEnterScoreScreenInitial()) {
    on<GetCheckSwitchEnterScoreEvent>((event, emit) async {
      emit(GetCheckSwitchEnterScoreLoading());
      try {
        var dataSwitch = await getCheckSwitcheScreen.getApiCheckConditionSwitchEnterScore();
        emit(GetCheckSwitchEnterScoreLoaded(checkEnterScoreSwitchScreen: dataSwitch));
      } catch (e) {
        print(e);
        emit(GetCheckSwitchEnterScoreError());
      }
    });
  }
}
