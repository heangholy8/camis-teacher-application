part of 'grading_bloc.dart';

abstract class GradingEvent extends Equatable {
  const GradingEvent();

  @override
  List<Object> get props => [];
}

class GetGradingEvent extends GradingEvent{
  final String idClass;
  final String subjectId;
  final String type;
  final String month;
  final String semester;
   String? examDate;

   GetGradingEvent({required this.idClass,required this.subjectId,required this.type,required this.month,required this.semester, this.examDate});
}
