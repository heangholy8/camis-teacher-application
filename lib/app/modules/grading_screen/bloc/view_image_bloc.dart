import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/image_view_model/image_view_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../service/api/attachment_api/get_attachment_api.dart';

part 'view_image_event.dart';
part 'view_image_state.dart';


class ViewImageBloc extends Bloc<ViewImageEvent, ViewImageState> {
  final GetAttachmentApi getViewImageData;
  ViewImageBloc({required this.getViewImageData}) : super(ViewImageInitial()) {
    on<GetViewImageEvent>((event, emit) async{
      emit(GetViewImageDataLoading());
      try {
        var dataViewImage = await getViewImageData.getAttachmentExam(exam_id: event.examId);
        emit(GetViewImageDataLoaded(viewImageList: dataViewImage));
      } catch (e) {
        print(e);
        emit(GetViewImageDataError());
      }
    });
  }
}