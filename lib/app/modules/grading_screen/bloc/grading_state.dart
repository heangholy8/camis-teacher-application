part of 'grading_bloc.dart';

abstract class GradingState extends Equatable {
  const GradingState();
  
  @override
  List<Object> get props => [];
}

class GradingInitial extends GradingState {}

class GetGradingDataLoading extends GradingState{}

class GetGradingDataLoaded extends GradingState{
  final GradingSubjectModel? GradingList;
  const GetGradingDataLoaded({required this.GradingList});
}
class GetGradingDataError extends GradingState{}
