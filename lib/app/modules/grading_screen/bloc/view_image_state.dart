part of 'view_image_bloc.dart';
abstract class ViewImageState extends Equatable {
  const ViewImageState();
  
  @override
  List<Object> get props => [];
}

class ViewImageInitial extends ViewImageState {}

class GetViewImageDataLoading extends ViewImageState{}

class GetViewImageDataLoaded extends ViewImageState{
  final ImageViewModel? viewImageList;
  const GetViewImageDataLoaded({required this.viewImageList});
}
class GetViewImageDataError extends ViewImageState{}