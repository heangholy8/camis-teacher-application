import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/grading_model/grading_model.dart';
import 'package:equatable/equatable.dart';

import '../../../service/api/grading_api/grading_api.dart';

part 'grading_event.dart';
part 'grading_state.dart';

class GradingBloc extends Bloc<GradingEvent, GradingState> {
  final GradingApi getGradingData;
  GradingBloc({required this.getGradingData}) : super(GradingInitial()) {
    on<GetGradingEvent>((event, emit) async{
      emit(GetGradingDataLoading());
      try {
        var dataGrading = await getGradingData.getListStudentSubjectExam(classId: event.idClass,type: event.type,semester: event.semester,month: event.month,subjectId: event.subjectId,examDate: event.examDate);
        emit(GetGradingDataLoaded(GradingList: dataGrading));
      } catch (e) {
        print(e);
        emit(GetGradingDataError());
      }
    });
  }
}
