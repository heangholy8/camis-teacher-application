
import 'package:flutter/material.dart';

import '../../../../core/constands/color_constands.dart';
import '../../../../core/thems/thems_constands.dart';

class WidgetTab extends StatefulWidget {
  final VoidCallback onTap;
  final bool displayTab;
  final String name;
  const WidgetTab({required this.name,required this.displayTab,required this.onTap,super.key});

  @override
  State<WidgetTab> createState() => _WidgetTabState();
}

class _WidgetTabState extends State<WidgetTab> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: widget.onTap,
        child: Container(
          alignment: Alignment.center,
          height: 45,
          margin:const EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: widget.displayTab ==true? Colorconstand.primaryColor:Colors.transparent,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Text(widget.name,style:ThemsConstands.headline_5_semibold_16.copyWith(color: widget.displayTab ==true?  Colorconstand.neutralSecondary:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
        ),
      ),
    );
  }
}