import 'dart:async';
import 'dart:math';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/state/all_year_in_school/get_year_school_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/state/summary_student_teacher_in_class/summary_student_teacher_in_class_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/state/summary_study_student/summary_student_result_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/view/summary_class.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/view/summary_student.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/view/summary_teacher.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../widget/widget_tab.dart';



class SummarySchoolInfoScreen extends StatefulWidget {
  const SummarySchoolInfoScreen({super.key});

  @override
  State<SummarySchoolInfoScreen> createState() => _SummarySchoolInfoScreenState();
}

class _SummarySchoolInfoScreenState extends State<SummarySchoolInfoScreen>with TickerProviderStateMixin {

  

    StreamSubscription? internetconnection;
    bool isoffline = true;
    String yearActive = "";
    bool showListDay = false;
    int disPlayTap = 1;
      
  @override
  void initState() {
    setState(() {
    //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

    });
    //============== Event call Data =================
    BlocProvider.of<GetYearSchoolBloc>(context).add(GetAllYearInSchoolEvent());
    BlocProvider.of<SummaryStudentResultBloc>(context).add(GetSummaryStudentResult());
    //============== End Event call Data =================
    super.initState();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: Container(
                  margin:const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      //========== Button widget Change class==========
                      GestureDetector(
                        child: Container(
                          margin:const EdgeInsets.only(left: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              IconButton(
                                splashColor: Colors.transparent,
                                highlightColor:  Colors.transparent,
                                padding:const EdgeInsets.all(0),
                                onPressed: (){Navigator.of(context).pop();}, 
                                icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 22,color: Colorconstand.neutralWhite,)),
                              Expanded(
                                child: Container(
                                  margin:const EdgeInsets.only(left: 10),
                                  child: Text("របាយការណ៍សង្ខេបក្នុងសាលា", style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                ),
                              ),
                              const SizedBox(width: 55,) 

                            ],
                          ),
                        ),
                      ),
                      //========== Button End widget Change class==========
                      const SizedBox(height: 10,),
                      Expanded(
                        child: Container(
                          width: wieght,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Colorconstand.neutralWhite),
                          child: BlocConsumer<GetYearSchoolBloc,GetYearSchoolState>(
                            listener: (context, state) {
                              if(state is GetYearSchoolLoaded){
                                var dataMonthActive = state.allYearInschool.data!.where((element) => element.currentActive == 1,).toList();
                                if(dataMonthActive.isNotEmpty){
                                  setState(() {
                                    BlocProvider.of<SummaryStudentTeacherInClassBloc>(context).add(GetSummaryStudentTeacherInSchool(schoolYearId: dataMonthActive[0].id.toString()));
                                    yearActive = translate == "km"?dataMonthActive[0].name.toString():dataMonthActive[0].nameEn.toString();
                                  });
                                }
                              }
                            },
                            builder: (context, state) {
                              if(state is GetYearSchoolLoading){
                                return const Center(child: CircularProgressIndicator(),);
                              }
                              else if(state is GetYearSchoolLoaded){
                                var dataMonth = state.allYearInschool.data;
                                return Container(
                                  width: wieght,
                                  child: Column(
                                    children: [
                                      Container(
                                        width: wieght,
                                        decoration: BoxDecoration(
                                          borderRadius:const BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
                                          color: Colorconstand.neutralGrey.withOpacity(0.5)
                                        ),
                                        child: MaterialButton(
                                          onPressed: () {
                                            setState(() {
                                              showListDay = !showListDay;
                                            });
                                          },
                                          splashColor: Colors.transparent,
                                          highlightColor: Colors.transparent,
                                          padding:const EdgeInsets.all(0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text("${"YEAR".tr()} $yearActive",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBlack),),
                                              const SizedBox(width: 8,),
                                              const Icon(Icons.keyboard_arrow_down_rounded,size: 22,color: Colorconstand.lightBlack,)
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Stack(
                                          children: [
                                            Positioned(
                                              top: 0,left: 0,right: 0,bottom: 0,
                                              child: Container(
                                                child: BlocConsumer<SummaryStudentTeacherInClassBloc, SummaryStudentTeacherInClassState>(
                                                  listener: (context, state) {
                                                    if(state is SummaryStudentTeacherInClassLoaded){                                                        
                                                    }
                                                  },
                                                  builder: (context, state) {
                                                    if(state is SummaryStudentTeacherInClassLoading){
                                                      return const Center(
                                                        child: CircularProgressIndicator(),
                                                      );
                                                    }
                                                    if(state is SummaryStudentTeacherInClassLoaded){
                                                      return Container(
                                                          child: Column(
                                                            children: [
                                                              Container(
                                                                margin:const EdgeInsets.all(18),
                                                                height: 55,
                                                                decoration: BoxDecoration(
                                                                  color: Colorconstand.neutralGrey,
                                                                  borderRadius: BorderRadius.circular(16),
                                                                ),
                                                                child: Row(
                                                                  children: [
                                                                    WidgetTab(
                                                                      onTap: (){
                                                                          setState(() {
                                                                            disPlayTap =1;
                                                                          });
                                                                        },
                                                                      name: "សង្ខេបសិស្ស",
                                                                      displayTab: disPlayTap == 1?true:false,
                                                                    ),
                                                                    WidgetTab(
                                                                      onTap: (){
                                                                          setState(() {
                                                                            disPlayTap =2;
                                                                          });
                                                                        },
                                                                      name: "សង្ខេបគ្រូ",
                                                                      displayTab: disPlayTap == 2?true:false,
                                                                    ),
                                                                    WidgetTab(
                                                                      onTap: (){
                                                                          setState(() {
                                                                            disPlayTap =3;
                                                                          });
                                                                        },
                                                                      name: "សង្ខេបថ្នាក់",
                                                                      displayTab: disPlayTap == 3?true:false,
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              Expanded(
                                                                child: SingleChildScrollView(
                                                                  child:disPlayTap == 2?const SummaryTeacher():disPlayTap == 3?const SummaryClass():const SummaryStudent(),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                      );
                                                    }
                                                    else{
                                                      return Container(
                                                        margin:const EdgeInsets.only(top: 50),
                                                        child: Center(
                                                          child: EmptyWidget(
                                                            title: "WE_DETECT".tr(),
                                                            subtitle: "WE_DETECT_DES".tr(),
                                                          ),
                                                        ),
                                                      );
                                                    }
                                                  },
                                                ),
                                              ),
                                            ),
                                            /// =============== Baground over when drop month =============
                                              showListDay == false
                                              ? Container()
                                              : Positioned(
                                                  bottom: 0,
                                                  left: 0,
                                                  right: 0,
                                                  top: 0,
                                                  child:GestureDetector(
                                                    onTap: (() {
                                                      setState(
                                                          () {
                                                        showListDay = false;
                                                      });
                                                    }),
                                                    child:Container(
                                                      color: const Color(0x7B9C9595),
                                                    ),
                                                  ),
                                                ),
                                              /// =============== Baground over when drop month =============
                                              /// 
                                              /// =============== Widget List Change Month =============
                                                    AnimatedPositioned(
                                                      top: showListDay == false? wieght > 800? -700: -400: 0,
                                                      left: 0,
                                                      right: 0,
                                                      duration: const Duration(milliseconds:300),
                                                      child:Container(
                                                        color: Colorconstand.neutralWhite,
                                                        child:
                                                            GestureDetector(
                                                          onTap:() {
                                                            setState(() {
                                                              showListDay = false;
                                                            });
                                                          },
                                                          child:Container(
                                                            margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                            child: GridView.builder(
                                                              shrinkWrap: true,
                                                              physics:const ScrollPhysics(),
                                                              padding:const EdgeInsets.all(.0),
                                                                itemCount: dataMonth!.length,
                                                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,mainAxisExtent: 60),
                                                                itemBuilder: (BuildContext context, int index) {
                                                                  return GestureDetector(
                                                                    onTap: (() {
                                                                      setState((){
                                                                        for (var element in dataMonth) {
                                                                            element.currentActive = 0;
                                                                        }
                                                                        dataMonth[index].currentActive = 1;
                                                                        yearActive = translate == "km"?dataMonth[index].name.toString():dataMonth[index].nameEn.toString();
                                                                        showListDay = !showListDay;
                                                                        BlocProvider.of<SummaryStudentTeacherInClassBloc>(context).add(GetSummaryStudentTeacherInSchool(schoolYearId: dataMonth[index].id.toString()));
                                                                      });
                                                                    }),
                                                                    child: Container(
                                                                      margin:const EdgeInsets.all(6),
                                                                      decoration:BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(12),
                                                                        color: dataMonth[index].currentActive == 1? Colorconstand.primaryColor:Colors.transparent,
                                                                      ),
                                                                      height: 20,width: 20,
                                                                      child:  Center(child: Text(translate == "km"?dataMonth[index].name.toString():dataMonth[index].nameEn.toString(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: dataMonth[index].currentActive==1?Colorconstand.neutralWhite:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                                    ),
                                                                  );
                                                                },
                                                              ),
                                                          )
                                                        ),
                                                      ),
                                                    ),
                                                /// =============== End Widget List Change Month =============
                                          ]
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }
                              else{
                                return Container(
                                  margin:const EdgeInsets.only(top: 50),
                                  child: Center(
                                    child: EmptyWidget(
                                      title: "WE_DETECT".tr(),
                                      subtitle: "WE_DETECT_DES".tr(),
                                    ),
                                  ),
                                );
                              }
                            }, 
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
          ],
        ),
      ));

  }
}
