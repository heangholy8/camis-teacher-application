import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pie_chart/pie_chart.dart';

import '../state/summary_student_teacher_in_class/summary_student_teacher_in_class_bloc.dart';

class SummaryClass extends StatefulWidget {
  const SummaryClass({super.key});

  @override
  State<SummaryClass> createState() => _SummaryClassState();
}

class _SummaryClassState extends State<SummaryClass> {

  double totalContainerChartsBar = 300;
  bool animateChartsBar = false;

  double totalGrapChartsHeightClass = 0;

  final colorList = <Color>[
    const Color(0xff0984e3),
    const Color(0xfffd79a8),
    const Color(0xFF8E5F70),
    const Color(0xFF31D7CC),
    const Color(0xFF7CDC3D),
    const Color(0xFFE4281A),
    const Color(0xFF5C1EC6),
    const Color(0xFF660EA1),
    ];

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 300),(){
      setState(() {
        animateChartsBar = true;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SummaryStudentTeacherInClassBloc, SummaryStudentTeacherInClassState>(
      listener: (context, state) {
        if(state is SummaryStudentTeacherInClassLoaded){
          
        }
      },
      builder: (context, state) {
        if(state is SummaryStudentTeacherInClassLoaded){
          var dataSummaryClass = state.studentAndTeacherInClass.data!.academicStructure;
          for(int i =0;i < dataSummaryClass!.totalClassLevel!.length;i++){
            if(dataSummaryClass.totalClassLevel![i].totalClass! > totalGrapChartsHeightClass){
                totalGrapChartsHeightClass = dataSummaryClass.totalClassLevel![i].totalClass!.toDouble();
            }
          }
          return Column(
              children: [
                Container(
                  margin:const EdgeInsets.all(18),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colorconstand.gray300
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(top:16),
                        child:Text("${"ចំនួនថ្នាក់សរុបនៅក្នុងសាលា"} (${dataSummaryClass.totalClass} ${"CLASS".tr()})",style: ThemsConstands.headline_4_semibold_18,),
                      ),
                      Container(
                        padding:const EdgeInsets.all(18),
                        height: totalContainerChartsBar,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            Container(
                                              child:Text("${(dataSummaryClass.totalClass! +10)}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                            ),
                                            Expanded(
                                              child: Container(),
                                            ),
                                            Container(
                                              child:Text("${(dataSummaryClass.totalClass! +10) /2}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                            ),
                                            Expanded(
                                              child: Container(),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Container(
                                              height: 2,width: 6,
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            Expanded(
                                              child: Container(
                                                width: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                            ),
                                            Container(
                                              height: 2,width: 6,
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            Expanded(
                                              child: Container(
                                                width: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: List.generate(dataSummaryClass.educationStage!.length, (index) => 
                                        Expanded(child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children:[
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                RotatedBox(quarterTurns: 0,child: Text("${dataSummaryClass.educationStage![index].total} ${"CLASS".tr()}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                const SizedBox(height: 3,),
                                                AnimatedContainer(
                                                  duration:const Duration(milliseconds: 300),
                                                  alignment: Alignment.bottomCenter,
                                                  height:animateChartsBar == false?0:(dataSummaryClass.educationStage![index].total! / (dataSummaryClass.totalClass! + 10))*(totalContainerChartsBar-38),
                                                  width: 30,
                                                  color: index ==0?const Color(0xB3F59A07):const Color(0xFF1361F1),
                                                ),
                                              ],
                                            ),
                                          ]
                                        ))
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 2,
                                            color: Colorconstand.neutralDarkGrey,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width-36,
                        child:Row(
                          children: List.generate(dataSummaryClass.educationStage!.length, (index) => Expanded(child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  width: 15,height: 15,
                                  color: index ==0?const Color(0xB3F59A07):const Color(0xFF1361F1),
                                ),
                                const SizedBox(width: 5,),
                              Text(dataSummaryClass.educationStage![index].name.toString().tr(),style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,),
                            ],
                          ))),
                        )
                      ),
                    ],
                  ),
                ),
                
                Container(
                  margin:const EdgeInsets.only(top: 0,left: 18,right: 18,bottom: 18),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colorconstand.gray300
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(top:16),
                        child:const Text("ចំនួនថ្នាក់តាមកម្រិត",style: ThemsConstands.headline_4_semibold_18,),
                      ),
                      Container(
                        padding:const EdgeInsets.all(18),
                        height: totalContainerChartsBar,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            Container(
                                              child:Text("${(totalGrapChartsHeightClass+ 10)}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                            ),
                                            Expanded(
                                              child: Container(),
                                            ),
                                            Container(
                                              child:Text("${(totalGrapChartsHeightClass+ 10) /2}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                            ),
                                            Expanded(
                                              child: Container(),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Container(
                                              height: 2,width: 6,
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            Expanded(
                                              child: Container(
                                                width: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                            ),
                                            Container(
                                              height: 2,width: 6,
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            Expanded(
                                              child: Container(
                                                width: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(height: 30,)
                              ],
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: List.generate(dataSummaryClass.totalClassLevel!.length, (index) => 
                                        Expanded(child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children:[
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                RotatedBox(quarterTurns: 3,child: Text("${dataSummaryClass.totalClassLevel![index].totalClass} ${"CLASS".tr()}",style: ThemsConstands.caption_simibold_12,textAlign: TextAlign.center,)),
                                                const SizedBox(height: 3,),
                                                AnimatedContainer(
                                                  duration:const Duration(milliseconds: 300),
                                                  alignment: Alignment.bottomCenter,
                                                  height:animateChartsBar == false?0:(dataSummaryClass.totalClassLevel![index].totalClass! / (totalGrapChartsHeightClass+10))*(totalContainerChartsBar-68),
                                                  width: 30,
                                                  color: colorList[index],
                                                ),
                                              ],
                                            ),
                                          ]
                                        ))
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 2,
                                            color: Colorconstand.neutralDarkGrey,
                                          ),
                                          Container(
                                            height: 30,
                                            width: MediaQuery.of(context).size.width-36,
                                            child:Row(
                                              children: List.generate(dataSummaryClass.totalClassLevel!.length, (index) => Expanded(child: Text(dataSummaryClass.totalClassLevel![index].level.toString().tr(),style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,))),
                                            )
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              
              ],
            );
        }
        else{
          return Container();
        }
      }, 
    );
  }
}