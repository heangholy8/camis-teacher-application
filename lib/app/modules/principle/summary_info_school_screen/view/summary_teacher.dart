import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pie_chart/pie_chart.dart';
import 'dart:math' as math;
import '../state/summary_student_teacher_in_class/summary_student_teacher_in_class_bloc.dart';

class SummaryTeacher extends StatefulWidget {
  const SummaryTeacher({super.key});

  @override
  State<SummaryTeacher> createState() => _SummaryTeacherState();
}

class _SummaryTeacherState extends State<SummaryTeacher> {

  double totalContainerChartsBar = 350;
  bool animateChartsBar = false;
  double totalGrapChartsHeightGradeStudent = 0;
  double totalGrapChartsHeightRankNumberGradeStudent = 0;

  double totalGrapChartsHeightStudent = 0;
  double totalGrapChartsHeightRankNumberStudent = 0;

  final colorList = <Color>[
    const Color(0xff0984e3),
    const Color(0xfffd79a8),
    // const Color(0xfffd79a8),
    ];
  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 300),(){
      setState(() {
        animateChartsBar = true;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SummaryStudentTeacherInClassBloc, SummaryStudentTeacherInClassState>(
      listener: (context, state) {
        if(state is SummaryStudentTeacherInClassLoaded){
          
        }
      },
      builder: (context, state) {
        if(state is SummaryStudentTeacherInClassLoaded){
          var dataSummaryTeacher = state.studentAndTeacherInClass.data!.teacher;
          var dataMap = <String, double>{
            "MALE".tr(): dataSummaryTeacher!.male!.toDouble(),
            "FEMALE".tr(): dataSummaryTeacher.female!.toDouble(),
          };
          for(int i =0;i < dataSummaryTeacher.educationStage!.length;i++){
            if(dataSummaryTeacher.educationStage![i].total! > totalGrapChartsHeightStudent){
                totalGrapChartsHeightStudent = dataSummaryTeacher.educationStage![i].total!.toDouble();
                totalGrapChartsHeightRankNumberStudent = (totalGrapChartsHeightStudent+10)/3.toDouble();
            }
          }
          for(int i =0;i < dataSummaryTeacher.dataGradeStage!.length;i++){
            for(int j =0;j < dataSummaryTeacher.dataGradeStage![i].level!.length;j++){
              if(dataSummaryTeacher.dataGradeStage![i].level![j].total! > totalGrapChartsHeightGradeStudent){
                totalGrapChartsHeightGradeStudent = dataSummaryTeacher.dataGradeStage![i].level![j].total!.toDouble();
                totalGrapChartsHeightRankNumberGradeStudent = (totalGrapChartsHeightGradeStudent+10)/3.toDouble();
              }
            }
          }
          return Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width-36,
                  height: MediaQuery.of(context).size.width-130,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colorconstand.gray300
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(top:16),
                        child:Text("ចំនួនគ្រូសរុបនៅក្នុងសាលា (${dataSummaryTeacher.total} ${"PEOPLE".tr()})",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.center,),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width-100,
                        padding:const EdgeInsets.all(12),
                        child: PieChart(
                          chartLegendSpacing: 20,
                          dataMap: dataMap,
                          chartType: ChartType.disc,
                          baseChartColor: Colorconstand.primaryColor,
                          colorList: colorList,
                          chartValuesOptions: const ChartValuesOptions(
                            showChartValuesInPercentage: false,
                          ),
                          totalValue: dataSummaryTeacher.total!.toDouble(),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin:const EdgeInsets.all(18),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colorconstand.gray300
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(top:16),
                        child:const Text("ចំនួនគ្រូសរុបនៅក្នុងសាលា",style: ThemsConstands.headline_4_semibold_18,),
                      ),
                      Container(
                        padding:const EdgeInsets.all(18),
                        height: totalContainerChartsBar,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            Container(
                                              child:Text("${(totalGrapChartsHeightStudent+10).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                            ),
                                            Expanded(
                                              child: Container(),
                                            ),
                                            Container(
                                              child:Text("${(totalGrapChartsHeightStudent+10 - (totalGrapChartsHeightRankNumberStudent)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                            ),
                                            Expanded(
                                              child: Container(),
                                            ),
                                            Container(
                                              child:Text("${(totalGrapChartsHeightStudent+10 - (totalGrapChartsHeightRankNumberStudent*2)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                            ),
                                            Expanded(
                                              child: Container(),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Container(
                                              height: 2,width: 6,
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            Expanded(
                                              child: Container(
                                                width: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                            ),
                                            Container(
                                              height: 2,width: 6,
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            Expanded(
                                              child: Container(
                                                width: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                            ),
                                            Container(
                                              height: 2,width: 6,
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            Expanded(
                                              child: Container(
                                                width: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 40,
                                ),
                              ],
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: List.generate(dataSummaryTeacher.educationStage!.length, (index) => 
                                        Expanded(child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children:[
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                RotatedBox(quarterTurns: 3,child: Text("${dataSummaryTeacher.educationStage![index].total}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                const SizedBox(height: 3,),
                                                AnimatedContainer(
                                                  duration:const Duration(milliseconds: 300),
                                                  alignment: Alignment.bottomCenter,
                                                  height:animateChartsBar == false?0:(dataSummaryTeacher.educationStage![index].total! / (totalGrapChartsHeightStudent+10))*(totalContainerChartsBar-78),
                                                  width: 30,
                                                  color: const Color(0xFFF1A713),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                RotatedBox(quarterTurns: 3,child: Text("${dataSummaryTeacher.educationStage![index].male}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                const SizedBox(height: 3,),
                                                AnimatedContainer(
                                                  duration:const Duration(milliseconds: 300),
                                                  height:animateChartsBar == false?0: (dataSummaryTeacher.educationStage![index].male! / (totalGrapChartsHeightStudent+10))*(totalContainerChartsBar-78),
                                                  width: 30,
                                                  color: const Color(0xff0984e3),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                RotatedBox(quarterTurns: 3,child: Text("${dataSummaryTeacher.educationStage![index].female}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                const SizedBox(height: 3,),
                                                AnimatedContainer(
                                                  duration:const Duration(milliseconds: 300),
                                                  height:animateChartsBar == false?0: (dataSummaryTeacher.educationStage![index].female! / (totalGrapChartsHeightStudent+10))*(totalContainerChartsBar-78),
                                                  width: 30,
                                                  color: const Color(0xfffd79a8),
                                                ),
                                              ],
                                            ),
                                          ]
                                        ))
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 2,
                                            color: Colorconstand.neutralDarkGrey,
                                          ),
                                          Container(
                                            height: 40,
                                            width: MediaQuery.of(context).size.width-36,
                                            child:Row(
                                              children: List.generate(dataSummaryTeacher.educationStage!.length, (index) => Expanded(child: Text("${dataSummaryTeacher.educationStage![index].name.toString().tr()}\n(${dataSummaryTeacher.educationStage![index].total} ${"PEOPLE".tr()})",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,))),
                                            )
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: 15,height: 15,
                                  color: const Color(0xFFF1A713),
                                ),
                                const SizedBox(width: 5,),
                                Text("TOTAL".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xFFF1A713),),),
                              ],
                            ),
                            Container(
                              margin:const EdgeInsets.symmetric(horizontal: 12),
                              child: Row(
                                children: [
                                  Container(
                                    width: 15,height: 15,
                                    color: const Color(0xff0984e3),
                                  ),
                                  const SizedBox(width: 5,),
                                  Text("MALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xff0984e3),),),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 15,height: 15,
                                  color: const Color(0xfffd79a8),
                                ),
                                const SizedBox(width: 5,),
                                Text("FEMALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xfffd79a8),),),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  child: ListView.builder(
                    padding:const EdgeInsets.all(0),
                    shrinkWrap: true,
                    physics:const ScrollPhysics(),
                    itemCount: dataSummaryTeacher.dataGradeStage!.length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin:const EdgeInsets.only(top: 0,left: 18,right: 18,bottom: 18),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colorconstand.gray300
                        ),
                        child: Column(
                          children: [
                            Container(
                              margin:const EdgeInsets.only(top:16),
                              child: Text("ចំនួនគ្រូសរុប \n${dataSummaryTeacher.dataGradeStage![index].name!.tr()} (${dataSummaryTeacher.educationStage![index].total!} ${"PEOPLE".tr()})",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.center,),
                            ),
                            
                            Container(
                          padding:const EdgeInsets.only(top: 18,left: 18,right: 18,bottom: 5),
                          height: totalContainerChartsBar,
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Row(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                child:Text("${(totalGrapChartsHeightGradeStudent+10).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:Text("${((totalGrapChartsHeightGradeStudent+10) - (totalGrapChartsHeightRankNumberGradeStudent)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:Text("${((totalGrapChartsHeightGradeStudent+10) - (totalGrapChartsHeightRankNumberGradeStudent*2)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: [
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 60,
                                  ),
                                ],
                              ),
                              Expanded(
                                child: SingleChildScrollView(
                                  physics:const ClampingScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  child: Column(
                                    children: [
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: List.generate(dataSummaryTeacher.dataGradeStage![index].level!.length, (indexGrade) => 
                                          Container(
                                            width: 100,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children:[
                                                Column(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: [
                                                      RotatedBox(quarterTurns: 3,child: Text("${dataSummaryTeacher.dataGradeStage![index].level![indexGrade].total}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                      const SizedBox(height: 3,),
                                                      AnimatedContainer(
                                                        duration:const Duration(milliseconds: 300),
                                                        alignment: Alignment.bottomCenter,
                                                        height:animateChartsBar == false?0:(dataSummaryTeacher.dataGradeStage![index].level![indexGrade].total! / (totalGrapChartsHeightGradeStudent+11))*(totalContainerChartsBar-68),
                                                        width: 30,
                                                        color: const Color(0xFFF1A713),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: [
                                                      RotatedBox(quarterTurns: 3,child: Text("${dataSummaryTeacher.dataGradeStage![index].level![indexGrade].male}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                      const SizedBox(height: 3,),
                                                      AnimatedContainer(
                                                        duration:const Duration(milliseconds: 300),
                                                        height:animateChartsBar == false?0: (dataSummaryTeacher.dataGradeStage![index].level![indexGrade].male! / (totalGrapChartsHeightGradeStudent+11))*(totalContainerChartsBar-68),
                                                        width: 30,
                                                        color: const Color(0xff0984e3),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: [
                                                      RotatedBox(quarterTurns: 3,child: Text("${dataSummaryTeacher.dataGradeStage![index].level![indexGrade].female}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                      const SizedBox(height: 3,),
                                                      AnimatedContainer(
                                                        duration:const Duration(milliseconds: 300),
                                                        height:animateChartsBar == false?0: (dataSummaryTeacher.dataGradeStage![index].level![indexGrade].female! / (totalGrapChartsHeightGradeStudent+11))*(totalContainerChartsBar-68),
                                                        width: 30,
                                                        color: const Color(0xfffd79a8),
                                                      ),
                                                    ],
                                                  ),
                                                
                                              ]
                                            ),
                                          )
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          children: [
                                            Row(
                                              children: List.generate(dataSummaryTeacher.dataGradeStage![index].level!.length, (indexGrade) => Container(
                                                height: 2,
                                                width: 100,
                                                color: Colorconstand.neutralDarkGrey,
                                                )
                                              )
                                            ),
                                            Container(
                                              height: 60,
                                              child:Row(
                                                children: List.generate(dataSummaryTeacher.dataGradeStage![index].level!.length, (indexGrade) => Container(width: 100,child: Transform.rotate(
                                                                                    angle: -math.pi / 4,child: Text("${"GRADED".tr()} ${dataSummaryTeacher.dataGradeStage![index].level![indexGrade].level.toString().tr()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)))),
                                              )
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                            Container(
                              margin:const EdgeInsets.only(bottom: 18),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        width: 15,height: 15,
                                        color: const Color(0xFFF1A713),
                                      ),
                                      const SizedBox(width: 5,),
                                      Text("TOTAL".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xFFF1A713),),),
                                    ],
                                  ),
                                  Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 12),
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 15,height: 15,
                                          color: const Color(0xff0984e3),
                                        ),
                                        const SizedBox(width: 5,),
                                        Text("MALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xff0984e3),),),
                                      ],
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 15,height: 15,
                                        color: const Color(0xfffd79a8),
                                      ),
                                      const SizedBox(width: 5,),
                                      Text("FEMALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xfffd79a8),),),
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                )
              ],
            );
        }
        else{
          return Container();
        }
      }, 
    );
  }
}