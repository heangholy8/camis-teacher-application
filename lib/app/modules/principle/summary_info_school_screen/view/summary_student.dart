import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/state/summary_study_student/summary_student_result_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pie_chart/pie_chart.dart';
import 'dart:math' as math;
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../state/summary_student_teacher_in_class/summary_student_teacher_in_class_bloc.dart';

class SummaryStudent extends StatefulWidget {
  const SummaryStudent({super.key});

  @override
  State<SummaryStudent> createState() => _SummaryStudentState();
}

class _SummaryStudentState extends State<SummaryStudent> {

  double totalContainerChartsBar = 350;
  bool animateChartsBar = false;
  double totalGrapChartsHeightGradeStudent = 0;
  double totalGrapChartsHeightRankNumberGradeStudent = 0;

  double totalGrapChartsHeightStudent = 0;
  double totalGrapChartsHeightRankNumberStudent = 0;

  int indexGradeResult = 0;
  String gradeName = "";

  final colorList = <Color>[
    const Color(0xff0984e3),
    const Color(0xfffd79a8),
    // const Color(0xfffd79a8),
    ];
  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 300),(){
      setState(() {
        animateChartsBar = true;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocConsumer<SummaryStudentTeacherInClassBloc, SummaryStudentTeacherInClassState>(
          listener: (context, state) {
            if(state is SummaryStudentTeacherInClassLoaded){
              
            }
          },
          builder: (context, state) {
            if(state is SummaryStudentTeacherInClassLoaded){
              var dataSummaryStudent = state.studentAndTeacherInClass.data!.student;
              var dataMap = <String, double>{
                "MALE".tr(): dataSummaryStudent!.male!.toDouble(),
                "FEMALE".tr(): dataSummaryStudent.female!.toDouble(),
              };
              for(int i =0;i < dataSummaryStudent.educationStage!.length;i++){
                if(dataSummaryStudent.educationStage![i].total! > totalGrapChartsHeightStudent){
                    totalGrapChartsHeightStudent = dataSummaryStudent.educationStage![i].total!.toDouble();
                    totalGrapChartsHeightRankNumberStudent = (totalGrapChartsHeightStudent+100)/4.toDouble();
                }
              }
              for(int i =0;i < dataSummaryStudent.dataGradeStage!.length;i++){
                for(int j =0;j < dataSummaryStudent.dataGradeStage![i].level!.length;j++){
                  if(dataSummaryStudent.dataGradeStage![i].level![j].total! > totalGrapChartsHeightGradeStudent){
                    totalGrapChartsHeightGradeStudent = dataSummaryStudent.dataGradeStage![i].level![j].total!.toDouble();
                    totalGrapChartsHeightRankNumberGradeStudent = (totalGrapChartsHeightGradeStudent+100)/4.toDouble();
                  }
                }
              }
              return Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width-36,
                      height: MediaQuery.of(context).size.width-130,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: Colorconstand.gray300
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin:const EdgeInsets.only(top:16),
                            child:Text("ចំនួនសិស្សសរុបនៅក្នុងសាលា (${dataSummaryStudent.total} ${"PEOPLE".tr()})",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.center,),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width-100,
                            padding:const EdgeInsets.all(12),
                            child: PieChart(
                              chartLegendSpacing: 20,
                              dataMap: dataMap,
                              chartType: ChartType.disc,
                              baseChartColor: Colorconstand.primaryColor,
                              colorList: colorList,
                              chartValuesOptions: const ChartValuesOptions(
                                showChartValuesInPercentage: false,
                              ),
                              totalValue: dataSummaryStudent.total!.toDouble(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin:const EdgeInsets.all(18),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: Colorconstand.gray300
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin:const EdgeInsets.only(top:16),
                            child:const Text("ចំនួនសិស្សសរុបនៅក្នុងសាលា",style: ThemsConstands.headline_4_semibold_18,),
                          ),
                          Container(
                            padding:const EdgeInsets.all(18),
                            height: totalContainerChartsBar,
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              children: [
                                Column(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Column(
                                              children: [
                                                Container(
                                                  child:Text("${(totalGrapChartsHeightStudent+200).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                ),
                                                Expanded(
                                                  child: Container(),
                                                ),
                                                Container(
                                                  child:Text("${(totalGrapChartsHeightStudent+200 - (totalGrapChartsHeightRankNumberStudent)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                ),
                                                Expanded(
                                                  child: Container(),
                                                ),
                                                Container(
                                                  child:Text("${(totalGrapChartsHeightStudent+200 - (totalGrapChartsHeightRankNumberStudent*2)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                ),
                                                Expanded(
                                                  child: Container(),
                                                ),
                                                Container(
                                                  child: Text("${(totalGrapChartsHeightStudent+200 - (totalGrapChartsHeightRankNumberStudent*3)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                ),
                                                Expanded(
                                                  child: Container(),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                Container(
                                                  height: 2,width: 6,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    width: 2,
                                                    color: Colorconstand.neutralDarkGrey,
                                                  ),
                                                ),
                                                Container(
                                                  height: 2,width: 6,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    width: 2,
                                                    color: Colorconstand.neutralDarkGrey,
                                                  ),
                                                ),
                                                Container(
                                                  height: 2,width: 6,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    width: 2,
                                                    color: Colorconstand.neutralDarkGrey,
                                                  ),
                                                ),
                                                Container(
                                                  height: 2,width: 6,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    width: 2,
                                                    color: Colorconstand.neutralDarkGrey,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 40,
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: List.generate(dataSummaryStudent.educationStage!.length, (index) => 
                                            Expanded(child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children:[
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  children: [
                                                    RotatedBox(quarterTurns: 4,child: Text("${dataSummaryStudent.educationStage![index].total}",style:const TextStyle(fontSize: 10,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)),
                                                  //const SizedBox(height: 3,),
                                                    AnimatedContainer(
                                                      duration:const Duration(milliseconds: 300),
                                                      alignment: Alignment.bottomCenter,
                                                      height:animateChartsBar == false?0:(dataSummaryStudent.educationStage![index].total! / (totalGrapChartsHeightStudent+200))*(totalContainerChartsBar-78),
                                                      width: 30,
                                                      color: const Color(0xFFF1A713),
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  children: [
                                                    RotatedBox(quarterTurns: 3,child: Text("${dataSummaryStudent.educationStage![index].male}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                    const SizedBox(height: 3,),
                                                    AnimatedContainer(
                                                      duration:const Duration(milliseconds: 300),
                                                      height:animateChartsBar == false?0: (dataSummaryStudent.educationStage![index].male! / (totalGrapChartsHeightStudent+200))*(totalContainerChartsBar-78),
                                                      width: 30,
                                                      color: const Color(0xff0984e3),
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  children: [
                                                    RotatedBox(quarterTurns: 3,child: Text("${dataSummaryStudent.educationStage![index].female}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                    const SizedBox(height: 3,),
                                                    AnimatedContainer(
                                                      duration:const Duration(milliseconds: 300),
                                                      height:animateChartsBar == false?0: (dataSummaryStudent.educationStage![index].female! / (totalGrapChartsHeightStudent+200))*(totalContainerChartsBar-78),
                                                      width: 30,
                                                      color: const Color(0xfffd79a8),
                                                    ),
                                                  ],
                                                ),
                                              ]
                                            ))
                                            ),
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            children: [
                                              Container(
                                                height: 2,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Container(
                                                height: 40,
                                                width: MediaQuery.of(context).size.width-36,
                                                child:Row(
                                                  children: List.generate(dataSummaryStudent.educationStage!.length, (index) => Expanded(child: Text("${dataSummaryStudent.educationStage![index].name.toString().tr()}\n(${dataSummaryStudent.educationStage![index].total} ${"PEOPLE".tr()})",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,))),
                                                )
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin:const EdgeInsets.only(bottom: 18),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      width: 15,height: 15,
                                      color: const Color(0xFFF1A713),
                                    ),
                                    const SizedBox(width: 5,),
                                    Text("TOTAL".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xFFF1A713),),),
                                  ],
                                ),
                                Container(
                                  margin:const EdgeInsets.symmetric(horizontal: 12),
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 15,height: 15,
                                        color: const Color(0xff0984e3),
                                      ),
                                      const SizedBox(width: 5,),
                                      Text("MALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xff0984e3),),),
                                    ],
                                  ),
                                ),
                                Row(
                                  children: [
                                    Container(
                                      width: 15,height: 15,
                                      color: const Color(0xfffd79a8),
                                    ),
                                    const SizedBox(width: 5,),
                                    Text("FEMALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xfffd79a8),),),
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: ListView.builder(
                        padding:const EdgeInsets.all(0),
                        shrinkWrap: true,
                        physics:const ScrollPhysics(),
                        itemCount: dataSummaryStudent.dataGradeStage!.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin:const EdgeInsets.only(top: 0,left: 18,right: 18,bottom: 18),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              color: Colorconstand.gray300
                            ),
                            child: Column(
                              children: [
                                Container(
                                  margin:const EdgeInsets.only(top:16),
                                  child: Text("ចំនួនសិស្សសរុប \n${dataSummaryStudent.dataGradeStage![index].name!.tr()} (${dataSummaryStudent.educationStage![index].total!} ${"PEOPLE".tr()})",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.center,),
                                ),
                                Container(
                                  padding:const EdgeInsets.all(18),
                                  height: totalContainerChartsBar,
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    children: [
                                      Column(
                                        children: [
                                          Expanded(
                                            child: Container(
                                              child: Row(
                                                children: [
                                                  Column(
                                                    children: [
                                                      Container(
                                                        child:Text("${(totalGrapChartsHeightGradeStudent+100).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                      ),
                                                      Expanded(
                                                        child: Container(),
                                                      ),
                                                      Container(
                                                        child:Text("${((totalGrapChartsHeightGradeStudent+100) - (totalGrapChartsHeightRankNumberGradeStudent)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                      ),
                                                      Expanded(
                                                        child: Container(),
                                                      ),
                                                      Container(
                                                        child:Text("${((totalGrapChartsHeightGradeStudent+100) - (totalGrapChartsHeightRankNumberGradeStudent*2)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                      ),
                                                      Expanded(
                                                        child: Container(),
                                                      ),
                                                      Container(
                                                        child: Text("${((totalGrapChartsHeightGradeStudent+100)- (totalGrapChartsHeightRankNumberGradeStudent*3)).toInt()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                                      ),
                                                      Expanded(
                                                        child: Container(),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    children: [
                                                      Container(
                                                        height: 2,width: 6,
                                                        color: Colorconstand.neutralDarkGrey,
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          width: 2,
                                                          color: Colorconstand.neutralDarkGrey,
                                                        ),
                                                      ),
                                                      Container(
                                                        height: 2,width: 6,
                                                        color: Colorconstand.neutralDarkGrey,
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          width: 2,
                                                          color: Colorconstand.neutralDarkGrey,
                                                        ),
                                                      ),
                                                      Container(
                                                        height: 2,width: 6,
                                                        color: Colorconstand.neutralDarkGrey,
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          width: 2,
                                                          color: Colorconstand.neutralDarkGrey,
                                                        ),
                                                      ),
                                                      Container(
                                                        height: 2,width: 6,
                                                        color: Colorconstand.neutralDarkGrey,
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          width: 2,
                                                          color: Colorconstand.neutralDarkGrey,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Container(
                                            height: 60,
                                          ),
                                        ],
                                      ),
                                      Expanded(
                                        child: SingleChildScrollView(
                                        physics:const ClampingScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                          child: Column(
                                            children: [
                                              Expanded(
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  children: List.generate(dataSummaryStudent.dataGradeStage![index].level!.length, (indexGrade) => 
                                                  Container(
                                                    width: 100,
                                                    child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children:[
                                                      Column(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                        children: [
                                                          RotatedBox(quarterTurns: 3,child: Text("${dataSummaryStudent.dataGradeStage![index].level![indexGrade].total}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                          const SizedBox(height: 3,),
                                                          AnimatedContainer(
                                                            duration:const Duration(milliseconds: 300),
                                                            alignment: Alignment.bottomCenter,
                                                            height:animateChartsBar == false?0:(dataSummaryStudent.dataGradeStage![index].level![indexGrade].total! / (totalGrapChartsHeightGradeStudent+150))*(totalContainerChartsBar-68),
                                                            width: 30,
                                                            color: const Color(0xFFF1A713),
                                                          ),
                                                        ],
                                                      ),
                                                      Column(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                        children: [
                                                          RotatedBox(quarterTurns: 3,child: Text("${dataSummaryStudent.dataGradeStage![index].level![indexGrade].male}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                          const SizedBox(height: 3,),
                                                          AnimatedContainer(
                                                            duration:const Duration(milliseconds: 300),
                                                            height:animateChartsBar == false?0: (dataSummaryStudent.dataGradeStage![index].level![indexGrade].male! / (totalGrapChartsHeightGradeStudent+150))*(totalContainerChartsBar-68),
                                                            width: 30,
                                                            color: const Color(0xff0984e3),
                                                          ),
                                                        ],
                                                      ),
                                                      Column(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                        children: [
                                                          RotatedBox(quarterTurns: 3,child: Text("${dataSummaryStudent.dataGradeStage![index].level![indexGrade].female}",style: ThemsConstands.headline_6_semibold_14,textAlign: TextAlign.center,)),
                                                          const SizedBox(height: 3,),
                                                          AnimatedContainer(
                                                            duration:const Duration(milliseconds: 300),
                                                            height:animateChartsBar == false?0: (dataSummaryStudent.dataGradeStage![index].level![indexGrade].female! / (totalGrapChartsHeightGradeStudent+150))*(totalContainerChartsBar-68),
                                                            width: 30,
                                                            color: const Color(0xfffd79a8),
                                                          ),
                                                        ],
                                                      ),
                                                    ]
                                                  ))
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      children: List.generate(dataSummaryStudent.dataGradeStage![index].level!.length, (indexGrade) => Container(
                                                          height: 2,
                                                          width: 100,
                                                          color: Colorconstand.neutralDarkGrey,
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: 60,
                                                      child:Row(
                                                        children: List.generate(dataSummaryStudent.dataGradeStage![index].level!.length, (indexGrade) => Transform.rotate(angle: -math.pi / 4,child: Container(width: 100,child: Text("${"GRADED".tr()} ${dataSummaryStudent.dataGradeStage![index].level![indexGrade].level.toString().tr()}",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)))),
                                                      )
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin:const EdgeInsets.only(bottom: 18),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: 15,height: 15,
                                            color: const Color(0xFFF1A713),
                                          ),
                                          const SizedBox(width: 5,),
                                          Text("TOTAL".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xFFF1A713),),),
                                        ],
                                      ),
                                      Container(
                                        margin:const EdgeInsets.symmetric(horizontal: 12),
                                        child: Row(
                                          children: [
                                            Container(
                                              width: 15,height: 15,
                                              color: const Color(0xff0984e3),
                                            ),
                                            const SizedBox(width: 5,),
                                            Text("MALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xff0984e3),),),
                                          ],
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            width: 15,height: 15,
                                            color: const Color(0xfffd79a8),
                                          ),
                                          const SizedBox(width: 5,),
                                          Text("FEMALE".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xfffd79a8),),),
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                );
            }
            else{
              return Container();
            }
          }, 
        ),
        BlocBuilder<SummaryStudentResultBloc,SummaryStudentResultState>(
          builder: (context, state) {
            if(state is SummaryStudentResultLoading){
              return const Center(child: CircularProgressIndicator(),);
            }
            else if(state is SummaryStudentResultLoaded){
              var data = state.summaryStudentStudyResult.data;
              return Column(
                children: [
                  Container(
                    padding:const EdgeInsets.symmetric(vertical: 0,horizontal: 12),
                    margin:const EdgeInsets.symmetric(horizontal: 18),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: Colorconstand.gray300
                    ),
                    child: MaterialButton(
                      padding:const EdgeInsets.all(0),
                      onPressed: () {
                        showModalBottomSheet(
                          shape:const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(18),topRight: Radius.circular(18),),
                          ),
                          isScrollControlled: true,
                          context: context, builder:(context) {
                          return StatefulBuilder(
                            builder: (context, snapshot) {
                              return SingleChildScrollView(
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(18),topRight: Radius.circular(18),)),
                                  child: Container(
                                    margin:const EdgeInsets.only(top: 0,bottom: 0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          width: MediaQuery.of(context).size.width,
                                          height: 55,
                                          decoration:const BoxDecoration(color: Colors.transparent,borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))),
                                          child: Text("ជ្រើសរើសកម្រិត",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          child: GridView.builder(
                                          shrinkWrap: true,
                                          physics:const ScrollPhysics(),
                                          padding:const EdgeInsets.only(top: 25,left: 28,right: 18,bottom: 28),
                                            itemCount: data!.length,
                                            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3,mainAxisExtent: 50),
                                            itemBuilder: (BuildContext context, int index) {
                                              return GestureDetector(
                                                onTap: (){
                                                  setState(() {
                                                    gradeName = data[index].level.toString();
                                                    indexGradeResult = index;
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(16),
                                                    color: index == indexGradeResult?Colorconstand.primaryColor:Colors.transparent
                                                  ),
                                                  child: Text("${"GRADED".tr()} ${data[index].level.toString().tr()}",style: ThemsConstands.headline_4_medium_18.copyWith(color: index != indexGradeResult?Colorconstand.lightBlack:Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                                ),
                                              );
                                            }
                                          )
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }
                          );
                        });
                                            
                      },
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("សង្ខេបលទ្ធផលសិស្ស",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.left,),
                            Container(
                              padding:const EdgeInsets.symmetric(horizontal: 6,vertical: 2),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: Colorconstand.mainColorSecondary
                              ),
                              child: Row(
                                children: [
                                  Text( gradeName==""?"${"GRADED".tr()} ${data![0].level}":"${"GRADED".tr()} $gradeName",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                  const Icon(Icons.keyboard_arrow_down_outlined,size: 28,color: Colorconstand.neutralWhite,)
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin:const EdgeInsets.all(18),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: Colorconstand.gray300
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.only(top:16),
                          child: Text("${"GRADED".tr()} ${data![indexGradeResult].level}",style: ThemsConstands.headline_4_semibold_18,),
                        ),
                        Container(
                          padding:const EdgeInsets.only(top: 18,left: 18,right: 18,bottom: 5),
                          height: totalContainerChartsBar,
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Row(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                child:const Text("140",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:const Text("120",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:const Text("100",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:const Text("80",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:const Text("60",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:const Text("40",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Container(
                                                child:const Text("20",style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)
                                              ),
                                              Expanded(
                                                child: Container(),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: [
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                              Container(
                                                height: 2,width: 6,
                                                color: Colorconstand.neutralDarkGrey,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: 2,
                                                  color: Colorconstand.neutralDarkGrey,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 60,
                                  ),
                                ],
                              ),
                              Expanded(
                                child: SingleChildScrollView(
                                  physics:const ClampingScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  child: Column(
                                    children: [
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: List.generate(data[indexGradeResult].result!.length, (index) => 
                                          Container(
                                            width: 60,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children:[
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  children: [
                                                    RotatedBox(quarterTurns: 3,child: Text("${(data[indexGradeResult].result![index].fail!.percent! *100).toStringAsFixed(1)}%",style: ThemsConstands.headline_6_semibold_14.copyWith(color:const Color(0xFFED2D2D)),textAlign: TextAlign.center,)),
                                                    const SizedBox(height: 3,),
                                                    AnimatedContainer(
                                                      duration:const Duration(milliseconds: 300),
                                                      alignment: Alignment.bottomCenter,
                                                      height:animateChartsBar == false?0:((data[indexGradeResult].result![index].fail!.percent! *100) / 140)*(totalContainerChartsBar-85),
                                                      width: 25,
                                                      color: const Color(0xFFED2D2D),
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  children: [
                                                    RotatedBox(quarterTurns: 3,child: Text("${(data[indexGradeResult].result![index].pass!.percent! *100).toStringAsFixed(1)}%",style: ThemsConstands.headline_6_semibold_14.copyWith(color:const Color(0xff0984e3)),textAlign: TextAlign.center,)),
                                                    const SizedBox(height: 3,),
                                                    AnimatedContainer(
                                                      duration:const Duration(milliseconds: 300),
                                                      height:animateChartsBar == false?0: ((data[indexGradeResult].result![index].pass!.percent!*100) / 140)*(totalContainerChartsBar-85),
                                                      width: 25,
                                                      color: const Color(0xff0984e3),
                                                    ),
                                                  ],
                                                ),
                                                
                                              ]
                                            ),
                                          )
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          children: [
                                            Row(
                                              children: List.generate(data[indexGradeResult].result!.length, (indexMonth) => Container(
                                                height: 2,
                                                width: 60,
                                                color: Colorconstand.neutralDarkGrey,
                                                )
                                              )
                                            ),
                                            Container(
                                              height: 60,
                                              child:Row(
                                                children: List.generate(data[indexGradeResult].result!.length, (indexMonth) => Container(width: 60,child: Transform.rotate(
                                                                                    angle: -math.pi / 4,child: Text(data[0].result![indexMonth].name.toString().tr(),style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)))),
                                              )
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin:const EdgeInsets.only(bottom: 18),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: 15,height: 15,
                                    color: const Color(0xFFED2D2D),
                                  ),
                                  const SizedBox(width: 5,),
                                  Text("FAIL_EXAM".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xFFF1A713),),),
                                ],
                              ),
                              const SizedBox(width: 12,),
                              Row(
                                children: [
                                  Container(
                                    width: 15,height: 15,
                                    color: const Color(0xff0984e3),
                                  ),
                                  const SizedBox(width: 5,),
                                  Text("PASS".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: const Color(0xff0984e3),),),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              );
            }
            else{
              return Container(
                margin:const EdgeInsets.only(top: 50),
                child: Center(
                  child: EmptyWidget(
                    title: "WE_DETECT".tr(),
                    subtitle: "WE_DETECT_DES".tr(),
                  ),
                ),
              );
            }
          }, 
        )
      ],
    );
  }
}