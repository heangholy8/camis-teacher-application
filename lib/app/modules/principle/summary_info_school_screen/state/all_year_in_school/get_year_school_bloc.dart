import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/principle_model/all_year_in_school_model.dart';
import '../../../../../service/api/principle_api/summary_information_school/summary_information_school_api.dart';
part 'get_year_school_event.dart';
part 'get_year_school_state.dart';

class GetYearSchoolBloc extends Bloc<GetYearSchoolEvent, GetYearSchoolState> {
  GetSummaryInforSchoolApi getSummaryInforSchoolApi;
  GetYearSchoolBloc({required this.getSummaryInforSchoolApi}) : super(GetYearSchoolInitial()) {
    on<GetYearSchoolEvent>((event, emit) async{
      emit(GetYearSchoolLoading());
      try {
        final data = await getSummaryInforSchoolApi.getAllYearInschoolApi();
        emit(GetYearSchoolLoaded(allYearInschool: data));
      } catch (e) {
        print(e.toString());
        emit(GetYearSchoolError());
      }
    });
  }
}
