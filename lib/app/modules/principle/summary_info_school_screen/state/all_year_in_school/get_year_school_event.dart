part of 'get_year_school_bloc.dart';

class GetYearSchoolEvent extends Equatable {
  const GetYearSchoolEvent();

  @override
  List<Object> get props => [];
}

class GetAllYearInSchoolEvent extends GetYearSchoolEvent{}
