part of 'get_year_school_bloc.dart';

class GetYearSchoolState extends Equatable {
  const GetYearSchoolState();
  
  @override
  List<Object> get props => [];
}

class GetYearSchoolInitial extends GetYearSchoolState {}

class GetYearSchoolLoading extends GetYearSchoolState {}
class GetYearSchoolLoaded extends GetYearSchoolState {
  AllYearInschool allYearInschool;
  GetYearSchoolLoaded({required this.allYearInschool});
}
class GetYearSchoolError extends GetYearSchoolState {}
