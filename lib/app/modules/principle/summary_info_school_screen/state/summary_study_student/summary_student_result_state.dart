part of 'summary_student_result_bloc.dart';

class SummaryStudentResultState extends Equatable {
  const SummaryStudentResultState();
  
  @override
  List<Object> get props => [];
}


class SummaryStudentResultInitial extends SummaryStudentResultState {}
class SummaryStudentResultLoading extends SummaryStudentResultState {}
class SummaryStudentResultLoaded extends SummaryStudentResultState {
  final SummaryStudentStudyResult summaryStudentStudyResult;
  const SummaryStudentResultLoaded({required this.summaryStudentStudyResult});
}
class SummaryStudentResultError extends SummaryStudentResultState {}
