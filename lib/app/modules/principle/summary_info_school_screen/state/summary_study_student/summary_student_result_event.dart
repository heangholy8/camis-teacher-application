part of 'summary_student_result_bloc.dart';

class SummaryStudentResultEvent extends Equatable {
  const SummaryStudentResultEvent();

  @override
  List<Object> get props => [];
}

class GetSummaryStudentResult extends SummaryStudentResultEvent{}
