import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/principle_model/summary_student_study_result_model.dart';
import '../../../../../service/api/principle_api/summary_information_school/summary_information_school_api.dart';
part 'summary_student_result_event.dart';
part 'summary_student_result_state.dart';

class SummaryStudentResultBloc extends Bloc<SummaryStudentResultEvent, SummaryStudentResultState> {
  GetSummaryInforSchoolApi getSummaryInforSchoolApi;
  SummaryStudentResultBloc({required this.getSummaryInforSchoolApi}) : super(SummaryStudentResultInitial()) {
    on<GetSummaryStudentResult>((event, emit) async{
      emit(SummaryStudentResultLoading());
      try {
        final data = await getSummaryInforSchoolApi.getSummaryStudentStudyResultApi();
        emit(SummaryStudentResultLoaded(summaryStudentStudyResult: data));
      } catch (e) {
        print(e.toString());
        emit(SummaryStudentResultError());
      }
    });
  }
}
