import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/principle_model/summary_student_teacher_model.dart';
import '../../../../../service/api/principle_api/summary_information_school/summary_information_school_api.dart';
part 'summary_student_teacher_in_class_event.dart';
part 'summary_student_teacher_in_class_state.dart';

class SummaryStudentTeacherInClassBloc extends Bloc<SummaryStudentTeacherInClassEvent, SummaryStudentTeacherInClassState> {
  GetSummaryInforSchoolApi getSummaryInforSchoolApi;
  SummaryStudentTeacherInClassBloc({required this.getSummaryInforSchoolApi}) : super(SummaryStudentTeacherInClassInitial()) {
    on<GetSummaryStudentTeacherInSchool>((event, emit) async{
      emit(SummaryStudentTeacherInClassLoading());
      try {
        final data = await getSummaryInforSchoolApi.getSrudentAndTeacherInClassApi(
          idSchoolYear:event.schoolYearId
        );
        emit(SummaryStudentTeacherInClassLoaded(studentAndTeacherInClass: data));
      } catch (e) {
        print(e.toString());
        emit(SummaryStudentTeacherInClassError());
      }
    });
  }
}
