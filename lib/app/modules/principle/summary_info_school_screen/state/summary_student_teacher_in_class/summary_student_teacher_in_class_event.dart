part of 'summary_student_teacher_in_class_bloc.dart';

class SummaryStudentTeacherInClassEvent extends Equatable {
  const SummaryStudentTeacherInClassEvent();

  @override
  List<Object> get props => [];
}

class GetSummaryStudentTeacherInSchool extends SummaryStudentTeacherInClassEvent{
  final String schoolYearId;
  const GetSummaryStudentTeacherInSchool({required this.schoolYearId});
}
