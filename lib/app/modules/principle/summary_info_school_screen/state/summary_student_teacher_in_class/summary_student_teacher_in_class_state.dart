part of 'summary_student_teacher_in_class_bloc.dart';

class SummaryStudentTeacherInClassState extends Equatable {
  const SummaryStudentTeacherInClassState();
  
  @override
  List<Object> get props => [];
}

class SummaryStudentTeacherInClassInitial extends SummaryStudentTeacherInClassState {}

class SummaryStudentTeacherInClassLoading extends SummaryStudentTeacherInClassState {}
class SummaryStudentTeacherInClassLoaded extends SummaryStudentTeacherInClassState {
  SrudentAndTeacherInClass studentAndTeacherInClass;
  SummaryStudentTeacherInClassLoaded({required this.studentAndTeacherInClass});
   
}
class SummaryStudentTeacherInClassError extends SummaryStudentTeacherInClassState {}
