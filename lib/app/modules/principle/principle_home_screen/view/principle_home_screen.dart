// ignore_for_file: avoid_unnecessary_containers
import 'dart:async';
import 'package:camis_teacher_application/app/bloc/check_update_version/bloc/check_version_update_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/list_class_check_attendance_screen/state/list_check_attendance_in_class_dashboard/bloc/get_list_attendance_dashboard_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/principle_home_screen/state/bloc/get_teacher_attendance_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/report/view/principle_report_screen.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/state/current_grade/bloc/current_grade_bloc.dart';
import 'package:camis_teacher_application/app/modules/time_line/time_line.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../widget/comfirm_information/comfirm_phoneNumber.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar_principle.dart';
import '../../../../../widget/widge_update.dart';
import '../../../../help/check_platform_device.dart';
import '../../../../models/grading_model/grading_model.dart';
import '../../../../storages/get_storage.dart';
import '../../../../storages/key_storage.dart';
import '../../../../storages/user_storage.dart';
import '../../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../../home_screen/e.homscreen.dart';
import '../../../home_screen/widget/button_quick_action_widget.dart';
import '../../../student_list_screen/widget/contact_pairent_widget.dart';
import '../../../study_affaire_screen/list_attendance_teacher/view/list_attendance_teacher_screen.dart';
import '../../../study_affaire_screen/list_class_in_grade/view/list_class_in_grade_screen.dart';
import '../../list_check_class_input_score_screen/view/check_class_input_score_screen.dart';
import '../../list_class_check_attendance_screen/view/list_class_check_attendance_screen.dart';

class PrincipleHomeScreen extends StatefulWidget {
  const PrincipleHomeScreen({Key? key}) : super(key: key);

  @override
  State<PrincipleHomeScreen> createState() => _PrincipleHomeScreenState();
}

class _PrincipleHomeScreenState extends State<PrincipleHomeScreen> {
  
 ///=========== Update Version ==============
  //////========= Change before update success =========
  final KeyStoragePref keyPref = KeyStoragePref();
  UserSecureStroage saveStoragePref = UserSecureStroage();
  bool updateVersion = false;
  String currentVersionIos = "2.5.0";
  String currentVersionAndroid = "2.5.0";
  ///========= Update success change in postman =========
  String releaseDateVersionIos = "28/Oct/2024";
  String releaseDateVersionAndroid = "28/Oct/2024";
///=========== Update Version ==============

  PageController? pagecMyclass;
  PageController? pagecNotification;
  double activeindexNotification = 0;
  double activeindexMyclass = 0;
  String? schoolName;
  String? instructorName;
  String? phoneNumber;
  String? phoneNumberInStoreLocal;
  int gender = 0;
  int? monthlocal;
  String? monthlocalCheck;
  int? daylocal;
  int? yearchecklocal;

  bool isLoading = false;
  String isInstructor= "";
  
  GradingData data = GradingData();
  List<StudentsData> studentData = [];

  StreamSubscription? internetconnection;
  bool isoffline = true;

  // Local Storage
  final GetStoragePref _getStoragePref = GetStoragePref();

  // Datetime && Timer
  DateTime now = DateTime.now();
  StreamSubscription? sub;
  bool isDisplay = true;
  bool showComfirmPhone = false;

  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  void getPhoneStore() async {
    final KeyStoragePref keyPref = KeyStoragePref();
    final SharedPreferences pref = await SharedPreferences.getInstance();
      phoneNumberInStoreLocal = pref.getString(keyPref.keyPhonePref) ?? "";
    if(phoneNumberInStoreLocal == ""|| phoneNumberInStoreLocal==null){
      setState(() {
        showComfirmPhone = true;
      });
    }
  }

  void getLocalData() async {

    var authStoragePref = await _getStoragePref.getJsonToken;
    setState(() {
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      schoolName = authStoragePref.schoolName.toString();
      if(authStoragePref.authUser == null){
      }
      else{
        instructorName = authStoragePref.authUser!.name;
        gender = authStoragePref.authUser!.gender;
        phoneNumber =  authStoragePref.authUser!.phone.toString();
      }
      
      if(monthlocal.toString().length == 1){
        monthlocalCheck = "0$monthlocal";
      }
      else{
        monthlocalCheck = monthlocal.toString();
      }
       BlocProvider.of<CheckVersionUpdateBloc>(context).add(CheckVersionUpdate());
       BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
       BlocProvider.of<GetListAttendanceDashboardBloc>(context).add(GetClassCheckAttDashboardEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/$monthlocalCheck/$yearchecklocal"));
       BlocProvider.of<GetTeacherAttendanceBloc>(context).add(GetAttTeacherDashboardEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/$monthlocalCheck/$yearchecklocal"));
    });
  }

  @override
  void initState() {
    getPhoneStore();
    getLocalData();
     //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true) {
          }
        });
      });
      //=============Eend Check internet===============
      //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================
    super.initState();
     if(isoffline == true){
      }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      bottomNavigationBar: isoffline==false || updateVersion == true ?Container(height: 0,):const BottomNavigateBarPrinciple(
        isActive: 1,
      ),
      backgroundColor: const Color.fromARGB(255, 247, 248, 249),
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Container(
          color: Colorconstand.primaryColor,
          child: Stack(
            children: [
              Positioned(
                right: 0,
                top: 0,
                child: Container(
                  child: SvgPicture.asset("assets/images/Path_home_1.svg"),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: SafeArea(
                  bottom: false,
                  child: Container(
                    margin: const EdgeInsets.only(top: 0),
                    child: Column(
                      children: [
                        headerWidget(translate: translate),
                        const SizedBox(height: 22,),
                        Expanded(
                          child: BlocListener<CheckVersionUpdateBloc, CheckVersionUpdateState>(
                            listener: (context, state) {
                              if(state is CheckVersionUpdateLoaded){
                                  var data = state.checkUpdateModel.data;
                                  var plateform = checkPlatformDevice();
                                  if(plateform=="Android"){
                                    if(data!.versionAndroid.toString() == currentVersionAndroid || data.releaseDateAndroid == releaseDateVersionAndroid){
                                        setState(() {
                                          updateVersion = false;
                                        });
                                    }
                                    else{
                                       setState(() {
                                          updateVersion = true;
                                        });
                                    }
                                  }
                                  if(plateform=="ios"){
                                    if(data!.versionIos.toString() == currentVersionIos || data.releaseDateIos == releaseDateVersionIos){
                                        setState(() {
                                          updateVersion = false;
                                        });
                                    }
                                    else{
                                       setState(() {
                                          updateVersion = true;
                                        });
                                    }
                                  }
                                }
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                    color: Colorconstand.neutralWhite,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(16),
                                      topRight: Radius.circular(16),
                                    ),
                                  ),
                              child: Stack(
                                children: [
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    bottom: 0,
                                    child: Container(
                                      child: SvgPicture.asset("assets/images/back_daskboard_left.svg"),
                                    ),
                                  ),
                                  Positioned(
                                    left: 0,
                                    bottom: 0,
                                    child: Container(
                                      child: SvgPicture.asset("assets/images/back_daskboard_buttom.svg"),
                                    ),
                                  ),
                                  Positioned(
                                    top: 0,
                                    left: 0,
                                    right: 0,
                                    bottom: 0,
                                    child: BlurryContainer(
                                      borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16),
                                      ),
                                      padding: const EdgeInsets.all(0),
                                      blur: 35,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colorconstand.neutralWhite.withOpacity(0.5),
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(16),
                                            topRight: Radius.circular(16),
                                          ),
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          margin: const EdgeInsets.only(top: 5),   
                                          child: RefreshIndicator(
                                            onRefresh: () async {
                                              BlocProvider.of<GetListAttendanceDashboardBloc>(context).add(GetClassCheckAttDashboardEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/$monthlocalCheck/$yearchecklocal"));
                                              BlocProvider.of<GetTeacherAttendanceBloc>(context).add(GetAttTeacherDashboardEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/$monthlocalCheck/$yearchecklocal"));
                                            },
                                            child: SingleChildScrollView(
                                              child: Column(
                                                children: [
                                                  // const Expanded(child: SizedBox()),
                                                  // Container(alignment: Alignment.center,child:const Text("ដំណើរការក្នុងពេលឆាប់ៗ...",style: ThemsConstands.headline_2_semibold_24,textAlign: TextAlign.center,),),
                                                  // const Expanded(child: SizedBox())
                                                  const SizedBox(height: 12,),
                                                  BlocBuilder<GetTeacherAttendanceBloc, GetTeacherAttendanceState>(
                                                    builder: (context, state) {
                                                      if(state is GetAttendanceTeacherSummaryDashboardLoading){
                                                        return Column(
                                                          children: [
                                                            Container(
                                                              margin:const EdgeInsets.symmetric(horizontal: 18),
                                                              child: Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Text("${"NOT_TAUGHT".tr()}៖",style: ThemsConstands.headline_4_semibold_18,),
                                                                  ),
                                                                  Container(
                                                                    child:const Icon(Icons.arrow_circle_right_outlined,color: Colorconstand.alertsDecline,),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                            Container(
                                                              margin:const EdgeInsets.all(18),
                                                              height: 90,
                                                              child: Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Shimmer.fromColors(
                                                                      baseColor: Colorconstand.neutralGrey,
                                                                      highlightColor:
                                                                          Colorconstand.neutralGrey.withOpacity(0.01),
                                                                      child: Container(
                                                                        decoration: BoxDecoration(
                                                                          color: Colors.white,
                                                                          borderRadius: BorderRadius.circular(16),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  const SizedBox(width: 18,),
                                                                  Expanded(
                                                                    child: Shimmer.fromColors(
                                                                      baseColor: Colorconstand.neutralGrey,
                                                                      highlightColor:
                                                                          Colorconstand.neutralGrey.withOpacity(0.01),
                                                                      child: Container(
                                                                        decoration: BoxDecoration(
                                                                          color: Colors.white,
                                                                          borderRadius: BorderRadius.circular(16),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        );
                                                      }
                                                      else if(state is GetAttendanceTeacherSummaryDashboardLoaded){
                                                        var dataAttTeacherDashboard = state.checkAttendanceSummaryTeacherModel.data;
                                                        return Column(
                                                          children: [
                                                            Container(
                                                              margin:const EdgeInsets.symmetric(horizontal: 18),
                                                              child: Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Text("${"NOT_TAUGHT".tr()}៖",style: ThemsConstands.headline_4_semibold_18,),
                                                                  ),
                                                                  GestureDetector(
                                                                    onTap: (){
                                                                      Navigator.pushReplacement(
                                                                        context,
                                                                        PageRouteBuilder(
                                                                          pageBuilder: (context, animation1, animation2) =>
                                                                            ListAttendanceTeacherScreen(principle: true),
                                                                          transitionDuration: Duration.zero,
                                                                          reverseTransitionDuration: Duration.zero,
                                                                        ),
                                                                      );
                                                                    },
                                                                    child:const Icon(Icons.arrow_circle_right_outlined,color: Colorconstand.alertsDecline,),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                            dataAttTeacherDashboard == null? 
                                                            Container(
                                                              alignment: Alignment.center,
                                                              height: 40,
                                                              child: Text("HAVE_NOT".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                            )
                                                            :Container(
                                                              child: Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Container(
                                                                      margin:const EdgeInsets.only(top: 12,left: 18,right: 8),
                                                                      decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(16),
                                                                        color: Colorconstand.alertsDecline,
                                                                      ),
                                                                      child: Container(
                                                                        padding:const EdgeInsets.all(1),
                                                                        child: Column(
                                                                          children: [
                                                                            Container(
                                                                              alignment: Alignment.center,
                                                                              height: 40,
                                                                              child: Text("TODAY".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                                            ),
                                                                            Container(
                                                                              alignment: Alignment.center,
                                                                              height: 45,
                                                                              decoration:const BoxDecoration(
                                                                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16),bottomRight: Radius.circular(16)),
                                                                                  color: Colorconstand.neutralWhite,
                                                                                ),
                                                                              child:Text(dataAttTeacherDashboard.currentDayCount == null?"--":dataAttTeacherDashboard.currentDayCount.toString(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.alertsDecline),),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                    child: Container(
                                                                      margin:const EdgeInsets.only(top: 12,left: 8,right: 18),
                                                                      decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(16),
                                                                        color: Colorconstand.alertsDecline,
                                                                      ),
                                                                      child: Container(
                                                                        padding:const EdgeInsets.all(1),
                                                                        child: Column(
                                                                          children: [
                                                                            Container(
                                                                              alignment: Alignment.center,
                                                                              height: 40,
                                                                              child: Text("AFFTER_DAY".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                                            ),
                                                                            Container(
                                                                              alignment: Alignment.center,
                                                                              height: 45,
                                                                              decoration:const BoxDecoration(
                                                                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16),bottomRight: Radius.circular(16)),
                                                                                  color: Colorconstand.neutralWhite,
                                                                                ),
                                                                              child:Text(dataAttTeacherDashboard.dayAfterCount == null?"--":dataAttTeacherDashboard.dayAfterCount.toString(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.alertsDecline),),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                            )
                                                          ],
                                                        );
                                                      }
                                                      else{
                                                        return Center(
                                                          child: EmptyWidget(
                                                            title: "WE_DETECT".tr(),
                                                            subtitle: "WE_DETECT_DES".tr(),
                                                          ),
                                                      );
                                                      }
                                                    },
                                                  ),
                                                  const SizedBox(height: 22,),
                                                  BlocBuilder<GetListAttendanceDashboardBloc, GetListAttendanceDashboardState>(
                                                    builder: (context, state) {
                                                      if(state is GetClassCheckAttendanceDashboardLoading){
                                                        return Container(
                                                          child: Column(
                                                            children: [
                                                              Container(
                                                                margin:const EdgeInsets.symmetric(horizontal: 18),
                                                                child: Row(
                                                                  children: [
                                                                    Expanded(
                                                                      child: Text("${"CHECK_ATTENDANCE".tr()}៖",style: ThemsConstands.headline_4_semibold_18,),
                                                                    ),
                                                                    Container(
                                                                      child: Row(
                                                                        children: [
                                                                          Container(
                                                                            child: Text("----".tr(),style: ThemsConstands.headline_4_semibold_18,),
                                                                          ),
                                                                          Container(
                                                                            child:const Icon(Icons.arrow_circle_right_outlined,color: Colorconstand.primaryColor,),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              Container(
                                                                child: ListView.builder(
                                                                  itemCount: 5,
                                                                  padding:const EdgeInsets.all(0),
                                                                  shrinkWrap: true,
                                                                  itemBuilder: (context, index) {
                                                                    return Container(
                                                                      padding:const EdgeInsets.only(top: 16,left: 18,right: 18),
                                                                      height: 100,
                                                                      child: Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Shimmer.fromColors(
                                                                              baseColor: Colorconstand.neutralGrey,
                                                                              highlightColor:
                                                                                  Colorconstand.neutralGrey.withOpacity(0.01),
                                                                              child: Container(
                                                                                decoration: BoxDecoration(
                                                                                  color: Colors.white,
                                                                                  borderRadius: BorderRadius.circular(16),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    );
                                                                  },
                                                                )
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      }
                                                      if(state is GetClassCheckAttendanceDashboardLoaded){
                                                        var dataAttDashboard = state.listClassCheckAttendanceByHourModel.data;
                                                        return Column(
                                                          children: [
                                                            Container(
                                                              margin:const EdgeInsets.symmetric(horizontal: 18),
                                                              child: Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Text("${"CHECK_ATTENDANCE".tr()}៖",style: ThemsConstands.headline_4_semibold_18,),
                                                                  ),
                                                                  Container(
                                                                    child: Row(
                                                                      children: [
                                                                        Container(
                                                                          child: Text("${dataAttDashboard!.startTime.toString()}-${dataAttDashboard.endTime.toString()}",style: ThemsConstands.headline_4_semibold_18,),
                                                                        ),
                                                                        GestureDetector(
                                                                          onTap: (){
                                                                             Navigator.push(context, MaterialPageRoute(builder: (context)=> const ListClassCheckAttendanceScreen()));
                                                                          },
                                                                          child:const Icon(Icons.arrow_circle_right_outlined,color: Colorconstand.primaryColor,),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                            Container(
                                                              child: ListView.builder(
                                                                physics:const ScrollPhysics(),
                                                                shrinkWrap: true,
                                                                itemCount: dataAttDashboard.grade!.length,
                                                                itemBuilder: (context, indexGrade) {
                                                                  return  Container(
                                                                    margin:const EdgeInsets.only(top: 12,left: 18,right: 18),
                                                                    decoration: BoxDecoration(
                                                                      borderRadius: BorderRadius.circular(16),
                                                                      color: Colorconstand.primaryColor,
                                                                    ),
                                                                    child: Container(
                                                                      padding:const EdgeInsets.all(1),
                                                                      child: Column(
                                                                        children: [
                                                                          Container(
                                                                            alignment: Alignment.center,
                                                                            height: 40,
                                                                            child: Text("${"GRADED".tr()} ${dataAttDashboard.grade![indexGrade].grade.toString()}",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                                          ),
                                                                          Container(
                                                                            decoration:const BoxDecoration(
                                                                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16),bottomRight: Radius.circular(16)),
                                                                                color: Colorconstand.neutralWhite,
                                                                              ),
                                                                            child:  dataAttDashboard.grade![indexGrade].classes!.isEmpty? 
                                                                            Container(
                                                                              width: MediaQuery.of(context).size.width,
                                                                              alignment: Alignment.center,
                                                                              height: 60,
                                                                              child: Text("NO_CLASS".tr()),
                                                                            )
                                                                            :GridView.builder(
                                                                              shrinkWrap: true,
                                                                              physics:const NeverScrollableScrollPhysics(),
                                                                              padding:const EdgeInsets.all(12),
                                                                              itemCount: dataAttDashboard.grade![indexGrade].classes!.length,
                                                                              gridDelegate:const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,childAspectRatio: 1.8,mainAxisSpacing: 8,crossAxisSpacing: 8),
                                                                              itemBuilder: (BuildContext context, int indexclass) {
                                                                                return MaterialButton(
                                                                                  splashColor: Colors.transparent,
                                                                                  highlightColor: Colors.transparent,
                                                                                  padding:const EdgeInsets.all(0),
                                                                                  onPressed: () {
                                                                                    showModalBottomSheet(
                                                                                      shape: const RoundedRectangleBorder(
                                                                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                                                                                      ),
                                                                                      context: context,
                                                                                      builder: (context) {
                                                                                        return Column(
                                                                                          mainAxisSize: MainAxisSize.min,
                                                                                          children: [
                                                                                            Container(
                                                                                              padding:const EdgeInsets.symmetric(vertical: 16, horizontal: 28),
                                                                                              child: Text("CONTACT".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey,)),
                                                                                            ),
                                                                                            const Divider(
                                                                                              height: 1,
                                                                                              thickness: 0.5,
                                                                                            ),
                                                                                            ContactPairentWidget(
                                                                                              subjectName: dataAttDashboard.grade![indexGrade].classes![indexclass].subjectName.toString(),
                                                                                              phone: dataAttDashboard.grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                              name: dataAttDashboard.grade![indexGrade].classes![indexclass].teacherName.toString(),
                                                                                              onTap: (){
                                                                                                _launchLink("tel:${dataAttDashboard.grade![indexGrade].classes![indexclass].teacherPhone}");
                                                                                              },
                                                                                              profile: "",
                                                                                              role: dataAttDashboard.grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                            ),
                                                                                            
                                                                                          ],
                                                                                        );
                                                                                      }
                                                                                    );
                                                                                  },
                                                                                  child: Container(
                                                                                    alignment: Alignment.center,
                                                                                    decoration: BoxDecoration(
                                                                                      borderRadius: BorderRadius.circular(16),
                                                                                      color: dataAttDashboard.grade![indexGrade].classes![indexclass].isTakeAttendance == true?Colorconstand.primaryColor:Colorconstand.alertsDecline
                                                                                    ),
                                                                                    child: Text(dataAttDashboard.grade![indexGrade].classes![indexclass].name.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                                                  ),
                                                                                );
                                                                              },
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  );
                                                                },
                                                              ),
                                                            )
                                                          ],
                                                        );
                                                      }
                                                      else{
                                                        return Center(
                                                          child: EmptyWidget(
                                                            title: "WE_DETECT".tr(),
                                                            subtitle: "WE_DETECT_DES".tr(),
                                                          ),
                                                      );
                                                      }
                                                    },
                                                  ),
                                                  quickActionWidget()
                                            
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              isoffline == false || showComfirmPhone == true
                ?Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: Container(
                    color:const Color(0x55101010),
                  ),
                ):Container(height: 0,),
                showComfirmPhone == true?WidgetComfirmPhoneNumber(
                  gender: gender,
                  phoneNumber:phoneNumber == "0"?"":phoneNumber.toString(),
                  onComfirmSkip: (){
                    setState(() {
                      showComfirmPhone = false;
                      saveStoragePref.savePhoneNumber(keyPhoneNumber: "SKIP");
                    });
                  },
                  onComfirm: (){
                    setState(() {
                      showComfirmPhone = false;
                      if(phoneNumber == ""||phoneNumber==null || phoneNumber == "0"){
                      }
                      else{
                        saveStoragePref.savePhoneNumber(keyPhoneNumber:phoneNumber.toString());
                      }
                    });
                  },
                ):Container(height: 0,),
              AnimatedPositioned(
                bottom: isoffline == true ? -150 : 0,
                left: 0,
                right: 0,
                duration: const Duration(milliseconds: 500),
                child: const NoConnectWidget(),
              ),
              updateVersion==false?Container(): const Positioned(
                //top: 0,left: 0,right: 0,bottom: 0,
                child: WidgetUpdate(),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget emergencyNotificationWidget({required String translate}) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(
            top: 18,
            bottom: 18,
          ),
          height: 180,
          child: PageView.builder(
              controller: pagecNotification,
              scrollDirection: Axis.horizontal,
              itemCount: 4,
              onPageChanged: (index) {
                setState(() {
                  activeindexNotification = index.toDouble();
                });
              },
              itemBuilder: (context, index) {
                return const CardAnounment();
              }),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 16, top: 0),
          child: DotsIndicator(
            dotsCount: 4,
            position: activeindexNotification,
            decorator: DotsDecorator(
              color: Colorconstand.neutralWhite,
              activeColor: Colorconstand.mainColorForecolor,
              size: const Size(16.0, 6),
              activeSize: const Size(35.0, 6.0),
              activeShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
            ),
          ),
        ),
      ],
    );
  }

  Widget headerWidget({required String translate}) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 0),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 0),
              alignment: Alignment.centerLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: (){
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colorconstand.neutralWhite.withOpacity(0.2),
                              borderRadius: const BorderRadius.all(Radius.circular(10))),
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  child: Text(now.day.toString(),
                                      style: ThemsConstands.headline_4_medium_18.copyWith(
                                          color: Colorconstand.neutralWhite,)),
                                ),
                                const SizedBox(width: 8,),
                                Container(
                                  child: Text(
                                    translate == "km"
                                        ? monthNamesKh[now.month]
                                        : monthNamesEn[now.month],
                                    style: ThemsConstands.headline_4_medium_18.copyWith(
                                        color: Colorconstand.neutralWhite,),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 12,),
                      Expanded(
                        child: Container(
                          child: Text(
                            schoolName.toString(),
                            style: ThemsConstands.subtitle1_regular_16.copyWith(
                                color: Colorconstand.darkBackgroundsDisabled),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BlocConsumer<ProfileUserBloc, ProfileUserState>(
                    listener: (context, state) {
                      // TODO: implement listener
                    },
                    builder: (context, state) {
                      if (state is GetUserProfileLoading){
                        return Container(
                          child: Text("${"HELLO".tr()}......",style: ThemsConstands.headline3_semibold_20
                              .copyWith(color: Colorconstand.lightTextsRegular),),
                        );
                      }
                      else if(state is GetUserProfileLoaded){
                        var dataUser = state.profileModel!.data;
                        return Container(
                          child: Text(
                            gender == 1?"${"HELLO".tr()}${translate=="km"?"លោកគ្រូ":" Teacher"} ${translate == "km"?dataUser!.name:dataUser!.nameEn}"
                            :gender == 2?"${"HELLO".tr()}${translate=="km"?"អ្នកគ្រូ":" Teacher"} ${translate == "km"?dataUser!.name:dataUser!.nameEn}"
                                :"${"HELLO".tr()} ${translate == "km"?dataUser!.name:dataUser!.nameEn}",
                            style: ThemsConstands.headline3_semibold_20
                                .copyWith(color: Colorconstand.lightTextsRegular),
                            textAlign: TextAlign.left,overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }
                      else{
                        return Container(
                          child: Text("HELLO".tr(),style: ThemsConstands.headline3_semibold_20
                              .copyWith(color: Colorconstand.lightTextsRegular),),
                        );
                      }
                    },
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
      
        ],
      ),
    );
  }
  Widget quickActionWidget() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 14, bottom: 30, top: 22),
          alignment: Alignment.centerLeft,
          child: Text(
            "QUICKACTION".tr(),
            style: ThemsConstands.button_semibold_16
                .copyWith(color: Colorconstand.lightBlack, height: 1),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
            left: 14,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> ReportForPricipleAndStudyAffair(role: "1",)));
                    },
                    namebutton: "REPORT".tr(),
                    imageIcon: ImageAssets.folder_icon,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                       Navigator.push(context, MaterialPageRoute(builder: (context)=> const ListClassCheckAttendanceScreen()));
                    },
                    namebutton: "QUOTE_TAB_PRESENT".tr(),
                    imageIcon: ImageAssets.task_icon,
                  ),
                ),
              ),
              // Expanded(
              //   child: Container(
              //     child: ButttonQuickAction(
              //       onTap: () {
              //         BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradePriciple());
              //         Navigator.push(context, MaterialPageRoute(builder: (context)=> const ListClassInGradeScreen(routFromScreen: 1,)));
              //       },
              //       namebutton: "LEDGERSTUDENT".tr(),
              //       imageIcon: ImageAssets.people_icon
              //     ),
              //   ),
              // ),
              
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                      BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradePriciple());
                       Navigator.push(context, MaterialPageRoute(builder: (context)=> const ListClassInGradeScreen(routFromScreen: 2,)));
                    },
                    namebutton: "LEDGERTEACHER".tr(),
                    imageIcon: ImageAssets.profile_2users,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                      Navigator.push(context,
                        PageRouteBuilder(
                          pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 0,role: "1",isPrinciple: true,),
                          transitionDuration: Duration.zero,
                          reverseTransitionDuration: Duration.zero,
                        ),
                      );
                    },
                    namebutton: "TIMELINE".tr(),
                    imageIcon: ImageAssets.fill_timeline_icon,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
