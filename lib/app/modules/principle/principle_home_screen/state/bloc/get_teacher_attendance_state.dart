part of 'get_teacher_attendance_bloc.dart';

class GetTeacherAttendanceState extends Equatable {
  const GetTeacherAttendanceState();
  
  @override
  List<Object> get props => [];
}

class GetTeacherAttendanceInitial extends GetTeacherAttendanceState {}

class GetAttendanceTeacherSummaryDashboardLoading extends GetTeacherAttendanceState {}

class GetAttendanceTeacherSummaryDashboardLoaded extends GetTeacherAttendanceState {
  final CheckAttendanceSummaryTeacherModel checkAttendanceSummaryTeacherModel;
  const GetAttendanceTeacherSummaryDashboardLoaded({required this.checkAttendanceSummaryTeacherModel});
}
class GetAttendanceTeacherSummaryDashboardError extends GetTeacherAttendanceState {}
