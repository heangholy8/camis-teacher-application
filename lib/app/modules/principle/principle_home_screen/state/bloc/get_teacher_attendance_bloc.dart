import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/principle_model/summary_attendance_teacher_model.dart';
import '../../../../../service/api/principle_api/get_list_class_check_attendance/get_class_check_attendance_api.dart';
part 'get_teacher_attendance_event.dart';
part 'get_teacher_attendance_state.dart';

class GetTeacherAttendanceBloc extends Bloc<GetTeacherAttendanceEvent, GetTeacherAttendanceState> {
  final GetClassCheckAttApi getClassCheckAttApi;
  GetTeacherAttendanceBloc({required this.getClassCheckAttApi}) : super(GetTeacherAttendanceInitial()) {
    on<GetAttTeacherDashboardEvent>((event, emit) async{
      emit(GetAttendanceTeacherSummaryDashboardLoading());
      try {
        final data = await getClassCheckAttApi.getSummaryAttTeacherDashboardApi(
          date: event.date,
        );
        emit(GetAttendanceTeacherSummaryDashboardLoaded(checkAttendanceSummaryTeacherModel: data));
      } catch (e) {
        print(e.toString());
        emit(GetAttendanceTeacherSummaryDashboardError());
      }
    });
  }
}
