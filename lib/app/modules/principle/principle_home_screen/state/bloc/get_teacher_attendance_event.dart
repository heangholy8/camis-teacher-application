part of 'get_teacher_attendance_bloc.dart';

class GetTeacherAttendanceEvent extends Equatable {
  const GetTeacherAttendanceEvent();

  @override
  List<Object> get props => [];
}

class GetAttTeacherDashboardEvent extends GetTeacherAttendanceEvent{
  String date;
  GetAttTeacherDashboardEvent({required this.date});
}
