import 'package:audio_waveforms/audio_waveforms.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import '../../../../models/principle_model/feedback_model.dart';
import '../../../time_line/widget/view_image.dart';

class FeedbackDetailScreen extends StatefulWidget {
  final DataFeedback dataFeedback;
  const FeedbackDetailScreen({Key? key,required this.dataFeedback}) : super(key: key);

  @override
  State<FeedbackDetailScreen> createState() => _FeedbackDetailScreenState();
}

class _FeedbackDetailScreenState extends State<FeedbackDetailScreen> {
  TextEditingController comment = TextEditingController();
  //-------------------------------
  List<double> waveformData = [];
  String playDuration = "0:00";
  bool isplay = false;
  bool getVoice = true;
  String? path;
  String voicePath = "";
  List listImage = [];
  List listVoice = [];
  late Directory appDirectory;
  late final RecorderController recorderController;
  PlayerController playerController = PlayerController();

  Future<void> _getVoiebyLink(String url) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';
    try {
      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = path!;
        file = File(filePath);
        voicePath = filePath;
        await file.writeAsBytes(bytes);
      }
      else
        filePath = 'Error code: '+response.statusCode.toString();
    }
    catch(ex){
      filePath = 'Can not fetch url';
    }
    Future.delayed(Duration.zero,() async {
      waveformData = await playerController.extractWaveformData(
      path: filePath,
      noOfSamples: 100,);
      await playerController.preparePlayer(
      path: filePath,
      shouldExtractWaveform: true,
      noOfSamples: 100,
      volume: 10.0);
      setState((){
        playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
        getVoice = false;
        isplay = false;
      });
    },);
  }
  void _getDir() async {
    
    appDirectory = await getApplicationDocumentsDirectory();
    path = "${appDirectory.path}/recording.m4a";
    setState(() {});
  }
  
  @override
  void initState() {
    setState(() {
      comment.text= widget.dataFeedback.comment.toString();
      listImage = widget.dataFeedback.file!.where((element) {return element.fileType!.contains("image");}).toList();
      listVoice = widget.dataFeedback.file!.where((element) {return element.fileType!.contains("audio");}).toList();
      Future.delayed(const Duration(milliseconds: 100),(){
        _getVoiebyLink(listVoice[0].fileShow.toString());
      });
    });
    _getDir();
    super.initState();
  }
  @override
  void dispose() {
    playerController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      body:Container(
            margin: const EdgeInsets.only(top: 50),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16),
                )),
            child: Stack(
              children: [
                Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Icon(
                                Icons.keyboard_arrow_left_rounded,
                                size: 38,
                                color: Colorconstand.neutralDarkGrey,
                              )),
                          Expanded(
                           child: Padding(
                            padding:const EdgeInsets.only(right: 0),
                            child: Center(child: Text("FEEDBACK_INBOX".tr(),
                              style: ThemsConstands.headline3_semibold_20,
                            )),
                          )),
                          IconButton(
                              onPressed: () {
                              },
                              icon: const Icon(
                                Icons.keyboard_arrow_left_rounded,
                                size: 32,
                                color: Colors.transparent,
                              )),
                        ],
                      ),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Column(
                            children: [
                              Container(
                                padding:const EdgeInsets.all(12),
                                decoration: BoxDecoration(
                                  color: Colorconstand.neutralWhite,
                                  borderRadius: BorderRadius.circular(18),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color(0xFFECEBEB),
                                      blurRadius: 25.0,
                                      spreadRadius: 5.0,
                                      offset: Offset(0.0,0.0,),
                                    )
                                  ],
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Container(
                                              child: Row(
                                                children: [
                                                  SvgPicture.asset(ImageAssets.CALENDAR_ICON,width: 18,height: 18,),
                                                  const SizedBox(width: 8,),
                                                  Text(
                                                    "DURATIONFINANCE".tr(),
                                                    style: ThemsConstands.headline4_regular_18
                                                        .copyWith(color: Colorconstand.lightBlack),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Text(
                                            widget.dataFeedback.createdAt.toString().substring(0,10),
                                            style: ThemsConstands.headline4_regular_18
                                                .copyWith(color: Colorconstand.lightBlack),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(height: 18,),
                                    Text("ខ្លឹមសារសំបុត្រ៖",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),),
                                    const SizedBox(height: 8,),
                                    const Divider(color: Colorconstand.neutralGrey,),
                                    const SizedBox(height: 8,),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(comment.text,style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.left,)),

                                  ],
                                ),
                              ),
                              listImage.isEmpty && listVoice.isEmpty?Container():Container(
                                margin:const EdgeInsets.only(top: 22),
                                decoration: BoxDecoration(
                                  color: Colorconstand.neutralWhite,
                                  borderRadius: BorderRadius.circular(18),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color(0xFFECEBEB),
                                      blurRadius: 25.0,
                                      spreadRadius: 5.0,
                                      offset: Offset(0.0,0.0,),
                                    )
                                  ],
                                ),
                                child: Column(
                                  children: [
                                    const SizedBox(height: 18,),
                                    Text("REFERRING_FILE".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                    const SizedBox(height: 8,),
                                    listImage.isEmpty?Container():Container(
                                      height: 80,
                                      child: Center(
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: listImage.length,
                                          itemBuilder: (context, indexImage) {
                                            return GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  PageRouteBuilder(
                                                    pageBuilder: (_, __, ___) =>
                                                        ImageViewDownloads(
                                                      listimagevide: listImage,
                                                      activepage: indexImage,
                                                    ),
                                                    transitionDuration:
                                                        const Duration(seconds: 0),
                                                  ),
                                                );
                                              },
                                              child: Container(
                                                margin:const EdgeInsets.symmetric(horizontal: 6,vertical: 12),
                                                child:ClipRRect(
                                                  borderRadius: BorderRadius.circular(10),
                                                  child: Image(
                                                    image: NetworkImage(listImage[indexImage].fileThumbnail.toString()),fit: BoxFit.cover,height: 60,width: 60,
                                                  ),
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                    listVoice.isEmpty?Container():Divider(),
                                    listVoice.isEmpty?Container():Container(
                                      margin:const EdgeInsets.only(bottom: 18),
                                      child: ListView.builder(
                                        padding:const EdgeInsets.all(0),
                                        physics:const NeverScrollableScrollPhysics(),
                                        itemCount: listVoice.length,
                                        shrinkWrap: true,
                                        itemBuilder: (context, indexVoice) {
                                        return Container(
                                            height: 50,
                                            margin:const EdgeInsets.symmetric(horizontal: 28),
                                            width: MediaQuery.of(context).size.width/1.5,
                                            decoration: BoxDecoration(color: Colorconstand.neutralGrey.withOpacity(0.5),borderRadius:const BorderRadius.all(Radius.circular(10))),
                                            child: Row(
                                              children: [
                                                GestureDetector(onTap:() async {
                                                  setState(() {
                                                    getVoice = false;
                                                  });
                                                  if(isplay == false){
                                                    setState(() {
                                                      isplay =true;
                                                      playerController.startPlayer(finishMode: FinishMode.pause);
                                                    });
                                                  }
                                                  else{
                                                    setState(() {
                                                      isplay =false;
                                                      playerController.pausePlayer();
                                                    });
                                                  }
                                                  playerController.onPlayerStateChanged.listen((event) {
                                                    if(event.isPlaying){
                                                        setState(() {
                                                          isplay =true;
                                                        },);
                                                    }else if(event.isPaused){
                                                        setState(() {
                                                          isplay =false;
                                                        },);
                                                    }
                                                  });
                                                  playerController.onCompletion.listen((event) {
                                                    setState(() {
                                                      playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
                                                    });
                                                  });
                                                  playerController.onCurrentDurationChanged.listen((duration) {
                                                    setState(() {
                                                      playDuration = (playerController.maxDuration~/1000 - (duration~/1000).toInt()).toMMSS();
                                                    },);
                                                  });
                                                  
                                                } , child: Icon(isplay? Icons.pause_rounded:Icons.play_arrow_rounded,color: Colorconstand.primaryColor,size: 30,)),
                                                Container(margin:const EdgeInsets.only(right: 5),child: Text(playDuration,style: ThemsConstands.headline_5_medium_16,)),
                                                Expanded(
                                                  child: AudioFileWaveforms(
                                                    size: Size(MediaQuery.of(context).size.width, 50.0),
                                                    playerController: playerController,
                                                    enableSeekGesture: true,
                                                    waveformType: WaveformType.long,
                                                    waveformData: waveformData,
                                                    playerWaveStyle: const PlayerWaveStyle(
                                                      scaleFactor: 200,
                                                      waveCap: StrokeCap.butt,
                                                      showSeekLine: false,
                                                      fixedWaveColor: Colorconstand.neutralDarkGrey,
                                                      liveWaveColor: Colors.blueAccent,
                                                      spacing: 6,),
                                                    ),
                                                ),
                                                const SizedBox(width: 8,),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
      );
  }
}
