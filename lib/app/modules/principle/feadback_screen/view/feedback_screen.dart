import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/principle/feadback_screen/state/get_feedback/bloc/get_feedback_guardian_bloc.dart';
import 'package:camis_teacher_application/app/service/api/principle_api/feedback_api/feedback_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../widget/empydata_widget/empty_widget.dart';
import 'feedback_detail_screen.dart';

class FeedbackScreen extends StatefulWidget {
  const FeedbackScreen({Key? key}) : super(key: key);

  @override
  State<FeedbackScreen> createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen>{

  bool feedbackIn = true;
  @override
  void initState() {
    BlocProvider.of<GetFeedbackGuardianBloc>(context).add(GetFeedbackEvent(isSeen: 0));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
        backgroundColor: Colorconstand.neutralWhite,
        body: Container(
          color: Colorconstand.primaryColor,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                right: 0,
                child: Image.asset("assets/images/Oval.png"),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,                
                    child: SafeArea(
                      bottom: false,
                      child: Container(
                        margin:const EdgeInsets.only(top: 10),
                        child: Column(
                          children: [
                            Container(
                              margin:const EdgeInsets.only(left: 18,right: 18,bottom: 18),
                              alignment: Alignment.centerLeft,
                              child: Row(
                                children: [
                                  IconButton(
                                    onPressed: (){
                                      Navigator.of(context).pop();
                                    }, 
                                   icon:const Icon(Icons.arrow_back_ios_outlined,color: Colorconstand.neutralWhite,size: 22,)
                                  ),
                                  const SizedBox(width: 12,),
                                  Text("FEEDBACK_INBOX".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                                ],
                              ),
                            ),
                            Container(
                              margin:const EdgeInsets.only(left: 18,right: 18),
                              height: 50,
                              decoration: BoxDecoration(
                                color: Colorconstand.neutralWhite,
                                borderRadius: BorderRadius.circular(12)
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: (){
                                        setState(() {
                                          feedbackIn = true;
                                          BlocProvider.of<GetFeedbackGuardianBloc>(context).add(GetFeedbackEvent(isSeen: 0));
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 50,
                                          margin:const EdgeInsets.all(3),
                                          decoration: BoxDecoration(
                                            color: feedbackIn==true?Colorconstand.primaryColor:Colorconstand.neutralWhite,
                                            borderRadius: BorderRadius.circular(12)
                                          ),
                                          child: Text("សំបុត្រចូល",style: ThemsConstands.headline_5_semibold_16.copyWith(color: feedbackIn==true?Colorconstand.neutralWhite:Colorconstand.lightBlack,),),
                                       )
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: (){
                                        setState(() {
                                          feedbackIn = false;
                                          BlocProvider.of<GetFeedbackGuardianBloc>(context).add(GetFeedbackEvent(isSeen: 1));
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 50,
                                          margin:const EdgeInsets.all(3),
                                          decoration: BoxDecoration(
                                            color: feedbackIn==false?Colorconstand.primaryColor:Colorconstand.neutralWhite,
                                            borderRadius: BorderRadius.circular(12)
                                          ),
                                          child: Text("សំបុត្ររួចរាល់",style: ThemsConstands.headline_5_semibold_16.copyWith(color: feedbackIn==false?Colorconstand.neutralWhite:Colorconstand.lightBlack,),), 
                                       )
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(height: 18,),
                            Expanded(
                              child: Container(
                                color: Colorconstand.neutralWhite,
                                child: BlocBuilder<GetFeedbackGuardianBloc, GetFeedbackGuardianState>(
                                  builder: (context, state) {
                                    if(state is GetFeedbackGuardianLoading){
                                      return const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    else if(state is GetFeedbackGuardianLoaded){
                                      var data = state.feedBackModel.data;
                                      return data!.data!.isEmpty?
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Center(
                                              child: EmptyWidget(
                                                title: "NOT_YET_DATA_AVAILABLE".tr(),
                                                subtitle: "".tr(),
                                              ),
                                            ),
                                          ],
                                        )
                                      :ListView.separated(
                                        separatorBuilder: (context, index) {
                                          return Container(
                                            height: 2,
                                            color: Colorconstand.gray300,
                                          );
                                        },
                                        padding:const EdgeInsets.all(0),
                                        itemCount: data.data!.length,
                                        itemBuilder: (context, index) {
                                          return MaterialButton(
                                            padding:const EdgeInsets.all(0),
                                            onPressed: () {
                                              if(feedbackIn == true){
                                                FeedbackApi().seenFeedback(id: data.data![index].id.toString());
                                              }
                                              Navigator.push(context, MaterialPageRoute(builder: (context)=>  FeedbackDetailScreen(dataFeedback: data.data![index],)));
                                            },
                                            child: Container(
                                              margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 6),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    alignment: Alignment.center,
                                                    height: 60,
                                                    width: 60,
                                                    padding:const EdgeInsets.all(5),
                                                    decoration:const BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Colorconstand.primaryColor
                                                    ),
                                                    child: data.data![index].classId==0?SvgPicture.asset(ImageAssets.school_icon,color: Colorconstand.neutralWhite,):Text(data.data![index].className.toString(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      margin:const EdgeInsets.symmetric(horizontal: 12),
                                                      alignment: Alignment.centerLeft,
                                                      child: Text(data.data![index].comment.toString(),style: ThemsConstands.headline_5_semibold_16,maxLines: 2,overflow: TextOverflow.ellipsis,),
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: [
                                                        data.data![index].file!.isEmpty?const SizedBox():Container(
                                                          margin:const EdgeInsets.only(bottom: 6),
                                                          child: SvgPicture.asset(ImageAssets.attach_circlle,width: 18,)),
                                                        Text(data.data![index].createdAt.toString().substring(0,5),style: ThemsConstands.headline_6_regular_14_20height,)
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    }
                                    else {
                                      return Center(
                                        child: EmptyWidget(
                                          title: "WE_DETECT".tr(),
                                          subtitle: "WE_DETECT_DES".tr(),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                ),
            ],
          ),
        ),
    );
  }
}
