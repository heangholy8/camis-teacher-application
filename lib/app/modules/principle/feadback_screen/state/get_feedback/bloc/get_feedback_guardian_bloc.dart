import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/principle_api/feedback_api/feedback_api.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../models/principle_model/feedback_model.dart';

part 'get_feedback_guardian_event.dart';
part 'get_feedback_guardian_state.dart';

class GetFeedbackGuardianBloc extends Bloc<GetFeedbackGuardianEvent, GetFeedbackGuardianState> {
  final FeedbackApi feedbackapi;
  GetFeedbackGuardianBloc({required this.feedbackapi}) : super(GetFeedbackGuardianInitial()) {
    on<GetFeedbackEvent>((event, emit) async {
      emit(GetFeedbackGuardianLoading());
      try {
        var data = await feedbackapi.getFeedBackApi(isSeen: event.isSeen);
        emit(GetFeedbackGuardianLoaded(feedBackModel: data));
      } catch (e) {
        print("error: $e");
        emit(GetFeedbackGuardianError());
      }
    });
  }
}
