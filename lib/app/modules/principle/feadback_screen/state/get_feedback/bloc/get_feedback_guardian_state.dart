part of 'get_feedback_guardian_bloc.dart';

class GetFeedbackGuardianState extends Equatable {
  const GetFeedbackGuardianState();
  @override
  List<Object> get props => [];
}
class GetFeedbackGuardianInitial extends GetFeedbackGuardianState {}
class GetFeedbackGuardianLoading extends GetFeedbackGuardianState {}
class GetFeedbackGuardianLoaded extends GetFeedbackGuardianState {
  final FeedBackModel feedBackModel;
  const GetFeedbackGuardianLoaded({required this.feedBackModel});
}
class GetFeedbackGuardianError extends GetFeedbackGuardianState {}
