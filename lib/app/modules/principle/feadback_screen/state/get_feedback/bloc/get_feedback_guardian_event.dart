part of 'get_feedback_guardian_bloc.dart';

class GetFeedbackGuardianEvent extends Equatable {
  const GetFeedbackGuardianEvent();

  @override
  List<Object> get props => [];
}

class GetFeedbackEvent extends GetFeedbackGuardianEvent{
  int isSeen;
  GetFeedbackEvent({required this.isSeen});
}
