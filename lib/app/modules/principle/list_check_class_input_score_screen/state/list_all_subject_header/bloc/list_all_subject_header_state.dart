part of 'list_all_subject_header_bloc.dart';

class ListAllSubjectHeaderState extends Equatable {
  const ListAllSubjectHeaderState();
  
  @override
  List<Object> get props => [];
}

class ListAllSubjectHeaderInitial extends ListAllSubjectHeaderState {}
class ListAllSubjectHeaderLoading extends ListAllSubjectHeaderState {}
class ListAllSubjectHeaderLoaded extends ListAllSubjectHeaderState {
   final AllSubjectListModel allSubjectListModel;
   const ListAllSubjectHeaderLoaded({required this.allSubjectListModel});
}
class ListAllSubjectHeaderError extends ListAllSubjectHeaderState {}
