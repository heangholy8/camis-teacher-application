part of 'list_all_subject_header_bloc.dart';

class ListAllSubjectHeaderEvent extends Equatable {
  const ListAllSubjectHeaderEvent();

  @override
  List<Object> get props => [];
}

class GetListAllSubjectHeaderEvent extends ListAllSubjectHeaderEvent{}