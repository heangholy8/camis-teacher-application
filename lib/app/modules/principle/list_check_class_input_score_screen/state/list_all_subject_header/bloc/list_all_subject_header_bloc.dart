import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/principle_model/list_all_subjects_header.dart';
import '../../../../../../service/api/principle_api/check_class_input_score_principle/get_list_check_input_score_api.dart';
part 'list_all_subject_header_event.dart';
part 'list_all_subject_header_state.dart';

class ListAllSubjectHeaderBloc extends Bloc<ListAllSubjectHeaderEvent, ListAllSubjectHeaderState> {
  final GetCheckClassInputScoreApi getListAllSubjectHeaderApi;
  ListAllSubjectHeaderBloc({required this.getListAllSubjectHeaderApi}) : super(ListAllSubjectHeaderInitial()) {
    on<GetListAllSubjectHeaderEvent>((event, emit) async {
      emit(ListAllSubjectHeaderLoading());
      try {
        var data = await getListAllSubjectHeaderApi.getListAllSubjectHeaderApi();
        emit(ListAllSubjectHeaderLoaded(allSubjectListModel: data));
      } catch (e) {
        print("error: $e");
        emit(ListAllSubjectHeaderError());
      }
    });
  }
}
