part of 'list_check_input_score_bloc.dart';

class ListCheckInputScoreState extends Equatable {
  const ListCheckInputScoreState();
  
  @override
  List<Object> get props => [];
}

class ListCheckInputScoreInitial extends ListCheckInputScoreState {}

class ListCheckClassInputScoreLoading extends ListCheckInputScoreState {}
class ListCheckClassInputScoreLoaded extends ListCheckInputScoreState {
   final ListCheckClassInputScoreModel listCheckClassInputScoreModel;
   const ListCheckClassInputScoreLoaded({required this.listCheckClassInputScoreModel});
}
class ListCheckClassInputScoreError extends ListCheckInputScoreState {}
