part of 'list_check_input_score_bloc.dart';

class ListCheckInputScoreEvent extends Equatable {
  const ListCheckInputScoreEvent();

  @override
  List<Object> get props => [];
}

class GetCheckListClassInputScore extends ListCheckInputScoreEvent{
  String type;
  String semester;
  String month;
  String defaultId;
  GetCheckListClassInputScore({required this.type,required this.defaultId,required this.month,required this.semester});
}
