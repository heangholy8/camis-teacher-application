import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/principle_model/list_check_class_input_score_model.dart';
import '../../../../../../service/api/principle_api/check_class_input_score_principle/get_list_check_input_score_api.dart';
part 'list_check_input_score_event.dart';
part 'list_check_input_score_state.dart';

class ListCheckInputScoreBloc extends Bloc<ListCheckInputScoreEvent, ListCheckInputScoreState> {
  final GetCheckClassInputScoreApi getCheckClassInputScoreApi;
  ListCheckInputScoreBloc({required this.getCheckClassInputScoreApi}) : super(ListCheckInputScoreInitial()) {
    on<GetCheckListClassInputScore>((event, emit) async{
      emit(ListCheckClassInputScoreLoading());
      try {
        var data = await getCheckClassInputScoreApi.getListClassInputScoreApi(
          mapDefaultSubjectId: event.defaultId,
          type: event.type,
          semester: event.semester,
          month: event.month,
        );
        emit(ListCheckClassInputScoreLoaded(listCheckClassInputScoreModel: data));
      } catch (e) {
        print("error: $e");
        emit(ListCheckClassInputScoreError());
      }
    });
  }
}
