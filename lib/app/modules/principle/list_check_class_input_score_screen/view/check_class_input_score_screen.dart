import 'dart:async';

import 'package:camis_teacher_application/app/bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/principle/list_check_class_input_score_screen/state/list_all_subject_header/bloc/list_all_subject_header_bloc.dart';
import 'package:camis_teacher_application/app/modules/principle/list_check_class_input_score_screen/state/list_check_input_score/bloc/list_check_input_score_bloc.dart';
import 'package:camis_teacher_application/widget/navigate_bottom_bar_widget/navigate_bottom_bar_principle.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../student_list_screen/widget/contact_pairent_widget.dart';

class CheckClassInputScoreScreen extends StatefulWidget {
  String? role;
  CheckClassInputScoreScreen({super.key,required this.role});

  @override
  State<CheckClassInputScoreScreen> createState() => _CheckClassInputScoreScreenState();
}
class _CheckClassInputScoreScreenState extends State<CheckClassInputScoreScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  bool activeMySchedule = true;
  //=============== End Varible main Tap =====================


  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================

 //============== verible Exam Tap ====================
    int? monthLocalExam;
    int? yearLocalExam;
    String? monthLocalNameExam;
    String? monthLocalNameExamFirstLoad;
    int? monthLocalExamFirstLoad;
    int? yearLocalExamFirstLoad;
    bool showListMonthExam = false;
    String semesterName = "";
    String typeSelection = "1";
    int activeIndexMonth = 0;
    String defaultId = "0";

    int activeIndexHeader = 0;

 //============== verible Exam Tap ====================

    StreamSubscription? internetconnection;
    bool isoffline = true;
    ScrollController scrollController = ScrollController();

//============== Check Save Score local do complet task ============

    StreamSubscription? sub;

  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }
  @override
  void initState() {
    setState(() {
    //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true) {
          }
        });
      });
      //=============Eend Check internet===============
    //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

      //============= controller class change option ========
      _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      monthLocalNameExam = DateFormat.MMMM("en-IN").format(DateTime.now()).toUpperCase();
      //==============  Exam Tap ====================
     monthLocalExam = monthlocal;
     yearLocalExam = yearchecklocal;
     monthLocalExamFirstLoad = monthlocal;
     yearLocalExamFirstLoad = yearchecklocal;
     monthLocalNameExamFirstLoad = monthLocalNameExam;
    //============== Exam Tap =====================
    });
    //============== Event call Data =================
    BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
    BlocProvider.of<ListAllSubjectHeaderBloc>(context).add(GetListAllSubjectHeaderEvent());
    //============== End Event call Data =================
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }
  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
        return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      bottomNavigationBar: const BottomNavigateBarPrinciple(
        isActive: 3,
      ),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: Container(
                  margin:const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: [
                          const SizedBox(width: 22),
                          Expanded(
                            child: Text("តារាងស្រង់ពិន្ទុ".tr(),
                              style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),
                            ),
                          ),
                          const SizedBox(
                            width: 8
                          ),
                          GestureDetector(
                            onTap:() {
                              setState(() {
                                  if(showListMonthExam==true){
                                      showListMonthExam =false;
                                  }
                                  else{
                                    showListMonthExam = true;
                                  }
                              });
                            },
                            child:Container(
                              height: 35,
                              padding:const EdgeInsets.all(6),
                               decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  color: Colorconstand.neutralWhite.withOpacity(0.2)
                                ),
                              child: Row(
                                children: [
                                  Text(monthLocalNameExam!.tr(),
                                  style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),
                                  ),
                                  const SizedBox(
                                    width: 8
                                  ),
                                  Icon(
                                    showListMonthExam ==false?Icons.expand_more:Icons.expand_less_rounded,
                                    size: 24,
                                    color: Colorconstand.neutralWhite
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(width: 28),
                        ],
                      ),
                    ),
                    Container(height: 15,),
                    Expanded(
                      child: BlocConsumer<ListMonthSemesterBloc, ListMonthSemesterState>(
                        listener: (context, state) {
                          if(state is ListMonthSemesterLoaded){
                            var data = state.monthAndSemesterList!.data;
                            for (var i = 0; i < data!.length; i++) {
                              if (data[i].displayMonth == monthLocalNameExam) {
                                setState(() {
                                  activeIndexMonth = i;
                                  semesterName = data[i].semester.toString();
                                  BlocProvider.of<ListCheckInputScoreBloc>(context).add(GetCheckListClassInputScore(
                                    month: monthLocalExam.toString(),type: typeSelection,semester: semesterName,defaultId: defaultId
                                  ));
                                });
                              }
                            }
                          }
                        },
                        builder: (context, state) {
                          if(state is ListMonthSemesterLoading){
                            return  Column(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 5),
                                  margin:const EdgeInsets.symmetric(horizontal: 18),
                                  height: 50,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Colorconstand.neutralWhite
                                  ),
                                ),

                                Container(height: 15,),
                                Expanded(
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10.0),
                                          topRight: Radius.circular(10.0),
                                        ),
                                      color: Colorconstand.neutralWhite
                                    ),
                                    child:const Center(
                                      child: CircularProgressIndicator(),
                                    ),
                                  )
                                )

                              ],
                            );
                          }
                          else if(state is ListMonthSemesterLoaded){
                            var datamonth = state.monthAndSemesterList!.data;
                            return Column(
                              children: [
                                Expanded(
                                  child: Stack(
                                    children: [
                                      Column(
                                        children: [
                                          Expanded(
                                            child: BlocConsumer<ListAllSubjectHeaderBloc, ListAllSubjectHeaderState>(
                                              listener: (context, state) {
                                                if(state is ListAllSubjectHeaderLoaded){
                                                  var data = state.allSubjectListModel.data;
                                                  setState(() {
                                                  });
                                                }
                                              },
                                              builder: (context, state) {
                                                if(state is ListAllSubjectHeaderLoading){
                                                    return Container(
                                                                        
                                                    );
                                                }
                                                else if(state is ListAllSubjectHeaderLoaded){
                                                  var dataHeaderSubject = state.allSubjectListModel.data;
                                                  return Column(
                                                    children: [
                                                      Container(
                                                        alignment: Alignment.center,
                                                        padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 5),
                                                        margin:const EdgeInsets.symmetric(horizontal: 18),
                                                        height: 50,
                                                        width: MediaQuery.of(context).size.width,
                                                        decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.circular(12),
                                                          color: Colorconstand.neutralWhite
                                                        ),
                                                        child: ListView.builder(
                                                          shrinkWrap: true,
                                                          scrollDirection: Axis.horizontal,
                                                          padding:const EdgeInsets.all(0),
                                                          itemCount: dataHeaderSubject!.length,
                                                          itemBuilder: (context, index) {
                                                            return Container(
                                                              margin:const EdgeInsets.only(right: 12),
                                                              padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 9),
                                                              alignment: Alignment.center,
                                                              decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(12),
                                                                color: activeIndexHeader == dataHeaderSubject[index].mapDefaultSubjectId?Colorconstand.primaryColor:Colorconstand.neutralWhite,
                                                              ),
                                                              child: GestureDetector(
                                                                onTap: (){
                                                                  setState(() {
                                                                    activeIndexHeader = dataHeaderSubject[index].mapDefaultSubjectId!;
                                                                    defaultId = dataHeaderSubject[index].mapDefaultSubjectId.toString();
                                                                    BlocProvider.of<ListCheckInputScoreBloc>(context).add(GetCheckListClassInputScore(
                                                                        month: monthLocalExam.toString(),type: typeSelection,semester: semesterName,defaultId:dataHeaderSubject[index].mapDefaultSubjectId.toString()
                                                                      ));
                                                                  });
                                                                },
                                                                child: Container(
                                                                  alignment: Alignment.center,
                                                                  child: Text(translate == "km"?dataHeaderSubject[index].name.toString():dataHeaderSubject[index].nameEn.toString(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: activeIndexHeader == dataHeaderSubject[index].mapDefaultSubjectId?Colorconstand.neutralWhite:Colorconstand.lightBlack),textAlign: TextAlign.center,)),
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ),
                                                      Container(height: 15,),
                                                      Expanded(
                                                        child: Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          decoration: const BoxDecoration(
                                                              borderRadius: BorderRadius.only(
                                                                topLeft: Radius.circular(10.0),
                                                                topRight: Radius.circular(10.0),
                                                              ),
                                                              color: Colorconstand.neutralWhite),
                                                          child: BlocBuilder<ListCheckInputScoreBloc, ListCheckInputScoreState>(

                                                            builder: (context, state) {
                                                              if(state is ListCheckClassInputScoreLoading){
                                                                return const Center(child: CircularProgressIndicator(),);
                                                              }
                                                              else if(state is ListCheckClassInputScoreLoaded){
                                                                var dataClass = state.listCheckClassInputScoreModel.data;
                                                                return ListView.builder(
                                                                  shrinkWrap: true,
                                                                  padding:const EdgeInsets.only(bottom: 28),
                                                                  itemCount: dataClass!.grades!.length,
                                                                  itemBuilder: (context, indexGrade) {
                                                                    return  Container(
                                                                      margin:const EdgeInsets.only(top: 12,left: 18,right: 18),
                                                                      decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(16),
                                                                        color: Colorconstand.primaryColor,
                                                                      ),
                                                                      child: Container(
                                                                        padding:const EdgeInsets.all(1),
                                                                        child: Column(
                                                                          children: [
                                                                            Container(
                                                                              alignment: Alignment.center,
                                                                              height: 40,
                                                                              child: Text("${"GRADED".tr()} ${dataClass.grades![indexGrade].level.toString()}",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                                            ),
                                                                            Container(
                                                                               decoration:const BoxDecoration(
                                                                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16),bottomRight: Radius.circular(16)),
                                                                                  color: Colorconstand.neutralWhite,
                                                                                ),
                                                                              child:  dataClass.grades![indexGrade].classes!.isEmpty? 
                                                                              Container(
                                                                                width: MediaQuery.of(context).size.width,
                                                                                alignment: Alignment.center,
                                                                                height: 60,
                                                                                child: Text("NO_CLASS".tr()),
                                                                              )
                                                                              :GridView.builder(
                                                                                 shrinkWrap: true,
                                                                                  physics:const NeverScrollableScrollPhysics(),
                                                                                  padding:const EdgeInsets.all(12),
                                                                                  itemCount: dataClass.grades![indexGrade].classes!.length,
                                                                                  gridDelegate:const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,childAspectRatio: 1.8,mainAxisSpacing: 8,crossAxisSpacing: 8),
                                                                                  itemBuilder: (BuildContext context, int indexclass) {
                                                                                    return MaterialButton(
                                                                                      splashColor: Colors.transparent,
                                                                                      highlightColor: Colors.transparent,
                                                                                      padding:const EdgeInsets.all(0),
                                                                                      onPressed: () {
                                                                                        showModalBottomSheet(
                                                                                          shape: const RoundedRectangleBorder(
                                                                                            borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                                                                                          ),
                                                                                          context: context,
                                                                                          builder: (context) {
                                                                                            return Column(
                                                                                              mainAxisSize: MainAxisSize.min,
                                                                                              children: [
                                                                                                Container(
                                                                                                  padding:const EdgeInsets.symmetric(vertical: 16, horizontal: 28),
                                                                                                  child: Text("CONTACT".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey,)),
                                                                                                ),
                                                                                                const Divider(
                                                                                                  height: 1,
                                                                                                  thickness: 0.5,
                                                                                                ),
                                                                                                ContactPairentWidget(
                                                                                                  subjectName: dataClass.grades![indexGrade].classes![indexclass].subjectName,
                                                                                                  phone: dataClass.grades![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                                  name: dataClass.grades![indexGrade].classes![indexclass].teacherName.toString(),
                                                                                                  onTap: (){
                                                                                                    _launchLink("tel:${dataClass.grades![indexGrade].classes![indexclass].teacherPhone}");
                                                                                                  },
                                                                                                  profile: "",
                                                                                                  role: dataClass.grades![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                                ),
                                                                                                
                                                                                              ],
                                                                                            );
                                                                                          }
                                                                                        );
                                                                                      },
                                                                                      child: Container(
                                                                                        alignment: Alignment.center,
                                                                                        decoration: BoxDecoration(
                                                                                          borderRadius: BorderRadius.circular(16),
                                                                                          color: dataClass.grades![indexGrade].classes![indexclass].isCheckScoreEnter.toString() == "0"?Colorconstand.alertsNotifications.withOpacity(0.25): dataClass.grades![indexGrade].classes![indexclass].isCheckScoreEnter.toString() == "1"?Colorconstand.primaryColor:Colorconstand.alertsAwaitingText
                                                                                        ),
                                                                                        child: Text(dataClass.grades![indexGrade].classes![indexclass].name.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color: dataClass.grades![indexGrade].classes![indexclass].isCheckScoreEnter.toString() == "0"? Colorconstand.alertsNotifications :  Colorconstand.neutralWhite),),
                                                                                      ),
                                                                                    );
                                                                                  },
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    );
                                                                  },
                                                                );
                                                              }
                                                              else {
                                                                return Center(
                                                                  child: EmptyWidget(
                                                                    title: "WE_DETECT".tr(),
                                                                    subtitle: "WE_DETECT_DES".tr(),
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                            
                                                    ],
                                                  );
                                                }
                                                else {
                                                  return Center(
                                                    child: EmptyWidget(
                                                      title: "WE_DETECT".tr(),
                                                      subtitle: "WE_DETECT_DES".tr(),
                                                    ),
                                                  );
                                                }
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                       /// 
                                      /// =============== Baground over when drop month =============
                                      showListMonthExam == false
                                      ? Container()
                                      : Positioned(
                                          bottom: 0,
                                          left: 0,
                                          right: 0,
                                          top: 0,
                                          child:
                                              GestureDetector(
                                            onTap: (() {
                                              setState(
                                                  () {
                                                showListMonthExam =
                                                    false;
                                              });
                                            }),
                                            child:
                                                Container(
                                              color: const Color(
                                                  0x7B9C9595),
                                            ),
                                          ),
                                        ),
                                /// =============== Baground over when drop month =============
                                /// 
                                /// =============== Widget List Change Month =============
                                      AnimatedPositioned(
                                          top: showListMonthExam ==
                                                  false
                                              ? wieght > 800?-700:-270
                                              : 0,
                                          left: 18,
                                          right: 18,
                                          duration: const Duration(milliseconds:300),
                                          child:Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(16),
                                              color: Colorconstand.neutralWhite,
                                            ),
                                            child:GestureDetector(
                                              onTap:() {
                                                setState(() {
                                                  showListMonthExam = false;
                                                });
                                              },
                                              child:Container(
                                                margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      child: Text("${"YEAR".tr()} ${yearLocalExam.toString()}",style: ThemsConstands.headline_5_semibold_16,),
                                                    ),
                                                    GridView.builder(
                                                      shrinkWrap: true,
                                                      physics:const NeverScrollableScrollPhysics(),
                                                      padding:const EdgeInsets.all(.0),
                                                        itemCount: datamonth!.length,
                                                        gridDelegate:const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,childAspectRatio: 1.4),
                                                        itemBuilder: (BuildContext context, int index) {
                                                          return activeMySchedule==true&&datamonth[index].displayMonth=="YEAR"?Container()
                                                          :GestureDetector(
                                                            onTap: (() {
                                                              setState(() {
                                                                monthLocalNameExam = datamonth[index].displayMonth;
                                                                showListMonthExam = false;
                                                                monthLocalExam =  int.parse(datamonth[index].month.toString());
                                                                semesterName = datamonth[index].semester.toString();
                                                                yearLocalExam = int.parse(datamonth[index].year.toString());
                                                                if(datamonth[index].displayMonth=="YEAR"){
                                                                  setState(() {
                                                                    typeSelection = "3";
                                                                    BlocProvider.of<ListCheckInputScoreBloc>(context).add(GetCheckListClassInputScore(
                                                                    month: monthLocalExam.toString(),type: typeSelection,semester: semesterName,defaultId:defaultId));
                                                                  });
                                                                }
                                                                else if(datamonth[index].displayMonth=="FIRST_SEMESTER"||datamonth[index].displayMonth=="SECOND_SEMESTER"){
                                                                  setState(() {
                                                                    activeIndexMonth = index - 2;
                                                                    typeSelection = "2";
                                                                    BlocProvider.of<ListCheckInputScoreBloc>(context).add(GetCheckListClassInputScore(
                                                                    month: monthLocalExam.toString(),type: typeSelection,semester: semesterName,defaultId:defaultId));
                                                                  });
                                                                }
                                                                else{
                                                                  setState(() {
                                                                    activeIndexMonth = index;
                                                                    typeSelection = "1";
                                                                    BlocProvider.of<ListCheckInputScoreBloc>(context).add(GetCheckListClassInputScore(
                                                                    month: monthLocalExam.toString(),type: typeSelection,semester: semesterName,defaultId:defaultId));
                                                                  });
                                                                  
                                                                }
                                                              });
                                                            }),
                                                            child: Container(
                                                              margin:const EdgeInsets.all(8),
                                                              decoration:BoxDecoration(
                                                                borderRadius: BorderRadius.circular(12),
                                                                color: monthLocalNameExam == datamonth[index].displayMonth? Colorconstand.primaryColor:Colors.transparent,
                                                              ),
                                                              child:  Center(child: Text("${datamonth[index].displayMonth}".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthLocalNameExam == datamonth[index].displayMonth?Colorconstand.neutralWhite
                                                              : datamonth[index].displayMonth =="FIRST_SEMESTER"||datamonth[index].displayMonth=="YEAR"||datamonth[index].displayMonth=="SECOND_SEMESTER"? Colorconstand.alertsDecline :Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                            ),
                                                          );
                                                        },
                                                      ),
                                                  ],
                                                ),
                                              )
                                            ),
                                          ),
                                        ),
                                    /// =============== End Widget List Change Month =============
                                    ],
                                  ),
                                ),
                              ],
                            );
                          }
                          else {
                            return Center(
                              child: EmptyWidget(
                                title: "WE_DETECT".tr(),
                                subtitle: "WE_DETECT_DES".tr(),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                    ],
                  ),
                ),
              ),
            ),
      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),

      //================ Internet offilne ========================
          ],
        ),
      ));

  }
}
