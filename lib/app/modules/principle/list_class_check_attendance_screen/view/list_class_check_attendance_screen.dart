import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/principle/list_class_check_attendance_screen/state/list_hour_in_date/bloc/hour_in_date_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../models/principle_model/list_hour_in_date_model.dart';
import '../../../student_list_screen/widget/contact_pairent_widget.dart';
import '../state/list_check_attendance_in_class/bloc/get_class_check_attendance_bloc.dart';

class ListClassCheckAttendanceScreen extends StatefulWidget {
  const ListClassCheckAttendanceScreen({super.key});

  @override
  State<ListClassCheckAttendanceScreen> createState() => _ListClassCheckAttendanceScreenState();
}
class _ListClassCheckAttendanceScreenState extends State<ListClassCheckAttendanceScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  //=============== End Varible main Tap =====================


  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================

 //============== verible Exam Tap ====================
    int? monthLocalExam;
    int? yearLocalExam;
    String? monthLocalNameExam;
    String? monthLocalNameExamFirstLoad;
    int? monthLocalExamFirstLoad;
    int? yearLocalExamFirstLoad;
    bool showListMonthExam = false;
    String date = "";
    String dateDisplay = "";

    DateTime now = DateTime.now();

    int indexHour = 0;
    List<HourDatum> listActiveTime = [];

 //============== verible Exam Tap ====================

    StreamSubscription? internetconnection;
    bool isoffline = true;
    ScrollController scrollController = ScrollController();

//============== Check Save Score local do complet task ============

Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
    }else{
      await launchUrl(Uri.parse(url),);
    }
  }

    StreamSubscription? sub;
  @override
  void initState() {
    setState(() {
    //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true) {
          }
        });
      });
      //=============Eend Check internet===============
    //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

      //============= controller class change option ========
      _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      monthLocalNameExam = DateFormat.MMMM("en-IN").format(DateTime.now()).toUpperCase();
      //==============  Exam Tap ====================
     monthLocalExam = monthlocal;
     yearLocalExam = yearchecklocal;
     monthLocalExamFirstLoad = monthlocal;
     yearLocalExamFirstLoad = yearchecklocal;
     monthLocalNameExamFirstLoad = monthLocalNameExam;
     date = "$daylocal/${monthlocal! <9?"0$monthlocal":monthlocal}/$yearchecklocal";
     dateDisplay = "$daylocal ${monthLocalNameExam.toString().tr()}";
    //============== Exam Tap =====================
    });
    //============== Event call Data =================
    BlocProvider.of<HourInDateBloc>(context).add(GetListHourEvent(date: date));
    //============== End Event call Data =================
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }
  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
        return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: Container(
                  margin:const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: [
                          const SizedBox(width: 4),
                          IconButton(
                            onPressed: (){
                              Navigator.of(context).pop();
                            }, 
                            icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 22,color: Colorconstand.neutralWhite,)
                          ),
                          const SizedBox(width: 4),
                          Text("QUOTE_TAB_PRESENT".tr(),
                            style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),
                          ),
                          const SizedBox(
                            width: 8
                          ),
                          GestureDetector(
                            onTap:() {
                              setState(() {
                                  if(showListMonthExam==true){
                                      showListMonthExam =false;
                                  }
                                  else{
                                    showListMonthExam = true;
                                  }
                              });
                            },
                            child:Container(
                              height: 35,
                              padding:const EdgeInsets.all(6),
                               decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  color: Colorconstand.neutralWhite.withOpacity(0.2)
                                ),
                              child: Row(
                                children: [
                                  Container(
                                    child: Text(dateDisplay.toString(),
                                        style: ThemsConstands.headline_4_medium_18.copyWith(
                                            color: Colorconstand.neutralWhite,)),
                                  ),
                                  // const SizedBox(width: 8,),
                                  // Container(
                                  //   child: Text(
                                  //     translate == "km"
                                  //       ? monthNamesKh[now.month]
                                  //       : monthNamesEn[now.month],
                                  //     style: ThemsConstands.headline_4_medium_18.copyWith(
                                  //         color: Colorconstand.neutralWhite,),
                                  //   ),
                                  // ),
                                  Icon(
                                    showListMonthExam ==false?Icons.expand_more:Icons.expand_less_rounded,
                                    size: 24,
                                    color: Colorconstand.neutralWhite
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(height: 15,),
                    Expanded(
                      child: Stack(
                        children: [
                          Column(
                            children: [
                              Expanded(
                                child: BlocConsumer<HourInDateBloc, HourInDateState>(
                                  listener: (context, state) {
                                    if(state is HourInDateLoaded){
                                      var data = state.listHourInDateModel.data!.hourData;
                                      setState(() {
                                        listActiveTime = data!.where((element) => element.isActive == true).toList();
                                      });
                                      if(listActiveTime.isNotEmpty){
                                        setState(() {
                                          indexHour = listActiveTime[0].index!;
                                          Future.delayed(const Duration(milliseconds: 300),(){
                                            setState(() {
                                              scrollController.animateTo((indexHour==0||indexHour == 1?0:(indexHour)*90).toDouble(), duration: const Duration(seconds: 2), curve: Curves.fastOutSlowIn);
                                            });
                                          });
                                        });
                                        BlocProvider.of<GetClassCheckAttendanceBloc>(context).add(GetClassCheckAttEvent(date: state.listHourInDateModel.data!.date.toString(),index: listActiveTime[0].index!));
                                      }
                                      else{
                                        setState(() {
                                          indexHour = 1;
                                          Future.delayed(const Duration(milliseconds: 300),(){
                                            setState(() {
                                              scrollController.animateTo((indexHour==0||indexHour == 1?0:(indexHour)*90).toDouble(), duration: const Duration(seconds: 2), curve: Curves.fastOutSlowIn);
                                            });
                                          });
                                        });
                                        BlocProvider.of<GetClassCheckAttendanceBloc>(context).add(GetClassCheckAttEvent(date: state.listHourInDateModel.data!.date.toString(),index: 1));
                                      }
                                    }
                                  },
                                  builder: (context, state) {
                                    if(state is HourInDateLoading){
                                        return  Column(
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 5),
                                              margin:const EdgeInsets.symmetric(horizontal: 18),
                                              height: 50,
                                              width: MediaQuery.of(context).size.width,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(12),
                                                color: Colorconstand.neutralWhite
                                              ),
                                            ),

                                            Container(height: 15,),
                                            Expanded(
                                              child: Container(
                                                width: MediaQuery.of(context).size.width,
                                                decoration: const BoxDecoration(
                                                    borderRadius: BorderRadius.only(
                                                      topLeft: Radius.circular(10.0),
                                                      topRight: Radius.circular(10.0),
                                                    ),
                                                  color: Colorconstand.neutralWhite
                                                ),
                                                child:const Center(
                                                  child: CircularProgressIndicator(),
                                                ),
                                              )
                                            )

                                          ],
                                        );
                                    }
                                    else if(state is HourInDateLoaded){
                                      var dataHour = state.listHourInDateModel.data!.hourData;
                                      return Column(
                                        children: [
                                          Container(
                                            alignment: Alignment.center,
                                            padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 5),
                                            margin:const EdgeInsets.symmetric(horizontal: 18),
                                            height: 50,
                                            width: MediaQuery.of(context).size.width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(12),
                                              color: Colorconstand.neutralWhite
                                            ),
                                            child: ListView.builder(
                                              shrinkWrap: true,
                                              physics: ClampingScrollPhysics(),
                                              controller: scrollController,
                                              scrollDirection: Axis.horizontal,
                                              padding:const EdgeInsets.all(0),
                                              itemCount: dataHour!.length,
                                              itemBuilder: (context, index) {
                                                return Container(
                                                  margin:const EdgeInsets.only(right: 12),
                                                  padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 9),
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(12),
                                                    color: indexHour == dataHour[index].index ?Colorconstand.primaryColor:Colorconstand.neutralWhite,
                                                  ),
                                                  child: GestureDetector(
                                                    onTap: (){
                                                      setState(() {
                                                        indexHour = dataHour[index].index!;
                                                        scrollController.animateTo(indexHour==0||indexHour == 1?0:indexHour*90, duration: const Duration(seconds: 2), curve: Curves.fastOutSlowIn);
                                                        BlocProvider.of<GetClassCheckAttendanceBloc>(context).add(GetClassCheckAttEvent(date: date,index: dataHour[index].index!));
                                                      });
                                                    },
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      child: Text("${dataHour[index].startTime.toString()}-${dataHour[index].endTime.toString()}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: dataHour[index].index == indexHour?Colorconstand.neutralWhite:Colorconstand.lightBlack,),textAlign: TextAlign.center,)),
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                          Container(height: 15,),
                                          Expanded(
                                            child: Container(
                                              width: MediaQuery.of(context).size.width,
                                              decoration: const BoxDecoration(
                                                  borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(10.0),
                                                    topRight: Radius.circular(10.0),
                                                  ),
                                                  color: Colorconstand.neutralWhite),
                                              child: BlocBuilder<GetClassCheckAttendanceBloc, GetClassCheckAttendanceState>(

                                                builder: (context, state) {
                                                  if(state is GetClassCheckAttendanceLoading){
                                                    return const Center(child: CircularProgressIndicator(),);
                                                  }
                                                  else if(state is GetClassCheckAttendanceLoaded){
                                                    var dataClass = state.listClassCheckAttendanceByHourModel.data;
                                                    return ListView.builder(
                                                      shrinkWrap: true,
                                                      padding:const EdgeInsets.only(bottom: 28),
                                                      itemCount: dataClass!.grade!.length,
                                                      itemBuilder: (context, indexGrade) {
                                                        return  Container(
                                                          margin:const EdgeInsets.only(top: 12,left: 18,right: 18),
                                                          decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(16),
                                                            color: Colorconstand.primaryColor,
                                                          ),
                                                          child: Container(
                                                            padding:const EdgeInsets.all(1),
                                                            child: Column(
                                                              children: [
                                                                Container(
                                                                  alignment: Alignment.center,
                                                                  height: 40,
                                                                  child: Text("${"GRADED".tr()} ${dataClass.grade![indexGrade].grade.toString()}",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                                ),
                                                                Container(
                                                                  decoration:const BoxDecoration(
                                                                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16),bottomRight: Radius.circular(16)),
                                                                      color: Colorconstand.neutralWhite,
                                                                    ),
                                                                  child:  dataClass.grade![indexGrade].classes!.isEmpty? 
                                                                  Container(
                                                                    width: MediaQuery.of(context).size.width,
                                                                    alignment: Alignment.center,
                                                                    height: 60,
                                                                    child: Text("NO_CLASS".tr()),
                                                                  )
                                                                  :GridView.builder(
                                                                    shrinkWrap: true,
                                                                      physics:const NeverScrollableScrollPhysics(),
                                                                      padding:const EdgeInsets.all(12),
                                                                      itemCount: dataClass.grade![indexGrade].classes!.length,
                                                                      gridDelegate:const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,childAspectRatio: 1.8,mainAxisSpacing: 8,crossAxisSpacing: 8),
                                                                      itemBuilder: (BuildContext context, int indexclass) {
                                                                        return MaterialButton(
                                                                          splashColor: Colors.transparent,
                                                                          highlightColor: Colors.transparent,
                                                                          padding:const EdgeInsets.all(0),
                                                                          onPressed: () {
                                                                            showModalBottomSheet(
                                                                              shape: const RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                                                                              ),
                                                                              context: context,
                                                                              builder: (context) {
                                                                                return Column(
                                                                                  mainAxisSize: MainAxisSize.min,
                                                                                  children: [
                                                                                    Container(
                                                                                      padding:const EdgeInsets.symmetric(vertical: 16, horizontal: 28),
                                                                                      child: Text("CONTACT".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey,)),
                                                                                    ),
                                                                                    const Divider(
                                                                                      height: 1,
                                                                                      thickness: 0.5,
                                                                                    ),
                                                                                    ContactPairentWidget(
                                                                                      subjectName:  dataClass.grade![indexGrade].classes![indexclass].subjectName,
                                                                                      phone: dataClass.grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                      name: dataClass.grade![indexGrade].classes![indexclass].teacherName.toString(),
                                                                                      onTap: (){
                                                                                        _launchLink("tel:${dataClass.grade![indexGrade].classes![indexclass].teacherPhone}");
                                                                                      },
                                                                                      profile: "",
                                                                                      role: dataClass.grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                    ),
                                                                                    
                                                                                  ],
                                                                                );
                                                                              }
                                                                            );
                                                                          },
                                                                          child: Container(
                                                                            alignment: Alignment.center,
                                                                            decoration: BoxDecoration(
                                                                              borderRadius: BorderRadius.circular(16),
                                                                              color: dataClass.grade![indexGrade].classes![indexclass].isTakeAttendance == true?Colorconstand.primaryColor:Colorconstand.alertsDecline
                                                                            ),
                                                                            child: Text(dataClass.grade![indexGrade].classes![indexclass].name.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralWhite),),
                                                                          ),
                                                                        );
                                                                      },
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  }
                                                  else {
                                                    return Center(
                                                      child: EmptyWidget(
                                                        title: "WE_DETECT".tr(),
                                                        subtitle: "WE_DETECT_DES".tr(),
                                                      ),
                                                    );
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                
                                        ],
                                      );
                                    }
                                    else {
                                      return Center(
                                        child: EmptyWidget(
                                          title: "WE_DETECT".tr(),
                                          subtitle: "WE_DETECT_DES".tr(),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                          /// 
                          /// =============== Baground over when drop month =============
                          showListMonthExam == false
                          ? Container()
                          : Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              top: 0,
                              child:
                                  GestureDetector(
                                onTap: (() {
                                  setState(
                                      () {
                                    showListMonthExam =
                                        false;
                                  });
                                }),
                                child:
                                    Container(
                                  color: const Color(
                                      0x7B9C9595),
                                ),
                              ),
                            ),
                    /// =============== Baground over when drop month =============
                    /// 
                    /// =============== Widget List Change Month =============
                          AnimatedPositioned(
                              top: showListMonthExam == false
                                  ? wieght > 800?-770:-350
                                  : 0,
                              left: 18,
                              right: 18,
                              duration: const Duration(milliseconds:300),
                              child:Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: Colorconstand.neutralWhite,
                                ),
                                child:GestureDetector(
                                  onTap:() {
                                    setState(() {
                                      showListMonthExam = false;
                                    });
                                  },
                                  child:Container(
                                    alignment: Alignment.center,
                                    margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                    child: SfDateRangePicker(
                                      selectableDayPredicate: (DateTime date) {
                                        if (date.weekday ==  DateTime.sunday) {
                                          return false;
                                        }
                                        return true;
                                      },
                                      view: DateRangePickerView.month,
                                      backgroundColor: Colors.transparent,
                                      selectionTextStyle: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),
                                      selectionColor: Colorconstand.primaryColor,
                                      todayHighlightColor: Colorconstand.primaryColor,
                                      selectionShape: DateRangePickerSelectionShape.rectangle,
                                      showNavigationArrow: true,
                                      selectionMode:DateRangePickerSelectionMode.single,
                                      headerStyle: DateRangePickerHeaderStyle(textAlign: TextAlign.center,textStyle: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.primaryColor)),
                                      enablePastDates: true,
                                      onSelectionChanged: (args) {
                                        setState(() {
                                          var dataFormate = DateFormat("dd/MM/yyyy");
                                          var month = DateFormat.MMMM("en-IN");
                                          var day = DateFormat("dd");
                                          date = dataFormate.format(args.value);
                                          dateDisplay = "${day.format(args.value)} ${month.format(args.value).toUpperCase().toString().tr()}";
                                            BlocProvider.of<HourInDateBloc>(context).add(GetListHourEvent(date: date));
                                            showListMonthExam = false;
                                          },
                                        );
                                      },
                                    )
                                  )
                                ),
                              ),
                            ),
                        /// =============== End Widget List Change Month =============
                        ],
                      ),
                    ),
                    ],
                  ),
                ),
              ),
            ),
      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),

      //================ Internet offilne ========================
          ],
        ),
      ));

  }
}
