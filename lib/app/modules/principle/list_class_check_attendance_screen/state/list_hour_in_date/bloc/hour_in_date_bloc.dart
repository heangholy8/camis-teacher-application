import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/principle_model/list_hour_in_date_model.dart';
import '../../../../../../service/api/principle_api/get_hour_in_date_api/get_hour_in_date_api.dart';

part 'hour_in_date_event.dart';
part 'hour_in_date_state.dart';

class HourInDateBloc extends Bloc<HourInDateEvent, HourInDateState> {
  final GetHourInDateApi getHourInDateApi;
  HourInDateBloc({required this.getHourInDateApi}) : super(HourInDateInitial()) {
    on<GetListHourEvent>((event, emit) async{
      emit(HourInDateLoading());
      try {
        final dataHour = await getHourInDateApi.getHourInDateApi(
          data: event.date
        );
        emit(HourInDateLoaded(listHourInDateModel: dataHour));
      } catch (e) {
        print(e.toString());
        emit(HourInDateError());
      }
    });
  }
}
