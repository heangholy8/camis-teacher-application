part of 'hour_in_date_bloc.dart';

class HourInDateEvent extends Equatable {
  const HourInDateEvent();

  @override
  List<Object> get props => [];
}
class GetListHourEvent extends HourInDateEvent{
  String date;
  GetListHourEvent({required this.date});
}
