part of 'hour_in_date_bloc.dart';

class HourInDateState extends Equatable {
  const HourInDateState();
  
  @override
  List<Object> get props => [];
}

class HourInDateInitial extends HourInDateState {}

class HourInDateLoading extends HourInDateState {}
class HourInDateLoaded extends HourInDateState {
  final ListHourInDateModel listHourInDateModel;
  const HourInDateLoaded({required this.listHourInDateModel});
}
class HourInDateError extends HourInDateState {}
