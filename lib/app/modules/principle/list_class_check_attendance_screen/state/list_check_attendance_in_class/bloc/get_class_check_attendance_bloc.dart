import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/principle_model/list_class_check_attendance_bu_hour_model.dart';
import '../../../../../../service/api/principle_api/get_list_class_check_attendance/get_class_check_attendance_api.dart';
part 'get_class_check_attendance_event.dart';
part 'get_class_check_attendance_state.dart';

class GetClassCheckAttendanceBloc extends Bloc<GetClassCheckAttendanceEvent, GetClassCheckAttendanceState> {
  final GetClassCheckAttApi getClassCheckAttApi;
  GetClassCheckAttendanceBloc({required this.getClassCheckAttApi}) : super(GetClassCheckAttendanceInitial()) {
    on<GetClassCheckAttEvent>((event, emit) async{
      emit(GetClassCheckAttendanceLoading());
      try {
        final data = await getClassCheckAttApi.getClassCheckAttApi(
          data: event.date,
          index: event.index,
        );
        emit(GetClassCheckAttendanceLoaded(listClassCheckAttendanceByHourModel: data));
      } catch (e) {
        print(e.toString());
        emit(GetClassCheckAttendanceError());
      }
    });
  }
}
