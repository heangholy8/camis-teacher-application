part of 'get_class_check_attendance_bloc.dart';

class GetClassCheckAttendanceState extends Equatable {
  const GetClassCheckAttendanceState();
  
  @override
  List<Object> get props => [];
}

class GetClassCheckAttendanceInitial extends GetClassCheckAttendanceState {}

class GetClassCheckAttendanceLoading extends GetClassCheckAttendanceState {}

class GetClassCheckAttendanceLoaded extends GetClassCheckAttendanceState {
  final ListClassCheckAttendanceByHourModel listClassCheckAttendanceByHourModel;
  const GetClassCheckAttendanceLoaded({required this.listClassCheckAttendanceByHourModel});
}
class GetClassCheckAttendanceError extends GetClassCheckAttendanceState {}
