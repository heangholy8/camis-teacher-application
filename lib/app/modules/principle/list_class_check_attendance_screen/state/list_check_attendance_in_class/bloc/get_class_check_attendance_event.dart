part of 'get_class_check_attendance_bloc.dart';

class GetClassCheckAttendanceEvent extends Equatable {
  const GetClassCheckAttendanceEvent();

  @override
  List<Object> get props => [];
}

class GetClassCheckAttEvent extends GetClassCheckAttendanceEvent{
  String date;
  int index;
  GetClassCheckAttEvent({required this.date,required this.index});
}
