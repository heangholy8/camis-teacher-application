import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/principle_model/list_class_check_attendance_bu_hour_model.dart';
import '../../../../../../service/api/principle_api/get_list_class_check_attendance/get_class_check_attendance_api.dart';
part 'get_list_attendance_dashboard_event.dart';
part 'get_list_attendance_dashboard_state.dart';

class GetListAttendanceDashboardBloc extends Bloc<GetListAttendanceDashboardEvent, GetListAttendanceDashboardState> {
  final GetClassCheckAttApi getClassCheckAttApi;
  GetListAttendanceDashboardBloc({required this.getClassCheckAttApi}) : super(GetListAttendanceDashboardInitial()) {
    on<GetClassCheckAttDashboardEvent>((event, emit) async{
      emit(GetClassCheckAttendanceDashboardLoading());
      try {
        final data = await getClassCheckAttApi.getClassCheckAttDashboardApi(
          data: event.date,
        );
        emit(GetClassCheckAttendanceDashboardLoaded(listClassCheckAttendanceByHourModel: data));
      } catch (e) {
        print(e.toString());
        emit(GetClassCheckAttendanceDashboardError());
      }
    });
  }
}
