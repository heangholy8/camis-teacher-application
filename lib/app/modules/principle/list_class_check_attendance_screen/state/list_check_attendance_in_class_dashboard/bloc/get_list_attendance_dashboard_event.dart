part of 'get_list_attendance_dashboard_bloc.dart';

class GetListAttendanceDashboardEvent extends Equatable {
  const GetListAttendanceDashboardEvent();

  @override
  List<Object> get props => [];
}

class GetClassCheckAttDashboardEvent extends GetListAttendanceDashboardEvent{
  String date;
  GetClassCheckAttDashboardEvent({required this.date});
}
