part of 'get_list_attendance_dashboard_bloc.dart';

class GetListAttendanceDashboardState extends Equatable {
  const GetListAttendanceDashboardState();
  
  @override
  List<Object> get props => [];
}

class GetListAttendanceDashboardInitial extends GetListAttendanceDashboardState {}


class GetClassCheckAttendanceDashboardLoading extends GetListAttendanceDashboardState {}
class GetClassCheckAttendanceDashboardLoaded extends GetListAttendanceDashboardState {
  final ListClassCheckAttendanceByHourModel listClassCheckAttendanceByHourModel;
  const GetClassCheckAttendanceDashboardLoaded({required this.listClassCheckAttendanceByHourModel});
}
class GetClassCheckAttendanceDashboardError extends GetListAttendanceDashboardState {}
