part of 'set_exam_bloc.dart';

abstract class SetExamState extends Equatable {
  const SetExamState();
  
  @override
  List<Object> get props => [];
}

class SetExamInitial extends SetExamState {}

class SetExamLoading extends SetExamState {}

class SetExamLoaded extends SetExamState {
  final PostExamModel? postExamModel;
  const SetExamLoaded({required this.postExamModel});
}

class SetNoExamLoaded extends SetExamState {
  final PostExamModel? postNoExamModel;
  const SetNoExamLoaded({required this.postNoExamModel});
}

class SetExamError extends SetExamState {}