import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/exam_model/post_exam_date.dart';
import '../../../../../service/api/schedule_api/set_exam_api.dart';
part 'set_exam_event.dart';
part 'set_exam_state.dart';

class SetExamBloc extends Bloc<SetExamEvent, SetExamState> {
  final SetExamApi setExam;
  SetExamBloc({required this.setExam}) : super(SetExamInitial()) {
    on<SetNoExam>((event, emit) async{
      emit(SetExamLoading());
      try {
        var dataNoSetExam = await setExam.setNoExamApi(
          idClass: event.idClass, idsubject: event.idsubject, month: event.month,
          isNoExam: event.isNoExam, type: event.type, semester: event.semester
        );
        emit(SetNoExamLoaded(postNoExamModel: dataNoSetExam));
      } catch (e) {
        print(e);
        emit(SetExamError());
      }
    });
    
    on<SetExam>((event, emit) async{
      emit(SetExamLoading());
      try {
        var dataSetExam = await setExam.setIsExamApi(
          idClass: event.idClass,idsubject: event.idsubject,
          isNoExam: event.isNoExam,type: event.type,date: event.date
          ,scheduleId: event.scheduleId,
        );
        emit(SetExamLoaded(postExamModel: dataSetExam));
      } catch (e) {
        print(e);
        emit(SetExamError());
      }
    });
  }
}
