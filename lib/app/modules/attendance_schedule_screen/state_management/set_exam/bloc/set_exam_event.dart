part of 'set_exam_bloc.dart';

abstract class SetExamEvent extends Equatable {
  const SetExamEvent();

  @override
  List<Object> get props => [];
}

class SetExam extends SetExamEvent {
  String? idClass;
  String? idsubject;
  String? date;
  String? isNoExam;
  String? scheduleId;
  String? type;
  SetExam({required this.idClass,required this.idsubject,
    required this.date,
    required this.isNoExam,required this.type,
    required this.scheduleId,});
}

class SetNoExam extends SetExamEvent {
  String? idClass;
  String? idsubject;
  String? isNoExam;
  String? semester;
  String? type;
  String? month;
  SetNoExam({required this.idClass,required this.idsubject,required this.semester,
    required this.isNoExam,required this.type,
    required this.month,});
}
