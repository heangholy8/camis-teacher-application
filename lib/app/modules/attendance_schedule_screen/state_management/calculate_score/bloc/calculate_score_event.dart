part of 'calculate_score_bloc.dart';

abstract class CalculateScoreEvent extends Equatable {
  const CalculateScoreEvent();

  @override
  List<Object> get props => [];
}

class CalculateExam extends CalculateScoreEvent {
  String? idClass;
  String? semester;
  String? type;
  String? month;
  CalculateExam({required this.idClass,required this.semester,required this.type,
    required this.month,});
}
