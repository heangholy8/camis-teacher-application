import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/calcultion_score/calculation_score_api.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/exam_model/calculate_score_model.dart';

part 'calculate_score_event.dart';
part 'calculate_score_state.dart';

class CalculateScoreBloc extends Bloc<CalculateScoreEvent, CalculateScoreState> {
  final CalculationScoreApi calculateScore;
  CalculateScoreBloc({required this.calculateScore}) : super(CalculateScoreInitial()) {
    on<CalculateExam>((event, emit) async{
      emit(CalculateLoading());
      try {
        var dataCalculate = await calculateScore.calculationScoreApi(
          classId: event.idClass,month: event.month,type: event.type,semester: event.semester
        );
        emit(CalculateLoaded(calculateScoreModel: dataCalculate));
      } catch (e) {
        print(e);
        emit(CalculateError());
      }
    });
  }
}
