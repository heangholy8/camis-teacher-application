part of 'calculate_score_bloc.dart';

abstract class CalculateScoreState extends Equatable {
  const CalculateScoreState();
  
  @override
  List<Object> get props => [];
}

class CalculateScoreInitial extends CalculateScoreState {}

class CalculateLoading extends CalculateScoreState {}

class CalculateLoaded extends CalculateScoreState {
  final CalculateScoreModel? calculateScoreModel;
  const CalculateLoaded({required this.calculateScoreModel});
}

class CalculateError extends CalculateScoreState {}