part of 'month_exam_set_bloc.dart';

abstract class MonthExamSetState extends Equatable {
  const MonthExamSetState();
  
  @override
  List<Object> get props => [];
}

class MonthExamSetInitial extends MonthExamSetState {}

class MonthExamSetLoading extends MonthExamSetState {}

class MonthExamSetLoaded extends MonthExamSetState {
  final TeachingDayModel? dayModel;
  const MonthExamSetLoaded({required this.dayModel});
}

class MonthExamSetError extends MonthExamSetState {}

