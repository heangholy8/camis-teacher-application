import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/teching_day_model/teaching_day_model.dart';
import '../../../../../service/api/teaching_day_api/get_teaching_day_api.dart';

part 'month_exam_set_event.dart';
part 'month_exam_set_state.dart';

class MonthExamSetBloc extends Bloc<MonthExamSetEvent, MonthExamSetState> {
  final  GetTeachingDayApi dayApi;
  MonthExamSetBloc({required this.dayApi}) : super(MonthExamSetInitial()) {
    on<MonthExamSet>((event, emit) async{
      emit(MonthExamSetLoading());
      try {
        var dataDayExam = await dayApi.getTeachingDayRequestApi(
          idClass: event.idClass,idsubject: event.idsubject,
        );
        emit(MonthExamSetLoaded(dayModel: dataDayExam));
      } catch (e) {
        print(e);
        emit(MonthExamSetError());
      }
    });
  }
}
