part of 'month_exam_set_bloc.dart';

abstract class MonthExamSetEvent extends Equatable {
  const MonthExamSetEvent();

  @override
  List<Object> get props => [];
}

class MonthExamSet extends MonthExamSetEvent {
  final String? idClass;
  final String? idsubject;
 const MonthExamSet({required this.idClass,required this.idsubject,});
}
