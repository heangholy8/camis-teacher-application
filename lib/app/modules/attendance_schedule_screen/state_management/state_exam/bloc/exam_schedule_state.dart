part of 'exam_schedule_bloc.dart';

abstract class ExamScheduleState extends Equatable {
  const ExamScheduleState();
  
  @override
  List<Object> get props => [];
}

class ExamScheduleInitial extends ExamScheduleState {}

class GetExamScheduleLoading extends ExamScheduleState{
  final String? loading;
  const GetExamScheduleLoading({required this.loading});
}
class GetMyExamScheduleLoaded extends ExamScheduleState{
  final MyExamScheduleModel? myExamScheduleModel;
  const GetMyExamScheduleLoaded({required this.myExamScheduleModel,});
}
class GetClassExamScheduleLoaded extends ExamScheduleState{
  final ClassExamScheduleModel? classExamScheduleModel;
  const GetClassExamScheduleLoaded({required this.classExamScheduleModel,});
}
class GetExamScheduleError extends ExamScheduleState{}