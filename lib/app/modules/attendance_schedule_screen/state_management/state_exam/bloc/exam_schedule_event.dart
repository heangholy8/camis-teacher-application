part of 'exam_schedule_bloc.dart';

abstract class ExamScheduleEvent extends Equatable {
  const ExamScheduleEvent();

  @override
  List<Object> get props => [];
}

class GetMyExamScheduleEvent extends ExamScheduleEvent {
  final String? semester;
  final String? type;
  final String? month;
 const GetMyExamScheduleEvent({this.semester,this.type,this.month});
}

class GetClassExamScheduleEvent extends ExamScheduleEvent {
  final String? semester;
  final String? classid;
  final String? type;
  final String? month;
 const GetClassExamScheduleEvent({this.semester,this.classid,this.type,this.month});
}
