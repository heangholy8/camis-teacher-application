import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/exam_model/class_exam_model.dart';
import 'package:camis_teacher_application/app/models/exam_model/my_exam_schedule.dart';
import 'package:equatable/equatable.dart';
import '../../../../../service/api/schedule_api/get_exam_schedule_api.dart';
part 'exam_schedule_event.dart';
part 'exam_schedule_state.dart';

class ExamScheduleBloc extends Bloc<ExamScheduleEvent, ExamScheduleState> {
  final GetExamScheduleApi myExamSchedule;
  final GetExamScheduleApi classExamSchedule;
  ExamScheduleBloc({required this.myExamSchedule,required this.classExamSchedule }) : super(ExamScheduleInitial()) {
    on<GetMyExamScheduleEvent>((event, emit) async {
      emit(const GetExamScheduleLoading(loading: "Loading............."));
      try {
        var dataMyExamSchedule = await myExamSchedule.getMyExamScheduleRequestApi(
          type: event.type!,
          month: event.month!,
          semester: event.semester!,
        );
        emit(GetMyExamScheduleLoaded(myExamScheduleModel: dataMyExamSchedule));
      } catch (e) {
        print(e);
        emit(GetExamScheduleError());
      }
    });

    on<GetClassExamScheduleEvent>((event, emit) async {
      emit(const GetExamScheduleLoading(loading: "Loading............."));
      try {
        var dataClassExamSchedule = await classExamSchedule.getClassExamScheduleRequestApi(
          type: event.type!,
          month: event.month!,
          semester: event.semester!,
          classId: event.classid!,
        );
        emit(GetClassExamScheduleLoaded(classExamScheduleModel: dataClassExamSchedule));
      } catch (e) {
        print(e);
        emit(GetExamScheduleError());
      }
    });
  }
}
