part of 'schedule_subject_time_in_day_bloc.dart';

abstract class ScheduleSubjectTimeInDayEvent extends Equatable {
  const ScheduleSubjectTimeInDayEvent();

  @override
  List<Object> get props => [];
}

class SchduleTimeInDaySet extends ScheduleSubjectTimeInDayEvent {
  final String? idClass;
  final String? idsubject;
  final String? date;
 const SchduleTimeInDaySet({required this.idClass,required this.idsubject,required this.date});
}
