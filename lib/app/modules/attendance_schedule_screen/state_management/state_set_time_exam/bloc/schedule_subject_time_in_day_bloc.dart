import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/schedule_model/schedule_time_in_day.dart';
import '../../../../../service/api/teaching_time/teaching_time_api.dart';

part 'schedule_subject_time_in_day_event.dart';
part 'schedule_subject_time_in_day_state.dart';

class ScheduleSubjectTimeInDayBloc extends Bloc<ScheduleSubjectTimeInDayEvent, ScheduleSubjectTimeInDayState> {
  final  GetTeachingTimeApi timeScheduleApi;
  ScheduleSubjectTimeInDayBloc({required this.timeScheduleApi}) : super(ScheduleSubjectTimeInDayInitial()) {
    on<SchduleTimeInDaySet>((event, emit) async {
      emit(ScheduleSubjectTimeInDayLoading());
      try {
        var dataTimeScheduleExam = await timeScheduleApi.getTeachingTimeRequestApi(
          idClass: event.idClass,idsubject: event.idsubject,date: event.date,
        );
        emit(ScheduleSubjectTimeInDayLoaded(timeModel: dataTimeScheduleExam));
      } catch (e) {
        print(e);
        emit(ScheduleSubjectTimeInDayError());
      }
    });
  }
}
