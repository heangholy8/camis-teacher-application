part of 'schedule_subject_time_in_day_bloc.dart';

abstract class ScheduleSubjectTimeInDayState extends Equatable {
  const ScheduleSubjectTimeInDayState();
  
  @override
  List<Object> get props => [];
}

class ScheduleSubjectTimeInDayInitial extends ScheduleSubjectTimeInDayState {}

class ScheduleSubjectTimeInDayLoading extends ScheduleSubjectTimeInDayState {}

class ScheduleSubjectTimeInDayLoaded extends ScheduleSubjectTimeInDayState {
  final ScheduleTimeModel? timeModel;
  const ScheduleSubjectTimeInDayLoaded({required this.timeModel});
}

class ScheduleSubjectTimeInDayError extends ScheduleSubjectTimeInDayState {}
