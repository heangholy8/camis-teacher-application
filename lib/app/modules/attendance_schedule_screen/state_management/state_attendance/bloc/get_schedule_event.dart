part of 'get_schedule_bloc.dart';

abstract class GetScheduleEvent extends Equatable {
  const GetScheduleEvent();

  @override
  List<Object> get props => [];
}

class GetMyScheduleEvent extends GetScheduleEvent {
  final String? date;
 const GetMyScheduleEvent({this.date});
}

class GetClassScheduleEvent extends GetScheduleEvent {
  final String? date;
  final String? classid;
 const GetClassScheduleEvent({this.date,this.classid});
}


