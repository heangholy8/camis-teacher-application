import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../models/schedule_model/schedule_model.dart';
import '../../../../../service/api/schedule_api/get_schedule_api.dart';
part 'get_schedule_event.dart';
part 'get_schedule_state.dart';

class GetScheduleBloc extends Bloc<GetScheduleEvent, GetScheduleState> {
  final GetScheduleApi mySchedule;
  final GetScheduleApi classSchedule;
  GetScheduleBloc({required this.mySchedule,required this.classSchedule}) : super(GetScheduleInitial()) {
    on<GetMyScheduleEvent>((event, emit) async {
      emit(const GetScheduleLoading(loading: "Loading............."));
      try {
        var dataMySchedule = await mySchedule.getMyScheduleRequestApi(
          date: event.date,
        );
        emit(GetMyScheduleLoaded(myScheduleModel: dataMySchedule));
      } catch (e) {
        print(e);
        emit(GetScheduleError());
      }
    });

    on<GetClassScheduleEvent>((event, emit) async {
      emit(const GetScheduleLoading(loading: "Loading............."));
      try {
        var dataClassSchedule = await classSchedule.getClassScheduleRequestApi(
            date: event.date.toString(), classid: event.classid.toString());
        emit(GetClassScheduleLoaded(classScheduleModel: dataClassSchedule));
      } catch (e) {
        print(e);
        emit(GetScheduleError());
      }
    });
  }
}
