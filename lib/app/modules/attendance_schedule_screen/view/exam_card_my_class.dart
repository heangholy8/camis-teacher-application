
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/models/exam_model/my_exam_schedule.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/thems/thems_constands.dart';
import '../../grading_screen/bloc/grading_bloc.dart';
import '../../grading_screen/bloc/view_image_bloc.dart';
import '../../grading_screen/view/grading_screen.dart';
import '../../grading_screen/view/result_screen.dart';
import '../../home_screen/widget/card_exam_subject_widget.dart';
import '../state_management/set_exam/bloc/set_exam_bloc.dart';
import '../state_management/state_exam/bloc/exam_schedule_bloc.dart';

class ExamMyClass extends StatefulWidget {
  final List<DataMyExam> dataMyExam;
  final int activeIndexMonth ;
  String semester;
  bool activeMySchedule;
  int month;
  int typeSemesterOrMonth;
  bool isPrimary;
  ExamMyClass({super.key,required this.isPrimary,required this.typeSemesterOrMonth,required this.dataMyExam, required this.activeIndexMonth,required this.activeMySchedule,required this.month,required this.semester});

  @override
  State<ExamMyClass> createState() => _ExamMyClassState();
}

class _ExamMyClassState extends State<ExamMyClass> with Toast {
  
  String classId = "";
  String subjectId = "";
  int? isInstructor;

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return BlocListener<SetExamBloc, SetExamState>(
      listener: (context, state) {
        if(state is SetExamLoading){
          setState(() {
            
          });
        }
        else if(state is SetNoExamLoaded){
            BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                idClass:  classId.toString(), 
                subjectId: subjectId.toString(), 
                type: widget.typeSemesterOrMonth.toString(), 
                month:  widget.month.toString(), 
                semester:  widget.semester.toString(),
                examDate: ""),
            );
            Navigator.push(context, 
                MaterialPageRoute(builder: (context) => GradingScreen(
                  isPrimary: widget.isPrimary,
                isInstructor: isInstructor == 0 ? false:true,
                myclass: widget.activeMySchedule,
                redirectFormScreen: 1,
                ),
              ),
            );
            if(widget.activeMySchedule==true){
              BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString()));
            }
            else{
              BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString(),classid: classId.toString()));
            }  
        }
        else{
          setState(() {
            
          });
        }
      },
      child: Container(
          margin:const EdgeInsets.only(bottom: 25),
          child: ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.all(0),
            itemCount: widget.dataMyExam.length,
            itemBuilder: (context, index) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(right: 8.0),
                          height: 13,
                          width: 20,
                          decoration: const BoxDecoration(
                            color: Colorconstand.primaryColor,
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(10.0),
                                topRight: Radius.circular(10.0)),
                          ),
                        ),
                        Text("${"GRADE".tr()} ${widget.dataMyExam[index].name}",style:ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                      ],
                    ),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: widget.dataMyExam[index].teachingSubjects!.length,
                    physics: const NeverScrollableScrollPhysics(),
                    padding: const EdgeInsets.all(0),
                    itemBuilder: (context, subIndex) {
                      var data = widget.dataMyExam;
                      return CardExamSubjectWidget(
                        maginleft: 22,
                        numClass: data[index].name.toString(),
                        titleSubject: translate =="km"? data[index].teachingSubjects![subIndex].subjectName.toString():data[index].teachingSubjects![subIndex].subjectNameEn.toString(),
                        present: data[index].teachingSubjects![subIndex].attendance!.isCheckAttendance==0?"--":data[index].teachingSubjects![subIndex].attendance!.present.toString(),
                        absent: data[index].teachingSubjects![subIndex].attendance!.isCheckAttendance==0?"--":data[index].teachingSubjects![subIndex].attendance!.absent.toString(),
                        examDate: data[index].teachingSubjects![subIndex].examDate.toString(),
                        isExamSet: data[index].teachingSubjects![subIndex].id,
                        isApprove:  data[index].teachingSubjects![subIndex].isApprove,
                        isCheckScoreEnter:  data[index].teachingSubjects![subIndex].isCheckScoreEnter,
                        isNoExam:  data[index].teachingSubjects![subIndex].isNoExam,
                        onPressed: data[index].teachingSubjects![subIndex].isCheckScoreEnter == 1?(){
                          BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                            idClass:  data[index].classId.toString(), 
                            subjectId:   data[index].teachingSubjects![subIndex].subjectId.toString(), 
                            type: data[index].teachingSubjects![subIndex].type.toString(), 
                            month:  data[index].teachingSubjects![subIndex].month.toString(), 
                            semester:  data[index].teachingSubjects![subIndex].semester.toString(),
                            examDate: ""));
                            BlocProvider.of<ViewImageBloc>(context).add(GetViewImageEvent(data[index].teachingSubjects![subIndex].id!.toString()),);
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> 
                              ResultScreen(routFromScreen: "MY_Schedule",isInstructor: data[index].isInstructor == 0 ? false:true
                            ),
                          )
                        );
                        }:(){
                          // showAlertLogout(
                          //     onSubmit: () {
                          //       Navigator.of(context).pop();
                          //     },
                          //     title: "តើអ្នកចង់បញ្ចូលពិន្ទុខែ ${data[index].teachingSubjects![subIndex].monthName.toString()} មែនទេ".tr(),
                          //     context: context,
                          //     onSubmitTitle: "GRADING".tr(),
                          //     bgColorSubmitTitle: Colorconstand.alertsDecline,
                          //     icon: const Icon(Icons.print_outlined,size: 38,color: Colorconstand.neutralWhite,),
                          //     bgColoricon:Colorconstand.mainColorSecondary
                          // );
                          if(data[index].teachingSubjects![subIndex].id == 0){
                            setState(() {
                              classId = data[index].classId.toString();
                              subjectId = data[index].teachingSubjects![subIndex].subjectId.toString();
                              isInstructor = data[index].isInstructor;
                            });
                              BlocProvider.of<SetExamBloc>(context).add(SetNoExam(idClass: data[index].classId.toString(), idsubject:  data[index].teachingSubjects![subIndex].subjectId.toString(), semester: widget.semester.toString(), isNoExam: "2", type:widget.typeSemesterOrMonth.toString(),  month: widget.month.toString(),));
                          }
                          else{
                              BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                                  idClass:  data[index].classId.toString(), 
                                  subjectId: data[index].teachingSubjects![subIndex].subjectId.toString(), 
                                  type: data[index].teachingSubjects![subIndex].type.toString(), 
                                  month:  data[index].teachingSubjects![subIndex].month.toString(), 
                                  semester:  data[index].teachingSubjects![subIndex].semester.toString(),
                                  examDate: ""),
                              );
    
                              Navigator.push(context, 
                                MaterialPageRoute(builder: (context) => GradingScreen(
                                  isPrimary: widget.isPrimary,
                                isInstructor: data[index].isInstructor == 0 ? false:true,
                                myclass: widget.activeMySchedule,
                                redirectFormScreen: 1,
                                ),
                              ),
                            );
                          }
                         },
                        // onPressed: data[index].teachingSubjects![subIndex].id == 0?() {
                        //   BlocProvider.of<MonthExamSetBloc>(context).add(MonthExamSet(idClass:data[index].classId.toString(), idsubject: data[index].teachingSubjects![subIndex].subjectId.toString()));
                        //   setState(() {
                        //     showModalBottomSheet(
                        //         isScrollControlled:true,
                        //         shape: const RoundedRectangleBorder(
                        //           borderRadius: BorderRadius.only(
                        //             topLeft: Radius.circular(16.0),
                        //             topRight: Radius.circular(16.0),
                        //           ),
                        //         ),
                        //         context: context,
                        //         builder: (context) {
                        //           return PopMonthForSelect(dataMyExam: widget.dataMyExam,listIndex: index,subIndex: subIndex, date: data[index].teachingSubjects![subIndex].examDate.toString(),typeSemesterOrMonth: widget.typeSemesterOrMonth,classId: data[index].classId.toString(),subjectId: data[index].teachingSubjects![subIndex].subjectId.toString(),activeMySchedule: widget.activeMySchedule,month: widget.month,semester: widget.semester,);
                        //         },
                        //       );
                        //   });
                        // }:() {
                        //   print(data[index].teachingSubjects![subIndex].month!);
                        //   Navigator.push(
                        //     context,
                        //     PageTransition(
                        //       type:PageTransitionType.bottomToTop,
                        //       child: ExamDetailScreen(
                        //         isInstructor: data[index].isInstructor!,
                        //         setExamFrom: 1,
                        //         subjectId: data[index].teachingSubjects![subIndex].subjectId.toString(),
                        //         isApprove: data[index].teachingSubjects![subIndex].isApprove!,
                        //         absent: data[index].teachingSubjects![subIndex].attendance!.absent.toString(), 
                        //         activeMySchedule: widget.activeMySchedule, 
                        //         classId: data[index].classId.toString(), 
                        //         className: data[index].name.toString(), 
                        //         date: data[index].teachingSubjects![subIndex].examDate.toString(), 
                        //         endTime: data[index].teachingSubjects![subIndex].endTime.toString(), 
                        //         examDate: data[index].teachingSubjects![subIndex].examDate.toString(), 
                        //         isCheckScoreEnter: data[index].teachingSubjects![subIndex].isCheckScoreEnter!, 
                        //         isNoExam: data[index].teachingSubjects![subIndex].isNoExam!, 
                        //         lateStudent:data[index].teachingSubjects![subIndex].attendance!.late.toString(), 
                        //         month: data[index].teachingSubjects![subIndex].month!, 
                        //         nameClassMonitor: data[index].teachingSubjects![subIndex].attendance!.attendanceTaker.toString(), 
                        //         permission: data[index].teachingSubjects![subIndex].attendance!.permission.toString(), 
                        //         presence: data[index].teachingSubjects![subIndex].attendance!.present.toString(), 
                        //         scheduleId: data[index].teachingSubjects![subIndex].scheduleId.toString(), 
                        //         semester: data[index].teachingSubjects![subIndex].semester.toString(), 
                        //         startTime: data[index].teachingSubjects![subIndex].stratTime.toString(), 
                        //         subjectName: translate=="km"? data[index].teachingSubjects![subIndex].subjectName.toString():data[index].teachingSubjects![subIndex].subjectNameEn.toString(), 
                        //         typeExam: data[index].teachingSubjects![subIndex].type!
                        //       ),
                        //     ),
                        //   );
                        // },
                      );
                    },
                  )
                ],
              );
            },
          ),
        ),
    );
  }
}