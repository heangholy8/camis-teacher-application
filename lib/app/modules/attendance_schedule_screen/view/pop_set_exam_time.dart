
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../widget/button_widget/button_widget.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../help/check_month.dart';
import '../../../models/grading_model/grading_model.dart';
import '../../../service/api/grading_api/post_graeding_data_api.dart';
import '../../grading_screen/bloc/grading_bloc.dart';
import '../state_management/set_exam/bloc/set_exam_bloc.dart';
import '../state_management/state_set_time_exam/bloc/schedule_subject_time_in_day_bloc.dart';

class PopSetTime extends StatefulWidget {
  String? date;
  List<StudentsData>? studentDataScore;
  String? classId;
   String? subjectId;
   String? semester;
   bool? activeMySchedule;
   int? month;
   int? typeSemesterOrMonth;
   int setExamFrom;
   String currenDate;
  PopSetTime({super.key,required this.studentDataScore,required this.currenDate,required this.setExamFrom, required this.date,required this.typeSemesterOrMonth,required this.month,required this.activeMySchedule,required this.classId,required this.subjectId,required this.semester});

  @override
  State<PopSetTime> createState() => _PopSetTimeState();
}
class _PopSetTimeState extends State<PopSetTime> {
  final _duration = const Duration(milliseconds: 500);
  String? sceduleId;
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        child: BlocListener<SetExamBloc, SetExamState>(
          listener: (context, state) {
            if(state is SetExamLoading){
              setState(() {
                isLoading = true;
              },);
            }
            else if(state is SetExamLoaded){
              // if(widget.activeMySchedule == true){
              //       if(widget.setExamFrom == 1 || widget.setExamFrom == 0){
              //         BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString()));
              //       }
              //       else{
              //         BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: widget.date));
              //       }  
              //     }
              //     else{
              //       if(widget.setExamFrom == 1 || widget.setExamFrom == 0){
              //         BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString(),classid: widget.classId));
              //       }
              //       else{
              //         BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: widget.date,classid: widget.classId));
              //       }
              //     }
              //     if(widget.setExamFrom == 0){
              //       Navigator.of(context).pop();
              //       Navigator.of(context).pop();
              //     }
              //     else{
              //       Navigator.of(context).pop();
              //       Navigator.of(context).pop();
              //       Navigator.of(context).pop();
              //     }

              setState(() {
                isLoading = false;
              },);

              BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                idClass:  widget.classId.toString(), 
                subjectId:   widget.subjectId.toString(), 
                type: widget.typeSemesterOrMonth.toString(), 
                month:  widget.month.toString(), 
                semester:  widget.semester.toString(),
                examDate: ""),
              );
              Navigator.of(context).pop();
              Navigator.of(context).pop(); 
              
            }
            else{
               setState(() {
                isLoading = false;
              },);
            }
          },
          child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        top: 8.0,
                        bottom: 20,
                        left: MediaQuery.of(context).size.width / 2.3,
                        right: MediaQuery.of(context).size.width / 2.3,
                      ),
                      height: 4,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          color: Colorconstand.neutralGrey),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "${"SELECT_TIME_EXAM_DAY".tr()}${widget.typeSemesterOrMonth == 1?" ${ translate=="km"?"MONTH".tr():""} ${checkMonth(widget.month!)}": " ${widget.semester!.tr()}"}",
                        style: ThemsConstands.headline3_medium_20_26height
                            .copyWith(color: Colorconstand.neutralDarkGrey),
                      ),
                    ),
                    const SizedBox(height: 0),
                    BlocConsumer<ScheduleSubjectTimeInDayBloc, ScheduleSubjectTimeInDayState>(
                      listener: (context, state) {
                        
                      },
                      builder: (context, state) {
                        if(state is ScheduleSubjectTimeInDayLoading){
                          return Shimmer.fromColors(
                            baseColor:Colorconstand.baseColor,
                            highlightColor:Colorconstand.highlightColor,
                            child: Container(
                              height: 200,
                              margin:const EdgeInsets.only(left: 20,top: 18,bottom: 18,),
                              child: ListView.builder(
                                padding:const EdgeInsets.symmetric(vertical: 5),
                                itemBuilder: ((context, index) {
                                  return Container(
                                    margin:const EdgeInsets.only(left: 8,top: 4,bottom: 4),
                                    height: 45,
                                      decoration: BoxDecoration(
                                      color: Colorconstand.neutralWhite,
                                      borderRadius: BorderRadius.circular(16)
                                      ),
                                  );
                                }),
                                itemCount: 2,
                              ),
                            ),
                          );
                        }
                        else if(state is ScheduleSubjectTimeInDayLoaded){
                          var datatime = state.timeModel!.data;
                            return Column(
                              children: [
                                Container(
                                  margin:const EdgeInsets.symmetric(horizontal: 12),
                                  height: 65,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(ImageAssets.calendar_icon,),
                                      const SizedBox(width: 8,),
                                      Text(widget.date!,style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 200,
                                  alignment: Alignment.center,
                                  child: ListView.builder(
                                        padding: const EdgeInsets.only(left: 22,right: 22,top: 0),
                                        itemCount: datatime!.length,
                                        itemBuilder: (context, indexTime) {
                                          return GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                for (var element in datatime) {
                                                  element.isSelect = false;
                                                }
                                                datatime[indexTime].isSelect = true;
                                                sceduleId = datatime[indexTime].id.toString();
                                              });
                                            },
                                            child: Container(
                                              margin:const EdgeInsets.symmetric(vertical: 4),
                                              padding:const EdgeInsets.symmetric(vertical: 16),
                                              decoration: BoxDecoration(
                                                color:datatime[indexTime].isSelect==true?Colorconstand.primaryColor: Colorconstand.neutralWhite,
                                                border: Border.all(color: Colorconstand.lightTrunks.withOpacity(0.6)),
                                                borderRadius: BorderRadius.circular(16)
                                              ),
                                              child: Text("${datatime[indexTime].startTime.toString()} - ${datatime[indexTime].endTime.toString()}",style: ThemsConstands.headline_5_semibold_16.copyWith(color:datatime[indexTime].isSelect==true?Colorconstand.neutralWhite:Colorconstand.lightBulma),textAlign: TextAlign.center,),
                                            ),
                                          );
                                        },
                                    )
                                 )
                              ],
                            );
                        }
                        else{
                          return Center(
                            child: EmptyWidget(
                              title: "WE_DETECT".tr(),
                              subtitle: "WE_DETECT_DES".tr(),
                            ),
                          );
                        }
                      },
                    ),
                 
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Divider(
                        thickness: 0.7,
                        color: Colorconstand.neutralGrey,
                        height: 1.2,
                      ),
                    ),
                    isLoading == true ? const Center(child: CircularProgressIndicator(),):Container(
                      height: 50,
                      width: double.infinity,
                      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                      child: CustomMaterialButton(
                      onPressed: sceduleId == null ? null : () {
                        setState(() {
                          isLoading = true;
                        });
                        PostGradingApi().postListStudentSubjectExam(
                          classID: int.parse(widget.classId!),
                          subjectID: int.parse(widget.subjectId!),
                          term: widget.semester.toString(),
                          examDate: "",
                          type: widget.typeSemesterOrMonth!,
                          month: widget.month!.toInt(),
                          listStudentScore: widget.studentDataScore!,
                        ).then((value) async{
                          BlocProvider.of<SetExamBloc>(context).add(
                          SetExam(
                              idClass: widget.classId, 
                              idsubject: widget.subjectId,
                              isNoExam: "0", 
                              type:widget.typeSemesterOrMonth.toString(), 
                              date: widget.date, 
                              scheduleId: sceduleId
                            )
                          );
                        });
                      },
                      borderRadius: BorderRadius.circular(8.0),
                      titleButton: "SET".tr(),
                      colorButton: Colorconstand.primaryColor,
                      padding: const EdgeInsets.symmetric(vertical: 12.0),
                      styleText: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightGoten),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    )
                  ],
                ),
        ),
      );
    });
  }
}