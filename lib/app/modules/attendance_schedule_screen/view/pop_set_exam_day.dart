
// ignore_for_file: must_be_immutable

import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/set_exam/bloc/set_exam_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/state_set_time_exam/bloc/schedule_subject_time_in_day_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/view/pop_set_exam_time.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../help/check_month.dart';
import '../../../models/grading_model/grading_model.dart';
import '../../grading_screen/bloc/grading_bloc.dart';
import '../../grading_screen/view/grading_screen.dart';
import '../state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../state_management/state_exam/bloc/exam_schedule_bloc.dart';
import '../state_management/state_month/bloc/month_exam_set_bloc.dart';

class PopMonthForSelect extends StatefulWidget {
  List<StudentsData>? studentDataScore;
  String classId;
  String subjectId;
  String semester;
  bool activeMySchedule;
  int month;
  int typeSemesterOrMonth;
  int setExamFrom;
  String date;
  bool isPrimary;
  PopMonthForSelect({super.key,required this.isPrimary,required this.studentDataScore, required this.date,this.setExamFrom = 0, required this.typeSemesterOrMonth,required this.month,required this.activeMySchedule,required this.classId,required this.subjectId,required this.semester});

  @override
  State<PopMonthForSelect> createState() => _PopMonthForSelectState();
}
class _PopMonthForSelectState extends State<PopMonthForSelect> {
  PageController? _pageController;
  bool isLoading = false;
  int activePageView = 0;
  bool isClicked = false;
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return StatefulBuilder(builder: (context, setState) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          BlocListener<SetExamBloc, SetExamState>(
            listener: (context, state) {
              if(state is SetExamLoading){
                setState(() {
                 isLoading = true;
                });
              }else if(state is SetNoExamLoaded){
                setState(() {
                  isLoading = false;
                },);
                if(widget.activeMySchedule == true){
                  if(widget.setExamFrom == 1 || widget.setExamFrom == 0){
                    BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString()));
                  }
                  else{
                    BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: widget.date));
                  }  
                }
                else{
                  if(widget.setExamFrom == 1 || widget.setExamFrom == 0){
                    BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString(),classid: widget.classId));
                  }
                  else{
                    BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: widget.date,classid: widget.classId));
                  }
                }
                if(widget.setExamFrom == 0){
                  // print("Set Date Examination");
                  // Navigator.of(context).pop();
                  // Navigator.push(context,
                  //   PageTransition(type:PageTransitionType.bottomToTop,
                  //     child: ExamDetailScreen(
                  //       isInstructor: widget.dataMyExam![widget.listIndex!].isInstructor!,
                  //       setExamFrom: 1,
                  //       subjectId: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].subjectId.toString(),
                  //       isApprove: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].isApprove!,
                  //       absent: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].attendance!.absent.toString(), 
                  //       activeMySchedule: widget.activeMySchedule, 
                  //       classId: widget.dataMyExam![widget.listIndex!].classId.toString(), 
                  //       className: widget.dataMyExam![widget.listIndex!].name.toString(), 
                  //       date: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].examDate.toString(), 
                  //       endTime: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].endTime.toString(), 
                  //       examDate:widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].examDate.toString(), 
                  //       isCheckScoreEnter: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].isCheckScoreEnter!, 
                  //       // isNoExam: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].isNoExam!, 
                  //       isNoExam: 2,
                  //       lateStudent:widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].attendance!.late.toString(), 
                  //       month: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].month!, 
                  //       nameClassMonitor: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].attendance!.attendanceTaker.toString(), 
                  //       permission: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].attendance!.permission.toString(), 
                  //       presence: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].attendance!.present.toString(), 
                  //       scheduleId: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].scheduleId.toString(), 
                  //       semester: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].semester.toString(), 
                  //       startTime:widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].stratTime.toString(), 
                  //       subjectName: translate=="km"? widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].subjectName.toString()
                  //         : widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].subjectNameEn.toString(), 
                  //       typeExam: widget.dataMyExam![widget.listIndex!].teachingSubjects![widget.subIndex!].type!
                  //     ),
                  //   ),
                  // );
                }
                else if(widget.setExamFrom == 1 ){
                  print("No Examination");
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                //   Navigator.push(context,MaterialPageRoute(
                //   builder: (context)=>
                //     ExamDetailScreen(
                //       isInstructor: widget.data[widget.listIndex!].isInstructor!, 
                //       setExamFrom: widget.setExamFrom, 
                //       examDate: widget.data[widget.listIndex!].teachingSubjects![0].examDate!, 
                //       isCheckScoreEnter: widget.isCheckScoreEnter, 
                //       isNoExam: isNoExam, 
                //       typeExam: typeExam, 
                //       semester: semester, 
                //       className: className, 
                //       subjectName: subjectName, 
                //       startTime: startTime, 
                //       endTime: endTime, 
                //       nameClassMonitor: nameClassMonitor,
                //       absent: absent, 
                //       presence: presence, 
                //       lateStudent: lateStudent, 
                //       permission: permission,
                //       classId: classId, 
                //       scheduleId: scheduleId, 
                //       date: date, 
                //       month: month, 
                //       activeMySchedule: activeMySchedule, 
                //       isApprove: isApprove, 
                //       subjectId: subjectId
                //     )
                //   )
                // );        
                }
                else{
                  print("Input Score immediately");
                  BlocProvider.of<GradingBloc>(context).add( GetGradingEvent(
                    idClass:  widget.classId.toString(), 
                    subjectId:  widget.subjectId.toString(), 
                    type:  '2', 
                    month:  widget.month.toString(), 
                    semester:  widget.semester.toString(),
                    examDate: ""),
                  ); 
                   Navigator.pushReplacement(context, 
                    MaterialPageRoute(builder: (context) => GradingScreen(
                      isPrimary:  widget.isPrimary,
                      isInstructor: false,
                      myclass: widget.activeMySchedule,
                      redirectFormScreen: widget.setExamFrom,
                      ),
                    ),
                  );
                } 
              }
              else{
                setState(() {
                  isLoading = false;
                });
              }
            },
            child: Container(
              margin: EdgeInsets.only(
                top: 8.0,
                bottom: 20,
                left: MediaQuery.of(context).size.width / 2.3,
                right: MediaQuery.of(context).size.width / 2.3,
              ),
              height: 4,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2.0)),
                  color: Colorconstand.neutralGrey),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Text(
              "${"SELECT_DAILY_EXAM_DAY".tr()}${widget.typeSemesterOrMonth == 1?" ${ translate=="km"?"MONTH".tr():""} ${checkMonth(widget.month)}": " ${widget.semester.tr()}"}",
              style: ThemsConstands.headline3_medium_20_26height
                  .copyWith(color: Colorconstand.neutralDarkGrey),
            ),
          ),
          const SizedBox(height: 0),
          BlocConsumer<MonthExamSetBloc, MonthExamSetState>(
            listener: (context, state) {
              if(state is MonthExamSetLoaded){
                var data = state.dayModel!.data;
                for (var i = 0; i < data!.length; i++) {
                  if (data[i].month == widget.month.toString()) {
                    setState(() {
                      activePageView = i;
                    });
                  }
                }
               _pageController = PageController(initialPage: activePageView);
              }
            },
            builder: (context, state) {
              if(state is MonthExamSetLoading){
                return Shimmer.fromColors(
                  baseColor:Colorconstand.baseColor,
                  highlightColor:Colorconstand.highlightColor,
                  child: Container(
                    height: 100,
                    margin:const EdgeInsets.only(left: 20,top: 18,bottom: 18,),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      padding:const EdgeInsets.all(0),
                      itemBuilder: ((context, index) {
                        return Container(
                          margin:const EdgeInsets.only(left: 8),
                          height: 75,
                          width: 65,
                            decoration: BoxDecoration(
                            color: Colorconstand.neutralWhite,
                            borderRadius: BorderRadius.circular(16)
                            ),
                        );
                      }),
                      itemCount: 7,
                    ),
                  ),
                );
              }
              else if(state is MonthExamSetLoaded){
                var dataMonth = state.dayModel!.data;
                  return Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.symmetric(horizontal: 12),
                        height: 65,
                        child: Row(
                          children: [
                            Expanded(child: Text("${checkMonth(int.parse(dataMonth![activePageView].month!))} ${dataMonth[activePageView].year}",style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,)),
                          ],
                        ),
                      ),
                      Container(
                        height: 100,
                        alignment: Alignment.center,
                        child: PageView.builder(
                          onPageChanged: (value) {
                            setState(() {
                              activePageView = value;
                              widget.month =  int.parse(dataMonth[value].month!);                         
                            }); 
                          },
                          controller: _pageController,
                          physics:const NeverScrollableScrollPhysics(),
                          itemCount: dataMonth.length,
                          itemBuilder: (context, indexMonth) {
                            return ListView.builder(
                              padding: const EdgeInsets.only(left: 22,right: 0,top: 0),
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: dataMonth[indexMonth].studyDays!.length,
                              itemBuilder: (context, indexDay) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isClicked = true;
                                      for (var element in dataMonth[indexMonth].studyDays!) {
                                        element.disable = false;
                                      }
                                      dataMonth[indexMonth].studyDays![indexDay].disable = true;
                                      BlocProvider.of<ScheduleSubjectTimeInDayBloc>(context).add(SchduleTimeInDaySet(date: dataMonth[indexMonth].studyDays![indexDay].date, idClass: widget.classId, idsubject: widget.subjectId));
                                      showModalBottomSheet(
                                          isScrollControlled:true,
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(16.0),
                                              topRight: Radius.circular(16.0),
                                            ),
                                          ),
                                          context: context,
                                          builder: (context) {
                                            return PopSetTime(studentDataScore: widget.studentDataScore,currenDate: widget.date,setExamFrom: widget.setExamFrom,date: dataMonth[indexMonth].studyDays![indexDay].date, activeMySchedule: widget.activeMySchedule, classId: widget.classId, month: widget.month, semester: widget.semester, subjectId: widget.subjectId, typeSemesterOrMonth: widget.typeSemesterOrMonth,);
                                          },
                                        );
                                      }
                                    );   
                                  },
                                  child: Container(
                                    width: 70,
                                    padding: const EdgeInsets.symmetric( horizontal:3.0, vertical: 3.0),
                                    margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colorconstand.lightTrunks.withOpacity(0.5),width: 1),
                                      color: dataMonth[indexMonth].studyDays![indexDay].disable == false?Colorconstand.neutralWhite:Colorconstand.primaryColor,
                                      borderRadius: BorderRadius.circular(12.0)
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment:CrossAxisAlignment.center,
                                      children: [
                                        Text(dataMonth[indexMonth].studyDays![indexDay].shortName.toString().tr(),textAlign: TextAlign.center,style: ThemsConstands.headline_6_semibold_14.copyWith(color: dataMonth[indexMonth].studyDays![indexDay].disable == false?Colorconstand.lightTrunks:Colorconstand.neutralWhite)),
                                        const SizedBox(height: 5,),
                                        Text(dataMonth[indexMonth].studyDays![indexDay].day.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16.copyWith(color: dataMonth[indexMonth].studyDays![indexDay].disable == false?Colorconstand.lightTrunks:Colorconstand.neutralWhite)),
                                        const SizedBox(height: 5,),
                                        Text( checkMonth(int.parse(dataMonth[indexMonth].month.toString())),textAlign: TextAlign.center,style: ThemsConstands.headline_6_semibold_14.copyWith(color: dataMonth[indexMonth].studyDays![indexDay].disable == false?Colorconstand.lightTrunks:Colorconstand.neutralWhite)),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  );
              }
              else{
                return Center(
                  child: EmptyWidget(
                    title: "WE_DETECT".tr(),
                    subtitle: "WE_DETECT_DES".tr(),
                  ),
                );
              }
            },
          ),

          const SizedBox(height: 65,),
    
          // const Padding(
          //   padding: EdgeInsets.symmetric(vertical: 16.0),
          //   child: Divider(
          //     thickness: 0.7,
          //     color: Colorconstand.neutralGrey,
          //     height: 1.2,
          //   ),
          // ),
         
        
          // isLoading ==true ? const Center( child: CircularProgressIndicator(),): 
          // Container(
          //   height: 50,
          //   width: double.infinity,
          //   padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          //   child: CustomMaterialButton(
          //     onPressed: () {
          //       BlocProvider.of<SetExamBloc>(context).add(SetNoExam(idClass: widget.classId, idsubject: widget.subjectId,semester: widget.semester, isNoExam: "1", type:widget.typeSemesterOrMonth.toString(), month: widget.month.toString()));
          //       setState(() {
          //         isLoading = true;
          //       });
          //     },
          //     borderRadius: BorderRadius.circular(8.0),
          //     titleButton: "NO_EXAM".tr(),
          //     padding: const EdgeInsets.symmetric(vertical: 12.0),
          //     borderColor: Colorconstand.lightTrunks,
          //     styleText: ThemsConstands.button_semibold_16 .copyWith(color: Colorconstand.lightBulma),
          //   ),
          // ),
          //   const SizedBox(height: 10,),
          //  Container(
          //   height: 50,
          //   width: double.infinity,
          //   padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          //   child: CustomMaterialButton(
          //     onPressed: () {
          //       setState((){
          //         widget.setExamFrom = 2;
          //       });
          //       BlocProvider.of<SetExamBloc>(context).add(SetNoExam(
          //           idClass: widget.classId, 
          //           idsubject: widget.subjectId,
          //           semester: widget.semester, 
          //           isNoExam: "2", 
          //           type:widget.typeSemesterOrMonth.toString(),
          //           month: widget.month.toString()
          //         )
          //       ); 

          //       // setState(() {
          //       //   isLoading = true;
          //       // });
          //     },
          //     borderRadius: BorderRadius.circular(8.0),
          //     titleButton: "INPUTSCORENOW".tr(),
          //     padding: const EdgeInsets.symmetric(vertical: 12.0),
          //     borderColor: Colorconstand.primaryColor,
          //     colorButton:  Colorconstand.primaryColor,
          //     styleText: ThemsConstands.button_semibold_16 .copyWith(color: Colorconstand.neutralWhite),
          //   ),
          // ),
          
          // const SizedBox(
          //   height: 18,
          // ),

          // Padding(padding: const EdgeInsets.all(18),child: InkWell(
          //   onTap: (){
          //     BlocProvider.of<SetExamBloc>(context).add(SetNoExam(idClass: widget.classId, idsubject: widget.subjectId,semester: widget.semester, isNoExam: "1", type:widget.typeSemesterOrMonth.toString(), month: widget.month.toString()));
          //   },
          //   child: Text("UNSCOREMONTH".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.subject18,decoration: TextDecoration.underline),),
          // ),
          // ),
          // const SizedBox(
          //   height: 20,
          // ),
        ],
      );
    });
  }
}