import 'package:camis_teacher_application/app/modules/home_screen/widget/card_exam_subject_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../models/exam_model/class_exam_model.dart';
import '../../grading_screen/bloc/grading_bloc.dart';
import '../../grading_screen/bloc/view_image_bloc.dart';
import '../../grading_screen/view/grading_screen.dart';
import '../../grading_screen/view/result_screen.dart';
import '../state_management/set_exam/bloc/set_exam_bloc.dart';
import '../state_management/state_exam/bloc/exam_schedule_bloc.dart';

class ExamCardInClass extends StatefulWidget {
  final DataClassExam dataClassExam;
  final int activeIndexMonth ;
  String semester;
  bool activeMySchedule;
  int month;
  bool isPrimary;
  int typeSemesterOrMonth;
  ExamCardInClass({ super.key,required this.isPrimary,required this.typeSemesterOrMonth, required this.dataClassExam, required this.activeIndexMonth,required this.activeMySchedule,required this.month,required this.semester});

  @override
  State<ExamCardInClass> createState() => _ExamCardInClassState();
}

class _ExamCardInClassState extends State<ExamCardInClass> with TickerProviderStateMixin {
  String classId = "";
  String subjectId = "";
  int? isInstructor;
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return BlocListener<SetExamBloc, SetExamState>(
      listener: (context, state) {
        if(state is SetExamLoading){
          setState(() {
            
          });
        }
        else if(state is SetNoExamLoaded){
            BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                idClass:  classId.toString(), 
                subjectId: subjectId.toString(), 
                type: widget.typeSemesterOrMonth.toString(), 
                month:  widget.month.toString(), 
                semester:  widget.semester.toString(),
                examDate: ""),
            );
            Navigator.push(context, 
                MaterialPageRoute(builder: (context) => GradingScreen(
                isPrimary:  widget.isPrimary,
                isInstructor: isInstructor == 0 ? false:true,
                myclass: widget.activeMySchedule,
                redirectFormScreen: 1,
                ),
              ),
            );
            if(widget.activeMySchedule==true){
              BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString()));
            }
            else{
              BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString(),classid: classId.toString()));
            }  
        }
        else{
          setState(() {
            
          });
        }
      },
      child: Container(
          margin:const EdgeInsets.only(bottom: 25,top: 12),
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.dataClassExam.teachingSubjects!.length,
              physics: const NeverScrollableScrollPhysics(),
              padding: const EdgeInsets.all(0),
              itemBuilder: (context, index) {
                final data = widget.dataClassExam.teachingSubjects![index];
                return CardExamSubjectWidget(
                  maginleft: 15,
                  numClass: widget.dataClassExam.name.toString(),
                  titleSubject:translate=="km"? data.subjectName.toString():data.subjectNameEn.toString(),
                  present: data.attendance!.isCheckAttendance==0?"--": data.attendance!.present.toString(),
                  absent: data.attendance!.isCheckAttendance==0?"--": data.attendance!.absent.toString(),
                  examDate: data.examDate.toString(),
                  isExamSet: data.id,
                  isApprove: data.isApprove,
                  isCheckScoreEnter:  data.isCheckScoreEnter,
                  isNoExam: data.isNoExam,
                  onPressed: data.isCheckScoreEnter == 1?(){
                    BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                      idClass:  widget.dataClassExam.classId.toString(), 
                      subjectId:   data.subjectId.toString(), 
                      type: data.type.toString(), 
                      month:  data.month.toString(), 
                      semester:  data.semester.toString(),
                      examDate: ""));
                    BlocProvider.of<ViewImageBloc>(context).add(GetViewImageEvent(data.id!.toString()),);
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> 
                        ResultScreen(isInstructor: widget.dataClassExam.isInstructor == 0 ? false:true
                      ),
                    )
                  );
                  }:(){
                    if(data.id == 0){
                          setState(() {
                            classId = widget.dataClassExam.classId.toString();
                            subjectId = data.subjectId.toString();
                            isInstructor = widget.dataClassExam.isInstructor;
                          });
                            BlocProvider.of<SetExamBloc>(context).add(SetNoExam(idClass: widget.dataClassExam.classId.toString(), idsubject:  data.subjectId.toString(), semester: widget.semester.toString(), isNoExam: "2", type:widget.typeSemesterOrMonth.toString(),  month: widget.month.toString(),));
                        }
                    else{

                      BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                        idClass:  widget.dataClassExam.classId.toString(), 
                        subjectId:   data.subjectId.toString(), 
                        type: data.type.toString(), 
                        month:  data.month.toString(), 
                        semester:  data.semester.toString(),
                        examDate: ""),
                        ); 
                        Navigator.push(context, 
                          MaterialPageRoute(builder: (context) => GradingScreen(
                            isPrimary:  widget.isPrimary,
                          isInstructor: widget.dataClassExam.isInstructor == 0 ? false:true,
                          myclass: widget.activeMySchedule,
                          redirectFormScreen: 1,
                          ),
                        ),
                        );

                    }
                  },
                  // onPressed: data.id == 0 ? () {
                  //   setState(() {
                  //     BlocProvider.of<MonthExamSetBloc>(context).add(MonthExamSet(idClass:widget.dataClassExam.classId.toString(), idsubject: data.subjectId.toString()));
                  //     showModalBottomSheet(
                  //       isScrollControlled:true,
                  //       shape: const RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.only(
                  //           topLeft: Radius.circular(16.0),
                  //           topRight: Radius.circular(16.0),
                  //         ),
                  //       ),
                  //       context: context,
                  //       builder: (context) {
                  //         return PopMonthForSelect(
                  //           date: data.examDate.toString(), 
                  //           typeSemesterOrMonth:widget.typeSemesterOrMonth,
                  //           classId: widget.dataClassExam.classId.toString(),
                  //           subjectId: data.subjectId.toString(),
                  //           activeMySchedule: widget.activeMySchedule,
                  //           month: widget.month,
                  //           semester: widget.semester
                  //         );
                  //       },
                  //     );
                  //   });
                  // }:() {
                  //   print(data.month!);
                  //   Navigator.push(context,
                  //     PageTransition(type:PageTransitionType.bottomToTop,child: ExamDetailScreen(
                  //       isInstructor: widget.dataClassExam.isInstructor!,
                  //       setExamFrom: 1,
                  //       subjectId: data.subjectId.toString(),
                  //       isApprove: data.isApprove!,
                  //       absent: data.attendance!.absent.toString(), 
                  //       activeMySchedule: widget.activeMySchedule, 
                  //       classId: widget.dataClassExam.classId.toString(), 
                  //       className: widget.dataClassExam.name.toString(), 
                  //       date: data.examDate.toString(), 
                  //       endTime: data.endTime.toString(), 
                  //       examDate: data.examDate.toString(), 
                  //       isCheckScoreEnter: data.isCheckScoreEnter!, 
                  //       isNoExam: data.isNoExam!, 
                  //       lateStudent: data.attendance!.late.toString(), 
                  //       month: data.month!, 
                  //       nameClassMonitor: data.attendance!.attendanceTaker.toString(), 
                  //       permission: data.attendance!.permission.toString(), 
                  //       presence: data.attendance!.present.toString(), 
                  //       scheduleId: data.scheduleId.toString(), 
                  //       semester: data.semester.toString(), 
                  //       startTime: data.startTime.toString(), 
                  //       subjectName:translate=="km"?data.subjectName.toString():data.subjectNameEn.toString(), 
                  //       typeExam: data.type!,),
                  //     ),
                  //   );
                  // },
                );
              },
            ),
        ),
    );
  }
}
