import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';

class OptionTeacherClass extends StatefulWidget {
  final String optionClassName;
  final VoidCallback ontap;
  final String isSelectClassname;
  final bool isIcon;
  final String className;
  OptionTeacherClass({super.key,required this.optionClassName,required this.ontap,required this.isSelectClassname, required this.isIcon, required this.className,});

  @override
  State<OptionTeacherClass> createState() => _OptionTeacherClassState();
}

class _OptionTeacherClassState extends State<OptionTeacherClass> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.ontap,
      child: Container(
        margin:const EdgeInsets.all(5),
        padding:const EdgeInsets.symmetric(horizontal:8,vertical: 15),
        decoration: BoxDecoration(
          color: widget.isSelectClassname==widget.optionClassName?Colorconstand.neutralSecondBackground:Colors.transparent,
          borderRadius: BorderRadius.circular(8)
        ),
        child:  Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children:[
            Text(widget.optionClassName,style: ThemsConstands.headline6_medium_14.copyWith(color: Colorconstand.lightBlack),),
            widget.isIcon == false
              ? Container(
                alignment: Alignment.centerRight,
                padding: const EdgeInsets.all(10),
                decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colorconstand.primaryColor,
                ),
                child: Text(
                  widget.className,
                  style: ThemsConstands
                      .caption_regular_12
                      .copyWith(
                          fontSize: 10.0,
                          color: Colorconstand
                              .neutralWhite),
                ),
              )
              : Container(
                margin:const EdgeInsets.only(right: 10),
                child: SvgPicture.asset(
                    ImageAssets.bank_icon_class),
              ),
          ],
        ),
      ),
    );
  }
}
