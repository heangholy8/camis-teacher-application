import 'dart:async';

import 'package:camis_teacher_application/app/bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/calculate_score/bloc/calculate_score_bloc.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/view/exam_card_in_class.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/view/exam_card_my_class.dart';
import 'package:camis_teacher_application/app/service/api/tracking_teacher_api/tracking_teacher_api.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../../widget/shimmer/shimmer_style.dart';
import '../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../exam_detail_screen/view/exam_detail_screen.dart';
import '../../home_screen/widget/card_schedule_subject_widget.dart';
import '../../schedule_expanded_screen/view/schedule_expanded_screen.dart';
import '../state_management/state_attendance/bloc/get_date_bloc.dart';
import '../state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../state_management/state_exam/bloc/exam_schedule_bloc.dart';
import '../widgets/date_picker_screen.dart';
import 'option_teacher_class.dart';
class AttendacneScheduleScreen extends StatefulWidget {
  int indexTab;
  AttendacneScheduleScreen({super.key,this.indexTab = 0});
  @override
  State<AttendacneScheduleScreen> createState() =>
      _AttendacneScheduleScreenState();
}
class _AttendacneScheduleScreenState extends State<AttendacneScheduleScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  bool isCheckClass = false;
  TabController? controller;
  String optionClassName = "MY_SCEDULE".tr();
  String optionClassNameSelect = "MY_SCEDULE".tr();
  bool isInstructor = false;
  bool activeMySchedule = true;
  String classIdTeacher = "";
  //=============== End Varible main Tap =====================


  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================

  //============= Calculate Score ===========
    bool isLoadingCalculate = false;
    int calculateSuccess = 0;
    String statusCalculate  = "";
  //================================

  //============== Varible scedule Tap ==================
  int myIndex = 0;
  PageController? pagec;
  int? monthselected;
  int? monthlocalfirst;
  int? daylocalfirst;
  int? yearchecklocalfirst;
  int daySelectNoCurrentMonth = 1;
  double offset = 0.0;
  List? lessList;
  List? homeList;
  List? teacherAbsentList;
  bool showListDay = false;
  String? date;
  int activeIndexDay=0;
  bool firstLoadOnlyIndexDay = true;
  ScrollController? controllerSchedule;

 //============== End Varible scedule Tap ==================

 //============== verible Exam Tap ====================
    int? monthLocalExam;
    int? yearLocalExam;
    String? monthLocalNameExam;
    String? monthLocalNameExamFirstLoad;
    int? monthLocalExamFirstLoad;
    int? yearLocalExamFirstLoad;
    bool showListMonthExam = false;
    String semesterName = "";
    String semesterNameFristLoad = "";
    String typeSelection = "1";
    int activeIndexMonth = 0;
 //============== verible Exam Tap ====================

    StreamSubscription? internetconnection;
    bool isoffline = true;

  bool isPrimary = false;

  final GetStoragePref _getStoragePref = GetStoragePref();


  void getLocalData() async {
    var authStoragePref = await _getStoragePref.getJsonToken;
    setState(() {
      isPrimary = authStoragePref.isPrimary!;
    });
  }

  @override
  void initState() {
    setState(() {
     getLocalData();
    //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

      //============= controller class change option ========
      _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      monthLocalNameExam = DateFormat("MMMM").format(DateTime.now()).toUpperCase();
      date = "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal";
    });
    monthlocalfirst = monthlocal;
    daylocalfirst = daylocal;
    yearchecklocalfirst = yearchecklocal;
    //==============  Exam Tap ====================
     monthLocalExam = monthlocal;
     yearLocalExam = yearchecklocal;
     monthLocalExamFirstLoad = monthlocal;
     yearLocalExamFirstLoad = yearchecklocal;
     monthLocalNameExamFirstLoad = monthLocalNameExam;
    //============== Exam Tap =====================
    //============== Event call Data =================
    BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
    BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent(month: monthlocal.toString(),year:yearchecklocal.toString()));
    BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
    BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
    //============== End Event call Data =================
    controller = TabController(length: 2, vsync: this,initialIndex: widget.indexTab);
    super.initState();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
    //========= controllerSchedule offset set ===================
    controllerSchedule = ScrollController(initialScrollOffset: offset);
    controllerSchedule!.addListener(() {
      setState(() {
        offset = controllerSchedule!.offset;
      });
    });
    //========= controllerSchedule offset set ===================
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      bottomNavigationBar:isoffline==false?Container(height: 0,):const BottomNavigateBar(isActive: 2),
       // bottomNavigationBar:Container(height: 0),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 55,
              bottom: 0,
              left: 0,
              right: 0,
              child: BlocListener<CalculateScoreBloc, CalculateScoreState>(
                listener: (context, state) {
                  if(state is CalculateLoading){
                    setState(() {
                      isLoadingCalculate = true;
                    });
                  }
                  else if(state is CalculateLoaded){
                    setState(() {
                      calculateSuccess = 1;
                      statusCalculate = "SUCCESSFULLY".tr();
                      Future.delayed(const Duration(seconds: 2),(){
                        setState(() {
                          isLoadingCalculate = false;
                        });
                      });
                    });
                  }
                  else{
                    setState(() {
                      calculateSuccess = 2;
                      statusCalculate = "FAIL".tr();
                        Future.delayed(const Duration(seconds: 2),(){
                        setState(() {
                          isLoadingCalculate = false;
                        });
                      });
                    });
                  }
                },
                child: Column(
                  children: [
                    //========== Button widget Change class==========
                    GestureDetector(
                      onTap: isInstructor == false ? null :(() {
                        setState(() {
                          _arrowAnimationController!.isCompleted
                            ? _arrowAnimationController!.reverse()
                            : _arrowAnimationController!.forward();
                          if(isCheckClass == false){
                            isCheckClass = true;
                          }
                          else{
                            isCheckClass = false;
                          }
                        });
                      }),
                      child: Container(
                        height: 45,
                        margin:const EdgeInsets.only(left: 18),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              child: Text(optionClassName, style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite)),
                            ),
                            const SizedBox(width: 12,),
                            isInstructor == false ? Container() : Align(
                              alignment: Alignment.centerRight,
                              child: AnimatedBuilder(
                                animation: _arrowAnimationController!,
                                builder: (context, child) => Transform.rotate(
                                  angle: _arrowAnimation!.value,
                                  child: const Icon(
                                    Icons.arrow_drop_down,
                                    size: 28.0,
                                    color: Colorconstand.neutralWhite,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //========== Button End widget Change class==========
                    const SizedBox(height: 10,),
                    Expanded(
                      child: BlocListener<ListMonthSemesterBloc, ListMonthSemesterState>(
                        listener: (context, state) {
                          if (state is ListMonthSemesterLoaded){
                            var data = state.monthAndSemesterList!.data;
                            for (var i = 0; i < data!.length; i++) {
                              if (data[i].displayMonth == monthLocalNameExam) {
                                setState(() {
                                  activeIndexMonth = i;
                                  semesterName = data[i].semester.toString();
                                  semesterNameFristLoad = data[i].semester.toString();
                                });
                              }
                            }
                            BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: semesterName,type:"1",month: monthLocalExam.toString()));
                          }
                        },
                        child: Container(
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Colorconstand.neutralWhite),
                          child: Column(
                            children: [
                              TabBar(
                                controller: controller,
                                onTap: (value) {
                                  setState(() {
                                    widget.indexTab = value;
                                  });
                                },
                                isScrollable: false,
                                indicatorWeight: 4,
                                indicator: const BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                        color: Colorconstand.primaryColor,
                                        width: 3),
                                  ),
                                ),
                                padding: const EdgeInsets.all(0),
                                indicatorColor: Colorconstand.primaryColor,
                                tabs: [
                                  my_tab(
                                      asset: ImageAssets.CALENDARFILL_ICON,
                                      title: "SCHEDULE".tr(),
                                      index: 0),
                                  my_tab(
                                      asset: ImageAssets.EXAM_ICON,
                                      title: "EXAM".tr(),
                                      index: 1),
                                ],
                              ),
                              Expanded(
                                child: TabBarView(
                                  physics:const NeverScrollableScrollPhysics(),
                                  controller: controller,
                                  children: [        
                      
                                //================== Schedule tab =======================
                      
                                    BlocBuilder<GetDateBloc, GetDateState>(
                                      builder: (context, state) {
                                        if (state is GetDateListLoading) {
                                          return Container(
                                            decoration: const BoxDecoration(
                                              color: Color(0xFFF1F9FF),
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(12),
                                                topRight: Radius.circular(12),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Container(
                                                  color: const Color(0x622195F3),
                                                  height: 165,
                                                ),
                                                Expanded(
                                                  child: ListView.builder(
                                                    padding: const EdgeInsets.all(0),
                                                    shrinkWrap: true,
                                                    itemBuilder: (BuildContext contex, index) {
                                                      return const ShimmerTimeTable();
                                                    },
                                                    itemCount: 5,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        } else if (state is GetDateListLoaded) {
                                          var selectDay;
                                          var datamonth = state.listDateModel!.data;
                                          var listMonthData = datamonth!.monthData;
                                          if(datamonth.month.toString() == monthlocalfirst.toString() && yearchecklocal==yearchecklocalfirst){
                                            daySelectNoCurrentMonth = 100;
                                          }
                                      //===================== loop list ======================
                                          var listDayWeek1 = listMonthData!.where((element) {
                                            return element.index! < 7;
                                          },).toList();
                                          var listDayWeek2 = listMonthData.where((element) {
                                            return element.index! >= 7 && element.index! <= 13;
                                          },).toList();
                                          var listDayWeek3 = listMonthData.where((element) {
                                            return element.index! >= 14 && element.index! <=20;
                                          },).toList();
                                          var listDayWeek4 = listMonthData.where((element) {
                                            return element.index! >= 21 && element.index! <=27;
                                          },).toList();
                                          var listDayWeek6 = listMonthData.where((element) {
                                            return element.index! > listMonthData.length-8;
                                          },).toList();
                                          var listDayWeek5 = listMonthData.length<=35?listDayWeek6:listMonthData.where((element) {
                                              return element.index! >= 28 && element.index! <= 34;
                                            },).toList();
                                      //===================== End loop list ======================
                                          return Container(
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: double.maxFinite,
                                                  decoration: BoxDecoration(
                                                      borderRadius:BorderRadius.circular(8.0),
                                                      color: Colorconstand.neutralSecondBackground,
                                                  ),
                                                  child: Column(
                                                    mainAxisSize: MainAxisSize.min,
                                                    children: [
                                                  //===========  Button Show List Month ===========================
                                                      GestureDetector(
                                                        onTap:() {
                                                          setState(() {
                                                              if(showListDay==true){
                                                                  showListDay =false;
                                                              }
                                                              else{
                                                                showListDay = true;
                                                              }
                                                          });
                                                        },
                                                        child:Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          height: 45,
                                                            child: Row(
                                                              children: [
                                                                showListDay==false ?Container():Container(
                                                                  margin:const EdgeInsets.only(left: 8),
                                                                  child: IconButton(
                                                                    onPressed: (){
                                                                      setState(() {
                                                                        if(yearchecklocal == (yearchecklocalfirst! -1)){}
                                                                        else{
                                                                          setState(() {
                                                                            daylocal = 1;
                                                                            monthlocal = 1;
                                                                            daySelectNoCurrentMonth = 1;
                                                                            yearchecklocal = yearchecklocalfirst! - 1;
                                                                            showListDay = false;
                                                                            date = "1/01/$yearchecklocal";
                                                                          });
                                                                          BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocal.toString(), year: yearchecklocal.toString()));
                                                                          if(activeMySchedule==true){
                                                                            BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                                          }
                                                                          else{
                                                                            BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: classIdTeacher));
                                                                          }
                                                                        }
                                                                      });
                                                                    }, 
                                                                    icon: Icon(Icons.arrow_back_ios_new_rounded,size: 18,color: yearchecklocal == yearchecklocalfirst! - 1?Colors.transparent:Colorconstand.primaryColor,))
                                                                ),
                                                                Expanded(
                                                                  child: Row(
                                                                  mainAxisAlignment:MainAxisAlignment.center,
                                                                  children: [
                                                                    offset > 130
                                                                        ? Container(
                                                                            height: 25,width: 25,
                                                                            margin: const EdgeInsets.only(right: 5),
                                                                            padding: const EdgeInsets.all(3),
                                                                            decoration: BoxDecoration(color: Colorconstand.primaryColor, borderRadius: BorderRadius.circular(6)),
                                                                            child: Center(
                                                                              child: Text(
                                                                                daylocal.toString(),
                                                                                style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralWhite),
                                                                              ),
                                                                            ),
                                                                          )
                                                                        : Container(),
                                                                    Text(
                                                                      checkMonth(int.parse(datamonth.month!)),
                                                                      style:
                                                                          ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),
                                                                    ),
                                                                    const SizedBox(width: 5,),
                                                                    Text(datamonth.year.toString(),
                                                                        style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor)),
                                                                    const SizedBox(
                                                                        width: 8),
                                                                  showListDay==true?Container(): Icon(
                                                                        showListDay ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                        size: 24,
                                                                        color: Colorconstand.primaryColor),
                                                                                                                                          ],
                                                                                                                                        ),
                                                                ),
                                                                showListDay==false?Container():Container(
                                                                  margin:const EdgeInsets.only(right: 8),
                                                                  child: IconButton(
                                                                    onPressed: (){
                                                                      setState(() {
                                                                        if(yearchecklocal == yearchecklocalfirst!){}
                                                                        else{
                                                                          yearchecklocal = yearchecklocalfirst;
                                                                          daySelectNoCurrentMonth = 100;
                                                                          daylocal = daylocalfirst;
                                                                          monthlocal = monthlocalfirst;
                                                                          showListDay = false;
                                                                        BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                                        if(activeMySchedule==true){
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: classIdTeacher));
                                                                        }
                                                                        }
                                                                      });
                                                                    }, 
                                                                    icon:Icon(Icons.arrow_forward_ios_rounded,size: 18,color:yearchecklocal==yearchecklocalfirst?Colors.transparent: Colorconstand.primaryColor,))
                                                                ),
                                                              ],
                                                            ),
                                                            ),
                                                      ),
                                                  //=========== End Button Show List Month ===========================
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Stack(
                                                    children: [
                                                      Column(
                                                        children: [
                                                          Container(
                                                            height: 0,
                                                            child: Stack(
                                                              children:List.generate(listMonthData.length,(subindex) {
                                                                if(firstLoadOnlyIndexDay==true){
                                                                  if(listMonthData[subindex].isActive==true){
                                                                    activeIndexDay = listMonthData[subindex].index!;
                                                                    firstLoadOnlyIndexDay = false;
                                                                  }
                                                                }
                                                                  return Container();
                                                                }
                                                              )
                                                            ),
                                                          ),
                                                          Expanded(
                                                            child: SingleChildScrollView(
                                                              controller: controllerSchedule,
                                                              child: BlocBuilder<GetScheduleBloc, GetScheduleState>(
                                                                builder: (context, state) {
                                                                  if(state is GetScheduleLoading){
                                                                    return Container(
                                                                      decoration: const BoxDecoration(
                                                                        color: Color(0xFFF1F9FF),
                                                                        borderRadius: BorderRadius.only(
                                                                          topLeft: Radius.circular(12),
                                                                          topRight: Radius.circular(12),
                                                                        ),
                                                                      ),
                                                                      child: Column(
                                                                        children: [
                                                                          ListView.builder(
                                                                            padding: const EdgeInsets.all(0),
                                                                            shrinkWrap: true,
                                                                            itemBuilder: (BuildContext contex, index) {
                                                                            return const ShimmerTimeTable();
                                                                            },
                                                                            itemCount: 5,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    );
                                                                  }
                                                                  //===========  My Schedule =========================
                                                                  else if(state is GetMyScheduleLoaded) {
                                                                    var dataMySchedule = state.myScheduleModel!.data;
                                                                    String dataMyScheduleMessage = state.myScheduleModel!.message.toString();
                                                                    return dataMySchedule!.isEmpty? 
                                                                    Container( 
                                                                      margin: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                      child: ScheduleEmptyWidget(
                                                                        title: dataMyScheduleMessage.tr(),
                                                                        subTitle: dataMyScheduleMessage == "HOLIDAY"?"HOLIDAY_DES".tr():dataMyScheduleMessage=="DATE_NOT_VALID_ACADEMIC"?"DATE_NOT_VALID_ACADEMIC_DES".tr():"SCHEDULE_NOT_SET_DES".tr(),
                                                                      )
                                                                    ):Container(
                                                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                        alignment: Alignment.center,
                                                                        child: ListView.builder(
                                                                          shrinkWrap: true,
                                                                          physics: const BouncingScrollPhysics(),
                                                                          itemCount:dataMySchedule.length,
                                                                          padding: const EdgeInsets.only(top: 10),
                                                                          itemBuilder: (context, indexmyschedule) {
                                                                            if(dataMySchedule.isNotEmpty){
                                                                              lessList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                                return element.activityType==1;
                                                                              }).toList();
                                                                            }
                                                                            if(dataMySchedule.isNotEmpty){
                                                                              homeList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                                return element.activityType==2;
                                                                              }).toList();
                                                                              teacherAbsentList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                                return element.activityType==4;
                                                                              }).toList();
                                                                            }
                                                                            return CardScheduleSubjectWidget(
                                                                              researchAbout: "",
                                                                              researchList: [],
                                                                              teacherAbsentList: teacherAbsentList,
                                                                              className: dataMySchedule[indexmyschedule].className.toString(),
                                                                              titleSubject:translate=="km"?dataMySchedule[indexmyschedule].subjectName.toString():dataMySchedule[indexmyschedule].subjectNameEn.toString(),
                                                                              isActive: dataMySchedule[indexmyschedule].isCurrent,
                                                                              subjectColor:dataMySchedule[indexmyschedule].isExam==false?"0xFF${dataMySchedule[indexmyschedule].subjectColor}":"0xFFEE964B",
                                                                              scheduleType: dataMySchedule[indexmyschedule].isExam==true?1:2,
                                                                              absent: dataMySchedule[indexmyschedule].attendance!.absent==0 || dataMySchedule[indexmyschedule].attendance!.isCheckAttendance==0?"-":dataMySchedule[indexmyschedule].attendance!.absent.toString(),
                                                                              present: dataMySchedule[indexmyschedule].attendance!.isCheckAttendance==0?"-":dataMySchedule[indexmyschedule].attendance!.present.toString(),
                                                                              examDate:dataMySchedule[indexmyschedule].isExam==true?dataMySchedule[indexmyschedule].examItem!.examDate.toString():"",
                                                                              homeworkExDate:homeList!.isEmpty?"":homeList![0].expiredAt.toString(),
                                                                              lessonAbout: lessList!.isEmpty?"":lessList![0].title.toString(),
                                                                              homeList: homeList,
                                                                              lessList: lessList,
                                                                              isApprove: dataMySchedule[indexmyschedule].isExam==true?dataMySchedule[indexmyschedule].examItem!.isApprove! :0,
                                                                              isCheckScoreEnter: dataMySchedule[indexmyschedule].isCheckScoreEnter!,
                                                                              durationSubject: dataMySchedule[indexmyschedule].duration.toString(),
                                                                              timeSubject:"${dataMySchedule[indexmyschedule].startTime} - ${dataMySchedule[indexmyschedule].endTime}",
                                                                              onPressed:dataMySchedule[indexmyschedule].isExam==true?(){ 
                                                                                Navigator.push(context,
                                                                                    PageTransition(type:PageTransitionType.bottomToTop,child: ExamDetailScreen(
                                                                                      isInstructor: dataMySchedule[indexmyschedule].isInstructor!,
                                                                                      setExamFrom: 2,
                                                                                      subjectId: dataMySchedule[indexmyschedule].subjectId.toString(),
                                                                                      isApprove: dataMySchedule[indexmyschedule].examItem!=null?dataMySchedule[indexmyschedule].examItem!.isApprove!:0,
                                                                                      absent: dataMySchedule[indexmyschedule].attendance!.absent.toString(), 
                                                                                      activeMySchedule: activeMySchedule, 
                                                                                      classId: dataMySchedule[indexmyschedule].classId.toString(), 
                                                                                      className: dataMySchedule[indexmyschedule].className.toString(), 
                                                                                      date: dataMySchedule[indexmyschedule].date.toString(), 
                                                                                      endTime: dataMySchedule[indexmyschedule].endTime.toString(), 
                                                                                      examDate: dataMySchedule[indexmyschedule].isExam==true?dataMySchedule[indexmyschedule].examItem!.examDate.toString():"", 
                                                                                      isCheckScoreEnter: dataMySchedule[indexmyschedule].isCheckScoreEnter!, 
                                                                                      isNoExam: 0, 
                                                                                      lateStudent: dataMySchedule[indexmyschedule].attendance!.late.toString(), 
                                                                                      month: monthlocal!, 
                                                                                      nameClassMonitor: dataMySchedule[indexmyschedule].attendance!.attendanceTaker.toString(), 
                                                                                      permission: dataMySchedule[indexmyschedule].attendance!.permission.toString(), 
                                                                                      presence: dataMySchedule[indexmyschedule].attendance!.present.toString(), 
                                                                                      scheduleId: dataMySchedule[indexmyschedule].scheduleId.toString(), 
                                                                                      semester: dataMySchedule[indexmyschedule].isExam==true?dataMySchedule[indexmyschedule].examItem!.semester.toString():"", 
                                                                                      startTime: dataMySchedule[indexmyschedule].startTime.toString(), 
                                                                                      subjectName:translate == "km"? dataMySchedule[indexmyschedule].subjectName.toString():dataMySchedule[indexmyschedule].subjectNameEn.toString(), 
                                                                                      typeExam: dataMySchedule[indexmyschedule].isExam==true?dataMySchedule[indexmyschedule].examItem!.type!:0,),
                                                                                    ),
                                                                                  );
                                                                                }:() {
                                                                                Navigator.push(context,
                                                                                  PageTransition(type:PageTransitionType.bottomToTop,
                                                                                  child: ScheduleExpandedScreen( 
                                                                                    displayCheckAttendance: true,
                                                                                    routFromScreen: 2,
                                                                                    data: dataMySchedule[indexmyschedule],
                                                                                    activeMySchedule: true, month: '', type: '',
                                                                                    ),
                                                                                  ),
                                                                                );
                                                                              },
                                                                            );
                                                                         
                                                                          },
                                                                        ),
                                                                    );
                                                                  }
                                                            //===========  End My Schedule =========================
                      
                                                            //===========  Class Schedule =========================
                                                                  else if( state is GetClassScheduleLoaded){
                                                                    var dataClassSchedule = state.classScheduleModel!.data;
                                                                    String dataMyScheduleMessage = state.classScheduleModel!.message.toString();
                                                                    return dataClassSchedule!.isEmpty? 
                                                                    Container( margin: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                      child: ScheduleEmptyWidget(
                                                                        title: dataMyScheduleMessage.tr(),
                                                                        subTitle: dataMyScheduleMessage=="HOLIDAY"?"HOLIDAY_DES".tr():dataMyScheduleMessage=="DATE_NOT_VALID_IN_ACADEMIC"?"DATE_NOT_VALID_ACADEMIC_DES".tr():"SCHEDULE_NOT_SET_DES".tr(),
                                                                          )
                                                                    ): Container(
                                                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:100),
                                                                      alignment: Alignment.center,
                                                                      child: ListView.builder(
                                                                        shrinkWrap: true,
                                                                        physics: const ScrollPhysics(),
                                                                        itemCount:dataClassSchedule.length,
                                                                        padding: const EdgeInsets.only(top: 10),
                                                                        itemBuilder: (context, indexclassschedule) {
                                                                          if(dataClassSchedule.isNotEmpty){
                                                                            lessList = dataClassSchedule[indexclassschedule].activities!.where((element) {
                                                                              return element.activityType==1;
                                                                            }).toList();
                                                                          }
                                                                          if(dataClassSchedule.isNotEmpty){
                                                                            homeList = dataClassSchedule[indexclassschedule].activities!.where((element) {
                                                                              return element.activityType==2;
                                                                            }).toList();
                                                                            teacherAbsentList = dataClassSchedule[indexclassschedule].activities!.where((element) {
                                                                              return element.activityType==4;
                                                                            }).toList();
                                                                          }
                                                                          return CardScheduleSubjectWidget(
                                                                            researchAbout: "",
                                                                            researchList: [],
                                                                            onPressed: dataClassSchedule[indexclassschedule].isExam==true? (){ 
                                                                                Navigator.push(context,
                                                                                    PageTransition(type:PageTransitionType.bottomToTop,child: ExamDetailScreen(
                                                                                        isInstructor: dataClassSchedule[indexclassschedule].isInstructor!,
                                                                                        setExamFrom: 2,
                                                                                        subjectId: dataClassSchedule[indexclassschedule].subjectId.toString(),
                                                                                        isApprove: dataClassSchedule[indexclassschedule].examItem!=null?dataClassSchedule[indexclassschedule].examItem!.isApprove!:0,
                                                                                        absent: dataClassSchedule[indexclassschedule].attendance!.absent.toString(), 
                                                                                        activeMySchedule: activeMySchedule, 
                                                                                        classId: dataClassSchedule[indexclassschedule].classId.toString(), 
                                                                                        className: dataClassSchedule[indexclassschedule].className.toString(), 
                                                                                        date: dataClassSchedule[indexclassschedule].date.toString(), 
                                                                                        endTime: dataClassSchedule[indexclassschedule].endTime.toString(), 
                                                                                        examDate: dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.examDate.toString():"", 
                                                                                        isCheckScoreEnter: dataClassSchedule[indexclassschedule].isCheckScoreEnter!, 
                                                                                        isNoExam: 0, 
                                                                                        lateStudent: dataClassSchedule[indexclassschedule].attendance!.late.toString(), 
                                                                                        month: monthlocal!, 
                                                                                        nameClassMonitor: dataClassSchedule[indexclassschedule].attendance!.attendanceTaker.toString(), 
                                                                                        permission: dataClassSchedule[indexclassschedule].attendance!.permission.toString(), 
                                                                                        presence: dataClassSchedule[indexclassschedule].attendance!.present.toString(), 
                                                                                        scheduleId: dataClassSchedule[indexclassschedule].scheduleId.toString(), 
                                                                                        semester: dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.semester.toString():"", 
                                                                                        startTime: dataClassSchedule[indexclassschedule].startTime.toString(),
                                                                                        subjectName:translate=="km"? dataClassSchedule[indexclassschedule].subjectName.toString():dataClassSchedule[indexclassschedule].subjectNameEn.toString(), 
                                                                                        typeExam: dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.type!:0,),
                                                                                      ),
                                                                                );
                                                                                }:() {
                                                                                Navigator.push(context,
                                                                                  PageTransition(type:PageTransitionType.bottomToTop,child: ScheduleExpandedScreen( routFromScreen: 2,data: dataClassSchedule[indexclassschedule],activeMySchedule: false, month: '', type: '', displayCheckAttendance: true,),
                                                                                  ),
                                                                                );
                                                                              },
                                                                            teacherAbsentList: teacherAbsentList,
                                                                            className: dataClassSchedule[indexclassschedule].className.toString(),
                                                                            titleSubject: translate=="km"?dataClassSchedule[indexclassschedule].subjectName.toString():dataClassSchedule[indexclassschedule].subjectNameEn.toString(),
                                                                            isActive: dataClassSchedule[indexclassschedule].isCurrent,
                                                                            subjectColor:dataClassSchedule[indexclassschedule].isExam==false?"0xFF${dataClassSchedule[indexclassschedule].subjectColor}":"0xFFEE964B",
                                                                            scheduleType: dataClassSchedule[indexclassschedule].isExam==true?1:2,
                                                                            absent: dataClassSchedule[indexclassschedule].attendance!.absent==0 || dataClassSchedule[indexclassschedule].attendance!.isCheckAttendance==0?"-":dataClassSchedule[indexclassschedule].attendance!.absent.toString(),
                                                                            present: dataClassSchedule[indexclassschedule].attendance!.isCheckAttendance==0?"-":dataClassSchedule[indexclassschedule].attendance!.present.toString(),
                                                                            examDate:dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.examDate.toString():"",
                                                                            homeworkExDate:homeList!.isEmpty?"":homeList![0].expiredAt.toString(),
                                                                            lessonAbout: lessList!.isEmpty?"":lessList![0].title.toString(),
                                                                            homeList: homeList,
                                                                            lessList: lessList,
                                                                            isApprove: dataClassSchedule[indexclassschedule].isExam==true?dataClassSchedule[indexclassschedule].examItem!.isApprove! :0,
                                                                            isCheckScoreEnter: dataClassSchedule[indexclassschedule].isCheckScoreEnter!,
                                                                            durationSubject: dataClassSchedule[indexclassschedule].duration.toString(),
                                                                            timeSubject:"${dataClassSchedule[indexclassschedule].startTime} - ${dataClassSchedule[indexclassschedule].endTime}",
                                                                          );
                                                                        
                                                                        },
                                                                      ),
                                                                    );
                                                                  }
                      
                                                            //=========== End Class Schedule =========================
                                                                  else {
                                                                    return Center(
                                                                      child: EmptyWidget(
                                                                        title: "WE_DETECT".tr(),
                                                                        subtitle: "WE_DETECT_DES".tr(),
                                                                      ),
                                                                    );
                                                                  }
                                                                },
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                      
                                                      //======================= widget Date List =======================================
                                                      AnimatedPositioned(
                                                        top: offset>130?-110:0,
                                                            left: 0,
                                                            right: 0,
                                                            duration: const Duration(milliseconds:300),
                                                        child: Container(
                                                            decoration:
                                                                const BoxDecoration(boxShadow: <BoxShadow>[
                                                              BoxShadow(
                                                                  color: Colorconstand.neutralGrey,
                                                                  blurRadius: 10.0,
                                                                  offset: Offset(5.0, 5.7))
                                                            ], color: Colorconstand.neutralSecondBackground),
                                                            width: MediaQuery.of(context).size.width,
                                                            height:MediaQuery.of(context).size.width>800? 205:97,
                                                            padding: const EdgeInsets.symmetric(vertical:10.0,horizontal:0.0),
                                                            child: PageView.builder(
                                                                controller: PageController(initialPage:monthlocalfirst != monthlocal || yearchecklocal != yearchecklocalfirst ?0:activeIndexDay <=6? 0:activeIndexDay >=7&&activeIndexDay <=13?1:activeIndexDay >=14&&activeIndexDay <=20?2:activeIndexDay >=21&&activeIndexDay <=27?3:activeIndexDay >=28&&activeIndexDay <=34?4:5),
                                                                scrollDirection: Axis.horizontal,
                                                                itemCount:listMonthData.length<=35?5:6,
                                                                itemBuilder: (context, indexDate) {
                                                                  return GridView.builder(
                                                                    physics:const NeverScrollableScrollPhysics(),
                                                                    padding:const EdgeInsets.all(0),
                                                                    itemCount: indexDate==0?listDayWeek1.length:indexDate==1?listDayWeek2.length:indexDate==2?listDayWeek3.length:indexDate==3?listDayWeek4.length:indexDate==4?listDayWeek5.length:listDayWeek6.length,
                                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 7,childAspectRatio: 0.8),
                                                                    itemBuilder: (context, index) {
                                                                      if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){
                                                                        selectDay = true;
                                                                      }
                                                                      else{
                                                                        selectDay = false;
                                                                      }
                                                                      return indexDate==0? DatePickerWidget(
                                                                        disable: listDayWeek1[index].disable!,
                                                                        eveninday: false,
                                                                        selectedday:listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth?selectDay:listDayWeek1[index].isActive!,
                                                                        day:listDayWeek1[index].shortName.toString().tr(),
                                                                        weekday:listDayWeek1[index].day.toString().tr(),
                                                                        onPressed: () {
                                                                          if(listDayWeek1[index].disable==true){}
                                                                          else{
                                                                            setState(() {
                                                                              if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){}
                                                                              else{
                                                                                daySelectNoCurrentMonth = 100;
                                                                              }
                                                                              daylocal = listDayWeek1[index].day;
                                                                              for (var element in listMonthData) {
                                                                                element.isActive = false;
                                                                              }
                                                                              date = listDayWeek1[index].date;
                                                                              listMonthData[listDayWeek1[index].index!.toInt()].isActive = true;                                                                                               
                                                                              if(activeMySchedule==true){                                                                                                
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek1[index].date));
                                                                              }
                                                                              else{
                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek1[index].date,classid: classIdTeacher));
                                                                              }
                                                                            });
                                                                          }
                                                                        },
                                                                      ):indexDate==1? DatePickerWidget(
                                                                        disable: listDayWeek2[index].disable!,
                                                                        eveninday: false,
                                                                        selectedday: listDayWeek2[index].isActive!,
                                                                        day:listDayWeek2[index].shortName.toString().tr(),
                                                                        weekday:listDayWeek2[index].day.toString(),
                                                                        onPressed: () {
                                                                          setState(() {
                                                                            daySelectNoCurrentMonth = 100;
                                                                            daylocal = listDayWeek2[index].day;
                                                                            for (var element in listMonthData) {
                                                                              element.isActive = false;
                                                                            }
                                                                            date = listDayWeek2[index].date;
                                                                            listMonthData[listDayWeek2[index].index!.toInt()].isActive = true;                                                                                                
                                                                            if(activeMySchedule==true){                                                                                                
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek2[index].date));
                                                                            }
                                                                            else{
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek2[index].date,classid: classIdTeacher));
                                                                            }
                                                                      
                                                                          });
                                                                        },
                                                                      ):indexDate==2? DatePickerWidget(
                                                                        disable: listDayWeek3[index].disable!,
                                                                        eveninday: false,
                                                                        selectedday: listDayWeek3[index].isActive!,
                                                                        day:listDayWeek3[index].shortName.toString().tr(),
                                                                        weekday:listDayWeek3[index].day.toString(),
                                                                        onPressed: () {
                                                                          setState(() {
                                                                            daySelectNoCurrentMonth = 100;
                                                                            daylocal = listDayWeek3[index].day;
                                                                            for (var element in listMonthData) {
                                                                              element.isActive = false;
                                                                            }
                                                                            date = listDayWeek3[index].date;
                                                                            listMonthData[listDayWeek3[index].index!.toInt()].isActive = true;                                                                                                
                                                                            if(activeMySchedule==true){                                                                                                
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek3[index].date));
                                                                            }
                                                                            else{
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek3[index].date,classid: classIdTeacher));
                                                                            }
                                                                      
                                                                          });
                                                                        },
                                                                      ):indexDate==3? DatePickerWidget(
                                                                        disable: listDayWeek4[index].disable!,
                                                                        eveninday: false,
                                                                        selectedday: listDayWeek4[index].isActive!,
                                                                        day:listDayWeek4[index].shortName.toString().tr(),
                                                                        weekday:listDayWeek4[index].day.toString(),
                                                                        onPressed: () {
                                                                          setState(() {
                                                                            daySelectNoCurrentMonth = 100;
                                                                            daylocal = listDayWeek4[index].day;
                                                                            for (var element in listMonthData) {
                                                                              element.isActive = false;
                                                                            }
                                                                            date = listDayWeek4[index].date;
                                                                            listMonthData[listDayWeek4[index].index!.toInt()].isActive = true;                                                                                                
                                                                            if(activeMySchedule==true){                                                                                                
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek4[index].date));
                                                                            }
                                                                            else{
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek4[index].date,classid: classIdTeacher));
                                                                            }
                                                                      
                                                                          });
                                                                        },
                                                                      ):indexDate==4? DatePickerWidget(
                                                                        disable: listDayWeek5[index].disable!,
                                                                        eveninday: false,
                                                                        selectedday: listDayWeek5[index].isActive!,
                                                                        day:listDayWeek5[index].shortName.toString().tr(),
                                                                        weekday:listDayWeek5[index].day.toString(),
                                                                        onPressed: () {
                                                                          setState(() {
                                                                            daySelectNoCurrentMonth = 100;
                                                                            daylocal = listDayWeek5[index].day;
                                                                            for (var element in listMonthData) {
                                                                              element.isActive = false;
                                                                            }
                                                                            date = listDayWeek5[index].date;
                                                                            listMonthData[listDayWeek5[index].index!.toInt()].isActive = true;                                                                                                
                                                                            if(activeMySchedule==true){                                                                                                
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek5[index].date));
                                                                            }
                                                                            else{
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek5[index].date,classid: classIdTeacher));
                                                                            }
                                                                          });
                                                                        },
                                                                      ): DatePickerWidget(
                                                                        disable: listDayWeek6[index].disable!,
                                                                        eveninday: false,
                                                                        selectedday: listDayWeek6[index].isActive!,
                                                                        day:listDayWeek6[index].shortName.toString().tr(),
                                                                        weekday:listDayWeek6[index].day.toString(),
                                                                        onPressed: () {
                                                                          setState(() {
                                                                            daySelectNoCurrentMonth = 100;
                                                                            daylocal = listDayWeek6[index].day;
                                                                            for (var element in listMonthData) {
                                                                              element.isActive = false;
                                                                            }
                                                                            date = listDayWeek6[index].date;
                                                                            listMonthData[listDayWeek6[index].index!.toInt()].isActive = true;
                                                                            if(activeMySchedule==true){                                                                                                
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: listDayWeek6[index].date));
                                                                            }
                                                                            else{
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: listDayWeek6[index].date,classid: classIdTeacher));
                                                                            }
                                                                          });
                                                                        },
                                                                      );
                                                                    },);
                                                                }),
                                                          ),
                                                      ),
                                                      /// ===================End widget List Day================================
                                                      /// 
                                                      /// =============== Baground over when drop month =============
                                                      showListDay == false
                                                      ? Container()
                                                      : Positioned(
                                                          bottom: 0,
                                                          left: 0,
                                                          right: 0,
                                                          top: 0,
                                                          child:
                                                              GestureDetector(
                                                            onTap: (() {
                                                              setState(
                                                                  () {
                                                                showListDay =
                                                                    false;
                                                              });
                                                            }),
                                                            child:
                                                                Container(
                                                              color: const Color(
                                                                  0x7B9C9595),
                                                            ),
                                                          ),
                                                        ),
                                                /// =============== Baground over when drop month =============
                                                /// 
                                                /// =============== Widget List Change Month =============
                                                      AnimatedPositioned(
                                                          top: showListDay ==
                                                                  false
                                                              ? wieght > 800? -400: -170
                                                              : 0,
                                                          left: 0,
                                                          right: 0,
                                                          duration: const Duration(milliseconds:300),
                                                          child:Container(
                                                            color: Colorconstand.neutralWhite,
                                                            child:
                                                                GestureDetector(
                                                              onTap:() {
                                                                setState(() {
                                                                  showListDay = false;
                                                                });
                                                              },
                                                              child:Container(
                                                                margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                                child: GridView.builder(
                                                                  shrinkWrap: true,
                                                                  physics:const ScrollPhysics(),
                                                                  padding:const EdgeInsets.all(.0),
                                                                    itemCount: 12,
                                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
                                                                    itemBuilder: (BuildContext context, int index) {
                                                                      return GestureDetector(
                                                                        onTap: (() {
                                                                          setState(() {
                                                                            if(monthlocal == index+1){}
                                                                            else{
                                                                              if(index+1==monthlocalfirst && yearchecklocal == yearchecklocalfirst){
                                                                                setState(() {
                                                                                  daylocal = daylocalfirst;
                                                                                });
                                                                              }
                                                                              else{
                                                                                setState(() {
                                                                                  daylocal = 1;
                                                                                });
                                                                              }
                                                                              daySelectNoCurrentMonth = 1;
                                                                              showListDay = false;
                                                                              monthlocal = (index+1);
                                                                              date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                              BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: (index+1).toString(), year: yearchecklocal.toString()));
                                                                              if(activeMySchedule==true){
                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                                              }
                                                                              else{
                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: classIdTeacher));
                                                                              }
                                                                            }
                                                                          });
                                                                        }),
                                                                        child: Container(
                                                                          margin:const EdgeInsets.all(6),
                                                                          decoration:BoxDecoration(
                                                                            borderRadius: BorderRadius.circular(12),
                                                                            color: monthlocal == (index+1)? Colorconstand.primaryColor:Colors.transparent,
                                                                          ),
                                                                          height: 20,width: 20,
                                                                          child:  Center(child: Text(((index+1) == 1 ? "JANUARY" : (index+1) == 2 ? "FEBRUARY" : (index+1) == 3 ? "MARCH" : (index+1) == 4 ? "APRIL" : (index+1) == 5 ? "MAY" : (index+1) == 6 ? "JUNE" : (index+1) == 7 ? "JULY" : (index+1) == 8 ? "AUGUST" : (index+1) == 9 ? "SEPTEMBER" : (index+1) == 10 ? "OCTOBER" : (index+1) == 11 ? "NOVEMBER" : "DECEMBER").tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthlocal==(index+1)?Colorconstand.neutralWhite:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                                        ),
                                                                      );
                                                                    },
                                                                  ),
                                                              )
                                                            ),
                                                          ),
                                                        ),
                                                    /// =============== End Widget List Change Month =============
                      
                                                  //======================= Icon Widget redirect to active date =======================
                                                    AnimatedPositioned(
                                                      bottom:monthlocal == monthlocalfirst && yearchecklocal == yearchecklocalfirst && daylocal == daylocalfirst || showListDay==true ? -60:30,right: 30,
                                                      duration: const Duration(milliseconds:300),
                                                      child: Container(
                                                        height: 55,width: 55,
                                                        decoration: BoxDecoration(
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: Colorconstand.neutralGrey.withOpacity(0.7),
                                                              spreadRadius: 4,
                                                              blurRadius: 4,
                                                              offset:const Offset(0, 3), // changes position of shadow
                                                            ),
                                                          ],
                                                          color: Colorconstand.neutralWhite,
                                                          shape: BoxShape.circle
                                                        ),
                                                        child: MaterialButton(
                                                          elevation: 8,
                                                          onPressed: (){
                                                            setState(() {
                                                                yearchecklocal = yearchecklocalfirst;
                                                                daySelectNoCurrentMonth = 100;
                                                                daylocal = daylocalfirst;
                                                                monthlocal = monthlocalfirst;
                                                                showListDay = false;
                                                            });
                                                            BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                            if(activeMySchedule==true){
                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocalfirst/${monthlocalfirst! <=9?"0$monthlocalfirst":monthlocalfirst}/$yearchecklocalfirst"));
                                                            }
                                                            else{
                                                            BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocalfirst/${monthlocalfirst! <=9?"0$monthlocalfirst":monthlocalfirst}/$yearchecklocalfirst",classid: classIdTeacher));
                                                            }
                                                          },
                                                          shape:const CircleBorder(),
                                                          child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                                        ),
                                                      ),
                                                    ),
                      
                                              //======================= End Icon Widget redirect to active date =======================
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        } else {
                                          return Center(
                                            child: EmptyWidget(
                                              title: "WE_DETECT".tr(),
                                              subtitle: "WE_DETECT_DES".tr(),
                                            ),
                                          );
                                        }
                                      },
                                    ),
                                  
                                  //================== End Schedule tab =======================
                      
                      //==============================================================================================================================================================================
                      
                                  //==================  Exam tab  =======================
                                    BlocBuilder<ListMonthSemesterBloc, ListMonthSemesterState>(
                                      builder: (context, state) {
                                        if (state is ListMonthSemesterLoading) {
                                          return Container(
                                            decoration: const BoxDecoration(
                                              color: Color(0xFFF1F9FF),
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(12),
                                                topRight: Radius.circular(12),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Container(
                                                  color: const Color(0x622195F3),
                                                  height: 165,
                                                ),
                                                Expanded(
                                                  child: ListView.builder(
                                                    padding: const EdgeInsets.all(0),
                                                    shrinkWrap: true,
                                                    itemBuilder: (BuildContext contex, index) {
                                                      return const ShimmerTimeTable();
                                                    },
                                                    itemCount: 5,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        } else if (state is ListMonthSemesterLoaded) {
                                          var datamonth = state.monthAndSemesterList!.data;
                                          return Container(
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: double.maxFinite,
                                                  decoration: BoxDecoration(
                                                      borderRadius:BorderRadius.circular(8.0),
                                                      color: Colorconstand.neutralSecondBackground,
                                                  ),
                                                  child: Column(
                                                    mainAxisSize: MainAxisSize.min,
                                                    children: [
                                                  //===========  Button Show List Month ===========================
                                                      GestureDetector(
                                                        onTap:() {
                                                          setState(() {
                                                              if(showListMonthExam==true){
                                                                  showListMonthExam =false;
                                                              }
                                                              else{
                                                                showListMonthExam = true;
                                                              }
                                                          });
                                                        },
                                                        child:Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          height: 45,
                                                            child: Row(
                                                              children: [
                                                                Expanded(
                                                                  child: Row(
                                                                  mainAxisAlignment:MainAxisAlignment.center,
                                                                  children: [
                                                                    Text(
                                                                      "${"$monthLocalNameExam".tr()}  ",
                                                                      style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),
                                                                      ),
                                                                        Text("${datamonth![activeIndexMonth].year}",
                                                                          style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor)
                                                                        ),
                                                                        const SizedBox(
                                                                          width: 8
                                                                        ),
                                                                        showListMonthExam==true?Container(): Icon(
                                                                          showListMonthExam ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                          size: 24,
                                                                          color: Colorconstand.primaryColor
                                                                        ),
                                                                      ],
                                                                    ),
                                                                ),
                                                              ],
                                                            ),
                                                            ),
                                                      ),
                                                  //=========== End Button Show List Month ===========================
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Stack(
                                                    children: [
                                                      Column(
                                                        children: [
                                                          Expanded(
                                                            child: SingleChildScrollView(
                                                              child: BlocBuilder<ExamScheduleBloc, ExamScheduleState>(
                                                                builder: (context, state) {
                                                                  if(state is GetExamScheduleLoading){
                                                                    return Container(
                                                                      decoration: const BoxDecoration(
                                                                        color: Color(0xFFF1F9FF),
                                                                        borderRadius: BorderRadius.only(
                                                                          topLeft: Radius.circular(12),
                                                                          topRight: Radius.circular(12),
                                                                        ),
                                                                      ),
                                                                      child: Column(
                                                                        children: [
                                                                          ListView.builder(
                                                                            padding: const EdgeInsets.all(0),
                                                                            shrinkWrap: true,
                                                                            itemBuilder: (BuildContext contex, index) {
                                                                            return const ShimmerTimeTable();
                                                                            },
                                                                            itemCount: 5,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    );
                                                                  }
                                                          //===========  My Exam =========================
                                                                  else if(state is GetMyExamScheduleLoaded){
                                                                    var dataMySchedule = state.myExamScheduleModel!.data;
                                                                    return dataMySchedule!.isEmpty? 
                                                                    ScheduleEmptyWidget(
                                                                      title: "SCHEDULE_NOT_SET".tr(),
                                                                      subTitle: "SCHEDULE_NOT_SET_DES".tr(),
                                                                    ) :
                                                                    Container(
                                                                      alignment: Alignment.center,
                                                                      child: ExamMyClass(
                                                                        isPrimary: isPrimary,
                                                                        dataMyExam: dataMySchedule,
                                                                        activeIndexMonth: activeIndexMonth,
                                                                        semester: semesterName,
                                                                        month: monthLocalExam!,
                                                                        activeMySchedule: activeMySchedule,
                                                                        typeSemesterOrMonth: int.parse(typeSelection),
                                                                      ),
                                                                    );
                                                                  }
                                                            //===========  End My Exam =========================
                      
                      //==============================================================================================================================================================================
                      
                                                            //===========  Class Exam =========================
                                                                  else if( state is GetClassExamScheduleLoaded){
                                                                    var dataClassSchedule = state.classExamScheduleModel!.data;
                                                                    return dataClassSchedule==null?
                                                                          ScheduleEmptyWidget(
                                                                            title: "SCHEDULE_NOT_SET".tr(),
                                                                            subTitle: "SCHEDULE_NOT_SET_DES".tr(),
                                                                          )
                                                                      : ExamCardInClass(
                                                                        isPrimary:isPrimary,
                                                                        activeIndexMonth: activeIndexMonth,
                                                                        dataClassExam: dataClassSchedule,
                                                                        semester: semesterName,
                                                                        month: monthLocalExam!,
                                                                        activeMySchedule: activeMySchedule, 
                                                                        typeSemesterOrMonth: int.parse(typeSelection),
                                                                      );
                                                                  }
                      
                                                            //=========== End Class Exam =========================
                                                                    else {
                                                                    return Center(
                                                                      child: EmptyWidget(
                                                                        title: "WE_DETECT".tr(),
                                                                        subtitle: "WE_DETECT_DES".tr(),
                                                                      ),
                                                                    );
                                                                  }
                                                                },
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      /// 
                                                      /// =============== Baground over when drop month =============
                                                      showListMonthExam == false
                                                      ? Container()
                                                      : Positioned(
                                                          bottom: 0,
                                                          left: 0,
                                                          right: 0,
                                                          top: 0,
                                                          child:
                                                              GestureDetector(
                                                            onTap: (() {
                                                              setState(
                                                                  () {
                                                                showListMonthExam =
                                                                    false;
                                                              });
                                                            }),
                                                            child:
                                                                Container(
                                                              color: const Color(
                                                                  0x7B9C9595),
                                                            ),
                                                          ),
                                                        ),
                                                /// =============== Baground over when drop month =============
                                                /// 
                                                /// =============== Widget List Change Month =============
                                                      AnimatedPositioned(
                                                          top: showListMonthExam ==
                                                                  false
                                                              ? wieght > 800?-700:-270
                                                              : 0,
                                                          left: 0,
                                                          right: 0,
                                                          duration: const Duration(milliseconds:300),
                                                          child:Container(
                                                            color: Colorconstand.neutralWhite,
                                                            child:
                                                                GestureDetector(
                                                              onTap:() {
                                                                setState(() {
                                                                  showListMonthExam = false;
                                                                });
                                                              },
                                                              child:Container(
                                                                margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                                child: GridView.builder(
                                                                  shrinkWrap: true,
                                                                  physics:const NeverScrollableScrollPhysics(),
                                                                  padding:const EdgeInsets.all(.0),
                                                                    itemCount: datamonth.length,
                                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5),
                                                                    itemBuilder: (BuildContext context, int index) {
                                                                      return datamonth[index].displayMonth== "YEAR"?Container():GestureDetector(
                                                                        onTap: (() {
                                                                          setState(() {
                                                                            monthLocalNameExam = datamonth[index].displayMonth;
                                                                            showListMonthExam = false;
                                                                            monthLocalExam =  int.parse(datamonth[index].month.toString());
                                                                            semesterName = datamonth[index].semester.toString();
                                                                            if(datamonth[index].displayMonth=="FIRST_SEMESTER"||datamonth[index].displayMonth=="SECOND_SEMESTER"){
                                                                              setState(() {
                                                                                activeIndexMonth = index - 2;
                                                                                typeSelection = "2";
                                                                              });
                                                                              if(activeMySchedule==true){
                                                                                BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: datamonth[index].semester,type: "2",month: datamonth[index].month.toString()));
                                                                              }
                                                                              else{
                                                                                BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: datamonth[index].semester,type: "2",month: datamonth[index].month.toString(),classid: classIdTeacher));
                                                                              }
                                                                            }
                                                                            else{
                                                                              setState(() {
                                                                                activeIndexMonth = index;
                                                                                typeSelection = "1";
                                                                              });
                                                                              if(activeMySchedule==true){
                                                                                BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: datamonth[index].semester,type: "1",month: datamonth[index].month.toString()));
                                                                              }
                                                                              else{
                                                                                BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: datamonth[index].semester,type: "1",month: datamonth[index].month.toString(),classid: classIdTeacher));
                                                                              }
                                                                            }
                                                                          });
                                                                        }),
                                                                        child: Container(
                                                                          margin:const EdgeInsets.all(8),
                                                                          decoration:BoxDecoration(
                                                                            borderRadius: BorderRadius.circular(12),
                                                                            color: monthLocalNameExam == datamonth[index].displayMonth? Colorconstand.primaryColor:Colors.transparent,
                                                                          ),
                                                                          child:  Center(child: Text("${datamonth[index].displayMonth}".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthLocalNameExam == datamonth[index].displayMonth?Colorconstand.neutralWhite
                                                                          : datamonth[index].displayMonth =="FIRST_SEMESTER"||datamonth[index].displayMonth=="YEAR"||datamonth[index].displayMonth=="SECOND_SEMESTER"? Colorconstand.alertsDecline :Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                                        ),
                                                                      );
                                                                    },
                                                                  ),
                                                              )
                                                            ),
                                                          ),
                                                        ),
                                                    /// =============== End Widget List Change Month =============
                      
                                                  //======================= Icon Widget redirect to active date =======================
                                                    AnimatedPositioned(
                                                      bottom: monthLocalNameExam == monthLocalNameExamFirstLoad || showListMonthExam==true?-60:30,right: 30,
                                                      duration: const Duration(milliseconds:300),
                                                      child: Container(
                                                        height: 55,width: 55,
                                                        decoration: BoxDecoration(
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: Colorconstand.neutralGrey.withOpacity(0.7),
                                                              spreadRadius: 4,
                                                              blurRadius: 4,
                                                              offset:const Offset(0, 3), // changes position of shadow
                                                            ),
                                                          ],
                                                          color: Colorconstand.neutralWhite,
                                                          shape: BoxShape.circle
                                                        ),
                                                        child: MaterialButton(
                                                          elevation: 8,
                                                          onPressed: (){
                                                            setState(() {
                                                              monthLocalNameExam = monthLocalNameExamFirstLoad;
                                                              if(activeMySchedule==true){
                                                                BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: semesterNameFristLoad,type: "1",month: monthLocalExamFirstLoad.toString()));
                                                              }
                                                              else{
                                                                BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: semesterNameFristLoad,type: "1",month: monthLocalExamFirstLoad.toString(),classid: classIdTeacher));
                                                              }
                                                            });
                                                          },
                                                          shape:const CircleBorder(),
                                                          child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                                        ),
                                                      ),
                                                    ),
                      
                                              //======================= End Icon Widget redirect to active date =======================
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        } else {
                                          return Center(
                                            child: EmptyWidget(
                                              title: "WE_DETECT".tr(),
                                              subtitle: "WE_DETECT_DES".tr(),
                                            ),
                                          );
                                        }
                                      },
                                    ),
                                //================== End Exam tab  ======================= 
                      
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    // ====================  Button Calculate Score ===========================
        AnimatedPositioned(
            duration:const Duration(milliseconds: 300),
              bottom:widget.indexTab == 1 && activeMySchedule == false?0:-60,left: 0,right: 0,
              child: MaterialButton(
                onPressed: (){
                  BlocProvider.of<CalculateScoreBloc>(context).add(CalculateExam(
                    idClass: classIdTeacher,
                    month: monthLocalExam.toString(),
                    type: typeSelection,
                    semester: semesterName,
                  ));
                  PostActionTeacherApi().actionTeacherApi(feature: "SCORE" ,keyFeature: "CALCULATE_SCORE");
                },
                height: 55,
                shape: const RoundedRectangleBorder( borderRadius: BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12))),
                  color: Colorconstand.primaryColor,
                  child: Text( "CALCULATE_SCORE".tr(), style:ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),
                ),
              ),
            ),
        // ====================  Button Calculate Score =================

        //================= background when pop change class ============
        isCheckClass==true ? AnimatedPositioned(
          duration:const Duration(milliseconds: 4000),
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        child: GestureDetector(
          onTap: () {
          setState(() {
              isCheckClass = false;
              _arrowAnimationController!.isCompleted
            ? _arrowAnimationController!.reverse()
            : _arrowAnimationController!.forward();
          });
        },child: Container(color: Colors.transparent,width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,),
        )) : Positioned(top: 0,child: Container(height: 0,)),
        //================= baground when pop change class ============

        //================ Popup Change Class ========================
            Positioned(
              top: 100,left: 12,
              child: BlocConsumer<ProfileUserBloc, ProfileUserState>(
                    listener: (context, state) {
                      if(state is GetUserProfileLoaded){
                        var dataTeacherprofile = state.profileModel!.data;
                        if(dataTeacherprofile!.instructorClass!.isNotEmpty){
                          setState(() {
                            isInstructor = true;
                          });
                        }
                      }
                    },
                    builder: (context, state) {
                      if(state is GetUserProfileLoading){
                        return Container();
                      }
                      else if(state is GetUserProfileLoaded){
                        var data = state.profileModel!.data;
                        return isCheckClass==true? Container(
                          width: 220,
                          decoration: BoxDecoration(
                            color: Colorconstand.neutralWhite,
                            borderRadius: BorderRadius.circular(12),
                            boxShadow: const[
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 15.0,spreadRadius: 1.0,
                                offset: Offset(1.0,1.0,
                                ),
                              )
                            ],
                          ),
                          child: Column(
                            children: [
                              OptionTeacherClass(
                                className: "",
                                isIcon: true,
                                isSelectClassname: optionClassName,
                                ontap:optionClassName==optionClassNameSelect?(){
                                  setState(() {
                                    isCheckClass= false;
                                    _arrowAnimationController!.isCompleted
                                    ? _arrowAnimationController!.reverse()
                                    : _arrowAnimationController!.forward();
                                  });
                                }:(){
                                  setState(() {
                                    activeMySchedule = true;
                                    BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                    BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: semesterName,type: typeSelection,month: monthLocalExam.toString()));
                                      isCheckClass= false;
                                    _arrowAnimationController!.isCompleted
                                    ? _arrowAnimationController!.reverse()
                                    : _arrowAnimationController!.forward();
                                    optionClassName = optionClassNameSelect;
                                  });
                                },
                                optionClassName: optionClassNameSelect,
                              ),
                              Container(
                                child: ListView.builder(
                                  physics:const NeverScrollableScrollPhysics(),
                                  padding:const EdgeInsets.all(0),
                                  shrinkWrap: true,
                                  itemCount: data!.instructorClass!.length,
                                  itemBuilder: (context, index) {
                                    return OptionTeacherClass(
                                      className: data.instructorClass![index].className.toString(),
                                      isIcon: false,
                                      isSelectClassname: optionClassName,
                                      ontap:optionClassName=="${"CLASS".tr()} : ${data.instructorClass![index].className}"?(){
                                        setState(() {
                                            activeMySchedule = false;
                                          isCheckClass= false;
                                            _arrowAnimationController!.isCompleted
                                          ? _arrowAnimationController!.reverse()
                                          : _arrowAnimationController!.forward();
                                        });
                                      }:(){
                                        setState(() {
                                          activeMySchedule = false;
                                          isCheckClass= false;
                                            _arrowAnimationController!.isCompleted
                                          ? _arrowAnimationController!.reverse()
                                          : _arrowAnimationController!.forward();
                                          classIdTeacher = data.instructorClass![index].classId.toString();
                                          optionClassName = "${"CLASS".tr()} : ${data.instructorClass![index].className}";
                                          BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal",classid: data.instructorClass![index].classId.toString()));
                                          BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: semesterName,type: typeSelection,month: monthLocalExam.toString(),classid: data.instructorClass![index].classId.toString()));
                                        });
                                      },
                                      optionClassName:"${"CLASS".tr()} : ${data.instructorClass![index].className}",
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                        ):Container(height: 0,);
                      }
                      else if(state is GetUserProfileError){
                        return Container();
                      }
                      else{
                        return Container();
                      }
                    },
                  ),
              
            ),
        //================ End Popup Change Class ========================

        //================ loading when calculate ========================
          isLoadingCalculate == true
            ? Positioned(
                child: Container(
                  color:
                      Colorconstand.primaryColor.withOpacity(.6),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "CALCULATE_SCORE".tr(),
                        style: ThemsConstands
                            .headline_2_semibold_24
                            .copyWith(color: Colors.white),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "IN_PROGRESS_CALCULATE_SCORE".tr(),
                        textAlign: TextAlign.center,
                        style: ThemsConstands.headline_5_medium_16
                            .copyWith(color: Colors.white),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      calculateSuccess ==1?const Icon(Icons.check_circle_outline_sharp,color: Colorconstand.alertsPositive,size: 65,)
                      :calculateSuccess==2? const Icon(Icons.error_outline_sharp,color: Colorconstand.alertsDecline,size: 65,):const CircularProgressIndicator(
                        color: Colors.white,
                      ),
                      Text(
                        statusCalculate,
                        style: ThemsConstands
                            .headline_4_medium_18
                            .copyWith(color: Colorconstand.neutralWhite),
                      ),
                    ],
                  ),
                ),
              )
            : Container(),
      //================ loading when calculate ========================

      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
          ],
        ),
      ));

  }

  Widget my_tab(
          {required String asset, required String title, required int index}) =>
      Tab(
        iconMargin: const EdgeInsets.all(0),
        child: Align(
          alignment: Alignment.center,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SvgPicture.asset(
                asset,
                color: widget.indexTab == index
                    ? Colorconstand.primaryColor
                    : Colorconstand.lightTrunks,
              ),
              const SizedBox(
                width: 16,
              ),
              Text(
                title,
                style: ThemsConstands.headline_4_medium_18.copyWith(
                  color: widget.indexTab == index
                      ? Colorconstand.primaryColor
                      : Colorconstand.lightTrunks,
                ),
              )
            ],
          ),
        ),
      );
}
