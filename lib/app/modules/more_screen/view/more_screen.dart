import 'dart:async';
import 'dart:ui';
import 'package:camis_teacher_application/app/help/save_file_helper.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/account_confirm_screen/bloc/profile_user_bloc.dart';
import 'package:camis_teacher_application/app/modules/help_screen/view/help_screen.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/principle/summary_info_school_screen/view/summary_info_school_screen.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/list_attendance_teacher/view/list_attendance_teacher_screen.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/screen/class_in_grade.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/check_school_pachage/check_school_use_package_bloc.dart';
import 'package:camis_teacher_application/app/routes/app_route.dart';
import 'package:camis_teacher_application/app/storages/remove_storage.dart';
import 'package:camis_teacher_application/widget/button_widget/button_setting_widget.dart';
import 'package:camis_teacher_application/widget/navigate_bottom_bar_widget/navigate_bottom_bar_principle.dart';
import 'package:camis_teacher_application/widget/navigate_bottom_bar_widget/navigate_bottom_bar_study_affair.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../widget/button_widget/button_icon_left_with_title.dart';
import '../../../../widget/button_widget/button_widget.dart';
import '../../../../widget/card_widget/card_feature_more.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/shimmer/shimmer_style.dart';
import '../../principle/feadback_screen/view/feedback_screen.dart';
import '../../principle/report/view/principle_report_screen.dart';
import '../../profile_user/profile_user_screen.dart';
import '../../report_screen/view/report_screen_v2.dart';
import '../../request_absent_screen/view/new_request_absent_screen/request_absent_screen.dart';
import '../../select_lang/local_data/local_langModel.dart';
import '../../student_list_screen/bloc/student_in_class_bloc.dart';
import '../../study_affaire_screen/affaire_home_screen/state/current_grade/bloc/current_grade_bloc.dart';
import '../../study_affaire_screen/list_class_in_grade/view/list_class_in_grade_screen.dart';

class MoreScreen extends StatefulWidget {
  String? role;
  bool? isPrimart;
  MoreScreen({Key? key,required this.role,required this.isPrimart}) : super(key: key);

  @override
  State<MoreScreen> createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> with Toast {
  List<Lang> langs = allLangs;
  updateLanguageLocal(Locale locale, BuildContext context) {
    context.setLocale(locale);
  }

  bool connection = true;
  StreamSubscription? sub;
  bool closeNotificate = false;
  bool showPaymentSevice = false;
  String teacherRole = "";
  String teachername = "";
  String qrCodeProfile = "";
  bool schoolIsUsePackage = false;
  final RemoveStoragePref _removeStorage = RemoveStoragePref();
  @override
  void initState() {
    setState(() {
      teacherRole = widget.role.toString();
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
      //=============Eend Check internet====================
    });
    BlocProvider.of<CheckSchoolUsePackageBloc>(context).add(GetCheckSchoolUsePachage());
    BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
    BlocProvider.of<GetClassBloc>(context).add(GetClass());
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    print("sadl;fdsa${widget.role} ${MediaQuery.of(context).size.width}");
    final translate = context.locale.toString();
    return Scaffold(
      bottomNavigationBar: connection == false
          ? Container(
              height: 0,
            )
          : widget.role == "1"? const BottomNavigateBarPrinciple(isActive: 5):widget.role == "6"? const BottomNavigateBarStudyAffair(isActive: 5):const BottomNavigateBar(isActive: 5),
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset("assets/images/Path_20.png"),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Image.asset("assets/images/Path_19.png"),
          ),
          Positioned(
            bottom: 30,
            right: 0,
            child: Image.asset("assets/images/Path_18.png"),
          ),
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
              child: Container(
                color: Colorconstand.neutralWhite.withOpacity(0.3),
                child: SafeArea(
                  bottom: false,
                  child: Container(
                    margin:const EdgeInsets.only(top: 10),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              top: 0, left: 26, right: 26, bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "MORENAV".tr(),
                                  style: ThemsConstands.headline_2_semibold_24
                                      .copyWith(
                                    color: Colorconstand.neutralDarkGrey,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Row(
                                children: [
                                  GestureDetector(
                                    onTap: (){
                                      showModalBottomSheet(
                                        context: context,
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                                        builder: (context) {
                                          return Container(
                                            padding: const EdgeInsets.only(
                                              top: 12.0,
                                            ),
                                            decoration: const BoxDecoration(
                                                color: Colorconstand.neutralWhite,
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(15),
                                                    topRight: Radius.circular(15))),
                                            child: Container(
                                              margin: const EdgeInsets.all(22),
                                              child: SingleChildScrollView(
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        GestureDetector(
                                                          onTap: (){
                                                            Navigator.of(context).pop();
                                                          },
                                                          child: Container(
                                                            alignment: Alignment.centerRight,
                                                            height: 30,
                                                            width: 30,
                                                            child: const Icon(Icons.close_rounded,color: Colorconstand.neutralDarkGrey,size: 28,),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets.only(top: 18),
                                                      child: Text(
                                                        "QR_CODE_ACCOUNT".tr(),
                                                        style: ThemsConstands.headline_2_semibold_24
                                                            .copyWith(color: Colorconstand.primaryColor),
                                                      ),
                                                    ),
                                                    Container(height: 20,),
                                                    Container(
                                                      height: MediaQuery.of(context).size.width/2.5,
                                                      width: MediaQuery.of(context).size.width/2.5,
                                                      child: Image.network(qrCodeProfile,fit: BoxFit.cover,),
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets.only(top: 22),
                                                      child: Container(
                                                        child: Button_Custom(
                                                          titleButton: "DOWNLOAD_PIC".tr(),
                                                          buttonColor: Colorconstand.primaryColor,
                                                          radiusButton: 10,
                                                          onPressed: () {
                                                            setState((){
                                                              saveImgae(image: qrCodeProfile);
                                                              // GallerySaver.saveImage(qrCodeProfile,albumName: "CAMIS");
                                                              Navigator.of(context).pop();
                                                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colorconstand.primaryColor,content: Text("Save to Gallarys!",style: TextStyle(fontSize: 16),textAlign:TextAlign.center)));
                                                            });
                                                          },
                                                          hightButton: 55,
                                                          titlebuttonColor: Colorconstand.neutralWhite,
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                                )
                                              )
                                            )
                                          );
                                        }
                                      );
                                    },
                                    child:const Icon(Icons.qr_code_scanner_outlined,size: 25,),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                    ///====================== Show Bottom Sheet ====================================
                                      showModalBottomSheet(
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(16),
                                                topRight: Radius.circular(16)),
                                          ),
                                          context: context,
                                          builder: (context) {
                                            return StatefulBuilder(builder:
                                                (BuildContext context,
                                                    StateSetter stateSetter) {
                                              return Container(
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: [
                                                    Container(
                                                      padding:
                                                          const EdgeInsets.symmetric(
                                                              vertical: 16,
                                                              horizontal: 28),
                                                      child: Text("CHOOSE_LANGUADE".tr(),
                                                          style: ThemsConstands
                                                              .headline_4_medium_18
                                                              .copyWith(
                                                            color: Colorconstand
                                                                .neutralDarkGrey,
                                                          )),
                                                    ),
                                                    const Divider(
                                                      height: 1,
                                                      thickness: 0.5,
                                                    ),
                                                    Container(
                                                      color: Colorconstand.neutralWhite,
                                                      child: ListView.builder(
                                                          shrinkWrap: true,
                                                          padding:
                                                              const EdgeInsets.all(0),
                                                          itemCount: langs.length,
                                                          itemBuilder:
                                                              (context, index) {
                                                            var lang = langs[index];
                                                            return Container(
                                                              alignment:
                                                                  Alignment.center,
                                                              decoration: BoxDecoration(
                                                                  color: translate ==
                                                                          lang.sublang
                                                                      ? Colorconstand
                                                                          .neutralSecondary
                                                                      : Colorconstand
                                                                          .neutralWhite,
                                                                  border: Border(
                                                                      bottom:
                                                                          BorderSide(
                                                                    color: Colorconstand
                                                                        .neutralDarkGrey
                                                                        .withOpacity(
                                                                            0.1),
                                                                    width: 1,
                                                                  ))),
                                                              height: 85,
                                                              child: ListTile(
                                                                leading: SizedBox(
                                                                  height: 50,
                                                                  width: 50,
                                                                  child: ClipOval(
                                                                      child:langs[index].sublang == "la"?
                                                                      Image.asset(lang.image,fit: BoxFit.cover,)
                                                                      :SvgPicture.asset(lang.image,fit: BoxFit.cover,)),
                                                                ),
                                                                title: Text(
                                                                  lang.title,
                                                                  style: translate ==
                                                                          langs[index]
                                                                              .sublang
                                                                      ? ThemsConstands
                                                                          .headline_4_medium_18
                                                                      : ThemsConstands
                                                                          .headline_4_medium_18
                                                                          .copyWith(
                                                                              color: Colorconstand
                                                                                  .lightBlack),
                                                                ),
                                                                trailing:langs[index].sublang == "th" || langs[index].sublang == "la" || langs[index].sublang == "vi"? 
                                                                 const Text("Comming soon...")
                                                                :translate == langs[index].sublang
                                                                    ? const Icon(
                                                                        Icons.check_circle_outline_sharp,
                                                                        color: Colorconstand.primaryColor,
                                                                      )
                                                                    : const Icon(null),
                                                                onTap: () {
                                                                  setState(() {
                                                                    //Navigator.of(context).pop();
                                                                    if(langs[index].sublang == "th" || langs[index].sublang == "la" || langs[index].sublang == "vi"){}
                                                                    else{
                                                                      updateLanguageLocal(Locale(langs[index].sublang),context);
                                                                      Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                                                                        pageBuilder: (context, animation1, animation2) =>
                                                                            MoreScreen(role: widget.role,isPrimart: widget.isPrimart,),
                                                                        transitionDuration: Duration.zero,
                                                                        reverseTransitionDuration: Duration.zero,
                                                                      ),(route) => false);
                                                                    }
                                                                  });
                                                                },
                                                              ),
                                                            );
                                                          }),
                                                    ),
                                                    // Container(
                                                    //   margin: const EdgeInsets.only(
                                                    //       left: 22,
                                                    //       right: 22,
                                                    //       top: 12,
                                                    //       bottom: 12),
                                                    //   child: Row(
                                                    //     children: [
                                                    //       Expanded(
                                                    //         child: Column(
                                                    //           crossAxisAlignment:
                                                    //               CrossAxisAlignment
                                                    //                   .start,
                                                    //           children: [
                                                    //             Text(
                                                    //                 "NOTIFICATION_TEACHER"
                                                    //                     .tr(),
                                                    //                 style: ThemeConstands
                                                    //                     .headline4_Medium_18
                                                    //                     .copyWith(
                                                    //                   color: Colorconstands
                                                    //                       .neutralDarkGrey,
                                                    //                 )),
                                                    //             Text(
                                                    //                 "SUBTITLE_NOTIFICATION"
                                                    //                     .tr(),
                                                    //                 style: ThemeConstands
                                                    //                     .headline6_Regular_14_24height
                                                    //                     .copyWith(
                                                    //                   color:
                                                    //                       Colorconstands
                                                    //                           .lightBulma,
                                                    //                 )),
                                                    //           ],
                                                    //         ),
                                                    //       ),
                                                    //       Container(
                                                    //         child: CupertinoSwitch(
                                                    //           activeColor:
                                                    //               Colorconstands
                                                    //                   .primaryColor,
                                                    //           value: closeNotificate,
                                                    //           onChanged: (val) {
                                                    //             setState(() {
                                                    //               closeNotificate = val;
                                                    //             });
                                                    //           },
                                                    //         ),
                                                    //       ),
                                                    //     ],
                                                    //   ),
                                                    // ),
                                                    const SizedBox(
                                                      height: 15,
                                                    )
                                                  ],
                                                ),
                                              );
                                            });
                                        }
                                      );
                                      
                                      ///====================== End Show Bottom Sheet ====================================
                                    },
                                    child: SvgPicture.asset(
                                      ImageAssets.setting_outlinr_icon,
                                      width: 24,
                                      height: 24,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Container(
                              margin: const EdgeInsets.only(
                                top: 15,
                                left: 26,
                                right: 26,
                              ),
                              child: Column(
                                children: [
                                  BlocConsumer<CheckSchoolUsePackageBloc, CheckSchoolUsePackageState>(
                                    listener: (context, state) {
                                      if(state is CheckSchoolUsePackageSuccess){
                                        var dataCheckSchoolUsePackage = state.checkSchoolUsePackage;
                                        setState(() {
                                          schoolIsUsePackage = dataCheckSchoolUsePackage.isUsePackage!;
                                        });
                                      }
                                    },
                                    builder: (context, state) {
                                      return Container();
                                    },
                                  ),
                                  BlocConsumer<ProfileUserBloc, ProfileUserState>(
                                    listener: (context, state) {
                                      if(state is GetUserProfileLoaded){
                                        var data = state.profileModel!.data;
                                        setState(() {
                                            qrCodeProfile = data!.qrCode.toString();
                                            teacherRole = data.role.toString();
                                            teachername = translate =="km"?data.name.toString():data.nameEn.toString();
                                        });
                                        if(data!.instructorClass!.isNotEmpty){
                                          setState(() {
                                            showPaymentSevice = true;
                                            teacherRole = data.role.toString();
                                          });
                                        }
                                      }
                                    },
                                    builder: (context, state) {
                                      if (state is GetUserProfileLoading) {
                                        return const ShimmerProfileLoading();
                                      } else if (state is GetUserProfileLoaded) {
                                        var data = state.profileModel!.data;
                                        return Column(
                                          children: [
                                            CircleAvatar(
                                              radius: 37, // Image radius
                                              backgroundImage: NetworkImage(data!
                                                  .profileMedia!.fileShow
                                                  .toString()),
                                            ),
                                            Container(
                                                margin: const EdgeInsets.only(
                                                    top: 19, bottom: 10),
                                                alignment: Alignment.center,
                                                child: Text(
                                                  translate == "km"
                                                      ? "${data.lastname} ${data.firstname}"
                                                      : " ${data.lastnameEn==""?"${data.lastname}":"${data.lastnameEn}"} ${data.firstnameEn==""?"${data.firstname}":"${data.firstnameEn}"}",
                                                  style: ThemsConstands
                                                      .headline_2_semibold_24
                                                      .copyWith(
                                                    color: Colorconstand
                                                        .neutralDarkGrey,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                )),
                                            Container(
                                                alignment: Alignment.center,
                                                child: Text(
                                                   data.role == 1?"នាយកសាលា".tr():data.monitorGrades != ""? "STUDY_AFFAIRE".tr():data.instructorClass!.isNotEmpty
                                                      ? "ISINSTRUCTOR".tr()
                                                      : "ISTEACHER".tr(),
                                                  style: ThemsConstands
                                                      .headline_6_regular_14_20height
                                                      .copyWith(
                                                    color: Colorconstand
                                                        .darkTextsPlaceholder,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                )),
                                            Container(
                                              margin: const EdgeInsets.only(
                                                top: 12,
                                              ),
                                              child: ButtonIconLeftWithTitle(
                                                iconImage: ImageAssets.user_edit,
                                                onTap: (() {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          ProfileUserScreen(
                                                            userRole: widget.role,
                                                        profileMedia: data
                                                            .profileMedia!.fileShow
                                                            .toString(),
                                                        firstname: data.firstname,
                                                        firstnameEn:
                                                            data.firstnameEn,
                                                        lastname: data.lastname,
                                                        lastnameEn: data.lastnameEn,
                                                        gmail: data.email,
                                                        gender: data.gender,
                                                        address: data.address,
                                                        role: data.instructorClass!
                                                            .length,
                                                        dob: data.dob,
                                                        translate: translate,
                                                        whereEdit: "profileUser",
                                                        phone: data.phone,
                                                      ),
                                                    ),
                                                  );
                                                }),
                                                title: "MYPROFILE".tr(),
                                                textStyleButton: ThemsConstands
                                                    .headline_6_semibold_14
                                                    .copyWith(
                                                        color: Colorconstand
                                                            .neutralWhite),
                                                buttonColor:
                                                    Colorconstand.primaryColor,
                                                radiusButton: 8,
                                                weightButton: 170,
                                                panddinHorButton: 16,
                                                panddingVerButton: 6,
                                                iconColor:
                                                    Colorconstand.neutralWhite,
                                              ),
                                            ),
                                          ],
                                        );
                                      } else if (State is GetUserProfileError) {
                                        return const Center(
                                          child: ShimmerProfileLoading(),
                                        );
                                      } else {
                                        return Column(
                                          children: [
                                            Shimmer.fromColors(
                                              baseColor: const Color(0xFF3688F4),
                                              highlightColor:
                                                  const Color(0xFF3BFFF5),
                                              child: Container(
                                                height: 75,
                                                width: 75,
                                                decoration: const BoxDecoration(
                                                    color: Color(0x622195F3),
                                                    shape: BoxShape.circle),
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.only(
                                                  top: 19, bottom: 10),
                                              alignment: Alignment.center,
                                              child: Shimmer.fromColors(
                                                baseColor: const Color(0xFF3688F4),
                                                highlightColor:
                                                    const Color(0xFF3BFFF5),
                                                child: Container(
                                                  height: 40,
                                                  width: 175,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(16),
                                                    color: const Color(0x622195F3),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.center,
                                              child: Shimmer.fromColors(
                                                baseColor: const Color(0xFF3688F4),
                                                highlightColor:
                                                    const Color(0xFF3BFFF5),
                                                child: Container(
                                                  height: 30,
                                                  width: 275,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(16),
                                                    color: const Color(0x622195F3),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      }
                                    },
                                  ),
                                  const Divider(
                                    height: 18,
                                    thickness: 0.5,
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                      top: 8,
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: CardFeatureMore(
                                            iconImage: ImageAssets.people_icon,
                                            onTap: () {
                                             if(teacherRole == "6" || teacherRole == "1"){
                                                if(teacherRole == "1"){
                                                  BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradePriciple());
                                                }
                                                else if(teacherRole == "6"){
                                                  BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradeAffaire());
                                                }
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=> const ListClassInGradeScreen(routFromScreen: 1,)));
                                             }
                                             else{
                                                Navigator.pushNamed(context,Routes.STUDENTLISTSCREEN);
                                             }
                                            },
                                            title: "LEDGERSTUDENT".tr(),
                                            textStyleButton: ThemsConstands
                                                .headline_6_semibold_14
                                                .copyWith(
                                              color: Colorconstand.darkTextsRegular,
                                            ),
                                            buttonColor: Colorconstand.neutralWhite,
                                            radiusButton: 12,
                                            heightButton: 79,
                                            panddinHorButton: 16,
                                            panddingVerButton: 6,
                                          ),
                                        ),
                                        SizedBox(
                                          width: widget.isPrimart == true?0:18,
                                        ),
                                        widget.isPrimart == true?const SizedBox():Expanded(
                                          child: CardFeatureMore(
                                            iconImage: ImageAssets.profile_2users,
                                            onTap: () {
                                              if(teacherRole == "6" || teacherRole == "1"){
                                                if(teacherRole == "1"){
                                                  BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradePriciple());
                                                }
                                                else if(teacherRole == "6"){
                                                  BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradeAffaire());
                                                }
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=> const ListClassInGradeScreen(routFromScreen: 2,)));
                                             }
                                             else{
                                              Navigator.pushNamed(context,
                                                  Routes.TEACHERLISTSCREEN);
                                             }
                                            },
                                            title: "LEDGERTEACHER".tr(),
                                            textStyleButton: ThemsConstands
                                                .headline_6_semibold_14
                                                .copyWith(
                                              color: Colorconstand.darkTextsRegular,
                                            ),
                                            buttonColor: Colorconstand.neutralWhite,
                                            radiusButton: 12,
                                            heightButton: 79,
                                            panddinHorButton: 16,
                                            panddingVerButton: 6,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  teacherRole == "1"?const SizedBox(height: 18): const SizedBox(),
                                   teacherRole == "1"?CardFeatureMore(
                                    iconImage: ImageAssets.ICON_COMMENT,
                                    onTap: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (context)=> const FeedbackScreen()));
                                    },
                                    title: "FEEDBACK_INBOX".tr(),
                                    textStyleButton: ThemsConstands
                                        .headline_6_semibold_14
                                        .copyWith(
                                      color: Colorconstand.darkTextsRegular,
                                    ),
                                    buttonColor: Colorconstand.neutralWhite,
                                    radiusButton: 12,
                                    heightButton: 79,
                                    panddinHorButton: 16,
                                    panddingVerButton: 6,
                                  ):Container(),
              
                            // ============== information school for principle ===================
                                  teacherRole == "1"?const SizedBox(height: 18): const SizedBox(),
                                  teacherRole == "1"?CardFeatureMore(
                                    iconImage: ImageAssets.ICON_CHARTS_GRAP,
                                    onTap: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (context)=> const SummarySchoolInfoScreen()));
                                    },
                                    title: "REPORT".tr()+"សង្ខេបក្នុងសាលា".tr(),
                                    textStyleButton: ThemsConstands
                                        .headline_6_semibold_14
                                        .copyWith(
                                      color: Colorconstand.darkTextsRegular,
                                    ),
                                    buttonColor: Colorconstand.neutralWhite,
                                    radiusButton: 12,
                                    heightButton: 79,
                                    panddinHorButton: 16,
                                    panddingVerButton: 6,
                                  ):Container(),
              
                            // ==============  End information school for principle ===================
              
              
                             // ================  list attendance teacher =================================
                                  teacherRole == "6" || teacherRole == "1"? const SizedBox():const SizedBox(height: 18),
                                  teacherRole == "6" || teacherRole == "1"? const SizedBox(): CardFeatureMore(
                                      iconImage: ImageAssets.calendar_tick_icon,
                                      onTap: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (context)=>  RequestAbsentScreen(nameTeacher: teachername,)));
                                      },
                                      title: "LIST_TEACHER_ABSENT".tr(),
                                      textStyleButton: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.darkTextsRegular ),
                                      buttonColor: Colorconstand.neutralWhite,
                                      radiusButton: 12,
                                      heightButton: 79,
                                      panddinHorButton: 16,
                                      panddingVerButton: 8,
                                  ),
                              // ================ End list attendance teacher =================================
              
              
                              // ================  Payment sevice =================================
                                  teacherRole == "6" || schoolIsUsePackage == true? const SizedBox(): SizedBox(height: showPaymentSevice == true?18:0),
                                  teacherRole == "6" || schoolIsUsePackage == true? const SizedBox(): AnimatedContainer(
                                    height: showPaymentSevice == false?0:80,
                                    duration:const Duration(milliseconds: 500),
                                     child: CardFeatureMore(
                                      iconImage: ImageAssets.card_tick_icon,
                                      onTap: () {
                                        Navigator.pushNamed(context, Routes.TEACHERFINANCES);
                                        // if (paid == true) {
                                        //   BlocProvider.of<PaymentOptionBloc>(context)
                                        //       .add(GetPaymentOptionEvent());
                                        //   Navigator.pushNamed(
                                        //       context, Routes.CURRENTPAYMENTSCREEN);
                                        // } else {
                                        //   BlocProvider.of<PaymentOptionBloc>(context)
                                        //       .add(GetPaymentOptionEvent());
                                        //   Navigator.pushNamed(
                                        //       context, Routes.PAYMENTOPTIONSCREEN);
                                        // }
                                      },
                                      title: "PAYMENT_SERVICE".tr(),
                                      textStyleButton: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.darkTextsRegular ),
                                      buttonColor: Colorconstand.neutralWhite,
                                      radiusButton: 12,
                                      heightButton: 79,
                                      panddinHorButton: 16,
                                      panddingVerButton: 8,
                                    ),
                                   ),
              
                                // ================  Payment sevice =================================
              
              
                                // ================   Open use app =================================
                                  Container(
                                    margin: EdgeInsets.only(top: (schoolIsUsePackage == true && widget.role !="3")? 18:0),
                                    child: AnimatedContainer(
                                      height: (schoolIsUsePackage == true && widget.role !="3")? 80:0,
                                      duration:const Duration(milliseconds: 500),
                                       child: CardFeatureMore(
                                        iconImage: ImageAssets.switch_icon,
                                        onTap: () {
                                          if(teacherRole == "6" || teacherRole == "1"){
                                              if(teacherRole == "1"){
                                                BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradePriciple());
                                              }
                                              else if(teacherRole == "6"){
                                                BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradeAffaire());
                                              }
                                              Navigator.push(context, MaterialPageRoute(builder: (context)=>  ListClassInGrade(isTeacher: false,)));
                                          }
                                          else{
                                            BlocProvider.of<GetClassBloc>(context).add(GetClass());
                                            Navigator.push(context, MaterialPageRoute(builder: (context)=>  ListClassInGrade(isTeacher: true,)));
                                          }
                                        },
                                        title: "UNLOCK_LOCK".tr(),
                                        textStyleButton: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.darkTextsRegular ),
                                        buttonColor: Colorconstand.neutralWhite,
                                        radiusButton: 12,
                                        heightButton: 79,
                                        panddinHorButton: 16,
                                        panddingVerButton: 8,
                                      ),
                                     ),
                                  ),
              
                                // ================   Open use app =================================
              
              
                                  const Divider(
                                    height: 35,
                                    thickness: 0.5,
                                  ),
              
              
                                  ButtonSettingWidget(
                                    iconImageright: ImageAssets.chevron_right_icon,
                                    iconImageleft: ImageAssets.folder_icon,
                                    onTap: () {
                                      if(teacherRole == "6"){
                                        Navigator.push(context, MaterialPageRoute(builder: (context)=> ReportForPricipleAndStudyAffair(role: "6",)));
                                      }
                                      else if(teacherRole == "1"){
                                         Navigator.push(context, MaterialPageRoute(builder: (context)=> ReportForPricipleAndStudyAffair(role: "1",)));
                                      }
                                      else{
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => const ReportScreenV2(),
                                          ),
                                        );
                                      }
                                    },
                                    title: "REPORT".tr(),
                                    textStyleButton: ThemsConstands
                                        .headline_6_semibold_14
                                        .copyWith(
                                      color: Colorconstand.darkTextsRegular,
                                    ),
                                    buttonColor: Colorconstand.neutralWhite,
                                    radiusButton: 8,
                                    panddinHorButton: 16,
                                    panddingVerButton: 10,
                                  ),
                                  const SizedBox(
                                    height: 16,
                                  ),
                                  teacherRole == "6"?ButtonSettingWidget(
                                    iconImageright: ImageAssets.chevron_right_icon,
                                    iconImageleft: ImageAssets.DOCUMENT_ICON,
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>  ListAttendanceTeacherScreen(principle: false,),
                                          ),
                                        );
                                    },
                                    title: "LIST_TEACHER_ABSENT".tr(),
                                    textStyleButton: ThemsConstands
                                        .headline_6_semibold_14
                                        .copyWith(
                                      color: Colorconstand.darkTextsRegular,
                                    ),
                                    buttonColor: Colorconstand.neutralWhite,
                                    radiusButton: 8,
                                    panddinHorButton: 16,
                                    panddingVerButton: 10,
                                  ):Container(height: 0,),
                                  SizedBox(
                                    height:teacherRole == "6"? 16:0,
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(bottom: 16),
                                    child: ButtonSettingWidget(
                                        iconImageright:
                                            ImageAssets.chevron_right_icon,
                                        iconImageleft: ImageAssets.phone_icon,
                                        onTap: (() {
                                              //BlocProvider<GetAllRequestBloc>(create: (context)=>GetAllRequestBloc(schoolarShipApi: GetSchoolarClass()));
                                          Navigator.push(context,MaterialPageRoute(builder: (context) => HelpScreen(isInstuctor: showPaymentSevice,)));
                                        }),
                                        title: "NEED_HELP".tr(),
                                        textStyleButton: ThemsConstands
                                            .headline_6_semibold_14
                                            .copyWith(
                                          color: Colorconstand.darkTextsRegular,
                                        ),
                                        buttonColor: Colorconstand.neutralWhite,
                                        radiusButton: 8,
                                        panddinHorButton: 16,
                                        panddingVerButton: 10),
                                  ),
                                  Container(
                                    child: ButtonSettingWidget(
                                      iconImageright:
                                          ImageAssets.chevron_right_icon,
                                      iconImageleft: ImageAssets.logoCamis,
                                      onTap: (() {
                                        Navigator.pushNamed(context, Routes.ABOUT);
                                      }),
                                      title: "ABOUT_CAMEMIS".tr(),
                                      textStyleButton: ThemsConstands
                                          .headline_6_semibold_14
                                          .copyWith(
                                        color: Colorconstand.darkTextsRegular,
                                      ),
                                      buttonColor: Colorconstand.neutralWhite,
                                      radiusButton: 8,
                                      panddinHorButton: 16,
                                      panddingVerButton: 10,
                                    ),
                                  ),
                                  
                                  const SizedBox(height: 35,),
                                  
                                  Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "${"VERSION".tr()} 2.5.0",
                                        style: ThemsConstands
                                            .headline_6_regular_14_20height
                                            .copyWith(
                                          color: Colorconstand.darkTextsPlaceholder,
                                        ),
                                        textAlign: TextAlign.center,
                                      )),
                                const SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          connection == true
              ? Container()
              : Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: Container(
                    color: const Color(0x7B9C9595),
                  ),
                ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }
}
