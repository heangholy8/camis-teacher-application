import 'dart:async';
import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../mixins/toast.dart';
import '../../../../service/api/isverify_api/update_isverify_api.dart';
import '../../../../storages/get_storage.dart';
import '../../../../storages/remove_storage.dart';
import '../../../../storages/user_storage.dart';
import '../../../account_confirm_screen/view/account_confirm_screen.dart';

class InputPhoneNumScreen extends StatefulWidget {
  const InputPhoneNumScreen({Key? key}) : super(key: key);
  static String verify = "";

  @override
  State<InputPhoneNumScreen> createState() => _InputPhoneNumScreenState();
}

class _InputPhoneNumScreenState extends State<InputPhoneNumScreen> with Toast {
  late TextEditingController phoneNumberController;
  UserSecureStroage saveStoragePref = UserSecureStroage();
  // final auth = FirebaseAuth.instance;
  final GlobalKey<FormState> formKey = GlobalKey();
  bool connection = true;
  StreamSubscription? sub;
  var checkFocusOTP = false;
  final RemoveStoragePref _removeStorage = RemoveStoragePref();
  @override
  void initState() {
    setState(() {
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
    });
    phoneNumberController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    phoneNumberController.dispose();
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colorconstand.primaryColor,
        body: WillPopScope(
          onWillPop:() => _removeStorage.removeToken(context),
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Positioned(
                  top: 0,
                  right: 0,
                  child: Image.asset("assets/images/Oval.png",width: MediaQuery.of(context).size.width / 1.7),
                ),
                Positioned(
                  top: 35,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.only(left: 28),
                              alignment: Alignment.centerLeft,
                              height: 45,
                              width: 45,
                              child: GestureDetector(
                                onTap: () {
                                  _removeStorage.removeToken(context);
                                },
                                child: const Icon(
                                  Icons.arrow_back_ios_new_rounded,
                                  size: 21,
                                  color: Colorconstand.neutralWhite,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.only(top: 25),
                          padding:  const EdgeInsets.only(top: 48, left: 27, right: 27),
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12.0),
                                  topRight: Radius.circular(12.0)),
                              color: Colorconstand.neutralWhite),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal:18.0),
                                child: Column(
                                  children: [
                                    Text(
                                      'ENTERPHONENUMBER'.tr(),
                                        style: ThemsConstands.headline_2_semibold_24.copyWith(
                                          color: Colorconstand.neutralDarkGrey,
                                          overflow: TextOverflow.fade,
                                        ),
                                    ),
                                    const SizedBox(
                                      height: 27,
                                    ),
                                    Text(
                                      'PLEASEENTERPHONE'.tr(),
                                      style: ThemsConstands.headline_4_medium_18.copyWith(
                                        color: Colorconstand.neutralDarkGrey,
                                        overflow: TextOverflow.fade,
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(top: 24, bottom: 30),
                                      decoration: BoxDecoration(
                                        color: Colorconstand.lightGohan,
                                        borderRadius: BorderRadius.circular(12),
                                        border: Border.all(
                                            color: checkFocusOTP == false
                                                ? Colorconstand.neutralGrey
                                                : Colorconstand.primaryColor,
                                            width: checkFocusOTP == false ? 1 : 2),
                                      ),
                                      child: Focus(
                                        onFocusChange: (hasfocus) {
                                          setState(() {
                                            hasfocus
                                                ? checkFocusOTP = true
                                                : checkFocusOTP = false;
                                          });
                                        },
                                        child: Form(
                                          key: formKey,
                                          child: TextFormField(
                                            controller: phoneNumberController,
                                            onChanged: (value) {
                                              setState(() {
                                                phoneNumberController.text;
                                              });
                                            },
                                            keyboardType: TextInputType.number,
                                            style: ThemsConstands.subtitle1_regular_16,
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(16.0)),
                                              contentPadding: const EdgeInsets.only(
                                                  top: 8, bottom: 5, left: 18, right: 18),
                                              labelText: 'PHONENUMBER'.tr(),
                                              labelStyle: ThemsConstands
                                                  .subtitle1_regular_16
                                                  .copyWith(
                                                      color: Colorconstand.lightTrunks),
                                              enabledBorder: const UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.transparent)),
                                              focusedBorder: const UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.transparent)),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    connection == false
                                        ? Container(height: 0)
                                        : MaterialButton(
                                              minWidth: double.maxFinite,
                                              onPressed: phoneNumberController.text.isNotEmpty
                                                  ? () async{
                                                    GetStoragePref _prefAuth = GetStoragePref();
                                                    var auth = await _prefAuth.getJsonToken;
                                                    var phoneNumToken = auth.authUser!.phone;
                                                    if(!phoneNumToken.startsWith("0")){
                                                      phoneNumToken = "0$phoneNumToken";
                                                      print("Add 0 beginning PhoneNum");
                                                    }
                                                    setState(() {
                                                      if(auth.authUser!.isVerify==1){
                                                        if(phoneNumberController.text == phoneNumToken){
                                                          UpdatingIsverifyApi() .updateIsverifyApi( phoneNumber: phoneNumberController.text) .then((value) => debugPrint("Checking Isverify Successfully $value"));
                                                          saveStoragePref.savePhoneNumber(keyPhoneNumber: phoneNumberController.text);
                                                          Navigator.pushAndRemoveUntil(context,
                                                            MaterialPageRoute(builder: (context) => const AccountConfirmScreen(),
                                                          ),(route) => false);
                                                          //Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOTPScreen(phone: phoneNumberController.text),));
                                                        }
                                                        else{
                                                          showMessage(
                                                            title:"PHONE_IS_NOT_CORRECT".tr(),
                                                            width:300.0,
                                                            hight:30.0, 
                                                            context: context);
                                                        }
                                                      }
                                                      else{
                                                        UpdatingIsverifyApi() .updateIsverifyApi( phoneNumber: phoneNumberController.text) .then((value) => debugPrint("Checking Isverify Successfully $value"));
                                                        saveStoragePref.savePhoneNumber(keyPhoneNumber: phoneNumberController.text);
                                                        Navigator.pushAndRemoveUntil(context,
                                                          MaterialPageRoute(builder: (context) => const AccountConfirmScreen(),
                                                        ),(route) => false);
                                                        //Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOTPScreen(phone: phoneNumberController.text),));
                                                      }
                                                    });
                                                  }: null,
                                              padding:
                                                  const EdgeInsets.symmetric(vertical: 12.0),
                                              disabledColor:
                                                  Colorconstand.mainColorUnderlayer,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(12.0)),
                                              color: Colorconstand.primaryColor,
                                              elevation: 0,
                                              child: Text(
                                                'CONTINUE'.tr(),
                                                style: ThemsConstands.headline_5_semibold_16
                                                    .copyWith(
                                                        color: Colorconstand.neutralWhite,
                                                        overflow: TextOverflow.fade),
                                              ),
                                            ),
                                  
                                  ],
                                ),
                              )
                        
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                AnimatedPositioned(
                  bottom: connection == true ? -150 : 0,
                  left: 0,
                  right: 0,
                  duration: const Duration(milliseconds: 500),
                  child: const NoConnectWidget(),
                ),
              ],
            ),
          ),
        ));
  }
}


/* () async {
  //010926316
  final phone ='+855 ${phoneNumberController.text.substring(1)}';
  await FirebaseAuth.instance.verifyPhoneNumber(
    phoneNumber: phone,
    verificationCompleted:(phoneAuthCredential) async {},
    verificationFailed: (error) {},
    codeSent: (verificationId,forceResendingToken) {InputPhoneNumScreen.verify =verificationId;Navigator.push(context,MaterialPageRoute(builder: (context) =>OTPVerifyScreen(),));},
    timeout: const Duration(seconds: 60),
    codeAutoRetrievalTimeout:(verificationId) {},
    );
                                      } */                                 