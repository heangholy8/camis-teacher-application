// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:async';
import 'package:camis_teacher_application/app/modules/account_confirm_screen/view/account_confirm_screen.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import '../../../../core/constands/color_constands.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../mixins/toast.dart';
import '../../../../service/api/isverify_api/update_isverify_api.dart';
import '../../../../storages/user_storage.dart';

class VerifyOTPScreen extends StatefulWidget {
  final phone;

  const VerifyOTPScreen({Key? key, required this.phone}) : super(key: key);

  @override
  State<VerifyOTPScreen> createState() => _VerifyOTPScreenState();
}

class _VerifyOTPScreenState extends State<VerifyOTPScreen> with Toast {
  UserSecureStroage saveStoragePref = UserSecureStroage();
  final defaulPinTheme = PinTheme(
      constraints: const BoxConstraints(minWidth: 46, minHeight: 56),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colorconstand.neutralWhite,
      ),
      margin: const EdgeInsets.all(2),
      textStyle: ThemsConstands.headline_4_medium_18 .copyWith(fontWeight: FontWeight.bold),
      padding: const EdgeInsets.only(left: 22, right: 22, top: 16, bottom: 12));

  final _formKey = GlobalKey<FormState>();
  // final FirebaseAuth auth = FirebaseAuth.instance;
  TextEditingController pinputController = TextEditingController();
  int? resentToken;
  late String _verificationId;
  // var phonauth = PhoneAuthCredential;
  int secondsRemaining = 30;
  Timer? timer;
  bool isChangeStatus =false;


  void startCountdownTimer() {
    secondsRemaining = 30;
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (secondsRemaining > 0) {
        setState(() {
          isChangeStatus = false;
          secondsRemaining--;
        });
      } else {
        stopCountdownTimer();
        setState(() {      
          isChangeStatus = true;
        });
      }
    });
  }

  void stopCountdownTimer() {
    timer?.cancel();
  }
  @override
  void initState() {
    phoneSignIn();
    super.initState();
    startCountdownTimer();
  }

  @override
  void dispose() {
    pinputController.dispose();
    super.dispose();
  }

  Future<void> phoneSignIn() async {
    final phone = '+855 ${widget.phone.substring(1)}';
    debugPrint("Phonenumber => $phone");
    // await auth.verifyPhoneNumber(
    //   phoneNumber: phone,
    //   timeout: const Duration(seconds: 30),
    //   verificationCompleted: (phoneAuthCredential) async {
    //     await auth.signInWithCredential(phoneAuthCredential).then((value) {
    //       if (value.user != null && timer!.isActive) {
    //         UpdatingIsverifyApi().updateIsverifyApi(phoneNumber: widget.phone).then((value) => debugPrint( "Checking Isverify Successfully $value"));
    //         saveStoragePref.savePhoneNumber(keyPhoneNumber: widget.phone);
    //         Navigator.pushAndRemoveUntil(context,
    //             MaterialPageRoute( 
    //               builder: (context) =>
    //                 const AccountConfirmScreen()),(route) => false);
    //       } else {
    //         showDialog(
    //           context: context,
    //           builder: (context) => const AlertDialog(
    //             title: Text("បរាជ័យ"),
    //             content: Text("សូមបញ្ចូលលេខកូដម្តងទៀត"),
    //           ),
    //         );
    //       }
    //     });
    //   },
    //   verificationFailed: (error) {
    //     const snackBar = SnackBar(
    //       content: Text('Verification Failed'),
    //       padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16.0),
    //     );
    //     ScaffoldMessenger.of(context).showSnackBar(snackBar);
    //     print("Get Error ${error.message.toString()}");
    //   },
    //   codeSent: (verificationId, forceResendingToken) {
    //     print("adfasdfasdfsadfsin $verificationId $forceResendingToken");
    //     setState(() {
    //       _verificationId = verificationId;
    //       resentToken = forceResendingToken;
    //     });
    //   },
    //   forceResendingToken: resentToken,
    //   codeAutoRetrievalTimeout: (verificationId) {
    //     setState(() {
    //       print("adfasdfasdf $verificationId");
    //       _verificationId = verificationId;
    //     });
    //   },
    // );
 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset(
              "assets/images/Oval.png",
              width: MediaQuery.of(context).size.width / 1.7,
            ),
          ),
          Positioned(
            top: 35,
            left: 0,
            right: 0,
            bottom: 0,
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(left: 28),
                        alignment: Alignment.centerLeft,
                        height: 45,
                        width: 45,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: const Icon(
                            Icons.arrow_back_ios_new_rounded,
                            size: 21,
                            color: Colorconstand.neutralWhite,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(top: 25),
                    padding:
                        const EdgeInsets.only(top: 48, left: 27, right: 27),
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                        color: Colorconstand.lightBeerusBeerus),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'ENTERPINCODE'.tr(),
                          style: ThemsConstands.headline_2_semibold_24.copyWith(
                            color: Colorconstand.neutralDarkGrey,
                            overflow: TextOverflow.fade,
                          ),
                        ),
                        const SizedBox(
                          height: 27,
                        ),
                        Text(
                          'INPUTPIN'.tr(),
                          textAlign: TextAlign.center,
                          style: ThemsConstands.headline4_regular_18.copyWith(
                            color: Colorconstand.neutralDarkGrey,
                            overflow: TextOverflow.fade,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 24, bottom: 16),
                          child: Form(
                            key: _formKey,
                            child: Pinput(
                              length: 6,
                              forceErrorState: true,
                              controller: pinputController,
                              keyboardType: TextInputType.number,
                              pinputAutovalidateMode:
                                  PinputAutovalidateMode.onSubmit,
                              defaultPinTheme: defaulPinTheme,
                              androidSmsAutofillMethod:
                                  AndroidSmsAutofillMethod.smsRetrieverApi,
                              autofocus: true,
                              smsCodeMatcher:
                                  PinputConstants.defaultSmsCodeMatcher,
                              listenForMultipleSmsOnAndroid: false,
                              useNativeKeyboard: true,
                              onChanged: (value) {
                                //0972257409
                                debugPrint("---> value ---> $value");
                              },
                              onCompleted: (value) async {
                                if (value == "111111") {
                                  UpdatingIsverifyApi() .updateIsverifyApi( phoneNumber: widget.phone) .then((value) => debugPrint(  "Checking Isverify Successfully $value"));
                                  saveStoragePref.savePhoneNumber(keyPhoneNumber: widget.phone,);
                                  Navigator.pushAndRemoveUntil(context,
                                    MaterialPageRoute(builder: (context) => const AccountConfirmScreen(),
                                  ),(route) => false);
                                } else {
                                  // try {
                                  //   final crediential = PhoneAuthProvider.credential( verificationId: _verificationId,smsCode: value);
                                  //   await auth.signInWithCredential(crediential).catchError((value) {
                                  //     showErrorDialog(() {
                                  //       Navigator.of(context).pop();
                                  //     }, context);
                                  //     pinputController.clear();
                                  //   }).then((value) {
                                  //     if (value.user != null) {
                                  //       UpdatingIsverifyApi() .updateIsverifyApi( phoneNumber: widget.phone) .then((value) => debugPrint(  "Checking Isverify Successfully $value"));
                                  //       saveStoragePref.savePhoneNumber(keyPhoneNumber: widget.phone);
                                  //       Navigator.pushAndRemoveUntil(context,
                                  //         MaterialPageRoute( builder: (context) => const AccountConfirmScreen()),
                                  //         (route) => false);
                                  //     }
                                  //   });
                                  // } catch (e) {
                                  //   FocusScope.of(context).unfocus();
                                  // }
                                }
                              },
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'NOTCODE'.tr(),
                              style:
                                  ThemsConstands.headline_5_medium_16.copyWith(
                                color: Colorconstand.neutralDarkGrey,
                                decoration: TextDecoration.none,
                                overflow: TextOverflow.visible,
                              ),
                            ),
                            const SizedBox(
                              width: 8.0,
                            ),
                            isChangeStatus==false ? Text("$secondsRemaining វិនាទី") : GestureDetector(
                              onTap: () {
                                phoneSignIn();
                                startCountdownTimer();
                                setState(() {
                                  var snackBar = SnackBar(
                                    content:Text('RESUBMITTED'.tr()),
                                    padding: const EdgeInsets.symmetric( horizontal: 16, vertical: 16.0),
                                  );
                                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                });
                              },
                              child: Text(
                              secondsRemaining<=0? 'RESENT'.tr() : "$secondsRemaining" ,
                              style: ThemsConstands.headline_5_medium_16
                                  .copyWith(
                                    color: Colorconstand.primaryColor,
                                    decoration: TextDecoration.underline,
                                    overflow: TextOverflow.visible,
                                    fontWeight: FontWeight.bold,
                                ),
                              ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
