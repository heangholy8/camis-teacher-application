// ignore_for_file: prefer_interpolation_to_compose_strings
import 'dart:async';
import 'dart:io';
import 'package:camis_teacher_application/app/modules/login_screen/views/scan_qr_screen.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/view/affaire_home_screen.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../widget/button_widget/button_icon_left_with_title.dart';
import '../../../../widget/button_widget/button_widget_custom.dart';
import '../../../../widget/expansion/expand_section.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../mixins/toast.dart';
import '../../../models/auth_model/list_school_model.dart';
import '../../../routes/app_route.dart';
import '../../../storages/key_storage.dart';
import '../../home_screen/view/home_screen.dart';
import '../../principle/principle_home_screen/view/principle_home_screen.dart';
import '../bloc/auth_bloc.dart';
import '../bloc/bloc/list_school_bloc.dart';
import '../widget/error_expaned_widget.dart';

class LoginEntryScreen extends StatefulWidget {
  const LoginEntryScreen({
    Key? key,
  }) : super(key: key);
  @override
  State<LoginEntryScreen> createState() => _LoginEntryScreenState();
}

class _LoginEntryScreenState extends State<LoginEntryScreen> with Toast {
  GetStoragePref getStoragePref = GetStoragePref();
  bool checkfocusschoolcode = false;
  bool checkfocususer = false;
  bool checkfocuspass = false;
  bool selectedSchool = false;
  int? isSeleted;
  String? schoolListName;
  bool passwordshow = true;
  bool loadingsumit = false;
  bool isExpanded = false;
  String schoolName = 'CHOOSESCHOOL'.tr();
  StreamSubscription? sub;
  bool connection = true;
  String schoolCode = "";
  String? phoneNumber;
  String? comfrimProfile;
  String?roleLogin;
  TextEditingController usercodeController = TextEditingController();
  TextEditingController passcodeController = TextEditingController();
  TextEditingController searchController = TextEditingController();
  int? isverify;
  
  Future checkCodeUser() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    if(dataaccess.authUser != null){
      setState(() {
        usercodeController.text = dataaccess.authUser!.code.toString();
        passcodeController.text = dataaccess.authUser!.code.toString();
      });
    }
  }

  void getPhoneStore() async {
    final KeyStoragePref keyPref = KeyStoragePref();
    final SharedPreferences pref = await SharedPreferences.getInstance();
      phoneNumber = pref.getString(keyPref.keyPhonePref) ?? "";
      comfrimProfile = pref.getString(keyPref.keyConfirmPref) ?? "";
  }

  List<Datum>? _foundData;

  FocusNode myfocus = FocusNode();

  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          connection = true;
        });
      }
    } on SocketException catch (_) {
      setState(() {
        connection = false;
      });
    }
  }

  @override
  void initState() {
    checkCodeUser();
    getPhoneStore();
    setState(() {
      checkUserConnection();
      //============= Check internet ====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
    });
    //=============Eend Check internet====================
    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      _foundData = BlocProvider.of<ListSchoolBloc>(context).preventData;
    });
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: GestureDetector(
        onTap: (){
           FocusScope.of(context).unfocus();
        },
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(
                "assets/images/Oval.png",
                width: MediaQuery.of(context).size.width / 1.7,
              ),
            ),
            Positioned(
              top: 150,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (1).png",
              ),
            ),
            Positioned(
              bottom: 35,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (3).png",
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height / 5,
              right: MediaQuery.of(context).size.width / 4,
              child: Center(
                child: Image.asset(
                  "assets/images/Oval (2).png",
                ),
              ),
            ),
            BlocListener<AuthBloc, AuthState>(
              listener: (context, state) {
                if (state is Loading) {
                  setState(() {
                    loadingsumit = true;
                  });
                } else if (state is Authenticated) {
                  var dataLogin = state.authModel.authUser;
                  setState(() {
                    roleLogin = dataLogin!.role.toString();
                    loadingsumit = false;
                    // if (isverify == 0) {
                    //    Navigator.pushNamed( context, Routes.VERIFICATIONSCREEN);
                    // } else {
                      // if (phoneNumber != "") {
                        if(comfrimProfile != "") {
                          if(roleLogin == "6"){
                            Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => const AffaireHomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),
                          (route) => false);
                          }
                          else if(roleLogin == "1"){
                            Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                                  pageBuilder: (context, animation1, animation2) =>
                                      const PrincipleHomeScreen(),
                                  transitionDuration: Duration.zero,
                                  reverseTransitionDuration: Duration.zero,
                                ),(route) => false);
                          }
                          else{
                            Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => const HomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),
                          (route) => false);
                          }
                        }
                        else{
                          Navigator.pushNamedAndRemoveUntil(context,Routes.COMFIRMATIONSCREEN,(Route<dynamic> route) => false);
                        }
                      // } 
                      // else {
                      //   Navigator.pushNamed(context, Routes.VERIFICATIONSCREEN);
                      // }
                    // }
                  });
                } else if (state is UnAUthenticated) {
                  setState(() {
                    loadingsumit = false;
                    showErrorDialog(() {
                      Navigator.of(context).pop();
                    }, context);
                  });
                }
              },
              child: Container(
                margin: const EdgeInsets.only(top: 70),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(ImageAssets.login_code_icon,
                            color: Colorconstand.neutralWhite,
                            width: 36,
                            height: 36),
                        Container(
                          margin: const EdgeInsets.only(
                            left: 18,
                          ),
                          child: Text(
                            'ENTERCODE'.tr().toUpperCase(),
                            style: ThemsConstands.headline_2_semibold_24
                                .copyWith(color: Colorconstand.neutralWhite),
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 28, left: 22, right: 22),
                      child: Text('SENTERCODELOGIN'.tr(),
                          style: ThemsConstands.subtitle1_regular_16
                              .copyWith(color: Colorconstand.neutralWhite),
                          textAlign: TextAlign.center),
                    ),
                    const SizedBox(height: 4),                  
                    Text(
                      "CAMEMISAPP".tr(),
                      style: ThemsConstands.headline_6_semibold_14.copyWith(
                        color: Colorconstand.neutralWhite),
                      textAlign: TextAlign.center,
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(
                          top: 23.0,
                        ),
                        decoration: const BoxDecoration(
                          color: Colorconstand.neutralSecondary,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12.0),
                            topRight: Radius.circular(12.0),
                          ),
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              bottom: 0,
                              top: 0,
                              left: 0,
                              right: 0,
                              child: SingleChildScrollView(
                                child: Container(
                                  padding: const EdgeInsets.only(
                                    left: 27,
                                    right: 27,
                                  ),
                                  child: Column(
                                    children: [
                                      const SizedBox(
                                        height: 40,
                                      ),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        width: MediaQuery.of(context).size.width,
                                        height: 48,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 18),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colorconstand.neutralGrey),
                                          color: Colors.white,
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(8)),
                                        ),
                                        child: Row(
                                          children: [
                                            AnimatedPadding(
                                              padding: EdgeInsets.only(
                                                  left: searchController
                                                          .text.isNotEmpty
                                                      ? 4
                                                      : 0),
                                              duration: const Duration(
                                                  milliseconds: 300),
                                              child:
                                                  searchController.text.isNotEmpty
                                                      ? SvgPicture.asset(
                                                          ImageAssets.school_icon,
                                                        )
                                                      : Container(),
                                            ),
                                            Expanded(
                                              child: Focus(
                                                onFocusChange: (value) {
                                                  setState(() {
                                                    isExpanded = value;
                                                  });
                                                },
                                                child: TextFormField(
                                                  scrollPhysics:
                                                      const BouncingScrollPhysics(),
                                                  controller: searchController,
                                                  style: ThemsConstands
                                                      .headline_5_medium_16,
                                                  focusNode: myfocus,
                                                  onChanged: (value) => _filter(
                                                      value,
                                                      BlocProvider.of<
                                                                  ListSchoolBloc>(
                                                              context)
                                                          .preventData!),
                                                  decoration: InputDecoration(
                                                    contentPadding:
                                                        const EdgeInsets
                                                                .symmetric(
                                                            vertical: 6.0,
                                                            horizontal: 8.0),
                                                    enabledBorder:
                                                        InputBorder.none,
                                                    focusedBorder:
                                                        InputBorder.none,
                                                    border:
                                                        const UnderlineInputBorder(),
                                                    hintText: 'CHOOSESCHOOL'.tr(),
                                                    hintStyle: ThemsConstands
                                                        .subtitle1_regular_16,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  isExpanded = !isExpanded;
                                                  if (isExpanded) {
                                                    myfocus.requestFocus();
                                                  } else {
                                                    myfocus.unfocus();
                                                  }
                                                });
                                              },
                                              child: SizedBox(
                                                width: 38,
                                                height: 38,
                                                child: !isExpanded
                                                    ? const Icon(
                                                        Icons.keyboard_arrow_down)
                                                    : const Icon(
                                                        Icons.keyboard_arrow_up,
                                                        color: Colorconstand
                                                            .darkBordersSecondaryButton,
                                                      ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      BlocBuilder<ListSchoolBloc,
                                          ListSchoolState>(
                                        builder: (context, state) {
                                          if (state is ListSchoolLoading) {
                                            return ErrorExpanedWidget(
                                              isExpaned: isExpanded,
                                              child:
                                                  const CircularProgressIndicator(),
                                            );
                                          } else if (state is ListSchoolLoaded) {
                                            return dropDownSchoolList(
                                                isEmptyString:searchController.text,
                                                translate: translate,
                                                selectedName: searchController.text,
                                                isExpaned: isExpanded,
                                                );
                                          }
                                          return ErrorExpanedWidget(
                                            isExpaned: isExpanded,
                                          );
                                        },
                                      ),
                                      Container(
                                        margin: const EdgeInsets.only(top: 16),
                                        padding: EdgeInsets.zero,
                                        height: 48,
                                        decoration: BoxDecoration(
                                          color: Colorconstand.lightGohan,
                                          borderRadius: BorderRadius.circular(8),
                                          border: Border.all(
                                              color: checkfocususer == false
                                                  ? Colorconstand.neutralGrey
                                                  : Colorconstand.primaryColor,
                                              width: checkfocususer == false
                                                  ? 1
                                                  : 2),
                                        ),
                                        child: Focus(
                                          onFocusChange: (hasfocus) {
                                            setState(() {
                                              hasfocus
                                                  ? checkfocususer = true
                                                  : checkfocususer = false;
                                            });
                                          },
                                          child: TextFormField(
                                            controller: usercodeController,
                                            onChanged: ((value) {
                                              setState(() {
                                                usercodeController.text;
                                              });
                                            }),
                                            style: ThemsConstands
                                                .headline_5_medium_16
                                                .copyWith(color: Colors.black),
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          16.0)),
                                              contentPadding:
                                                  const EdgeInsets.only(
                                                      top: 4,
                                                      bottom: 5,
                                                      left: 18,
                                                      right: 18),
                                              labelText: 'USERCODE'.tr(),
                                              labelStyle: ThemsConstands
                                                  .subtitle1_regular_16
                                                  .copyWith(
                                                      color: Colorconstand
                                                          .lightTrunks),
                                              enabledBorder:
                                                  const UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Colors
                                                              .transparent)),
                                              focusedBorder:
                                                  const UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Colors
                                                              .transparent)),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.only(
                                            top: 16, bottom: 24),
                                        padding: EdgeInsets.zero,
                                        decoration: BoxDecoration(
                                          color: Colorconstand.lightGohan,
                                          borderRadius: BorderRadius.circular(12),
                                          border: Border.all(
                                              color: checkfocuspass == false
                                                  ? Colorconstand.neutralGrey
                                                  : Colorconstand.primaryColor,
                                              width: checkfocuspass == false
                                                  ? 1
                                                  : 2),
                                        ),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Focus(
                                                onFocusChange: (hasfocus) {
                                                  setState(() {
                                                    hasfocus
                                                        ? checkfocuspass = true
                                                        : checkfocuspass = false;
                                                  });
                                                },
                                                child: TextFormField(
                                                  obscuringCharacter: "*",
                                                  controller: passcodeController,
                                                  onChanged: ((value) {
                                                    setState(() {
                                                      passcodeController.text;
                                                    });
                                                  }),
                                                  obscureText: passwordshow,
                                                  style: ThemsConstands
                                                      .headline_5_medium_16,
                                                  scrollPadding:
                                                      const EdgeInsets.all(0),
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              16.0),
                                                    ),
                                                    contentPadding:
                                                        const EdgeInsets.only(
                                                      top: 8,
                                                      bottom: 5,
                                                      left: 18,
                                                      right: 18,
                                                    ),
                                                    labelText: 'PASSWORD'.tr(),
                                                    labelStyle: ThemsConstands
                                                        .subtitle1_regular_16
                                                        .copyWith(
                                                            color: Colorconstand
                                                                .lightTrunks),
                                                    enabledBorder:
                                                        const UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Colors.transparent),
                                                    ),
                                                    focusedBorder:
                                                        const UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Colors.transparent),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  if (passwordshow) {
                                                    passwordshow = false;
                                                  } else {
                                                    passwordshow = true;
                                                  }
                                                });
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 18.0),
                                                child: SvgPicture.asset(
                                                  passwordshow == true
                                                      ? ImageAssets.eye_icon
                                                      : ImageAssets.eye_off_icon,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      ButtonWidgetCustom(
                                        buttonColor: Colorconstand.primaryColor,
                                        onTap: searchController.text == "" ||
                                                usercodeController.text == "" ||
                                                passcodeController.text == "" ||
                                                connection == false
                                            ? null
                                            : () {
                                                BlocProvider.of<AuthBloc>(context).add(
                                                  SignInRequestedEvent(
                                                    schoolCode: BlocProvider.of<ListSchoolBloc>(context).schoolId.toString(),
                                                    loginName: usercodeController.text.toString(),
                                                    password: passcodeController.text.toString(),
                                                  ),
                                                );
                                              },
                                        panddinHorButton: 22,
                                        panddingVerButton: 12,
                                        radiusButton: 8,
                                        textStyleButton: ThemsConstands.button_semibold_16.copyWith(
                                          color: Colorconstand.neutralWhite,
                                        ),
                                        title: 'CONTINUE'.tr(),
                                      ),
                                      Container(
                                        height: 160,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
               
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height - 160,
              left: 0,
              right: 0,
              child: Container(
                height: 160,
                color: Colorconstand.neutralSecondary,
                padding: const EdgeInsets.symmetric(horizontal: 28),
                child: Column(
                  children: [
                    const Divider(
                      height: 1,
                      color: Colorconstand.neutralGrey,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      child: Text(
                        'DONTQRCODE'.tr(),
                        style: ThemsConstands.headline_5_semibold_16
                            .copyWith(color: Colorconstand.neutralDarkGrey),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 15),
                      color: Colorconstand.neutralSecondary,
                      child: ButtonIconLeftWithTitle(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const QrCodeLoginScreen(),
                            ),
                          );
                          // BlocProvider.of<AuthBloc>(context).add(
                          //   SignInRequestedEvent(
                          //     schoolCode: BlocProvider.of<ListSchoolBloc>(context)
                          //         .schoolId
                          //         .toString(),
                          //     loginName: usercodeController.text.toString(),
                          //     password: passcodeController.text.toString(),
                          //   ),
                          // );
                        },
                        buttonColor: Colorconstand.neutralWhite,
                        iconImage: ImageAssets.scan_qr_icon,
                        weightButton: MediaQuery.of(context).size.width,
                        panddinHorButton: 28,
                        panddingVerButton: 12,
                        radiusButton: 8,
                        speaceTitleAndImage: 16,
                        textStyleButton: ThemsConstands.button_semibold_16
                            .copyWith(color: Colorconstand.mainColorSecondary),
                        title: 'SCANQR'.tr(),
                        iconColor: Colorconstand.mainColorSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            loadingsumit == false
                ? Container()
                : Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      color: Colorconstand.subject13.withOpacity(0.6),
                      child: const Center(
                        child: CircularProgressIndicator(
                          color: Colorconstand.neutralWhite,
                        ),
                      ),
                    )),
            connection == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget dropDownSchoolList(
      {required String translate,
      required String selectedName,
      String isEmptyString = "",
      bool isExpaned = false}) {
    return ExpandedSection(
      expand: isExpanded,
      child: _foundData != null || _foundData != 0
          ? ListView.separated(
              itemCount: _foundData!.length,
              separatorBuilder: ((context, index) {
                return const Divider(
                  height: 1,
                );
              }),
              physics: const ScrollPhysics(),
              padding: const EdgeInsets.all(0),
              shrinkWrap: true,
              itemBuilder: ((context, index) {
                return Container(
                  height: 44,
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(19)),
                  child: MaterialButton(
                    height: 40,
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: index == 0
                            ? const Radius.circular(8.0)
                            : const Radius.circular(0),
                        topRight: index == 0
                            ? const Radius.circular(8.0)
                            : const Radius.circular(0),
                        bottomLeft: index == _foundData!.length - 1
                            ? const Radius.circular(8.0)
                            : const Radius.circular(0),
                        bottomRight: index == _foundData!.length - 1
                            ? const Radius.circular(8.0)
                            : const Radius.circular(0),
                      ),
                    ),
                    color: isSeleted == index
                        ? Colorconstand.primaryColor
                        : Colors.white,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                    onPressed: () {
                      myfocus.unfocus();
                      setState(() {
                        isSeleted = index;
                        print("flkasdfka${_foundData?[isSeleted!].id}");
                        BlocProvider.of<ListSchoolBloc>(context).schoolId =
                            _foundData?[isSeleted!].id;

                        if (translate == "km") {
                          schoolListName = _foundData?[isSeleted!].schoolName;
                        } else if (translate == "en") {
                          schoolListName = _foundData?[isSeleted!].schoolNameEn;
                        } else {
                          schoolListName = _foundData?[isSeleted!].schoolNameEn;
                        }
                        searchController.text = schoolListName!;

                        print("sinatSchool" + searchController.text.toString());
                        isExpanded = false;
                      });
                    },
                    child: Text(
                      translate == "km"
                          ? _foundData![index].schoolName.toString()
                          : _foundData![index].schoolNameEn.toString(),
                      style: ThemsConstands.subtitle1_regular_16.copyWith(
                          color: isSeleted == index
                              ? Colorconstand.neutralWhite
                              : Colors.black),
                    ),
                  ),
                );
              }),
            )
          : Container(),
    );
  }

  void _filter(String query, List<Datum> allResult) {
    List<Datum> results = [];
    if (query.isEmpty) {
      results = allResult;
    } else {
      results = allResult.where((element) =>
              element.schoolName!.toLowerCase().contains(query.toLowerCase()) ||
              element.schoolNameEn!.toLowerCase().contains(query.toLowerCase()))
          .toList();
    }
    setState(() {
      _foundData = results;
    });
  }
}
