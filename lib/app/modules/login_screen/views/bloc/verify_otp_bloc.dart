import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'verify_otp_event.dart';
part 'verify_otp_state.dart';

class VerifyOtpBloc extends Bloc<VerifyOtpEvent, VerifyOtpState> {
  VerifyOtpBloc() : super(VerifyOtpInitial()) {
    on<UpdateIsverifyPhoneNumEvent>((event, emit) {
      emit(CheckingPhoneNumLoading());
      try {
        
      } catch (e) {
        
      }
    });
  }
}
