part of 'verify_otp_bloc.dart';

abstract class VerifyOtpState extends Equatable {
  const VerifyOtpState();

  @override
  List<Object> get props => [];
}

class VerifyOtpInitial extends VerifyOtpState {}

class CheckingPhoneNumLoading extends VerifyOtpState {}

class CheckingPhoneNumSuccess extends VerifyOtpState {}

class CheckingPhoneNumError extends VerifyOtpState {
  final String? error;
  const CheckingPhoneNumError({this.error});
}
