part of 'verify_otp_bloc.dart';

abstract class VerifyOtpEvent extends Equatable {
  const VerifyOtpEvent();

  @override
  List<Object> get props => [];
}

class UpdateIsverifyPhoneNumEvent extends VerifyOtpEvent {
  final String phoneNumber;
  const UpdateIsverifyPhoneNumEvent({required this.phoneNumber});
}
