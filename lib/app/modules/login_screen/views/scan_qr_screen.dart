// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
// import 'package:scan/scan.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../widget/button_widget/button_icon_left_with_title.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/scan_view_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../help/qrcode_split.dart';
import '../../../help/request_camera_permission.dart';
import '../../../mixins/toast.dart';
import '../../../routes/app_route.dart';
import '../../../storages/key_storage.dart';
import '../../home_screen/view/home_screen.dart';
import '../../principle/principle_home_screen/view/principle_home_screen.dart';
import '../../study_affaire_screen/affaire_home_screen/view/affaire_home_screen.dart';
import '../bloc/auth_bloc.dart';

class QrCodeLoginScreen extends StatefulWidget {
  const QrCodeLoginScreen({Key? key}) : super(key: key);

  @override
  State<QrCodeLoginScreen> createState() => _QrCodeLoginScreenState();
}

class _QrCodeLoginScreenState extends State<QrCodeLoginScreen> with Toast {
  late MobileScannerController _mobileScannerController;
  GetStoragePref getStoragePref = GetStoragePref();
  bool _isFlash = false;
  StreamSubscription? stsub;
  bool connection = true;
  bool loadingsumit = false;
  String? phoneNumber;
  String? roleLogin;
  String? comfrimProfile;

  final ImagePicker _picker = ImagePicker();
  File? _image;
  XFile? file;

  late Timer scanControl;

  double scanScrool = 0; 

  void getPhoneStore() async {
    final KeyStoragePref keyPref = KeyStoragePref();
    final SharedPreferences pref = await SharedPreferences.getInstance();
      phoneNumber = pref.getString(keyPref.keyPhonePref) ?? "";
      comfrimProfile = pref.getString(keyPref.keyConfirmPref) ?? "";
  }

  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          connection = true;
        });
      }
    } on SocketException catch (_) {
      setState(() {
        connection = false;
      });
    }
  }

  @override
  initState() {
    super.initState();
    getPhoneStore();
    checkUserConnection();
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
    _mobileScannerController = MobileScannerController();

    requestCameraPermission();
  }

  @override
  void dispose() {
    _mobileScannerController.dispose();
    stsub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

 return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: BlocListener<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state is Loading) {
              setState(() {
                _mobileScannerController.stop();
                loadingsumit = true;
              });
            } else if (state is Authenticated) {
              var dataLogin = state.authModel.authUser;
                  setState(() async{
                    roleLogin = dataLogin!.role.toString();
                loadingsumit = false;
                GetStoragePref _prefAuth = GetStoragePref();
                  var auth = await _prefAuth.getJsonToken;
                setState(() {
                  // if (auth.authUser!.isVerify == 0) {
                  //   Navigator.pushNamedAndRemoveUntil(
                  //       context,
                  //       Routes.VERIFICATIONSCREEN,
                  //       (Route<dynamic> route) => false);
                  // } else {
                  //   if(phoneNumber!=""){
                      if (comfrimProfile != "") {
                        if(roleLogin == "6"){
                          Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) =>
                                    const AffaireHomeScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                              (route) => false);
                        }
                        else if(roleLogin == "1"){
                          Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) =>
                                    const PrincipleHomeScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),(route) => false);
                        }
                        else{
                          Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) =>
                                    const HomeScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                              (route) => false);
                        }
                      } else {
                        Navigator.pushNamedAndRemoveUntil(
                            context,
                            Routes.COMFIRMATIONSCREEN,
                            (Route<dynamic> route) => false);
                      }
                  //   }
                  //   else{
                  //     Navigator.pushNamedAndRemoveUntil(context, Routes.VERIFICATIONSCREEN,
                  //       (Route<dynamic> route) => false);
                  //   }
                  // }
                });
              });
              HapticFeedback.vibrate();
            } else if (state is UnAUthenticated) {
              setState(() {
                _mobileScannerController.stop();
                loadingsumit = false;
                showErrorDialog(() {
                  _mobileScannerController.start();
                  Navigator.of(context).pop();
                }, context);
              });
            } else {
              setState(() {
                _mobileScannerController.stop();
                loadingsumit = false;
                showErrorDialog(() {
                  _mobileScannerController.start();
                  Navigator.of(context).pop();
                }, context);
              });
            }
          },
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                top: 0,
                child: ScanViewWidget(
                  mobileScannerController: _mobileScannerController,
                    onDetect: (barcode) async {
                      if(connection==false){
                      }
                      else if (barcode.barcodes.first.rawValue != null) {
                        final String code =barcode.barcodes.first.rawValue!;
                        qrCodeSplit(code, context);
                      } else {
                        debugPrint('Failed to scan Barcode');
                      }
                    },
                ),
              ),
              _image!=null?Center(
                  child: Container(
                    alignment: Alignment.center,
                    width: 210,height: 210,
                    child: Stack(
                      children: [
                        ClipRRect(borderRadius: BorderRadius.circular(12.0),child: Image.file(_image!,fit: BoxFit.cover,width: double.infinity,height: double.infinity,)),
                        AnimatedPositioned(
                          left: 0,right: 0,top: scanScrool,
                          duration:const Duration(milliseconds:10),
                          child: Container(
                            height: 5,
                            decoration: BoxDecoration(
                              color: Colorconstand.primaryColor,
                              boxShadow: [
                                BoxShadow(
                                  color: Colorconstand.primaryColor.withOpacity(0.5),
                                  spreadRadius: 10,
                                  blurRadius: 15,
                                  offset:const Offset(0, 0), // changes position of shadow
                                ),
                              ],
                            ),
                          ), 
                        )
                      ],
                    ),
                  ),
                ):Container(height: 0,),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                        color: const Color(0x891855AB),
                      ),
                    ),
                    Container(
                      height: 230,
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              color: const Color(0x891855AB),
                            ),
                          ),
                          Container(
                            width: 230,
                            decoration: BoxDecoration(
                                border: Border.all(
                              color: const Color(0x891855AB),
                            )),
                            child: Stack(
                              children: [
                                //============= top Rigeht radius ========================
                                Positioned(
                                  top: -57.3,
                                  left: -57.3,
                                  child: SvgPicture.asset(
                                    "assets/images/1.svg",
                                    color: const Color(0x891855AB),
                                    width: 480,
                                    height: 480,
                                  ),
                                ),
                                Positioned(
                                  top: -1,
                                  left: -1,
                                  child: SvgPicture.asset(
                                    "assets/images/1.svg",
                                    width: 60,
                                    height: 60,
                                  ),
                                ),
                                Positioned(
                                  top: -0.1,
                                  left: -0.1,
                                  child:
                                      SvgPicture.asset("assets/images/1.svg"),
                                ),
                                //============= top Rigeht radius ========================
                                Positioned(
                                  top: -53.3,
                                  right: -53.3,
                                  child: SvgPicture.asset(
                                    "assets/images/2.svg",
                                    color: const Color(0x891855AB),
                                    width: 480,
                                    height: 480,
                                  ),
                                ),
                                Positioned(
                                  top: -1,
                                  right: -1,
                                  child: SvgPicture.asset(
                                    "assets/images/2.svg",
                                    width: 65,
                                    height: 65,
                                  ),
                                ),
                                Positioned(
                                  top: -0.1,
                                  right: -0.1,
                                  child:
                                      SvgPicture.asset("assets/images/2.svg"),
                                ),
                                //============= Buttom Rigeht radius ========================
                                Positioned(
                                  bottom: -58.7,
                                  right: -58.7,
                                  child: SvgPicture.asset(
                                    "assets/images/4.svg",
                                    color: const Color(0x891855AB),
                                    width: 480,
                                    height: 480,
                                  ),
                                ),
                                Positioned(
                                  bottom: -1,
                                  right: -1,
                                  child: SvgPicture.asset(
                                    "assets/images/4.svg",
                                    width: 60,
                                    height: 60,
                                  ),
                                ),
                                Positioned(
                                  bottom: -0.1,
                                  right: -0.1,
                                  child:
                                      SvgPicture.asset("assets/images/4.svg"),
                                ),

                                //============= Buttom Left radius ========================
                                Positioned(
                                  bottom: -52.5,
                                  left: -52.5,
                                  child: SvgPicture.asset(
                                    "assets/images/3.svg",
                                    color: const Color(0x891855AB),
                                    width: 480,
                                    height: 480,
                                  ),
                                ),
                                Positioned(
                                  bottom: -1,
                                  left: -1,
                                  child: SvgPicture.asset(
                                    "assets/images/3.svg",
                                    width: 67,
                                    height: 67,
                                  ),
                                ),
                                Positioned(
                                  bottom: -0.1,
                                  left: -0.1,
                                  child:
                                      SvgPicture.asset("assets/images/3.svg"),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              color: const Color(0x891855AB),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        color: const Color(0x891855AB),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 60 * 2,
                left: 0,
                right: 0,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          child: SvgPicture.asset(
                            ImageAssets.scanner_icon,
                            color: Colorconstand.neutralWhite,
                            width: 36,
                            height: 36,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                            left: 18,
                          ),
                          child: Text(
                            "SSCANQRCODE".tr(),
                            style: ThemsConstands.headline_2_semibold_24
                                .copyWith(color: Colorconstand.neutralWhite),
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin:
                          const EdgeInsets.only(top: 28, left: 22, right: 22),
                      child: Text("SSCANORLOGIN".tr(),
                          style: ThemsConstands.subtitle1_regular_16
                              .copyWith(color: Colorconstand.neutralWhite),
                          textAlign: TextAlign.center),
                    ),
                    const SizedBox(height: 14,),
                    Text(
                      "CAMEMISAPP".tr(),
                      style: ThemsConstands.headline_6_semibold_14.copyWith(
                          color: Colorconstand.neutralWhite,),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                top: 300,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: InkWell(
                        onTap: () async {
                          await _mobileScannerController.toggleTorch();
                          setState(() {
                            _isFlash = !_isFlash;
                          });
                        },
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colorconstand.primaryColor,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: !_isFlash
                                ? SvgPicture.asset(ImageAssets.flash_icon)
                                : SvgPicture.asset(
                                    ImageAssets.flash_off_icon,
                                    color: Colors.white,
                                  ),
                          ),
                        ),
                      ),
                    ),
                    // const SizedBox(
                    //     width: 50,
                    //   ),
                    //   Center(
                    //     child: InkWell(
                    //       onTap: (){
                    //         _imgFromGallery();
                    //       },
                    //       child: Container(
                    //         width: 50,
                    //         height: 50,
                    //         decoration: BoxDecoration(
                    //           color: Colorconstand.primaryColor.withOpacity(0.5),
                    //           borderRadius: BorderRadius.circular(8),
                    //         ),
                    //         child: const Padding(
                    //           padding: EdgeInsets.all(8.0),
                    //           child: Icon(Icons.qr_code_2_outlined,color: Colors.white,size: 34,),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                  ],
                ),
              ),
              // Positioned(
              //   bottom: 0,
              //   left: 0,
              //   right: 0,
              //   top: 440,
              //   child: Center(
              //     child: Text(
              //       "FLASH".tr(),
              //       style: ThemsConstands.headline6_medium_14
              //           .copyWith(color: Colors.white),
              //     ),
              //   ),
              // ),
              Positioned(
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                  padding:
                      const EdgeInsets.only(bottom: 35, left: 27, right: 27),
                  decoration: const BoxDecoration(
                      color: Colorconstand.neutralSecondary,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12),
                          topRight: Radius.circular(12))),
                  child: Column(
                    children: [
                      const Divider(
                        height: 1,
                        color: Colorconstand.neutralGrey,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20),
                        child: Text(
                          "DONTHAVEQR".tr(),
                          style: ThemsConstands.headline_5_semibold_16
                              .copyWith(color: Colorconstand.neutralDarkGrey),
                        ), 
                      ),
                      Container(
                          margin: const EdgeInsets.only(top: 15),
                          color: Colorconstand.neutralSecondary,
                          child: ButtonIconLeftWithTitle(
                            onTap: () {
                              Navigator.pushReplacementNamed(context, Routes.LOGINENTRYSCREEN);
                            },
                            buttonColor: Colorconstand.neutralWhite,
                            iconImage: ImageAssets.login_code_icon,
                            weightButton: MediaQuery.of(context).size.width,
                            panddinHorButton: 28,
                            panddingVerButton: 12,
                            radiusButton: 8,
                            speaceTitleAndImage: 16,
                            textStyleButton: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.mainColorSecondary),
                            title: 'ENTERCODE'.tr(),
                            iconColor: Colorconstand.mainColorSecondary,
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              loadingsumit == false
                  ? Container()
                  : Positioned(
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: 0,
                      child: Container(
                        color: Colorconstand.subject13.withOpacity(0.6),
                        child: const Center(
                          child: CircularProgressIndicator(
                            color: Colorconstand.neutralWhite,
                          ),
                        ),
                      ),
                    ),
              connection == true
                  ? Container()
                  : Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      top: 0,
                      child: Container(
                        color: const Color(0x7B9C9595),
                      ),
                    ),
              AnimatedPositioned(
                bottom: connection == true ? -150 : 0,
                left: 0,
                right: 0,
                duration: const Duration(milliseconds: 500),
                child: const NoConnectWidget(),
              ),
            ],
          ),
        ),
      ),
    );
  }
  // void _imgFromGallery() async {
  //   XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
  //     setState(() {
  //       scanScrool = 0;
  //       _image = File(pickedFile!.path);
  //       file = pickedFile;
  //     });
  //     scanControl = Timer.periodic(const Duration(milliseconds: 15), (Timer t) {
  //        if(scanScrool < 210){
  //         setState(() {
  //           scanScrool = scanScrool+2;
  //         });
  //        }
  //        else{
  //         setState(() {
  //           scanScrool = 0;
  //         });
  //        }
  //     });
  //     String? str = await Scan.parse(pickedFile!.path).then((value){
  //     if(value == null){
  //       Future.delayed(const Duration(seconds:2),(){
  //         setState(() {
  //           scanControl.cancel();
  //         });
  //         showDialogFailImage(() {
  //           setState(() {
  //             _image = null;
  //             scanControl.cancel();
  //           });
  //             Navigator.of(context).pop();
  //           },context);
  //       });
  //     }
  //     else{
  //         Future.delayed(const Duration(seconds:2),(){
  //           qrCodeSplit(value.toString(), context);
  //           setState(() {
  //             _image = null;
  //             scanControl.cancel();
  //           });
  //         });
  //     }
  //     return null;
  //   });
  // }
}
