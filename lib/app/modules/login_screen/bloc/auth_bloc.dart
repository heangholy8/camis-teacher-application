import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../help/check_platform_device.dart';
import '../../../models/auth_model/auth_model.dart';
import '../../../service/api/auth_api/auth_api.dart';
import '../../../storages/user_storage.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthApi authApi;
  UserSecureStroage _prefs = UserSecureStroage();
  AuthBloc({required this.authApi}) : super(UnAUthenticated()) {
    
    on<SignInRequestedEvent>((event, emit) async {
      emit(Loading());
      try {
        var platform = checkPlatformDevice();
        await authApi.signInRequestApi(
          schoolCode: event.schoolCode,
          loginName: event.loginName,
          password: event.password,
          platform: platform,
        ).then((value) {
          _prefs.setToken(token: json.encode(value));
          emit(Authenticated(authModel: value));
        });
      } catch (e) {
        emit(
          AuthError(e.toString()),
        );
        emit(UnAUthenticated());
      }
    });
  }
}
