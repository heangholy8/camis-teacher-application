import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../../../models/auth_model/list_school_model.dart';
import '../../../../service/api/auth_api/get_list_school.dart';

part 'list_school_event.dart';
part 'list_school_state.dart';

class ListSchoolBloc extends Bloc<ListSchoolEvent, ListSchoolState> {
  final GetListSchoolApi getListSchool;
  List<Datum>? preventData;
  int? schoolId;
  ListSchoolBloc({required this.getListSchool}) : super(ListSchoolInitial()) {
    on<GetListSchoolEvent>((event, emit) async {
      emit(ListSchoolLoading());
      try {
        var data = await getListSchool.getListSchoolRequestApi();
        emit(ListSchoolLoaded(listSchool: data));
        preventData = data.data;
        debugPrint("dateRepose ${data.toString()}");
      } catch (e) {
        emit(ListSchoolError());
        preventData = [
          Datum(
              id: 0,
              schoolName: "មិនមានសាលា",
              schoolNameEn: "No School",
              isActive: 1),
        ];
      }
    });
  }
}
