part of 'list_school_bloc.dart';

abstract class ListSchoolEvent extends Equatable {
  const ListSchoolEvent();

  @override
  List<Object> get props => [];
}
class GetListSchoolEvent extends ListSchoolEvent {}

