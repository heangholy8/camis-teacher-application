import 'dart:async';
import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/login_screen/views/scan_qr_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../widget/button_widget/button_icon_left_with_title.dart';
import 'bloc/bloc/list_school_bloc.dart';
import 'views/code_entry_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key,}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // Future<String> initializeFlutterFire() async {
  //   try {
  //     await Firebase.initializeApp();

  //     deviceToken = await FirebaseMessaging.instance.getToken();
  //     if (deviceToken != null) {
  //       return deviceToken;
  //     }
  //     return "";
  //   } catch (e) {
  //     print("################error:$e");
  //     return "";
  //   }
  // }

  @override
  void initState() {
    //initializeFlutterFire();
    // setState(() {
    //   //=============Check internet====================
    //   sub = Connectivity().onConnectivityChanged.listen((event) {
    //     setState(() {
    //       widget.connection = (event != ConnectivityResult.none);
    //     });
    //   });
    // });
    //=============End Check internet====================
    BlocProvider.of<ListSchoolBloc>(context).add(GetListSchoolEvent());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(
                "assets/images/Oval.png",
                width: MediaQuery.of(context).size.width / 1.7,
              ),
            ),
            Positioned(
              top: 150,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (1).png",
              ),
            ),
            Positioned(
              bottom: 35,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (3).png",
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height / 5,
              right: MediaQuery.of(context).size.width / 4,
              child: Center(
                  child: Image.asset(
                "assets/images/Oval (2).png",
              )),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 75),
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        "Powered by:",
                        style: ThemsConstands.caption_regular_12
                            .copyWith(color: Colors.transparent),
                      ),
                      Text(
                        "CAMIS Solutions",
                        style: ThemsConstands.caption_regular_12
                            .copyWith(color: Colors.transparent),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        ImageAssets.logoCamis,
                        width: 45,
                        height: 45,
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                          top: 0,
                          left: 15,
                          bottom: 60,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              "CAMEMIS",
                              style: ThemsConstands.headline_1_semibold_32
                                  .copyWith(
                                      color: Colorconstand.neutralWhite,
                                      fontSize: 40),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Text(
                                "RECOMTEACHER".tr(),
                                style:
                                    ThemsConstands.caption_regular_12.copyWith(
                                  color: Colorconstand.neutralWhite,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        ButtonIconLeftWithTitle(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => QrCodeLoginScreen(),
                              ),
                            );
                          },
                          buttonColor: Colorconstand.mainColorSecondary,
                          iconImage: ImageAssets.scan_qr_icon,
                          weightButton: MediaQuery.of(context).size.width,
                          panddinHorButton: 28,
                          panddingVerButton: 12,
                          radiusButton: 8,
                          speaceTitleAndImage: 16,
                          textStyleButton: ThemsConstands.button_semibold_16
                              .copyWith(color: Colorconstand.neutralWhite),
                          title: 'SCANQR'.tr(),
                          iconColor: Colorconstand.neutralWhite,
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        ButtonIconLeftWithTitle(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const LoginEntryScreen(),
                              ),
                            );
                          },
                          buttonColor: Colorconstand.neutralWhite,
                          iconImage: ImageAssets.login_code_icon,
                          weightButton: MediaQuery.of(context).size.width,
                          panddinHorButton: 28,
                          panddingVerButton: 12,
                          radiusButton: 8,
                          speaceTitleAndImage: 16,
                          textStyleButton: ThemsConstands.button_semibold_16
                              .copyWith(color: Colorconstand.primaryColor),
                          title: 'ENTERCODE'.tr(),
                          iconColor: Colorconstand.primaryColor,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
