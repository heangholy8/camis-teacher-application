
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';

class HeadTitleBig extends StatelessWidget {
  final String title;
  final double? top;
  const HeadTitleBig({Key? key, required this.title, this.top = 0.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.only(left: 17.0, bottom: 16.92, top: top!),
      child: Text(
        title,
        style: ThemsConstands.headline_2_semibold_24
            .copyWith(color: Colorconstand.lightBlack),
      ),
    );
  }
}
