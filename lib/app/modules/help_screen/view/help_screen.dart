import 'dart:ui';
import 'dart:math' as math;
import 'package:camis_teacher_application/app/models/contact_model/contact_model.dart';
import 'package:camis_teacher_application/app/modules/help_screen/state/bloc/contact_bloc.dart';
import 'package:camis_teacher_application/app/modules/help_screen/widget/head_title.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/widget/top_bar_widget/top_bar_one_line_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../scholarship_screen/bloc/get_all_request_bloc/get_all_request_bloc.dart';
import '../../scholarship_screen/view/scholarship_screen.dart';
import '../../student_list_screen/bloc/student_in_class_bloc.dart';
import 'help_list_modal_bottom_sheet.dart';

class HelpScreen extends StatefulWidget {
  final bool isInstuctor;
  const HelpScreen({required this.isInstuctor, super.key});

  @override
  State<HelpScreen> createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  final searchHelpController = TextEditingController();
  bool isExpanded = false;
  ScrollController? controller;
  List listClassIsinstructor = [];
  ContactModel? contactModel;

  @override
  void initState() {
    controller = ScrollController();
     BlocProvider.of<GetClassBloc>(context).add(GetClass());
     BlocProvider.of<ContactBloc>(context).add(GetContactSupport());
    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: -220,
            left: 0,
            child: Transform.rotate(
              angle: math.pi / 2,
              child: Image.asset(ImageAssets.Path_20)
            ),
          ),
          Positioned(top: 0, right: 0, child: Image.asset(ImageAssets.Path_22)),
          Positioned(
              bottom: 0,
              left: 0,
              child: Image.asset(ImageAssets.Path_21_help)),
          Positioned(
              bottom: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_19_help)),
          Positioned(
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
                      child: SafeArea(
                        bottom: false,
                        child: BlocListener<ContactBloc, ContactState>(
                          listener: (context, state) {
                            if(state is ContactLoaded){
                              setState(() {
                                contactModel = state.contactModel;
                              });
                            }
                          },
                          child: ListView(
                            controller: controller,
                            shrinkWrap: true,
                            padding: const EdgeInsets.only( top: 10, bottom: 19, left: 0, right: 0),
                            scrollDirection: Axis.vertical,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 26, right: 26, bottom: 20),
                                child: TopBarOneLineWidget(
                                    titleTopBar: "SUPPORT_CENTRE".tr(),
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: const EdgeInsets.symmetric(horizontal: 24.0,vertical: 18.0),
                                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                decoration: BoxDecoration(
                                  border:Border.all(color: Colorconstand.neutralGrey),
                                  borderRadius: BorderRadius.circular(50)
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      ImageAssets.SEARCH_ICON,
                                      height: 16.0,
                                      alignment: Alignment.center,
                                      color: Colorconstand.darkTextsRegular,
                                    ),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    Expanded(
                                      child: TextField(
                                        cursorRadius: const Radius.circular(50.0),
                                        controller: searchHelpController,
                                        decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.all(0),
                                          border: InputBorder.none,
                                          hintText: "HINT_SEARCH_HELP".tr(),
                                          hintStyle: ThemsConstands.headline6_medium_14,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 8,),
                              HeadTitleBig(title: "CONTACT".tr()),
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                  
                                    // Card box for Adminstraction
                                    Expanded(
                                      child:customBoxContact (
                                        onPressed: () {},
                                      )
                                    ),
                                    const SizedBox(
                                      width: 18,
                                    ),
                                  
                                    // Card box for Support centre
                                    Expanded(
                                      child: customBoxContact(subDescription: "CLICK_HERE".tr(),descriptoin: "\n", title:'CUSTOMER_SERVICE'.tr(),icon: SvgPicture.asset(
                                        ImageAssets.PEOPLE2_ICON,
                                        height: 25.38,
                                      ),
                                      onPressed: (){
                                        showModalBottomSheet(
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(16.0),
                                                topRight: Radius.circular(16.0)),
                                          ),
                                          context: context,
                                          builder: (context) {
                                            return HelpListModalBottomSheet(contactModel: contactModel,);
                                          },
                                        );
                                  
                                      },
                                    ),
                                    
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 18,),
                              AnimatedContainer(
                                duration: const Duration(milliseconds: 300),
                                curve: Curves.easeInOut,
                                height: widget.isInstuctor == true?65:0,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal:18.0,),
                                  child: MaterialButton( 
                                    onPressed: (){
                                      BlocProvider.of<GetAllRequestBloc>(context).add(GetRequestEvent());
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const ScholarshipScreen()));
                                    },
                                    highlightColor: Colors.transparent,
                                     padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 18),
                                    color: Colorconstand.lightBackgroundsWhite,
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(8.0),
                                      ),
                                    ),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            SvgPicture.asset(ImageAssets.upload_attach_icon, height: 40),
                                            const SizedBox(width: 11),
                                            Text("REQUESTCOM".tr(), style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                                          ],
                                        ),
                                        const SizedBox(height: 10),
                                        SvgPicture.asset(ImageAssets.chevron_right_icon, height: 30,color: Colors.black,),
                                       
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
        ],
      ),
    );
  
  }

  Widget cusDivider() => Container(
        height: 1.1,
        color: Colorconstand.neutralGrey,
      );

  Widget supportTeamWidget({required void Function()? onPressed}) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: MaterialButton(
        onPressed: onPressed,
        highlightColor: Colors.transparent,
        padding: const EdgeInsets.only(left: 16, right: 22, top: 18.5, bottom: 16),
        color: Colorconstand.lightBackgroundsWhite,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SvgPicture.asset(
              ImageAssets.PEOPLE2_ICON,
              height: 25.38,
            ),
            const SizedBox(height: 9.1),
            Text("CUSTOMER_SERVICE".tr(),
                style: ThemsConstands.headline_5_semibold_16
                    .copyWith(color: Colorconstand.lightBulma)),
            const SizedBox(height: 10),
            Text("CLICK_HERE".tr(),
                style: ThemsConstands.subtitle1_regular_16.copyWith(
                    color: Colorconstand.darkTextsRegular,
                    overflow: TextOverflow.clip)),
            // Text("today!",
            //     style: ThemsConstands.subtitle1_regular_16.copyWith(
            //         color: Colorconstand.darkTextsRegular,
            //         overflow: TextOverflow.clip)),
          ],
        ),
      ),
    );
  }

  Widget customBoxContact({ Widget? icon ,required void Function()? onPressed, String? title, String? descriptoin, String? subDescription }) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: MaterialButton(
        onPressed: onPressed,
        highlightColor: Colors.transparent,
        padding: const EdgeInsets.only(left: 16, right: 22, top: 18.5, bottom: 16),
        color: Colorconstand.lightBackgroundsWhite,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            icon ?? SvgPicture.asset(ImageAssets.OFFICESPACE, height: 25.38),
            const SizedBox(height: 9.1),
            Text(title ?? "HEAD_OFFICE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBulma)),
            const SizedBox(height: 10),
            Text( descriptoin ?? "WEEKDAY".tr(), style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey)),
            Text( subDescription ?? "DAYTIME".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey))
          ],
        ),
      ),
    
    );
  }
}
