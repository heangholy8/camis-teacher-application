import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/models/contact_model/contact_model.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HelpContact {
  final String imgUrl;
  final String title;
  HelpContact({required this.title, required this.imgUrl});
}

class HelpListModalBottomSheet extends StatelessWidget {
  ContactModel? contactModel;
  HelpListModalBottomSheet({Key? key, required this.contactModel})
      : super(key: key);

  _launchURL({required String url}) async {
    url = url;
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(
                left: 26.0, top: 18, bottom: 8.5, right: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'ត្រូវការ​ជំនួយ?',
                  style: ThemsConstands.headline_4_medium_18
                      .copyWith(color: Colorconstand.neutralDarkGrey),
                ),
                Text(
                  "CAMEMIS Support",
                  style: ThemsConstands.headline_4_medium_18
                      .copyWith(color: Colorconstand.lightBulma),
                )
              ],
            ),
          ),
          const Divider(
            color: Colorconstand.neutralDarkGrey,
          ),
          Wrap(
            children: [
              CustomHelpWidget(
                title: "Telegram",
                imgUrl:"assets/icons/png/png-transparent-telegram-logo-computer-icons-telegram-logo-blue-angle-triangle-thumbnail.png",
                onPressed: () {
                  _launchURL(url: contactModel!.data![0].link == ""? 'https://t.me/camemissolution':contactModel!.data![0].link);
                },
                colorback: const Color(0xff2ba1da),
              ),
              cus_divider(context),
              CustomHelpWidget(
                title: "Cellcard",
                imgUrl:
                    "assets/icons/png/cellcard_icon.png",
                onPressed: () {
                  _launchURL(url: 'tel://${contactModel!.data![1].link == ""?"077926316":contactModel!.data![1].link}');
                },
                colorback: const Color(0xfffaa31d),
              ),
              cus_divider(context),
              CustomHelpWidget(
                title: "Smart",
                imgUrl:
                    "assets/icons/png/smartnas_logo.png",
                onPressed: () {
                  _launchURL(url: 'tel://${contactModel!.data![2].link==""?"010926316":contactModel!.data![2].link}');
                },
                colorback: const Color(0xff04ab53),
              ),
              cus_divider(context),
              CustomHelpWidget(
                title: "Metfone",
                imgUrl:
                    "assets/icons/png/metfone_icon.png",
                onPressed: () {
                  _launchURL(url: 'tel://${contactModel!.data![3].link==""?"0718664316":contactModel!.data![3].link}');
                },
                colorback: const Color(0xffe30414),
              ),
            ],
          ),
          const SizedBox(height: 20.0),
        ],
      ),
    );
  }

  Widget cus_divider(BuildContext context) => Divider(
    height: 1,
        indent: MediaQuery.of(context).size.width * 0.22,
        color: Colorconstand.neutralGrey,
      );
}

class CustomHelpWidget extends StatelessWidget {
  final String imgUrl;
  final String title;
  Color? colorback;
  void Function()? onPressed;

  CustomHelpWidget(
      {Key? key,
      required this.title,
      required this.imgUrl,
      this.onPressed,
      this.colorback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      padding: const EdgeInsets.only(left: 17, top: 8, bottom: 8, right: 30),
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(right: 20.79),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: colorback ?? Colors.white,
            ),
            // color: colorback,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(27.5),
              child: Image.asset(
                imgUrl,
                fit: BoxFit.cover,
                width: 55,
                height: 55,
              ),
            ),
          ),
          Expanded(
              child: Text(
            title,
            style: ThemsConstands.button_semibold_16,
          )),
          Icon(
            Icons.arrow_forward_ios,
            size: 24,
            color: Colorconstand.neutralDarkGrey,
          )
        ],
      ),
    );
  }
}
