import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/contact_model/contact_model.dart';
import 'package:camis_teacher_application/app/service/api/get_contact_api/get_contect_api.dart';
import 'package:equatable/equatable.dart';
part 'contact_event.dart';
part 'contact_state.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState> {
  final GetContactApi getContactApi;
  ContactBloc({required this.getContactApi}) : super(ContactInitial()) {
    on<GetContactSupport>((event, emit) async{
      emit(ContactLoading());
      try {
        var dataContact = await getContactApi.getContactApi();
        emit(ContactLoaded(contactModel: dataContact));
      } catch (e) {
        print(e);
        emit(ContactError());
      }
    });
  }
}
