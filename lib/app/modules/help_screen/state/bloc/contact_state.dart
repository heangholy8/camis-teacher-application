part of 'contact_bloc.dart';

class ContactState extends Equatable {
  const ContactState();
  
  @override
  List<Object> get props => [];
}

class ContactInitial extends ContactState {}

class ContactLoading extends ContactState {}
class ContactLoaded extends ContactState {
  final ContactModel contactModel;
  const ContactLoaded({required this.contactModel});
}
class ContactError extends ContactState {}

