part of 'print_report_big_book_bloc.dart';

class PrintReportBigBookEvent extends Equatable {
  const PrintReportBigBookEvent();

  @override
  List<Object> get props => [];
}

class GetSemeterResultBigBookEvent extends PrintReportBigBookEvent{
  final String classId;
  final String semester;

  const GetSemeterResultBigBookEvent({required this.classId,required this.semester});
}

class GetMonthResultBigBookEvent extends PrintReportBigBookEvent{
  final String classId;
  final String month;

  const GetMonthResultBigBookEvent({required this.classId,required this.month});
}
