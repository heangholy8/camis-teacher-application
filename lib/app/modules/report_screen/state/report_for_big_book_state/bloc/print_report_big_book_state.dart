part of 'print_report_big_book_bloc.dart';

class PrintReportBigBookState extends Equatable {
  const PrintReportBigBookState();
  
  @override
  List<Object> get props => [];
}

class PrintReportBigBookInitial extends PrintReportBigBookState {}

class PrintResultForBigBookLoading extends PrintReportBigBookState{}
class PrintResultForBigBookMonthLoaded extends PrintReportBigBookState{
  final String bodyResultMonth;
  const PrintResultForBigBookMonthLoaded({required this.bodyResultMonth});
}

class PrintResultForBigBookSemesterLoaded extends PrintReportBigBookState{
  final String bodyResultSemester;
  const PrintResultForBigBookSemesterLoaded({required this.bodyResultSemester});
}

class PrintResultForBigBookError extends PrintReportBigBookState{
   String? error;
  PrintResultForBigBookError({this.error});
}
