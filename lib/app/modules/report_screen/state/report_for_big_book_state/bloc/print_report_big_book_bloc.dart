import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../service/api/print_api/print_report_api.dart';
part 'print_report_big_book_event.dart';
part 'print_report_big_book_state.dart';

class PrintReportBigBookBloc extends Bloc<PrintReportBigBookEvent, PrintReportBigBookState> {
  PrintReport printSemesterReport;
  PrintReportBigBookBloc({required this.printSemesterReport}) : super(PrintReportBigBookInitial()) {
    on<GetMonthResultBigBookEvent>((event, emit) async {
      emit(PrintResultForBigBookLoading());
      try {
        var data = await printSemesterReport.printReportResultBigBookMonthApi(classid: event.classId,month: event.month);
        emit(PrintResultForBigBookMonthLoaded(bodyResultMonth: data));
      } catch (e) {
        print("error: $e");
        emit(PrintResultForBigBookError(error: e.toString()));
      }
    });

    on<GetSemeterResultBigBookEvent>((event, emit) async {
      emit(PrintResultForBigBookLoading());
      try {
        var data = await printSemesterReport.printReportResultBigBookSemesterApi(classid: event.classId,semester: event.semester);
        emit(PrintResultForBigBookSemesterLoaded(bodyResultSemester: data));
      } catch (e) {
        print("error: $e");
        emit(PrintResultForBigBookError(error: e.toString()));
      }
    });
  }
}
