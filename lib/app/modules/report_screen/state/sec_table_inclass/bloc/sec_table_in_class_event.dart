part of 'sec_table_in_class_bloc.dart';

sealed class SecTableInClassEvent extends Equatable {
  const SecTableInClassEvent();

  @override
  List<Object> get props => [];
}

class GetSECTableEvent extends SecTableInClassEvent{
  final String classId;

  const GetSECTableEvent({required this.classId});
}
