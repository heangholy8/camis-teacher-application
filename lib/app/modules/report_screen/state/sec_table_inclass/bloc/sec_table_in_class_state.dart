part of 'sec_table_in_class_bloc.dart';

sealed class SecTableInClassState extends Equatable {
  const SecTableInClassState();
  
  @override
  List<Object> get props => [];
}

final class SecTableInClassInitial extends SecTableInClassState {}

class SecTableInClassInitialLoading extends SecTableInClassState{}
class PrintSecTableInClassStateLoaded extends SecTableInClassState{
  final String body;
  const PrintSecTableInClassStateLoaded({required this.body});
}

class SecTableInClassInitialError extends SecTableInClassState{
   String? error;
   SecTableInClassInitialError({this.error});
}
