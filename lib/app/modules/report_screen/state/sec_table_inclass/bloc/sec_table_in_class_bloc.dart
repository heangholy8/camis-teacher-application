import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/print_api/print_report_api.dart';
import 'package:equatable/equatable.dart';

part 'sec_table_in_class_event.dart';
part 'sec_table_in_class_state.dart';

class SecTableInClassBloc extends Bloc<SecTableInClassEvent, SecTableInClassState> {
  PrintReport printSECTableReport;
  SecTableInClassBloc({required this.printSECTableReport}) : super(SecTableInClassInitial()) {
    on<GetSECTableEvent>((event, emit) async {
      emit(SecTableInClassInitialLoading());
      try {
        var data = await printSECTableReport.printReportSECApi(classId: event.classId);
        emit(PrintSecTableInClassStateLoaded(body: data));
      } catch (e) {
        print("error: $e");
        emit(SecTableInClassInitialError(error: e.toString()));
      }
    });
  }
}
