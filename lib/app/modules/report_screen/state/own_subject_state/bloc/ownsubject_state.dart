part of 'ownsubject_bloc.dart';

abstract class OwnsubjectState extends Equatable {
  const OwnsubjectState();
  
  @override
  List<Object> get props => [];
}

class OwnsubjectInitial extends OwnsubjectState {}

class OwnsubjectLoading extends OwnsubjectState{}

class OwnsubjectLoaded extends OwnsubjectState{
  final OwnSubjectModel ownsubjectModel;
  const OwnsubjectLoaded({required this.ownsubjectModel});
}
class OwnsubjectError extends OwnsubjectState{
  String? error;
  OwnsubjectError({ this.error});
}
 
