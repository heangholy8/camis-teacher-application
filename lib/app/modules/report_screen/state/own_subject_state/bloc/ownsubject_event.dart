part of 'ownsubject_bloc.dart';

abstract class OwnsubjectEvent extends Equatable {
  const OwnsubjectEvent();

  @override
  List<Object> get props => [];
}

class GetOwnSubjectEvent extends OwnsubjectEvent{
  final String classId;
  const GetOwnSubjectEvent({required this.classId});
}