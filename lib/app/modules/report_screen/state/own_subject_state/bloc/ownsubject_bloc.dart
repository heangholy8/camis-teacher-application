import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/report_model/own_subject_model.dart';
import '../../../../../service/api/print_api/print_report_api.dart';
import '../../../../../service/api/report_api/own_subject_api.dart';

part 'ownsubject_event.dart';
part 'ownsubject_state.dart';

class OwnsubjectBloc extends Bloc<OwnsubjectEvent, OwnsubjectState> {
  final OwnSubjectListApi ownSubjectListApi;
  OwnsubjectBloc({required this.ownSubjectListApi}) : super(OwnsubjectInitial()) {
    on<GetOwnSubjectEvent>((event, emit) async {
      emit(OwnsubjectLoading());
      try {
        var data = await ownSubjectListApi.getOwnSubjectApi(classId: event.classId);
        emit(OwnsubjectLoaded(ownsubjectModel: data));
      } catch (e) {
        print("error: $e");
        emit(OwnsubjectError(error: e.toString()));
      }
    });
  }
}
