part of 'honorary_list_bloc.dart';

class HonoraryListEvent extends Equatable {
  const HonoraryListEvent();

  @override
  List<Object> get props => [];
}

class GetHonoraryListEvent extends HonoraryListEvent{
  final String classId;
  final String month;
  final String semester;
  final String type;
  GetHonoraryListEvent({required this.classId,required this.month,required this.semester,required this.type});
}
