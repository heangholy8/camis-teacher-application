import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '../../../../../service/api/print_api/print_report_api.dart';
part 'honorary_list_event.dart';
part 'honorary_list_state.dart';

class HonoraryListBloc extends Bloc<HonoraryListEvent, HonoraryListState> {
  final PrintReport printHonoraryList;
  HonoraryListBloc({required this.printHonoraryList}) : super(HonoraryListInitial()) {
    on<GetHonoraryListEvent>((event, emit) async{
      emit(HonoraryListLoading());
      try {
        var data = await printHonoraryList.printReportHonoraryListApi(classid: event.classId,semester: event.semester,month: event.month,type: event.type);
        emit(HonoraryListLoaded(body: data));
      } catch (e) {
        debugPrint("Honorary list Error $e");
        emit(HonoraryListError());
      }
    });
  }
}
