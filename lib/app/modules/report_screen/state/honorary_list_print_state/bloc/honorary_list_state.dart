part of 'honorary_list_bloc.dart';

class HonoraryListState extends Equatable {
  const HonoraryListState();
  @override
  List<Object> get props => [];
}

class HonoraryListInitial extends HonoraryListState {}

class HonoraryListLoading extends HonoraryListState{}
class HonoraryListLoaded extends HonoraryListState{
  final String body;
  const HonoraryListLoaded({required this.body});
}

class HonoraryListError extends HonoraryListState{}
