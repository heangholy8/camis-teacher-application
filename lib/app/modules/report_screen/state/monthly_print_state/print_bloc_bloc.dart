import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../service/api/print_api/print_report_api.dart';

part 'print_bloc_event.dart';
part 'print_bloc_state.dart';

class PrintBlocBloc extends Bloc<PrintBlocEvent, PrintBlocState> {
  PrintReport printReportApi = PrintReport();
  String? pdfParse;
  PrintBlocBloc() : super(PrintBlocInitial()) {
    on<GetResultPDFEvent>((event, emit) async{
      try {
        emit(PrintPDFLoadingState());
        var data = await printReportApi.printMonthlyResutly(classid:event.classId,column: event.column,month: event.month,semester: event.semester);

        print("pdfsdfpsdf $data");
        emit(PrintPDFSuccessState(pdfBodyString: data));
      } catch (e) {
        emit(PrintPDFErrorState(error: e.toString()));
      }
      
    });
    
    on<GetSubjectResultPDFEvent>((event, emit) async{
      try {
        emit(PrintPDFLoadingState());
        var data = await printReportApi.printSubjectMonthltyResultApi(
          classId:event.classId,
          istwocolumN: event.istwocolumn,
          month: event.month,
          term: event.term,
          type: event.type.toString(),
          subjectId: event.subjectId
        );
        emit(PrintPDFSubjectSuccessState(pdfBodySubjectResult: data));
        print("Success");
      } catch (e) {
        emit(PrintPDFErrorState(error: e.toString()));
        print("Error");
      }
    });
  }
}
