part of 'print_bloc_bloc.dart';

abstract class PrintBlocState extends Equatable {
  const PrintBlocState();
  
  @override
  List<Object> get props => [];
}

class PrintBlocInitial extends PrintBlocState {}

class PrintPDFLoadingState extends PrintBlocState{}

class PrintPDFSuccessState extends PrintBlocState{
  final String pdfBodyString;
  const PrintPDFSuccessState({required this.pdfBodyString});
}

class PrintPDFSubjectSuccessState extends PrintBlocState{
  final String pdfBodySubjectResult;
  const PrintPDFSubjectSuccessState({required this.pdfBodySubjectResult});
}

class PrintPDFErrorState extends PrintBlocState{
  final String error;
  const PrintPDFErrorState({required this.error});
}