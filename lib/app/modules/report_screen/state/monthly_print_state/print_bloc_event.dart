part of 'print_bloc_bloc.dart';

abstract class PrintBlocEvent extends Equatable {
  const PrintBlocEvent();

  @override
  List<Object> get props => [];
}

class GetResultPDFEvent extends PrintBlocEvent{
  final String classId;
  final String semester;
  final String month;
  final String column;

  const GetResultPDFEvent({required this.classId,required this.semester,required this.month,required this.column});
}

class GetSubjectResultPDFEvent extends PrintBlocEvent{
  final String classId;
  final String month;
  final String term;
  final String subjectId;
  final String type;
  final String istwocolumn;

  const GetSubjectResultPDFEvent(
    {
      required this.term, 
      required this.subjectId,
      required this.type, 
      required this.classId,
      required this.month,
      required this.istwocolumn
    }
  );
}