import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/print_api/print_report_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
part 'teacher_attendance_event.dart';
part 'teacher_attendance_state.dart';

class TeacherAttendanceBloc extends Bloc<TeacherAttendanceEvent, TeacherAttendanceState> {
  final PrintReport printAttTeacher;
  TeacherAttendanceBloc({required this.printAttTeacher}) : super(TeacherAttendanceInitial()) {
    on<GetTeacherAttendance>((event, emit) async {
      emit(TeacherAttendanceLoading());
      try {
        var data = await printAttTeacher.printReportTeacherAttApi(date: event.date,month: event.month,type: event.type);
        emit(TeacherAttendanceLoaded(body: data));
      } catch (e) {
        debugPrint("attendance teacher Error $e");
        emit(TeacherAttendanceError());
      }
    });
  }
}
