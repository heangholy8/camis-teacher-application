part of 'teacher_attendance_bloc.dart';

sealed class TeacherAttendanceEvent extends Equatable {
  const TeacherAttendanceEvent();

  @override
  List<Object> get props => [];
}


class GetTeacherAttendance extends TeacherAttendanceEvent{
  final String month;
  final String date;
  final String type;
  GetTeacherAttendance({required this.date,required this.month,required this.type});
}