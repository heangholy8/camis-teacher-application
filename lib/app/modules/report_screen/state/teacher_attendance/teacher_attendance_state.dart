part of 'teacher_attendance_bloc.dart';

sealed class TeacherAttendanceState extends Equatable {
  const TeacherAttendanceState();
  
  @override
  List<Object> get props => [];
}

final class TeacherAttendanceInitial extends TeacherAttendanceState {}

class TeacherAttendanceLoading extends TeacherAttendanceState{}
class TeacherAttendanceLoaded extends TeacherAttendanceState{
  final String body;
  const TeacherAttendanceLoaded({required this.body});
}

class TeacherAttendanceError extends TeacherAttendanceState{}
