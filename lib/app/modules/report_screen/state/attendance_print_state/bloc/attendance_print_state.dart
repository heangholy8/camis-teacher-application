part of 'attendance_print_bloc.dart';

abstract class AttendancePrintState extends Equatable {
  const AttendancePrintState();
  
  @override
  List<Object> get props => [];
}

class AttendancePrintInitial extends AttendancePrintState {}

class PrintAttendanceLoadingState extends AttendancePrintState{}

class PrintAttendanceMonthlySuccessState extends AttendancePrintState{
  final String pdfAttendanceMonthly;
  const PrintAttendanceMonthlySuccessState({required this.pdfAttendanceMonthly});
}

class PrintAttendanceYearlySuccessState extends AttendancePrintState{
  final String pdfAttendanceYearly;
  const PrintAttendanceYearlySuccessState({required this.pdfAttendanceYearly});
}

class PrintAttendanceErrorState extends AttendancePrintState{
  final String error;
  const PrintAttendanceErrorState({required this.error});
}
