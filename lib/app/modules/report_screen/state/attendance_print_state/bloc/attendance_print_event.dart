part of 'attendance_print_bloc.dart';

abstract class AttendancePrintEvent extends Equatable {
  const AttendancePrintEvent();

  @override
  List<Object> get props => [];
}

class GetAttendanceMonthlyEvent extends AttendancePrintEvent{
  final String classId;
  final String month;

  const GetAttendanceMonthlyEvent(
    { 
      required this.classId,
      required this.month,
    }
  );
}
class GetAttendanceYearlyEvent extends AttendancePrintEvent{
  final String classId;

  const GetAttendanceYearlyEvent(
    { 
      required this.classId,
    }
  );
}
