import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../service/api/print_api/print_report_api.dart';
part 'attendance_print_event.dart';
part 'attendance_print_state.dart';

class AttendancePrintBloc extends Bloc<AttendancePrintEvent, AttendancePrintState> {
  PrintReport printReportApi = PrintReport();
  AttendancePrintBloc({required this.printReportApi}) : super(AttendancePrintInitial()) {
    on<GetAttendanceMonthlyEvent>((event, emit) async{
      try {
        emit(PrintAttendanceLoadingState());
        var data = await printReportApi.printReportAttendanceMonthApi(classid:event.classId,month: event.month);
        emit(PrintAttendanceMonthlySuccessState(pdfAttendanceMonthly: data));
      } catch (e) {
        emit(PrintAttendanceErrorState(error: e.toString()));
      }
      
    });
    on<GetAttendanceYearlyEvent>((event, emit) async{
      try {
        emit(PrintAttendanceLoadingState());
        var data = await printReportApi.printReportAttendanceYearApi(classid:event.classId);
        emit(PrintAttendanceYearlySuccessState(pdfAttendanceYearly: data));
      } catch (e) {
        emit(PrintAttendanceErrorState(error: e.toString()));
      }
      
    });
  }
}
