import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../service/api/print_api/print_report_api.dart';
part 'result_all_subject_event.dart';
part 'result_all_subject_state.dart';

class ResultAllSubjectBloc extends Bloc<ResultAllSubjectEvent, ResultAllSubjectState> {
  PrintReport printSemesterReport;
  ResultAllSubjectBloc({required this.printSemesterReport}) : super(ResultAllSubjectInitial()) {
    on<GetMonthResultAllSubjectEvent>((event, emit) async {
      emit(PrintResultAllSubjectLoading());
      try {
        var data = await printSemesterReport.printReportResultAllSubjectMonthApi(classid: event.classId,month: event.month);
        emit(PrintResultAllSubjectMonthLoaded(bodyResultMonth: data));
      } catch (e) {
        print("error: $e");
        emit(PrintResultAllSubjectError(error: e.toString()));
      }
    });

    on<GetSemeterResultAllSubjectEvent>((event, emit) async {
      emit(PrintResultAllSubjectLoading());
      try {
        var data = await printSemesterReport.printReportResultAllSubjectSemesterApi(classid: event.classId,semester: event.semester);
        emit(PrintResultAllSubjectSemesterLoaded(bodyResultSemester: data));
      } catch (e) {
        print("error: $e");
        emit(PrintResultAllSubjectError(error: e.toString()));
      }
    });

    on<GetYearResultAllSubjectEvent>((event, emit) async {
      emit(PrintResultAllSubjectLoading());
      try {
        var data = await printSemesterReport.printReportResultAllSubjectYearApi(classid: event.classId);
        emit(PrintResultAllSubjectYearLoaded(bodyResultYear: data));
      } catch (e) {
        print("error: $e");
        emit(PrintResultAllSubjectError(error: e.toString()));
      }
    });
  }
}
