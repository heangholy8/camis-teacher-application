part of 'result_all_subject_bloc.dart';

class ResultAllSubjectEvent extends Equatable {
  const ResultAllSubjectEvent();

  @override
  List<Object> get props => [];
}

class GetSemeterResultAllSubjectEvent extends ResultAllSubjectEvent{
  final String classId;
  final String semester;

  const GetSemeterResultAllSubjectEvent({required this.classId,required this.semester});
}

class GetMonthResultAllSubjectEvent extends ResultAllSubjectEvent{
  final String classId;
  final String month;

  const GetMonthResultAllSubjectEvent({required this.classId,required this.month});
}

class GetYearResultAllSubjectEvent extends ResultAllSubjectEvent{
  final String classId;
  const GetYearResultAllSubjectEvent({required this.classId});
}
