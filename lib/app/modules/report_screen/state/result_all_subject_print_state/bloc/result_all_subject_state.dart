part of 'result_all_subject_bloc.dart';

class ResultAllSubjectState extends Equatable {
  const ResultAllSubjectState();
  
  @override
  List<Object> get props => [];
}

class ResultAllSubjectInitial extends ResultAllSubjectState {}

class PrintResultAllSubjectLoading extends ResultAllSubjectState{}
class PrintResultAllSubjectMonthLoaded extends ResultAllSubjectState{
  final String bodyResultMonth;
  const PrintResultAllSubjectMonthLoaded({required this.bodyResultMonth});
}

class PrintResultAllSubjectYearLoaded extends ResultAllSubjectState{
  final String bodyResultYear;
  const PrintResultAllSubjectYearLoaded({required this.bodyResultYear});
}

class PrintResultAllSubjectSemesterLoaded extends ResultAllSubjectState{
  final String bodyResultSemester;
  const PrintResultAllSubjectSemesterLoaded({required this.bodyResultSemester});
}

class PrintResultAllSubjectError extends ResultAllSubjectState{
   String? error;
  PrintResultAllSubjectError({this.error});
}
