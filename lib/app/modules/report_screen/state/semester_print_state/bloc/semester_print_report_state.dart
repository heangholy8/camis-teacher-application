part of 'semester_print_report_bloc.dart';

abstract class SemesterPrintReportState extends Equatable {
  const SemesterPrintReportState();
  
  @override
  List<Object> get props => [];
}

class SemesterPrintReportInitial extends SemesterPrintReportState {}

class PrintReportAcademyLoading extends SemesterPrintReportState{}
class PrintReportSemesterExamLoaded extends SemesterPrintReportState{
  final String body;
  const PrintReportSemesterExamLoaded({required this.body});
}

class PrintReportSemesterResultLoaded extends SemesterPrintReportState{
  final String bodyResultSemester;
  const PrintReportSemesterResultLoaded({required this.bodyResultSemester});
}

class PrintReportAcademyError extends SemesterPrintReportState{
   String? error;
   PrintReportAcademyError({this.error});
}
