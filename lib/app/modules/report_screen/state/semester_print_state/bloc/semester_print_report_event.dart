part of 'semester_print_report_bloc.dart';

abstract class SemesterPrintReportEvent extends Equatable {
  const SemesterPrintReportEvent();

  @override
  List<Object> get props => [];
}

class GetSemeterExamEvent extends SemesterPrintReportEvent{
  final String classId;
  final String semester;

  const GetSemeterExamEvent({required this.classId,required this.semester});
}

class GetSemeterResulEvent extends SemesterPrintReportEvent{
  final String classId;
  final String semester;

  const GetSemeterResulEvent({required this.classId,required this.semester});
}
