import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../service/api/print_api/print_report_api.dart';

part 'semester_print_report_event.dart';
part 'semester_print_report_state.dart';

class SemesterPrintReportBloc extends Bloc<SemesterPrintReportEvent, SemesterPrintReportState> {
  PrintReport printSemesterReport;
  SemesterPrintReportBloc({required this.printSemesterReport}) : super(SemesterPrintReportInitial()) {
    on<GetSemeterExamEvent>((event, emit) async {
      emit(PrintReportAcademyLoading());
      try {
        var data = await printSemesterReport.printReportSemesterExamApi(classid: event.classId,semester: event.semester);
        emit(PrintReportSemesterExamLoaded(body: data));
      } catch (e) {
        print("error: $e");
        emit(PrintReportAcademyError(error: e.toString()));
      }
    });

    on<GetSemeterResulEvent>((event, emit) async {
      emit(PrintReportAcademyLoading());
      try {
        var data = await printSemesterReport.printReportSemesterResultApi(classid: event.classId,semester: event.semester);
        emit(PrintReportSemesterResultLoaded(bodyResultSemester: data));
      } catch (e) {
        print("error: $e");
        emit(PrintReportAcademyError(error: e.toString()));
      }
    });
  }
}
