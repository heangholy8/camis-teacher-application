import 'dart:async';

import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/report_screen/state/honorary_list_print_state/bloc/honorary_list_bloc.dart';
import 'package:camis_teacher_application/app/modules/report_screen/state/sec_table_inclass/bloc/sec_table_in_class_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../widget/Popup_Print_widget/view_pdf.dart';
import '../../../../widget/button_widget/button_setting_widget.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import '../../../models/class_list_model/class_list_model.dart';
import '../../student_list_screen/bloc/student_in_class_bloc.dart';
import '../state/attendance_print_state/bloc/attendance_print_bloc.dart';
import '../state/monthly_print_state/print_bloc_bloc.dart';
import '../state/own_subject_state/bloc/ownsubject_bloc.dart';
import '../state/report_for_big_book_state/bloc/print_report_big_book_bloc.dart';
import '../state/result_all_subject_print_state/bloc/result_all_subject_bloc.dart';
import '../state/semester_print_state/bloc/semester_print_report_bloc.dart';

class ReportScreenV2 extends StatefulWidget {
  const ReportScreenV2({Key? key}) : super(key: key);

  @override
  State<ReportScreenV2> createState() => _ReportScreenV2State();
}

class _ReportScreenV2State extends State<ReportScreenV2> with Toast{
  bool? classmonitor = false;
  String? idClass;
  bool connection = true;
  bool isInstructor = false;
  bool isSaveLoading = false;
  StreamSubscription? sub;
  int conditionGetStudent = 1;
  int activeClass = 0;
  int? monthlocal;
  int? monthlocalFirst;
  int? yearlocal;
  int? yearlocalFirst;
  String fileName = "";
  String monthNameSelected = "";
  String semesterName= "";
  String className = "";
  String subjectName = "";
 
  List<TeachingClass> dataClass = [];
   
  List<dynamic> attendanceMonth = [];
  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    BlocProvider.of<GetClassBloc>(context).add(GetClass());
    BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
    setState(() {
      monthNameSelected = "${"MONTH".tr()} ${checkMonth(int.parse(DateFormat("MM").format(DateTime.now())))}";
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      monthlocalFirst = int.parse(DateFormat("MM").format(DateTime.now()));
      yearlocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      yearlocalFirst = int.parse(DateFormat("yyyy").format(DateTime.now()));
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
          } else {}
        });
      });
      //=============End Check internet====================
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
        backgroundColor: Colorconstand.primaryColor,
        body: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset("assets/images/Oval.png"),
            ),
            Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,                
                    child: SafeArea(
                      bottom: false,
                      child: Column(
                        children: [
                          BlocListener<ListMonthSemesterBloc, ListMonthSemesterState>(
                            listener: (context, state) {
                              if(state is ListMonthSemesterLoaded){
                                var data = state.monthAndSemesterList!.data;
                                var currentData = data!.where((element) => element.month==monthlocal.toString() && element.year==yearlocal.toString()).toList();
                                if(currentData.isNotEmpty){
                                  setState(() {
                                    semesterName = currentData[0].semester.toString();
                                    monthNameSelected = currentData[0].displayMonth.toString();
                                    currentData[0].active = true;
                                  });
                                }
                              }
                            },
                            child: Container(),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                top: 10, left: 22, right: 22, bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: SvgPicture.asset(
                                    ImageAssets.chevron_left,
                                    color: Colorconstand.neutralWhite,
                                    width: 32,
                                    height: 32,
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "REPORT".tr(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                                      color: Colorconstand.neutralWhite,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                const SizedBox(
                                  width: 32,
                                  height: 32,
                                ),
                              ],
                            ),
                          ),
                          // Center(
                          //   child: Text(
                          //     yearAcademyName,
                          //     style:const TextStyle(color: Colorconstand.neutralWhite),
                          //   ),
                          // ),
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 12),
                            child: Row(
                              children: [
                                // Expanded(
                                //   child: Row(
                                //     mainAxisAlignment: MainAxisAlignment.center,
                                //     children: [
                                //       SvgPicture.asset(ImageAssets.ADD_ICON),
                                //       const SizedBox(width: 8,),
                                //       Text("បានកំណត់".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),)
                                //     ],
                                //   ),
                                // ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: (){
                                      chooseMonth ();
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(monthNameSelected.tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),),
                                        const Icon(Icons.arrow_drop_down_rounded,size: 22,color: Colorconstand.neutralWhite,)
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: dataClass.length==1?null:(){
                                      chooseClass();
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(className,style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),),
                                        const Icon(Icons.arrow_drop_down_rounded,size: 22,color: Colorconstand.neutralWhite,)
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 18,),
                          Expanded(
                            child: Container(
                              color: Colorconstand.neutralWhite,
                              child: BlocConsumer<GetClassBloc, GetClassState>(
                                listener: (context, state) {
                                    if(state is GetClassLoaded){
                                      BlocProvider.of<OwnsubjectBloc>(context).add(GetOwnSubjectEvent(classId: state.classModel!.data!.teachingClasses![0].id.toString()));
                                      setState(() {
                                        dataClass = state.classModel!.data!.teachingClasses!;
                                        idClass = state.classModel!.data!.teachingClasses![0].id.toString();
                                        className = translate == "km"?state.classModel!.data!.teachingClasses![0].name.toString():state.classModel!.data!.teachingClasses![0].nameEn.toString();
                                      });
                                      if(state.classModel!.data!.teachingClasses![0].isInstructor==1){
                                        setState(() {
                                          isInstructor = true;
                                        });
                                      }
                                    }
                                  },
                                builder: (context, state) {
                                  if(state is GetClassLoading){
                                    return Container(
                                      alignment: Alignment.center,
                                      //margin:const EdgeInsets.only(top: 50),
                                      child: const CircularProgressIndicator());
                                  }
                                  else if( state is GetClassLoaded){
                                    return SingleChildScrollView(
                                      child: Column(
                                        children: [
                                          // Container(
                                          //   padding:const EdgeInsets.symmetric(vertical: 10),
                                          //   color: Colorconstand.primaryColor.withOpacity(0.9),
                                          //   child: Row(
                                          //     crossAxisAlignment: CrossAxisAlignment.center,
                                          //     children: [
                                          //       Container(width: 55,),
                                          //       Expanded(
                                          //         child: Row(
                                          //           mainAxisAlignment: MainAxisAlignment.center,
                                          //           children: [
                                          //             const Icon(Icons.star_rate_rounded,size: 28,color: Colorconstand.neutralWhite,),
                                          //             const SizedBox(width: 8,),
                                          //             Text("របាយប្រចាំ",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),)
                                          //           ],
                                          //         )
                                          //       ),
                                          //       GestureDetector(
                                          //         child: Container(
                                          //           width: 55,
                                          //           child: Icon(Icons.arrow_circle_up_rounded,size: 22,color: Colorconstand.neutralWhite.withOpacity(0.5),),
                                          //         ),
                                          //       )
                                          //     ],
                                          //   ),
                                          // ),
                                          // AnimatedContainer(
                                          //   duration:const Duration(milliseconds: 200),
                                          //   height: 151,
                                          //   color: Colorconstand.neutralWhite,
                                          // ),
                                          BlocListener<PrintBlocBloc, PrintBlocState>(
                                            listener: (context, state) {
                                              if(state is PrintPDFLoadingState){
                                                setState(() {
                                                  isSaveLoading = true;
                                                });
                                              }
                                              else if(state is PrintPDFSubjectSuccessState){
                                                setState(() { 
                                                  isSaveLoading = false;
                                                });
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  enableDrag:false,
                                                  isDismissible: true,
                                                  context: context, builder:(context) {
                                                  return viewPDFReportWidget(state:state.pdfBodySubjectResult,fileName: "${"LEARNING_RESULT".tr()}${"SUBJECT".tr()} $subjectName ${monthNameSelected.tr()}");
                                                });
                                              }
                                              else if(state is PrintPDFSuccessState){
                                                setState(() { 
                                                  isSaveLoading = false;
                                                });
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  enableDrag:false,
                                                  isDismissible: true,
                                                  context: context, builder:(context) {
                                                  return viewPDFReportWidget(state: state.pdfBodyString,fileName: "${"LEARNING_RESULT".tr()} ${monthNameSelected.tr()}");
                                                });
                                              }else{
                                              setState(() {
                                                isSaveLoading=false;
                                              });
                                              }
                                            },
                                            child: Container(),
                                          ),
      
                                          // ShowModalBottomSheet Print semester  when success
                                          BlocListener<SemesterPrintReportBloc, SemesterPrintReportState>(
                                            listener: (context, state) {
                                              if(state is PrintReportAcademyLoading){
                                                setState(() {
                                                  isSaveLoading = true;
                                                });
                                              }
                                              else if(state is PrintReportSemesterExamLoaded){
                                                setState(() { 
                                                  isSaveLoading = false;
                                                });
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  enableDrag:false,
                                                  isDismissible: true,
                                                  context: context, builder:(context) {
                                                  return viewPDFReportWidget(state:state.body,fileName: "${"RESULT_SEMESTER_EXAM".tr()} ${monthNameSelected.tr()}");
                                                });
                                              }
                                              else if(state is PrintReportSemesterResultLoaded){
                                                setState(() { 
                                                  isSaveLoading = false;
                                                });
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  enableDrag:false,
                                                  isDismissible: true,
                                                  context: context, builder:(context) {
                                                  return viewPDFReportWidget(state:state.bodyResultSemester,fileName: "${"LEARNING_RESULT".tr()} ${monthNameSelected.tr()}");
                                                });
                                              }
                                              else{
                                              setState(() {
                                                isSaveLoading=false;
                                              });
                                              }
                                            },
                                            child: Container(),
                                          ),
                                          Container(
                                            padding:const EdgeInsets.symmetric(vertical: 10),
                                            color: const Color(0xFFEBF1F5),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Container(width: 55,),
                                                Expanded(
                                                  child: Text("LEARNING_RESULT".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,)
                                                ),
                                                GestureDetector(
                                                  child: Container(
                                                    width: 55,
                                                  // child: Icon(Icons.arrow_circle_up_rounded,size: 22,color: Colorconstand.primaryColor.withOpacity(0.4),),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          AnimatedContainer(
                                            duration:const Duration(milliseconds: 200),
                                            height: 151,
                                            color: Colorconstand.neutralWhite,
                                            child: BlocBuilder<OwnsubjectBloc, OwnsubjectState>(
                                              builder: (context, state) {
                                                if (state is OwnsubjectLoading) {
                                                  return Container(
                                                    alignment: Alignment.center,
                                                    //margin:const EdgeInsets.only(top: 50),
                                                    child: const CircularProgressIndicator());
                                                }
                                                if(state is OwnsubjectLoaded){
                                                  var data = state.ownsubjectModel.data;
                                                  return Container(
                                                    alignment: Alignment.center,
                                                    child: GridView.builder(
                                                    shrinkWrap: true,
                                                    scrollDirection: Axis.horizontal,
                                                    physics:const ScrollPhysics(),
                                                    padding:const EdgeInsets.only(top: 12,left: 0,right: 12,bottom: 0),
                                                      itemCount: data!.length,
                                                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,childAspectRatio: 0.56,),
                                                      itemBuilder: (BuildContext context, int index) {
                                                        return GestureDetector(
                                                          onTap: (){
                                                            setState(() {
                                                              subjectName = translate=="km"?data[index].subjectName.toString():data[index].subjectNameEn.toString();
                                                              BlocProvider.of<PrintBlocBloc>(context).add(GetSubjectResultPDFEvent(classId: "$idClass",term:semesterName,month: monthlocal.toString(),istwocolumn: "1", subjectId: data[index].subjectId.toString(), type: monthNameSelected == "SECOND_SEMESTER" || monthNameSelected == "FIRST_SEMESTER"?"2":'1'));
                                                            });
                                                          },
                                                          child: Container(
                                                            margin:const EdgeInsets.only(bottom: 12,left: 12),
                                                            decoration: BoxDecoration(
                                                              color:  Colorconstand.primaryColor,
                                                              borderRadius: BorderRadius.circular(8)
                                                            ),
                                                            alignment: Alignment.center,
                                                            child: Text(translate=="km"?data[index].subjectName.toString().tr():data[index].subjectNameEn.toString().tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color:Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                                          ),
                                                        );
                                                      }
                                                    )
                                                  );
                                                }
                                                else{
                                                  return Center(
                                                    child: EmptyWidget(
                                                      title: "WE_DETECT".tr(),
                                                      subtitle: "WE_DETECT_DES".tr(),
                                                    ),
                                                  );
                                                }
                                              },
                                            ),
                                          ),
                                          isInstructor == true? Container(
                                            padding:const EdgeInsets.symmetric(vertical: 10),
                                            color: const Color(0xFFEBF1F5),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Container(width: 55,),
                                                Expanded(
                                                  child: Text("របាយការណ៍សិស្ស".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,)
                                                ),
                                                GestureDetector(
                                                  child: Container(
                                                    width: 55,
                                                    child: Icon(Icons.arrow_circle_down_rounded,size: 22,color: Colorconstand.primaryColor.withOpacity(0.4),),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ):Container(),
                                          Column(
                                            children: [
                                              monthNameSelected=="YEAR" || isInstructor==false?Container():Container(
                                                  margin: const EdgeInsets.symmetric(horizontal: 18,vertical:18),
                                                  child: ButtonSettingWidget(
                                                    iconImageright: ImageAssets.chevron_right_icon,
                                                    iconImageleft: ImageAssets.star_icon,
                                                    onTap: () {
                                                      if(monthNameSelected == "SECOND_SEMESTER" || monthNameSelected == "FIRST_SEMESTER"){
                                                        widgetChooseTemplate(
                                                        actionBt1: () {
                                                          BlocProvider.of<SemesterPrintReportBloc>(context).add(GetSemeterResulEvent(classId: "$idClass",semester:semesterName,));
                                                          Navigator.of(context).pop();
                                                        },
                                                        actionBt2: () {
                                                          BlocProvider.of<SemesterPrintReportBloc>(context).add(GetSemeterExamEvent(classId: "$idClass",semester:semesterName,));
                                                          Navigator.of(context).pop();
                                                        },title: "",titleBt1: "RESULT_SEMESTER".tr(),titleBt2: "RESULT_SEMESTER_EXAM".tr());
                                                      }
                                                      else{
                                                        BlocProvider.of<PrintBlocBloc>(context).add(GetResultPDFEvent(classId: "$idClass",semester:semesterName,month: monthlocal.toString(),column: "1"));
                                                      }
                                                    },
                                                    title: "REPORT".tr()+"LEARNING_RESULT".tr(),
                                                    textStyleButton: ThemsConstands
                                                        .headline_5_semibold_16
                                                        .copyWith(
                                                      color: Colorconstand.darkTextsRegular,
                                                    ),
                                                    buttonColor:const Color(0xFFEBF1F5),
                                                    radiusButton: 8,
                                                    panddinHorButton: 16,
                                                    panddingVerButton: 10,
                                                    backgroundIconsRightColor: Colors.transparent,
                                                    iconsRightColor: Colorconstand.primaryColor,
                                                    iconsleftColor: Colorconstand.neutralDarkGrey,
                                                  ),
                                                ),
                                                monthNameSelected=="YEAR" || isInstructor==false?Container()
                                                :BlocListener<PrintReportBigBookBloc, PrintReportBigBookState>(
                                                  listener: (context, state) {
                                                    if(state is PrintResultForBigBookLoading){
                                                      setState(() {
                                                        isSaveLoading = true;
                                                      });
                                                    }
                                                    else if(state is PrintResultForBigBookMonthLoaded){
                                                      setState(() { 
                                                        isSaveLoading = false;
                                                      });
                                                      showModalBottomSheet(
                                                        isScrollControlled: true,
                                                        enableDrag:false,
                                                        isDismissible: true,
                                                        context: context, builder:(context) {
                                                        return viewPDFReportWidget(state:state.bodyResultMonth,fileName: "${"LEARNING_RESULT".tr()}${"សម្រាប់បិទបញ្ជីសម្រង់ពិន្ទុ".tr()} ${monthNameSelected.tr()}");
                                                      });
                                                    }
                                                    else if(state is PrintResultForBigBookSemesterLoaded){
                                                      setState(() { 
                                                        isSaveLoading = false;
                                                      });
                                                      showModalBottomSheet(
                                                        isScrollControlled: true,
                                                        enableDrag:false,
                                                        isDismissible: true,
                                                        context: context, builder:(context) {
                                                        return viewPDFReportWidget(state:state.bodyResultSemester,fileName: "${"LEARNING_RESULT".tr()}${"សម្រាប់បិទបញ្ជីសម្រង់ពិន្ទុ".tr()} ${monthNameSelected.tr()}");
                                                      });
                                                    }
                                                    else{
                                                      setState(() {
                                                        isSaveLoading=false;
                                                      });
                                                    }
                                                  },
                                                  child:Container(
                                                    margin: const EdgeInsets.symmetric(horizontal: 18,vertical:0),
                                                    child: ButtonSettingWidget(
                                                      iconImageright: ImageAssets.chevron_right_icon,
                                                      iconImageleft: ImageAssets.star_icon,
                                                      onTap: () {
                                                        if(monthNameSelected == "SECOND_SEMESTER" || monthNameSelected == "FIRST_SEMESTER"){
                                                          BlocProvider.of<PrintReportBigBookBloc>(context).add(GetSemeterResultBigBookEvent(classId: "$idClass",semester:semesterName,));
                                                        }
                                                        else{
                                                          BlocProvider.of<PrintReportBigBookBloc>(context).add(GetMonthResultBigBookEvent(classId: "$idClass",month:monthlocal.toString(),));
                                                        }
                                                      },
                                                      title: "${"REPORT".tr()}${"LEARNING_RESULT".tr()}សម្រាប់បិទបញ្ជីសម្រង់ពិន្ទុ",
                                                      textStyleButton: ThemsConstands
                                                          .headline_5_semibold_16
                                                          .copyWith(
                                                        color: Colorconstand.darkTextsRegular,
                                                      ),
                                                      buttonColor:const Color(0xFFEBF1F5),
                                                      radiusButton: 8,
                                                      panddinHorButton: 16,
                                                      panddingVerButton: 10,
                                                      backgroundIconsRightColor: Colors.transparent,
                                                      iconsRightColor: Colorconstand.primaryColor,
                                                      iconsleftColor: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  ),
                                                ),
                                                monthNameSelected == "YEAR"?const SizedBox(height: 16,):const SizedBox(height: 16,),
                                                isInstructor==false?Container()
                                                :BlocListener<ResultAllSubjectBloc, ResultAllSubjectState>(
                                                  listener: (context, state) {
                                                    if(state is PrintResultAllSubjectLoading){
                                                      setState(() {
                                                        isSaveLoading = true;
                                                      });
                                                    }
                                                    else if(state is PrintResultAllSubjectMonthLoaded){
                                                      setState(() { 
                                                        isSaveLoading = false;
                                                      });
                                                      showModalBottomSheet(
                                                        isScrollControlled: true,
                                                        enableDrag:false,
                                                        isDismissible: true,
                                                        context: context, builder:(context) {
                                                        return viewPDFReportWidget(state:state.bodyResultMonth,fileName: "${"LEARNING_RESULT".tr()}${"ALL_SUBJECT".tr()} ${monthNameSelected.tr()}");
                                                      });
                                                    }
                                                    else if(state is PrintResultAllSubjectSemesterLoaded){
                                                      setState(() { 
                                                        isSaveLoading = false;
                                                      });
                                                      showModalBottomSheet(
                                                        isScrollControlled: true,
                                                        enableDrag:false,
                                                        isDismissible: true,
                                                        context: context, builder:(context) {
                                                        return viewPDFReportWidget(state:state.bodyResultSemester,fileName: "${"LEARNING_RESULT".tr()}${"ALL_SUBJECT".tr()} ${monthNameSelected.tr()}");
                                                      });
                                                    }
                                                    else if(state is PrintResultAllSubjectYearLoaded){
                                                      setState(() { 
                                                        isSaveLoading = false;
                                                      });
                                                      showModalBottomSheet(
                                                        isScrollControlled: true,
                                                        enableDrag:false,
                                                        isDismissible: true,
                                                        context: context, builder:(context) {
                                                        return viewPDFReportWidget(state:state.bodyResultYear,fileName: "${"LEARNING_RESULT".tr()}${"ALL_SUBJECT".tr()} ${monthNameSelected.tr()}");
                                                      });
                                                    }
                                                    else{
                                                      setState(() {
                                                        isSaveLoading=false;
                                                      });
                                                    }
                                                  },
                                                  child:Container(
                                                      margin: const EdgeInsets.symmetric(horizontal: 18,vertical:0),
                                                      child: ButtonSettingWidget(
                                                        iconImageright: ImageAssets.chevron_right_icon,
                                                        iconImageleft: ImageAssets.star_icon,
                                                        onTap: () {
                                                          if(monthNameSelected == "YEAR"){
                                                            BlocProvider.of<ResultAllSubjectBloc>(context).add(GetYearResultAllSubjectEvent(classId: "$idClass",));
                                                          }
                                                          else if(monthNameSelected == "SECOND_SEMESTER" || monthNameSelected == "FIRST_SEMESTER"){
                                                            BlocProvider.of<ResultAllSubjectBloc>(context).add(GetSemeterResultAllSubjectEvent(classId: "$idClass",semester:semesterName,));
                                                          }
                                                          else{
                                                            BlocProvider.of<ResultAllSubjectBloc>(context).add(GetMonthResultAllSubjectEvent(classId: "$idClass",month: monthlocal.toString()));
                                                          }
                                                        },
                                                        title: "REPORT".tr()+"LEARNING_RESULT".tr()+"ALL_SUBJECT".tr(),
                                                        textStyleButton: ThemsConstands
                                                            .headline_5_semibold_16
                                                            .copyWith(
                                                          color: Colorconstand.darkTextsRegular,
                                                        ),
                                                        buttonColor:const Color(0xFFEBF1F5),
                                                        radiusButton: 8,
                                                        panddinHorButton: 16,
                                                        panddingVerButton: 10,
                                                        backgroundIconsRightColor: Colors.transparent,
                                                        iconsRightColor: Colorconstand.primaryColor,
                                                        iconsleftColor: Colorconstand.neutralDarkGrey,
                                                      ),
                                                    ),
                                                ),
                                                const SizedBox(height: 16,),
                                                isInstructor==false?Container()
                                                :BlocListener<HonoraryListBloc, HonoraryListState>(
                                                  listener: (context, state) {
                                                    if(state is HonoraryListLoading){
                                                      setState(() {
                                                        isSaveLoading = true;
                                                      });
                                                    }
                                                    else if(state is HonoraryListLoaded){
                                                      setState(() { 
                                                        isSaveLoading = false;
                                                      });
                                                      showModalBottomSheet(
                                                        isScrollControlled: true,
                                                        enableDrag:false,
                                                        isDismissible: true,
                                                        context: context, builder:(context) {
                                                        return viewPDFReportWidget(state:state.body,fileName: "${"តារាងកិត្តិយស".tr()}${className.toString()} ${monthNameSelected.tr()}");
                                                      });
                                                    }
                                                    else{
                                                      setState(() {
                                                        isSaveLoading=false;
                                                      });
                                                    }
                                                  },
                                                  child: Container(
                                                    margin: const EdgeInsets.symmetric(horizontal: 18,vertical:0),
                                                    child: ButtonSettingWidget(
                                                      iconImageright: ImageAssets.chevron_right_icon,
                                                      iconImageleft: ImageAssets.star_icon,
                                                      onTap: () {
                                                        if(monthNameSelected == "SECOND_SEMESTER" || monthNameSelected == "FIRST_SEMESTER"){
                                                          widgetChooseTemplate(
                                                          actionBt1: () {
                                                            BlocProvider.of<HonoraryListBloc>(context).add(GetHonoraryListEvent(classId: "$idClass",month: monthlocal.toString(),semester: semesterName,type: "2"));
                                                            Navigator.of(context).pop();
                                                          },
                                                          actionBt2: () {
                                                            BlocProvider.of<HonoraryListBloc>(context).add(GetHonoraryListEvent(classId: "$idClass",month: monthlocal.toString(),semester: semesterName,type: "4"));
                                                            Navigator.of(context).pop();
                                                          },title: "",titleBt1: "តារាងកិត្តិយសប្រចាំឆមាស".tr(),titleBt2: "តារាងកិត្តិយសប្រឡងឆមាស".tr());
                                                        }
                                                        else{
                                                          BlocProvider.of<HonoraryListBloc>(context).add(GetHonoraryListEvent(classId: "$idClass",month: monthlocal.toString(),semester: semesterName,type: monthNameSelected == "YEAR"?"3":"1"));
                                                        }
                                                      },
                                                      title: "តារាងកិត្តិយស".tr(),
                                                      textStyleButton: ThemsConstands
                                                          .headline_5_semibold_16
                                                          .copyWith(
                                                        color: Colorconstand.darkTextsRegular,
                                                      ),
                                                      buttonColor:const Color(0xFFEBF1F5),
                                                      radiusButton: 8,
                                                      panddinHorButton: 16,
                                                      panddingVerButton: 10,
                                                      backgroundIconsRightColor: Colors.transparent,
                                                      iconsRightColor: Colorconstand.primaryColor,
                                                      iconsleftColor: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  ),
                                                ),
                                                monthNameSelected == "SECOND_SEMESTER"||monthNameSelected=="FIRST_SEMESTER" || isInstructor==false?Container():BlocListener<AttendancePrintBloc, AttendancePrintState>(
                                                listener: (context, state) {
                                                  if(state is PrintAttendanceLoadingState){
                                                    setState(() {
                                                      isSaveLoading = true;
                                                    });
                                                  }
                                                  else if(state is PrintAttendanceMonthlySuccessState){
                                                    setState(() { 
                                                      isSaveLoading = false;
                                                    });
                                                    showModalBottomSheet(
                                                      isScrollControlled: true,
                                                      enableDrag:false,
                                                      isDismissible: true,
                                                      context: context, builder:(context) {
                                                      return viewPDFReportWidget(state:state.pdfAttendanceMonthly,fileName: "${"តារាងវត្តមានសិស្ស".tr()} ${monthNameSelected.tr()}");
                                                    });
                                                  }
                                                  else if(state is PrintAttendanceYearlySuccessState){
                                                    setState(() { 
                                                      isSaveLoading = false;
                                                    });
                                                    showModalBottomSheet(
                                                      isScrollControlled: true,
                                                      enableDrag:false,
                                                      isDismissible: true,
                                                      context: context, builder:(context) {
                                                      return viewPDFReportWidget(state:state.pdfAttendanceYearly,fileName: "${"តារាងវត្តមានសិស្ស".tr()} ${monthNameSelected.tr()}");
                                                    });
                                                  }
                                                  else{
                                                  setState(() {
                                                    isSaveLoading=false;
                                                  });
                                                  }
                                                },
                                                child: Container(
                                                  margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 16),
                                                  child: ButtonSettingWidget(
                                                    iconImageright: ImageAssets.chevron_right_icon,
                                                    iconImageleft: ImageAssets.star_icon,
                                                    onTap: () {
                                                      if(monthNameSelected == "YEAR"){
                                                        BlocProvider.of<AttendancePrintBloc>(context).add(GetAttendanceYearlyEvent(classId: "$idClass"));
                                                      }
                                                      else{
                                                        BlocProvider.of<AttendancePrintBloc>(context).add(GetAttendanceMonthlyEvent(classId: "$idClass",month: monthlocal.toString(),));
                                                      }
                                                    },
                                                    title: "តារាងវត្តមានសិស្ស".tr(),
                                                    textStyleButton: ThemsConstands
                                                        .headline_5_semibold_16
                                                        .copyWith(
                                                      color: Colorconstand.darkTextsRegular,
                                                    ),
                                                    buttonColor:const Color(0xFFEBF1F5),
                                                    radiusButton: 8,
                                                    panddinHorButton: 16,
                                                    panddingVerButton: 10,
                                                    backgroundIconsRightColor: Colors.transparent,
                                                    iconsRightColor: Colorconstand.primaryColor,
                                                    iconsleftColor: Colorconstand.neutralDarkGrey,
                                                  ),
                                                ),
                                              ),
                                                isInstructor==false?Container()
                                                :BlocListener<SecTableInClassBloc, SecTableInClassState>(
                                                  listener: (context, state) {
                                                    if(state is SecTableInClassInitialLoading){
                                                      setState(() {
                                                        isSaveLoading = true;
                                                      });
                                                    }
                                                    else if(state is PrintSecTableInClassStateLoaded){
                                                      setState(() { 
                                                        isSaveLoading = false;
                                                      });
                                                      showModalBottomSheet(
                                                        isScrollControlled: true,
                                                        enableDrag:false,
                                                        isDismissible: true,
                                                        context: context, builder:(context) {
                                                        return viewPDFReportWidget(state:state.body,fileName: "${"តារាង SEC ប្រចាំថ្នាក់".tr()}${className.toString()}}");
                                                      });
                                                    }
                                                    else{
                                                      setState(() {
                                                        isSaveLoading=false;
                                                      });
                                                    }
                                                  },
                                                  child: Container(
                                                    margin: const EdgeInsets.symmetric(horizontal: 18,vertical:0),
                                                    child: ButtonSettingWidget(
                                                      iconImageright: ImageAssets.chevron_right_icon,
                                                      iconImageleft: ImageAssets.star_icon,
                                                      onTap: () {
                                                        BlocProvider.of<SecTableInClassBloc>(context).add(GetSECTableEvent(classId: "$idClass"));
                                                      },
                                                      title: "តារាង SEC ប្រចាំថ្នាក់".tr(),
                                                      textStyleButton: ThemsConstands
                                                          .headline_5_semibold_16
                                                          .copyWith(
                                                        color: Colorconstand.darkTextsRegular,
                                                      ),
                                                      buttonColor:const Color(0xFFEBF1F5),
                                                      radiusButton: 8,
                                                      panddinHorButton: 16,
                                                      panddingVerButton: 10,
                                                      backgroundIconsRightColor: Colors.transparent,
                                                      iconsRightColor: Colorconstand.primaryColor,
                                                      iconsleftColor: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  ),
                                                ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  }
                                  else{
                                    return Center(
                                      child: EmptyWidget(
                                        title: "WE_DETECT".tr(),
                                        subtitle: "WE_DETECT_DES".tr(),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                ),
            // Checking Loading when click to preview Summary Report
            isSaveLoading ==true? Positioned(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.grey.withOpacity(.5),
                child: const Center(child: CircularProgressIndicator(),),
              ),
            ):Container(),
          ],
        ),
    );
  }
  String convertToKhmerNumeral(int number) {
    final translate = context.locale.toString();
    List<String> khmerNumerals = [
      translate == "km"?'១':'1',
      translate == "km"?'២':'2',
      translate == "km"?'៣':'3',
      translate == "km"?'៤':'4',
      translate == "km"?'៥':'5',
      translate == "km"?'៦':'6',
      translate == "km"?'៧':'7',
      translate == "km"?'៨':'8',
      translate == "km"?'៩':'9',
      translate == "km"?'១០':'10',
      translate == "km"?'១១':'11',
      translate == "km"?'១២':'12',
    ];

  if (number >= 1 && number <= 12) {
    return khmerNumerals[number - 1];
  } else {
    return '';
  }
}

void chooseMonth (){
  showModalBottomSheet(
    enableDrag: false,
    isDismissible: true,
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16)),
    ),
    context: context, builder:(context) {
    return StatefulBuilder(
      builder: (context, snapshot) {
        return Container(
          margin:const EdgeInsets.only(top: 65),
          decoration: BoxDecoration(
            color: Colorconstand.neutralWhite,
            borderRadius: BorderRadius.circular(18),
          ),
          child: SingleChildScrollView(
            physics:const ClampingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 55,
                  decoration:const BoxDecoration(color: Colorconstand.neutralWhite,borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          width: 55,height: 55,
                          child:const Icon(Icons.close_rounded,size: 28,color: Colorconstand.neutralDarkGrey,),
                        ),
                      ),
                      Text("SELECT_MONTH".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),),
                      GestureDetector(
                        onTap: (){
                        },
                        child: Container(
                          width: 55,height: 55,
                        ),
                      ),
                    ],
                  ),
                ),
                const Divider(color: Colorconstand.neutralDarkGrey,thickness: 0.5,height: 1,),
                BlocBuilder<ListMonthSemesterBloc, ListMonthSemesterState>(
                  builder: (context, state) {
                    if(state is ListMonthSemesterLoaded){
                      var dataMonth = state.monthAndSemesterList!.data;
                      return Container(
                        alignment: Alignment.center,
                        child: GridView.builder(
                        shrinkWrap: true,
                        physics:const ScrollPhysics(),
                        padding:const EdgeInsets.only(top: 25,left: 28,right: 18,bottom: 28),
                          itemCount: dataMonth!.length,
                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4,childAspectRatio: 2.0),
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap:(){
                                setState(() {
                                  Navigator.of(context).pop();
                                  for (var element in dataMonth) {
                                    element.active = false;
                                  }
                                  dataMonth[index].active = true;
                                  if(dataMonth[index].displayMonth == "YEAR"){
                                    monthNameSelected = dataMonth[index].displayMonth.toString();
                                    monthlocal = 0;
                                    semesterName = dataMonth[index].semester.toString();
                                    yearlocal = int.parse(dataMonth[index].year.toString());
                                  }
                                  else if(dataMonth[index].displayMonth == "SECOND_SEMESTER" || dataMonth[index].displayMonth == "FIRST_SEMESTER"){
                                    monthNameSelected = dataMonth[index].displayMonth.toString();
                                    semesterName = dataMonth[index].semester.toString();
                                    yearlocal = int.parse(dataMonth[index].year.toString());
                                    monthlocal = 0;
                                  }
                                  else{
                                    monthNameSelected = "${"MONTH".tr()} ${checkMonth(int.parse(dataMonth[index].month.toString()))}";
                                    monthlocal = int.parse(dataMonth[index].month.toString());
                                    semesterName = dataMonth[index].semester.toString();
                                    yearlocal = int.parse(dataMonth[index].year.toString());
                                  }
                                });
                              },
                              child: Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  color: dataMonth[index].active == true?Colorconstand.primaryColor:Colors.transparent,
                                ),
                                child: Text(dataMonth[index].displayMonth.toString().tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color:dataMonth[index].active == true? Colorconstand.neutralWhite:Colorconstand.lightBlack),textAlign: TextAlign.center,),
                              ),
                            );
                          }
                        )
                      );
                    }
                    else{
                      return Container();
                    }
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
  });
}

void chooseClass (){
  showModalBottomSheet(
    enableDrag: false,
    isDismissible: true,
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16)),
    ),
    context: context, builder:(context) {
    final translate = context.locale.toString();
    return StatefulBuilder(
      builder: (context, snapshot) {
        return Container(
          margin:const EdgeInsets.only(top: 65),
          decoration: BoxDecoration(
            color: Colorconstand.neutralWhite,
            borderRadius: BorderRadius.circular(18),
          ),
          child: SingleChildScrollView(
            physics:const ClampingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 55,
                  decoration:const BoxDecoration(color: Colorconstand.neutralWhite,borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          width: 55,height: 55,
                          child:const Icon(Icons.close_rounded,size: 28,color: Colorconstand.neutralDarkGrey,),
                        ),
                      ),
                      Text("CHOOSECLASS".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),),
                      GestureDetector(
                        onTap: (){
                        },
                        child: Container(
                          width: 55,height: 55,
                        ),
                      ),
                    ],
                  ),
                ),
                const Divider(color: Colorconstand.neutralDarkGrey,thickness: 0.5,height: 1,),
                Container(
                  alignment: Alignment.center,
                  child: GridView.builder(
                  shrinkWrap: true,
                  physics:const ScrollPhysics(),
                  padding:const EdgeInsets.only(top: 25,left: 28,right: 18,bottom: 28),
                    itemCount: dataClass.length,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4,childAspectRatio: 2.0),
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: (){
                          setState(() {
                            idClass = dataClass[index].id.toString();
                            className = translate == "km"?dataClass[index].name.toString():dataClass[index].nameEn.toString();
                            isInstructor = dataClass[index].isInstructor==1;
                          });
                          BlocProvider.of<OwnsubjectBloc>(context).add(GetOwnSubjectEvent(classId: dataClass[index].id.toString()));
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color:idClass == dataClass[index].id.toString()?Colorconstand.primaryColor:Colors.transparent,
                          ),
                          child: Text(translate=="km"?dataClass[index].name.toString().tr():dataClass[index].nameEn.toString().tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color:idClass == dataClass[index].id.toString()?Colorconstand.neutralWhite:Colorconstand.lightBlack),textAlign: TextAlign.center,),
                        ),
                      );
                    }
                  )
                )
              ],
            ),
          ),
        );
      }
    );
  });
}

void widgetChooseTemplate ({required String title,required String titleBt1,required String titleBt2,required VoidCallback actionBt1,required VoidCallback actionBt2}){
  showModalBottomSheet(
    enableDrag: false,
    isDismissible: true,
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16)),
    ),
    context: context, builder:(context) {
    final translate = context.locale.toString();
    return StatefulBuilder(
      builder: (context, snapshot) {
        return Container(
          margin:const EdgeInsets.only(top: 65),
          decoration: BoxDecoration(
            color: Colorconstand.neutralWhite,
            borderRadius: BorderRadius.circular(18),
          ),
          child: SingleChildScrollView(
            physics:const ClampingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 55,
                  decoration:const BoxDecoration(color: Colorconstand.neutralWhite,borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          width: 55,height: 55,
                          child:const Icon(Icons.close_rounded,size: 28,color: Colorconstand.neutralDarkGrey,),
                        ),
                      ),
                      Text(title,style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),),
                      GestureDetector(
                        onTap: (){
                        },
                        child: Container(
                          width: 55,height: 55,
                        ),
                      ),
                    ],
                  ),
                ),
                const Divider(color: Colorconstand.neutralDarkGrey,thickness: 0.5,height: 1,),
                Container(
                  margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 28),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:[
                      Expanded(
                        child: GestureDetector(
                          onTap: actionBt1,
                          child: Container(
                            padding:const EdgeInsets.symmetric(horizontal: 12,vertical: 18),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colorconstand.primaryColor),
                              borderRadius: BorderRadius.circular(12)
                            ),
                            child: Text(titleBt1,style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,),
                          ),
                        ),
                      ),
                      const SizedBox(width: 18,),
                      Expanded(
                        child: GestureDetector(
                          onTap: actionBt2,
                          child: Container(
                            padding:const EdgeInsets.symmetric(horizontal: 12,vertical: 18),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colorconstand.primaryColor),
                              borderRadius: BorderRadius.circular(12)
                            ),
                            child: Text(titleBt2,style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center),
                          ),
                        ),
                      )
                    ]
                  ),
                ),
              ],
            ),
          ),
        );
      }
    );
  });
}

void showMessageNointernet(
    BuildContext context, String title, double width, double hight) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: const Duration(milliseconds: 800),
      width: width,
      elevation: 2.0,
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      content: Wrap(
        children: [
          Container(
            height: hight,
            child: Center(
              child: Text(
                title,
                style: ThemsConstands.headline6_regular_14_24height
                    .copyWith(color: Colorconstand.neutralWhite),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
