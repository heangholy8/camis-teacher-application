import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_screenshot/no_screenshot.dart';
import '../../../../widget/Popup_Print_widget/view_pdf.dart';
import '../../report_screen/state/semester_print_state/bloc/semester_print_report_bloc.dart';
import '../state/bloc/monthly_result_bloc.dart';
import 'package:http/http.dart' as http;

class ViewSemesterResultScreen extends StatefulWidget {
  final String semester;
  final String classname;
  final String classId;
  const ViewSemesterResultScreen({super.key,required this.classname,required this.semester,required this.classId});

  @override
  State<ViewSemesterResultScreen> createState() => _ViewSemesterResultScreenState();
}

class _ViewSemesterResultScreenState extends State<ViewSemesterResultScreen> {
  bool isSaveLoading = false;
  String fileName = "";

  final _noScreenshot = NoScreenshot.instance;

  void enableScreenshot() async {
    bool result = await _noScreenshot.screenshotOn();
    debugPrint('Enable Screenshot: $result');
  }

  void disableScreenshot() async {
    bool result = await _noScreenshot.screenshotOff();
    debugPrint('Screenshot Off: $result');
  }
  @override
  void initState() {
    // disableScreenshot();
    super.initState();
  }

  @override
  void dispose() {
    // enableScreenshot();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
       backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        top: true,
        bottom: false,
        child: Container(
           color: Colorconstand.neutralWhite,
          child: Column(
            children: [
              Container(
                height: 80,
                padding:const EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  children: [
                    Container(
                      child: IconButton(
                        icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 25,),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("${"PETRIOD_RESULT".tr()} ${widget.semester.tr()}",style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.darkTextsRegular),),
                            const SizedBox(height: 8,),
                            Text( "${"CLASS".tr()} ${widget.classname}",style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.mainColorSecondary),),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 12),
                      width: 30,
                      child: PopupMenuButton<int>(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        offset:const Offset(-40, 4),
                        icon:const Icon(Icons.print_outlined,size: 28,color: Colorconstand.primaryColor,),
                        itemBuilder: (BuildContext context) => [
                              PopupMenuItem(
                                onTap: (){
                                  setState(() {
                                    fileName = "RESULT_SEMESTER_EXAM".tr()+widget.semester.tr();
                                  });
                                  BlocProvider.of<SemesterPrintReportBloc>(context).add(GetSemeterExamEvent(classId: widget.classId.toString(),semester:widget.semester,));
                                },
                                child: Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 2,vertical: 1),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(width: 35, child: const Icon(Icons.print_outlined,size: 28,color: Colorconstand.primaryColor,)),
                                        Expanded(child: Text("RESULT_SEMESTER_EXAM".tr(),style: ThemsConstands.headline6_regular_14_24height.copyWith(color: Colorconstand.primaryColor),))
                                      ]),
                                ),
                              ),
                              PopupMenuItem(
                                onTap: (){
                                  setState(() {
                                    fileName = "RESULT_SEMESTER".tr()+widget.semester.tr();
                                  });
                                  BlocProvider.of<SemesterPrintReportBloc>(context).add(GetSemeterResulEvent(classId: widget.classId.toString(),semester:widget.semester,));
                                },
                                child: Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 2,vertical: 1),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(width: 35, child:const Icon(Icons.print_outlined,size: 28,color: Colorconstand.primaryColor,)),
                                        Expanded(child: Text("RESULT_SEMESTER".tr(),style: ThemsConstands.headline6_regular_14_24height.copyWith(color: Colorconstand.primaryColor),))
                                      ]
                                    ),
                                  ),
                              ),                              
                            ]
                        )
                    )
                  ],
                ),
              ),
              Expanded(
                child:Stack(
                  children: [
                    Container(
                      child:BlocBuilder<MonthlyResultBloc, MonthlyResultState>(
                        builder: (context, state) {
                          if(state is MonthlyResultLoading){
                            return Container(child: Center(child: CircularProgressIndicator(),),);
                          }
                          else if(state is SemesterResultLoaded){
                            var dataSemesterResult = state.studentSemesterResultModel;
                            return Column(
                              children: [
                                // ShowModalBottomSheet Print semester  when success
                                BlocListener<SemesterPrintReportBloc, SemesterPrintReportState>(
                                  listener: (context, state) {
                                    if(state is PrintReportAcademyLoading){
                                      setState(() {
                                        isSaveLoading = true;
                                      });
                                    }
                                    else if(state is PrintReportSemesterExamLoaded){
                                      setState(() { 
                                        isSaveLoading = false;
                                      });
                                      showModalBottomSheet(
                                        isScrollControlled: true,
                                        enableDrag:false,
                                        isDismissible: true,
                                        context: context, builder:(context) {
                                        return viewPDFReportWidget( state: state.body,fileName: fileName,);
                                      });
                                    }
                                    else if(state is PrintReportSemesterResultLoaded){
                                      setState(() { 
                                        isSaveLoading = false;
                                      });
                                      showModalBottomSheet(
                                        isScrollControlled: true,
                                        enableDrag:false,
                                        isDismissible: true,
                                        context: context, builder:(context) {
                                        return viewPDFReportWidget( state: state.bodyResultSemester,fileName: fileName,);
                                      });
                                    }
                                    else{
                                    setState(() {
                                      isSaveLoading=false;
                                    });
                                    }
                                  },
                                  child: Container(),
                                ),
                                Container(
                                  height: 60,
                                  color: Colorconstand.neutralGrey,
                                  child: Row(
                                    children: [
                                       Expanded(child: _getTitleItemWidget("N.O".tr(),50)),
                                       _getTitleItemWidget('FULL_NAME'.tr(), 130),
                                       //Expanded(child: _getTitleItemWidget('TOTAL_SCORE'.tr(), 130),),
                                      // Expanded(child: _getTitleItemWidget('TOTAL_SCORE'.tr(), 130),),
                                       Expanded(child: _getTitleItemWidget('TOTAL_AVG'.tr(), 130),),
                                       Expanded(child: _getTitleItemWidget('RANK'.tr(), 130),),
                                       Expanded(child: _getTitleItemWidget('GRADE_POINT'.tr(), 130),),
                                     
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    child: ListView.separated(
                                      padding:const EdgeInsets.all(0),
                                      separatorBuilder: (context, index) {
                                        return Container(
                                          height: 1,
                                          color: Colorconstand.neutralGrey,
                                        );
                                      },
                                      shrinkWrap:true,
                                      itemCount: dataSemesterResult!.data!.length,
                                      itemBuilder: (context, indexResult) {
                                        return Container(
                                          height: 55,
                                          padding:const EdgeInsets.only(right: 5),
                                          child: Row(
                                            children: [
                                               Expanded(child: _getTitleItemBodyWidget("${indexResult+1}",50,Alignment.center,Colorconstand.neutralDarkGrey)),
                                                _getTitleItemBodyWidget(translate == "km"?dataSemesterResult.data![indexResult].name.toString():dataSemesterResult.data![indexResult].nameEn.toString()==" "?dataSemesterResult.data![indexResult].name.toString():dataSemesterResult.data![indexResult].nameEn.toString(), 130,Alignment.centerLeft,Colors.black),
                                               // Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultSemester==null?"---":dataSemesterResult.data![indexResult].resultSemester!.totalScore.toString(), 130,Alignment.center,Colors.black),),
                                               // Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultSemesterExam==null?"---":"${dataSemesterResult.data![indexResult].resultSemesterExam!.totalScore.toString()}/${dataSemesterResult.data![indexResult].resultSemesterExam!.totalCoeff.toString()}", 130,Alignment.center,Colorconstand.lightBlack),),
                                                Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultSemester==null?"---":dataSemesterResult.data![indexResult].resultSemester!.avgScore.toString(), 130,Alignment.center,Colors.black),),                                            
                                                Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultSemester==null?"---":dataSemesterResult.data![indexResult].resultSemester!.rank.toString(), 130,Alignment.center,Colors.red),),
                                                Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultSemester==null?"---":"${ translate=="km"?dataSemesterResult.data![indexResult].resultSemester!.grading.toString():""} (${dataSemesterResult.data![indexResult].resultSemester!.letterGrade.toString()})", 130,Alignment.center,dataSemesterResult.data![indexResult].resultSemester==null?Colorconstand.lightBulma:dataSemesterResult.data![indexResult].resultSemester!.letterGrade=="A" || dataSemesterResult.data![indexResult].resultSemester!.letterGrade=="B"?Colorconstand.alertsPositive:dataSemesterResult.data![indexResult].resultSemester!.letterGrade == null?Colorconstand.lightBlack:dataSemesterResult.data![indexResult].resultSemester!.letterGrade=="F"?Colorconstand.alertsDecline:Colorconstand.alertsAwaitingText),),
                                            ],
                                          ),
                                        );
                                      } 
                                    ),
                                  ),
                                )
                              ],
                            );
                          }
                          else{
                            return Container();
                          }
                        },
                      )
                    ),
                    isSaveLoading ==true? Positioned(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.grey.withOpacity(.5),
                        child: const Center(child: CircularProgressIndicator(),),
                      ),
                    ):Container(),
                  ],
                )
              ),
            ],
          ),
        ),
      ),
    );
  }

   
  Widget _getTitleItemWidget(String label, double width,) {
    return Container(
      margin:const EdgeInsets.only(left: 0),
      width: width,
      height: 56,
      color: Colorconstand.neutralGrey,
      alignment: Alignment.center,
      child: Text(label, style:ThemsConstands.headline_6_semibold_14.copyWith(color: Colors.black),
    ),
    );
  }

  Widget _getTitleItemBodyWidget(String label,double width, Alignment alignment,Color textColor) {
    return Container(
      width: width,
      color: Colorconstand.neutralWhite,
      alignment: alignment,
      child: Text(label,textAlign: TextAlign.center,style:ThemsConstands.headline_6_regular_14_20height.copyWith(color:textColor),
    ),
    );
  }

}