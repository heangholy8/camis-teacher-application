import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_screenshot/no_screenshot.dart';
import '../../../../widget/Popup_Print_widget/view_pdf.dart';
import '../../report_screen/state/monthly_print_state/print_bloc_bloc.dart';
import '../state/bloc/monthly_result_bloc.dart';
import 'package:http/http.dart' as http;

class ViewMonthlyResultScreen extends StatefulWidget {
  final String month;
  final String classname;
  final String semesterName;
  final String classId;
  final String monthId;
  const ViewMonthlyResultScreen({super.key,required this.monthId,required this.classname,required this.month,required this.classId,required this.semesterName});

  @override
  State<ViewMonthlyResultScreen> createState() => _ViewMonthlyResultScreenState();
}

class _ViewMonthlyResultScreenState extends State<ViewMonthlyResultScreen> {
  bool isLoading = false;
  String fileName = "";
  
  final _noScreenshot = NoScreenshot.instance;

  void enableScreenshot() async {
    bool result = await _noScreenshot.screenshotOn();
    debugPrint('Enable Screenshot: $result');
  }

  void disableScreenshot() async {
    bool result = await _noScreenshot.screenshotOff();
    debugPrint('Screenshot Off: $result');
  }
  @override
  void initState() {
    // disableScreenshot();
    super.initState();
  }

  @override
  void dispose() {
    // enableScreenshot();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
       backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        top: true,
        bottom: false,
        child: BlocListener<PrintBlocBloc, PrintBlocState>(
          listener: (context, state) {
            if(state is PrintPDFLoadingState){
              setState(() {
                isLoading = true;
              });

            }else if(state is PrintPDFSuccessState){
              setState(() {
                isLoading =false;
              });
              showModalBottomSheet(
                isScrollControlled: true,
                enableDrag:false,
                isDismissible: true,
                context: context, builder:(context) {
                return viewPDFReportWidget( state: state.pdfBodyString,fileName: fileName,);
              });
            }else{
              setState(() {
                isLoading = false;
              });
              print("Error");
            }
          },
          child: Stack(
            children:[
              Container(
                color: Colorconstand.neutralWhite,
                child: Column(
                  children: [
                    Container(
                      height: 80,
                      padding:const EdgeInsets.symmetric(horizontal: 12),
                      child: Row(
                        children: [
                          IconButton(
                            icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 25,),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("${"PETRIOD_RESULT".tr()} ${"MONTH".tr()} ${widget.month.tr()}",style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.darkTextsRegular),),
                                const SizedBox(height: 8,),
                                Text( "${"CLASS".tr()} ${widget.classname}",style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.mainColorSecondary),),
                              ],
                            ),
                          ),
                          IconButton(
                            icon:const Icon(Icons.print_outlined,size: 28,color: Colorconstand.primaryColor,),
                            onPressed: (){
                              setState(() {
                                fileName = "LEARNING_RESULT".tr()+widget.month;
                              });
                              BlocProvider.of<PrintBlocBloc>(context).add(GetResultPDFEvent(semester: widget.semesterName,month: widget.monthId.toString(),classId: widget.classId,column: "1"));
                            },
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child:Container(
                        child:BlocBuilder<MonthlyResultBloc, MonthlyResultState>(
                          builder: (context, state) {
                            if(state is MonthlyResultLoading){
                              return Container(child: Center(child: CircularProgressIndicator(),),);
                            }
                            else if(state is MonthlyResultLoaded){
                              var dataMonthlyResult = state.studentMonthlyResultModel;
                              return Column(
                                children: [
                                  Container(
                                    height: 60,
                                    color: Colorconstand.neutralGrey,
                                    child: Row(
                                      children: [
                                          Expanded(child: _getTitleItemWidget("N.O".tr(),50)),
                                          _getTitleItemWidget('FULL_NAME'.tr(), 130),
                                          Expanded(child: _getTitleItemWidget('TOTAL_SCORE'.tr(), 130),),
                                          Expanded(child: _getTitleItemWidget('TOTAL_AVG'.tr(), 130),),
                                          Expanded(child: _getTitleItemWidget('RANK'.tr(), 130),),
                                          Expanded(child: _getTitleItemWidget('GRADE_POINT'.tr(), 130),),
                                        
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: ListView.separated(
                                        padding:const EdgeInsets.all(0),
                                        separatorBuilder: (context, index) {
                                          return Container(
                                            height: 1,
                                            color: Colorconstand.neutralGrey,
                                          );
                                        },
                                        shrinkWrap:true,
                                        itemCount: dataMonthlyResult!.data!.length,
                                        itemBuilder: (context, indexResult) {
                                          return Container(
                                            height: 55,
                                            padding:const EdgeInsets.only(right: 5,),
                                            child: Row(
                                              children: [
                                                  Expanded(child: _getTitleItemBodyWidget("${indexResult+1}",50,Alignment.center,Colorconstand.neutralDarkGrey)),
                                                  _getTitleItemBodyWidget(translate == "km"?dataMonthlyResult.data![indexResult].name.toString():dataMonthlyResult.data![indexResult].nameEn.toString()==" "?dataMonthlyResult.data![indexResult].name.toString():dataMonthlyResult.data![indexResult].nameEn.toString(), 130,Alignment.centerLeft,Colors.black),
                                                  Expanded(child: _getTitleItemBodyWidget(dataMonthlyResult.data![indexResult].resultMonthly==null?"---":"${dataMonthlyResult.data![indexResult].resultMonthly!.totalScore.toString()}/${dataMonthlyResult.data![indexResult].resultMonthly!.totalCoeff.toString()}", 130,Alignment.center,Colors.black),),
                                                  Expanded(child: _getTitleItemBodyWidget(dataMonthlyResult.data![indexResult].resultMonthly==null?"---":dataMonthlyResult.data![indexResult].resultMonthly!.avgScore.toString(), 130,Alignment.center,Colors.black),),                                            
                                                  Expanded(child: _getTitleItemBodyWidget(dataMonthlyResult.data![indexResult].resultMonthly==null?"---":dataMonthlyResult.data![indexResult].resultMonthly!.rank.toString(), 130,Alignment.center,Colors.red),),
                                                  Expanded(child: _getTitleItemBodyWidget(dataMonthlyResult.data![indexResult].resultMonthly==null?"---":"${translate=="km"?dataMonthlyResult.data![indexResult].resultMonthly!.grading.toString():""} (${dataMonthlyResult.data![indexResult].resultMonthly!.letterGrade.toString()})", 130,Alignment.center,dataMonthlyResult.data![indexResult].resultMonthly==null?Colorconstand.lightBulma:dataMonthlyResult.data![indexResult].resultMonthly!.letterGrade=="A" || dataMonthlyResult.data![indexResult].resultMonthly!.letterGrade=="B"?Colorconstand.alertsPositive:dataMonthlyResult.data![indexResult].resultMonthly!.letterGrade == null?Colorconstand.lightBlack:dataMonthlyResult.data![indexResult].resultMonthly!.letterGrade=="F"?Colorconstand.alertsDecline:Colorconstand.alertsAwaitingText),),
                                              ],
                                            ),
                                          );
                                        } 
                                      ),
                                    ),
                                  )
                                ],
                              );
                            }
                            else{
                              return Container();
                            }
                          },
                        )
                      )
                    ),
                  ],
                ),
              ),
             isLoading == true? Positioned(
                child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.grey.withOpacity(.5),
                child: const Center(child: CircularProgressIndicator(),),
                ),
              ):Container(),
            ],
          ),
        ),
      ),
    );
  }

   
  Widget _getTitleItemWidget(String label, double width,) {
    return Container(
      margin:const EdgeInsets.only(left: 0),
      width: width,
      height: 56,
      color: Colorconstand.neutralGrey,
      alignment: Alignment.center,
      child: Text(label, style:ThemsConstands.headline_6_semibold_14.copyWith(color: Colors.black),
    ),
    );
  }

  Widget _getTitleItemBodyWidget(String label,double width, Alignment alignment,Color textColor) {
    return Container(
      width: width,
      color: Colorconstand.neutralWhite,
      alignment: alignment,
      child: Text(label,textAlign: TextAlign.center,style:ThemsConstands.headline_6_regular_14_20height.copyWith(color:textColor),
    ),
    );
  }

}