import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_screenshot/no_screenshot.dart';
import '../state/bloc/monthly_result_bloc.dart';
import 'package:http/http.dart' as http;

class ViewYearResultScreen extends StatefulWidget {
  final String classname;
  const ViewYearResultScreen({super.key,required this.classname});

  @override
  State<ViewYearResultScreen> createState() => _ViewYearResultScreenState();
}

class _ViewYearResultScreenState extends State<ViewYearResultScreen> {

  final _noScreenshot = NoScreenshot.instance;

  void enableScreenshot() async {
    bool result = await _noScreenshot.screenshotOn();
    debugPrint('Enable Screenshot: $result');
  }

  void disableScreenshot() async {
    bool result = await _noScreenshot.screenshotOff();
    debugPrint('Screenshot Off: $result');
  }
  @override
  void initState() {
    // disableScreenshot();
    super.initState();
  }

  @override
  void dispose() {
    // enableScreenshot();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
       backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        top: true,
        bottom: false,
        child: Container(
           color: Colorconstand.neutralWhite,
          child: Column(
            children: [
              Container(
                height: 80,
                padding:const EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  children: [
                    Container(
                      child: IconButton(
                        icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 25,),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("${"YEAR".tr()} ${"PETRIOD_RESULT".tr()}",style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.darkTextsRegular),),
                            const SizedBox(height: 8,),
                            Text( "${"CLASS".tr()} ${widget.classname}",style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.mainColorSecondary),),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 30,
                      // child: IconButton(
                      //   icon:const Icon(Icons.file_download_outlined,size: 28,),
                      //   onPressed: () async{
                          
                      //   },
                      // ),
                    )
                  ],
                ),
              ),
              Expanded(
                child:Container(
                  child:BlocBuilder<MonthlyResultBloc, MonthlyResultState>(
                    builder: (context, state) {
                      if(state is MonthlyResultLoading){
                        return Container(child: Center(child: CircularProgressIndicator(),),);
                      }
                      else if(state is YearResultLoaded){
                        var dataSemesterResult = state.studentYearResultModel;
                        var dataYearResult = dataSemesterResult!.data!.where((element) => element.resultYearly!=null,).toList();
                        return Column(
                          children: [
                            Container(
                              height: 60,
                              color: Colorconstand.neutralGrey,
                              child: Row(
                                children: [
                                   Expanded(child: _getTitleItemWidget("N.O".tr(),80)),
                                   _getTitleItemWidget('FULL_NAME'.tr(), 130),
                                   Expanded(child: _getTitleItemWidget('AVG_SES_1'.tr(), 130),),
                                   Expanded(child: _getTitleItemWidget('AVG_SES_2'.tr(), 130),),
                                   Expanded(child: _getTitleItemWidget('RANK'.tr(), 130),),
                                   Expanded(child: _getTitleItemWidget('GRADE_POINT'.tr(), 130),),
                                 
                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                child: ListView.separated(
                                  padding:const EdgeInsets.all(0),
                                  separatorBuilder: (context, index) {
                                    return Container(
                                      height: 1,
                                      color: Colorconstand.neutralGrey,
                                    );
                                  },
                                  shrinkWrap:true,
                                  itemCount: dataSemesterResult.data!.length,
                                  itemBuilder: (context, indexResult) {
                                    return Container(
                                      height: 55,
                                      padding:const EdgeInsets.only(right: 5),
                                      child: Row(
                                        children: [
                                           Expanded(child: _getTitleItemBodyWidget("${indexResult+1}",80,Alignment.center,Colorconstand.neutralDarkGrey)),
                                            _getTitleItemBodyWidget(translate == "km"?dataSemesterResult.data![indexResult].name.toString():dataSemesterResult.data![indexResult].nameEn.toString()==" "?dataSemesterResult.data![indexResult].name.toString():dataSemesterResult.data![indexResult].nameEn.toString(), 130,Alignment.centerLeft,Colors.black),
                                            Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultYearly==null?"---":dataSemesterResult.data![indexResult].resultYearly!.firstSemesterAvg==null?"---":dataSemesterResult.data![indexResult].resultYearly!.firstSemesterAvg.toString(), 130,Alignment.center,Colors.black),),
                                            Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultYearly==null?"---":dataSemesterResult.data![indexResult].resultYearly!.secondSemesterAvg==null?"---":dataSemesterResult.data![indexResult].resultYearly!.secondSemesterAvg.toString(), 130,Alignment.center,Colors.black),),                                            
                                            Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultYearly==null?"---":dataSemesterResult.data![indexResult].resultYearly!.rank.toString(), 130,Alignment.center,Colors.red),),
                                            Expanded(child: _getTitleItemBodyWidget(dataSemesterResult.data![indexResult].resultYearly==null?"---":"${translate=="km"?dataSemesterResult.data![indexResult].resultYearly!.grading.toString():""} (${dataSemesterResult.data![indexResult].resultYearly!.letterGrade.toString()})", 130,Alignment.center,dataSemesterResult.data![indexResult].resultYearly==null?Colorconstand.lightBulma:dataSemesterResult.data![indexResult].resultYearly!.letterGrade=="A" || dataSemesterResult.data![indexResult].resultYearly!.letterGrade=="B"?Colorconstand.alertsPositive:dataSemesterResult.data![indexResult].resultYearly!.letterGrade == null?Colorconstand.lightBlack:dataSemesterResult.data![indexResult].resultYearly!.letterGrade=="F"?Colorconstand.alertsDecline:Colorconstand.alertsAwaitingText),),
                                        ],
                                      ),
                                    );
                                  } 
                                ),
                              ),
                            )
                          ],
                        );
                      }
                      else{
                        return Container();
                      }
                    },
                  )
                )
              ),
            ],
          ),
        ),
      ),
    );
  }

   
  Widget _getTitleItemWidget(String label, double width,) {
    return Container(
      margin:const EdgeInsets.only(left: 0),
      width: width,
      height: 56,
      color: Colorconstand.neutralGrey,
      alignment: Alignment.center,
      child: Text(label, style:ThemsConstands.headline_6_semibold_14.copyWith(color: Colors.black),
    ),
    );
  }

  Widget _getTitleItemBodyWidget(String label,double width, Alignment alignment,Color textColor) {
    return Container(
      width: width,
      color: Colorconstand.neutralWhite,
      alignment: alignment,
      child: Text(label,textAlign: TextAlign.center,style:ThemsConstands.headline_6_regular_14_20height.copyWith(color:textColor),
    ),
    );
  }

}