import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/result_api/list_student_monthly_result.dart';
import 'package:equatable/equatable.dart';

import '../../../../models/result/list_student_monthly_result_model.dart';
import '../../../../models/result/list_student_semester_result_model.dart';
import '../../../../models/result/list_student_year_result_model.dart';

part 'monthly_result_event.dart';
part 'monthly_result_state.dart';

class MonthlyResultBloc extends Bloc<MonthlyResultEvent, MonthlyResultState> {
  final GetResultApi resultApi;
  MonthlyResultBloc({required this.resultApi}) : super(MonthlyResultInitial()) {
    on<ListStudentMonthlyResultEvent>((event, emit) async{
     emit(MonthlyResultLoading());
      try {
        var dataMonthlyResult = await resultApi.getMonthlyResultApi(
          idClass: event.idClass,term: event.semester,month: event.month,
        );
        emit(MonthlyResultLoaded(studentMonthlyResultModel: dataMonthlyResult));
      } catch (e) {
        print(e);
        emit(MonthlyResultError());
      }
    });
    on<ListStudentSemesterResultEvent>((event, emit) async{
     emit(MonthlyResultLoading());
      try {
        var dataSemesterResult = await resultApi.getSemesterResultApi(
          idClass: event.idClass,term: event.semester,
        );
        emit(SemesterResultLoaded(studentSemesterResultModel: dataSemesterResult));
      } catch (e) {
        print(e);
        emit(MonthlyResultError());
      }
    });

    on<ListStudentYearResultEvent>((event, emit) async{
     emit(MonthlyResultLoading());
      try {
        var dataYearResult = await resultApi.getYearResultApi(
          idClass: event.idClass,
        );
        emit(YearResultLoaded(studentYearResultModel: dataYearResult));
      } catch (e) {
        print(e);
        emit(MonthlyResultError());
      }
    });
  }
}
