part of 'monthly_result_bloc.dart';

abstract class MonthlyResultState extends Equatable {
  const MonthlyResultState();
  
  @override
  List<Object> get props => [];
}

class MonthlyResultInitial extends MonthlyResultState {}

class  MonthlyResultLoading extends MonthlyResultState {}

class  MonthlyResultLoaded extends MonthlyResultState {
  final StudentMonthlyResultModel? studentMonthlyResultModel;
  const  MonthlyResultLoaded({required this.studentMonthlyResultModel});
}

class  SemesterResultLoaded extends MonthlyResultState {
  final StudentSemesterResultModel? studentSemesterResultModel;
  const  SemesterResultLoaded({required this.studentSemesterResultModel});
}

class  YearResultLoaded extends MonthlyResultState {
  final StudentYearResultModel? studentYearResultModel;
  const  YearResultLoaded({required this.studentYearResultModel});
}

class  MonthlyResultError extends MonthlyResultState {}