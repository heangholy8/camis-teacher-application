part of 'monthly_result_bloc.dart';

abstract class MonthlyResultEvent extends Equatable {
  const MonthlyResultEvent();

  @override
  List<Object> get props => [];
}

class ListStudentMonthlyResultEvent extends MonthlyResultEvent {
  final String? semester;
  final String? idClass;
  final int? month;
 const ListStudentMonthlyResultEvent({required this.semester,required this.idClass,required this.month});
} 

class ListStudentSemesterResultEvent extends MonthlyResultEvent {
  final String? semester;
  final String? idClass;
 const ListStudentSemesterResultEvent({required this.semester,required this.idClass,});
} 

class ListStudentYearResultEvent extends MonthlyResultEvent {
  final String? idClass;
 const ListStudentYearResultEvent({required this.idClass,});
} 
