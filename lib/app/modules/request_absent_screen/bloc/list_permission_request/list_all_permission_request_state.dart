part of 'list_all_permission_request_bloc.dart';

class ListAllPermissionRequestState extends Equatable {
  const ListAllPermissionRequestState();
  @override
  List<Object> get props => [];
}
class ListAllPermissionRequestInitial extends ListAllPermissionRequestState {}

class GetAllPermissionRequestLoadingState extends ListAllPermissionRequestState {}

class GetAllPermissionRequestLoadedState extends ListAllPermissionRequestState {
  final AllPermissionRequestModel listPermissionRequestModel;
  const GetAllPermissionRequestLoadedState({required this.listPermissionRequestModel});
}

class GetAllPermissionRequestErrorState extends ListAllPermissionRequestState {}