part of 'list_all_permission_request_bloc.dart';

class ListAllPermissionRequestEvent extends Equatable {
  const ListAllPermissionRequestEvent();

  @override
  List<Object> get props => [];
}

class GetAllPermissionRequestEvent extends ListAllPermissionRequestEvent{
  final String month;
  final String year;
  const GetAllPermissionRequestEvent({required this.month,required this.year});
}
