import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/request_permission_api/get_all_permission_request.dart';
import 'package:equatable/equatable.dart';
import '../../../../models/request_absent_model/list_all_permission_request_model.dart';
part 'list_all_permission_request_event.dart';
part 'list_all_permission_request_state.dart';

class ListAllPermissionRequestBloc extends Bloc<ListAllPermissionRequestEvent, ListAllPermissionRequestState> {
  final GetAllPermissionRequestApi getAllPermissionRequest;
  ListAllPermissionRequestBloc({required this.getAllPermissionRequest}) : super(ListAllPermissionRequestInitial()) {
    on<GetAllPermissionRequestEvent>((event, emit) async{
      emit(GetAllPermissionRequestLoadingState());
      try{
        var data = await getAllPermissionRequest.getAllPermissionRequestApi(month: event.month,year: event.year);
        emit(GetAllPermissionRequestLoadedState(listPermissionRequestModel: data));
      }
      catch(e){
        emit(GetAllPermissionRequestErrorState());
      }
    });
  }
}
