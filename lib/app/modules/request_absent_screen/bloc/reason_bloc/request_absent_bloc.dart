import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../models/request_absent_model/admission_model.dart';
import '../../../../service/api/request_permission_api/get_reason_api.dart';

part 'request_absent_event.dart';
part 'request_absent_state.dart';

class RequestReasonBloc extends Bloc<RequestAbsentEvent, RequestAbsentState> {
  final GetReasonApi getReasonApi;
  RequestReasonBloc({required this.getReasonApi}) : super(RequestAbsentInitial()) {
    on<RequestReasonEvent>((event, emit) async{
      emit(const RequestAbsentLoadingState(message: "Loading....."));
      try{
        var data = await getReasonApi.getReasonApi();
        emit(RequestReasonLoadedState(admissionModel: data));
      }
      catch(e){
        emit(const RequestAbsentErrorState(error: "Error Data"));
      }
    });
  }
}

