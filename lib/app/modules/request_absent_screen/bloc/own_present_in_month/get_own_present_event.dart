part of 'get_own_present_bloc.dart';

class GetOwnPresentEvent extends Equatable {
  const GetOwnPresentEvent();

  @override
  List<Object> get props => [];
}
class GetOwnPresentInMonthEvent extends GetOwnPresentEvent{
  final String month;
  final String year;
  final String type;
  const GetOwnPresentInMonthEvent({required this.month,required this.year,required this.type});
}