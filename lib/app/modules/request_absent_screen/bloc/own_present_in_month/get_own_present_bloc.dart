import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../models/request_absent_model/own_present_in_month_model.dart';
import '../../../../service/api/request_permission_api/get_own_present_in_month.dart';
part 'get_own_present_event.dart';
part 'get_own_present_state.dart';

class GetOwnPresentBloc extends Bloc<GetOwnPresentEvent, GetOwnPresentState> {
  final GetOwnPresentApi getOwnPresentApi;
  GetOwnPresentBloc({required this.getOwnPresentApi}) : super(GetOwnPresentInitial()) {
    on<GetOwnPresentInMonthEvent>((event, emit) async{
      emit(GetOwnPresentLoadingState());
      try{
        var data = await getOwnPresentApi.getOwnPresentApi(month: event.month,year: event.year,type:event.type);
        emit(GetOwnPresentLoadedState(ownPrensentInMonthModel: data));
      }
      catch(e){
        emit(GetOwnPresentErrorState());
      }
    });
  }
}
