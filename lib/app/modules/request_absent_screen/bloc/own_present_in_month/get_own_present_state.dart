part of 'get_own_present_bloc.dart';

class GetOwnPresentState extends Equatable {
  const GetOwnPresentState();
  
  @override
  List<Object> get props => [];
}

class GetOwnPresentInitial extends GetOwnPresentState {}

class GetOwnPresentLoadingState extends GetOwnPresentState {}

class GetOwnPresentLoadedState extends GetOwnPresentState {
  final OwnPrensentInMonthModel ownPrensentInMonthModel;
  const GetOwnPresentLoadedState({required this.ownPrensentInMonthModel});
}

class GetOwnPresentErrorState extends GetOwnPresentState {}
