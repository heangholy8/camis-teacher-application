
import 'package:easy_localization/easy_localization.dart';

checkMonth( int month) {
  if (month == 01) {
    return "JANUARY".tr();
  } else if (month == 02) {
    return "FEBRUARY".tr();
  }else if (month == 03) {
    return "MARCH".tr();
  }else if (month == 04) {
    return "APRIL".tr();
  }else if (month == 05) {
    return "MAY".tr();
  }else if (month == 06) {
    return "JUNE".tr();
  }else if (month == 07) {
    return "JULY".tr();
  }else if (month == 08) {
    return "AUGUST".tr();
  }else if (month == 09) {
    return "SEPTEMBER".tr();
  }else if (month == 10) {
    return "OCTOBER".tr();
  }else if (month == 11) {
    return "NOVEMBER".tr();
  }else if (month == 12) {
    return "DECEMBER".tr();
  }
}
