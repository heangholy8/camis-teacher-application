import 'dart:io';
import 'package:audio_waveforms/audio_waveforms.dart';
import 'package:camis_teacher_application/widget/button_widget/button_icon_right_with_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import '../../../../widget/button_widget/button_widget.dart';
import '../../../../widget/schedule_empty.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../models/request_absent_model/list_all_permission_request_model.dart';
import '../../../service/api/request_permission_api/delect_att_request.dart';
import '../../../service/api/request_permission_api/post_permission.dart';
import '../../attendance_schedule_screen/state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../bloc/list_permission_request/list_all_permission_request_bloc.dart';
import '../bloc/own_present_in_month/get_own_present_bloc.dart';
import '../bloc/reason_bloc/request_absent_bloc.dart';
class AbsentCreate extends StatefulWidget {
  DataAllPermission? listData;
  List<String> listStudentName;
  List<String> listStudentId; 
  List<String> listDate; 
  String admissionId;
  String admissionName;
  String teachername;
  List<String> listscheduleId;
  List<String> listscheduleName;
  String? year;
  String? month;
  int step;
  String displayDate;
  AbsentCreate({Key? key, 
  required this.listDate,
  this.listData,
  this.month,
  this.year,
  required this.teachername,
  required this.admissionId,
  required this.listStudentName,
  required this.listscheduleId,
  required this.listscheduleName,
  required this.listStudentId,
  required this.admissionName,
  required this.step,
  required this.displayDate 
  }) : super(key: key);

  @override
  State<AbsentCreate> createState() => _AbsentCreateState();
}

class _AbsentCreateState extends State<AbsentCreate> {
  bool isLoading = false;
  String duration = "";
  List<File> listImage = [];
  late final RecorderController recorderController;
  PlayerController playerController = PlayerController();
  String? path;
  String? musicFile;
  bool isRecording = false;
  bool isRecordingCompleted = false;
  bool isLoadingRecord = true;
  String playDuration = "0";
  List<double> waveformData = [];
  late Directory appDirectory;
  bool isplay = false;
  File? voice;
  bool viewImage = false;
  int viewImageType = 1; // 1 network / 2 file
  String imageViewnetwork = "";
  File? imageViewfile;
  //-------------------------------
  final ImagePicker _picker = ImagePicker();
  var _image;
  XFile? file;

  List<String>? listStudentNameFirst = [];
  List<String>? listStudentIdFirst = []; 
  List<String>? listDateFirst = []; 
  String? admissionIdFirst = "";
  String? admissionNameFirst="";
  List<String>? listscheduleIdFirst=[];
  List<String>? listscheduleNameFirst=[];
  List listImageNetwork = [];
  List listVoice = [];
  String voicePath = "";
  int indexImage = 0;
  String idAttachment = "";
  String? date;
  int _totalDay = 0;
  String durationRecord = "00:00";
  Future<void> _getVoiebyLink(String url) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';
    try {
      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = path!;
        file = File(filePath);
        voicePath = filePath;
        await file.writeAsBytes(bytes);
      }
      else
        filePath = 'Error code: '+response.statusCode.toString();
    }
    catch(ex){
      filePath = 'Can not fetch url';
    }
    Future.delayed(Duration.zero,() async {
      waveformData = await playerController.extractWaveformData(
      path: filePath,
      noOfSamples: 100,);
      await playerController.preparePlayer(
      path: filePath,
      shouldExtractWaveform: true,
      noOfSamples: 100,
      volume: 10.0);
      setState((){
        playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
      });
    },);
  }

  Future<void> _getVoie(String path) async {
    waveformData = await playerController.extractWaveformData(
    path: path,
    noOfSamples: 100,);
    await playerController.preparePlayer(
    path: path,
    shouldExtractWaveform: true,
    noOfSamples: 100,
    volume: 10.0);
    setState((){
      playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
    });
  }
  void _imgFromCamera() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile!.path);
      listImage.add(_image);
    });
  }
  Future<void> getMediaFromDevice() async {
    // final listMedia = await ImagesPicker.pick(
    //   count: 10,
    //   gif: true,
    //   pickType: PickType.all,
    //   language: Language.System,
    //   maxTime: 3600,
    //   quality: 0.8,
    // );
    List<XFile>? listMedia = await _picker.pickMultiImage();
    if (listMedia.isNotEmpty) {
      for (var item in listMedia) {
        setState(() {
          listImage.add(File(item.path));
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.of<RequestReasonBloc>(context).add(const RequestReasonEvent());
    setState(() {
      if(widget.listData!=null){
        if(widget.listData!.file!.isNotEmpty){
          setState(() {
            listImageNetwork = widget.listData!.file!.where((element) {return element.fileType!.contains("image");}).toList();
            listVoice = widget.listData!.file!.where((element) {return element.fileType!.contains("audio");}).toList();
            if(listVoice.isNotEmpty){
              _getVoiebyLink(listVoice[0].fileShow.toString());
              //isRecordingCompleted = true;
            }
          });
        }
        if(widget.listData!.schedules!.isNotEmpty){
          setState(() {
            BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date:widget.listData!.startDate.toString(),));
          });
        }
      }
    });
    _getDir();
    _initialiseControllers();
  }
    void _getDir() async {
    appDirectory = await getApplicationDocumentsDirectory();
    path = "${appDirectory.path}/recording.m4a";
    isLoading = false;
    setState(() {
      listImage = [];
    });
  }
  void _initialiseControllers() {
    recorderController = RecorderController()
      ..androidEncoder = AndroidEncoder.aac
      ..androidOutputFormat = AndroidOutputFormat.mpeg4
      ..iosEncoder = IosEncoder.kAudioFormatMPEG4AAC
      ..sampleRate = 44100;
  }
  void _pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      musicFile = result.files.single.path;
      setState(() {});
    } else {
      debugPrint("File not picked");
    }
  }
  @override
  void dispose() {
    recorderController.dispose();
    playerController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          margin: const EdgeInsets.only(top: 50),
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              )),
          child: Stack(
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: const Icon(
                              Icons.keyboard_arrow_left_rounded,
                              size: 32,
                              color: Colorconstand.neutralDarkGrey,
                            )),
                        Expanded(
                         child: Padding(
                          padding:const EdgeInsets.only(right: 0),
                          child: Center(child: Text("PERMISSIONLETTER".tr(),
                            style: ThemsConstands.headline3_semibold_20,
                          )),
                        )),
                        // widget.listData == null?Container(width: 25,):
                        Container(
                          margin: const EdgeInsets.only(right: 0),
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: Colorconstand.neutralDarkGrey.withOpacity(0.3),
                              borderRadius:const BorderRadius.all(Radius.circular(15))),
                          child: Row(
                            children: [
                              const Icon(Icons.check_circle,color: Colorconstand.neutralDarkGrey,
                              ),
                              const SizedBox(width: 5,),
                              Text("ALREADY_REQUEST".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralDarkGrey),),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        child: Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    offset: const Offset(0, 0),
                                    color: const Color(0xff878787).withOpacity(0.5),
                                    blurRadius: 8)
                                ],
                                color: Colorconstand.neutralWhite,
                                borderRadius:const BorderRadius.all(Radius.circular(9))),
                              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 30, top: 20),
                              child: Column(
                                children: [
                                  MaterialButton(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed:(){},
                                    child: Container(
                                      margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 22),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                const Icon(Icons.account_circle_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                const SizedBox(width: 5,),
                                                Text("NAME".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child:  Container( alignment: Alignment.centerRight,child: Text( 
                                             widget.teachername,style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),))
                                                
                                          ),
                                          const SizedBox(width: 5,),
                                          // widget.listData==null?Container():
                                          // SvgPicture.asset(ImageAssets.edit)
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    height: 0,
                                    color: Colorconstand.neutralGrey,
                                  ),
                                  MaterialButton(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed:
                                    widget.listData==null?null:
                                    widget.listData!.schedules!.isNotEmpty?null: 
                                    (){
                                      setState(() {
                                        showModalBottomSheet(
                                          enableDrag: false,
                                          isDismissible: false,
                                          backgroundColor: Colors.transparent,
                                          isScrollControlled: true,
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(16),
                                                topRight: Radius.circular(16)),
                                          ),
                                          context: context,
                                          builder: (context) {
                                            return dateSelect(context,translate);
                                          });
                                      },);
                                    },
                                    child: Container(
                                      height: 80,
                                      margin:const EdgeInsets.symmetric(horizontal: 12),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                const Icon(Icons.date_range_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                const SizedBox(width: 5,),
                                                Text("PERMISSIONDATE".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                              ],
                                            ),
                                          ),
                                          widget.listDate.isNotEmpty?
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                Text(widget.listDate[0].toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                                widget.listDate[1] ==  widget.listDate[0]?Container():Container(margin:const EdgeInsets.only(right: 28),child: Text("TO".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),)),
                                                widget.listDate[1] ==  widget.listDate[0]?Container():Text( widget.listDate[1].toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                              ],
                                            ),
                                          ):Expanded(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                Text( widget.listData!.startDate.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                                widget.listData!.startDate.toString() ==  widget.listData!.endDate.toString()?Container():Container(margin:const EdgeInsets.only(right: 28),child:Text("TO".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),)),
                                                widget.listData!.startDate.toString() ==  widget.listData!.endDate.toString()?Container():Text(widget.listData!.endDate.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(width: 5,),
                                          widget.listData==null?
                                          Container():widget.listData!.schedules!.isNotEmpty?Container():
                                          SvgPicture.asset(ImageAssets.edit)
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    height: 0,
                                    color: Colorconstand.neutralGrey,
                                  ),
                                  widget.listscheduleName.isEmpty?Container():
                                  MaterialButton(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed:
                                    widget.listData==null?null: 
                                    (){
                                      setState(() {
                                        showModalBottomSheet(
                                          enableDrag: false,
                                          isDismissible: false,
                                          backgroundColor: Colors.transparent,
                                          isScrollControlled: true,
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(16),
                                                topRight: Radius.circular(16)),
                                          ),
                                          context: context,
                                          builder: (context) {
                                            return scheduleSelect(context,translate,1);
                                          });
                                      },);
                                    },
                                    child: Container(
                                      margin:const EdgeInsets.symmetric(horizontal: 12),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                const Icon(Icons.access_time,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                const SizedBox(width: 5,),
                                                Text("PERMISSION_TIME".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              padding:const EdgeInsets.all(0),
                                              alignment: Alignment.centerRight,
                                              margin:const EdgeInsets.only(top: 35 ),
                                              child: ListView.builder(
                                                shrinkWrap: true,
                                                physics:const NeverScrollableScrollPhysics(),
                                                itemCount: widget.listscheduleName.length,
                                                itemBuilder: (context, indexSchedule){
                                                  return Container( alignment: Alignment.centerRight,child: Text( widget.listscheduleName[indexSchedule],style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),));
                                                }
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 5,),
                                          widget.listData==null?Container():
                                          SvgPicture.asset(ImageAssets.edit)
                                        ],
                                      ),
                                    ),
                                  ),
                                  widget.listData == null?Container():
                                  widget.listData!.schedules!.isEmpty?Container()
                                  :widget.listscheduleName.isNotEmpty?Container():
                                  MaterialButton(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed: 
                                    widget.listData==null?null:
                                    (){
                                      setState(() {
                                        showModalBottomSheet(
                                          enableDrag: false,
                                          isDismissible: false,
                                          backgroundColor: Colors.transparent,
                                          isScrollControlled: true,
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(16),
                                                topRight: Radius.circular(16)),
                                          ),
                                          context: context,
                                          builder: (context) {
                                            return scheduleSelect(context,translate,1);
                                          });
                                      },);
                                    },
                                    child: Container(
                                      margin:const EdgeInsets.symmetric(horizontal: 12),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                const Icon(Icons.access_time,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                const SizedBox(width: 5,),
                                                Text("PERMISSION_TIME".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              padding:const EdgeInsets.all(0),
                                              alignment: Alignment.centerRight,
                                              margin:const EdgeInsets.only(top: 35 ),
                                              child: ListView.builder(
                                                shrinkWrap: true,
                                                physics:const NeverScrollableScrollPhysics(),
                                                itemCount: widget.listData!.schedules!.length,
                                                itemBuilder: (context, indexScheduledata){
                                                  print("fsadf${widget.listData!.schedules!.length}");
                                                  return Container( alignment: Alignment.centerRight,child: Text("${widget.listData!.schedules![indexScheduledata].startTime}-${widget.listData!.schedules![indexScheduledata].endTime}",style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),));
                                                }
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 5,),
                                          widget.listData==null?Container():SvgPicture.asset(ImageAssets.edit)
                                        ],
                                      ),
                                    ),
                                  ),
                                  widget.listscheduleName.isEmpty?
                                  Container(): const Divider(
                                    height: 0,
                                    color: Colorconstand.neutralGrey,
                                  ),
                                  MaterialButton(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed: 
                                    widget.listData==null?null:
                                    (){
                                      setState(
                                        () {
                                          showModalBottomSheet(
                                            enableDrag: false,
                                            isDismissible: false,
                                            backgroundColor: Colors.transparent,
                                            isScrollControlled: true,
                                            shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(16),
                                                  topRight: Radius.circular(16)),
                                            ),
                                            context: context,
                                            builder: (context) {
                                              return reasonWidget(context,translate);
                                          });
                                        }
                                      );
                                    },
                                    child: Container(
                                      height: 80,
                                      margin:const EdgeInsets.symmetric(horizontal: 12),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                const Icon(Icons.message_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                const SizedBox(width: 5,),
                                                Text("REASON".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                              ],
                                            ),
                                          ),
                                         Expanded(child: Text(
                                         widget.admissionName !=""?
                                          widget.admissionName.toString()
                                          :translate =="km"?widget.listData!.admissionTypeName.toString():widget.listData!.admissionTypeNameEn.toString(),
                                          style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.end,overflow: TextOverflow.ellipsis,)),
                                          const SizedBox(width: 5,),
                                          widget.listData==null?Container():SvgPicture.asset(ImageAssets.edit)
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                              decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                offset: const Offset(0, 0),
                                color: const Color(0xff878787).withOpacity(0.5),
                                blurRadius: 8)
                              ],
                              color: Colorconstand.neutralWhite,
                              borderRadius:const BorderRadius.all(Radius.circular(9))),
                              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 30, top: 20),
                              child: Column(children: [
                                 Center(
                                    child: Text(
                                  "ATTACH_REFERENCES".tr(),
                                  style: ThemsConstands.headline3_semibold_20,
                                )),
                                const SizedBox(
                                  height: 20,
                                ),
                                Center(
                                    child: Text(
                                  "EXAMPLE_ATTACHMENT".tr(),
                                  style: ThemsConstands.headline_5_medium_16
                                      .copyWith(color: const Color(0xff9d9d9d)),
                                )),
                                const SizedBox(
                                  height: 20,
                                ),
                                Center(
                                  child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            InkWell(
                                              onTap: _imgFromCamera,
                                              child: Row(
                                                children: [
                                                  SvgPicture.asset(ImageAssets.CAMERA_icon),
                                                  listImage.isNotEmpty || listImageNetwork.isNotEmpty?Container():Container(
                                                    margin:const EdgeInsets.only(left: 5),
                                                    child: Text("TAKE_PHOTO".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            InkWell(
                                              onTap: getMediaFromDevice,
                                              child: Row(
                                                children: [
                                                  SvgPicture.asset(ImageAssets.gallary_add),
                                                  listImage.isNotEmpty || listImageNetwork.isNotEmpty?Container():Container(
                                                    margin:const EdgeInsets.only(left: 5),
                                                    child:Text("ATTACH_PICTURE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),
                                                    )
                                                  ),
                                                ],
                                              ),
                                            ),
                                            listImage.isEmpty && listImageNetwork.isEmpty?Container():Container(
                                               width: (listImage.length+listImageNetwork.length)>3?MediaQuery.of(context).size.width-160:(63*(listImage.length+listImageNetwork.length).toDouble()),
                                               margin: const EdgeInsets.symmetric(horizontal: 10),
                                                  height: 50,
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.horizontal,
                                                child: Row(
                                                  children: [
                                                    ListView.builder(
                                                      scrollDirection: Axis.horizontal,
                                                      shrinkWrap: true,
                                                      padding:const EdgeInsets.all(0),
                                                      physics:const NeverScrollableScrollPhysics(),
                                                      itemCount:listImageNetwork.length,
                                                      itemBuilder: (context, indexNet) {
                                                        return GestureDetector(
                                                          onTap: (){
                                                            setState(() {
                                                              viewImageType = 1;
                                                              imageViewnetwork = listImageNetwork[indexNet].fileShow.toString();
                                                              viewImage = true;
                                                              idAttachment = listImageNetwork[indexNet].id.toString();
                                                              indexImage = indexNet;
                                                            },);
                                                          },
                                                          child: Padding(
                                                            padding: const EdgeInsets.symmetric(horizontal: 5),
                                                            child:ClipRRect(
                                                              borderRadius:BorderRadius.circular(10),
                                                              child: Image.network(listImageNetwork[indexNet].fileShow.toString(),
                                                                fit: BoxFit.cover,
                                                                height: 48,
                                                                width: 48,
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                    ListView.builder(
                                                      scrollDirection: Axis.horizontal,
                                                      shrinkWrap: true,
                                                      padding:const EdgeInsets.all(0),
                                                      physics:const NeverScrollableScrollPhysics(),
                                                      itemCount:listImage.length,
                                                      itemBuilder: (context, index) {
                                                        return GestureDetector(
                                                          onTap: (){
                                                            setState(() {
                                                              viewImageType = 2;
                                                              imageViewfile = listImage[index];
                                                              indexImage = index;
                                                              viewImage = true;
                                                            },);
                                                          },
                                                          child: Padding(
                                                            padding: const EdgeInsets.symmetric(horizontal: 5),
                                                            child:ClipRRect(
                                                              borderRadius:BorderRadius.circular(10),
                                                              child: Image.file(listImage[index],
                                                                fit: BoxFit.cover,
                                                                height: 48,
                                                                width: 48,
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                ),
                                const Divider(
                                  thickness: 0.5,
                                  color: Color.fromRGBO(0, 0, 0, 0.1),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                             // =========  voice from network =======
                                listVoice.isNotEmpty?
                                  Container(
                                    width: MediaQuery.of(context).size.width/1.5,
                                    decoration: const BoxDecoration(color: Colorconstand.neutralGrey,borderRadius: BorderRadius.all(Radius.circular(10))),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                      IconButton(onPressed:() {
                                        !isplay? playerController.startPlayer(finishMode: FinishMode.pause):playerController.pausePlayer();
                                        playerController.onPlayerStateChanged.listen((event) {
                                           if(event.isPlaying){
                                              setState(() {
                                                isplay =true;
                                              },);
                                           }else if(event.isPaused){
                                              setState(() {
                                                isplay =false;
                                              },);
                                           }
                                         });
                                        playerController.onCompletion.listen((event) {
                                          setState(() {
                                            playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
                                            playerController.setRefresh(false);
                                          });
                                        });
                                        playerController.onCurrentDurationChanged.listen((duration) {
                                          setState(() {
                                            playDuration = (playerController.maxDuration~/1000 - (duration~/1000).toInt()).toMMSS();
                                          },);
                                        });
                                        
                                      } , icon: Icon(isplay? Icons.pause_rounded:Icons.play_arrow_rounded,color: Colorconstand.primaryColor,size: 30,)),
                                        Expanded(
                                          flex: 2,
                                          child: Text(playDuration,style: ThemsConstands.headline_4_semibold_18,)),
                                        Expanded(
                                          flex: 4,
                                          child: AudioFileWaveforms(
                                            margin: const EdgeInsets.symmetric(horizontal: 5),
                                            size: Size(MediaQuery.of(context).size.width/1.5,50),
                                            playerController: playerController,
                                            waveformType: WaveformType.long,
                                            waveformData: waveformData,
                                            animationCurve : Curves.bounceInOut,
                                            enableSeekGesture: false,
                                            playerWaveStyle: const PlayerWaveStyle(
                                                  fixedWaveColor: Colorconstand.mainColorSecondary,
                                                  liveWaveColor: Colorconstand.primaryColor,
                                                  showSeekLine: false
                                              ),
                                            ),
                                        ),
                                      IconButton(onPressed:(){
                                        setState((){
                                          DelectPermissionApi().deleteRequestAttApi(idRequestAtt: listVoice[0].id.toString()).then((value){
                                            setState(() {
                                                listVoice=[];
                                                BlocProvider.of<ListAllPermissionRequestBloc>(context).add(GetAllPermissionRequestEvent(month: widget.month.toString(),year: widget.year.toString()));
                                            },);
                                          },);
                                          playerController.dispose();
                                        },);
                                      } , icon: const Icon(Icons.close,color: Colorconstand.primaryColor,size: 30,)),
                                      ],
                                    ),
                                  ):Container(),
                            // =========  end voice from network =======  
                            //  
                            // =========  voice from record =======
                                isRecording? Container(
                                  width: MediaQuery.of(context).size.width/1.5,
                                  decoration: const BoxDecoration(color: Colorconstand.neutralGrey,borderRadius: BorderRadius.all(Radius.circular(10))),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      IconButton(onPressed:(){
                                        _startOrStopRecording(waveformData);
                                      } , icon: const Icon(Icons.stop_rounded,color: Colorconstand.alertsDecline,size: 30,)),
                                      Text(durationRecord,style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.alertsDecline),),
                                      Expanded(
                                        child: AudioWaveforms(
                                            enableGesture: true,
                                            size: Size( MediaQuery.of(context).size.width/2+10,50),
                                            recorderController: recorderController,
                                            waveStyle:  const WaveStyle(
                                              waveColor: Colorconstand.alertsDecline,
                                              extendWaveform: true,
                                              showMiddleLine: false,
                                            ),
                                            margin: const EdgeInsets.only(
                                                left: 10),
                                          ),
                                      ),
                                      const Icon(Icons.mic_sharp,color: Colorconstand.alertsDecline,size: 30,)
                                    ],
                                  ),
                                )
                                :isRecordingCompleted && listVoice.isEmpty?
                                  Container(
                                    width: MediaQuery.of(context).size.width/1.5,
                                    decoration: const BoxDecoration(color: Colorconstand.neutralGrey,borderRadius: BorderRadius.all(Radius.circular(10))),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                      IconButton(onPressed:() {
                                        !isplay? playerController.startPlayer(finishMode: FinishMode.pause):playerController.pausePlayer();
                                        playerController.onPlayerStateChanged.listen((event) {
                                           if(event.isPlaying){
                                              setState(() {
                                                isplay =true;
                                              },);
                                           }else if(event.isPaused){
                                              setState(() {
                                                isplay =false;
                                              },);
                                           }
                                         });
                                        playerController.onCompletion.listen((event) {
                                          setState(() {
                                            playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
                                            playerController.setRefresh(false);
                                          });
                                        });
                                        playerController.onCurrentDurationChanged.listen((duration) {
                                          setState(() {
                                            playDuration = (playerController.maxDuration~/1000 - (duration~/1000).toInt()).toMMSS();
                                          },);
                                        });
                                        
                                        } , icon: Icon(isplay? Icons.pause_rounded:Icons.play_arrow_rounded,color: Colorconstand.primaryColor,size: 30,)),
                                        Expanded(
                                          flex: 2,
                                          child: Text(playDuration,style: ThemsConstands.headline_4_semibold_18,)),
                                        Expanded(
                                          flex: 4,
                                          child: AudioFileWaveforms(
                                            margin: const EdgeInsets.symmetric(horizontal: 5),
                                            size: Size(MediaQuery.of(context).size.width/1.5,50),
                                            playerController: playerController,
                                            waveformType: WaveformType.long,
                                            waveformData: waveformData,
                                            animationCurve : Curves.bounceInOut,
                                            enableSeekGesture: false,
                                            playerWaveStyle: const PlayerWaveStyle(
                                                  fixedWaveColor: Colorconstand.mainColorSecondary,
                                                  liveWaveColor: Colorconstand.primaryColor,
                                                  showSeekLine: false
                                              ),
                                            ),
                                        ),
                                      IconButton(onPressed:(){
                                        setState(() {
                                          isRecording=false;
                                          isRecordingCompleted = false;
                                          voice = null;
                                          playerController.dispose();
                                        },);
                                      } , icon: const Icon(Icons.close,color: Colorconstand.primaryColor,size: 30,)),
                                      ],
                                    ),
                                  )
                              // ===============  End voice from record ===============
                                : listVoice.isEmpty?InkWell(
                                  onTap: (){
                                    _startOrStopRecording(waveformData);
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(ImageAssets.voice_cricle),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        "ATTACH_VOICE".tr(),
                                        style: ThemsConstands.headline_5_medium_16
                                            .copyWith(
                                                color:
                                                    Colorconstand.primaryColor),
                                      )
                                    ],
                                  ),
                                ):Container()
                              ]),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                offset: const Offset(0, 0),
                                color: const Color(0xff878787).withOpacity(0.5),
                                blurRadius: 8)
                          ],
                          color: Colorconstand.neutralWhite,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(8),
                              topRight: Radius.circular(8))),
                      padding: const EdgeInsets.only(bottom: 30),
                      child: Column(
                        children: [
                         widget.admissionId!=""||widget.listscheduleId.isNotEmpty||widget.listStudentId.isNotEmpty||listImage.isNotEmpty||widget.listDate.isNotEmpty||voice !=null
                         ? 
                         Container(
                            margin: const EdgeInsets.only(
                                left: 30, right: 30, top: 20, bottom: 10),
                            child: ButtonIconRightWithTitle(
                              heightButton: 48,
                              onTap:!isRecording? () {
                                RequestPermssionApi permissionApi = RequestPermssionApi();
                      /// ============== Add request ======================
                                if(widget.listData==null){
                                  setState(() {
                                    isLoading = true;
                                  },);
                                  List<File> listFile = [];
                                  if(listImage.isNotEmpty){
                                    for (var element in listImage) {
                                      listFile.add(element);
                                    }
                                  }
                                  if(voice!=null){
                                    listFile.add(voice!);
                                  }
                                  permissionApi.postRequestPermssionApi(
                                    startDate: widget.listDate[0], 
                                    endDate: widget.listDate[1], 
                                    requestType: widget.listscheduleId.isNotEmpty?"1":"2", 
                                    admissionId: widget.admissionId, 
                                    studentId: widget.listStudentId, 
                                    scheduleId: widget.listscheduleId, 
                                    listFile: listFile).then((value){
                                      setState(() {
                                        BlocProvider.of<GetOwnPresentBloc>(context).add(GetOwnPresentInMonthEvent(month: widget.month.toString(),year: widget.year.toString(),type: "2"));
                                        BlocProvider.of<ListAllPermissionRequestBloc>(context).add(GetAllPermissionRequestEvent(month: widget.month.toString(),year: widget.year.toString()));
                                        print("sfsadf${widget.month}  ${widget.month}");
                                      },);
                                      if(widget.step==3){
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      }else{
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      }
                                      setState(() {
                                        isLoading=false;
                                        Future.delayed(const Duration(milliseconds: 200),(){
                                            setState(() {
                                              widget.listStudentName = [];
                                              widget.listStudentId= []; 
                                              widget.listDate= []; 
                                              widget.admissionId= "";
                                              widget.admissionName="";
                                              widget.listscheduleId = [];
                                              widget.listscheduleName = [];
                                            },);
                                          });
                                      },);
                                    }
                                  );
                             }
                        /// ============== update request ======================
                                else{
                                  if(widget.listDate.isEmpty){
                                      widget.listDate = [widget.listData!.startDate!,widget.listData!.endDate!];
                                  }
                                  if(widget.listscheduleId.isEmpty){
                                    widget.listData!.schedules!.forEach((element) {
                                        widget.listscheduleId.add(element.scheduleId.toString());
                                    });
                                  }
                                  setState(() {
                                    isLoading = true;
                                  },);
                                  List<File> listFile = [];
                                  if(listImage.isNotEmpty){
                                    for (var element in listImage) {
                                      listFile.add(element);
                                    }
                                  }
                                  if(voice!=null){
                                    listFile.add(voice!);
                                  }
                                  permissionApi.updateRequestPermssionApi(
                                    id: widget.listData!.id.toString(),
                                    startDate: widget.listDate[0], 
                                    endDate: widget.listDate[1], 
                                    requestType: widget.listscheduleId.isNotEmpty?"1":"2", 
                                    admissionId: widget.admissionId == ""?widget.listData!.admissionTypes.toString():widget.admissionId, 
                                    studentId: widget.listStudentId, 
                                    scheduleId: widget.listscheduleId, 
                                    listFile: listFile).then((value){
                                      setState(() {
                                        BlocProvider.of<GetOwnPresentBloc>(context).add(GetOwnPresentInMonthEvent(month: widget.month.toString(),year: widget.year.toString(),type: "2"));
                                        BlocProvider.of<ListAllPermissionRequestBloc>(context).add(GetAllPermissionRequestEvent(month: widget.month.toString(),year: widget.year.toString()));
                                      },);
                                        Navigator.pop(context);
                                        setState(() {
                                          isLoading=false;
                                          Future.delayed(const Duration(milliseconds: 200),(){
                                            setState(() {
                                              widget.listStudentName = [];
                                              widget.listStudentId= []; 
                                              widget.listDate= []; 
                                              widget.admissionId= "";
                                              widget.admissionName="";
                                              widget.listscheduleId = [];
                                              widget.listscheduleName = [];
                                            },);
                                          });
                                        },);
                                    }
                                  );
                                }
                              }:null,
                              title: "SENT_TO_SCHOOL".tr(),
                              textStyleButton:
                                  ThemsConstands.headline_5_semibold_16.copyWith(
                                color: Colorconstand.neutralWhite,
                              ),
                              buttonColor: Colorconstand.primaryColor,
                              radiusButton: 8,
                              panddinHorButton: 20,
                              panddingVerButton: 6,
                              iconColor: Colorconstand.neutralWhite,
                              iconImageSvg: ImageAssets.send_outline_icon,
                            ),
                          )
                         :Container(height: 20,),
                          const SizedBox(height: 5),
                          InkWell(
                              onTap:!isRecording? () {
                                if(widget.listData !=null){
                                  print("dsfas${widget.listData!.id.toString()}");
                                  DelectPermissionApi().deleteRequestApi(idRequest: widget.listData!.id.toString()).then((value) {
                                    setState(() {
                                      BlocProvider.of<GetOwnPresentBloc>(context).add(GetOwnPresentInMonthEvent(month: widget.month.toString(),year: widget.year.toString(),type: "2"));
                                      BlocProvider.of<ListAllPermissionRequestBloc>(context).add(GetAllPermissionRequestEvent(month: widget.month.toString(),year: widget.year.toString()));
                                        Future.delayed(const Duration(milliseconds: 10),(){
                                          Navigator.pop(context);
                                        });
                                    },);
                                  });
                                }
                                else{
                                 if(widget.step==3){
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  }else{
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  }
                                }
                                Future.delayed(const Duration(milliseconds: 200),(){
                                  setState(() {
                                    widget.listStudentName = [];
                                    widget.listStudentId= []; 
                                    widget.listDate= []; 
                                    widget.admissionId= "";
                                    widget.admissionName="";
                                    widget.listscheduleId = [];
                                    widget.listscheduleName = [];
                                  },);
                                });
                                
                              }:null,
                              child: Text(
                              widget.listData == null?
                               "CANCEL".tr()
                               :"DELETE_REQUEST".tr(),
                                style: ThemsConstands.headline6_medium_14
                                    .copyWith(
                                        color: Colorconstand.alertsDecline,
                                        decoration: TextDecoration.underline),
                              ))
                        ],
                      ),
                    ),
                  )
                ],
              ),
              isLoading == true?Positioned(
                top: 60,left: 0,right: 0,bottom: 0,
                child: Container(
                  color: Colorconstand.neutralGrey.withOpacity(0.8),
                  child:Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "SENTING_REUEST".tr(),
                        style: ThemsConstands.headline3_semibold_20
                            .copyWith(
                                color:
                                    Colorconstand.lightBlack),),
                      Container(
                        margin: const EdgeInsets.all(10),
                        height: 30,width: 30,
                      child: const CircularProgressIndicator(),),
                    ],
                  )),
                ),
              ):Container(height: 0,),
              viewImage == false?Container(height: 0,):AnimatedPositioned(
                duration:const Duration(milliseconds: 100),
                child: Container(
                  color: Colorconstand.lightBlack,
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(onPressed: (){
                            setState(() {
                              viewImage = false;
                            },);}, icon:const Icon(Icons.close_rounded,size: 32,color: Colorconstand.neutralWhite,)),
                            IconButton(onPressed: (){
                              if(viewImageType == 1){
                                DelectPermissionApi().deleteRequestAttApi(idRequestAtt: idAttachment).then((value){
                                setState(() {
                                    listImageNetwork.removeAt(indexImage);
                                    BlocProvider.of<ListAllPermissionRequestBloc>(context).add(GetAllPermissionRequestEvent(month: widget.month.toString(),year: widget.year.toString()));
                                },);
                              });
                              }
                              else{
                                setState(() {
                                  listImage.removeAt(indexImage);
                                },);
                              }
                              setState(() {
                                viewImage = false;
                              },);
                            }, icon:const Icon(Icons.delete_outline_rounded,size: 32,color: Colorconstand.neutralWhite,))
                          ],
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: viewImageType==1?Image.network(imageViewnetwork,fit: BoxFit.contain,):Image.file(imageViewfile!,fit: BoxFit.contain,),
                        ),
                      ),
                      Container(height: 60,)
                    ],
                  ),
                ), 
              )
            ],
          ),
        );
      },
    );
  }
  void _startOrStopRecording(List<double> waveformData) async {
    try {
      if (isRecording) {
        recorderController.reset();
        final path = await recorderController.stop(false);
        if (path != null) {
          isRecordingCompleted = true;
          debugPrint(path);
          debugPrint("Recorded file size: ${File(path).lengthSync()}");
          Future.delayed(Duration.zero,() {
            _getVoie(path);
          },);
          voice = File(path);
        }
      } else {
        await recorderController.record(path: path!);
        recorderController.onCurrentDuration.listen((event) {
          setState(() {
            durationRecord = event.toHHMMSS().substring(3);
          });
        },);
      }
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      setState(() {
        isRecording = !isRecording;
      });
    }
  }
  // // =========== Reason ============
  Widget reasonWidget(BuildContext context, String translate){
    return StatefulBuilder(builder: (context, setStateadmission) {
          return Container(
            margin:const EdgeInsets.only(top: 65),
            padding:const EdgeInsets.all(18),
            decoration:const BoxDecoration(
              color: Colorconstand.neutralSecondBackground,
               borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18),
                topRight: Radius.circular(18),
              ),
            ),
            child: SingleChildScrollView(
              child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment:CrossAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(
                            () {
                              widget.admissionId = admissionIdFirst!;
                              widget.admissionName = admissionNameFirst!;
                              Navigator.pop(context);
                            },
                          );
                        },
                        icon: const Icon(Icons.keyboard_arrow_left_rounded,size: 32,color: Colorconstand.neutralDarkGrey,)),
                     Expanded(child: Padding(
                      padding: const EdgeInsets.only(right: 60),
                      child: Center(child: Text(
                        "SELETE_ADMISSION".tr(),
                        style: ThemsConstands.headline3_medium_20_26height,
                      )),
                    ))
                  ],
                ),
                const Divider(
                  thickness: 0.5,
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                ),
                BlocBuilder<RequestReasonBloc,RequestAbsentState>(
                  builder: (context, state) {
                    if (state is RequestAbsentLoadingState) {
                      return const Center(
                        child: SizedBox(
                            height: 150,
                            width: 50,
                            child:CircularProgressIndicator()),
                      );
                    } else if (state is RequestReasonLoadedState) {
                      var data = state.admissionModel.data;
                      return ListView.builder(
                        physics:const NeverScrollableScrollPhysics(),
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        shrinkWrap: true,
                        itemCount: data!.length,
                        itemBuilder:(context, index) {
                          return Container(
                            margin:const EdgeInsets.symmetric(vertical: 10),
                            width: MediaQuery.of(context).size.width,
                            height: 55,
                            child: MaterialButton(
                              disabledColor:Colorconstand.mainColorUnderlayer,
                              color: Colorconstand.primaryColor,
                              shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(8))),
                              padding:const EdgeInsets.all(0),
                              onPressed: () {
                                setState(() {
                                  admissionIdFirst = data[index].id.toString();
                                  admissionNameFirst = translate == "km"?data[index].name.toString():data[index].nameEn.toString();
                                  widget.admissionId = data[index].id.toString();
                                  widget.admissionName = translate == "km"?data[index].name.toString():data[index].nameEn.toString();
                                },);
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                alignment:Alignment.center,
                                padding: const EdgeInsets.symmetric(vertical: 6,horizontal:20),
                                child: Text(
                                  translate =="km"? data[index].name.toString(): data[index].nameEn.toString(),
                                  style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),
                                  textAlign:TextAlign.center,
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    } else {
                      print("get admission error");
                      return const Center(
                        child: SizedBox(
                            height: 50,
                            width: 50,
                            child:CircularProgressIndicator()),
                      );
                    }
                  },
                ),
                const SizedBox(
                  height: 25,
                ),
              ],
            ),),
          );
        },);
  }
  // // ==========  Schedue  =================
  Widget scheduleSelect(BuildContext context, String translate,int stepDone){
    return StatefulBuilder(builder: (context, setStatechangeschedule) {
        return Container(
          margin:const EdgeInsets.only(top: 65),
          padding:const EdgeInsets.all(18),
          decoration:const BoxDecoration(
            color: Colorconstand.neutralSecondBackground,
             borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18),
                topRight: Radius.circular(18),
              ),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Row(mainAxisAlignment:MainAxisAlignment.spaceBetween,
                  crossAxisAlignment:CrossAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () {
                          setStatechangeschedule(
                            () {
                              setState(() {
                                widget.listscheduleId = listscheduleIdFirst!;
                                widget.listscheduleName = listscheduleNameFirst!;
                              });
                              Navigator.of(context).pop();   
                            },
                          );
                        },
                        icon: const Icon(Icons.keyboard_arrow_left_rounded,size: 32,color: Colorconstand.neutralDarkGrey,
                        )),
                     Expanded(
                        child: Padding(
                        padding: const EdgeInsets.only(right: 60),
                        child: Center(
                          child: Text("SELECTE_TIME".tr(),
                          style: ThemsConstands.headline3_medium_20_26height,
                      )),
                    ))
                  ],
                ),
                const Divider(
                  thickness: 0.5,
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                ),
                Text(
                  widget.displayDate.toString(),
                  style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.primaryColor),
                ),
                BlocBuilder<GetScheduleBloc,GetScheduleState>(
                  builder: (context, state) { 
                    if(state is GetScheduleLoading){
                      return Container(
                        margin:const EdgeInsets.symmetric(vertical: 15),
                        child: const Center(
                          child: SizedBox(height: 30, width: 30,
                            child: CircularProgressIndicator(),
                          ),
                        ),
                      );
                    }
                    else if(state is GetMyScheduleLoaded){
                      var dataSchedule = state.myScheduleModel!.data;
                      return dataSchedule!.isEmpty? DataEmptyWidget(
                        title: "NO_CLASS_TODAY".tr(),
                        description: "",
                      ): Container(
                        margin:const EdgeInsets.only(top: 10),
                        child: ListView.separated(
                        physics:const NeverScrollableScrollPhysics(),
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        shrinkWrap: true,
                        itemCount: dataSchedule.length,
                        itemBuilder:(context, index) {
                          return Row(
                            crossAxisAlignment:CrossAxisAlignment.center,
                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                            children: [
                              IconButton(
                                  onPressed: () {
                                    setStatechangeschedule(
                                      () {
                                        if(dataSchedule[index].isSelected == false){
                                          setState(() {
                                            if(listscheduleIdFirst!.isEmpty){
                                              listscheduleIdFirst = [];
                                              listscheduleNameFirst = [];
                                            }
                                          widget.listscheduleId.add(dataSchedule[index].scheduleId.toString());
                                          widget.listscheduleName.add("${dataSchedule[index].startTime.toString()}-${dataSchedule[index].endTime.toString()}");
                                          dataSchedule[index].isSelected = true;
                                          });
                                        }
                                        else{
                                          setState(() {
                                            if(listscheduleIdFirst!.isEmpty){
                                              listscheduleIdFirst = [];
                                              listscheduleNameFirst = [];
                                            }
                                          widget.listscheduleId.remove(dataSchedule[index].scheduleId.toString());
                                          widget.listscheduleName.remove("${dataSchedule[index].startTime.toString()}-${dataSchedule[index].endTime.toString()}");
                                          dataSchedule[index].isSelected = false;
                                          });
                                        }
                                      },
                                    );
                                  },
                                  icon: Icon( dataSchedule[index].isSelected
                                        ? Icons.check_box: Icons.check_box_outline_blank_rounded,
                                    size: 35,
                                    color:  dataSchedule[index].isSelected
                                        ? Colorconstand.primaryColor
                                        : Colorconstand.lightTrunks,
                                  )),
                              const SizedBox(width: 30,),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "${dataSchedule[index].startTime} - ${dataSchedule[index].endTime}",
                                    style: ThemsConstands.headline_5_medium_16,),
                                ),
                              ),
                              Align(
                              alignment: Alignment.centerRight, 
                              child: Text(
                              translate=="km"?dataSchedule[index].subjectName.toString():dataSchedule[index].subjectNameEn.toString(),
                              style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),
                              ),
                              )
                            ],
                          );
                        },
                        separatorBuilder:(BuildContext context,int index) {
                          return const Divider(
                            indent: 10,
                            endIndent: 10,
                            thickness: 0.5,
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                          );
                        },
                                                                                                                                          ),
                      );
                    }
                    else{
                      return const Center(
                        child: SizedBox(height: 50, width: 50,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                  },
                ),
                const Divider(
                  thickness: 0.5,
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                ),
                Container(
                  margin:const EdgeInsets.symmetric(horizontal: 30,vertical: 20),
                  child: Button_Custom(
                    hightButton: 48,
                    onPressed:widget.listscheduleId.isNotEmpty
                        ? () {
                            setState(
                              () {
                                listStudentIdFirst = widget.listStudentId;
                                listStudentNameFirst = widget.listStudentName;
                                listscheduleIdFirst = widget.listscheduleId;
                                listscheduleNameFirst = widget.listscheduleName;
                                listDateFirst = widget.listDate;
                                if(stepDone == 1){
                                  Navigator.of(context).pop();
                                }
                                else if(stepDone == 2){
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                }
                                else{
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                }
                              }
                            );
                          }
                        : null,
                        titlebuttonColor: Colorconstand.neutralWhite,
                    titleButton: "DONE".tr(),
                    buttonColor: Colorconstand.primaryColor,
                    radiusButton: 8,
                  ),
                ),
                const SizedBox(
                  height: 25,
                ),
              ],
            )),
        );
      },);                                                                                              
  }
  // // ==========  child  =================
  Widget dateSelect(BuildContext context, String translate,){
    return StatefulBuilder(builder: (context, setStatechangedate) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.all(18),
        decoration:const BoxDecoration(
          color: Colorconstand.neutralSecondBackground,
           borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18),
                topRight: Radius.circular(18),
              ),
        ),
        child: SingleChildScrollView(
          child: Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment:MainAxisAlignment.spaceBetween,
                      crossAxisAlignment:CrossAxisAlignment.center,
                      children: [
                        IconButton(
                            onPressed: () {
                              setStatechangedate(
                                () {
                                  widget.listDate = listDateFirst!;
                                  _totalDay = 0;
                                  Navigator.pop(context);
                                },
                              );
                            },
                            icon: const Icon(
                              Icons.keyboard_arrow_left_rounded,size: 32,color:Colorconstand.neutralDarkGrey,
                            )),
                         Expanded(
                            child: Padding(
                            padding:const EdgeInsets.only(right: 60),
                            child: Center(
                                child: Text(
                              "SELECTE_DAY_PERMISSION".tr(),
                              style: ThemsConstands.headline3_semibold_20,
                            )),
                          ))
                      ],
                    ),
                    const Divider(
                      thickness: 0.5,
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                    ),
                    SfDateRangePicker(
                      backgroundColor: Colors.transparent,
                      selectionMode:DateRangePickerSelectionMode.range,
                      headerStyle: DateRangePickerHeaderStyle(
                          textAlign: TextAlign.center,
                          textStyle: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.primaryColor)),
                      enablePastDates: false,
                      onSelectionChanged: (args) {
                        if (args.value is PickerDateRange) {
                          try {
                            final DateTime rangeStartDate = args.value.startDate;
                            final DateTime rangeEndDate = args.value.endDate ?? args.value.startDate;
                            setStatechangedate(
                              () {
                                _totalDay = rangeEndDate.difference(rangeStartDate).inDays;
                                _totalDay = _totalDay + 1;
                                var dataFormate = DateFormat("dd/MM/yyyy");
                                date = dataFormate.format(rangeStartDate);
                                var startDate = dataFormate.format(rangeStartDate);
                                var endDate = dataFormate.format(rangeEndDate);
                                widget.displayDate = DateFormat.yMMMMd(translate).format(rangeStartDate);
                                widget.listDate = [startDate,endDate];
                                print("listdate : ${widget.listDate.length} ");
                              
                              },
                            );
                          } catch (e) {
                            print("error date : $e");
                          }
                          print("total day: $_totalDay");
                        } else if (args.value is DateTime) {
                          final DateTime selectedDate = args.value;
                          setStatechangedate(
                            () {
                              _totalDay = args.value;
                            },
                          );
                          print("total day: ${_totalDay + 1}");
                        } else if (args.value is List<DateTime>) {
                          final List<DateTime> selectedDates = args.value;
                        } else {
                          final List<PickerDateRange> selectedRanges = args.value;
                        }
                      },
                    ),
                    const Divider(
                      thickness: 0.5,
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Button_Custom(
                            hightButton: 45,
                            buttonColor:Colorconstand.primaryColor,
                            radiusButton: 8,
                            titleButton: "ASK_BY_DAY".tr(),
                            titlebuttonColor:Colorconstand.neutralWhite,
                            onPressed: (){
                                setState(() {
                                  Navigator.of(context).pop();
                                  listDateFirst = widget.listDate;
                                  widget.listscheduleId = [];
                                  widget.listscheduleName = [];
                                  listStudentIdFirst = widget.listStudentId;
                                  listStudentNameFirst = widget.listStudentName;
                                  if(widget.listData != null){
                                    widget.listData!.schedules = [];
                                  }
                                });
                            },
                          ),
                        ),
                        widget.listData==null?Container(): widget.listData!.schedules!.isEmpty?Container():Expanded(
                          child: Button_Custom(
                            hightButton: 45,
                            buttonColor:Colorconstand.primaryColor,
                            radiusButton: 8,
                            titleButton: "ASK_BY_SCHDULE".tr(),
                            titlebuttonColor:Colorconstand.neutralWhite,
                            onPressed: (){
                              if(widget.listData!.schedules!.isEmpty || _totalDay > 1){
                                setState(() {
                                  Navigator.of(context).pop();
                                  listDateFirst = widget.listDate;
                                  widget.listscheduleId = [];
                                  widget.listscheduleName = [];
                                  listStudentIdFirst = widget.listStudentId;
                                  listStudentNameFirst = widget.listStudentName;
                                  if(widget.listData != null){
                                    widget.listData!.schedules = [];
                                  }
                                });
                              }
                              else{
                                setState(() {
                                  showModalBottomSheet(
                                    enableDrag: false,
                                    isDismissible: false,
                                    backgroundColor: Colors.transparent,
                                    isScrollControlled: true,
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(16),
                                          topRight: Radius.circular(16)),
                                    ),
                                    context: context,
                                    builder: (context) {
                                      return scheduleSelect(context,translate,1);
                                    });
                                });
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    )
                  ],
                ),
              ),
      );
    },);
  }
  // // ==========  date  =================

}
