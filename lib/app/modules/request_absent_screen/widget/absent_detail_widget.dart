import 'dart:io';

import 'package:audio_waveforms/audio_waveforms.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import '../../../models/request_absent_model/list_all_permission_request_model.dart';
import '../../time_line/widget/view_image.dart';

class AbsentDetail extends StatefulWidget {
  final DataAllPermission listData;
  AbsentDetail({Key? key, required this.listData})
      : super(key: key);

  @override
  State<AbsentDetail> createState() => _AbsentDetailState();
}

class _AbsentDetailState extends State<AbsentDetail> {
  final _controller = PageController(viewportFraction: 0.85);
  List<double> waveformData = [];
  String playDuration = "0:00";
  bool isplay = false;
  bool getVoice = true;
  String? path;
  String voicePath = "";
  List listImage = [];
  List listVoice = [];
  late Directory appDirectory;
  late final RecorderController recorderController;
  PlayerController playerController = PlayerController();

  Future<void> _getVoiebyLink(String url) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';
    try {
      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = path!;
        file = File(filePath);
        voicePath = filePath;
        await file.writeAsBytes(bytes);
      }
      else
        filePath = 'Error code: '+response.statusCode.toString();
    }
    catch(ex){
      filePath = 'Can not fetch url';
    }
    Future.delayed(Duration.zero,() async {
      waveformData = await playerController.extractWaveformData(
      path: filePath,
      noOfSamples: 100,);
      await playerController.preparePlayer(
      path: filePath,
      shouldExtractWaveform: true,
      noOfSamples: 100,
      volume: 10.0);
      setState((){
        playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
        getVoice = false;
        isplay = false;
      });
    },);
  }
  void _getDir() async {
    
    appDirectory = await getApplicationDocumentsDirectory();
    path = "${appDirectory.path}/recording.m4a";
    setState(() {});
  }
  
  @override
  void initState() {
    setState(() {
      listImage = widget.listData.file!.where((element) {return element.fileType!.contains("image");}).toList();
      listVoice = widget.listData.file!.where((element) {return element.fileType!.contains("audio");}).toList();
      Future.delayed(const Duration(milliseconds: 100),(){
        _getVoiebyLink(listVoice[0].fileShow.toString());
      });
    });
    _getDir();
    super.initState();
  }
  @override
  void dispose() {
    playerController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          margin:const EdgeInsets.only(top: 65),
          padding:const EdgeInsets.all(18),
          decoration: BoxDecoration(
            color: Colorconstand.neutralSecondBackground,
            borderRadius: BorderRadius.circular(18),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 18),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: const Icon(
                            Icons.close_rounded,
                            size: 32,
                            color: Colorconstand.neutralDarkGrey,
                          )),
                        Expanded(
                          child: Center(
                              child: Text(
                            "REQUESTPERMISSION".tr(),
                            style: ThemsConstands.headline3_semibold_20,
                          ))),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: const BoxDecoration(
                            color: Colorconstand.alertsPositiveBg,
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        child: Row(
                          children: [
                            const Icon(
                              Icons.check_circle,
                              color: Color(0xff00763D),size: 18,
                            ),
                            const SizedBox(width: 5,),
                            Text(
                              "SEEN".tr(),
                              style: ThemsConstands
                                  .headline_6_regular_14_20height
                                  .copyWith(
                                      color: Colorconstand.alertsPositive),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                decoration: BoxDecoration(
                    color: Colorconstand.neutralWhite,
                    borderRadius: BorderRadius.circular(18),
                    // boxShadow:[
                    //   BoxShadow(
                    //     color: Colors.grey.withOpacity(0.5),
                    //     blurRadius: 30.0, // soften the shadow
                    //     spreadRadius: 2.0, //extend the shadow
                    //     offset:const Offset(2.0,2.0,),
                    //   )
                    // ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        height: 80,
                        margin:const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  const Icon(Icons.date_range_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                  const SizedBox(width: 5,),
                                  Text("PERMISSIONDATE".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text( widget.listData.startDate.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                   widget.listData.startDate ==  widget.listData.endDate?Container():Text("TO".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                   widget.listData.startDate ==  widget.listData.endDate?Container():Text( widget.listData.endDate.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      const Divider(
                        height: 0,
                        color: Colorconstand.neutralGrey,
                      ),
                       widget.listData.requestTypes == 2?Container():Container(
                        margin:const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  const Icon(Icons.access_time,size: 18,color: Colorconstand.neutralDarkGrey,),
                                  const SizedBox(width: 5,),
                                  Text("PERMISSION_TIME".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                padding:const EdgeInsets.all(0),
                                alignment: Alignment.centerRight,
                                margin:const EdgeInsets.only(top: 35 ),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  physics:const NeverScrollableScrollPhysics(),
                                  itemCount:  widget.listData.schedules!.length,
                                  itemBuilder: (context, indexSchedule){
                                    return Container( alignment: Alignment.centerRight,child: Text("${ widget.listData.schedules![indexSchedule].startTime}-${ widget.listData.schedules![indexSchedule].endTime}",style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),));
                                  }
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                       widget.listData.requestTypes == 2?Container(): const Divider(
                        height: 0,
                        color: Colorconstand.neutralGrey,
                      ),
                      Container(
                        height: 80,
                        margin:const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  const Icon(Icons.message_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                  const SizedBox(width: 5,),
                                  Text("REASON".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                ],
                              ),
                            ),
                            Expanded(child: Text(translate=="km"? widget.listData.admissionTypeName.toString():widget.listData.admissionTypeNameEn.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.end,overflow: TextOverflow.ellipsis,))
                          ],
                        ),
                      ),
                      const Divider(
                        height: 0,
                        color: Colorconstand.neutralGrey,
                      )
                    ],
                  ),
                ),
                listImage.isEmpty && listVoice.isEmpty?Container():Container(
                  margin:const EdgeInsets.only(top: 22),
                  decoration: BoxDecoration(
                    color: Colorconstand.neutralWhite,
                    borderRadius: BorderRadius.circular(18),
                   
                  ),
                  child: Column(
                    children: [
                      const SizedBox(height: 18,),
                      Text("REFERRING_FILE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                      const SizedBox(height: 8,),
                      listImage.isEmpty?Container():Container(
                        height: 80,
                        child: Center(
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: listImage.length,
                            itemBuilder: (context, indexImage) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      pageBuilder: (_, __, ___) =>
                                          ImageViewDownloads(
                                        listimagevide: listImage,
                                        activepage: indexImage,
                                      ),
                                      transitionDuration:
                                          const Duration(seconds: 0),
                                    ),
                                  );
                                },
                                child: Container(
                                  margin:const EdgeInsets.symmetric(horizontal: 6,vertical: 12),
                                  child:ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Image(
                                      image: NetworkImage(listImage[indexImage].fileThumbnail.toString()),fit: BoxFit.cover,height: 60,width: 60,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      listVoice.isEmpty?Container():Divider(),
                      listVoice.isEmpty?Container():Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: ListView.builder(
                          padding:const EdgeInsets.all(0),
                          physics:const NeverScrollableScrollPhysics(),
                          itemCount: listVoice.length,
                          shrinkWrap: true,
                          itemBuilder: (context, indexVoice) {
                          return Container(
                              height: 50,
                              margin:const EdgeInsets.symmetric(horizontal: 28),
                              width: MediaQuery.of(context).size.width/1.5,
                              decoration: BoxDecoration(color: Colorconstand.neutralGrey.withOpacity(0.5),borderRadius:const BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                children: [
                                  GestureDetector(onTap:() async {
                                    setState(() {
                                      getVoice = false;
                                    });
                                    if(isplay == false){
                                      setState(() {
                                        isplay =true;
                                        playerController.startPlayer(finishMode: FinishMode.pause);
                                      });
                                    }
                                    else{
                                      setState(() {
                                        isplay =false;
                                        playerController.pausePlayer();
                                      });
                                    }
                                    playerController.onPlayerStateChanged.listen((event) {
                                      if(event.isPlaying){
                                          setState(() {
                                            isplay =true;
                                          },);
                                      }else if(event.isPaused){
                                          setState(() {
                                            isplay =false;
                                          },);
                                      }
                                    });
                                    playerController.onCompletion.listen((event) {
                                      setState(() {
                                        playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
                                      });
                                    });
                                    playerController.onCurrentDurationChanged.listen((duration) {
                                      setState(() {
                                        playDuration = (playerController.maxDuration~/1000 - (duration~/1000).toInt()).toMMSS();
                                      },);
                                    });
                                    
                                  } , child: Icon(isplay? Icons.pause_rounded:Icons.play_arrow_rounded,color: Colorconstand.primaryColor,size: 30,)),
                                  Container(margin:const EdgeInsets.only(right: 5),child: Text(playDuration,style: ThemsConstands.headline_5_medium_16,)),
                                  Expanded(
                                    child: AudioFileWaveforms(
                                      size: Size(MediaQuery.of(context).size.width, 50.0),
                                      playerController: playerController,
                                      enableSeekGesture: true,
                                      waveformType: WaveformType.long,
                                      waveformData: waveformData,
                                      playerWaveStyle: const PlayerWaveStyle(
                                        scaleFactor: 200,
                                        waveCap: StrokeCap.butt,
                                        showSeekLine: false,
                                        fixedWaveColor: Colorconstand.neutralDarkGrey,
                                        liveWaveColor: Colors.blueAccent,
                                        spacing: 6,),
                                      ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 25,),
              ],
            ),
          ),
        );
                                
      },
    );
  }
}
