import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:camis_teacher_application/app/bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/modules/request_absent_screen/bloc/list_permission_request/list_all_permission_request_bloc.dart';
import 'package:camis_teacher_application/app/modules/request_absent_screen/bloc/own_present_in_month/get_own_present_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import '../../../../../widget/button_widget/button_icon_left_with_title.dart';
import '../../../../../widget/button_widget/button_icon_right_with_title.dart';
import '../../../../../widget/button_widget/button_widget.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/schedule_empty.dart';
import '../../../../core/constands/color_constands.dart';
import '../../../../core/resources/asset_resource.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../attendance_schedule_screen/state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../../bloc/reason_bloc/request_absent_bloc.dart';
import '../../widget/absent_create_widget.dart';
import '../../widget/absent_detail_widget.dart';

class RequestAbsentScreen extends StatefulWidget {
  final String nameTeacher;
  const RequestAbsentScreen({Key? key,required this.nameTeacher}) : super(key: key);

  @override
  State<RequestAbsentScreen> createState() => _RequestAbsentScreenState();
}

class _RequestAbsentScreenState extends State<RequestAbsentScreen> {
  void updateLanguage(Locale locale, BuildContext context) {
    context.setLocale(locale);
  }
  
  bool filtterPermission=  false;
  bool connection = true;
  bool isLoading = false;
  StreamSubscription? sub;
  int step = 1;
  int _previousStep = 0;
  int _totalChild = 0;
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  String? date;
  String displayDate="";
  int _totalDay = 0;
  bool getChildSucess = false;
  List<String> listStudentName = [];
  List<String> listStudentId = [];
  List<String> listDate = [];
  List<DateTime> _date = [];
  String admissionId = "";
  String admissionName = "";
  List<String> listscheduleId = [];
  List<String> listscheduleName = [];
  String? studentId;
  bool showListMonth = false;
  formartMonth(String dateString){
      DateFormat format = DateFormat("dd");
      var formattedDate = format.parse(dateString);
  }
  @override
  void initState() {
    setState(() {
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      date = "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal";
      BlocProvider.of<RequestReasonBloc>(context).add(const RequestReasonEvent());
      BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
      BlocProvider.of<GetOwnPresentBloc>(context).add(GetOwnPresentInMonthEvent(month: monthlocal! <=9?"0$monthlocal":monthlocal.toString().toString(),year: yearchecklocal.toString(),type: "2"));
      BlocProvider.of<ListAllPermissionRequestBloc>(context).add(GetAllPermissionRequestEvent(month: monthlocal! <=9?"0$monthlocal":monthlocal.toString().toString(),year: yearchecklocal.toString()));
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
      //=============Eend Check internet====================
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset("assets/images/Path_20.png"),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Image.asset("assets/images/Path_19.png"),
          ),
          Positioned(
            bottom: 30,
            right: 0,
            child: Image.asset("assets/images/Path_18.png"),
          ),
          Positioned(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
                child: SafeArea(
                  bottom: false,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment:MainAxisAlignment.spaceBetween,
                        crossAxisAlignment:CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 60,
                            child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Icon(
                                Icons.keyboard_arrow_left_rounded,size: 32,color:Colorconstand.primaryColor,)
                              ),
                          ),
                          Expanded(
                              child: Padding(
                              padding:const EdgeInsets.only(),child: Center( child: Text(
                                "MY_ATTENDANCE".tr(),
                                style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.primaryColor)
                              ),),
                          )),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colorconstand.mainColorForecolor
                            ),
                            margin: const EdgeInsets.only(right: 18),
                            child: GestureDetector(
                              onTap: (){
                                setState(() {
                                  if(filtterPermission==true){
                                    filtterPermission = false;
                                  }
                                  else{
                                    filtterPermission = true;
                                  }
                                });
                              },
                              child: Text("LIST_PERMISSION".tr(),
                                  style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralWhite)
                                ),
                            ),
                          )
                          
                        ],
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            showListMonth = true;
                          });
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                                padding:const EdgeInsets.only(right: 0),child: Center( child: Text(
                                  "${"MONTH".tr()} ${checkMonth(monthlocal!.toInt())}",
                                  style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor)
                                ),
                              ),
                            ),
                            const Icon(Icons.arrow_drop_down_rounded,size: 35,color: Colorconstand.primaryColor,)
                          ],
                        ),
                      ),
                      Expanded(
                        child: filtterPermission==false?BlocBuilder<GetOwnPresentBloc, GetOwnPresentState>(
                            builder: (context, state) {
                              if(state is GetOwnPresentLoadingState){
                                return const Center(child: CircularProgressIndicator(),);
                              }
                              else if(state is GetOwnPresentLoadedState){
                                var data = state.ownPrensentInMonthModel.data;
                                return Column(
                                  children: [
                                    Container(
                                      margin:const EdgeInsets.symmetric(vertical: 8),
                                      child: Text("${data!.startDayOfMonth} ${"TO".tr()} ${data.endDayOfMonth}",style: ThemsConstands.headline_6_regular_14_20height,),
                                    ),
                                    Container(
                                      decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(10)),
                                          color: Color.fromRGBO(126, 187, 253, 0.25)),
                                      margin: const EdgeInsets.only(
                                          left: 20, right: 20, bottom: 10),
                                      padding: const EdgeInsets.only(
                                          left: 15, right: 65, bottom: 20, top: 20),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            "DURATIONFINANCE".tr(),
                                            style: ThemsConstands.headline_5_semibold_16,
                                          ),
                                          Text(
                                            "ATTENDANCE_STATUS".tr(),
                                            style: ThemsConstands.headline_5_semibold_16,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: data.staffPresentData!.isEmpty? 
                                      Center(
                                        child: Text(
                                            "NOT_YET_DATA_AVAILABLE".tr(),
                                            style: ThemsConstands.headline_4_semibold_18,
                                          ),
                                      )
                                      :ListView.separated(
                                        padding: const EdgeInsets.symmetric(horizontal: 28),
                                        itemCount:  data.staffPresentData!.length,
                                        separatorBuilder: (context, index) {
                                          return data.staffPresentData![index].presentData!.isEmpty? const SizedBox():const Divider(height: 1,color: Colorconstand.neutralDarkGrey,);
                                        },
                                        itemBuilder: (context, index) {
                                          return data.staffPresentData![index].presentData!.isEmpty?Container():Container(
                                            padding:const EdgeInsets.symmetric(vertical: 8),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                    child: Text(data.staffPresentData![index].date.toString(),style: ThemsConstands.headline_6_regular_14_20height,),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    child: ListView.builder(
                                                      physics:const NeverScrollableScrollPhysics(),
                                                      padding:const EdgeInsets.all(0),
                                                      shrinkWrap: true,
                                                      itemCount: data.staffPresentData![index].presentData!.length,
                                                      itemBuilder: (context, indexSub) {
                                                        return Container(
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                              Container(
                                                                margin:const EdgeInsets.only(bottom: 5),
                                                                height: 22,width: 22,
                                                                decoration:BoxDecoration(
                                                                  color: data.staffPresentData![index].presentData![indexSub].staffPresentStatus == 4?Colorconstand.alertsPositive:data.staffPresentData![index].presentData![indexSub].staffPresentStatus == 2?Colorconstand.mainColorSecondary:data.staffPresentData![index].presentData![indexSub].staffPresentStatus == 3?Colorconstand.alertsDecline:Colorconstand.alertsPositive,
                                                                  shape: BoxShape.circle
                                                                ),
                                                                child:Icon( data.staffPresentData![index].presentData![indexSub].staffPresentStatus==3?Icons.close:Icons.check,size: 18,color: Colorconstand.neutralWhite,),
                                                              ),
                                                              const SizedBox(width: 12,),
                                                              Container(
                                                                child: Text( data.staffPresentData![index].presentData![indexSub].staffPresentStatus == 2?"ABSENTS".tr()+"PERMISSION".tr():"${data.staffPresentData![index].presentData![indexSub].startTime} - ${data.staffPresentData![index].presentData![indexSub].endTime}",style: ThemsConstands.headline_6_regular_14_20height,),
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                );
                              }
                              else{
                                return Center(
                                  child: EmptyWidget(
                                    title: "WE_DETECT".tr(),
                                    subtitle: "WE_DETECT_DES".tr(),
                                  ),
                                );
                              }
                            },
                        )
                        :BlocBuilder<ListAllPermissionRequestBloc,ListAllPermissionRequestState>(
                          builder: (context, state) {
                            if(state is GetAllPermissionRequestLoadingState){
                              return const Center(child: CircularProgressIndicator(),);
                            }
                            else if(state is GetAllPermissionRequestLoadedState){
                              var dataRequestPermission = state.listPermissionRequestModel.data;
                              return Column(
                                  children: [
                                    Container(
                                      decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(10)),
                                          color: Color.fromRGBO(126, 187, 253, 0.25)),
                                      margin: const EdgeInsets.only(
                                          left: 20, right: 20, bottom: 10),
                                      padding: const EdgeInsets.only(
                                          left: 15, right: 15, bottom: 20, top: 20),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              "DURATIONFINANCE".tr(),
                                              style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.left,
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              "REASON".tr(),
                                              style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 85,
                                            child: Text(
                                              "REQUEST_TYPE".tr(),
                                              style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,
                                            ),
                                          ),
                                          const SizedBox(width: 20,)
                                          
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: dataRequestPermission!.isEmpty? 
                                      Center(
                                        child: Text(
                                            "NOT_YET_DATA_AVAILABLE".tr(),
                                            style: ThemsConstands.headline_4_semibold_18,
                                          ),
                                      )
                                      :ListView.separated(
                                        padding: const EdgeInsets.symmetric(horizontal: 28),
                                        itemCount:  dataRequestPermission.length,
                                        separatorBuilder: (context, index) {
                                          return const Divider(height: 1,color: Colorconstand.neutralDarkGrey,);
                                        },
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                            onTap: () {
                                              showModalBottomSheet(
                                                enableDrag:false,
                                                isDismissible:false,
                                                backgroundColor:Colors.transparent,
                                                isScrollControlled:true,
                                                shape:const RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.only(
                                                      topLeft:Radius.circular(16),
                                                      topRight:Radius.circular(16)),
                                                ),
                                                context:context,
                                                builder:(context) {
                                                  if(dataRequestPermission[index].status == false){
                                                     return AbsentDetail(
                                                        listData: dataRequestPermission[index],
                                                      );
                                                  }
                                                  else{
                                                    return AbsentCreate(
                                                      month: monthlocal! <=9?"0$monthlocal":monthlocal.toString().toString(),
                                                      year: yearchecklocal.toString(),
                                                      listData: dataRequestPermission[index],
                                                      teachername: widget.nameTeacher,
                                                      step: step,
                                                      listDate: listDate,
                                                      listStudentId: listStudentId,
                                                      listscheduleId: listscheduleId,
                                                      listscheduleName: listscheduleName,
                                                      admissionId: admissionId,
                                                      listStudentName: listStudentName,
                                                      admissionName: admissionName,
                                                      displayDate: displayDate,
                                                    );
                                                  }
                                                });
                                            },
                                            child: Container(
                                              padding:const EdgeInsets.symmetric(vertical: 8),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(dataRequestPermission[index].startDate.toString(),style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.left,),
                                                        // dataRequestPermission[index].requestTypes==2?Text("TO".tr(),style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.left,):Container(),
                                                        // dataRequestPermission[index].requestTypes==2?Text(dataRequestPermission[index].startDate.toString(),style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.left,):Container(),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(child: Text(translate=="km"?dataRequestPermission[index].admissionTypeName.toString():dataRequestPermission[index].admissionTypeNameEn.toString(),style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)),
                                                  SizedBox(width: 85,child: Text(dataRequestPermission[index].requestTypes==2?"PERMISSION_DAY".tr():"PERMISSION_SCHEDULE".tr(),style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.center,)),
                                                  const SizedBox(width: 25,child: Icon(Icons.arrow_forward_ios_sharp,size: 12,),)
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                );
                            }
                            else{
                              return Center(
                                  child: EmptyWidget(
                                    title: "WE_DETECT".tr(),
                                    subtitle: "WE_DETECT_DES".tr(),
                                  ),
                                );
                            }
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.bottomCenter,
                        decoration: const BoxDecoration(
                          color: Colorconstand.neutralWhite,
                           borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                            ),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, -5),
                              blurRadius: 10,
                              color: Color.fromRGBO(0, 0, 0, 0.1),
                            ),
                          ],
                        ),
                        padding: const EdgeInsets.only(top: 23, bottom: 28, right: 25, left: 25),
                        child: Column(
                          children: [
                            ButtonIconLeftWithTitle(
                              onTap: () {
                                setState(() {
                                  listDate = [];
                                 _totalDay = 0;
                                 listscheduleId = [];
                                 listscheduleName = [];
                                 admissionId = "";
                                 admissionName = "";
                                });
                                showModalBottomSheet(
                                  enableDrag: false,
                                  isDismissible: false,
                                  backgroundColor: Colors.transparent,
                                  isScrollControlled: true,
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16),),
                                  ),
                                  context: context, 
                                  builder: (BuildContext context) { 
                                    return StatefulBuilder(builder: (context, setStatechangedate) {
                                      return Container(
                                        margin:const EdgeInsets.only(top: 65),
                                        padding:const EdgeInsets.all(18),
                                        decoration:const BoxDecoration(
                                          color: Colorconstand.neutralSecondBackground,
                                           borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(18),
                                            topRight: Radius.circular(18),),
                                          ),
                                        child: SingleChildScrollView(
                                          child: Column(
                                                  children: [
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                      crossAxisAlignment:CrossAxisAlignment.center,
                                                      children: [
                                                        IconButton(
                                                            onPressed: () {
                                                              setStatechangedate(
                                                                () {
                                                                  listDate = [];
                                                                  studentId;
                                                                  _totalDay = 0;
                                                                  Navigator.pop(context);
                                                                },
                                                              );
                                                            },
                                                            icon: const Icon(
                                                              Icons.keyboard_arrow_left_rounded,size: 32,color:Colorconstand.neutralDarkGrey,
                                                            )),
                                                          Expanded(
                                                            child: Padding(
                                                            padding:const EdgeInsets.only(right: 60),
                                                            child: Center(
                                                                child: Text(
                                                              "SELECTE_DAY_PERMISSION".tr(),
                                                              style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center,
                                                            )),
                                                          ))
                                                      ],
                                                    ),
                                                    const Divider(
                                                      thickness: 0.5,
                                                      color: Color.fromRGBO(0, 0, 0, 0.1),
                                                    ),
                                                    SfDateRangePicker(
                                                      backgroundColor: Colors.transparent,
                                                      selectionMode:DateRangePickerSelectionMode.range,
                                                      headerStyle: DateRangePickerHeaderStyle(
                                                          textAlign: TextAlign.center,
                                                          textStyle: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.primaryColor)),
                                                      enablePastDates: false,
                                                      onSelectionChanged: (args) {
                                                        if (args.value is PickerDateRange) {
                                                          try {
                                                            final DateTime rangeStartDate = args.value.startDate;
                                                            final DateTime rangeEndDate = args.value.endDate ?? args.value.startDate;
                                                            setStatechangedate(
                                                              () {
                                                                _totalDay = rangeEndDate.difference(rangeStartDate).inDays;
                                                                _totalDay = _totalDay + 1;
                                                                var dataFormate = DateFormat("dd/MM/yyyy");
                                                                date = dataFormate.format(rangeStartDate);
                                                                var startDate = dataFormate.format(rangeStartDate);
                                                                var endDate = dataFormate.format(rangeEndDate);
                                                                displayDate = DateFormat.yMMMMd(translate).format(rangeStartDate);
                                                                listDate = [startDate,endDate];
                                                                print("listdate : ${listDate.length} ");
                                                                
                                                              },
                                                            );
                                                          } catch (e) {
                                                            print("error date : $e");
                                                          }
                                                          print("total day: $_totalDay");
                                                        } else if (args.value is DateTime) {
                                                          final DateTime selectedDate = args.value;
                                                          setStatechangedate(
                                                            () {
                                                              _totalDay = args.value;
                                                            },
                                                          );
                                                          print("total day: ${_totalDay + 1}");
                                                        } else if (args.value is List<DateTime>) {
                                                          final List<DateTime> selectedDates = args.value;
                                                        } else {
                                                          final List<PickerDateRange> selectedRanges = args.value;
                                                        }
                                                      },
                                                    ),
                                                    const Divider(
                                                      thickness: 0.5,
                                                      color: Color.fromRGBO(0, 0, 0, 0.1),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: [
                                                        Expanded(
                                                          child: Button_Custom(
                                                            hightButton: 45,
                                                            widthButton:  _totalDay > 1 || _totalChild > 1? 325: 157.5,
                                                            buttonColor:Colorconstand.primaryColor,
                                                            radiusButton: 8,
                                                            titleButton: "ASK_BY_DAY".tr(),
                                                            titlebuttonColor:Colorconstand.neutralWhite,
                                                            onPressed: _totalDay > 0
                                                                ? () {
                                                                    setState(
                                                                      () {
                                                                      step = 3;
                                                                      showModalBottomSheet(
                                                                        enableDrag: false,
                                                                        isDismissible: false,
                                                                        backgroundColor: Colors.transparent,
                                                                        isScrollControlled: true,
                                                                        shape: const RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.only(
                                                                              topLeft: Radius.circular(16),
                                                                              topRight: Radius.circular(16)),
                                                                        ),
                                                                        context: context,
                                                                        builder: (context) {
                                                                          return reasonWidget(context,translate);
                                                                        });
                                                                      },
                                                                    );
                                                                  }
                                                                : null,
                                                          ),
                                                        ),
                                                        _totalDay > 1 || _totalChild > 1
                                                            ? Container()
                                                            : const SizedBox(
                                                                width: 20,
                                                              ),
                                                        _totalDay > 1 || _totalChild > 1
                                                            ? Container()
                                                            : Expanded(
                                                              child: Button_Custom(
                                                                  hightButton: 45,
                                                                  widthButton: 157.5,
                                                                  buttonColor:Colorconstand.primaryColor,
                                                                  radiusButton: 8,
                                                                  titleButton:"ASK_BY_SCHDULE".tr(),
                                                                  titlebuttonColor:Colorconstand.neutralWhite,
                                                                  onPressed: _totalDay == 1
                                                                    ? () {
                                                                        BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: date));
                                                                        setStatechangedate(
                                                                          () {
                                                                            step = 4;
                                                                            print("date : $date");
                                                                            showModalBottomSheet(
                                                                                enableDrag: false,
                                                                                isDismissible: false,
                                                                                backgroundColor: Colors.transparent,
                                                                                isScrollControlled: true,
                                                                                shape: const RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.only(
                                                                                      topLeft: Radius.circular(16),
                                                                                      topRight: Radius.circular(16)),
                                                                                ),
                                                                                context: context,
                                                                                builder: (context) {
                                                                                  return StatefulBuilder(builder: (context, setStatechangeschedule) {
                                                                                    return Container(
                                                                                      margin:const EdgeInsets.only(top: 65),
                                                                                      padding:const EdgeInsets.all(18),
                                                                                      decoration:const BoxDecoration(
                                                                                        color: Colorconstand.neutralSecondBackground,
                                                                                         borderRadius: BorderRadius.only(
                                                                                          topLeft: Radius.circular(18),
                                                                                          topRight: Radius.circular(18),),
                                                                                      ),
                                                                                      child: SingleChildScrollView(
                                                                                        child: Column(
                                                                                          children: [
                                                                                            const SizedBox(
                                                                                              height: 10,
                                                                                            ),
                                                                                            Row(mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                                              crossAxisAlignment:CrossAxisAlignment.center,
                                                                                              children: [
                                                                                                IconButton(
                                                                                                    onPressed: () {
                                                                                                      setStatechangeschedule(
                                                                                                        () {
                                                                                                          listscheduleId = [];
                                                                                                          listscheduleName = [];                                                                                                                                      Navigator.pop(context);
                                                                                                        },
                                                                                                      );
                                                                                                    },
                                                                                                    icon: const Icon(Icons.keyboard_arrow_left_rounded,size: 32,color: Colorconstand.neutralDarkGrey,
                                                                                                    )),
                                                                                                  Expanded(
                                                                                                    child: Padding(
                                                                                                    padding: const EdgeInsets.only(right: 60),
                                                                                                    child: Center(
                                                                                                      child: Text("SELECTE_TIME".tr(),
                                                                                                      style: ThemsConstands.headline3_semibold_20,
                                                                                                  )),
                                                                                                ))
                                                                                              ],
                                                                                            ),
                                                                                            const Divider(
                                                                                              thickness: 0.5,
                                                                                              color: Color.fromRGBO(0, 0, 0, 0.1),
                                                                                            ),
                                                                                            Text(
                                                                                              displayDate.toString(),
                                                                                              style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.primaryColor),
                                                                                            ),
                                                                                            BlocBuilder<GetScheduleBloc,GetScheduleState>(
                                                                                              builder: (context, state) { 
                                                                                                if(state is GetScheduleLoading){
                                                                                                  return Container(
                                                                                                    margin:const EdgeInsets.symmetric(vertical: 15),
                                                                                                    child: const Center(
                                                                                                      child: SizedBox(height: 30, width: 30,
                                                                                                        child: CircularProgressIndicator(),
                                                                                                      ),
                                                                                                    ),
                                                                                                  );
                                                                                                }
                                                                                                else if(state is GetMyScheduleLoaded){
                                                                                                  var dataSchedule = state.myScheduleModel!.data;
                                                                                                  return dataSchedule!.isEmpty? DataEmptyWidget(
                                                                                                    title: "NO_CLASS_TODAY".tr(),
                                                                                                    description: "",
                                                                                                  ): Container(
                                                                                                    margin:const EdgeInsets.only(top: 10),
                                                                                                    child: ListView.separated(
                                                                                                    physics:const NeverScrollableScrollPhysics(),
                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 30),
                                                                                                    shrinkWrap: true,
                                                                                                    itemCount: dataSchedule.length,
                                                                                                    itemBuilder:(context, index) {
                                                                                                      return Row(
                                                                                                        crossAxisAlignment:CrossAxisAlignment.center,
                                                                                                        mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                                                        children: [
                                                                                                          IconButton(
                                                                                                              onPressed: () {
                                                                                                                setStatechangeschedule(
                                                                                                                  () {
                                                                                                                    if(dataSchedule[index].isSelected == false){
                                                                                                                      listscheduleId.add(dataSchedule[index].scheduleId.toString());
                                                                                                                      listscheduleName.add("${dataSchedule[index].startTime.toString()}-${dataSchedule[index].endTime.toString()}");
                                                                                                                      dataSchedule[index].isSelected = true;
                                                                                                                    }
                                                                                                                    else{
                                                                                                                      listscheduleId.remove(dataSchedule[index].scheduleId.toString());
                                                                                                                      listscheduleName.remove("${dataSchedule[index].startTime.toString()}-${dataSchedule[index].endTime.toString()}");
                                                                                                                      dataSchedule[index].isSelected = false;
                                                                                                                    }
                                                                                                                  },
                                                                                                                );
                                                                                                              },
                                                                                                              icon: Icon( dataSchedule[index].isSelected
                                                                                                                    ? Icons.check_box: Icons.check_box_outline_blank_rounded,
                                                                                                                size: 35,
                                                                                                                color:  dataSchedule[index].isSelected
                                                                                                                    ? Colorconstand.primaryColor
                                                                                                                    : Colorconstand.lightTrunks,
                                                                                                              )),
                                                                                                          const SizedBox(width: 30,),
                                                                                                          Expanded(
                                                                                                            child: Align(
                                                                                                              alignment: Alignment.centerLeft,
                                                                                                              child: Text(
                                                                                                                "${dataSchedule[index].startTime} - ${dataSchedule[index].endTime}",
                                                                                                                style: ThemsConstands.headline_5_semibold_16,),
                                                                                                            ),
                                                                                                          ),
                                                                                                          Align(
                                                                                                          alignment: Alignment.centerRight, 
                                                                                                          child: Text(
                                                                                                          translate=="km"?dataSchedule[index].subjectName.toString():dataSchedule[index].subjectNameEn.toString(),
                                                                                                          style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),
                                                                                                          ),
                                                                                                          )
                                                                                                        ],
                                                                                                      );
                                                                                                    },
                                                                                                    separatorBuilder:(BuildContext context,int index) {
                                                                                                      return const Divider(
                                                                                                        indent: 10,
                                                                                                        endIndent: 10,
                                                                                                        thickness: 0.5,
                                                                                                        color: Color.fromRGBO(0, 0, 0, 0.1),
                                                                                                      );
                                                                                                    },
                                                                                                                                                                                                                      ),
                                                                                                  );
                                                                                                }
                                                                                                else{
                                                                                                  return const Center(
                                                                                                    child: SizedBox(height: 50, width: 50,
                                                                                                      child: CircularProgressIndicator(),
                                                                                                    ),
                                                                                                  );
                                                                                                }
                                                                                              },
                                                                                            ),
                                                                                            const Divider(
                                                                                              thickness: 0.5,
                                                                                              color: Color.fromRGBO(0, 0, 0, 0.1),
                                                                                            ),
                                                                                            Container(
                                                                                              margin:const EdgeInsets.symmetric(horizontal: 30,vertical: 20),
                                                                                              child: ButtonIconRightWithTitle(
                                                                                                heightButton: 48,
                                                                                                onTap:listscheduleId.isNotEmpty
                                                                                                    ? () {
                                                                                                        setState(
                                                                                                          () {
                                                                                                            showModalBottomSheet(
                                                                                                              enableDrag: false,
                                                                                                              isDismissible: false,
                                                                                                              backgroundColor: Colors.transparent,
                                                                                                              isScrollControlled: true,
                                                                                                              shape: const RoundedRectangleBorder(
                                                                                                                borderRadius: BorderRadius.only(
                                                                                                                    topLeft: Radius.circular(16),
                                                                                                                    topRight: Radius.circular(16)),
                                                                                                              ),
                                                                                                              context: context,
                                                                                                              builder: (context) {
                                                                                                                return reasonWidget(context,translate);
                                                                                                            });
                                                                                                          }
                                                                                                        );
                                                                                                      }
                                                                                                    : null,
                                                                                                title: "SELETE_ADMISSION".tr(),
                                                                                                textStyleButton:ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite,
                                                                                                ),
                                                                                                buttonColor: Colorconstand.primaryColor,
                                                                                                radiusButton: 8,
                                                                                                panddinHorButton: 20,
                                                                                                panddingVerButton: 6,
                                                                                                iconColor: Colorconstand.neutralWhite,
                                                                                                iconImage: Icons.keyboard_arrow_right_rounded,
                                                                                              ),
                                                                                            ),
                                                                                            const SizedBox(
                                                                                              height: 25,
                                                                                            ),
                                                                                          ],
                                                                                        )),
                                                                                    );
                                                                                  },);
                                                                                
                                                                                });      
                                                                            },
                                                                          );
                                                                      }
                                                                      : null,
                                                                  ),
                                                            ),
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                      height: 30,
                                                    )
                                                  ],
                                                ),
                                              ),
                                      );
                                          },);
                                        
                                        },
                                      );
                                                            
                              },
                              buttonColor: Colorconstand.primaryColor,
                              iconImage: ImageAssets.card_add_icon,
                              weightButton: MediaQuery.of(context).size.width,
                              panddinHorButton: 0,
                              panddingVerButton: 0,
                              radiusButton: 8,
                              heightButton: 55,
                              speaceTitleAndImage: 16,
                              textStyleButton: ThemsConstands.button_semibold_16
                                  .copyWith(color: Colorconstand.neutralWhite),
                              title: 'REQUEST_PERMISSION'.tr(),
                              iconColor: Colorconstand.neutralWhite,
                            ),
                          ],
                        ),
                        
                      ),
                    ],
                  )
                ),
              )
          ),
           showListMonth == false?const SizedBox():Positioned(
            top: 0,left: 0,right: 0,bottom: 0,
            child:  GestureDetector(
              onTap: () {
                setState(() {
                  showListMonth = false;
                });
              },
              child: Container(
                color: Colorconstand.neutralWhite.withOpacity(0.5),
              ),
            ),
          ),
          BlocBuilder<ListMonthSemesterBloc, ListMonthSemesterState>(
            builder: (context, state) {
              if(state is ListMonthSemesterLoading){
                return const Center(
                  //child: CircularProgressIndicator(),
                );
              }
              else if(state is ListMonthSemesterLoaded){
                var data = state.monthAndSemesterList!.data;
                var attendanceMonth = data!.where((element) => element.displayMonth != "SECOND_SEMESTER" && element.displayMonth != "FIRST_SEMESTER" && element.displayMonth != "YEAR").toList();
                return Positioned(
                  top: 80,left: 0,right: 0,
                child: SafeArea(
                  bottom: false,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 28,right: 28),
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 200),
                      height: showListMonth == false?0 :210,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        color: Colorconstand.neutralWhite,
                      ),
                      alignment: Alignment.center,
                      child: GridView.builder(
                      shrinkWrap: true,
                      physics:const ScrollPhysics(),
                      padding:const EdgeInsets.only(top: 25,left: 28,right: 18),
                        itemCount: attendanceMonth.length,
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: (){
                              setState(() {
                                BlocProvider.of<ListAllPermissionRequestBloc>(context).add(GetAllPermissionRequestEvent(month:attendanceMonth[index].month.toString(),year: yearchecklocal.toString()));
                                BlocProvider.of<GetOwnPresentBloc>(context).add(GetOwnPresentInMonthEvent(month:attendanceMonth[index].month.toString(),year: yearchecklocal.toString(),type: "2"));
                                showListMonth = false;
                                monthlocal =int.parse(attendanceMonth[index].month!.toString());
                              });
                            },
                            child: Container(
                              child: Text(attendanceMonth[index].displayMonth.toString().tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.lightBlack),),
                            ),
                          );
                        }
                      )
                    ),
                  ),
                ),
              );
              }
              else{
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          )
        ],
      ),
    );
  }
  Widget reasonWidget(BuildContext context, String translate,){
    return StatefulBuilder(builder: (context, setStateadmission) {
          return Container(
            margin:const EdgeInsets.only(top: 65),
            padding:const EdgeInsets.all(18),
            decoration:const BoxDecoration(
              color: Colorconstand.neutralSecondBackground,
               borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18),
                topRight: Radius.circular(18),),
            ),
            child: SingleChildScrollView(
              child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment:CrossAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(
                            () {
                              admissionId = "";
                              admissionName = "";
                              studentId;
                              Navigator.pop(context);
                            },
                          );
                        },
                        icon: const Icon(Icons.keyboard_arrow_left_rounded,size: 32,color: Colorconstand.neutralDarkGrey,)),
                     Expanded(child: Padding(
                      padding: const EdgeInsets.only(right: 60),
                      child: Center(child: Text(
                        "SELETE_ADMISSION".tr(),
                        style: ThemsConstands.headline3_medium_20_26height,
                      )),
                    ))
                  ],
                ),
                const Divider(
                  thickness: 0.5,
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                ),
                BlocBuilder<RequestReasonBloc,RequestAbsentState>(
                  builder: (context, state) {
                    if (state is RequestAbsentLoadingState) {
                      return const Center(
                        child: SizedBox(
                            height: 150,
                            width: 50,
                            child:CircularProgressIndicator()),
                      );
                    } else if (state is RequestReasonLoadedState) {
                      var data = state.admissionModel.data;
                      return ListView.builder(
                        physics:const NeverScrollableScrollPhysics(),
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        shrinkWrap: true,
                        itemCount: data!.length,
                        itemBuilder:(context, index) {
                          return Container(
                            margin:const EdgeInsets.symmetric(vertical: 10),
                            width: MediaQuery.of(context).size.width,
                            height: 55,
                            child: MaterialButton(
                              disabledColor:Colorconstand.mainColorUnderlayer,
                              color: Colorconstand.primaryColor,
                              shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(8))),
                              padding:const EdgeInsets.all(0),
                              onPressed: () {
                                setState(() {
                                  admissionId = data[index].id.toString();
                                  admissionName = translate == "km"?data[index].name.toString():data[index].nameEn.toString();
                                },);
                                showModalBottomSheet(
                                    enableDrag:false,
                                    isDismissible:false,
                                    backgroundColor:Colors.transparent,
                                    isScrollControlled:true,
                                    shape:const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft:Radius.circular(16),
                                          topRight:Radius.circular(16)),
                                    ),
                                    context:context,
                                    builder:(context) {
                                      return AbsentCreate(
                                        month: monthlocal! <=9?"0$monthlocal":monthlocal.toString().toString(),
                                        year: yearchecklocal.toString(),
                                        teachername: widget.nameTeacher,
                                        step: step,
                                        listDate: listDate,
                                        listStudentId: listStudentId,
                                        listscheduleId: listscheduleId,
                                        listscheduleName: listscheduleName,
                                        admissionId: admissionId,
                                        listStudentName: listStudentName,
                                        admissionName: admissionName,
                                        displayDate: displayDate,
                                      );
                                    });
                              },
                              child: Container(
                                alignment:Alignment.center,
                                padding: const EdgeInsets.symmetric(vertical: 6,horizontal:20),
                                child: Text(
                                  translate =="km"? data[index].name.toString(): data[index].nameEn.toString(),
                                  style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),
                                  textAlign:TextAlign.center,
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    } else {
                      print("get admission error");
                      return const Center(
                        child: SizedBox(
                            height: 50,
                            width: 50,
                            child:CircularProgressIndicator()),
                      );
                    }
                  },
                ),
                const SizedBox(
                  height: 25,
                ),
              ],
            ),),
          );
        },);
  }
}