// To parse this JSON data, do
//
//     final listStudentUnlockUsePackage = listStudentUnlockUsePackageFromJson(jsonString);

import 'dart:convert';

ListGuardianRequestUnlock ListGuardianRequestUnlockFromJson(String str) => ListGuardianRequestUnlock.fromJson(json.decode(str));

String ListGuardianRequestUnlockToJson(ListGuardianRequestUnlock data) => json.encode(data.toJson());

class ListGuardianRequestUnlock {
    bool? status;
    String? message;
    DataGuardianRequest? data;

    ListGuardianRequestUnlock({
        this.status,
        this.message,
        this.data,
    });

    factory ListGuardianRequestUnlock.fromJson(Map<String, dynamic> json) => ListGuardianRequestUnlock(
        status: json["status"],
        message: json["message"],
        data: json["data"] == null?null: DataGuardianRequest.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data==null?null:data!.toJson(),
    };
}

class DataGuardianRequest {
    int? total;
    int? currentPage;
    dynamic hasMorePages;
    List<Datum>? data;

    DataGuardianRequest({
        this.total,
        this.currentPage,
        this.hasMorePages,
        this.data,
    });

    factory DataGuardianRequest.fromJson(Map<String, dynamic> json) => DataGuardianRequest(
        total: json["total"],
        currentPage: json["current_page"],
        hasMorePages: json["hasMorePages"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "current_page": currentPage,
        "hasMorePages": hasMorePages,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    int? id;
    int? customerId;
    String? guardianId;
    Guardian? guardian;
    int? classId;
    String? className;
    int? status;
    String? requestTo;
    String? requestedAt;
    dynamic unlockedBy;
    dynamic unlockedAt;
    int? schoolPromotionId;
    dynamic createdAt;
    dynamic updatedAt;

    Datum({
        this.id,
        this.customerId,
        this.guardianId,
        this.guardian,
        this.classId,
        this.className,
        this.status,
        this.requestTo,
        this.requestedAt,
        this.unlockedBy,
        this.unlockedAt,
        this.schoolPromotionId,
        this.createdAt,
        this.updatedAt,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        customerId: json["customer_id"],
        guardianId: json["guardian_id"],
        guardian: Guardian.fromJson(json["guardian"]),
        classId: json["class_id"],
        className: json["class_name"],
        status: json["status"],
        requestTo: json["request_to"],
        requestedAt: json["requested_at"],
        unlockedBy: json["unlocked_by"],
        unlockedAt: json["unlocked_at"],
        schoolPromotionId: json["school_promotion_id"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "customer_id": customerId,
        "guardian_id": guardianId,
        "guardian": guardian!.toJson(),
        "class_id": classId,
        "class_name": className,
        "status": status,
        "request_to": requestTo,
        "requested_at": requestedAt,
        "unlocked_by": unlockedBy,
        "unlocked_at": unlockedAt,
        "school_promotion_id": schoolPromotionId,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}

class Guardian {
    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? role;
    dynamic isVerify;
    ProfileMedia? profileMedia;
    String? qrUrl;
    dynamic childHasClassTemp;

    Guardian({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.isVerify,
        this.profileMedia,
        this.qrUrl,
        this.childHasClassTemp,
    });

    factory Guardian.fromJson(Map<String, dynamic> json) => Guardian(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        isVerify: json["is_verify"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
        childHasClassTemp: json["child_has_class_temp"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "is_verify": isVerify,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
        "child_has_class_temp": childHasClassTemp,
    };
}

class ProfileMedia {
    dynamic id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    dynamic isGdrive;
    String? fileGdriveId;
    dynamic isS3;
    String? fileShow;
    String? fileThumbnail;

    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
