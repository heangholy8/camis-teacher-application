// To parse this JSON data, do
//
//     final checkSchoolUsePackage = checkSchoolUsePackageFromJson(jsonString);

import 'dart:convert';

CheckSchoolUsePackage checkSchoolUsePackageFromJson(String str) => CheckSchoolUsePackage.fromJson(json.decode(str));

String checkSchoolUsePackageToJson(CheckSchoolUsePackage data) => json.encode(data.toJson());

class CheckSchoolUsePackage {
    bool? isUsePackage;

    CheckSchoolUsePackage({
        this.isUsePackage,
    });

    factory CheckSchoolUsePackage.fromJson(Map<String, dynamic> json) => CheckSchoolUsePackage(
        isUsePackage: json["is_use_package"],
    );

    Map<String, dynamic> toJson() => {
        "is_use_package": isUsePackage,
    };
}
