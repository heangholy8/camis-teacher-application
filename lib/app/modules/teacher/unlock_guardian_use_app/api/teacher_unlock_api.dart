import 'dart:convert';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/model/check_school_use_package.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/model/list_guardian_request_unlock.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/model/list_student_unlock_use_package.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


class TeacherUnlockGuardianUseAppApi  {

  Future<CheckSchoolUsePackage> checkSchoolUserPackageApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/guardian-use-package/check-if-school-use-package"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint('check-if-school-use-package API: ${response.body}');
      return CheckSchoolUsePackage.fromJson(jsonDecode(response.body));
    } else {
      debugPrint('check-if-school-use-package API: ${response.body}');
      throw Exception((e) {
       debugPrint('check-if-school-use-package API: $e');
      });
    }
  }

  Future<ListStudentUnlockUsePackage> listStudentUserPackageApi(String classId) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/guardian-use-package/list-student-to-unlock?class_id=$classId"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ListStudentUnlockUsePackage.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
       debugPrint('list-student-to-unlock API: $e');
      });
    }
  }

  Future<ListGuardianRequestUnlock> listGuardianrequestUnlockApi(String classId,status,role) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/guardian-use-package/list-request?status=$status${role=="1"||role=="6"?"&class_id=$classId":""}"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint('list-guardian-request: ${response.body}');
      return ListGuardianRequestUnlock.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
       debugPrint('list-guardian-request: ${response.body}');
       debugPrint('list-guardian-request: $e');
      });
    }
  }

  Future<bool> unlockAndLockStudentUserPackageApi(String classId,status,studentId,guardianId,expiredAt,closedAt) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/guardian-use-package/unlock-and-update"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
      body: {
        "expired_at":expiredAt,
        "closed_at":closedAt,
        "class_id":classId,
        "status":status,
        "student_id":studentId,
        "guardian_id":guardianId,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint('list-student-to-unlock API: ${response.body}');
      return true;
      
    } else {
      
      debugPrint('list-student-to-unlock API: ${response.body}');
      throw Exception((e) {
       debugPrint('list-student-to-unlock API: $e');
       return false;
      });
    }
  }

  Future<bool> unlockGuardianRequestApi(String id,status,expiredAt) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/guardian-use-package/update/$id"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
      body: {
        "status":status,
        "expired_at":expiredAt,

      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint('list-student-to-lock API: ${response.body}');
      return true;
      
    } else {
      
      debugPrint('list-student-to-lock API: ${response.body}');
      throw Exception((e) {
       debugPrint('list-student-to-lock API: $e');
       return false;
      });
    }
  }

}
