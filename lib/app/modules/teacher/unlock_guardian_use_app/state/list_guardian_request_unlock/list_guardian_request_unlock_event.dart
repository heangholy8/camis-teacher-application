part of 'list_guardian_request_unlock_bloc.dart';

sealed class ListGuardianRequestUnlockEvent extends Equatable {
  const ListGuardianRequestUnlockEvent();

  @override
  List<Object> get props => [];
}

class GetListGuardianRequestUnlock extends ListGuardianRequestUnlockEvent{
  final String classId;
  final String status;
  final String role;
  const GetListGuardianRequestUnlock({required this.classId,required this.role,required this.status});
}


