part of 'list_guardian_request_unlock_bloc.dart';

sealed class ListGuardianRequestUnlockState extends Equatable {
  const ListGuardianRequestUnlockState();
  
  @override
  List<Object> get props => [];
}

final class ListGuardianRequestUnlockInitial extends ListGuardianRequestUnlockState {}

final class ListGuardianRequestUnlockLoading extends ListGuardianRequestUnlockState {}
final class ListGuardianRequestUnlockSucess extends ListGuardianRequestUnlockState {
  final ListGuardianRequestUnlock listGuardianRequestUnlock;
  const ListGuardianRequestUnlockSucess({required this.listGuardianRequestUnlock});
}
final class ListGuardianRequestUnlockError extends ListGuardianRequestUnlockState {}
