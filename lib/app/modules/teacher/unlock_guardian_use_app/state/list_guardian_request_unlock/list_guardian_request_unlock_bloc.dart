import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/api/teacher_unlock_api.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/model/list_guardian_request_unlock.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
part 'list_guardian_request_unlock_event.dart';
part 'list_guardian_request_unlock_state.dart';

class ListGuardianRequestUnlockBloc extends Bloc<ListGuardianRequestUnlockEvent, ListGuardianRequestUnlockState> {
  TeacherUnlockGuardianUseAppApi teacherUnlockGuardianUseAppApi;
  ListGuardianRequestUnlockBloc({required this.teacherUnlockGuardianUseAppApi}) : super(ListGuardianRequestUnlockInitial()) {
    on<GetListGuardianRequestUnlock>((event, emit) async {
      emit(ListGuardianRequestUnlockLoading());
      try{
         var dataGuardian = await teacherUnlockGuardianUseAppApi.listGuardianrequestUnlockApi(event.classId,event.status,event.role);
         emit(ListGuardianRequestUnlockSucess(listGuardianRequestUnlock: dataGuardian));
      }catch(e){
        debugPrint('list-guardian-request: $e');
        emit(ListGuardianRequestUnlockError());
      }
    });
  }
}
