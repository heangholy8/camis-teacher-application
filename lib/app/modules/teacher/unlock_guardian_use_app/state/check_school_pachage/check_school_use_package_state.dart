part of 'check_school_use_package_bloc.dart';

sealed class CheckSchoolUsePackageState extends Equatable {
  const CheckSchoolUsePackageState();
  
  @override
  List<Object> get props => [];
}

final class CheckSchoolUsePackageInitial extends CheckSchoolUsePackageState {}

final class CheckSchoolUsePackageLoading extends CheckSchoolUsePackageState {}

final class CheckSchoolUsePackageError extends CheckSchoolUsePackageState {}

final class CheckSchoolUsePackageSuccess extends CheckSchoolUsePackageState {
  final CheckSchoolUsePackage  checkSchoolUsePackage;
  const CheckSchoolUsePackageSuccess({required this.checkSchoolUsePackage});
}
