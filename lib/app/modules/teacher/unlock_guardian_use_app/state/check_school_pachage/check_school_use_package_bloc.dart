import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/api/teacher_unlock_api.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/model/check_school_use_package.dart';
import 'package:equatable/equatable.dart';
part 'check_school_use_package_event.dart';
part 'check_school_use_package_state.dart';

class CheckSchoolUsePackageBloc extends Bloc<CheckSchoolUsePackageEvent, CheckSchoolUsePackageState> {
  TeacherUnlockGuardianUseAppApi teacherUnlockGuardianUseAppApi;
  CheckSchoolUsePackageBloc({required this.teacherUnlockGuardianUseAppApi}) : super(CheckSchoolUsePackageInitial()) {
    on<GetCheckSchoolUsePachage>((event, emit) async{
      emit(CheckSchoolUsePackageLoading());
      try{
        var data = await teacherUnlockGuardianUseAppApi.checkSchoolUserPackageApi();
        emit(CheckSchoolUsePackageSuccess(checkSchoolUsePackage: data));
      }catch(e){
        emit(CheckSchoolUsePackageError());
      }
    });
  }
}
