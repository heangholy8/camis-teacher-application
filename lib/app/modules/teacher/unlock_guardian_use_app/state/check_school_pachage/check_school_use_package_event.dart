part of 'check_school_use_package_bloc.dart';

sealed class CheckSchoolUsePackageEvent extends Equatable {
  const CheckSchoolUsePackageEvent();

  @override
  List<Object> get props => [];
}

class GetCheckSchoolUsePachage extends CheckSchoolUsePackageEvent{}
