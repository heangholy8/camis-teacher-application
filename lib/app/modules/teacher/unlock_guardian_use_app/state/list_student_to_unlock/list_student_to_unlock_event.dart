part of 'list_student_to_unlock_bloc.dart';

sealed class ListStudentToUnlockEvent extends Equatable {
  const ListStudentToUnlockEvent();

  @override
  List<Object> get props => [];
}

class GetListStudentUnlock extends ListStudentToUnlockEvent{
  final String classId;
  const GetListStudentUnlock({required this.classId});
}
