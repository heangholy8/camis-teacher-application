part of 'list_student_to_unlock_bloc.dart';

sealed class ListStudentToUnlockState extends Equatable {
  const ListStudentToUnlockState();
  
  @override
  List<Object> get props => [];
}

final class ListStudentToUnlockInitial extends ListStudentToUnlockState {}
final class ListStudentToUnlockLoading extends ListStudentToUnlockState {}
final class ListStudentUnlockSucess extends ListStudentToUnlockState {
  final ListStudentUnlockUsePackage  listStudentUnlockUsePackage;
  const ListStudentUnlockSucess({required this.listStudentUnlockUsePackage});
}
final class ListStudentToUnlockError extends ListStudentToUnlockState {}
