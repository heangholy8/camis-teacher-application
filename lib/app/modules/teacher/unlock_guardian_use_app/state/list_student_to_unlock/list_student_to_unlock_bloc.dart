import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/api/teacher_unlock_api.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/model/list_student_unlock_use_package.dart';
import 'package:equatable/equatable.dart';
part 'list_student_to_unlock_event.dart';
part 'list_student_to_unlock_state.dart';

class ListStudentToUnlockBloc extends Bloc<ListStudentToUnlockEvent, ListStudentToUnlockState> {
  TeacherUnlockGuardianUseAppApi teacherUnlockGuardianUseAppApi;
  ListStudentToUnlockBloc({required this.teacherUnlockGuardianUseAppApi}) : super(ListStudentToUnlockInitial()) {
    on<GetListStudentUnlock>((event, emit) async {
      emit(ListStudentToUnlockLoading());
      try{
         var dataStudnet = await teacherUnlockGuardianUseAppApi.listStudentUserPackageApi(event.classId);
         emit(ListStudentUnlockSucess(listStudentUnlockUsePackage: dataStudnet));
      }catch(e){
        emit(ListStudentToUnlockError());
      }
    });
  }
}
