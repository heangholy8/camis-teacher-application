import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/help/show_toast.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/api/teacher_unlock_api.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/list_guardian_request_unlock/list_guardian_request_unlock_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/list_student_to_unlock/list_student_to_unlock_bloc.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:camis_teacher_application/widget/empydata_widget/data_empy_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_switch/flutter_switch.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../core/resources/asset_resource.dart';
import '../../../../core/thems/thems_constands.dart';

class ListStudentUnlockUseApp extends StatefulWidget {
  final String classId;
  final String role;
  ListStudentUnlockUseApp({super.key,required this.classId,required this.role});
  @override
  State<ListStudentUnlockUseApp> createState() => _ListStudentUnlockUseAppState();
}

class _ListStudentUnlockUseAppState extends State<ListStudentUnlockUseApp>{
  bool connection = true;
  StreamSubscription? sub;
  int intdexPageView = 0;
  PageController? _pageController;
  int numGuardianRequest = 0;
  int _currentPage = 0;
  @override
  void initState() {
    _pageController = PageController(initialPage: intdexPageView);
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 10, left: 22, right: 22, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            ImageAssets.chevron_left,
                            color: Colorconstand.neutralWhite,
                            width: 32,
                            height: 32,
                          ),
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: Text(
                              "UNLOCK_LOCK".tr(),
                              style: ThemsConstands.headline_2_semibold_24 .copyWith(
                                color: Colorconstand.neutralWhite,
                              ),
                              textAlign: TextAlign.center,
                            )
                        ),
                        // Container(
                        //   child: FlutterSwitch(
                        //     activeTextColor: Colorconstand.neutralWhite,
                        //     inactiveTextColor: Colorconstand.primaryColor,
                        //     activeColor: Colorconstand.primaryColor,
                        //     inactiveColor: Colorconstand.neutralGrey,
                        //     activeText: "បើក",
                        //     inactiveText: "CLOSE".tr(),
                        //     value: dataGuardian.data![index].status == 0?false:true,
                        //     valueFontSize: 16.0,
                        //     width: 75,
                        //     borderRadius: 17.5,
                        //     height: 30,
                        //     toggleSize: 35,
                        //     showOnOff: true,
                        //     onToggle: (val) {
                        //       setState(() {
                        //         if(dataStudent[index].isUnlock == false){
                        //           showDatePicker(
                        //           helpText: "ជ្រើសរើសថ្ងៃផុតកំណត់ នៃការប្រើប្រាស់",
                        //           context: context,
                        //           initialDate: DateTime.now(),
                        //           firstDate: DateTime.now(),
                        //           lastDate: DateTime(DateTime.now().year+5),
                        //           ).then((onValue){
                        //             if(onValue != null){
                        //               String formattedDate = DateFormat('dd/MM/yyyy').format(onValue);
                        //               dataStudent[index].isUnlock = !dataStudent[index].isUnlock!;
                        //               TeacherUnlockGuardianUseAppApi().unlockAndLockStudentUserPackageApi(widget.classId, dataStudent[index].isUnlock==true? "1":"0" , dataStudent[index].studentId.toString(), dataStudent[index].guardianId.toString(),formattedDate,"").then((value){
                        //                 if(value == true){
                        //                   BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId:widget.classId,role: "1",status: "0"));
                        //                   BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: widget.classId));
                        //                   showToast("ជោគជ័យ");
                        //                 }
                        //                 else{
                        //                   showToast("មិនជោគជ័យ");
                        //                 }
                        //               });
                        //             }
                        //           });
                        //         }
                        //         else{
                        //           showDatePicker(
                        //           helpText: "ជ្រើសរើសថ្ងៃផុតកំណត់ នៃការបិទប្រើប្រាស់",
                        //           context: context,
                        //           initialDate: DateTime.now(),
                        //           firstDate: DateTime.now(),
                        //           lastDate: DateTime(DateTime.now().year+5),
                        //           ).then((onValue){
                        //             if(onValue != null){
                        //               String formattedDate = DateFormat('dd/MM/yyyy').format(onValue);
                        //               dataStudent[index].isUnlock = !dataStudent[index].isUnlock!;
                        //               TeacherUnlockGuardianUseAppApi().unlockAndLockStudentUserPackageApi(widget.classId, dataStudent[index].isUnlock==true? "1":"0" , dataStudent[index].studentId.toString(), dataStudent[index].guardianId.toString(),"",formattedDate).then((value){
                        //                 if(value == true){
                        //                   BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId:widget.classId,role: "1",status: "0"));
                        //                   BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: widget.classId));
                        //                   showToast("ជោគជ័យ");
                        //                 }
                        //                 else{
                        //                   showToast("មិនជោគជ័យ");
                        //                 }
                        //               });
                        //             }
                        //           });
                        //         }
                        //     });
                        //     },
                        //   ),
                        // )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colorconstand.neutralWhite,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
                        ),
                        child:  Column(
                          children: [
                             Container(
                              height: 50,
                              padding: const EdgeInsets.all(6),
                              margin: const EdgeInsets.only(top: 8,left: 8,right: 8),
                              decoration:  BoxDecoration(
                                  color: Colorconstand.neutralGrey.withOpacity(0.55),
                                  borderRadius:  const BorderRadius.all(Radius.circular(10))),
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                      child: Button_Custom(
                                    buttonColor: _currentPage == 0
                                        ? Colorconstand.primaryColor
                                        : Colorconstand.neutralWhite,
                                    hightButton: 40,
                                    radiusButton: 8,
                                    titleButton: "${"GUARDIAN".tr()} ${"REQUEST".tr()} ",
                                    titlebuttonColor: _currentPage == 0
                                        ? Colorconstand.neutralWhite
                                        : Colorconstand.primaryColor,
                                    onPressed: () {
                                      setState(() {
                                        _currentPage =0;
                                          _pageController!.previousPage(
                                            duration:const Duration(milliseconds: 300),
                                            curve: Curves.easeIn
                                          );
                                      });
                                    },
                                  )),
                                  const SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                      child: Button_Custom(
                                    buttonColor: _currentPage==1
                                        ? Colorconstand.primaryColor
                                        : Colorconstand.neutralWhite,
                                    hightButton: 40,
                                    radiusButton: 8,
                                    titleButton: "STUDENT_IN_CLASS".tr(),
                                    titlebuttonColor: _currentPage==1
                                        ? Colorconstand.neutralWhite
                                        : Colorconstand.primaryColor,
                                    onPressed: () {
                                      setState(() {
                                        _currentPage =1;
                                        _pageController!.nextPage(
                                            duration:const Duration(milliseconds: 300),
                                            curve: Curves.easeIn
                                          );
                                      });
                                    },
                                  ))
                                ],
                              ),
                            ),
                            Expanded(
                              child: PageView(
                                controller: _pageController,
                                onPageChanged: (value) {
                                  setState(() {
                                    intdexPageView = value;
                                  });
                                },
                                children: [
                                  //===========================  guardian request unlock in class ===============
                                  BlocConsumer<ListGuardianRequestUnlockBloc,ListGuardianRequestUnlockState>(
                                    listener: (context, state) {
                                      if(state is ListGuardianRequestUnlockSucess){
                                        var dataGuardian = state.listGuardianRequestUnlock.data;
                                        setState(() {
                                          numGuardianRequest = dataGuardian!.total!;
                                        });
                                      }
                                    },
                                    builder: (context, state) {
                                      if(state is ListGuardianRequestUnlockLoading){
                                        return Container(
                                          child:const Center(child: CircularProgressIndicator()),
                                        );
                                      }
                                      else if(state is ListGuardianRequestUnlockSucess){
                                          var dataGuardian = state.listGuardianRequestUnlock.data;
                                          return dataGuardian!.data!.isEmpty
                                        ? Container(
                                            color: Colorconstand.neutralWhite,
                                            child: DataNotFound(
                                              subtitle: "".tr(),
                                              title: "${"HAVE_NOT".tr()} ${"GUARDIAN".tr()} ${"REQUEST".tr()}",
                                            ),
                                          )
                                        : ListView.separated(
                                         padding:const EdgeInsets.all(18),
                                         itemCount: dataGuardian.data!.length,
                                         separatorBuilder: (context, index) {
                                           return const Padding(
                                             padding: EdgeInsets.only(left: 55),
                                             child:  Divider(color: Colorconstand.neutralGrey,),
                                           );
                                         },
                                         itemBuilder: (context, index) {
                                           return Container(
                                            margin:const EdgeInsets.only(bottom: 0),
                                             child: Row(
                                               children: [
                                                 SizedBox(
                                                  width: 50,
                                                  height: 50,
                                                  child: ClipOval(
                                                    child:  Image.network(
                                                        dataGuardian.data![index].guardian!.profileMedia!.fileShow == "" ||
                                                        dataGuardian.data![index].guardian! .profileMedia!.fileShow == null
                                                          ? dataGuardian.data![index].guardian!.profileMedia! .fileThumbnail.toString()
                                                          : dataGuardian.data![index].guardian!.profileMedia!.fileShow.toString(),
                                                          fit: BoxFit.cover,
                                                      ),
                                                    //   
                                                    // ),
                                                  ),
                                                ),
                                                const SizedBox(width: 12,),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      Text(
                                                        translate=="km" ? "${dataGuardian.data![index].guardian!.name}":dataGuardian.data![index].guardian!.nameEn==""? dataGuardian.data![index].guardian!.name.toString():"${dataGuardian.data![index].guardian!.nameEn}",
                                                        style: ThemsConstands.headline_4_medium_18.copyWith(
                                                          color: Colorconstand.lightBulma,
                                                        ),
                                                      ),
                                                      Text(
                                                        "${"STUDENT_NAME".tr()} : ",
                                                        style: ThemsConstands.headline_6_regular_14_20height.copyWith(
                                                          color: Colorconstand.lightBulma,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: FlutterSwitch(
                                                    activeTextColor: Colorconstand.neutralWhite,
                                                    inactiveTextColor: Colorconstand.primaryColor,
                                                    activeColor: Colorconstand.primaryColor,
                                                    inactiveColor: Colorconstand.neutralGrey,
                                                    activeText: "បើក",
                                                    inactiveText: "CLOSE".tr(),
                                                    value: dataGuardian.data![index].status == 0?false:true,
                                                    valueFontSize: 16.0,
                                                    width: 75,
                                                    borderRadius: 17.5,
                                                    height: 30,
                                                    toggleSize: 35,
                                                    showOnOff: true,
                                                    onToggle: (val) {
                                                      setState(() {
                                                        showDatePicker(
                                                          helpText: "ជ្រើសរើសថ្ងៃផុតកំណត់ នៃការប្រើប្រាស់",
                                                          context: context,
                                                          initialDate: DateTime.now(),
                                                          firstDate: DateTime.now(),
                                                          lastDate: DateTime(DateTime.now().year+5),
                                                          ).then((onValue){
                                                            if(onValue != null){
                                                              String formattedDate = DateFormat('dd/MM/yyyy').format(onValue);
                                                              dataGuardian.data![index].status = 1;
                                                              TeacherUnlockGuardianUseAppApi().unlockGuardianRequestApi(dataGuardian.data![index].id.toString(), dataGuardian.data![index].status.toString(),formattedDate).then((onValue){
                                                                if(onValue == true){
                                                                  BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: widget.classId));
                                                                  BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId:widget.classId,role: widget.role,status: "0"));
                                                                  showToast("ជោគជ័យ");
                                                                }
                                                                else{
                                                                  showToast("មិនជោគជ័យ");
                                                                }
                                                              });
                                                            }
                                                          }
                                                        );
                                                      });
                                                    },
                                                  ),
                                                )
                                               ],
                                             ),
                                           );
                                         },
                                        );
                                      }
                                      else{
                                        return Center(
                                          child: EmptyWidget(
                                            title: "WE_DETECT".tr(),
                                            subtitle: "WE_DETECT_DES".tr(),
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                  BlocBuilder<ListStudentToUnlockBloc,ListStudentToUnlockState>(
                                    builder: (context, state) {
                                      if(state is ListStudentToUnlockLoading){
                                        return Container(
                                          child:const Center(child: CircularProgressIndicator()),
                                        );
                                      }
                                      else if(state is ListStudentUnlockSucess){
                                          var dataStudent = state.listStudentUnlockUsePackage.data;
                                          return dataStudent!.isEmpty
                                        ? Container(
                                            color: Colorconstand.neutralWhite,
                                            child: DataNotFound(
                                              subtitle: "".tr(),
                                              title: "NO_STUDENT".tr(),
                                            ),
                                          )
                                        : ListView.separated(
                                         padding:const EdgeInsets.all(18),
                                         itemCount: dataStudent.length,
                                         separatorBuilder: (context, index) {
                                           return const Padding(
                                             padding: EdgeInsets.only(left: 55),
                                             child:  Divider(color: Colorconstand.neutralGrey,),
                                           );
                                         },
                                         itemBuilder: (context, index) {
                                           return Container(
                                            margin:const EdgeInsets.only(bottom: 0),
                                             child: Row(
                                               children: [
                                                 SizedBox(
                                                  width: 50,
                                                  height: 50,
                                                  child: ClipOval(
                                                    child:  Image.network(
                                                        dataStudent[index].profileMedia!.fileShow == "" ||
                                                        dataStudent[index] .profileMedia!.fileShow == null
                                                          ? dataStudent[index].profileMedia! .fileThumbnail.toString()
                                                          : dataStudent[index].profileMedia!.fileShow.toString(),
                                                          fit: BoxFit.cover,
                                                      ),
                                                    //   
                                                    // ),
                                                  ),
                                                ),
                                                const SizedBox(width: 12,),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      Text(
                                                        translate=="km" ? "${dataStudent[index].name}":dataStudent[index].nameEn==" "? dataStudent[index].name:"${dataStudent[index].nameEn}",
                                                        style: ThemsConstands.headline_4_medium_18.copyWith(
                                                          color: Colorconstand.lightBulma,
                                                        ),
                                                      ),
                                                      Text(
                                                      "${"GUARDIAN".tr()}: ${translate=="km" ? "${dataStudent[index].guardianName}":dataStudent[index].guardianNameEn==" "? dataStudent[index].guardianName:"${dataStudent[index].guardianNameEn}"}",
                                                        style: ThemsConstands.headline_6_regular_14_20height.copyWith(
                                                          color: Colorconstand.lightBulma,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: FlutterSwitch(
                                                    activeTextColor: Colorconstand.neutralWhite,
                                                    inactiveTextColor: Colorconstand.primaryColor,
                                                    activeColor: Colorconstand.primaryColor,
                                                    inactiveColor: Colorconstand.neutralGrey,
                                                    activeText: "បើក",
                                                    inactiveText: "CLOSE".tr(),
                                                    value: dataStudent[index].isUnlock!,
                                                    valueFontSize: 16.0,
                                                    width: 75,
                                                    borderRadius: 17.5,
                                                    height: 30,
                                                    toggleSize: 35,
                                                    showOnOff: true,
                                                    onToggle: (val) {
                                                       setState(() {
                                                          if(dataStudent[index].isUnlock == false){
                                                            showDatePicker(
                                                            helpText: "ជ្រើសរើសថ្ងៃផុតកំណត់ នៃការប្រើប្រាស់",
                                                            context: context,
                                                            initialDate: DateTime.now(),
                                                            firstDate: DateTime.now(),
                                                            lastDate: DateTime(DateTime.now().year+5),
                                                            ).then((onValue){
                                                              if(onValue != null){
                                                                String formattedDate = DateFormat('dd/MM/yyyy').format(onValue);
                                                                dataStudent[index].isUnlock = !dataStudent[index].isUnlock!;
                                                                TeacherUnlockGuardianUseAppApi().unlockAndLockStudentUserPackageApi(widget.classId, dataStudent[index].isUnlock==true? "1":"0" , dataStudent[index].studentId.toString(), dataStudent[index].guardianId.toString(),formattedDate,"").then((value){
                                                                  if(value == true){
                                                                    BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId:widget.classId,role: "1",status: "0"));
                                                                    BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: widget.classId));
                                                                    showToast("ជោគជ័យ");
                                                                  }
                                                                  else{
                                                                    showToast("មិនជោគជ័យ");
                                                                  }
                                                                });
                                                              }
                                                            });
                                                          }
                                                          else{
                                                            showDatePicker(
                                                            helpText: "ជ្រើសរើសថ្ងៃផុតកំណត់ នៃការបិទប្រើប្រាស់",
                                                            context: context,
                                                            initialDate: DateTime.now(),
                                                            firstDate: DateTime.now(),
                                                            lastDate: DateTime(DateTime.now().year+5),
                                                            ).then((onValue){
                                                              if(onValue != null){
                                                                String formattedDate = DateFormat('dd/MM/yyyy').format(onValue);
                                                                dataStudent[index].isUnlock = !dataStudent[index].isUnlock!;
                                                                TeacherUnlockGuardianUseAppApi().unlockAndLockStudentUserPackageApi(widget.classId, dataStudent[index].isUnlock==true? "1":"0" , dataStudent[index].studentId.toString(), dataStudent[index].guardianId.toString(),"",formattedDate).then((value){
                                                                  if(value == true){
                                                                    BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId:widget.classId,role: "1",status: "0"));
                                                                    BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: widget.classId));
                                                                    showToast("ជោគជ័យ");
                                                                  }
                                                                  else{
                                                                    showToast("មិនជោគជ័យ");
                                                                  }
                                                                });
                                                              }
                                                            });
                                                          }
                                                       });
                                                    },
                                                  ),
                                                )
                                               ],
                                             ),
                                           );
                                         },
                                        );
                                      }
                                      else{
                                        return Center(
                                          child: EmptyWidget(
                                            title: "WE_DETECT".tr(),
                                            subtitle: "WE_DETECT_DES".tr(),
                                          ),
                                        );
                                      }
                                    },
                                  ),
       //=========================== End student all in class ===============

       
                                ],
                              ),
                            ),
                          ],
                        ),
                                                        
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          //submitButtonWidget(context),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }
}
