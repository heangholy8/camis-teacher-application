import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/state/current_grade/bloc/current_grade_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/screen/list_student_unlock.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/list_guardian_request_unlock/list_guardian_request_unlock_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher/unlock_guardian_use_app/state/list_student_to_unlock/list_student_to_unlock_bloc.dart';
import 'package:camis_teacher_application/widget/empydata_widget/data_empy_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../core/resources/asset_resource.dart';
import '../../../../core/thems/thems_constands.dart';

class ListClassInGrade extends StatefulWidget {
  bool isTeacher;
  ListClassInGrade({super.key,required this.isTeacher});
  @override
  State<ListClassInGrade> createState() =>
      _ListClassInGradeState();
}

class _ListClassInGradeState extends State<ListClassInGrade>{
  bool connection = true;
  StreamSubscription? sub;
  @override
  void initState() {
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 10, left: 22, right: 22, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            ImageAssets.chevron_left,
                            color: Colorconstand.neutralWhite,
                            width: 32,
                            height: 32,
                          ),
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: Text(
                              "CHOOSECLASS".tr(),
                              style: ThemsConstands.headline_2_semibold_24 .copyWith(
                                color: Colorconstand.neutralWhite,
                              ),
                              textAlign: TextAlign.center,
                            )),
                        const SizedBox(
                          width: 32,
                          height: 32,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colorconstand.neutralWhite,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
                        ),
                        child:  Column(
                          children: [
                            Expanded(
                              child: widget.isTeacher == true? 
                              BlocBuilder<GetClassBloc, GetClassState>(
                                 builder: (context, state) {
                                  if(state is GetClassLoading){
                                    return Container(
                                      child:const Center(child: CircularProgressIndicator()),
                                    );
                                  }
                                  else if(state is GetClassLoaded){
                                    var dataGrade = state.classModel!.data!.teachingClasses!.where((element) => element.isInstructor == 1,).toList();
                                    return dataGrade.isEmpty
                                    ? Container(
                                        color: Colorconstand.neutralWhite,
                                        child: DataNotFound(
                                          subtitle: "".tr(),
                                          title: "NO_CLASS".tr(),
                                        ),
                                      )
                                    : Column(
                                      children: [
                                        Container(
                                          
                                          padding:const EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                            color: Colorconstand.mainColorUnderlayer,
                                            borderRadius: BorderRadius.circular(20)
                                          ),
                                          margin:const EdgeInsets.only(top: 15,left:12,right: 12),
                                          child: GridView.builder(
                                            shrinkWrap: true,
                                            physics:const NeverScrollableScrollPhysics(),
                                            padding:const EdgeInsets.all(.0),
                                              itemCount: dataGrade.length,
                                              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                                              itemBuilder: (BuildContext context, int indexClass) {
                                              return Container(
                                                margin:const EdgeInsets.only(right: 8,left: 8,bottom: 12,top: 12),
                                                child: GestureDetector(
                                                  onTap: () {
                                                    BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId: dataGrade[indexClass].id.toString(),role: "2",status: "0"));
                                                    BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: dataGrade[indexClass].id.toString()));
                                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>  ListStudentUnlockUseApp(classId: dataGrade[indexClass].id.toString(),role: "2",)));
                                                  },
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    decoration:const BoxDecoration(
                                                      color: Colorconstand.primaryColor,
                                                      borderRadius: BorderRadius.all(Radius.circular(25))
                                                    ),
                                                    child: Container(
                                                      child: Text(dataGrade[indexClass].name!.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                                                    ),
                                                
                                                  ),
                                                ),
                                              );
                                            }
                                          ),
                                        ),
                                      ],
                                    );
                                  }
                                  else{
                                    return Center(
                                      child: EmptyWidget(
                                        title: "WE_DETECT".tr(),
                                        subtitle: "WE_DETECT_DES".tr(),
                                      ),
                                    );
                                  }
                                 }
                              )
                              :BlocBuilder<CurrentGradeBloc, CurrentGradeState>(
                                builder: (context, state) {
                                  if(state is CurrentGradeAffaireLoading){
                                    return Container(
                                      child:const Center(child: CircularProgressIndicator()),
                                    );
                                  }
                                  else if( state is CurrentGradeAffaireLoaded){
                                    var dataGrade  = state.currentGradeAffaireModel!.data;
                                    return dataGrade!.isEmpty? 
                                    Container(
                                      margin:const EdgeInsets.only(top: 50),
                                        child: EmptyWidget(
                                          title: "CAPTION_GRADE".tr(),
                                          subtitle: "CAPTION_NO_GRADE".tr(),
                                        ),
                                      )
                                    :Container(
                                      margin:const EdgeInsets.only(left: 12,right: 12,top: 10),
                                      child: ListView.builder(
                                        padding:const EdgeInsets.only(bottom: 25),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount: dataGrade.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                margin:const EdgeInsets.only(top: 15,left: 8),
                                                child: Text("${"GRADED".tr()} ${dataGrade[index].level}",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.left,),
                                              ),
                                              Container(
                                                padding:const EdgeInsets.all(8),
                                                decoration: BoxDecoration(
                                                  color: Colorconstand.mainColorUnderlayer,
                                                  borderRadius: BorderRadius.circular(20)
                                                ),
                                                margin:const EdgeInsets.only(top: 15),
                                                child: GridView.builder(
                                                  shrinkWrap: true,
                                                  physics:const NeverScrollableScrollPhysics(),
                                                  padding:const EdgeInsets.all(.0),
                                                    itemCount: dataGrade[index].classes!.length,
                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                                                    itemBuilder: (BuildContext context, int indexClass) {
                                                    return Container(
                                                      margin:const EdgeInsets.only(right: 8,left: 8,bottom: 12,top: 12),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId: dataGrade[index].classes![indexClass].classId.toString(),role: "6",status: "0"));
                                                          BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: dataGrade[index].classes![indexClass].classId.toString()));
                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>  ListStudentUnlockUseApp(classId:  dataGrade[index].classes![indexClass].classId.toString(),role: "6",)));
                                                        },
                                                        child: Container(
                                                          alignment: Alignment.center,
                                                          decoration:const BoxDecoration(
                                                            color: Colorconstand.primaryColor,
                                                            borderRadius: BorderRadius.all(Radius.circular(25))
                                                          ),
                                                          child: Container(
                                                            child: Text(dataGrade[index].classes![indexClass].className!.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                                                          ),
                                                      
                                                        ),
                                                      ),
                                                    );
                                                  }
                                                ),
                                              ),
                                          
                                            ],
                                          );
                                        },
                                      ),
                                    );
                                  }
                                  else if(state is CurrentGradePricipleLoaded){
                                    var dataGrade  = state.currentGradeAffaireModel!.data;
                                    return dataGrade!.isEmpty? 
                                    Container(
                                      margin:const EdgeInsets.only(top: 50),
                                        child: EmptyWidget(
                                          title: "CAPTION_GRADE".tr(),
                                          subtitle: "CAPTION_NO_GRADE".tr(),
                                        ),
                                      )
                                    :Container(
                                      margin:const EdgeInsets.only(left: 12,right: 12,top: 10),
                                      child: ListView.builder(
                                        padding:const EdgeInsets.only(bottom: 25),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount: dataGrade.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                margin:const EdgeInsets.only(top: 15,left: 8),
                                                child: Text("${"GRADED".tr()} ${dataGrade[index].level}",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.left,),
                                              ),
                                              Container(
                                                padding:const EdgeInsets.all(8),
                                                decoration: BoxDecoration(
                                                  color: Colorconstand.mainColorUnderlayer,
                                                  borderRadius: BorderRadius.circular(20)
                                                ),
                                                margin:const EdgeInsets.only(top: 15),
                                                child: GridView.builder(
                                                  shrinkWrap: true,
                                                  physics:const NeverScrollableScrollPhysics(),
                                                  padding:const EdgeInsets.all(.0),
                                                    itemCount: dataGrade[index].classes!.length,
                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                                                    itemBuilder: (BuildContext context, int indexClass) {
                                                    return Container(
                                                      margin:const EdgeInsets.only(right: 8,left: 8,bottom: 12,top: 12),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          BlocProvider.of<ListGuardianRequestUnlockBloc>(context).add(GetListGuardianRequestUnlock(classId: dataGrade[index].classes![indexClass].classId.toString(),role: "1",status: "0"));
                                                          BlocProvider.of<ListStudentToUnlockBloc>(context).add(GetListStudentUnlock(classId: dataGrade[index].classes![indexClass].classId.toString()));
                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>  ListStudentUnlockUseApp(classId:  dataGrade[index].classes![indexClass].classId.toString(),role: "1",)));
                                                        },
                                                        child: Container(
                                                          alignment: Alignment.center,
                                                          decoration:const BoxDecoration(
                                                            color: Colorconstand.primaryColor,
                                                            borderRadius: BorderRadius.all(Radius.circular(25))
                                                          ),
                                                          child: Container(
                                                            child: Text(dataGrade[index].classes![indexClass].className!.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                                                          ),
                                                      
                                                        ),
                                                      ),
                                                    );
                                                  }
                                                ),
                                              ),
                                          
                                            ],
                                          );
                                        },
                                      ),
                                    );
                                  }
                                  else{
                                    return Center(
                                      child: EmptyWidget(
                                        title: "WE_DETECT".tr(),
                                        subtitle: "WE_DETECT_DES".tr(),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                                                        
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          //submitButtonWidget(context),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }
}
