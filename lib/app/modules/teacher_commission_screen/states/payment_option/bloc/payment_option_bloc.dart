import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import '../../../../../models/teacher_commission_model/payment_option_model.dart';
import '../../../../../service/api/teacher_commission_api/get_payment_option.dart';
part 'payment_option_event.dart';
part 'payment_option_state.dart';

class PaymentOptionBloc extends Bloc<PaymentOptionEvent, PaymentOptionState> {
  final PaymentOptionApi currenApi;
  PaymentOptionBloc({required this.currenApi}) : super(PaymentOptionInitial()) {
    on<GetPaymentOptionEvent>((event, emit) async {
      emit(PaymentOptionLoadingState());
      try {
        var data = await currenApi.getPaymentOptionRequestApi();
        emit(PaymentOptionLoadedState(currentPaymentOption: data));
      } catch (e) {
        emit(PaymentOptionErrorState(error: e.toString()));
      }
    });
  }
}
