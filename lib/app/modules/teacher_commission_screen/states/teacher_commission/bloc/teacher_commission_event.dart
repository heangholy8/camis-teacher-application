part of 'teacher_commission_bloc.dart';

abstract class TeacherCommissionEvent extends Equatable {
  const TeacherCommissionEvent();

  @override
  List<Object> get props => [];
}

class GetCommissionEvent extends TeacherCommissionEvent{
  final String year;
  final String month;
  const GetCommissionEvent({required this.year,required this.month});
}
