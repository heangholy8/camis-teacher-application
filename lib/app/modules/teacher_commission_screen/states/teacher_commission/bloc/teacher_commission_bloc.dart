import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/teacher_commission_api/get_teacher_commission.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/teacher_commission_model/commission_teacher_model.dart';
part 'teacher_commission_event.dart';
part 'teacher_commission_state.dart';

class TeacherCommissionBloc extends Bloc<TeacherCommissionEvent, TeacherCommissionState> {
  final GetCommissionApi dataCommission;
  TeacherCommissionBloc({required this.dataCommission}) : super(TeacherCommissionInitial()) {
      on<GetCommissionEvent>((event, emit) async {
        emit(GetCommissionLoading());
        try {
          var dataCommissionTeacher = await dataCommission
              .getCommissionRequestApi(month: event.month,year: event.year);
          emit(GetCommissionLoaded(teacherCommissionModel: dataCommissionTeacher));
        } catch (e) {
          emit(GetCommissionError());
        }
      });
  }
}
