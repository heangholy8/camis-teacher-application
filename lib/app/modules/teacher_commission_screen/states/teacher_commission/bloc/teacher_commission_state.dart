part of 'teacher_commission_bloc.dart';

abstract class TeacherCommissionState extends Equatable {
  const TeacherCommissionState();
  
  @override
  List<Object> get props => [];
}

class TeacherCommissionInitial extends TeacherCommissionState {}

class GetCommissionLoading extends TeacherCommissionState{}

class GetCommissionLoaded extends TeacherCommissionState {
  final TeacherCommissionModel? teacherCommissionModel;
  const GetCommissionLoaded({required this.teacherCommissionModel});
}

class GetCommissionError extends TeacherCommissionState {}
