part of 'create_payment_bloc.dart';

abstract class CreatePaymentEvent extends Equatable {
  const CreatePaymentEvent();

  @override
  List<Object> get props => [];
}

class CreatePaymentCashEvent extends CreatePaymentEvent {
  final String studentId;
  final String guardianId;
  final String paymentType;
  final String classId;
  const CreatePaymentCashEvent({required this.studentId,required this.guardianId,required this.paymentType,required this.classId});
}

class CreatePaymentBakongEvent extends CreatePaymentEvent {
  final String studentId;
  final String guardianId;
  final String paymentType;
  final String classId;
  const CreatePaymentBakongEvent({required this.studentId,required this.guardianId,required this.paymentType,required this.classId});
}