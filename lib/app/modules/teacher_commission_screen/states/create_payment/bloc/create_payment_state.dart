part of 'create_payment_bloc.dart';

abstract class CreatePaymentState extends Equatable {
  const CreatePaymentState();
  
  @override
  List<Object> get props => [];
}

class CreatePaymentInitial extends CreatePaymentState {}

class CreatePaymentLoading extends CreatePaymentState{}

class CreatePaymentCashLoaded extends CreatePaymentState {
  final CreatePaymentCashModel? createPaymentCash;
  const CreatePaymentCashLoaded({required this.createPaymentCash});
}

class CreatePaymentBakongLoaded extends CreatePaymentState {
  final CreatePaymentBakongModel? createPaymentBakong;
  const CreatePaymentBakongLoaded({required this.createPaymentBakong});
}

class CreatePaymentError extends CreatePaymentState {}
