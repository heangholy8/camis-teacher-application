import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/teacher_commission_api/create_payment.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/teacher_commission_model/create_payment_bakong_model.dart';
import '../../../../../models/teacher_commission_model/create_payment_cash_model.dart';
part 'create_payment_event.dart';
part 'create_payment_state.dart';

class CreatePaymentBloc extends Bloc<CreatePaymentEvent, CreatePaymentState> {
  final CreatePaymentApi createPayment;
  CreatePaymentBloc({ required this.createPayment,}) : super(CreatePaymentInitial()) {
    on<CreatePaymentCashEvent>((event, emit) async {
        emit(CreatePaymentLoading());
        try {
          var dataCreatePaymentCash = await createPayment.getPaymentCreateCashApi(guardianId: event.guardianId,studentId: event.studentId,paymentType: event.paymentType,classId: event.classId);
          emit(CreatePaymentCashLoaded(createPaymentCash: dataCreatePaymentCash));
        } catch (e) {
          emit(CreatePaymentError());
        }
    });

    on<CreatePaymentBakongEvent>((event, emit) async {
        emit(CreatePaymentLoading());
        try {
          var dataCreatePaymentBakong = await createPayment.getPaymentCreateBakongApi(guardianId: event.guardianId,studentId: event.studentId,paymentType: event.paymentType,classId: event.classId);
          emit(CreatePaymentBakongLoaded(createPaymentBakong: dataCreatePaymentBakong));
        } catch (e) {
          emit(CreatePaymentError());
        }
    });
  }
}
