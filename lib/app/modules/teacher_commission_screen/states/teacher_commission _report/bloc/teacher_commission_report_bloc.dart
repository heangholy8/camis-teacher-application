import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/teacher_commission_model/commission_teacher_model.dart';
import '../../../../../service/api/teacher_commission_api/get_teacher_commission.dart';

part 'teacher_commission_report_event.dart';
part 'teacher_commission_report_state.dart';

class TeacherCommissionReportBloc extends Bloc<TeacherCommissionReportEvent, TeacherCommissionReportState> {
  final GetCommissionApi dataCommissionReport;
  TeacherCommissionReportBloc({required this.dataCommissionReport}) : super(TeacherCommissionReportInitial()) {
      on<GetCommissionReportEvent>((event, emit) async {
        emit(GetCommissionReportLoading());
        try {
          var dataCommissionReportTeacher = await dataCommissionReport
              .getCommissionRequestApi(month: event.month,year: event.year);
          emit(GetCommissionReportLoaded(teacherCommissionReportModel: dataCommissionReportTeacher));
        } catch (e) {
          emit(GetCommissionReportError());
        }
      });
  }
}
