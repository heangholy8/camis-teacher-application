part of 'teacher_commission_report_bloc.dart';

abstract class TeacherCommissionReportState extends Equatable {
  const TeacherCommissionReportState();
  
  @override
  List<Object> get props => [];
}

class TeacherCommissionReportInitial extends TeacherCommissionReportState {}

class GetCommissionReportLoading extends TeacherCommissionReportState{}

class GetCommissionReportLoaded extends TeacherCommissionReportState {
  final TeacherCommissionModel? teacherCommissionReportModel;
  const GetCommissionReportLoaded({required this.teacherCommissionReportModel});
}

class GetCommissionReportError extends TeacherCommissionReportState {}
