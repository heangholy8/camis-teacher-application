part of 'teacher_commission_report_bloc.dart';

abstract class TeacherCommissionReportEvent extends Equatable {
  const TeacherCommissionReportEvent();

  @override
  List<Object> get props => [];
}
class GetCommissionReportEvent extends TeacherCommissionReportEvent{
  final String year;
  final String month;
  const GetCommissionReportEvent({required this.year,required this.month});
}