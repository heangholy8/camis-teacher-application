part of 'student_list_payment_bloc.dart';

abstract class StudentListPaymentState extends Equatable {
  const StudentListPaymentState();
  
  @override
  List<Object> get props => [];
}

class StudentListPaymentInitial extends StudentListPaymentState {}

class GetStudentListPaymentLoading extends StudentListPaymentState{}

class GetStudentListPaymentLoaded extends StudentListPaymentState {
  final ListStudentPaymentModel? studentListPaymentModel;
  const GetStudentListPaymentLoaded({required this.studentListPaymentModel});
}

class GetStudentListPaymentError extends StudentListPaymentState {}
