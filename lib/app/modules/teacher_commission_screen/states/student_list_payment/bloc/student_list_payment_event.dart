part of 'student_list_payment_bloc.dart';

abstract class StudentListPaymentEvent extends Equatable {
  const StudentListPaymentEvent();

  @override
  List<Object> get props => [];
}

class GetStudentListPaymentEvent extends StudentListPaymentEvent{
  final String idClass;
  const GetStudentListPaymentEvent({required this.idClass});
}
