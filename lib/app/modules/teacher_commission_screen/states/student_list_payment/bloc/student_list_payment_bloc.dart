import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../models/teacher_commission_model/student_list_payment_free_model.dart';
import '../../../../../service/api/teacher_commission_api/get_student_list_payment.dart';
part 'student_list_payment_event.dart';
part 'student_list_payment_state.dart';

class StudentListPaymentBloc extends Bloc<StudentListPaymentEvent, StudentListPaymentState> {
  final GetStudentListPaymentApi dataStudentListPayment;
  StudentListPaymentBloc({required this.dataStudentListPayment}) : super(StudentListPaymentInitial()) {
    on<GetStudentListPaymentEvent>((event, emit) async {
      emit(GetStudentListPaymentLoading());
      try {
        var datastudentPayment = await dataStudentListPayment
            .getStudentListPaymentRequestApi(idClass: event.idClass);
            print(datastudentPayment);
        emit(GetStudentListPaymentLoaded(studentListPaymentModel: datastudentPayment));
      } catch (e) {
        emit(GetStudentListPaymentError());
      }
    });
  }
}
