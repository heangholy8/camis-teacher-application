part of 'post_attachment_bloc.dart';

abstract class PostAttachmentEvent extends Equatable {
  const PostAttachmentEvent();

  @override
  List<Object> get props => [];
}



class PostAttachmentCommissionEvent extends PostAttachmentEvent{
  final List<File> attachment;
  final String year;
  final String month;

  const PostAttachmentCommissionEvent({required this.attachment,required this.year,required this.month});
}