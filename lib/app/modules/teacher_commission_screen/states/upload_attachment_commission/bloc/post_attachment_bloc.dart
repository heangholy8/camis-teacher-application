import 'dart:io';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../service/api/teacher_commission_api/upload_attachment_api.dart';

part 'post_attachment_event.dart';
part 'post_attachment_state.dart';

class PostAttachmentBloc extends Bloc<PostAttachmentEvent, PostAttachmentState> {
  final UploadAttachmentCommissionApi uploadAttachment;
  PostAttachmentBloc({required this.uploadAttachment}) : super(PostAttachmentInitial()) {
    on<PostAttachmentCommissionEvent>((event, emit)async {
      emit(PostAttachmentLoading());
      try {
        await uploadAttachment.uploadAttachmentCommissionApi(attachments: event.attachment, year: event.year, month: event.month);
        emit(PostAttachmentLoaded());
      } catch (e) {
        emit(PostAttachmentError());
      }
    });
  }
}
