part of 'post_attachment_bloc.dart';

abstract class PostAttachmentState extends Equatable {
  const PostAttachmentState();
  
  @override
  List<Object> get props => [];
}

class PostAttachmentInitial extends PostAttachmentState {}

class PostAttachmentLoading extends PostAttachmentState{}
class PostAttachmentLoaded extends PostAttachmentState{
  
}
class PostAttachmentError extends PostAttachmentState{}
