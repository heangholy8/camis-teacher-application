import 'dart:async';
import 'dart:ui';

import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/teacher_commission/bloc/teacher_commission_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/views/teacher_finance_report_screen.dart';
import 'package:camis_teacher_application/app/routes/app_route.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../widget/popup_detail_payment.dart';

class TeacherFinanceScreen extends StatefulWidget {
  const TeacherFinanceScreen({super.key});

  @override
  State<TeacherFinanceScreen> createState() => _TeacherFinanceScreenState();
}

class _TeacherFinanceScreenState extends State<TeacherFinanceScreen> {
  bool connection = true;
  StreamSubscription? sub;
  bool isEmptyState = false;
  String endDate = '';
  String startDate = '';
  int? monthlocal;
  int? yearchecklocal;
  final oCcy =  NumberFormat("#,##0", "en_US");

    getCurrentDate(){
        final now = DateTime.now();

        var date = DateTime(now.year, now.month+1, 0).toString();

        var dateParse = DateTime.parse(date);

        var formattedDate = "${dateParse.day}/${dateParse.month}/${dateParse.year}";
        setState(() {
            endDate = formattedDate.toString() ;
            startDate = "${"01"}/${dateParse.month}/${dateParse.year}";
        });
    }
  @override
  void initState() {
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
            //  BlocProvider.of<GetClassBloc>(context).add(GetClass());
          } else {}
        });
      });
    getCurrentDate();
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
    super.initState();

    BlocProvider.of<TeacherCommissionBloc>(context).add(GetCommissionEvent(month: monthlocal.toString(),year: yearchecklocal.toString()),);
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset("assets/images/Path_20.png"),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Image.asset("assets/images/Path_19.png"),
          ),
          Positioned(
            bottom: 30,
            right: 0,
            child: Image.asset("assets/images/Path_18.png"),
          ),
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
              child: SafeArea(
                bottom: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal:0),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 10,bottom: 20,left: 18,right: 18),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: SvgPicture.asset(
                                ImageAssets.chevron_left,
                                color: Colorconstand.primaryColor,
                                width: 32,
                                height: 32,
                              ),
                            ),
                            Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "PACKAGESERVICE".tr(),
                                  style: ThemsConstands.headline_2_semibold_24 .copyWith(
                                    color: Colorconstand.primaryColor,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context)=> TeacherFinanceReportScreen()));
                              },
                              child: SvgPicture.asset(
                                ImageAssets.document,
                                color: Colorconstand.primaryColor,
                                width: 32,
                                height: 32,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal:18),
                          child: BlocBuilder<TeacherCommissionBloc, TeacherCommissionState>(
                            builder: (context, state) {
                              if(state is GetCommissionLoading){
                                 return Column(
                                   children: [
                                    Expanded(child: Container()),
                                    Container(
                                      height: 35,width: 35,
                                      alignment: Alignment.center,
                                      child: const CircularProgressIndicator(),
                                    ),
                                    Expanded(child: Container()),
                                   ],
                                 );
                              }
                              else if(state is GetCommissionLoaded){
                                var dataTeacherCommission = state.teacherCommissionModel!.data;
                                var dataCommissionItem = dataTeacherCommission!.items;
                                var dataCommissionItemCash;
                                var dataCommissionItemBank;
                                if(dataCommissionItem!.isNotEmpty){
                                    dataCommissionItemCash = dataCommissionItem.where((element) => element.paymentMethod == "CASH").toList();
                                    dataCommissionItemBank = dataCommissionItem.where((element) => element.paymentMethod != "CASH").toList();
                                }
                                return SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      financeCardWidget(context,dataTeacherCommission.totalCommission.toString(),dataTeacherCommission.totalIncome.toString()), 
                                      const SizedBox(height: 12,),    
                                      Divider(
                                        height: 1,
                                        thickness: 1,
                                        color: Colorconstand.mainColorSecondary.withOpacity(0.2),
                                      ),    
                                      SizedBox(height:dataCommissionItem.isEmpty?0:16,),  
                                      dataTeacherCommission.items!.isEmpty?Container():Text("REVENUE".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.primaryColor),),
                                      Container(
                                        child: dataTeacherCommission.items!.isEmpty?
                                        Container(
                                          margin:const EdgeInsets.only(top: 50,left: 28,right: 28),
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Text("NOT_OPERATION_YET".tr(),style: ThemsConstands.headline3_medium_20_26height.copyWith(fontWeight: FontWeight.bold)),
                                              const  SizedBox(height: 8,),
                                              Text("DES_NOT_OPERATION_YET".tr(), style: ThemsConstands.headline6_medium_14.copyWith(color: Colorconstand.neutralDarkGrey,height: 2),textAlign: TextAlign.center,),
                                            ],
                                          ),
                                        ) : Container(
                                          margin:const EdgeInsets.symmetric(vertical: 15),
                                          child: Column(
                                            children: [
                                              dataCommissionItemCash.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    padding:const EdgeInsets.symmetric(vertical: 12),
                                                    width: MediaQuery.of(context).size.width,
                                                    decoration: BoxDecoration(
                                                      color:const Color(0xFF7EBBFD).withOpacity(0.25),
                                                      borderRadius: BorderRadius.circular(12)
                                                    ),
                                                    child: Text("CASH_PAYMENT_VAI_TEACHER".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                                  ),
                                                  ListView.separated(
                                                    itemCount: dataCommissionItemCash.length,
                                                    physics:const NeverScrollableScrollPhysics(),
                                                    padding: const EdgeInsets.all(0),
                                                    shrinkWrap: true,
                                                    itemBuilder: (context, index){
                                                      String formattedDate = DateFormat('dd/MM/yyyy').format(dataCommissionItemCash[index].createdAt);
                                                      return Container(
                                                        height: 45,
                                                        child: MaterialButton(
                                                          splashColor: Colors.transparent,
                                                          highlightColor: Colors.transparent,
                                                          hoverColor: Colors.transparent,
                                                          focusColor: Colors.transparent,
                                                          padding:const EdgeInsets.all(0),
                                                          onPressed: (){
                                                            showModalBottomSheet(
                                                              backgroundColor: Colors.transparent,
                                                              isScrollControlled: true,
                                                                shape: const RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                                                                ),
                                                                context: context,
                                                                builder: (context) {
                                                                  return SingleChildScrollView(
                                                                    child: Container(
                                                                      margin:const EdgeInsets.only(top: 50),
                                                                      decoration: const BoxDecoration(
                                                                        color: Colors.white,
                                                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
                                                                      child: PopUpDetailStudentPayment(
                                                                        classname: dataCommissionItemCash[index].className.toString(),
                                                                        guardianQr: dataCommissionItemCash[index].guardianQrUrl.toString(), 
                                                                        paymentDate: formattedDate, 
                                                                        paymentOption: translate == "km"?dataCommissionItemCash[index].guardianPayment.choosePayOptionName.toString():dataCommissionItemCash[index].guardianPayment.choosePayOptionNameEn.toString(),
                                                                        pricePayment: dataCommissionItemCash[index].amount.toString(), 
                                                                        studentName: dataCommissionItemCash[index].studentName.toString(), 
                                                                        studentQr: dataCommissionItemCash[index].studentQrUrl.toString(),
                                                                        btnClose: (){
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                      ),
                                                                    ),
                                                                  );
                                                                }
                                                              );
                                                          },
                                                           child: Container(
                                                            height: 45,
                                                            child: Row(
                                                              children: [
                                                                Expanded(
                                                                  child: Row(
                                                                    children: [
                                                                      Container(
                                                                        child: Text(formattedDate,style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                      Container(
                                                                        margin:const EdgeInsets.symmetric(horizontal: 8),
                                                                        child: SvgPicture.asset(ImageAssets.dollar_circle_icon)
                                                                      ),
                                                                      Container(
                                                                        child: Text(dataCommissionItemCash[index].studentName.toString(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  child: Text("${oCcy.format(int.parse(dataCommissionItemCash[index].amount.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                ),
                                                              ],
                                                            ),
                                                           ),
                                                        ),
                                                      );       
                                                    },
                                                    separatorBuilder: (context,index){
                                                      return Divider(
                                                        height: 1,
                                                        color: Colorconstand.neutralDarkGrey.withOpacity(.4),
                                                      );
                                                    },
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Container(
                                                          margin:const EdgeInsets.only(right: 15),
                                                          child: Text("TOTAL".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              dataCommissionItemBank.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    padding:const EdgeInsets.symmetric(vertical: 12),
                                                    width: MediaQuery.of(context).size.width,
                                                    decoration: BoxDecoration(
                                                      color:const Color(0xFF7EBBFD).withOpacity(0.25),
                                                      borderRadius: BorderRadius.circular(12)
                                                    ),
                                                    child: Text("CASH_PAYMENT_VAI_COMPANY".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                                  ),
                                                  ListView.separated(
                                                    itemCount: dataCommissionItemBank.length,
                                                    physics:const NeverScrollableScrollPhysics(),
                                                    padding: const EdgeInsets.all(0),
                                                    shrinkWrap: true,
                                                    itemBuilder: (context, index){
                                                      String formattedDate = DateFormat('dd/MM/yyyy').format(dataCommissionItemBank[index].createdAt);
                                                      return Container(
                                                        height: 45,
                                                        child: MaterialButton(
                                                          splashColor: Colors.transparent,
                                                          highlightColor: Colors.transparent,
                                                          hoverColor: Colors.transparent,
                                                          focusColor: Colors.transparent,
                                                          padding:const EdgeInsets.all(0),
                                                          onPressed: (){
                                                            showModalBottomSheet(
                                                              backgroundColor: Colors.transparent,
                                                              isScrollControlled: true,
                                                                shape: const RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                                                                ),
                                                                context: context,
                                                                builder: (context) {
                                                                  return SingleChildScrollView(
                                                                    child: Container(
                                                                      margin:const EdgeInsets.only(top: 50),
                                                                      decoration: const BoxDecoration(
                                                                        color: Colors.white,
                                                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
                                                                      child: PopUpDetailStudentPayment(
                                                                        classname: dataCommissionItemBank[index].className.toString(), 
                                                                        guardianQr: dataCommissionItemBank[index].guardianQr.toString(), 
                                                                        paymentDate: formattedDate, 
                                                                        paymentOption: translate == "km"?dataCommissionItemBank[index].guardianPayment.choosePayOptionName.toString():dataCommissionItemBank[index].guardianPayment.choosePayOptionNameEn.toString(),
                                                                        pricePayment: dataCommissionItemBank[index].amount.toString(), 
                                                                        studentName: dataCommissionItemBank[index].studentName.toString(), 
                                                                        studentQr: dataCommissionItemBank[index].studentQr.toString(),
                                                                        btnClose: (){
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                      ),
                                                                    ),
                                                                  );
                                                                }
                                                              );
                                                          },
                                                           child: Container(
                                                            height: 45,
                                                            child: Row(
                                                              children: [
                                                                Expanded(
                                                                  child: Row(
                                                                    children: [
                                                                      Container(
                                                                        child: Text(formattedDate,style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                      Container(
                                                                        margin:const EdgeInsets.symmetric(horizontal: 8),
                                                                        child: SvgPicture.asset(ImageAssets.pay_company_icon)
                                                                      ),
                                                                      Container(
                                                                        child: Text(dataCommissionItemBank[index].studentName.toString(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  child: Text("${oCcy.format(int.parse(dataCommissionItemBank[index].amount.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                ),
                                                              ],
                                                            ),
                                                           ),
                                                        ),
                                                      );       
                                                    },
                                                    separatorBuilder: (context,index){
                                                      return Divider(
                                                        height: 1,
                                                        color: Colorconstand.neutralDarkGrey.withOpacity(.4),
                                                      );
                                                    },
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Container(
                                                          margin:const EdgeInsets.only(right: 15),
                                                          child: Text("TOTAL".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalIncome.toString())- int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              Container(
                                                padding:const EdgeInsets.symmetric(vertical: 12),
                                                width: MediaQuery.of(context).size.width,
                                                decoration: BoxDecoration(
                                                  color:const Color(0xFF7EBBFD).withOpacity(0.25),
                                                  borderRadius: BorderRadius.circular(12)
                                                ),
                                                child: Text("TOTAL_TASK_INCOME_TEACHER".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                              ),
              
                                              const SizedBox(height: 12,),
                                              
                                              dataCommissionItemCash.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_CASH_PAYMENT_VAI_TEACHER".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
              
                                              dataCommissionItemBank.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_CASH_PAYMENT_VAI_COMPANY".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalIncome.toString())-int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  ),
                                                ],
                                              ),
              
                                              Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_ALL_SEVICE_REVENUE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBulma),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalIncome.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBulma),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
              
                                              Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("COMMISSION_PERCENTAGE".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                        Container(
                                                          child: Text("${dataTeacherCommission.commissionPercentage.toString()}%",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_TASK_INCOME_TEACHER".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalCommission.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 2,
                                                      color: Colorconstand.primaryColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }
                              else{
                                return Column(
                                  children: [
                                    Expanded(child: Container()),
                                    Container(
                                      alignment: Alignment.center,
                                      child: EmptyWidget(
                                        title: "WE_DETECT".tr(),
                                        subtitle: "WE_DETECT_DES".tr(),
                                      ),
                                    ),
                                    Expanded(child: Container()),
                                  ],
                                );
                              }
                            },
                          ),
                        ),
                      ),
                      subscriptionBtnWidget(context,onTap: (){
                        Navigator.pushNamed(context, Routes.STUDENTFEELIST);
                      }),
                    ],
                  ),
                ),
              ),
            ),
          ),
          //================ Internet offilne ========================
            connection == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
        ],
      ),
    );
  }

  Widget subscriptionBtnWidget(BuildContext context,{void Function()? onTap}) {
    return Container(
      padding:  const EdgeInsets.symmetric(vertical: 18,horizontal: 18),
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8))
      ),
      child: 
      MaterialButton(
        color: Colorconstand.primaryColor,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12))),
        padding: const EdgeInsets.symmetric( vertical: 18, horizontal: 28),
        onPressed: onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(ImageAssets.card_add_icon),
           const SizedBox(width: 8,),
            Text("PAYMENTPLAN".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite)),
          ],
        ),
      ),
   
    );
  }

  Widget financeCardWidget(BuildContext context,String totalCommission,String totalIncome) {
    return Container(
          decoration: BoxDecoration(
            gradient: const LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
              Color(0xFF4D9EF6),
              Color(0xFF2C70C7),
              Color(0xFF1855AB),
            ]),
            borderRadius: BorderRadius.circular(16)
          ),
          width: MediaQuery.of(context).size.width,
          child: MaterialButton(
            padding:const EdgeInsets.all(0),
            onPressed: (){
                  // BlocProvider.of<PaymentDetailBloc>(
                  //         context)
                  //     .add(GetPaymentDetailEvent(
                  //         paymentDetailId:currentPay.id!));
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> TeacherFinanceReportScreen()));
            },
            child: Column(
              children: [
                SizedBox(
                  height: 135,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: [
                        Positioned(
                          left: -10,
                          bottom: -20,
                          child: Container(
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colorconstand.neutralWhite.withOpacity(0.1)
                            ),
                            child: Container(
                              padding:const EdgeInsets.all(18),
                              child: SvgPicture.asset(ImageAssets.card_tick_outline_icon,color: Colorconstand.neutralWhite.withOpacity(0.2),),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 0,left: 25,bottom: 0),
                          child: Row(
                            children: [
                              Container(
                                margin:const EdgeInsets.only(top: 12),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text("TOTALSTUDENTFEE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text("COMMISSIONFEE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.exam),),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text("DURATIONFINANCE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralGrey),),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  margin:const EdgeInsets.only(left: 25),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            children: [
                                              Text(oCcy.format(int.parse(totalIncome)).toString(),style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),),
                                              const SizedBox(width: 14,),
                                              Text("៛".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            children: [
                                              Text(oCcy.format(int.parse(totalCommission)).toString(),style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.exam),),
                                              const SizedBox(width: 8,),
                                              Text("៛".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.exam),),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 10,),
                                      Expanded(
                                        child: Container(
                                          child: Text("${startDate.toString()} ${"TO".tr()} ${endDate.toString()}".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.left,),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
  }
}