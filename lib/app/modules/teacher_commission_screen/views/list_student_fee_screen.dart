import 'dart:async';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/views/payment_option_screen.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../student_list_screen/widget/body_shimmer_widget.dart';
import '../../student_list_screen/widget/header_shimmer_widget.dart';
import '../states/student_list_payment/bloc/student_list_payment_bloc.dart';

class StudentFeeListScreen extends StatefulWidget {
  const StudentFeeListScreen({super.key});
  @override
  State<StudentFeeListScreen> createState() => _StudentFeeListScreenState();
}

class _StudentFeeListScreenState extends State<StudentFeeListScreen> {
  bool checkedbox = false;
  bool connection = true;
  StreamSubscription? sub;
  int isSelectedClass = 0;
  bool isFirstReload = false;
  bool findStudentPayment = false;
  String? classId,studentId,guardianId,className,studentName;

  @override
  void initState() {
     BlocProvider.of<GetClassBloc>(context).add(GetClass());
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
            //  BlocProvider.of<GetClassBloc>(context).add(GetClass());
          } else {}
        });
      });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 10, left: 22, right: 22, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            ImageAssets.chevron_left,
                            color: Colorconstand.neutralWhite,
                            width: 32,
                            height: 32,
                          ),
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: Text("MAKEPAYMENTFORSTUDENT".tr(),
                              style: ThemsConstands.headline_2_semibold_24.copyWith(
                                color: Colorconstand.neutralWhite,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if(findStudentPayment == false){
                                setState(() {
                                  findStudentPayment = true;
                                });
                              }
                              else{
                                setState(() {
                                  findStudentPayment = false;
                                });
                              }
                            },
                          child: SvgPicture.asset(
                            findStudentPayment ==true? ImageAssets.fill_verify_icon: ImageAssets.card_verify_icon,
                            color: Colorconstand.neutralWhite,
                            width: 32,
                            height: 32,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colorconstand.neutralWhite,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
                        ),
                        child:bodyWidget(translate: translate),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          //submitButtonWidget(context),
          //================ Internet offilne ========================
            connection == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
        ],
      ),
    );
  }
  Widget bodyWidget({required var translate}) {
    return BlocConsumer<GetClassBloc, GetClassState>(
      listener: (context, state) {
        if(state is GetClassLoaded){
          var data = state.classModel!.data;
          var listofInstructor = data!.teachingClasses!.where((element) => element.isInstructor==1).toList();
            setState(() {
                classId = listofInstructor[0].id.toString();
                className = listofInstructor[0].name.toString();
            });
        }
      },
      builder: (context, state) {
        if (state is GetClassLoading) {
          return const HeaderShimmerWidget();
        }
        else if (state is GetClassLoaded) {
          var data = state.classModel!.data;
          var listofInstructor = data!.teachingClasses!.where((element) => element.isInstructor==1).toList();
          if (isFirstReload != true) {
            BlocProvider.of<StudentListPaymentBloc>(context).add(
              GetStudentListPaymentEvent(idClass: listofInstructor[0].id.toString()),
            );
          }
          isFirstReload = true;
          return data.teachingClasses!.isEmpty ? 
           ScheduleEmptyWidget(
              title: "NOT_YET_AVAILABLE".tr(),
              subTitle: "EMPTHY_CONTENT_DES".tr(),
            ):Column(
              children: [
                Container(
                decoration: BoxDecoration(
                  color: Colorconstand.primaryColor.withOpacity(0.9),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                  ),
                ),
                height:55,
                child: listofInstructor.length==1?Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                      translate == 'km'
                        ? "${listofInstructor[0].name}"
                        : "${listofInstructor[0].nameEn}",
                    style: ThemsConstands.headline3_semibold_20.copyWith(
                      color: Colorconstand.neutralWhite,
                    ),textAlign: TextAlign.center,
                  ),
                )
                :ListView.separated(
                  separatorBuilder: (context, index) {
                    return Container(
                      width: 8,
                    );
                  },
                  scrollDirection: Axis.horizontal,
                  itemCount: listofInstructor.length,
                  itemBuilder: (context, indexClass) {
                    return InkWell(
                      onTap:connection == false
                        ? (){}
                        : (){
                          BlocProvider.of<StudentListPaymentBloc>(context).add(
                            GetStudentListPaymentEvent(idClass: listofInstructor[indexClass].id.toString()),
                          );
                          setState(() {
                            classId = listofInstructor[indexClass].id.toString();
                            className = listofInstructor[indexClass].name.toString();
                          });
                        },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                        decoration: BoxDecoration(
                          color: isSelectedClass == indexClass
                              ? Colorconstand.neutralWhite
                              : Colors.transparent,
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: SvgPicture.asset(
                                ImageAssets.ranking_icon,
                                width: 23,
                                color: isSelectedClass == indexClass
                                    ? Colorconstand.primaryColor
                                    : Colorconstand.screenWhite,
                              ),
                            ),
                            Text(
                              translate == 'km'
                                  ? "${listofInstructor[indexClass].name}"
                                  : "${listofInstructor[indexClass].nameEn}",
                              style: ThemsConstands.headline_5_medium_16.copyWith(
                                fontWeight: FontWeight.bold,
                                color: isSelectedClass == indexClass
                                    ? Colorconstand.primaryColor
                                    : Colorconstand.neutralWhite,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
               ),
               BlocConsumer<StudentListPaymentBloc, StudentListPaymentState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is GetStudentListPaymentLoading) {
                    return const BodyShimmerWidget();
                  }
                  else if (state is GetStudentListPaymentLoaded) {
                    var data = state.studentListPaymentModel!.data;
                    var listStudentPayment;
                    var listStudentNotPayment;
                    if(data!.isNotEmpty){
                      listStudentPayment = data.where((element) => element.appPaymentExpire!=null).toList();
                      listStudentNotPayment = data.where((element) => element.appPaymentExpire==null).toList();
                    }
                    return Expanded(
                      child: data.isEmpty
                          ?  Column(
                            children: [
                              Expanded(child: Container()),
                              ScheduleEmptyWidget(
                                  title: "NOT_YET_AVAILABLE".tr(),
                                  subTitle: "EMPTHY_CONTENT_DES".tr(),
                                ),
                              Expanded(child: Container()),
                            ],
                          )
                          :Column(
                            children: [
                              listStudentNotPayment.isEmpty || findStudentPayment == true ?Container():Container(
                                height: 25,
                                color: const Color(0xFFFF7575),
                                child: Container(
                                  margin:const EdgeInsets.symmetric(horizontal: 18),
                                  child: Row(
                                    children: [
                                      Container(
                                        margin:const EdgeInsets.only(right: 10),
                                        child: SvgPicture.asset(
                                          ImageAssets.student_not_paid_icon,
                                          width: 18,
                                          color: Colorconstand.screenWhite,
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Text("NOT_PAY_SEVICE".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralWhite),),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      listStudentNotPayment.isEmpty || findStudentPayment == true ?Container():Column(
                                        children: [
                                          ListView.separated(
                                              shrinkWrap: true,
                                              physics:const NeverScrollableScrollPhysics(),
                                              padding: const EdgeInsets.all(0),
                                              itemCount: listStudentNotPayment.length,
                                              itemBuilder: (context, indexstu) {
                                                return MaterialButton(
                                                      onPressed:() {
                                                        studentId = listStudentNotPayment[indexstu].studentId.toString();
                                                        guardianId = listStudentNotPayment[indexstu].guardianId.toString();
                                                        studentName = listStudentNotPayment[indexstu].name.toString();
                                                        showModalBottomSheet(
                                                          backgroundColor: Colors.transparent,
                                                          isScrollControlled: true,
                                                          isDismissible: false,
                                                          enableDrag: false,
                                                          shape: const RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                                                          ),
                                                          context: context,
                                                          builder: (context) {
                                                            return confirmPolicyWidget(
                                                              studentName: studentName,
                                                              guardianName: listStudentNotPayment[indexstu].guardianName.toString()
                                                            );
                                                          }
                                                        );
                                                      },
                                                      padding: const EdgeInsets.symmetric(  vertical: 8, horizontal: 22),
                                                      child: Row(
                                                        children: [
                                                          SizedBox(
                                                            width: 70,
                                                            height: 70,
                                                            child: ClipOval(
                                                              child: Image.network(
                                                                  listStudentNotPayment[indexstu].profileMedia!.fileShow == "" ||
                                                                  listStudentNotPayment[indexstu] .profileMedia!.fileShow == null
                                                                    ? listStudentNotPayment[indexstu].profileMedia! .fileThumbnail.toString()
                                                                    : listStudentNotPayment[indexstu].profileMedia!.fileShow.toString(),
                                                                
                                                                fit: BoxFit.cover,
                                                              ),
                                                            ),
                                                          ),
                                                          Expanded(
                                                            child: Container(
                                                              margin: const EdgeInsets.only(left: 18, right: 0),
                                                              child: Text(
                                                                translate=="km" ? "${listStudentNotPayment[indexstu].name}":listStudentNotPayment[indexstu].nameEn==" "? "${listStudentNotPayment[indexstu].name}":"${listStudentNotPayment[indexstu].nameEn}",
                                                                style: ThemsConstands.headline_4_medium_18.copyWith(
                                                                  color: Colorconstand.lightBulma,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          const SizedBox(width: 5,)
                                                        ],
                                                      ),
                                                    );
                                              },
                                              separatorBuilder: (context, ind) {
                                                return Container(
                                                    margin: const EdgeInsets.only(left: 100),
                                                    child: const Divider(
                                                      height: 1,
                                                      thickness: 0.5,
                                                    ));
                                              },
                                            ),
                                        ],
                                      ),
                              
                                    //====================Student Payment =================================
                                      listStudentPayment.isEmpty?Container():Column(
                                        children: [
                                          Container(
                                            height: 25,
                                            color: Colorconstand.primaryColor,
                                            child: Container(
                                              margin:const EdgeInsets.symmetric(horizontal: 18),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    margin:const EdgeInsets.only(right: 10),
                                                    child: SvgPicture.asset(
                                                      ImageAssets.card_verify_icon,
                                                      width: 18,
                                                      color: Colorconstand.screenWhite,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      child: Text("PAID_PAYMENT_SEVICE".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralWhite),),
                                                    ),
                                                  ),
                                                  Container(
                                                      child: Text("DATELINE".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralWhite),),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          ListView.separated(
                                              shrinkWrap: true,
                                              physics:const NeverScrollableScrollPhysics(),
                                              padding: const EdgeInsets.all(0),
                                              itemCount: listStudentPayment.length,
                                              itemBuilder: (context, indexstu) {
                                                return MaterialButton(
                                                      onPressed:() {
                                                          studentId = listStudentPayment[indexstu].studentId.toString();
                                                          guardianId = listStudentPayment[indexstu].guardianId.toString();
                                                          studentName = listStudentPayment[indexstu].name.toString();
                                                        showModalBottomSheet(
                                                          backgroundColor: Colors.transparent,
                                                          isScrollControlled: true,
                                                          shape: const RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                                                          ),
                                                          context: context,
                                                          builder: (context) {
                                                            return confirmPolicyWidget(
                                                              studentName: studentName,
                                                              guardianName: listStudentPayment[indexstu].guardianName.toString()
                                                            );
                                                          }
                                                        );
                                                      },
                                                      padding: const EdgeInsets.symmetric(  vertical: 8, horizontal: 22),
                                                      child: Row(
                                                        children: [
                                                          SizedBox(
                                                            width: 70,
                                                            height: 70,
                                                            child: ClipOval(
                                                              child:  Image.asset(
                                                                  listStudentPayment[indexstu].profileMedia!.fileShow == "" ||
                                                                  listStudentPayment[indexstu] .profileMedia!.fileShow == null
                                                                    ? listStudentPayment[indexstu].profileMedia! .fileThumbnail.toString()
                                                                    : listStudentPayment[indexstu].profileMedia!.fileShow.toString(),
                                                                fit: BoxFit.cover,
                                                              ),
                                                            ),
                                                          ),
                                                          Expanded(
                                                            child: Container(
                                                              margin: const EdgeInsets.only(left: 18, right: 0),
                                                              child: Text(
                                                                translate=="km" ?"${listStudentPayment[indexstu].name}":listStudentPayment[indexstu].nameEn==" "? "${listStudentPayment[indexstu].name}":"${listStudentPayment[indexstu].nameEn}",
                                                                style: ThemsConstands.headline_4_medium_18.copyWith(
                                                                  color: Colorconstand.lightBulma,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            child: Text(listStudentPayment[indexstu].appPaymentExpire,style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.alertsDecline),),
                                                          )
                                                        ],
                                                      ),
                                                    );
                                              },
                                              separatorBuilder: (context, ind) {
                                                return Container(
                                                    margin: const EdgeInsets.only(left: 100),
                                                    child: const Divider(
                                                      height: 1,
                                                      thickness: 0.5,
                                                    ));
                                              },
                                            ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                    );
                  } else{
                    return EmptyWidget(
                          title: "WE_DETECT".tr(),
                          subtitle: "WE_DETECT_DES".tr(),
                        );
                  }
                },
              ),
              ],
            );
        }else{
          return EmptyWidget(
            title: "WE_DETECT".tr(),
            subtitle: "WE_DETECT_DES".tr(),
          );
        }
      },
    );
  }
  Widget confirmPolicyWidget({String? studentName,String?guardianName}){
    return StatefulBuilder(
      builder: (BuildContext context, setState){
        return SingleChildScrollView(
          child: Container(
            margin:const EdgeInsets.only(top: 50),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
            child: Container(
                margin:const EdgeInsets.only(bottom: 25,top: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right:18.0,top: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [   
                          IconButton(onPressed: null, icon: Container(),),
                          Container(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                checkedbox = false;
                              },);
                              Navigator.pop(context);
                            }, 
                            icon: SvgPicture.asset(ImageAssets.CLOSE_ICON,color: Colorconstand.lightTrunks,)
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal:42.0,vertical: 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SvgPicture.asset(ImageAssets.large_warning_icon),
                            const SizedBox(height: 8,),
                            Text("VOLUNTARY_POLICIES".tr(),style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.primaryColor),),
                            const  SizedBox(height: 8,),
                            RichText(
                              text: TextSpan(
                                style:const TextStyle(wordSpacing: 2.1),
                                  children: [ 
                                      TextSpan(text:"OUR_NAMING".tr(), style: ThemsConstands.headline_5_medium_16.copyWith(color:Colorconstand.neutralDarkGrey)),
                                      TextSpan(text:" [$guardianName] ", style: ThemsConstands.headline_5_semibold_16.copyWith(color:Colorconstand.lightBulma)),
                                      TextSpan(text:"PARENTS_FOR_STUDENT".tr(), style: ThemsConstands.headline_5_medium_16.copyWith(color:Colorconstand.neutralDarkGrey)),
                                      TextSpan(text:" $studentName ", style: ThemsConstands.headline_5_semibold_16.copyWith(color:Colorconstand.lightBulma)),
                                      TextSpan(text:"VOLUNTARY_PRINCIPLES".tr(), style: ThemsConstands.headline_5_medium_16.copyWith(color:Colorconstand.neutralDarkGrey)),
                                      TextSpan(text:"១៤១៨ អយក បវព។", style: ThemsConstands.headline_5_semibold_16.copyWith(color:Colorconstand.lightBulma)),
                                  ]
                              )
                            ),  
                            const SizedBox(height: 18,),
                            Row (
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [  
                                InkWell(
                                  onTap: (){
                                      if(checkedbox == false){
                                        setState(() {
                                          checkedbox = true;
                                        });
                                      }
                                      else{
                                        setState(() {
                                          checkedbox = false;
                                        });
                                      }
                                  },
                                  child: Container (
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(top: 10,right: 8),
                                    padding: const EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                      color: checkedbox == true? Colorconstand.primaryColor:Colors.white,
                                      border: Border.all(color: Colorconstand.primaryColor),
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    child: const Icon(Icons.check,color: Colors.white,size: 18,),
                                  ),
                                ),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width - 132,
                                  child: Text("THIS_AGREEMENT".tr(), style: ThemsConstands.headline4_regular_18.copyWith( color: Colorconstand.neutralDarkGrey,height: 1.56),maxLines: 2,overflow: TextOverflow.ellipsis,)),
                              ],
                            ),
                            const  SizedBox(height: 18,),
                            MaterialButton(
                              disabledColor: Colorconstand.neutralGrey,
                              color: Colorconstand.primaryColor,
                              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                              padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 28),
                              onPressed: checkedbox == false?null:(){
                                Navigator.of(context).pop();
                                setState(() {
                                  checkedbox = false;
                                },);
                                Navigator.push(context, MaterialPageRoute(builder: (context)=> PaymentOptionScreen(classId: classId,studentId: studentId,guardianId: guardianId,className: className,studentName: studentName,)));
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("APPROVE_AND_PAY".tr(),style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.neutralWhite,fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ),
                
                          ],
                        ),
                      )
                    ),
                  ],
                ),
              )
            ),
        );
      }
    );
  }
    


  Future<dynamic> yesnoAlertbox(BuildContext context, {required bool isFull}) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              isFull != true
                  ? SvgPicture.asset(
                      ImageAssets.check_icon,
                      width: 62,
                      color: const Color.fromARGB(255, 0, 185, 96),
                    )
                  : SvgPicture.asset(
                      ImageAssets.warning_icon,
                      width: 62,
                      color: Colorconstand.primaryColor,
                    ),
            ],
          ),
          content: Text(
            isFull == true ? "MESSAGEMAZZER".tr() : "MODIFYMAZZER".tr(),
            textAlign: TextAlign.center,
            style: ThemsConstands.headline3_medium_20_26height
                .copyWith(height: 1.5),
          ),
          actions: <Widget>[
            TextButton(
              child: Text("OKAY".tr(),
                  style: ThemsConstands.button_semibold_16.copyWith(
                      color: isFull != true
                          ? const Color.fromARGB(255, 0, 185, 96)
                          : Colorconstand.primaryColor)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}