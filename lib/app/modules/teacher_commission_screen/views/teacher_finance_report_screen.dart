import 'dart:async';
import 'dart:ui';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/models/teacher_commission_model/month_model.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../time_line/widget/view_image.dart';
import '../states/teacher_commission _report/bloc/teacher_commission_report_bloc.dart';
import '../widget/add_attachment_widget.dart';
import '../widget/popup_detail_payment.dart';

class TeacherFinanceReportScreen extends StatefulWidget {
  const TeacherFinanceReportScreen({super.key});

  @override
  State<TeacherFinanceReportScreen> createState() => _TeacherFinanceReportScreenState();
}

class _TeacherFinanceReportScreenState extends State<TeacherFinanceReportScreen> {
  bool connection = true;
  StreamSubscription? sub;
  bool isEmptyState = false;
  String endDate = '';
  String startDate = '';
  int? monthlocal;
  int? currentmonthlocal;
  int? yearchecklocal;
  int? currentyearchecklocal;
  bool checkcurrentyearchecklocal = false;
  bool showListMonth = false;
  var date;
  var dateParse;
  var formattedDate;
  final now = DateTime.now();
  final oCcy =  NumberFormat("#,##0", "en_US");
  final listMonth = MonthModel.generate();    
  @override
  void initState() {
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
            //  BlocProvider.of<GetClassBloc>(context).add(GetClass());
          } else {}
        });
      });
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    currentmonthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
    currentyearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
  ///========= check first day and end day in month ======
     date = DateTime(yearchecklocal!, monthlocal!+1, 0).toString();
     dateParse = DateTime.parse(date);
     formattedDate = "${dateParse.day}/${dateParse.month}/${dateParse.year}";
    setState(() {
        endDate = formattedDate.toString() ;
        startDate = "${"01"}/${dateParse.month}/${dateParse.year}";
    });
  ///========= check first day and end day in month ======
    super.initState();

    BlocProvider.of<TeacherCommissionReportBloc>(context).add(GetCommissionReportEvent(month: monthlocal.toString(),year: yearchecklocal.toString()),);
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset("assets/images/Path_20.png"),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Image.asset("assets/images/Path_19.png"),
          ),
          Positioned(
            bottom: 30,
            right: 0,
            child: Image.asset("assets/images/Path_18.png"),
          ),
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
              child: SafeArea(
                bottom: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal:0),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 10,bottom: 20,left: 18,right: 18),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: SvgPicture.asset(
                                ImageAssets.chevron_left,
                                color: Colorconstand.primaryColor,
                                width: 32,
                                height: 32,
                              ),
                            ),
                            Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "REPORT".tr(),
                                  style: ThemsConstands.headline_2_semibold_24 .copyWith(
                                    color: Colorconstand.primaryColor,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            // GestureDetector(
                            //   onTap: () {
                            //     print("Document");
                            //   },
                            //   child: SvgPicture.asset(
                            //     ImageAssets.document,
                            //     color: Colorconstand.primaryColor,
                            //     width: 32,
                            //     height: 32,
                            //   ),
                            // ),
                            Container(width: 32,)
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal:18),
                          child: BlocBuilder<TeacherCommissionReportBloc, TeacherCommissionReportState>(
                            builder: (context, state) {
                              if(state is GetCommissionReportLoading){
                                 return Column(
                                   children: [
                                    Expanded(child: Container()),
                                    Container(
                                      height: 35,width: 35,
                                      alignment: Alignment.center,
                                      child: const CircularProgressIndicator(),
                                    ),
                                    Expanded(child: Container()),
                                   ],
                                 );
                              }
                              else if(state is GetCommissionReportLoaded){
                                var dataTeacherCommission = state.teacherCommissionReportModel!.data;
                                var dataCommissionItem = dataTeacherCommission!.items;
                                var dataCommissionItemCash;
                                var dataCommissionItemBank;
                                if(dataCommissionItem!.isNotEmpty){
                                    dataCommissionItemCash = dataCommissionItem.where((element) => element.paymentMethod == "CASH").toList();
                                    dataCommissionItemBank = dataCommissionItem.where((element) => element.paymentMethod != "CASH").toList();
                                }
                                return SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  showListMonth = true;
                                                });
                                              },
                                              child: Text("${"REGULAR".tr()} ${"MONTH".tr()} ${checkMonth(monthlocal!)}",style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.primaryColor),),
                                            ),
                                            const Icon(Icons.arrow_drop_down,size: 28,color: Colorconstand.primaryColor,)
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin:const EdgeInsets.symmetric(vertical: 18,horizontal: 18),
                                        child: Text("${startDate.toString()} ${"TO".tr()} ${endDate.toString()}".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.lightBulma),textAlign: TextAlign.left,),
                                      ),
                                      financeCardWidget(context,dataTeacherCommission.totalCommission.toString(),dataTeacherCommission.totalCashIncome.toString(),dataTeacherCommission.status.toString()), 
                                      Container(
                                        margin:const EdgeInsets.symmetric(vertical: 12),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Icon(dataTeacherCommission.status=="open"?Icons.timer_sharp:Icons.check_circle_rounded,size: 22,color:dataTeacherCommission.status=="open"?Colorconstand.alertsAwaitingText.withOpacity(0.5): Colorconstand.alertsPositive.withOpacity(0.5),),
                                            const SizedBox(width: 6,),
                                            Text(dataTeacherCommission.status=="open"?"WAITTING_FOR_PAY".tr():"PAY_READY".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: dataTeacherCommission.status=="open"?Colorconstand.alertsAwaitingText: Colorconstand.alertsPositive,),textAlign: TextAlign.center,),
                                            const SizedBox(width: 12,),
                                            dataTeacherCommission.status=="open" || dataTeacherCommission.attachments!.isNotEmpty?GestureDetector(
                                              onTap: dataTeacherCommission.status=="open" && dataTeacherCommission.attachments!.isEmpty?(){
                                                showModalBottomSheet(
                                                  shape: const RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
                                                  ),
                                                  isScrollControlled: false,
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    // BlocProvider.of<ViewImageBloc>(context).add(GetViewImageEvent(data.id!.toString()),);
                                                    return const AddAttachmentWidget();
                                                  });
                                              }:(){
                                                Navigator.push(
                                                  context,
                                                  PageRouteBuilder(
                                                    pageBuilder: (_, __, ___) =>
                                                        ImageViewDownloads(
                                                      listimagevide: dataTeacherCommission.attachments!,
                                                      activepage: 0,
                                                    ),
                                                    transitionDuration:
                                                        const Duration(seconds: 0),
                                                  ),
                                                );
                                              },
                                              child: Container(
                                                padding:const EdgeInsets.all(1),
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(15),
                                                  color: dataTeacherCommission.status=="open"?Colorconstand.alertsAwaitingText: Colorconstand.alertsPositive,
                                                ),
                                                child: Container(
                                                  padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 8),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(13),
                                                    color: Colorconstand.lightBeerusBeerus
                                                  ),
                                                  child: Row(
                                                    children: [
                                                      const SizedBox(width: 3,),
                                                      SvgPicture.asset(ImageAssets.attach_circlle,width: 18,color:dataTeacherCommission.status=="open"?Colorconstand.alertsAwaitingText.withOpacity(0.5): Colorconstand.alertsPositive.withOpacity(0.5),),
                                                      const SizedBox(width: 8,),
                                                      Text(dataTeacherCommission.status=="open"?"ATTACH_REFERRING_FILE".tr():"REFERRING_FILE".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: dataTeacherCommission.status=="open"?Colorconstand.alertsAwaitingText: Colorconstand.alertsPositive,),textAlign: TextAlign.center,),
                                                      const SizedBox(width: 3,),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ):Container(),
                                          ],
                                        ),
                                      ),
                                      Divider(
                                        height: 1,
                                        thickness: 1,
                                        color: Colorconstand.mainColorSecondary.withOpacity(0.2),
                                      ),    
                                      SizedBox(height:dataCommissionItem.isEmpty?0:16,),  
                                      dataTeacherCommission.items!.isEmpty?Container():Text("REVENUE".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.primaryColor),),
                                      Container(
                                        child: dataTeacherCommission.items!.isEmpty?
                                        Container(
                                          margin:const EdgeInsets.only(top: 50,left: 28,right: 28),
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Text("NOT_OPERATION_YET".tr(),style: ThemsConstands.headline3_medium_20_26height.copyWith(fontWeight: FontWeight.bold)),
                                              const  SizedBox(height: 8,),
                                              Text("DES_NOT_OPERATION_YET".tr(), style: ThemsConstands.headline6_medium_14.copyWith(color: Colorconstand.neutralDarkGrey,height: 2),textAlign: TextAlign.center,),
                                            ],
                                          ),
                                        ) : Container(
                                          margin:const EdgeInsets.symmetric(vertical: 15),
                                          child: Column(
                                            children: [
                                              dataCommissionItemCash.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    padding:const EdgeInsets.symmetric(vertical: 12),
                                                    width: MediaQuery.of(context).size.width,
                                                    decoration: BoxDecoration(
                                                      color:const Color(0xFF7EBBFD).withOpacity(0.25),
                                                      borderRadius: BorderRadius.circular(12)
                                                    ),
                                                    child: Text("CASH_PAYMENT_VAI_TEACHER".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                                  ),
                                                  ListView.separated(
                                                    itemCount: dataCommissionItemCash.length,
                                                    physics:const NeverScrollableScrollPhysics(),
                                                    padding: const EdgeInsets.all(0),
                                                    shrinkWrap: true,
                                                    itemBuilder: (context, index){
                                                      String formattedDate = DateFormat('dd/MM/yyyy').format(dataCommissionItemCash[index].createdAt);
                                                      return Container(
                                                        height: 45,
                                                        child: MaterialButton(
                                                          splashColor: Colors.transparent,
                                                          highlightColor: Colors.transparent,
                                                          hoverColor: Colors.transparent,
                                                          focusColor: Colors.transparent,
                                                          padding:const EdgeInsets.all(0),
                                                          onPressed: (){
                                                            showModalBottomSheet(
                                                              backgroundColor: Colors.transparent,
                                                              isScrollControlled: true,
                                                                shape: const RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                                                                ),
                                                                context: context,
                                                                builder: (context) {
                                                                  return SingleChildScrollView(
                                                                    child: Container(
                                                                      margin:const EdgeInsets.only(top: 50),
                                                                      decoration: const BoxDecoration(
                                                                        color: Colors.white,
                                                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
                                                                      child: PopUpDetailStudentPayment(
                                                                        classname: dataCommissionItemCash[index].className.toString(),
                                                                        guardianQr: dataCommissionItemCash[index].guardianQrUrl.toString(), 
                                                                        paymentDate: formattedDate, 
                                                                        paymentOption: translate == "km"?dataCommissionItemCash[index].guardianPayment.choosePayOptionName.toString():dataCommissionItemCash[index].guardianPayment.choosePayOptionNameEn.toString(),
                                                                        pricePayment: dataCommissionItemCash[index].amount.toString(), 
                                                                        studentName: dataCommissionItemCash[index].studentName.toString(), 
                                                                        studentQr: dataCommissionItemCash[index].studentQrUrl.toString(),
                                                                        btnClose: () {
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                      ),
                                                                    ),
                                                                  );
                                                                }
                                                              );
                                                          },
                                                           child: Container(
                                                            height: 45,
                                                            child: Row(
                                                              children: [
                                                                Expanded(
                                                                  child: Row(
                                                                    children: [
                                                                      Container(
                                                                        child: Text(formattedDate,style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                      Container(
                                                                        margin:const EdgeInsets.symmetric(horizontal: 8),
                                                                        child: SvgPicture.asset(ImageAssets.dollar_circle_icon)
                                                                      ),
                                                                      Container(
                                                                        child: Text(dataCommissionItemCash[index].studentName.toString(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  child: Text("${oCcy.format(int.parse(dataCommissionItemCash[index].amount.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                ),
                                                              ],
                                                            ),
                                                           ),
                                                        ),
                                                      );       
                                                    },
                                                    separatorBuilder: (context,index){
                                                      return Divider(
                                                        height: 1,
                                                        color: Colorconstand.neutralDarkGrey.withOpacity(.4),
                                                      );
                                                    },
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Container(
                                                          margin:const EdgeInsets.only(right: 15),
                                                          child: Text("TOTAL".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              dataCommissionItemBank.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    padding:const EdgeInsets.symmetric(vertical: 12),
                                                    width: MediaQuery.of(context).size.width,
                                                    decoration: BoxDecoration(
                                                      color:const Color(0xFF7EBBFD).withOpacity(0.25),
                                                      borderRadius: BorderRadius.circular(12)
                                                    ),
                                                    child: Text("CASH_PAYMENT_VAI_COMPANY".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                                  ),
                                                  ListView.separated(
                                                    itemCount: dataCommissionItemBank.length,
                                                    physics:const NeverScrollableScrollPhysics(),
                                                    padding: const EdgeInsets.all(0),
                                                    shrinkWrap: true,
                                                    itemBuilder: (context, index){
                                                      String formattedDate = DateFormat('dd/MM/yyyy').format(dataCommissionItemBank[index].createdAt);
                                                      return Container(
                                                        height: 45,
                                                        child: MaterialButton(
                                                          splashColor: Colors.transparent,
                                                          highlightColor: Colors.transparent,
                                                          hoverColor: Colors.transparent,
                                                          focusColor: Colors.transparent,
                                                          padding:const EdgeInsets.all(0),
                                                          onPressed: (){
                                                            showModalBottomSheet(
                                                              backgroundColor: Colors.transparent,
                                                              isScrollControlled: true,
                                                                shape: const RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                                                                ),
                                                                context: context,
                                                                builder: (context) {
                                                                  return SingleChildScrollView(
                                                                    child: Container(
                                                                      margin:const EdgeInsets.only(top: 50),
                                                                      decoration: const BoxDecoration(
                                                                        color: Colors.white,
                                                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
                                                                      child: PopUpDetailStudentPayment(
                                                                        classname: dataCommissionItemBank[index].className.toString(), 
                                                                        guardianQr: dataCommissionItemBank[index].guardianQr.toString(), 
                                                                        paymentDate: formattedDate, 
                                                                        paymentOption: translate == "en"?dataCommissionItemBank[index].guardianPayment.choosePayOptionNameEn.toString():dataCommissionItemBank[index].guardianPayment.choosePayOptionName.toString(),
                                                                        pricePayment: dataCommissionItemBank[index].amount.toString(), 
                                                                        studentName: dataCommissionItemBank[index].studentName.toString(), 
                                                                        studentQr: dataCommissionItemBank[index].studentQr.toString(),
                                                                        btnClose: () {
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                      ),
                                                                    ),
                                                                  );
                                                                }
                                                              );
                                                          },
                                                           child: Container(
                                                            height: 45,
                                                            child: Row(
                                                              children: [
                                                                Expanded(
                                                                  child: Row(
                                                                    children: [
                                                                      Container(
                                                                        child: Text(formattedDate,style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                      Container(
                                                                        margin:const EdgeInsets.symmetric(horizontal: 8),
                                                                        child: SvgPicture.asset(ImageAssets.pay_company_icon)
                                                                      ),
                                                                      Container(
                                                                        child: Text(dataCommissionItemBank[index].studentName.toString(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  child: Text("${oCcy.format(int.parse(dataCommissionItemBank[index].amount.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                                ),
                                                              ],
                                                            ),
                                                           ),
                                                        ),
                                                      );       
                                                    },
                                                    separatorBuilder: (context,index){
                                                      return Divider(
                                                        height: 1,
                                                        color: Colorconstand.neutralDarkGrey.withOpacity(.4),
                                                      );
                                                    },
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Container(
                                                          margin:const EdgeInsets.only(right: 15),
                                                          child: Text("TOTAL".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalIncome.toString())- int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              Container(
                                                padding:const EdgeInsets.symmetric(vertical: 12),
                                                width: MediaQuery.of(context).size.width,
                                                decoration: BoxDecoration(
                                                  color:const Color(0xFF7EBBFD).withOpacity(0.25),
                                                  borderRadius: BorderRadius.circular(12)
                                                ),
                                                child: Text("TOTAL_TASK_INCOME_TEACHER".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                              ),
              
                                              const SizedBox(height: 12,),
                                              
                                              dataCommissionItemCash.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_CASH_PAYMENT_VAI_TEACHER".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
              
                                              dataCommissionItemBank.isEmpty?Container():Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_CASH_PAYMENT_VAI_COMPANY".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalIncome.toString())-int.parse(dataTeacherCommission.totalCashIncome.toString()))} ${"៛"}",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  ),
                                                ],
                                              ),
              
                                              Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_ALL_SEVICE_REVENUE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBulma),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalIncome.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBulma),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
              
                                              Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("COMMISSION_PERCENTAGE".tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                        Container(
                                                          child: Text("${dataTeacherCommission.commissionPercentage.toString()}%",style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 1,
                                                      color: Colorconstand.neutralDarkGrey,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Column(
                                                children: [
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          child: Text("TOTAL_TASK_INCOME_TEACHER".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                        Container(
                                                          child: Text("${oCcy.format(int.parse(dataTeacherCommission.totalCommission.toString()))} ${"៛"}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                    child:const Divider(
                                                      height: 1,
                                                      thickness: 2,
                                                      color: Colorconstand.primaryColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }
                              else{
                                return Column(
                                  children: [
                                    Expanded(child: Container()),
                                    Container(
                                      alignment: Alignment.center,
                                      child: EmptyWidget(
                                        title: "WE_DETECT".tr(),
                                        subtitle: "WE_DETECT_DES".tr(),
                                      ),
                                    ),
                                    Expanded(child: Container()),
                                  ],
                                );
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          showListMonth == true?Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
            child: GestureDetector(
              onTap: (){
                setState(() {
                  showListMonth = false;
                  yearchecklocal = currentyearchecklocal;
                  checkcurrentyearchecklocal = false;
                });
              },
              child: Container(
                color: const Color(0x7B9C9595),
              ),
            ),
          ):Container(height: 0,),
          AnimatedPositioned(
            top:showListMonth == true?140:100,
            duration:const Duration(milliseconds: 300),
            child:showListMonth == false?Container():Container(
              alignment: Alignment.center,
              height: 210,width: MediaQuery.of(context).size.width-30,
              margin:const EdgeInsets.only(top: 15,bottom: 5,left: 15,right: 15),
              decoration: BoxDecoration(
                 color: Colorconstand.neutralWhite,
                 borderRadius: BorderRadius.circular(16)
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Container(
                          child: IconButton(
                            onPressed: yearchecklocal == 2023?(){}:(){
                              setState(() {
                                yearchecklocal = yearchecklocal! - 1;
                                if(yearchecklocal == currentyearchecklocal){
                                  checkcurrentyearchecklocal = false;
                                }
                                else{
                                  checkcurrentyearchecklocal = true;
                                }
                              });
                            }, 
                            icon:const Icon(Icons.arrow_back_ios_new_rounded)
                          ),
                        ),
                        Expanded(
                          child: Text(yearchecklocal.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_4_medium_18,),
                        ),
                        Container(
                          child: IconButton(
                            onPressed: (){
                               setState(() {
                                if(yearchecklocal == currentyearchecklocal){
                                }
                                else{
                                  yearchecklocal = yearchecklocal! + 1;
                                  if(yearchecklocal == currentyearchecklocal){
                                    checkcurrentyearchecklocal = false;
                                    monthlocal = currentmonthlocal;
                                  }
                                  else{
                                    checkcurrentyearchecklocal = true;
                                    monthlocal = currentmonthlocal;
                                  }
                                }
                              });
                            }, 
                            icon:const Icon(Icons.arrow_forward_ios_rounded)
                          ),
                        )
                      ],
                    ),
                  ),
                  GridView.builder(
                    shrinkWrap: true,
                    physics:const NeverScrollableScrollPhysics(),
                    padding:const EdgeInsets.all(.0),
                      itemCount: listMonth.length,
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          child: GestureDetector(
                            onTap: yearchecklocal == currentyearchecklocal&& (index+1) > currentmonthlocal!?(){}:(() {
                              setState(() {
                                monthlocal = listMonth[index].month!.toInt();
                                showListMonth = false;
                                checkcurrentyearchecklocal = false;
                                ///========= check first day and end day in month ======
                                  date = DateTime(yearchecklocal!, monthlocal!+1, 0).toString();
                                  dateParse = DateTime.parse(date);
                                  formattedDate = "${dateParse.day}/${dateParse.month}/${dateParse.year}";
                                  setState(() {
                                      endDate = formattedDate.toString() ;
                                      startDate = "${"01"}/${dateParse.month}/${dateParse.year}";
                                  });
                                ///========= check first day and end day in month ======
                                BlocProvider.of<TeacherCommissionReportBloc>(context).add(GetCommissionReportEvent(month: monthlocal.toString(),year: yearchecklocal.toString()),);
                              });
                            }),
                            child: Container(
                              margin:const EdgeInsets.all(6),
                              decoration:BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: monthlocal == (listMonth[index].month!.toInt()) && checkcurrentyearchecklocal == false? Colorconstand.primaryColor:Colors.transparent,
                              ),
                              height: 20,width: 20,
                              child:  Center(child: Text(checkMonth(listMonth[index].month!.toInt()),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthlocal==(index+1)&&checkcurrentyearchecklocal == false?Colorconstand.neutralWhite:yearchecklocal == currentyearchecklocal&& (index+1) > currentmonthlocal!?Colorconstand.neutralGrey : Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                            ),
                          ),
                        );
                      },
                    ),
                ],
              ),
            )
          ),
          //================ Internet offilne ========================
            connection == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
        ],
      ),
    );
  }

  Widget financeCardWidget(BuildContext context,String? totalCommission, String? totalCashIncome,String? statusPayment) {
    return Container(
          decoration: BoxDecoration(
            // gradient: const LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
            //   Color(0xFF4D9EF6),
            //   Color(0xFF2C70C7),
            //   Color(0xFF1855AB),
            // ]),
            color: statusPayment=="open"?Colorconstand.alertsAwaitingText:Colorconstand.alertsPositive,
            borderRadius: BorderRadius.circular(16)
          ),
          width: MediaQuery.of(context).size.width,
          child: Container(
            child: Column(
              children: [
                SizedBox(
                  height: 145,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: [
                        Positioned(
                          left: -7,
                          top: -7,
                          child: Container(
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colorconstand.neutralWhite.withOpacity(0.2)
                            ),
                            child: Container(
                              padding:const EdgeInsets.all(18),
                              //child: SvgPicture.asset(ImageAssets.card_tick_outline_icon,color: Colorconstand.neutralWhite.withOpacity(0.2),),
                              child: Icon(statusPayment=="open"?Icons.timer_sharp:Icons.check_circle_rounded,size: 35,color: Colorconstand.neutralWhite.withOpacity(0.4),),
                            ),
                          ),
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      child: Text("CASH_PAYMENT_VAI_TEACHER".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),),
                                    ),
                                  ),
                                  const SizedBox(width: 12,),
                                  Container(
                                    width: 120,
                                    alignment: Alignment.centerRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(oCcy.format(int.parse(totalCashIncome!)).toString(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                                        const SizedBox(width: 14,),
                                        Text("៛".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 5,),
                              Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      child: Text("-${"TOTAL_TASK_INCOME_TEACHER".tr()}",style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),),
                                    ),
                                  ),
                                  const SizedBox(width: 12,),
                                  Container(
                                    width: 120,
                                    alignment: Alignment.centerRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text("-${oCcy.format(int.parse(totalCommission!))}",style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                                        const SizedBox(width: 14,),
                                        Text("៛".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                height: 20,
                                child:const Divider(
                                  thickness: 1,
                                  color: Colorconstand.neutralWhite,
                                ),
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      child: Text( int.parse(totalCommission) > int.parse(totalCashIncome) ? "CASH_PAID_TO_TEACHER".tr():"CASH_PAID_TO_COMPANY".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite),),
                                    ),
                                  ),
                                  const SizedBox(width: 12,),
                                  Container(
                                    width: 120,
                                    alignment: Alignment.centerRight,
                                    child: Text("(${oCcy.format(int.parse(totalCommission)>int.parse(totalCashIncome)?int.parse(totalCommission) - int.parse(totalCashIncome):int.parse(totalCashIncome)-int.parse(totalCommission)).toString()}៛)",style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
  }
}