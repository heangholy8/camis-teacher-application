import 'dart:async';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/create_payment/bloc/create_payment_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/shimmer/shimmer_style.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../states/check_transaction_payment/bloc/check_transaction_bloc.dart';
import '../states/payment_option/bloc/payment_option_bloc.dart';
import '../states/teacher_commission/bloc/teacher_commission_bloc.dart';
import '../widget/popup_comfrim_payment.dart';
import '../widget/popup_detail_payment.dart';

class PaymentOptionScreen extends StatefulWidget {
  String? classId,studentId,guardianId,studentName,className;
  PaymentOptionScreen({ required this.classId,required this.guardianId,required this.studentId,required this.className,required this.studentName,super.key});
  @override
  State<PaymentOptionScreen> createState() => _PaymentOptionScreenState();
}

class _PaymentOptionScreenState extends State<PaymentOptionScreen> {
  bool connection = true;
  StreamSubscription? sub;
  String isSelectPaymentOption = "bakong";
  bool loadingCreatePayment = false;
  int? monthlocal;
  int? yearchecklocal;
  String? location = "";
  bool showKHQR = false;
  String formattedDate = "";
  String classname="";
  String guardianQr= ""; 
  String paymentOption= "";
  String pricePayment= ""; 
  String studentName= "";
  String studentQr= "";
  late Timer _every10MinutesKHQR;
  final oCcy =  NumberFormat("#,##0", "en_US");

  @override
  void initState() {
     BlocProvider.of<GetClassBloc>(context).add(GetClass());
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
            //  BlocProvider.of<GetClassBloc>(context).add(GetClass());
          } else {}
        });
      });
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
    BlocProvider.of<PaymentOptionBloc>(context).add(GetPaymentOptionEvent());
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    Future.delayed(const Duration(minutes: 4), () {
      setState(() {
        _every10MinutesKHQR.cancel();
      });
    });
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: BlocListener<CreatePaymentBloc, CreatePaymentState>(
        listener: (context, state) {
          if(state is CreatePaymentLoading){
            setState(() {
              loadingCreatePayment = true;
            });
          }
          else if(state is CreatePaymentCashLoaded){
            var dataCommissionItemCash = state.createPaymentCash!.data;
            String formattedDate = DateFormat('dd/MM/yyyy').format(dataCommissionItemCash!.createdAt);
            setState(() {
              showModalBottomSheet(
                backgroundColor: Colors.transparent,
                isScrollControlled: true,
                isDismissible: false,
                    enableDrag: false,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                  ),
                  context: context,
                  builder: (context) {
                    return SingleChildScrollView(
                      child: Container(
                        margin:const EdgeInsets.only(top: 50),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
                        child: PopUpDetailStudentPayment(
                          classname: dataCommissionItemCash.className.toString(),
                          guardianQr: dataCommissionItemCash.guardianQrUrl.toString(), 
                          paymentDate: formattedDate, 
                          paymentOption: translate == "km"?dataCommissionItemCash.guardianPayment!.choosePayOptionName.toString():dataCommissionItemCash.guardianPayment!.choosePayOptionNameEn.toString(),
                          pricePayment: dataCommissionItemCash.amount.toString(), 
                          studentName: dataCommissionItemCash.studentName.toString(), 
                          studentQr: dataCommissionItemCash.studentQrUrl.toString(),
                          btnClose: () {
                            BlocProvider.of<TeacherCommissionBloc>(context).add(GetCommissionEvent(month: monthlocal.toString(),year: yearchecklocal.toString()),);
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    );
                  }
                );
              loadingCreatePayment = false;
            });
          }
          else if(state is CreatePaymentBakongLoaded){
            var dataLink = state.createPaymentBakong!.link;
            var dataCommissionItemBank = state.createPaymentBakong!.data;
             setState(() {
              formattedDate = DateFormat('dd/MM/yyyy').format(dataCommissionItemBank!.createdAt);
              classname= dataCommissionItemBank.className.toString();
              guardianQr= dataCommissionItemBank.guardianQrUrl.toString(); 
              paymentOption= translate == "km"?dataCommissionItemBank.guardianPayment!.choosePayOptionName.toString():dataCommissionItemBank.guardianPayment!.choosePayOptionNameEn.toString();
              pricePayment= dataCommissionItemBank.amount.toString(); 
              studentName= dataCommissionItemBank.studentName.toString();
              studentQr= dataCommissionItemBank.studentQrUrl.toString();


              showKHQR = true;
              location = dataLink!.location.toString();
              loadingCreatePayment = false;
             });
             _every10MinutesKHQR = Timer.periodic(const Duration(seconds: 4), (Timer t) {
                    setState(() {
                      BlocProvider.of<CheckTransactionBloc>(context) .add(TransactionEvent(tranId:dataLink!.transactionId.toString(), guardianId: widget.guardianId.toString()));
                    });
                });
          }
          else{
            setState(() {
              loadingCreatePayment = false;
            });
          }
        },
        child: BlocListener<CheckTransactionBloc, CheckTransactionState>(
          listener: (context, state) {
            if(state is TransactionLoading){
            }
            else if(state is TransactionLoaded){
              var data = state.transactionModel.data;
              var status = state.transactionModel.status;
              setState(() {
                if(data !=null && data.status=="paid"){
                  showKHQR = false;
                  _every10MinutesKHQR.cancel();
                  showModalBottomSheet(
                    backgroundColor: Colors.transparent,
                    isScrollControlled: true,
                    isDismissible: false,
                    enableDrag: false,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                      ),
                      context: context,
                      builder: (context) {
                        return SingleChildScrollView(
                          child: Container(
                            margin:const EdgeInsets.only(top: 50),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
                            child: PopUpDetailStudentPayment(
                              classname: classname,
                              guardianQr: guardianQr, 
                              paymentDate: formattedDate, 
                              paymentOption: paymentOption,
                              pricePayment: pricePayment, 
                              studentName: studentName, 
                              studentQr: studentQr,
                              btnClose: () {
                                 BlocProvider.of<TeacherCommissionBloc>(context).add(GetCommissionEvent(month: monthlocal.toString(),year: yearchecklocal.toString()),);
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                              },
                            ),
                          ),
                        );
                      }
                    );
                }
               });
              
            }
            else{
              setState(() {
                _every10MinutesKHQR.cancel();
              });
            }
          },
          child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Image.asset("assets/images/Oval.png"),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: SafeArea(
                      bottom: false,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                top: 10, left: 22, right: 22, bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: SvgPicture.asset(
                                    ImageAssets.chevron_left,
                                    color: Colorconstand.neutralWhite,
                                    width: 32,
                                    height: 32,
                                  ),
                                ),
                                Container(
                                    alignment: Alignment.center,
                                    child: Text("PAYMENT".tr(),
                                      style: ThemsConstands.headline_2_semibold_24.copyWith(
                                        color: Colorconstand.neutralWhite,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                Container(width: 32,),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.only(top: 10),
                              width: MediaQuery.of(context).size.width,
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: Colorconstand.neutralWhite,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(12),
                                    topRight: Radius.circular(12),
                                  ),
                                ),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                        margin:const EdgeInsets.only(top: 28),
                                        child: Text("PAY_SERVICE_TO_STUDENT".tr(),style: ThemsConstands.headline3_medium_20_26height.copyWith(color:Colorconstand.lightBlack),),
                                      ),
                                      Container(
                                         margin:const EdgeInsets.only(top: 18),
                                        child: Text(widget.studentName.toString(),style: ThemsConstands.headline_2_semibold_24.copyWith(color:Colorconstand.primaryColor),),
                                      ),
                                      Container(
                                         margin:const EdgeInsets.only(top: 18),
                                        child: Text("${"CLASS".tr()} ${widget.className.toString()}".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color:Colorconstand.lightBlack),),
                                      ),
                                      // Container(
                                      //   margin:const EdgeInsets.only(top: 28,left: 28,right: 28),
                                      //   child: Row(
                                      //     children: [
                                      //       Expanded(
                                      //         child: Container(
                                      //           height: 90,
                                      //             decoration: BoxDecoration(
                                      //               color:Colorconstand.primaryColor,
                                      //               borderRadius: BorderRadius.circular(18)
                                      //             ),
                                      //           child: Container(
                                      //             margin:const EdgeInsets.all(1),
                                      //             decoration: BoxDecoration(
                                      //               color: isSelectPaymentOption == "cash"?Colorconstand.primaryColor:Colorconstand.neutralWhite,
                                      //               borderRadius: BorderRadius.circular(18)
                                      //             ),
                                      //           child: MaterialButton(
                                      //             padding:const EdgeInsets.all(0),
                                      //             shape:const RoundedRectangleBorder(
                                      //                       borderRadius: BorderRadius.all(Radius.circular(18))),
                                      //             onPressed: (){
                                      //               setState(() {
                                      //                 isSelectPaymentOption = "cash";
                                      //               });
                                      //             },
                                      //             child: Container(
                                      //                 child: Column(
                                      //                   mainAxisAlignment: MainAxisAlignment.center,
                                      //                   children: [
                                      //                     Container(
                                      //                       child: SvgPicture.asset(ImageAssets.dollar_circle_filed_icon,color: isSelectPaymentOption == "cash"?Colorconstand.neutralWhite:Colorconstand.primaryColor,width: 32,),
                                      //                     ),
                                      //                     Container(
                                      //                       child: Text("PAY_THEACHER".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: isSelectPaymentOption == "cash"?Colorconstand.neutralWhite:Colorconstand.lightBlack),),
                                      //                     )
                                      //                   ],
                                      //                 ),
                                      //               ),
                                      //             ),
                                      //         ),
                                      //         ),
                                      //       ),
                                  
                                      //       const SizedBox(width: 28,),
                                  
                                      //       Expanded(
                                      //         child: Container(
                                      //           height: 90,
                                      //             decoration: BoxDecoration(
                                      //               color:Colorconstand.primaryColor,
                                      //               borderRadius: BorderRadius.circular(18)
                                      //             ),
                                      //           child: Container(
                                      //             margin:const EdgeInsets.all(1),
                                      //             decoration: BoxDecoration(
                                      //               color: isSelectPaymentOption == "bakong"?Colorconstand.primaryColor:Colorconstand.neutralWhite,
                                      //               borderRadius: BorderRadius.circular(18)
                                      //             ),
                                      //           child: MaterialButton(
                                      //             padding:const EdgeInsets.all(0),
                                      //             shape:const RoundedRectangleBorder(
                                      //                     borderRadius: BorderRadius.all(Radius.circular(18))),
                                      //             onPressed: (){
                                      //               setState(() {
                                      //                 isSelectPaymentOption = "bakong";
                                      //               });
                                      //             },
                                      //             child: Container(
                                                    
                                      //                 child: Column(
                                      //                   mainAxisAlignment: MainAxisAlignment.center,
                                      //                   children: [
                                      //                     Container(
                                      //                       child: SvgPicture.asset(ImageAssets.pay_company_icon,color:isSelectPaymentOption == "bakong"?Colorconstand.neutralWhite:Colorconstand.primaryColor,width: 32,),
                                      //                     ),
                                      //                     Container(
                                      //                       child: Text("PAY_COMPANY".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: isSelectPaymentOption == "bakong"?Colorconstand.neutralWhite:Colorconstand.lightBlack),),
                                      //                     )
                                      //                   ],
                                      //                 ),
                                      //               ),
                                      //             ),
                                      //           ),
                                      //         ),
                                      //       ),
                                      //     ],
                                      //   ),
                                      // ),
                                      // Container(
                                      //   margin:const EdgeInsets.symmetric(vertical: 28),
                                      //   child: Text("CHOOSE_PAYMENT".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: isSelectPaymentOption == ""?Colorconstand.neutralWhite:Colorconstand.lightBlack),),
                                      // ),
                                      BlocBuilder<PaymentOptionBloc, PaymentOptionState>(
                                      builder: (context, state) {
                                        if (state is PaymentOptionLoadingState) {
                                          return ListView.builder(
                                            padding:const EdgeInsets.all(0),
                                            shrinkWrap: true,
                                            itemBuilder: (BuildContext contex, index) {
                                              return Container(
                                                margin:const EdgeInsets.only(top: 15,left: 15,right: 15),
                                                child: const ShimmerAbsentRequest(),
                                              );
                                            },
                                            itemCount: 3,
                                          );
                                        }
                                        if (state is PaymentOptionLoadedState) {
                                          var paymentOptionDate = state.currentPaymentOption.data!;
                                          paymentOptionDate.sort((b, a) => a.amount!.compareTo(b.amount!));                                  
                                          return ListView.builder(
                                            physics:const NeverScrollableScrollPhysics(),
                                            padding: const EdgeInsets.all(0),
                                            shrinkWrap: true,
                                            itemCount: paymentOptionDate.length,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                margin:const EdgeInsets.symmetric(horizontal: 22),
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      margin:const EdgeInsets.only(top: 15,bottom: 5),
                                                      child: MaterialButton(
                                                        elevation: 0,
                                                        color:paymentOptionDate[index].type==3? Colorconstand.primaryBackgroundColor:Colorconstand.neutralWhite,
                                                        padding:const EdgeInsets.all(0),
                                                        onPressed: () {
                                                          setState(() {
                                                            showModalBottomSheet(
                                                                backgroundColor: Colors.transparent,
                                                                isScrollControlled: true,
                                                                shape: const RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
                                                                ),
                                                                context: context,
                                                                builder: (context) {
                                                                  return PopUpComfrimPayment(
                                                                    classname: widget.className.toString(),
                                                                    pricePayment: paymentOptionDate[index].amount.toString(),
                                                                    studentName: widget.studentName.toString(),
                                                                    paymentType:  paymentOptionDate[index].type!.toInt(),
                                                                    payFrom: isSelectPaymentOption,
                                                                    payForMonth: paymentOptionDate[index].amountOfMonth.toString(),
                                                                    studentId: widget.studentId.toString(),
                                                                    guardinaId: widget.guardianId.toString(),
                                                                    classId: widget.classId.toString(),
                                                                  );
                                                                }
                                                              );
                                                          });
                                                        },
                                                        shape:const RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.all(Radius.circular(15))),
                                                        child: Container(
                                                          height: 84,
                                                          decoration: BoxDecoration(
                                                              borderRadius: BorderRadius.circular(15),
                                                              border: Border.all(color: Colorconstand.primaryColor,width: 2),
                                                          ),
                                                          child: Container(
                                                            margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 6),
                                                            child: Row(mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                Text(translate=="km"? paymentOptionDate[index].name!:paymentOptionDate[index].nameEn!,
                                                                    style: ThemsConstands.headline3_semibold_20.copyWith(
                                                                      color: Colorconstand.primaryColor,
                                                                    )),
                                                                Text(oCcy.format(int.parse(paymentOptionDate[index].amount!.toString())) +
                                                                        (paymentOptionDate[index].currency!.toString() == "KHR"? " ៛": " \$") +(" / ${paymentOptionDate[index].amountOfMonth} ${"MONTH".tr()}"),
                                                                    style: ThemsConstands.headline3_semibold_20.copyWith(color:  Colorconstand.primaryColor
                                                                )),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    paymentOptionDate[index].type==3? Positioned(
                                                        top: 0,
                                                        right: 30,
                                                        child: Container(
                                                            padding: const EdgeInsets.symmetric(
                                                                horizontal: 20, vertical: 8),
                                                            decoration: BoxDecoration(
                                                              borderRadius:BorderRadius.circular(8),
                                                              color:Colorconstand.primaryColor,
                                                                  
                                                            ),
                                                            child: Text( "SAVE_TWO_MONTH".tr(),
                                                                style: ThemsConstands
                                                                    .headline6_medium_14.copyWith(color: Colorconstand.neutralWhite,))),
                                                      ):Container(),
                                                  ],
                                                ),
                                              );
                                            },
                                          );
                                        } else {
                                          return  Text("DATA_ERROR".tr());
                                        }
                                      },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  loadingCreatePayment ==true?
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      color: const Color(0x7B9C9595),
                      child: Center(
                        child: Container(
                          width: 35,height: 35,
                          child:const CircularProgressIndicator()
                        )
                      ),
                    ),
                  ):Container(),
                  //submitButtonWidget(context),
                  //================ Internet offilne ========================
                    connection == true
                        ? Container()
                        : Positioned(
                            bottom: 0,
                            left: 0,
                            right: 0,
                            top: 0,
                            child: Container(
                              
                              color: const Color(0x7B9C9595),
                            ),
                          ),
                    AnimatedPositioned(
                      bottom: connection == true ? -150 : 0,
                      left: 0,
                      right: 0,
                      duration: const Duration(milliseconds: 500),
                      child: const NoConnectWidget(),
                    ),
              //================ Internet offilne ========================

           //=============== Display KHQR ===============
                   showKHQR == true?Container(
                      color: Colorconstand.neutralWhite,
                        child: Column(
                          children: [
                            Container(
                              padding:const EdgeInsets.only(top: 15),
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              color:const Color(0xDFBDBDBD),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:const EdgeInsets.only(left: 18),
                                    child: IconButton(
                                      onPressed: () {
                                        setState((){
                                          _every10MinutesKHQR.cancel();
                                          showKHQR=false;
                                        });
                                      },
                                      icon:const Icon(Icons.close_rounded,color: Colorconstand.neutralWhite,size: 28,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: WebView(
                                initialUrl: location,
                                backgroundColor: Colorconstand.neutralWhite,
                                zoomEnabled:false,
                                javascriptMode: JavascriptMode.unrestricted,
                                onWebViewCreated: (WebViewController webViewController) {
                                },
                              ),
                            ),
                          ],
                        ),
                        
                    ):Container(height: 0,),
                 
                ],
              ),
        ),
      ),
    );
  }
}