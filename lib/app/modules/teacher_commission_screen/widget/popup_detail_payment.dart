import 'package:camis_teacher_application/app/help/save_file_helper.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
// import 'package:gallery_saver/gallery_saver.dart';
import '../../../../widget/button_widget/button_widget.dart';

class PopUpDetailStudentPayment extends StatefulWidget {
  String paymentDate;
  String classname;
  String studentName;
  String paymentOption;
  String pricePayment;
  String studentQr;
  String guardianQr;
  VoidCallback btnClose;
  PopUpDetailStudentPayment({required this.btnClose,required this.classname,required this.guardianQr,required this.paymentDate,required this.paymentOption,required this.pricePayment,required this.studentName,required this.studentQr,super.key});

  @override
  State<PopUpDetailStudentPayment> createState() => _PopUpDetailStudentPaymentState();
}

class _PopUpDetailStudentPaymentState extends State<PopUpDetailStudentPayment> {
  final oCcy =  NumberFormat("#,##0", "en_US");
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.symmetric(vertical: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
             Container(
                height: 50,
                width: 50,
                decoration: const BoxDecoration(
                    color: Colorconstand.mainColorForecolor,
                    shape: BoxShape.circle),
                child: const Icon(
                  Icons.check_outlined,
                  color: Colorconstand.neutralWhite,
                  size: 48,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  "PAYMENT_SUCCESS".tr(),
                  style: ThemsConstands.headline_2_semibold_24
                      .copyWith(color: Colorconstand.primaryColor),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  "DURATIONFINANCE".tr(),
                  style: ThemsConstands.headline4_regular_18
                      .copyWith(color: Colorconstand.lightBlack),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(widget.paymentDate,
                  style: ThemsConstands.headline_2_semibold_24
                      .copyWith(color: Colorconstand.primaryColor),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  "STUDENT_NAME".tr(),
                  style: ThemsConstands.headline4_regular_18
                      .copyWith(color: Colorconstand.lightBlack),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  widget.studentName,
                  style: ThemsConstands.headline_2_semibold_24
                      .copyWith(color: Colorconstand.primaryColor),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  "${"CLASS".tr()} ${widget.classname}",
                  style: ThemsConstands.headline4_regular_18
                      .copyWith(color: Colorconstand.lightBlack),
                ),
              ),

              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  widget.paymentOption,
                  style: ThemsConstands.headline_2_semibold_24
                      .copyWith(color: Colorconstand.primaryColor),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  "PAYMENT_PRICE".tr(),
                  style: ThemsConstands.headline4_regular_18
                      .copyWith(color: Colorconstand.lightBlack),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
                child: Text(
                  "${oCcy.format(int.parse(widget.pricePayment))} ៛",
                  style: ThemsConstands.headline_2_semibold_24
                      .copyWith(color: Colorconstand.primaryColor),
                ),
              ),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: (){
                        setState((){
                          saveImgae(image: widget.guardianQr);
                          saveImgae(image: widget.studentQr);
                          // GallerySaver.saveImage(widget.guardianQr,albumName: "CAMIS QRCcode Guardian");
                          // GallerySaver.saveImage(widget.studentQr,albumName: "CAMIS QRCcode Student");
                          Navigator.of(context).pop();
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colorconstand.primaryColor,content: Text("SAVE_TO_GALLARY".tr(),style: TextStyle(fontSize: 16),textAlign:TextAlign.center)));
                        });
                      },
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              height: 50,width: 50,
                              child: SvgPicture.asset(ImageAssets.scan_qr_icon,color: Colorconstand.primaryColor,),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 18),
                              child: Text(
                                "DOWNLOAD_QRCODE".tr(),
                                style: ThemsConstands.headline_6_semibold_14
                                    .copyWith(color: Colorconstand.lightBlack),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(width: 15,),
                    GestureDetector(
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              height: 50,width: 50,
                              child: SvgPicture.asset(ImageAssets.document,color: Colorconstand.primaryColor,),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 18),
                              child: Text(
                                "DOWNLOAD_INVOICE".tr(),
                                style: ThemsConstands.headline_6_semibold_14
                                    .copyWith(color: Colorconstand.lightBlack),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 15),
                child: Container(
                  child: Button_Custom(
                    titleButton: "CLOSE".tr(),
                    buttonColor: Colorconstand.primaryColor,
                    radiusButton: 10,
                    onPressed: widget.btnClose,
                    hightButton: 55,
                    maginRight: 22,
                    maginleft: 22,
                    titlebuttonColor: Colorconstand.neutralWhite,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}