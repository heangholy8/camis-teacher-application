import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/create_payment/bloc/create_payment_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../widget/button_widget/button_widget.dart';

class PopUpComfrimPayment extends StatefulWidget {
  String classname;
  String studentName;
  int paymentType;
  String pricePayment;
  String payFrom;
  String payForMonth;
  String guardinaId;
  String studentId;
  String classId;
  PopUpComfrimPayment({required this.classId,required this.guardinaId,required this.studentId,required this.payForMonth,required this.payFrom,required this.classname,required this.paymentType,required this.pricePayment,required this.studentName,super.key});

  @override
  State<PopUpComfrimPayment> createState() => _PopUpComfrimPaymentState();
}

class _PopUpComfrimPaymentState extends State<PopUpComfrimPayment> {
  final oCcy =  NumberFormat("#,##0", "en_US");
  bool checkBox = false;

  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  String? date;
  @override
  void initState() {
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
    yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
    date = "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal";
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (context, snapshot) {
        return Container(
          decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
          margin:const EdgeInsets.only(top: 65),
          child: Container(
             margin:const EdgeInsets.only(top: 15),
            child: SingleChildScrollView(
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
                child: Container(
                  margin:const EdgeInsets.only(top: 10,bottom: 25),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                         Container(
                          margin:const EdgeInsets.only(right: 12),
                            alignment: Alignment.centerRight,
                            child: IconButton(
                              onPressed: (){
                                Navigator.of(context).pop();
                              }, 
                              icon:const Icon(Icons.close_rounded,weight: 32,)
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 0),
                            child: Text(
                              "COMFIRM_PAYMENT".tr(),
                              style: ThemsConstands.headline_2_semibold_24
                                  .copyWith(color: Colorconstand.primaryColor),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(
                              "DURATIONFINANCE".tr(),
                              style: ThemsConstands.headline4_regular_18
                                  .copyWith(color: Colorconstand.lightBlack),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(date.toString(),
                              style: ThemsConstands.headline_2_semibold_24
                                  .copyWith(color: Colorconstand.primaryColor),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(
                              "STUDENT_NAME".tr(),
                              style: ThemsConstands.headline4_regular_18
                                  .copyWith(color: Colorconstand.lightBlack),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(
                              widget.studentName,
                              style: ThemsConstands.headline_2_semibold_24
                                  .copyWith(color: Colorconstand.primaryColor),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(
                              "${"CLASS".tr()} ${widget.classname}",
                              style: ThemsConstands.headline4_regular_18
                                  .copyWith(color: Colorconstand.lightBlack),
                            ),
                          ),
                         widget.payFrom=="cash"?Container():Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 18),
                                child: Text(
                                  "PAY_COMPANY_VAI".tr(),
                                  style: ThemsConstands.headline4_regular_18
                                      .copyWith(color: Colorconstand.lightBlack),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 18),
                                child: Text(
                                  "KHQR",
                                  style: ThemsConstands.headline_2_semibold_24
                                      .copyWith(color: Colorconstand.alertsDecline),
                                ),
                              ),
                            ],
                          ),
                          widget.payFrom =="cash"?Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 18),
                                child: Text(
                                  "PAY_THEACHER".tr(),
                                  style: ThemsConstands.headline4_regular_18
                                      .copyWith(color: Colorconstand.lightBlack),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 18),
                                child: Text(
                                  "IN_CASH".tr(),
                                  style: ThemsConstands.headline_2_semibold_24
                                      .copyWith(color: Colorconstand.primaryColor),
                                ),
                              ),
                            ],
                          ):Container(),
              
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(
                              "${"PAYMENT_SEVICE".tr()} ${widget.payForMonth} ${"MONTH".tr()}",
                              style: ThemsConstands.headline_2_semibold_24
                                  .copyWith(color: Colorconstand.primaryColor),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(
                              "TOTAL_PRICE".tr(),
                              style: ThemsConstands.headline4_regular_18
                                  .copyWith(color: Colorconstand.lightBlack),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18),
                            child: Text(
                              "${oCcy.format(int.parse(widget.pricePayment))} ៛",
                              style: ThemsConstands.headline_2_semibold_24
                                  .copyWith(color: Colorconstand.primaryColor),
                            ),
                          ),
                          widget.payFrom =="cash"?Column(
                            children: [
                              const SizedBox(height: 18,),
                              Container(
                                  margin:const EdgeInsets.symmetric(horizontal: 28),
                                  child: Row (
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [  
                                      InkWell(
                                        onTap: (){
                                            if(checkBox == false){
                                              setState(() {
                                                checkBox = true;
                                              });
                                            }
                                            else{
                                              setState(() {
                                                checkBox = false;
                                              });
                                            }
                                        },
                                        child: Container (
                                          alignment: Alignment.center,
                                          margin: const EdgeInsets.only(top: 10,right: 8),
                                          padding: const EdgeInsets.all(2),
                                          decoration: BoxDecoration(
                                            color: checkBox == true? Colorconstand.primaryColor:Colors.white,
                                            border: Border.all(color: Colorconstand.primaryColor),
                                            borderRadius: BorderRadius.circular(4),
                                          ),
                                          child: const Icon(Icons.check,color: Colors.white,size: 18,),
                                        ),
                                      ),
                                      SizedBox(
                                        width: MediaQuery.of(context).size.width - 132,
                                        child: Text("I_REALLRY_COMFIRMED".tr(), style: ThemsConstands.headline4_regular_18.copyWith( color: Colorconstand.neutralDarkGrey,height: 1.56),maxLines: 2,overflow: TextOverflow.ellipsis,)),
                                    ],
                                  ),
                                ),
                                const  SizedBox(height: 18,),
                            ],
                          ):Container(),
                          widget.payFrom !="cash"? Container(
                            margin: const EdgeInsets.only(top: 15),
                            child: Container(
                              child: Button_Custom(
                                titleButton:"APPROVE".tr(),
                                buttonColor: Colorconstand.primaryColor,
                                radiusButton: 10,
                                onPressed:() {
                                  setState(() {
                                    BlocProvider.of<CreatePaymentBloc>(context).add(CreatePaymentBakongEvent(studentId: widget.studentId.toString(),guardianId: widget.guardinaId.toString(),paymentType: widget.paymentType.toString(),classId: widget.classId.toString()),);
                                    Navigator.of(context).pop();
                                  });
                                },
                                hightButton: 55,
                                maginRight: 22,
                                maginleft: 22,
                                titlebuttonColor: Colorconstand.neutralWhite,
                              ),
                            ),
                          ):Container(),
                           widget.payFrom =="cash"? Container(
                            margin: const EdgeInsets.only(top: 15),
                            child: Container(
                              child: Button_Custom(
                                titleButton:"APPROVAL_MONEY".tr(),
                                buttonColor: Colorconstand.primaryColor,
                                radiusButton: 10,
                                onPressed: checkBox == true?() {
                                  setState(() {
                                    BlocProvider.of<CreatePaymentBloc>(context).add(CreatePaymentCashEvent(studentId: widget.studentId.toString(),guardianId: widget.guardinaId.toString(),paymentType: widget.paymentType.toString(),classId: widget.classId.toString()),);
                                    Navigator.of(context).pop();
                                  });
                                }:null,
                                hightButton: 55,
                                maginRight: 22,
                                maginleft: 22,
                                titlebuttonColor: Colorconstand.neutralWhite,
                              ),
                            ),
                          ):Container()
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      }
    );
  }
}