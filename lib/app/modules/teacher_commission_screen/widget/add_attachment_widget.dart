import 'dart:io';
import 'package:camis_teacher_application/app/modules/grading_screen/widget/custom_attach_widget.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/widget/preview_image_widget.dart';
import 'package:camis_teacher_application/app/modules/teacher_commission_screen/states/upload_attachment_commission/bloc/post_attachment_bloc.dart';

import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';


class AddAttachmentWidget extends StatefulWidget {

  const AddAttachmentWidget({Key? key,})
    : super(key: key);
  @override
  State<AddAttachmentWidget> createState() => _AddAttachmentWidgetState();
}

class _AddAttachmentWidgetState extends State<AddAttachmentWidget> {
  final ImagePicker _picker = ImagePicker();
  List<File> listImage = [];
  bool _isLoading = false;

  void getImageFromCamera() async {
    final PickedFile = await _picker.pickImage(source: ImageSource.camera);
    if (PickedFile != null) {
      setState(() {
        listImage.add(File(PickedFile.path));
      });
    }
  }

  Future getImageFromGallery() async {
    final pickedFiles = await _picker.pickMultiImage(imageQuality: 50);
    if (pickedFiles != null) {
      setState(() {
        pickedFiles.forEach((pickedFile) {
          listImage.add(File(pickedFile.path));
        
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                  icon: const Icon(Icons.close),
                ),
                Text(
                  "ATTACHMENT".tr(),
                  style: ThemsConstands.headline4_regular_18.copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(width: 40),
              ],
            ),
            Text(
              "ATTACHMENTTYPE".tr(),
              style: ThemsConstands.headline4_regular_18.copyWith(fontWeight: FontWeight.w600,color: Colorconstand.darkTextsDisabled),
            ),
            const SizedBox(
              height: 20,
            ),
            listImage.isNotEmpty
              ?  Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                height: 265,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: listImage.length + 1,
                  itemBuilder: (context, index) {
                    return index != listImage.length
                        ? Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: DottedBorder(
                              color: Colorconstand.neutralGrey,
                              dashPattern: const <double>[8, 8],
                              padding: const EdgeInsets.all(5),
                              radius: const Radius.circular(12),
                              borderType: BorderType.RRect,
                              strokeWidth: 1,
                              borderPadding: const EdgeInsets.all(1),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push( context,
                                    animtedRouteBottomToTop(
                                      topage: PreviewImageWidget(imageIndex: index),
                                    ),
                                  );
                                },
                                child: Stack(
                                  children: [
                                   
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius:BorderRadius.circular(20),
                                      ),
                                      child: Image.file(
                                        listImage[index],
                                        width: 150,
                                        height: 250,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Container(
                                      height: 250,
                                      width: 150,
                                      color: Colors.black.withOpacity(0.4),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment:CrossAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset( ImageAssets.eye_icon, color: Colors.white,
                                          ),
                                          const SizedBox( height: 10),
                                          Text(
                                            "PREVIEW".tr(),
                                            style: ThemsConstands.headline6_medium_14.copyWith(
                                              color: Colorconstand.lightBackgroundsWhite,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                     Positioned(
                                      top: 0,
                                      right: 0,
                                      child: InkWell(
                                        onTap: (){
                                          setState(() {
                                             listImage.removeAt(index);
                                          });
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: SvgPicture.asset(ImageAssets.CLOSE_ICON),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        : imageOptoinWidget(
                            onCamera: () {
                              getImageFromCamera();
                            },
                            onGallery: () {
                              getImageFromGallery();
                            },
                          );
                  },
                ),
              )
              : imageOptoinWidget(
                  onCamera: () {
                    getImageFromCamera();
                  },
                  onGallery: () {
                    getImageFromGallery();
                  },
                ),
                const SizedBox(height: 24,),
              BlocConsumer<PostAttachmentBloc, PostAttachmentState>(
                listener: (context, state) {
                  if (state is PostAttachmentLoaded) {
                    Navigator.pop(context);
                  }
                },
                builder: (context, state) {
                  if(state is PostAttachmentLoading){
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }else if(state is PostAttachmentLoaded){
                      return MaterialButton(
                        height: 57.18,
                        minWidth: MediaQuery.of(context).size.width,
                        onPressed:listImage.isEmpty?(){}:(){
                          BlocProvider.of<PostAttachmentBloc>(context).add(PostAttachmentCommissionEvent(attachment: listImage, year: "2023", month: "6"));
                        },
                        color: listImage.isEmpty?Colorconstand.darkTextsDisabled: Colorconstand.primaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.5),),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                          _isLoading?  const SizedBox(
                              height: 25,
                              width: 25,
                              child: CircularProgressIndicator(color: Colorconstand.neutralWhite,strokeWidth: 3)):Container(),
                            const SizedBox(width: 15,),
                            Text(
                              "UPLOAD".tr(),
                              style: ThemsConstands.button_semibold_16
                                  .copyWith(color: Colorconstand.lightGoten),
                            ),
                          ],
                        ),
                      );             
                    }
                  return Container();
                },
              )
          ],
        ),
      ),
    );

  }

  Widget imageOptoinWidget(
      {required Function()? onCamera, required Function()? onGallery}) {
    return DottedBorder(
      color: Colorconstand.neutralGrey,
      dashPattern: const <double>[8, 4],
      padding: const EdgeInsets.all(10),
      radius: const Radius.circular(12),
      borderType: BorderType.RRect,
      strokeWidth: 1.5,
      borderPadding: const EdgeInsets.all(1),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: onCamera,
            // () {
            //   _imgFromCamera();
            // },
            child: CustomAttachListWidget(
              title: "CAMERA".tr(),
              svg: ImageAssets.camera,
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          InkWell(
            onTap: onGallery,
            // () {
            //   getImageFromGallery();
            // },
            child: CustomAttachListWidget(
              title: "GALLERY".tr(),
              svg: ImageAssets.gallery,
            ),
          ),
        ],
      ),
    );
  }

}

Route animtedRouteBottomToTop({required Widget topage}) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => topage,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = const Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}