import 'dart:async';

import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../core/thems/thems_constands.dart';
import '../../student_list_screen/widget/body_shimmer_widget.dart';
import '../../student_list_screen/widget/contact_pairent_widget.dart';
import '../../student_list_screen/widget/header_shimmer_widget.dart';
import '../bloc/teacher_list_bloc.dart';

class TeacherListScreen extends StatefulWidget {
  const TeacherListScreen({Key? key}) : super(key: key);

  @override
  State<TeacherListScreen> createState() => _TeacherListScreenState();
}

class _TeacherListScreenState extends State<TeacherListScreen> with Toast {
  bool? classmonitor = false;
  bool connection = true;
  bool fristLoad = true;
  StreamSubscription? sub;
  String? idClass;
  int conditionGetTeacher = 1;
  int activeClass = 0;
  bool? selectClass;
  @override
  void initState() {
    setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true && fristLoad == true) {
            BlocProvider.of<GetClassBloc>(context).add(GetClass());
          } else {}
        });
      });
      //=============Eend Check internet====================
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Container(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10, left: 22, right: 22, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: SvgPicture.asset(
                              ImageAssets.chevron_left,
                              color: Colorconstand.neutralWhite,
                              width: 32,
                              height: 32,
                            ),
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "LEDGERTEACHER".tr(),
                                style: ThemsConstands.headline_2_semibold_24
                                    .copyWith(
                                  color: Colorconstand.neutralWhite,
                                ),
                                textAlign: TextAlign.center,
                              )),
                         const SizedBox( width: 32, height: 32,
                            //child: SvgPicture.asset(ImageAssets.question_circle,color: Colorconstand.neutralWhite,width: 28,height: 28,),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          decoration: const BoxDecoration(
                              color: Colorconstand.neutralWhite,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12))),
                          child: BlocBuilder<GetClassBloc, GetClassState>(
                            builder: (context, state) {
                              if (state is GetClassLoading) {
                                return Column(
                                  children:const[
                                    SizedBox(
                                      height: 50,
                                      child:  HeaderShimmerWidget()),
                                  ],
                                );
                              } else if (state is GetClassLoaded) {
                                var dataclass = state.classModel!.data;
                                var teachingClass = dataclass!.teachingClasses;
                                if (conditionGetTeacher == 1) {
                                  if (connection == true && fristLoad == true) {
                                   BlocProvider.of<TeacherListBloc>(context).add(GetTeacherListEvent(idClass:teachingClass![0].id.toString()));
                                  }
                                }
                                return teachingClass!.isEmpty
                                    ? Column(
                                        children: [
                                          Expanded(child: Container()),
                                          ScheduleEmptyWidget(
                                            title: "NO_CLASS".tr(),
                                            subTitle: "DESCRIPTION_DIVISION_READY".tr(),
                                          ),
                                          Expanded(child: Container()),
                                        ],
                                    )
                                    : Column(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              color: Colorconstand.primaryColor .withOpacity(0.9),
                                              borderRadius:const BorderRadius.only(topLeft:Radius.circular(12),topRight: Radius.circular(12))
                                            ),
                                            height: 55,
                                            child: ListView.separated(
                                              separatorBuilder: (context, index) {
                                                return Container(
                                                  width: 8,
                                                );
                                              },
                                              scrollDirection: Axis.horizontal,
                                              itemCount: teachingClass.length,
                                              itemBuilder: (context, indexClass) {
                                                return MaterialButton(
                                                  textColor: Colorconstand.primaryColor,
                                                  elevation: 0,
                                                  color: activeClass == indexClass
                                                          ? Colorconstand .neutralWhite
                                                          : Colors.transparent,
                                                  shape: const RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topLeft: Radius .circular(12),
                                                              topRight: Radius.circular(12)
                                                            )
                                                          ),
                                                  padding: const EdgeInsets.symmetric( vertical: 4,horizontal: 6),
                                                  onPressed: connection == false
                                                      ? () {
                                                          showMessage(
                                                            color: Colorconstand.neutralDarkGrey,
                                                              context: context,
                                                              title:"NOCONNECTIVITY".tr(),
                                                              hight: 25.0,
                                                              width: 250.0);
                                                        }
                                                      : selectClass == false? (){} :() {
                                                          setState(() {
                                                            BlocProvider.of<TeacherListBloc>( context).add(GetTeacherListEvent(idClass: teachingClass[indexClass].id.toString()));
                                                            activeClass = indexClass;
                                                            conditionGetTeacher = 0;
                                                            if (teachingClass[indexClass].isInstructor==0) {
                                                              classmonitor =false;
                                                            } else {
                                                              classmonitor = true;
                                                            }
                                                          });
                                                        },
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment .center,
                                                    children: [
                                                      teachingClass[indexClass] .isInstructor == 1
                                                          ? SvgPicture.asset(ImageAssets.ranking_icon,
                                                            width: 23,
                                                            color: activeClass ==indexClass
                                                                ? Colorconstand.primaryColor
                                                                : Colorconstand.neutralWhite,
                                                          )
                                                          : Container(),
                                                      Text(
                                                      translate =="km" ?"${teachingClass[indexClass].name}":teachingClass[indexClass].nameEn.toString()==""?"${teachingClass[indexClass].name}":"${teachingClass[indexClass].nameEn}",
                                                        style: activeClass == indexClass
                                                            ? ThemsConstands.button_semibold_16
                                                            : ThemsConstands.headline_5_medium_16.copyWith(color:Colorconstand.neutralWhite),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                          Expanded(
                                            child: BlocConsumer<TeacherListBloc,TeacherListState>(
                                              listener: (context, state) {
                                                if(state is GetTeacherLoading){
                                                  setState(() {
                                                    selectClass = false;
                                                  });
                                                }
                                                else if(state is GetTeacherLoaded){
                                                   setState(() {
                                                    selectClass = true;
                                                    fristLoad = false;
                                                  });
                                                }
                                                else{
                                                   setState(() {
                                                    selectClass = true;
                                                  });
                                                }
                                              },
                                              builder: (context, state) {
                                                if (state is GetTeacherLoading) {
                                                  return Column(
                                                    children: const[
                                                       BodyShimmerWidget(),
                                                    ],
                                                  );
                                                } else if (state
                                                    is GetTeacherLoaded) {
                                                  var datateacher = state
                                                      .teacherListModel!.data;
                                                  return datateacher!.isEmpty
                                                      ? Column(
                                                        children: [
                                                          Expanded(child: Container()),
                                                          ScheduleEmptyWidget(
                                                              title: "NOT_YET_AVAILABLE".tr(),
                                                              subTitle: "EMPTHY_CONTENT_DES".tr(),
                                                            ),
                                                          Expanded(child: Container()),
                                                        ],
                                                      )
                                                      : ListView.separated(
                                                            padding:
                                                        const EdgeInsets
                                                            .all(0),
                                                            itemCount: datateacher
                                                        .length,
                                                            itemBuilder: (context,
                                                        indexteacher) {
                                                      return ContactPairentWidget(
                                                          subjectName: translate == "km"? datateacher[indexteacher].subjectName.toString():datateacher[ indexteacher].subjectNameEn.toString(),
                                                          phone: datateacher[ indexteacher].teacherPhone.toString(),
                                                          name: translate =="km"? datateacher[ indexteacher].teacherName .toString():datateacher[ indexteacher].teacherNameEn.toString()==" "?datateacher[ indexteacher].teacherName .toString():datateacher[ indexteacher].teacherNameEn.toString(),
                                                          onTap: () {
                                                            if (datateacher[ indexteacher] .teacherPhone ==  "") {
                                                              showMessage(
                                                                  context: context,
                                                                  title: "NOPHONENUM".tr(),
                                                                  hight: 250.0,
                                                                  width:25.0);
                                                            } else {
                                                              String phone = datateacher[indexteacher].teacherPhone!
                                                                  .replaceAll( ' ', '');
                                                              _launchLink( "tel:$phone");
                                                            }
                                                          },
                                                          profile: datateacher[indexteacher].profileMedia!.fileShow ==
                                                                "" ||
                                                            datateacher[indexteacher].profileMedia!.fileShow ==
                                                                null
                                                        ? datateacher[
                                                                indexteacher]
                                                            .profileMedia!
                                                            .fileThumbnail
                                                            .toString()
                                                        : datateacher[
                                                                indexteacher]
                                                            .profileMedia!
                                                            .fileShow
                                                            .toString(),
                                                          role: translate=="km"? datateacher[indexteacher].subjectName.toString():datateacher[indexteacher].subjectNameEn.toString(),
                                                        );
                                                            },
                                                            separatorBuilder:
                                                        (context, ind) {
                                                      return Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left:
                                                                      100),
                                                          child:
                                                              const Divider(
                                                            height: 1,
                                                            thickness:
                                                                0.5,
                                                          ));
                                                            },
                                                          );
                                                } else {
                                                  return EmptyWidget(
                                                      title: "WE_DETECT".tr(),
                                                      subtitle: "WE_DETECT_DES".tr(),
                                                    );
                                                }
                                              },
                                            ),
                                          )
                                        ],
                                      );
                              } else{
                                return EmptyWidget(
                                        title: "WE_DETECT".tr(),
                                        subtitle: "WE_DETECT_DES".tr(),
                                      );
                              }
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          // connection==true?Container():Positioned(
          //   bottom: 0,left: 0,right: 0,top: 0,
          //   child: Container(
          //     color:const Color(0x7B9C9595),
          //   ),
          // ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }

  PopupMenuItem _buildPopupMenuItem(String title, IconData iconData) {
    return PopupMenuItem(
      mouseCursor: MouseCursor.uncontrolled,
      padding: const EdgeInsets.all(0),
      child: Container(
        padding: const EdgeInsets.only(bottom: 5),
        child: Container(
          height: 50,
          decoration: BoxDecoration(
              color: Colorconstand.gray300.withOpacity(0.4),
              borderRadius: const BorderRadius.all(Radius.circular(12))),
          margin: const EdgeInsets.symmetric(horizontal: 7),
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: Row(
            children: [
              Icon(
                iconData,
                color: Colorconstand.primaryColor,
                size: 20,
              ),
              const SizedBox(
                width: 8.0,
              ),
              Text(
                title,
                style: ThemsConstands.headline6_regular_14_24height,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }
}
