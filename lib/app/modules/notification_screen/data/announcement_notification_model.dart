// ignore_for_file: public_member_api_docs, sort_constructors_first


import 'package:camis_teacher_application/app/modules/notification_screen/data/type_announcement.dart';

class AnnouncementNotificationModel {
  final String? announcementStatus;
  final String? announcementTitle;
  final DateTime? dateTime;
  final bool? announcementSchool;
  TypeAnnouncement? typeAnnouncement;

  AnnouncementNotificationModel({
    this.announcementStatus,
    this.announcementTitle,
    this.dateTime,
    this.announcementSchool = false,
    required this.typeAnnouncement,
  });
}

final announcementNotificationModelList = <AnnouncementNotificationModel>[
  AnnouncementNotificationModel(
    announcementStatus: "សំបុត្រថ្នាក់រៀន",
    announcementTitle: "គម្រោងកែលម្អគុណភាពអប់រំចំណេះទូទៅ",
    dateTime: DateTime(2022, 10, 11),
    typeAnnouncement: TypeAnnouncement.myAnnouncement,
  ),
  AnnouncementNotificationModel(
    announcementStatus: "ដំណឹងបន្ទាន់របស់សាលា",
    announcementTitle: "ប្រកាសបិទសាលាបណ្ដោះអាសន្ន ទប់ស្កាត់ជំងឺ...",
    announcementSchool: true,
    dateTime: DateTime(2022, 10, 11),
    typeAnnouncement: TypeAnnouncement.myAnnouncement,
  ),
  
  AnnouncementNotificationModel(
    announcementStatus: "សេចក្តីជូនដំណឹង",
    announcementTitle: "គម្រោងកែលម្អគុណភាពអប់រំចំណេះទូទៅ",
    dateTime: DateTime(2022, 10, 11),
    typeAnnouncement: TypeAnnouncement.anouncement,
  ),
  AnnouncementNotificationModel(
    announcementStatus: "សេចក្តីជូនដំណឹង",
    announcementTitle: "គម្រោងកែលម្អគុណភាពអប់រំចំណេះទូទៅ",
    dateTime: DateTime(2022, 10, 11),
    typeAnnouncement: TypeAnnouncement.anouncement,
  ),
  AnnouncementNotificationModel(
    announcementStatus: "ដំណឹងបន្ទាន់របស់សាលា",
    announcementTitle: "គម្រោងកែលម្អគុណភាពអប់រំចំណេះទូទៅ",
    announcementSchool: true,
    dateTime: DateTime(2022, 10, 11),
    typeAnnouncement: TypeAnnouncement.anouncement,
  )
];
