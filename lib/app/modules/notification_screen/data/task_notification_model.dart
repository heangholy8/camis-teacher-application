// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:flutter/material.dart';

enum TypeTask {
  onGoing,
  history,
}

class TaskNotificationModel {
  final String? taskStatus;
  final bool? checkIconStatus;
  final String? taskTitle;
  final DateTime? dateTime;
  final String? deadline;
  final Color? classColor;
  final bool? checkAttendace;
  final bool? checkCompleteAttendace;
  
  TypeTask? typeTask;
  TaskNotificationModel({
    this.taskStatus = '',
    this.checkIconStatus = false,
    this.taskTitle = '',
    this.dateTime,
    this.deadline = '',
    this.classColor,
    this.checkCompleteAttendace = false,
    this.checkAttendace = false,
    this.typeTask,
  });
}

final TaskNotificationModelList = <TaskNotificationModel>[
  TaskNotificationModel(
    taskTitle: "ដាក់វត្តមានសិស្សថ្នាក់បន្ទាប់",
    taskStatus: "៩B",
    dateTime: DateTime(2022,10,11),
    checkAttendace: true,
    checkIconStatus: true,
    typeTask: TypeTask.onGoing
  ),
  TaskNotificationModel(
    taskTitle: "កំណែប្រឡងឆមាសទី១ ថ្នាក់១០B",
    taskStatus: "១០B",
    deadline: "១",
    classColor: Colorconstand.alertsDecline,
    typeTask: TypeTask.onGoing
    
  ),
  TaskNotificationModel(
    taskTitle: "កំណែប្រឡងឆមាសទី១ ថ្នាក់១១B",
    taskStatus: "១១B",
    deadline: "១",
    classColor: Colorconstand.alertsAwaitingText,
    typeTask: TypeTask.onGoing
    
  ),
  TaskNotificationModel(
    taskTitle: "កំណែប្រឡងឆមាសទី១ ថ្នាក់១២B",
    taskStatus: "១២B",
    deadline: "៥",
    classColor: Colorconstand.alertsPositive,
    typeTask: TypeTask.onGoing
    
  ),
  TaskNotificationModel(
    taskTitle: "ដាក់វត្តមានសិស្សថ្នាក់បន្ទាប់",
    taskStatus: "៩B",
    dateTime: DateTime(2022,10,11),
    checkAttendace: true,
    checkIconStatus: true,
    checkCompleteAttendace: true,
    typeTask: TypeTask.history
  ),
  TaskNotificationModel(
    taskTitle: "កំណែប្រឡងឆមាសទី១ ថ្នាក់១១B",
    taskStatus: "១១B",
    deadline: "១",
    classColor: Colorconstand.alertsAwaitingText,
    typeTask: TypeTask.history
    
  ),
  TaskNotificationModel(
    taskTitle: "កំណែប្រឡងឆមាសទី១ ថ្នាក់១២B",
    taskStatus: "១២B",
    deadline: "៥",
    classColor: Colorconstand.alertsPositive,
    typeTask: TypeTask.history
    
  ),
];
