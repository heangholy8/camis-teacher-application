import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/routes/app_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CreateModalBottomSheet extends StatelessWidget {
  const CreateModalBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3.0),
                  color: Colorconstand.lightBeerusBeerus),
              margin: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              width: 48.0,
              height: 6.0,
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'បង្កើតដំណឹង',
                style: ThemsConstands.headline_4_medium_18,
              ),
            ),
            CreateTextTileComponent(
              title: "សំបុត្រថ្នាក់រៀន",
              assetSvg: ImageAssets.NOTED_ICON,
              onTap: () {
                Navigator.pushNamed(context, Routes.ANNOUNCEMENT);
              },
            ),
            CreateTextTileComponent(
              title: "សំបុត្រអាណាព្យាបាល",
              assetSvg: ImageAssets.PEOPLE2_ICON,
              onTap: () {
                print("សំបុត្រអាណាព្យាបាល");
              },
            ),
            const SizedBox(
              height: 44.0,
            )
          ],
        ),
      ],
    );
  }
}

class CreateTextTileComponent extends StatelessWidget {
  String title;
  String assetSvg;
  void Function()? onTap;
  CreateTextTileComponent(
      {Key? key, required this.title, required this.assetSvg, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 64,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Colorconstand.neutralBtnBg),
        margin: const EdgeInsets.symmetric(horizontal: 20.5, vertical: 6.0),
        padding: const EdgeInsets.only(left: 12.0, right: 24.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: ThemsConstands.headline_4_medium_18,
            ),
            SvgPicture.asset(
              assetSvg,
              height: 24,
            ),
          ],
        ),
      ),
    );
  }
}
