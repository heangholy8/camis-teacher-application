import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TaskListWidget extends StatelessWidget {
  final String? taskStatus;
  final String? taskTitle;
  final String? datetime;
  bool? checkAttendace;
  final String? deadline;
  final Color? classColor;
  bool? checkCompleteAttendace;
  List<Color> gradientColors;
  TaskListWidget(
      {Key? key,
      this.taskStatus = "",
      this.taskTitle = "",
      this.datetime = "",
      this.deadline = "",
      this.classColor,
      this.gradientColors = const [Color(0xff911bc0), Color(0xff5f1fad)],
      this.checkAttendace = false,
      this.checkCompleteAttendace = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colorconstand.neutralWhite,
          border: Border(bottom: BorderSide(color: Colorconstand.neutralGrey))),
      padding:
          const EdgeInsets.only(left: 20.0, top: 12.0, bottom: 12, right: 24),
      child: Stack(
        children: [
          Row(
            children: [
              Stack(
                children: [
                  Container(
                    width: 45,
                    height: 45,
                    margin: const EdgeInsets.only(right: 19.0),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colorconstand.neutralDarkGrey,
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        begin: const Alignment(0, 0.5),
                        end: const Alignment(1, 0.5),
                        colors: gradientColors,
                      ),
                    ),
                    child: Text(
                      taskStatus!,
                      style: ThemsConstands.headline6_medium_14
                          .copyWith(color: Colorconstand.neutralWhite),
                    ),
                  ),
                  Positioned(
                      right: 19,
                      bottom: -1,
                      child: Container(
                        height: 16,
                        width: 16,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colorconstand.neutralBtnBg),
                          color: Colorconstand.primaryColor,
                          shape: BoxShape.circle,
                        ),
                        child: SvgPicture.asset(
                          checkAttendace!
                              ? ImageAssets.PEOPLETASK_ICON
                              : ImageAssets.CLIPBOARDTICK_ICON,
                          fit: BoxFit.cover,
                        ),
                      ))
                ],
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 19.0, bottom: 10.0),
                      child: Text(
                        taskTitle!,
                        overflow: TextOverflow.ellipsis,
                        style: ThemsConstands.button_semibold_16,
                      ),
                    ),
                    checkAttendace!
                        ? Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SvgPicture.asset(
                                ImageAssets.CALENDAR_ICON,
                                height: 16,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text(
                                  datetime!,
                                  style: ThemsConstands.caption_regular_12
                                      .copyWith(
                                    color: Colorconstand.darkTextsPlaceholder,
                                  ),
                                ),
                              ),
                            ],
                          )
                        : Row(
                            children: [
                              Text(
                                "ផុតកំណត់ៈ",
                                style: ThemsConstands
                                    .headline_6_regular_14_20height
                                    .copyWith(
                                        color: Colorconstand.neutralDarkGrey),
                              ),
                              Text(
                                " $deadline ថ្ងៃក្រោយ",
                                textAlign: TextAlign.center,
                                style: ThemsConstands.headline6_medium_14
                                    .copyWith(color: classColor),
                              ),
                            ],
                          )
                  ],
                ),
              ),
              checkAttendace!
                  ? !checkCompleteAttendace!
                      ? CustomMaterialButton(
                          onPressed: () {},
                          titleButton: "CHECK_ATTENDANCE".tr(),
                          padding: const EdgeInsets.all(0),
                          styleText: ThemsConstands.button_semibold_16
                              .copyWith(color: Colorconstand.neutralWhite),
                          colorButton: Colorconstand.primaryColor,
                        )
                      : CustomMaterialButton(
                          onPressed: () {},
                          titleButton: "DONE".tr(),
                          padding: const EdgeInsets.all(0),
                          styleText: ThemsConstands.headline_6_semibold_14
                              .copyWith(color: Colorconstand.alertsPositive),
                          colorButton: Colorconstand.alertsPositiveBg,
                          borderColor: Colorconstand.alertsPositive,
                        )
                  : Container(
                      width: 10,
                      height: 10,
                      decoration: BoxDecoration(
                          color: classColor, shape: BoxShape.circle),
                    ),
            ],
          )
        ],
      ),
    );
  }
}
