
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AnnouncementWidget extends StatelessWidget {
  String statusAnnouncement;
  String titleAnnouncement;
  String dateTime;
  bool? announcementSchool;
  AnnouncementWidget({
    Key? key,
    required this.statusAnnouncement,
    required this.titleAnnouncement,
    required this.dateTime,
    this.announcementSchool = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colorconstand.neutralWhite,
          border: Border(bottom: BorderSide(color: Colorconstand.neutralGrey))),
      padding: const EdgeInsets.only(left: 20.0, top: 12.0, bottom: 12),
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16.0),
                    color: (announcementSchool!)
                        ? Colorconstand.alertsDecline
                        : Colorconstand.primaryColor),
                padding:
                    const EdgeInsets.only(left: 10, right: 10, top: 12.0, bottom: 8.0),
                child: Text(
                  statusAnnouncement,
                  style: ThemsConstands.overline_semibold_12
                      .copyWith(color: Colorconstand.neutralWhite),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 10.0, bottom: 14.0, right: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        titleAnnouncement,
                        overflow: TextOverflow.ellipsis,
                        style: ThemsConstands.headline_5_medium_16
                            .copyWith(color: Colorconstand.primaryColor),
                      ),
                    ),
                    announcementSchool!
                        ? Container(
                            width: 10,
                            height: 10,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colorconstand.alertsDecline),
                          )
                        : Container()
                  ],
                ),
              ),
              Row(
                children: [
                  SvgPicture.asset(
                    ImageAssets.CALENDAR_ICON,
                    height: 18,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      dateTime,
                      style: ThemsConstands.caption_regular_12.copyWith(
                        color: Colorconstand.darkTextsPlaceholder,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          
        ],
      ),
    );
  }
}
