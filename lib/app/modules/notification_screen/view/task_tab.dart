import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/data/task_notification_model.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/widgets/task_list_widget.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TaskTab extends StatefulWidget {
  const TaskTab({super.key});

  @override
  State<TaskTab> createState() => _TaskTabState();
}

class _TaskTabState extends State<TaskTab> {
  List<TaskNotificationModel> data = TaskNotificationModelList;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.all(0),
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 21.0, top: 4, bottom: 4),
              child: Text(
                'OnGoing',
                style: ThemsConstands.headline6_medium_14,
              ),
            ),
            Container(
              child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                itemCount: data.length,
                padding: const EdgeInsets.all(0),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var myOnGoing = data[index].typeTask;
                  return myOnGoing == TypeTask.onGoing
                      ? TaskListWidget(
                          checkAttendace: data[index].checkAttendace,
                          classColor: data[index].classColor,
                          taskTitle: data[index].taskTitle,
                          taskStatus: data[index].taskStatus,
                          deadline: data[index].deadline,
                          checkCompleteAttendace:
                              data[index].checkCompleteAttendace,
                          datetime:
                              "${data[index].dateTime?.year} ${data[index].dateTime?.month} ${data[index].dateTime?.day}",
                        )
                      : Container();
                },
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 21.0, top: 4, bottom: 4),
              child: Text(
                'History',
                style: ThemsConstands.headline6_medium_14,
              ),
            ),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              itemCount: data.length,
              shrinkWrap: true,
              padding: const EdgeInsets.all(0),
              itemBuilder: (context, index) {
                var myHistory = data[index].typeTask;
                return myHistory == TypeTask.history
                    ? TaskListWidget(
                        checkAttendace: data[index].checkAttendace,
                        classColor: data[index].classColor,
                        taskTitle: data[index].taskTitle,
                        taskStatus: data[index].taskStatus,
                        deadline: data[index].deadline,
                        checkCompleteAttendace:
                            data[index].checkCompleteAttendace,
                        datetime:
                            "${data[index].dateTime?.year} ${data[index].dateTime?.month} ${data[index].dateTime?.day}",
                      )
                    : Container();
              },
            )
          ]),
    );
  }
}
