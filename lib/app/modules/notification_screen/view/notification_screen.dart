import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/view/announcement_tab.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/view/task_tab.dart';
import 'package:camis_teacher_application/widget/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotifictaionScreen extends StatefulWidget {
  const NotifictaionScreen({super.key});

  @override
  State<NotifictaionScreen> createState() => _NotifictaionScreenState();
}

class _NotifictaionScreenState extends State<NotifictaionScreen>
    with SingleTickerProviderStateMixin {
  late TabController controller;
  @override
  void initState() {
    controller = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNavigateBar(isActive: 3),
      body: Column(
        children: [
          Container(
            constraints: const BoxConstraints(maxHeight: 200),
            width: double.maxFinite,
            decoration: const BoxDecoration(
              color: Colorconstand.primaryColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12.0),
                  topRight: Radius.circular(12.0)),
            ),
            child: Stack(
              alignment: Alignment.bottomRight,
              children: [
                SvgPicture.asset(
                  ImageAssets.topbar_status_path,
                  fit: BoxFit.cover,
                  alignment: Alignment.topRight,
                ),
                Positioned(
                  bottom: 91.0,
                  left: 17.0,
                  child: Text(
                    'Notifications',
                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                      color: Colorconstand.neutralWhite,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Positioned(
                  bottom: 15,
                  left: 18.0,
                  right: 18.0,
                  child: Container(
                    constraints: const BoxConstraints(maxHeight: 50),
                    padding: const EdgeInsets.all(4.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.0),
                        color: Colorconstand.neutralSecondBackground),
                    child: TabBar(
                      controller: controller,
                      unselectedLabelColor: Colorconstand.lightBlack,
                      labelColor: Colorconstand.neutralWhite,
                      indicatorWeight: 2,
                      indicator: BoxDecoration(
                          color: Colorconstand.primaryColor,
                          borderRadius: BorderRadius.circular(12.0)),
                      unselectedLabelStyle:
                          ThemsConstands.headline6_regular_14_24height,
                      labelStyle: ThemsConstands.headline_6_semibold_14,
                      padding: const EdgeInsets.all(0),
                      tabs: const [
                        Tab(text: 'Announcement'),
                        Tab(text: 'Task'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
              child: TabBarView(
                controller: controller,
            children: const [
              AnnouncementTab(),
              TaskTab()
            ],
          ))
        ],
      ),
    );
  }
}
