import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/data/announcement_notification_model.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/data/type_announcement.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/widgets/announcement_list_widget.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/widgets/create_announcement_bottom_sheet.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scrolling_fab_animated/flutter_scrolling_fab_animated.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AnnouncementTab extends StatefulWidget {
  const AnnouncementTab({super.key});

  @override
  State<AnnouncementTab> createState() => _AnnouncementTabState();
}

class _AnnouncementTabState extends State<AnnouncementTab> {
  late ScrollController controller;
  List<AnnouncementNotificationModel> data = announcementNotificationModelList;
  @override
  void initState() {
    controller = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(0),
              controller: controller,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 21.0, top: 4, bottom: 4),
                  child: Text(
                    'My Announcement',
                    style: ThemsConstands.headline6_medium_14,
                  ),
                ),
                Container(
                  child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: data.length,
                    padding: const EdgeInsets.all(0),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      var myAnnouncement = data[index].typeAnnouncement;
                      return myAnnouncement == TypeAnnouncement.myAnnouncement
                          ? AnnouncementWidget(
                              statusAnnouncement:
                                  data[index].announcementStatus!,
                              titleAnnouncement:
                                  data[index].announcementTitle!,
                              announcementSchool:
                                  data[index].announcementSchool,
                              dateTime:
                                  "${data[index].dateTime!.year} ${data[index].dateTime!.month} ${data[index].dateTime!.day}",
                            )
                          : Container();
                    },
                  ),
                ),
                const Padding(
                  padding:
                      const EdgeInsets.only(left: 21.0, top: 4, bottom: 4),
                  child: Text(
                    'Announcement',
                    style: ThemsConstands.headline6_medium_14,
                  ),
                ),
                ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: data.length,
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(0),
                  itemBuilder: (context, index) {
                    var myAnnouncement = data[index].typeAnnouncement;
                    return myAnnouncement == TypeAnnouncement.anouncement
                        ? AnnouncementWidget(
                            statusAnnouncement:
                                data[index].announcementStatus!,
                            titleAnnouncement: data[index].announcementTitle!,
                            announcementSchool:
                                data[index].announcementSchool,
                            dateTime:
                                "${data[index].dateTime!.year} ${data[index].dateTime!.month} ${data[index].dateTime!.day}",
                          )
                        : Container();
                  },
                )
              ]),
          Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: Container(
                alignment: Alignment.center,
                child: ScrollingFabAnimated(
                  animateIcon: false,
                  
                  icon: SvgPicture.asset(
                    ImageAssets.ADD_ICON,
                    height: 24,
                    color: Colorconstand.neutralWhite,
                  ),
                  text: Text(
                    'បង្កើត',
                    style: ThemsConstands.button_semibold_16
                        .copyWith(color: Colorconstand.neutralWhite),
                  ),
                  onPress: () {
                    setState(() {
                      showModalBottomSheet(
                        context: context,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16.0),
                              topRight: Radius.circular(16.0)),
                        ),
                        builder: (context) {
                          return const CreateModalBottomSheet();
                        },
                      );
                    });
                  },
                  scrollController: controller,
                ),
              )),
        ],
      ),
    );
  }
}
