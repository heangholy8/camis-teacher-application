part of 'update_student_profile_bloc.dart';

abstract class UpdateStudentProfileState extends Equatable {
  const UpdateStudentProfileState();
  
  @override
  List<Object> get props => [];
}

class UpdateStudentProfileInitial extends UpdateStudentProfileState {}

class UpdateStudentProfileLoading extends UpdateStudentProfileState{}

class UpdateStudentProfileLoaded extends UpdateStudentProfileState {
  final UpdateStudentProfileModel? studentUpdateProfileModel;
  const UpdateStudentProfileLoaded({required this.studentUpdateProfileModel});
}

class UpdateStudentProfileError extends UpdateStudentProfileState {}
