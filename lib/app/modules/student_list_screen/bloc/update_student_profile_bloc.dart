import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/student_in_class_list/update_sutdent_api.dart';
import 'package:equatable/equatable.dart';
import '../../../models/student_list_model/update_profile_student_model.dart';
part 'update_student_profile_event.dart';
part 'update_student_profile_state.dart';

class UpdateStudentProfileBloc extends Bloc<UpdateStudentProfileEvent, UpdateStudentProfileState> {
  final UpdateStudentProfileApi updateStudentInfo;
  UpdateStudentProfileBloc({required this.updateStudentInfo}) : super(UpdateStudentProfileInitial()) {
    on<UpdateStudentInfoEvent>((event, emit) async{
      emit(UpdateStudentProfileLoading());
      try {
        var datastudentUpdate = await updateStudentInfo.updateStudentProfileApi(
          address: event.address, 
          dob: event.dob, 
          email: event.email, 
          fatheraddress: event.fatheraddress, 
          fatheremail: event.fatheremail, 
          fatherfirstname: event.fatherfname, 
          fatherfirstnameEn: event.fatherfnameEn, 
          fatherjob: event.fatherjob, 
          fatherlastname: event.fatherlname, 
          fatherlastnameEn: event.fatherlnameEn, 
          fatherphone: event.fatherphone, 
          firstname: event.fname, 
          firstnameEn: event.fnameEn, 
          gender: event.gender, 
          image: event.image, 
          lastname: event.lname, 
          lastnameEn: event.lnameEn, 
          motheraddress: event.motheraddress, 
          motheremail: event.motheremail, 
          motherfirstname: event.motherfname, 
          motherfirstnameEn: event.motherfnameEn, 
          motherjob: event.motherjob, 
          motherlastname: event.motherlname, 
          motherlastnameEn: event.motherlnameEn, 
          motherphone: event.motherphone, 
          phone: event.phone,
          classId: event.classId,
          studentId: event.studentId
        );
        emit(UpdateStudentProfileLoaded(studentUpdateProfileModel: datastudentUpdate));
      } catch (e) {
        emit(UpdateStudentProfileError());
      }
    });
  }
}
