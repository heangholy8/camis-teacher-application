part of 'student_in_class_bloc.dart';

abstract class GetClassEvent extends Equatable {
  const GetClassEvent();

  @override
  List<Object> get props => [];
}

class GetClass extends GetClassEvent{}
class GetStudentInClass extends GetClassEvent{
  final String idClass;
  const GetStudentInClass({required this.idClass});
}
