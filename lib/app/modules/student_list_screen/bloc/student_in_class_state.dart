part of 'student_in_class_bloc.dart';

abstract class GetClassState extends Equatable {
  const GetClassState();

  @override
  List<Object> get props => [];
}

class StudentInClassInitial extends GetClassState {}

class GetClassLoading extends GetClassState {}

class GetStudentClassLoading extends GetClassState{}

class GetClassLoaded extends GetClassState{
  final ClassModel? classModel;
  const GetClassLoaded({required this.classModel});
}

class GetStudentInClassLoaded extends GetClassState {
  final StudentInClassListModel? studentinclassModel;
  const GetStudentInClassLoaded({required this.studentinclassModel});
}

class GetClassError extends GetClassState {}

class GetStudentInClassError extends GetClassState {}
