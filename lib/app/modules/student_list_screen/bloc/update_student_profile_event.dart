part of 'update_student_profile_bloc.dart';

abstract class UpdateStudentProfileEvent extends Equatable {
  const UpdateStudentProfileEvent();

  @override
  List<Object> get props => [];
}

class UpdateStudentInfoEvent extends UpdateStudentProfileEvent {
  final String classId;
  final String studentId;

  final String lname;
  final String fname;
  final int gender;
  final String phone;
  final String lnameEn;
  final String fnameEn;
  final String dob;
  final String email;
  final String address;
  final File? image;

  final String motherlname;
  final String motherfname;
  final String motherphone;
  final String motherjob;
  final String motherlnameEn;
  final String motherfnameEn;
  final String motheremail;
  final String motheraddress;

  final String fatherlname;
  final String fatherfname;
  final String fatherphone;
  final String fatherjob;
  final String fatherlnameEn;
  final String fatherfnameEn;
  final String fatheremail;
  final String fatheraddress;

  const UpdateStudentInfoEvent(
      {required this.fname,
      required this.motherjob,
      required this.fatherjob,
      required this.classId,
      required this.studentId,
      required this.gender,
      required this.lname,
      required this.phone,required this.lnameEn, required this.fnameEn,required this.dob,required this.email,required this.address,
      required this.image,required this.motherlname, required this.motherfname, required this.motherphone, 
      required this.motherlnameEn, required this.motherfnameEn, required this.motheremail, 
      required this.motheraddress, required this.fatherlname, required this.fatherfname, 
      required this.fatherphone, required this.fatherlnameEn, required this.fatherfnameEn, 
      required this.fatheremail, required this.fatheraddress, 
    });
}