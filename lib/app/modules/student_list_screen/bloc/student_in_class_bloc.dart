import 'package:camis_teacher_application/app/models/class_list_model/class_list_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../models/student_list_model/list_student_in_class_model.dart';
import '../../../service/api/class_api/get_class_api.dart';
import '../../../service/api/student_in_class_list/get_Student_In_Class_Api.dart';
part 'student_in_class_event.dart';
part 'student_in_class_state.dart';

class GetClassBloc extends Bloc<GetClassEvent, GetClassState> {
  final GetClassApi getclass;
  int activeClass = 0;
  int classIDBloc = 0;
  final GetStudentInClassApi getstudentinclass = GetStudentInClassApi();
  List<TeachingClass> preventClass = [];

  GetClassBloc({required this.getclass}) : super(StudentInClassInitial()) {
    on<GetClass>((event, emit) async {
      emit(GetClassLoading());
      try {
        var dataclass = await getclass.getClassRequestApi();
        preventClass = dataclass.data!.teachingClasses!;

        emit(GetClassLoaded(classModel: dataclass));
      } catch (e) {
        debugPrint(e.toString());
        emit(GetClassError());
      }
    });
  }
}

class GetStudentInClassBloc extends Bloc<GetClassEvent, GetClassState> {
  final GetStudentInClassApi getstudentinclass;
  bool isFirstReloaded = false;
  GetStudentInClassBloc({required this.getstudentinclass})
      : super(StudentInClassInitial()) {
    on<GetStudentInClass>((event, emit) async {
      emit(GetStudentClassLoading());
      try {
        var datastudentinclass = await getstudentinclass
            .getStudentInClassRequestApi(idClass: event.idClass);
        emit(GetStudentInClassLoaded(studentinclassModel: datastudentinclass));
        isFirstReloaded = true;
      } catch (e) {
        debugPrint(e.toString());
        emit(GetStudentInClassError());
      }
    });
  }
}
