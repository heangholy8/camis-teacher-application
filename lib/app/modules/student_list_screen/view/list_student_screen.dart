import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:camis_teacher_application/app/service/api/student_in_class_list/post_student_role.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../widget/button_widget/button_widget_custom.dart';
import '../../../../widget/classmonitor_tag_button.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../mixins/toast.dart';
import '../../../models/class_list_model/class_list_model.dart';
import '../../../models/student_list_model/list_student_in_class_model.dart';
import '../../profile_user/profile_user_screen.dart';
import '../../set_mazer_screen/bloc/bloc/set_class_monitor_bloc.dart';
import '../widget/body_shimmer_widget.dart';
import '../widget/contact_pairent_widget.dart';
import '../widget/header_shimmer_widget.dart';

class LedgerStudentListScreen extends StatefulWidget {
  const LedgerStudentListScreen({super.key});

  @override
  State<LedgerStudentListScreen> createState() =>
      _LedgerStudentListScreenState();
}

class _LedgerStudentListScreenState extends State<LedgerStudentListScreen>
    with Toast {
  bool connection = true;
  StreamSubscription? sub;
  int isSelectedClass = 0;
  List<TeachingClass> previewClass = [];
  int maxClassmonitor = 0;
  int vice_maxClassmonitor = 0;
  bool isFirstReload = true;
  String? classID;
  bool isClickSubmit = false;
  bool changedLeader = false;
  int? setAttendance ;
  int? setInputScore;
  bool isInstructorOnClass = false;
  bool isDisplaySubmit = false;
  String? codeID;
  bool? dbClick = false;
  var classMonitorlenght;
  var vice_classMonitorlenght;
  List<ClassMonitorSet> dataSubmit = [];

  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {
        } else {}
      });
    });
     BlocProvider.of<GetClassBloc>(context).add(GetClass());
    super.initState();
  }

  @override
  void didChangeDependencies() {
    previewClass = BlocProvider.of<GetClassBloc>(context).preventClass;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: BlocListener<SetClassMonitorBloc, SetClassMonitorState>(
        listener: (context, state) {
          if (state is SetClassMonitorLoadingState) {
            setState(() {
              isClickSubmit = true;
            });
          } else if (state is SetClassMonitorSuccessState) {
            setState(() {
              yesnoAlertbox(context, isFull: false);
              isClickSubmit = false;
              changedLeader = false;
            });
          } else if (state is SetClassMonitorErrorState) {
          }
        },
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset("assets/images/Oval.png"),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: SafeArea(
                bottom: false,
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10, left: 22, right: 22, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: SvgPicture.asset(
                              ImageAssets.chevron_left,
                              color: Colorconstand.neutralWhite,
                              width: 32,
                              height: 32,
                            ),
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "LEDGERSTUDENT".tr(),
                                style: ThemsConstands.headline_2_semibold_24 .copyWith(
                                  color: Colorconstand.neutralWhite,
                                ),
                                textAlign: TextAlign.center,
                              )),
                          const SizedBox(
                            width: 32,
                            height: 32,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          decoration: const BoxDecoration(
                            color: Colorconstand.neutralWhite,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12),
                            ),
                          ),
                          child: headerWidget(translate: translate),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //submitButtonWidget(context),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget submitButtonWidget(BuildContext context) {
    return AnimatedPositioned(
      duration: const Duration(milliseconds: 400),
      bottom: isDisplaySubmit == true
          ? isInstructorOnClass == true
              ? changedLeader == true
                  ? 0
                  : -100
              : -100
          : -100,
      right: 0,
      left: 0,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomLeft,
            colors: [
              Colors.white.withOpacity(0.5),
              Colorconstand.lightBeerusBeerus.withOpacity(.4),
            ],
          ),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ButtonWidgetCustom(
              buttonColor: Colorconstand.primaryColor,
              onTap: () async {
                BlocProvider.of<SetClassMonitorBloc>(context).add(
                  SelectedClassMonitorEvent(
                    idClass: codeID.toString(),
                    listClassMonitor: dataSubmit,
                  ),
                );
              },
              panddinHorButton: 22,
              panddingVerButton: 12,
              radiusButton: 8,
              textStyleButton: ThemsConstands.button_semibold_16.copyWith(
                color: Colorconstand.neutralWhite,
              ),
              title: 'SUBMIT'.tr(),
            ),
            const SizedBox(
              height: 18,
            ),
          ],
        ),
      ),
    );
  }

  Widget headerWidget({required var translate}) {
    return BlocConsumer<GetClassBloc, GetClassState>(
      listener: (context, state) {
        if (state is GetClassLoaded) {
          setState(() {
            classID = state.classModel!.data!.teachingClasses![isSelectedClass].id.toString();
          });
        }
      },
      builder: (context, state) {
        if (state is GetClassLoading) {
          return Column(
            children:const [
              HeaderShimmerWidget(),
              BodyShimmerWidget(),
            ],
          );
        }
        else if (state is GetClassLoaded) {
          var data = state.classModel!.data;
          previewClass = data!.teachingClasses!;

          if (isFirstReload == true) {
            BlocProvider.of<GetStudentInClassBloc>(context).add(
              GetStudentInClass(
                idClass: previewClass[isSelectedClass].id.toString(),
              ),
            );
          }
          return data.teachingClasses!.isEmpty? 
           ScheduleEmptyWidget(
              title: "NOT_YET_AVAILABLE".tr(),
              subTitle: "EMPTHY_CONTENT_DES".tr(),
            ):Column(
              children: [
                Container(
                decoration: BoxDecoration(
                  color: Colorconstand.primaryColor.withOpacity(0.9),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                  ),
                ),
                height: 55,
                child: ListView.separated(
                  separatorBuilder: (context, index) {
                    return Container(
                      width: 8,
                    );
                  },
                  scrollDirection: Axis.horizontal,
                  itemCount: data.teachingClasses!.length,
                  itemBuilder: (context, indexClass) {
                    return InkWell(
                      onTap:connection == false
                        ? () {
                            showMessage(
                              color: Colorconstand.neutralDarkGrey,
                              context: context,
                              title:"NOCONNECTIVITY".tr(),
                              hight: 25.0,
                              width: 250.0
                            );
                          }
                        :
                       dbClick == false
                          ? null
                          : () {
                              setState(() {
                                changedLeader = false;
                                isSelectedClass = indexClass;
                                codeID = data.teachingClasses![isSelectedClass].id.toString();
                                classID =  data.teachingClasses![isSelectedClass].id.toString();
                                BlocProvider.of<GetStudentInClassBloc>(context).add(GetStudentInClass(idClass: data.teachingClasses![isSelectedClass].id.toString()));
                              });
                              if (data.teachingClasses![isSelectedClass].isInstructor == 1) {
                                setState(() {
                                  isInstructorOnClass = true;
                                });
                              } else {
                                setState(() {
                                  isInstructorOnClass = false;
                                });
                              }
                            },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                        decoration: BoxDecoration(
                          color: isSelectedClass == indexClass
                              ? Colorconstand.neutralWhite
                              : Colors.transparent,
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            data.teachingClasses![indexClass].isInstructor == 1
                                ? Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: SvgPicture.asset(
                                      ImageAssets.ranking_icon,
                                      width: 23,
                                      color: isSelectedClass == indexClass
                                          ? Colorconstand.primaryColor
                                          : Colorconstand.screenWhite,
                                    ),
                                  )
                                : Container(),
                            Text(
                              translate == 'km'
                                  ? "${data.teachingClasses![indexClass].name}"
                                  : "${data.teachingClasses![indexClass].nameEn}",
                              style: ThemsConstands.headline_5_medium_16.copyWith(
                                fontWeight: FontWeight.bold,
                                color: isSelectedClass == indexClass
                                    ? Colorconstand.primaryColor
                                    : Colorconstand.neutralWhite,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
               ),
               bodyWidget(),
              ],
            );
        }
        else{
           return EmptyWidget(
                  title: "WE_DETECT".tr(),
                  subtitle: "WE_DETECT_DES".tr(),
                );
        }
      },
    );
  }

  Widget bodyWidget() {
    final translate = context.locale.toString();
    return BlocConsumer<GetStudentInClassBloc, GetClassState>(
      listener: (context, state) {
        if (state is GetStudentClassLoading) {
          setState(() {
            dbClick = false;
          });
        } else if (state is GetStudentInClassLoaded) {
          setState(() {
            dbClick = true;
            isFirstReload = false;
          });
        } else {
          setState(() {
            dbClick = true;
          });
        }
      },
      builder: (context, state) {
        if (state is GetStudentClassLoading) {
          return const BodyShimmerWidget();
        }
        else if (state is GetStudentInClassLoaded) {
          var data = state.studentinclassModel!.data;
          classMonitorlenght = data!.where((element) => element.leadership == 1).length;
          maxClassmonitor = classMonitorlenght;
          vice_classMonitorlenght = data.where((element) => element.leadership == 2).length;
          vice_maxClassmonitor = vice_classMonitorlenght;
          dataSubmit = data;
          print("fasdf${data.length}");
          isDisplaySubmit = true;
          return Expanded(
            flex: 1,
            child: Stack(
              children: [
                data.isEmpty
                    ?  Column(
                      children: [
                        Expanded(child: Container()),
                        ScheduleEmptyWidget(
                            title: "NOT_YET_AVAILABLE".tr(),
                            subTitle: "EMPTHY_CONTENT_DES".tr(),
                          ),
                        Expanded(child: Container()),
                      ],
                    )
                    : ListView.separated(
                        cacheExtent: 9999,
                        shrinkWrap: true,
                        padding: const EdgeInsets.only(top: 0,left: 0,right: 0,bottom: 28),
                        itemCount: data.length,
                        itemBuilder: (context, indexstu) {
                          return MaterialButton(
                                  onLongPress: (){
                                    detailInfoStudentBottomSheet(context, data, indexstu,translate);
                                  },
                                  onPressed: previewClass[isSelectedClass].isInstructor == 0?null:() {
                                    Navigator.push(context,MaterialPageRoute(
                                      builder: (context) => ProfileUserScreen(
                                          classId: classID,
                                          profileMedia: data[indexstu].profileMedia!.fileShow.toString(),
                                          firstname: data[indexstu].firstname,
                                          firstnameEn: data[indexstu].firstnameEn,
                                          lastname: data[indexstu].lastname,
                                          lastnameEn: data[indexstu].lastnameEn,
                                          gmail: data[indexstu].email,
                                          gender: data[indexstu].gender == "ប្រុស" || data[indexstu].gender == "Male"?1:data[indexstu].gender == "ស្រី" || data[indexstu].gender == "Female"?2:0,
                                          address: data[indexstu].address,
                                          dob: data[indexstu].dob,
                                          leadership: data[indexstu].leadership,
                                          translate: translate,
                                          whereEdit: "profileStudent",
                                          dataStudentInfo: data[indexstu],
                                        ),
                                      ),
                                    );
                                  },
                                  padding: const EdgeInsets.symmetric(  vertical: 8, horizontal: 22),
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width: 70,
                                        height: 70,
                                        child: ClipOval(
                                          child:  Image.network(
                                              data[indexstu].profileMedia!.fileShow == "" ||
                                              data[indexstu] .profileMedia!.fileShow == null
                                                ? data[indexstu].profileMedia! .fileThumbnail.toString()
                                                : data[indexstu].profileMedia!.fileShow.toString(),
                                                fit: BoxFit.cover,
                                            ),
                                          //   
                                          // ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: const EdgeInsets.only(left: 18, right: 0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                translate=="km" ? "${data[indexstu].name}":data[indexstu].nameEn==" "? "NONAME".tr():"${data[indexstu].nameEn}",
                                                style: ThemsConstands.headline_4_medium_18.copyWith(
                                                  color: Colorconstand.lightBulma,
                                                ),
                                              ),
                                              Text(
                                               "${"GENDER".tr()}: ${data[indexstu].gender.toString().tr()}",
                                                style: ThemsConstands.caption_regular_12.copyWith(
                                                  color: Colorconstand.lightBulma,
                                                ),
                                              ),
                                              
                                            ],
                                          ),
                                        ),
                                      ),
                                      previewClass[isSelectedClass].isInstructor == 1
                                          ? Row(
                                            children: [
                                              data[indexstu].leadership == 1||data[indexstu].leadership == 2?
                                                  Container(
                                                    child: PopupMenuButton(
                                                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
                                                      padding:const EdgeInsets.symmetric(horizontal: 8),
                                                      onSelected: (value) {
                                                        print("count: $value");
                                                      },
                                                      icon:  const Icon(Icons.add, size: 22, color: Colorconstand.onLightOnLight04),
                                                      itemBuilder: (ctx) => [
                                                        // popupItemLong(
                                                        //   maximunClassmonitor:  maxClassmonitor,
                                                        //   title: 'SETATTENDANCE'.tr(),
                                                        //   iconData:   Icons.person_pin_rounded,
                                                        //   onTap: () {
                                                        //     setState(() {
                                                        //       if(data[indexstu].takeAttendance == 1){
                                                        //         data[indexstu].takeAttendance = 0;
                                                        //       }
                                                        //       else{
                                                        //         data[indexstu].takeAttendance = 1;
                                                        //       }
                                                        //     });
                                                        //     var api = PostStudentRole();
                                                        //     print("StudentId : ${data[indexstu].studentId.toString()}, classId : ${data[indexstu].takeAttendance}");
                                                        //     api.postStudentRoleTask(
                                                        //       classID: data[indexstu].classId!,
                                                        //       student_id:data[indexstu].studentId!,
                                                        //       take_attendance: data[indexstu].takeAttendance!,
                                                        //       input_score: data[indexstu].inputScore!
                                                        //     );
                                                        //   },
                                                        //   isTask: data[indexstu].takeAttendance
                                                        // ),
                                                        popupItemLong(
                                                          maximunClassmonitor:  maxClassmonitor,
                                                          title: 'SETINPUTSCORE'.tr(),
                                                          iconData:   Icons.wysiwyg_rounded,
                                                          onTap: () {
                                                           setState(() {
                                                            if( data[indexstu].inputScore == 1){
                                                               data[indexstu].inputScore = 0;
                                                            }else{
                                                               data[indexstu].inputScore = 1;
                                                            }
                                                          });
                                                          var api = PostStudentRole();
                                                          api.postStudentRoleTask(
                                                              classID: data[indexstu].classId!,
                                                              student_id:data[indexstu].studentId!,
                                                              take_attendance: data[indexstu].takeAttendance!,
                                                              input_score: data[indexstu].inputScore!
                                                            );
                                                          },
                                                          isTask: data[indexstu].inputScore
                                                        ),
                                                      ],
                                                    ),
                                                  ):Container(),
                                                  data[indexstu].inputScore==1?const Icon(Icons.wysiwyg_rounded,color: Colorconstand.primaryColor,size: 15,):Container(),
                                                  data[indexstu].takeAttendance==1?const Icon(Icons.person_pin_rounded,color: Colorconstand.primaryColor,size: 15):Container(),
                                              Container(
                                                decoration: const BoxDecoration(shape: BoxShape.circle),
                                                child: data[indexstu].leadership ==  0
                                                    ? 
                                                   vice_maxClassmonitor + maxClassmonitor >=5?Container(width: 0,height: 0,) : PopupMenuButton(
                                                      shape: const RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.all(
                                                          Radius.circular(15.0),
                                                        ),
                                                      ),
                                                      onSelected: (value) {
                                                        if (value == 5) {
                                                          yesnoAlertbox(context,isFull: true);
                                                        }
                                                      },
                                                      icon: const Icon(Icons.more_vert_outlined,
                                                          size: 22,
                                                          color: Colorconstand.onLightOnLight04),
                                                      itemBuilder: (ctx) => [
                                                        maxClassmonitor > 0? PopupMenuItem(height: 0,child: Container(),) : _buildPopupMenuItem(
                                                          maximunClassmonitor:  maxClassmonitor,
                                                          title: 'Appointer'.tr(),
                                                          iconData: Icons .person_pin_outlined,
                                                          onTap: () {
                                                              setState(() {
                                                                changedLeader =  true;
                                                                data[indexstu].leadership = 1;
                                                                BlocProvider.of<SetClassMonitorBloc>(context).add(SelectedClassMonitorEvent(idClass: data[indexstu].classId.toString(),listClassMonitor: data));
                                                              });
                                                          },
                                                        ),
                                                        vice_maxClassmonitor>=4?PopupMenuItem(height: 0,child: Container(),) : _buildPopupMenuItem(
                                                          maximunClassmonitor:  maxClassmonitor,
                                                          title: 'VICE_APPOINTER'.tr(),
                                                          iconData: Icons .person_pin_outlined,
                                                          onTap: () {
                                                            if (vice_maxClassmonitor >= 4) {
                                                            } else {
                                                              setState(() {
                                                                changedLeader =  true;
                                                                data[indexstu].leadership = 2;
                                                                BlocProvider.of<SetClassMonitorBloc>(context).add(SelectedClassMonitorEvent(idClass: data[indexstu].classId.toString(),listClassMonitor: data));
                                                              });
                                                            }
                                                          },
                                                        ),
                                                       
                                                      ],
                                                    )
                                                    : data[indexstu].leadership == 1?InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            data[indexstu].leadership = 0;
                                                            data[indexstu].takeAttendance = 0;
                                                            data[indexstu].inputScore = 0;
                                                            BlocProvider.of<SetClassMonitorBloc>(context).add(SelectedClassMonitorEvent(idClass: data[indexstu].classId.toString(),listClassMonitor: data));
                                                            var api = PostStudentRole();
                                                            api.postStudentRoleTask(
                                                              classID: data[indexstu].classId!,
                                                              student_id:data[indexstu].studentId!,
                                                              take_attendance: data[indexstu].takeAttendance!,
                                                              input_score: data[indexstu].inputScore!
                                                            );
                                                            changedLeader = true;
                                                          });
                                                        },
                                                        child: ClassMonitorTagWidget(title: "CLASSMONITOR".tr(),),
                                                    ):data[indexstu].leadership == 2?InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            data[indexstu].leadership = 0;
                                                            data[indexstu].takeAttendance = 0;
                                                            data[indexstu].inputScore = 0;
                                                            BlocProvider.of<SetClassMonitorBloc>(context).add(SelectedClassMonitorEvent(idClass: data[indexstu].classId.toString(),listClassMonitor: data));
                                                            var api = PostStudentRole();
                                                            api.postStudentRoleTask(
                                                              classID: data[indexstu].classId!,
                                                              student_id:data[indexstu].studentId!,
                                                              take_attendance: data[indexstu].takeAttendance!,
                                                              input_score: data[indexstu].inputScore!
                                                            );
                                                            changedLeader = true;
                                                          });
                                                        },
                                                        child:ClassMonitorTagWidget(title: "VICE".tr(),),
                                                    ):Container(),
                                              ),
                                            ],
                                          )
                                          : data[indexstu].leadership == 1  ? ClassMonitorTagWidget(title: "CLASSMONITOR".tr(),):data[indexstu].leadership == 2?ClassMonitorTagWidget(title: "VICE".tr(),): Container()
                                  
                                    ],
                                  ),
                                );
                              // : SizedBox(
                              //     height: 80,
                              //     width: MediaQuery.of(context).size.width,
                              //   );
                        },
                        separatorBuilder: (context, ind) {
                          return Container(
                              margin: const EdgeInsets.only(left: 100),
                              child: const Divider(
                                height: 1,
                                thickness: 0.5,
                              ));
                        },
                      ),
                isClickSubmit == true
                    ? AnimatedContainer(
                        duration: const Duration(milliseconds: 300),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colorconstand.subject13.withOpacity(0.6),
                        child: const Center(
                          child: CircularProgressIndicator(
                            color: Colorconstand.neutralWhite,
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          );
        } else{
          return EmptyWidget(
                title: "WE_DETECT".tr(),
                subtitle: "WE_DETECT_DES".tr(),
              );
        }
      },
    );
  }

  Future<dynamic> yesnoAlertbox(BuildContext context, {required bool isFull}) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              isFull != true
                  ? SvgPicture.asset(
                      ImageAssets.check_icon,
                      width: 62,
                      color: const Color.fromARGB(255, 0, 185, 96),
                    )
                  : SvgPicture.asset(
                      ImageAssets.warning_icon,
                      width: 62,
                      color: Colorconstand.primaryColor,
                    ),
            ],
          ),
          content: Text(
            isFull == true ? "MESSAGEMAZZER".tr() : "MODIFYMAZZER".tr(),
            textAlign: TextAlign.center,
            style: ThemsConstands.headline3_medium_20_26height
                .copyWith(height: 1.5),
          ),
          actions: <Widget>[
            TextButton(
              child: Text("OKAY".tr(),
                  style: ThemsConstands.button_semibold_16.copyWith(
                      color: isFull != true
                          ? const Color.fromARGB(255, 0, 185, 96)
                          : Colorconstand.primaryColor)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  detailInfoStudentBottomSheet( BuildContext context, List<ClassMonitorSet> datastudent, int indexstu,String translate) {
    return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        context: context,
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 16, horizontal: 28),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("STUDENTINFO".tr(),
                        style: ThemsConstands.headline6_regular_14_24height
                            .copyWith(
                          color: Colorconstand.neutralDarkGrey,
                        )),
                    Text(
                    translate=="km"?  datastudent[indexstu].name.toString():datastudent[indexstu].nameEn==" "?"No Name":datastudent[indexstu].nameEn.toString(),
                      style: ThemsConstands.headline_4_medium_18.copyWith(
                        color: Colorconstand.lightBulma,
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                height: 1,
                thickness: 0.5,
              ),
              ContactPairentWidget(
                subjectName: "",
                phone: datastudent[indexstu].guardianPhone.toString(),
                name: datastudent[indexstu].guardianName.toString(),
                onTap: datastudent[indexstu].guardianPhone == ""
                    ? () {}
                    : () {
                        Navigator.of(context).pop();
                        if (datastudent[indexstu].guardianPhone == "") {
                          showMessage(
                              context: context,
                              title: "NOPHONENUM".tr(),
                              hight: 250.0,
                              width: 25.0);
                        } else {
                          _launchLink( "tel:${datastudent[indexstu].guardianPhone}");
                        }
                      },
                profile:
                    "",
                role: "អាណាព្យាបាល",
              ),
              ContactPairentWidget(
                subjectName: "",
                phone: datastudent[indexstu].fatherPhone.toString(),
                name: datastudent[indexstu].fatherName.toString(),
                onTap: () {
                  Navigator.of(context).pop();
                  if (datastudent[indexstu].fatherPhone == "") {
                    showMessage(
                        context: context,
                        title: "NOPHONENUM".tr(),
                        hight: 250.0,
                        width: 25.0);
                  } else {
                    _launchLink("tel:${datastudent[indexstu].fatherPhone}");
                  }
                },
                profile: "",
                role: "ឪពុក",
              ),
              ContactPairentWidget(
                subjectName: "",
                phone: datastudent[indexstu].motherPhone.toString(),
                name: datastudent[indexstu].motherName.toString(),
                onTap: () {
                  Navigator.of(context).pop();
                  if (datastudent[indexstu].motherPhone == "") {
                    showMessage(
                        context: context,
                        title: "NOPHONENUM".tr(),
                        hight: 250.0,
                        width: 25.0);
                  } else {
                    _launchLink("tel:${datastudent[indexstu].motherPhone}");
                  }
                },
                profile: "",
                role: "ម្ដាយ",
              ),
            ],
          );
        });
  }
    PopupMenuItem popupItemLong(
      {required String title,
      required IconData iconData,
      int? isTask,
      int? maximunClassmonitor,
      required Function() onTap}) {
    return PopupMenuItem(
      
      value: maxClassmonitor,
      onTap: onTap,
      mouseCursor: MouseCursor.uncontrolled,
      padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 4),

      child: Container(
        decoration: BoxDecoration(
          color: Colorconstand.gray300.withOpacity(0.4),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
        ),
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 18,horizontal: 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              iconData,
              color: Colorconstand.primaryColor,
              size: 20,
            ),
            const SizedBox(
              width: 8.0,
            ),
            Text(
              title,
              style: ThemsConstands.headline_6_regular_14_20height,
            ),
            const SizedBox(
              width: 8.0,
            ),
            isTask==1? const Icon(
              Icons.check_circle_sharp,
              color: Colorconstand.alertsPositive,
              size: 20,
            ):Container(),
          ],
        ),
      ),
    );
  }


  PopupMenuItem _buildPopupMenuItem(
      {required String title,
      required IconData iconData,
      int? maximunClassmonitor,
      required Function() onTap}) {
    return PopupMenuItem(
      value: maxClassmonitor,
      onTap: onTap,
      mouseCursor: MouseCursor.uncontrolled,
      padding: const EdgeInsets.all(0),
      child: Container(
        decoration: BoxDecoration(
          color: Colorconstand.gray300.withOpacity(0.4),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
        ),
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 18,horizontal: 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              iconData,
              color: Colorconstand.primaryColor,
              size: 20,
            ),
            const SizedBox(
              width: 8.0,
            ),
            Text(
              title,
              style: ThemsConstands.headline_6_regular_14_20height,
            ),
          ],
        ),
      ),
    );
  }

}
