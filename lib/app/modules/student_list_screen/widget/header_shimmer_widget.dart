import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../../core/constands/color_constands.dart';

class HeaderShimmerWidget extends StatelessWidget {
  const HeaderShimmerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: const Color(0xFF3688F4),
      highlightColor: const Color(0xFF3BFFF5),
      child: Container(
        decoration: BoxDecoration(
          color: Colorconstand.primaryColor.withOpacity(0.2),
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(12), topRight: Radius.circular(12)),
        ),
        height: 55,
      ),
    );
  }
}
