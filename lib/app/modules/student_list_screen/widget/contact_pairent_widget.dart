import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';

class ContactPairentWidget extends StatelessWidget {
  final String profile;
  final String name;
  final String subjectName;
  final String role;
  final String phone;
  VoidCallback onTap;
  ContactPairentWidget(
      {Key? key,
      required this.profile,
      required this.name,
      required this.role,
      required this.onTap,
      required this.subjectName,
      required this.phone})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var translate =context.locale.toString();
    return MaterialButton(
      onPressed: phone == "" ? (){} : onTap,
      padding: const EdgeInsets.all(0),
      color: Colorconstand.screenWhite,
      disabledColor: const Color(0xFF000000).withOpacity(.02),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 22),
        child: Row(
          children: [
            SizedBox(
              width: 50,
              height: 50,
              child: ClipOval(
                child: profile == "" || profile.isEmpty
                    ? Image.asset(
                        "assets/images/blank-profile.png",
                        fit: BoxFit.cover,
                      )
                    : Image.network(
                        profile,
                        fit: BoxFit.cover,
                      ),
              ),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(left: 18, right: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                     name == "" || name.isEmpty ? "NO Name" : name,
                      style: ThemsConstands.headline_4_medium_18.copyWith(
                        color: Colorconstand.lightBulma,
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      children: [
                        subjectName == ""?const SizedBox():Text(
                         "($subjectName) ",
                          style: ThemsConstands.caption_simibold_12.copyWith(
                            color: Colorconstand.lightBulma,
                          ),
                        ),
                        Text(
                         phone,
                          style: ThemsConstands.caption_regular_12.copyWith(
                            color: Colorconstand.lightBulma,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colorconstand.mainColorSecondary.withOpacity(0.1),
                  shape: BoxShape.circle),
              child: SvgPicture.asset(phone == ""
                  ? "assets/icons/svg/call-slash.svg"
                  : ImageAssets.call_outlinr_icon),
            )
          ],
        ),
      ),
    );
  }
}
