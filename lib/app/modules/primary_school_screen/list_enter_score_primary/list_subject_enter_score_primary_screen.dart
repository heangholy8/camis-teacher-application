import 'dart:async';

import 'package:camis_teacher_application/app/bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/state_management/calculate_score/bloc/calculate_score_bloc.dart';
import 'package:camis_teacher_application/app/modules/primary_school_screen/list_enter_score_primary/state/list_all_subject_primary/bloc/list_subject_enter_score_bloc.dart';
import 'package:camis_teacher_application/app/modules/result_screen/state/bloc/monthly_result_bloc.dart';
import 'package:camis_teacher_application/app/service/api/tracking_teacher_api/tracking_teacher_api.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../../widget/shimmer/shimmer_style.dart';
import '../../../models/grading_model/grading_model.dart';
import '../../../models/profile_model/profile_model.dart';
import '../../../service/api/approved_api/approved_api.dart';
import '../../../service/api/grading_api/post_graeding_data_api.dart';
import '../../../storages/get_storage.dart';
import '../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../result_screen/view/year_result_screen.dart';
import '../result_primary_school/primary_school_result_screen.dart';
import '../result_primary_school/state/result_primary_state/bloc/result_primary_school_bloc.dart';
import '../widget/card_enter_score_primary.dart';

class ListSubjectEnterScorePrimaryScreen extends StatefulWidget {
  const ListSubjectEnterScorePrimaryScreen({super.key});

  @override
  State<ListSubjectEnterScorePrimaryScreen> createState() => _ListSubjectEnterScorePrimaryScreenState();
}
class _ListSubjectEnterScorePrimaryScreenState extends State<ListSubjectEnterScorePrimaryScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  bool isCheckClass = false;
  TabController? controller;
  String optionClassName = "";
  String optionClassNameSelect = "SUBJECT".tr();
  bool isInstructor = false;
  bool activeMySchedule = false;
  String classIdTeacher = "";
  String classIdName = "";
  //=============== End Varible main Tap =====================


  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================
  
  //============= Calculate Score ===========
    bool isLoadingCalculate = false;
    bool firstLoadProfile = false;
    int calculateSuccess = 0;
    String statusCalculate  = "";
  //================================

 //============== verible Exam Tap ====================
    int? monthLocalExam;
    int? yearLocalExam;
    String? monthLocalNameExam;
    String? monthLocalNameExamFirstLoad;
    int? monthLocalExamFirstLoad;
    int? yearLocalExamFirstLoad;
    bool showListMonthExam = false;
    String semesterName = "";
    String semesterNameFristLoad = "";
    String typeSelection = "1";
    int activeIndexMonth = 0;

  int inStuctorLenght = 0;
  int indexInStuctorLenght = 0;
  Data? dataTeacherprofile;
 //============== verible Exam Tap ====================

    List subjectApprove = [];

    StreamSubscription? internetconnection;
    bool isoffline = true;

     ScrollController scrollController = ScrollController();
     bool _isLast = false;

     bool displayCalculateScoreYear = false;
//============== Check Save Score local do complet task ============
    //bool isHasDoTask=false;
    GradingData data = GradingData();
    List<StudentsData> studentData = [];
    String isInstructorLocalSave = "";
    bool isLoading = false;
    StreamSubscription? sub;
final GetStoragePref _getStoragePref = GetStoragePref();

  void loadCheckScore() async{
    BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
    var isIntructorPref = await _getStoragePref.getInstructor.then((value){
      if(value!=null){
          setState(() {
            isInstructorLocalSave = value;
          });
      }
    });
    var isLocalDataPref =  await _getStoragePref.getScoreData.then((value){
      if(value.studentsData!=null){
          setState(() {
           // isHasDoTask = true;
            data = value;
            studentData  = data.studentsData!.toList();

            var api = PostGradingApi();
            api.postListStudentSubjectExam(
              classID: data.classId!,
              subjectID: data.subjectId!,
              term: data.semester,
              examDate: "",
              type: data.type!,
              month: data.month,
              listStudentScore: studentData,
            ).then((value) async {
              setState(() {
                // isLoading = false;
                // isHasDoTask = false;
              });
              int totalScoreStudent = studentData.where((element) => element.score!=null).length;
              if(isInstructorLocalSave=="1"&&totalScoreStudent==0){
                ApprovedApi().postApproved(classId: data.classId, examObjectId: data.id, isApprove: 1, discription: "");
              }
              SharedPreferences pref = await SharedPreferences.getInstance();
              pref.remove("Data Score").then((value) => print("remove data score successfully"));
              pref.remove("Instructor").then((value) => print("remove instructor successfully"));
            });
          });
      }
      
    });
  }
//============== End Check Save Score local do complet task ============

  @override
  void initState() {
    setState(() {
    //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true) {
            loadCheckScore();
          }
        });
      });
      //=============Eend Check internet===============
    //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

      //============= controller class change option ========
      _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      monthLocalNameExam = DateFormat.MMMM("en-IN").format(DateTime.now()).toUpperCase();
      //==============  Exam Tap ====================
     monthLocalExam = monthlocal;
     yearLocalExam = yearchecklocal;
     monthLocalExamFirstLoad = monthlocal;
     yearLocalExamFirstLoad = yearchecklocal;
     monthLocalNameExamFirstLoad = monthLocalNameExam;
    //============== Exam Tap =====================
    });
    //============== Event call Data =================
    //============== End Event call Data =================
    super.initState();
    if(isoffline == true){
      loadCheckScore();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }
  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
        return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      bottomNavigationBar: isoffline==false?Container(height: 0,):const BottomNavigateBar(isActive: 3),
       // bottomNavigationBar:Container(height: 0),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: Container(
                  margin:const EdgeInsets.only(top: 10),
                  child: BlocListener<CalculateScoreBloc, CalculateScoreState>(
                    listener: (context, state) {
                      if(state is CalculateLoading){
                        setState(() {
                          isLoadingCalculate = true;
                        });
                      }
                      else if(state is CalculateLoaded){
                        setState(() {
                          calculateSuccess = 1;
                          statusCalculate = "SUCCESSFULLY".tr();
                          Future.delayed(const Duration(milliseconds: 500),() async{
                            setState(() {
                              isLoadingCalculate = false;
                              if(typeSelection == "1" || typeSelection == "2" ){
                                BlocProvider.of<ResultPrimarySchoolBloc>(context).add(ListStudentResultEvent(semester: semesterName,idClass:classIdTeacher,month: monthLocalExam.toString(),type: typeSelection));
                                Navigator.push(context,
                                  PageTransition( type:PageTransitionType.bottomToTop,
                                    child: ViewResultPrimarySchoolScreen(type: typeSelection,classId: classIdTeacher,semesterName: semesterName, month: monthLocalNameExam.toString(), monthId: monthLocalExam.toString(),),
                                  ),
                                );
                              }
                              else{
                                BlocProvider.of<MonthlyResultBloc>(context).add(ListStudentYearResultEvent(idClass:classIdTeacher));
                                  Navigator.push(context,
                                    PageTransition( type:PageTransitionType.bottomToTop,
                                      child: ViewYearResultScreen(classname: classIdName,),
                                    ),
                                  );
                              }
                            });
                          });
                        });
                      }
                      else{
                        setState(() {
                          calculateSuccess = 2;
                          statusCalculate = "FAIL".tr();
                            Future.delayed(const Duration(seconds: 2),(){
                            setState(() {
                              isLoadingCalculate = false;
                            });
                          });
                        });
                      }
                    },
                    child: Column(
                      children: [
                        //========== Button widget Change class==========
                        GestureDetector(
                          onTap: isInstructor == false ? null :() {
                            if(indexInStuctorLenght == inStuctorLenght){
                              activeMySchedule = false;
                                optionClassName = "${dataTeacherprofile!.instructorClass![0].className}";
                                classIdTeacher = dataTeacherprofile!.instructorClass![0].classId.toString();
                                classIdName = dataTeacherprofile!.instructorClass![0].className.toString();
                                BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: semesterName,type: typeSelection,month: monthLocalExam.toString(),idClass: dataTeacherprofile!.instructorClass![0].classId.toString()));
                                _isLast = false;
                                indexInStuctorLenght = 0;
                            }
                            else{
                              setState(() {
                                activeMySchedule = false;
                                optionClassName = "${dataTeacherprofile!.instructorClass![indexInStuctorLenght].className}";
                                classIdTeacher = dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString();
                                classIdName = dataTeacherprofile!.instructorClass![indexInStuctorLenght].className.toString();
                                BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: semesterName,type: typeSelection,month: monthLocalExam.toString(),idClass: dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString()));
                                _isLast = false;
                                indexInStuctorLenght = indexInStuctorLenght+1;
                              });
                            }
                          },
                          child: Container(
                            margin: const EdgeInsets.only(left: 28),
                            child: Row(
                              children: [
                                Container(
                                  height: 30,width: 30,
                                  decoration: BoxDecoration(
                                    color: Colorconstand.primaryColor,
                                    borderRadius: BorderRadius.circular(8)
                                  ),
                                  child: SvgPicture.asset(ImageAssets.switch_icon),
                                ),
                                const SizedBox(width: 12,),
                                Expanded(
                                  child: Row(
                                    children: [
                                      Text("${"SCORING".tr()}៖ ",style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colorconstand.neutralWhite)),
                                      Expanded(child: Text(optionClassName,style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite))),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 15,),
                        Expanded(
                          child: Container(
                            decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0),
                                  ),
                                  color: Colorconstand.neutralWhite),
                            child: BlocConsumer<ListMonthSemesterBloc, ListMonthSemesterState>(
                              listener: (context, state) {
                                if (state is ListMonthSemesterLoaded){
                                  var data = state.monthAndSemesterList!.data;
                                  if(firstLoadProfile == false){
                                    BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
                                  }
                                  setState(() {
                                    firstLoadProfile = true;
                                  });
                                  for (var i = 0; i < data!.length; i++) {
                                    if (data[i].displayMonth == monthLocalNameExam) {
                                      setState(() {
                                        activeIndexMonth = i;
                                        semesterName = data[i].semester.toString();
                                        semesterNameFristLoad = data[i].semester.toString();
                                      });
                                    }
                                  }
                                }
                              },
                              builder: (context, state) {
                                if (state is ListMonthSemesterLoading) {
                                  return Container(
                                    decoration: const BoxDecoration(
                                      color: Color(0xFFF1F9FF),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(12),
                                        topRight: Radius.circular(12),
                                      ),
                                    ),
                                    child: Column(
                                      children: [
                                        Container(
                                          color: const Color(0x622195F3),
                                          height: 165,
                                        ),
                                        Expanded(
                                          child: ListView.builder(
                                            padding: const EdgeInsets.all(0),
                                            shrinkWrap: true,
                                            itemBuilder: (BuildContext contex, index) {
                                              return const ShimmerTimeTable();
                                            },
                                            itemCount: 5,
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                } else if (state is ListMonthSemesterLoaded) {
                                  var datamonth = state.monthAndSemesterList!.data;
                                  return Container(
                                    child: Column(
                                      children: [
                                        Container(
                                          width: double.maxFinite,
                                          decoration: BoxDecoration(
                                              borderRadius:BorderRadius.circular(8.0),
                                              color: Colorconstand.neutralSecondBackground,
                                          ),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                          //===========  Button Show List Month ===========================
                                              GestureDetector(
                                                onTap:() {
                                                  setState(() {
                                                      if(showListMonthExam==true){
                                                          showListMonthExam =false;
                                                      }
                                                      else{
                                                        showListMonthExam = true;
                                                      }
                                                  });
                                                },
                                                child:Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  height: 45,
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child: Row(
                                                          mainAxisAlignment:MainAxisAlignment.center,
                                                          children: [
                                                            Text(
                                                              "${monthLocalNameExam=="YEAR"?"":"${"IN_PUT_SCORE_FOR".tr()} ${monthLocalNameExam=="FIRST_SEMESTER"||monthLocalNameExam=="SECOND_SEMESTER"?"":"MONTH".tr()}"} ${"$monthLocalNameExam".tr()} ",
                                                              style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),
                                                              ),
                                                                Text("${datamonth![activeIndexMonth].year}",
                                                                  style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor)
                                                                ),
                                                                const SizedBox(
                                                                  width: 8
                                                                ),
                                                                showListMonthExam==true?Container(): Icon(
                                                                  showListMonthExam ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                  size: 24,
                                                                  color: Colorconstand.primaryColor
                                                                ),
                                                              ],
                                                            ),
                                                        ),
                                                      ],
                                                    ),
                                                    ),
                                              ),
                                          //=========== End Button Show List Month ===========================
                                            ],
                                          ),
                                        ),
                                      
                                      Expanded(
                                        child: Stack(
                                          children: [
                                            Column(
                                              children: [
                                                Expanded(
                                                  child:displayCalculateScoreYear == true?  
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      Container(
                                                        alignment: Alignment.center,
                                                        child: Container(
                                                          margin:const EdgeInsets.symmetric(horizontal: 55,vertical: 25),
                                                          width:MediaQuery.of(context).size.width,
                                                          height:55,
                                                          decoration: const BoxDecoration(
                                                            borderRadius: BorderRadius.all(Radius.circular(75)),
                                                          color: Colorconstand.primaryColor,
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: Colors.grey,
                                                                spreadRadius: 3,
                                                                blurRadius: 10,
                                                                offset: Offset(0, 3), // changes position of shadow
                                                              ),
                                                            ],
                                                          ),
                                                          child: MaterialButton(
                                                            onPressed: (){
                                                                BlocProvider.of<CalculateScoreBloc>(context).add(CalculateExam(
                                                                  idClass: classIdTeacher,
                                                                  month: monthLocalExam.toString(),
                                                                  type: typeSelection,
                                                                  semester: "SECOND_SEMESTER",
                                                                ));
                                                            },
                                                            height:150,
                                                            shape: const RoundedRectangleBorder( borderRadius: BorderRadius.all(Radius.circular(75))),
                                                              color: Colorconstand.primaryColor,
                                                              child: Text( "CALCULATE_SCORE_YEARLY".tr(), style:ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite,),textAlign: TextAlign.center,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        alignment: Alignment.center,
                                                        child: Container(
                                                            margin:const EdgeInsets.symmetric(horizontal: 55,vertical: 25),
                                                          width:MediaQuery.of(context).size.width,
                                                          height:55,
                                                          decoration: const BoxDecoration(
                                                            borderRadius: BorderRadius.all(Radius.circular(75)),
                                                          color: Colorconstand.primaryColor,
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: Colors.grey,
                                                                spreadRadius: 3,
                                                                blurRadius: 10,
                                                                offset: Offset(0, 3), // changes position of shadow
                                                              ),
                                                            ],
                                                          ),
                                                          child: MaterialButton(
                                                            onPressed: (){
                                                                BlocProvider.of<MonthlyResultBloc>(context).add(ListStudentYearResultEvent(idClass:classIdTeacher));
                                                                Navigator.push(context,
                                                                  PageTransition( type:PageTransitionType.bottomToTop,
                                                                    child: ViewYearResultScreen(classname: classIdName,),
                                                                  ),
                                                                );
                                                            },
                                                            height:150,
                                                            shape: const RoundedRectangleBorder( borderRadius: BorderRadius.all(Radius.circular(75))),
                                                              color: Colorconstand.mainColorSecondary,
                                                              child: Text( "${"CHECK".tr()} ${"RESULTS".tr()}", style:ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite,),textAlign: TextAlign.center,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ) : NotificationListener(
                                                    child: SingleChildScrollView(
                                                      controller: scrollController,
                                                      child: BlocConsumer<ProfileUserBloc, ProfileUserState>(
                                                        listener: (context, state) {
                                                          if(state is GetUserProfileLoaded){
                                                            dataTeacherprofile = state.profileModel!.data;
                                                            inStuctorLenght = dataTeacherprofile!.instructorClass!.length;
                                                            if(dataTeacherprofile!.instructorClass!.isNotEmpty){
                                                              BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: semesterName,type: typeSelection.toString(),month: monthLocalExam.toString(),idClass: dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString()));
                                                              setState(() {
                                                                isInstructor = true;
                                                                optionClassName = dataTeacherprofile!.instructorClass![indexInStuctorLenght].className.toString();
                                                                classIdTeacher = dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString();
                                                                classIdName = dataTeacherprofile!.instructorClass![indexInStuctorLenght].className.toString();
                                                                _isLast = false;
                                                                indexInStuctorLenght = indexInStuctorLenght+1;
                                                              });
                                                            }
                                                          }
                                                        },
                                                        builder: (context, state) {
                                                          if(state is GetUserProfileLoading){
                                                            return Container(
                                                            decoration: const BoxDecoration(
                                                              color: Color(0xFFF1F9FF),
                                                              borderRadius: BorderRadius.only(
                                                                topLeft: Radius.circular(12),
                                                                topRight: Radius.circular(12),
                                                              ),
                                                            ),
                                                              child: Column(
                                                                children: [
                                                                  ListView.builder(
                                                                    padding: const EdgeInsets.all(0),
                                                                    shrinkWrap: true,
                                                                    itemBuilder: (BuildContext contex, index) {
                                                                    return const ShimmerTimeTable();
                                                                    },
                                                                    itemCount: 5,
                                                                  ),
                                                                ],
                                                              ),
                                                            );
                                                          }
                                                          if(state is GetUserProfileLoaded){
                                                            return BlocConsumer<ListSubjectEnterScoreBloc, ListSubjectEnterScoreState>(
                                                            listener: (context, state) {
                                                              if(state is ListAllSubjectPrimaryLoaded){
                                                                var data = state.listAllSubjectPrimaryModel!.dataSubjectPrimaryEnterScore!.subjectsGroupTag;
                                                                setState(() {
                                                                  if(data!.isNotEmpty){
                                                                    for(int i =0 ;i<= data.length-1;i++){
                                                                      if(subjectApprove.isEmpty){
                                                                        setState(() {
                                                                          subjectApprove = data[i].teachingSubjects!.where((element){
                                                                            return element.isApprove == 1;
                                                                          }).toList();
                                                                        });
                                                                      }
                                                                    }
                                                                  }
                                                                });
                                                              }
                                                            },
                                                            builder: (context, state) {
                                                              if(state is ListAllSubjectPrimaryLoading){
                                                                return Container(
                                                                  decoration: const BoxDecoration(
                                                                    color: Color(0xFFF1F9FF),
                                                                    borderRadius: BorderRadius.only(
                                                                      topLeft: Radius.circular(12),
                                                                      topRight: Radius.circular(12),
                                                                    ),
                                                                  ),
                                                                    child: Column(
                                                                      children: [
                                                                        ListView.builder(
                                                                          padding: const EdgeInsets.all(0),
                                                                          shrinkWrap: true,
                                                                          itemBuilder: (BuildContext contex, index) {
                                                                          return const ShimmerTimeTable();
                                                                          },
                                                                          itemCount: 5,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  );
                                                                }
                                                                else if( state is ListAllSubjectPrimaryLoaded){
                                                                  var dataSubjectEnterScore = state.listAllSubjectPrimaryModel!.dataSubjectPrimaryEnterScore;
                                                                  return dataSubjectEnterScore == null || dataSubjectEnterScore.subjectsGroupTag!.isEmpty?
                                                                        ScheduleEmptyWidget(
                                                                          title: "SCHEDULE_NOT_SET".tr(),
                                                                          subTitle: "SCHEDULE_NOT_SET_DES".tr(),
                                                                        )
                                                                    : Column(
                                                                      children: [
                                                                        CardEnterScorePrimary(
                                                                          activeIndexMonth: activeIndexMonth,
                                                                          dataSubjectPrimaryEnterScore: dataSubjectEnterScore,
                                                                          semester: semesterName,
                                                                          month: monthLocalExam!,
                                                                          activeMySchedule: activeMySchedule, 
                                                                          typeSemesterOrMonth: int.parse(typeSelection),
                                                                        ),
                                                                        Container(height:activeMySchedule == false && subjectApprove.isNotEmpty && _isLast == true?50:0,)
                                                                      ],
                                                                    );
                                                                }
                                                                                
                                                            //=========== End Class Exam =========================
                                                                else {
                                                                  return Center(
                                                                    child: EmptyWidget(
                                                                      title: "WE_DETECT".tr(),
                                                                      subtitle: "WE_DETECT_DES".tr(),
                                                                    ),
                                                                  );
                                                                }
                                                              },
                                                            );
                                                          }
                                                          else{
                                                            return Center(
                                                              child: EmptyWidget(
                                                                title: "WE_DETECT".tr(),
                                                                subtitle: "WE_DETECT_DES".tr(),
                                                              ),
                                                            );
                                                          }
                                                        },
                                                      ),
                                                    ),
                                                    onNotification: (t) {
                                                      if (t is ScrollEndNotification) {
                                                        if (scrollController.position.pixels > 0 && scrollController.position.atEdge) {
                                                          setState(() {
                                                            _isLast = true;
                                                          });
                                                        }
                                                        else{
                                                          setState(() {
                                                            _isLast = false;
                                                          });
                                                        }
                                                        return true;
                                                      } else {
                                                        return false;
                                                      }
                                                    }
                                                  ),
                                                ),
                                              ],
                                            ),
                                            /// 
                                            /// =============== Baground over when drop month =============
                                            showListMonthExam == false
                                            ? Container()
                                            : Positioned(
                                                bottom: 0,
                                                left: 0,
                                                right: 0,
                                                top: 0,
                                                child:
                                                    GestureDetector(
                                                  onTap: (() {
                                                    setState(
                                                        () {
                                                      showListMonthExam =
                                                          false;
                                                    });
                                                  }),
                                                  child:
                                                      Container(
                                                    color: const Color(
                                                        0x7B9C9595),
                                                  ),
                                                ),
                                              ),
                                      /// =============== Baground over when drop month =============
                                      /// 
                                      /// =============== Widget List Change Month =============
                                            AnimatedPositioned(
                                                top: showListMonthExam ==
                                                        false
                                                    ? wieght > 800?-700:-270
                                                    : 0,
                                                left: 0,
                                                right: 0,
                                                duration: const Duration(milliseconds:300),
                                                child:Container(
                                                  color: Colorconstand.neutralWhite,
                                                  child:
                                                      GestureDetector(
                                                    onTap:() {
                                                      setState(() {
                                                        showListMonthExam = false;
                                                      });
                                                    },
                                                    child:Container(
                                                      margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                      child: GridView.builder(
                                                        shrinkWrap: true,
                                                        physics:const NeverScrollableScrollPhysics(),
                                                        padding:const EdgeInsets.all(.0),
                                                          itemCount: datamonth.length,
                                                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5),
                                                          itemBuilder: (BuildContext context, int index) {
                                                            return activeMySchedule==true&&datamonth[index].displayMonth=="YEAR"?Container()
                                                            :GestureDetector(
                                                              onTap: (() {
                                                                setState(() {
                                                                  _isLast = false;
                                                                  monthLocalNameExam = datamonth[index].displayMonth;
                                                                  showListMonthExam = false;
                                                                  monthLocalExam =  int.parse(datamonth[index].month.toString());
                                                                  semesterName = datamonth[index].semester.toString();
                                                                  if(datamonth[index].displayMonth=="YEAR"){
                                                                    setState(() {
                                                                        displayCalculateScoreYear = true;
                                                                        typeSelection = "3";
                                                                    });
                                                                  }
                                                                  else if(datamonth[index].displayMonth=="FIRST_SEMESTER"||datamonth[index].displayMonth=="SECOND_SEMESTER"){
                                                                    setState(() {
                                                                      displayCalculateScoreYear = false;
                                                                      activeIndexMonth = index - 2;
                                                                      typeSelection = "2";
                                                                    });
                                                                      BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: datamonth[index].semester,type: "2",month: datamonth[index].month.toString(),idClass: classIdTeacher));                                                                          
                                                                  }
                                                                  else{
                                                                    setState(() {
                                                                      displayCalculateScoreYear = false;
                                                                      activeIndexMonth = index;
                                                                      typeSelection = "1";
                                                                    });
                                                                      BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: datamonth[index].semester,type: "1",month: datamonth[index].month.toString(),idClass: classIdTeacher));                                                                          
                                                                  }
                                                                });
                                                              }),
                                                              child: Container(
                                                                margin:const EdgeInsets.all(8),
                                                                decoration:BoxDecoration(
                                                                  borderRadius: BorderRadius.circular(12),
                                                                  color: monthLocalNameExam == datamonth[index].displayMonth? Colorconstand.primaryColor:Colors.transparent,
                                                                ),
                                                                child:  Center(child: Text("${datamonth[index].displayMonth}".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthLocalNameExam == datamonth[index].displayMonth?Colorconstand.neutralWhite
                                                                : datamonth[index].displayMonth =="FIRST_SEMESTER"||datamonth[index].displayMonth=="YEAR"||datamonth[index].displayMonth=="SECOND_SEMESTER"? Colorconstand.alertsDecline :Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                    )
                                                  ),
                                                ),
                                              ),
                                          /// =============== End Widget List Change Month =============
                                              
                                        //======================= Icon Widget redirect to active date =======================
                                          AnimatedPositioned(
                                            bottom: monthLocalNameExam == monthLocalNameExamFirstLoad || showListMonthExam==true?-60:subjectApprove.isNotEmpty && activeMySchedule == false && _isLast == true?80:30,right: 30,
                                            duration: const Duration(milliseconds:300),
                                            child: Container(
                                              height: 55,width: 55,
                                              decoration: BoxDecoration(
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colorconstand.neutralGrey.withOpacity(0.7),
                                                    spreadRadius: 4,
                                                    blurRadius: 4,
                                                    offset:const Offset(0, 3), // changes position of shadow
                                                  ),
                                                ],
                                                color: Colorconstand.neutralWhite,
                                                shape: BoxShape.circle
                                              ),
                                              child: MaterialButton(
                                                elevation: 8,
                                                onPressed: (){
                                                  setState(() {
                                                    typeSelection = "1";
                                                    displayCalculateScoreYear = false;
                                                    monthLocalNameExam = monthLocalNameExamFirstLoad;
                                                    BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: semesterNameFristLoad,type: "1",month: monthLocalExamFirstLoad.toString(),idClass: classIdTeacher));
                                                  });
                                                },
                                                shape:const CircleBorder(),
                                                child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                              ),
                                            ),
                                          ),
                                                  
                                    //======================= End Icon Widget redirect to active date =======================
                                          ],
                                        ),
                                      
                                      ),
                                    ],
                                  ));
                                } else if(state is ListMonthSemesterError){
                                  return Center(
                                    child: EmptyWidget(
                                      title: "WE_DETECT".tr(),
                                      subtitle: "WE_DETECT_DES".tr(),
                                    ),
                                  );
                                }
                                else {
                                  return const Center();
                                }
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    // ====================  Button Calculate Score ===========================
        AnimatedPositioned(
          duration:const Duration(milliseconds: 400),
              //bottom: activeMySchedule == false && subjectApprove.isNotEmpty && _isLast == true? 30:0,left: activeMySchedule == false && subjectApprove.isNotEmpty && _isLast == true? MediaQuery.of(context).size.width - ((MediaQuery.of(context).size.width/2)+50):MediaQuery.of(context).size.width/2.2,right: _isLast == true? MediaQuery.of(context).size.width - ((MediaQuery.of(context).size.width/2)+50):MediaQuery.of(context).size.width/2.2,
              bottom: 0,left: 0,right: 0,
              child: AnimatedContainer(
                margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 8),
                alignment: Alignment.center,
                width:subjectApprove.isNotEmpty && activeMySchedule == false && _isLast == true?60:0,
                height:subjectApprove.isNotEmpty && activeMySchedule == false && _isLast == true?60:0,
                duration:const Duration(milliseconds: 300),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                 color: Colorconstand.primaryColor,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: MaterialButton(
                        shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(15),bottomLeft: Radius.circular(15)),
                        ),
                        onPressed: (){
                            BlocProvider.of<CalculateScoreBloc>(context).add(CalculateExam(
                              idClass: classIdTeacher,
                              month: monthLocalExam.toString(),
                              type: typeSelection,
                              semester: semesterName,
                            ));
                          PostActionTeacherApi().actionTeacherApi(feature: "SCORE" ,keyFeature: "CALCULATE_SCORE");
                        },
                        height: activeMySchedule == false && subjectApprove.isNotEmpty && _isLast == true?100:0,
                          color: Colorconstand.primaryColor,
                          child: Text( "CALCULATE_SCORE".tr(), style:ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Expanded(
                      child: MaterialButton(
                        shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(topRight: Radius.circular(15),bottomRight: Radius.circular(15)),
                        ),
                        onPressed: (){
                            if(typeSelection == "2" || typeSelection == "1"){
                            BlocProvider.of<ResultPrimarySchoolBloc>(context).add(ListStudentResultEvent(semester: semesterName,idClass:classIdTeacher,month: monthLocalExam.toString(),type: typeSelection));
                              Navigator.push(context,
                                PageTransition( type:PageTransitionType.bottomToTop,
                                  child: ViewResultPrimarySchoolScreen(
                                    type: typeSelection,
                                    month: monthLocalNameExam.toString(),
                                    semesterName: semesterName,
                                      classId: classIdTeacher,
                                      monthId: monthLocalExam.toString(),
                                  ),
                                ),
                              );
                          }
                          else{
                            
                          }
                        },
                        height: activeMySchedule == false && subjectApprove.isNotEmpty && _isLast == true?100:0,
                          color: Colorconstand.mainColorSecondary,
                          child: Text("${"CHECK".tr()} ${"RESULTS".tr()}", style:ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
        // ====================  Button Calculate Score =================

        //================= background when pop change class ============
        isCheckClass==true ? AnimatedPositioned(
          duration:const Duration(milliseconds: 4000),
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        child: GestureDetector(
          onTap: () {
          setState(() {
              isCheckClass = false;
              _arrowAnimationController!.isCompleted
            ? _arrowAnimationController!.reverse()
            : _arrowAnimationController!.forward();
          });
        },child: Container(color: Colors.transparent,width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,),
        )):Positioned(top: 0,child: Container(height: 0,)),
        //================= baground when pop change class ============

        // //================ Popup Change Class ========================
        //     Positioned(
        //       top: 100,left: 12,
        //       child: BlocConsumer<ProfileUserBloc, ProfileUserState>(
        //             listener: (context, state) {
        //               if(state is GetUserProfileLoaded){
        //                 dataTeacherprofile = state.profileModel!.data;
        //                 inStuctorLenght = dataTeacherprofile!.instructorClass!.length;
        //                 if(dataTeacherprofile!.instructorClass!.isNotEmpty){
        //                   setState(() {
        //                     isInstructor = true;
        //                     optionClassName = dataTeacherprofile!.instructorClass![indexInStuctorLenght].className.toString();
        //                     classIdTeacher = dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString();
        //                     BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: semesterName,type: typeSelection.toString(),month: monthLocalExam.toString(),idClass: dataTeacherprofile!.instructorClass![indexInStuctorLenght].classId.toString()));
        //                     _isLast = false;
        //                     indexInStuctorLenght = indexInStuctorLenght+1;
        //                   });
        //                 }
        //               }
        //             },
        //             builder: (context, state) {
        //               return Container(height: 0,);
        //             },
        //           ),
              
        //     ),
        // //================ End Popup Change Class ========================

        //================ loading when calculate ========================
          isLoadingCalculate == true
            ? Positioned(
                child: Container(
                  color:
                      Colorconstand.primaryColor.withOpacity(.6),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "CALCULATE_SCORE".tr(),
                        style: ThemsConstands
                            .headline_2_semibold_24
                            .copyWith(color: Colors.white),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "IN_PROGRESS_CALCULATE_SCORE".tr(),
                        textAlign: TextAlign.center,
                        style: ThemsConstands.headline_5_medium_16
                            .copyWith(color: Colors.white),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      calculateSuccess ==1?const Icon(Icons.check_circle_outline_sharp,color: Colorconstand.alertsPositive,size: 65,)
                      :calculateSuccess==2? const Icon(Icons.error_outline_sharp,color: Colorconstand.alertsDecline,size: 65,):const CircularProgressIndicator(
                        color: Colors.white,
                      ),
                      Text(
                        statusCalculate,
                        style: ThemsConstands
                            .headline_4_medium_18
                            .copyWith(color: Colorconstand.neutralWhite),
                      ),
                    ],
                  ),
                ),
              )
            : Container(),
      //================ loading when calculate ========================

      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),

      //================ Internet offilne ========================
          ],
        ),
      ));

  }
}
