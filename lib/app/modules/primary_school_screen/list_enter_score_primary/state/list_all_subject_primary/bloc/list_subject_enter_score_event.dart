part of 'list_subject_enter_score_bloc.dart';

class ListSubjectEnterScoreEvent extends Equatable {
  const ListSubjectEnterScoreEvent();

  @override
  List<Object> get props => [];
}

class ListAllSubjectEnterScoreEvent extends ListSubjectEnterScoreEvent {
  final String? semester;
  final String? idClass;
  final String? month;
  final String?type;
 const ListAllSubjectEnterScoreEvent({required this.type,required this.semester,required this.idClass,required this.month});
}
