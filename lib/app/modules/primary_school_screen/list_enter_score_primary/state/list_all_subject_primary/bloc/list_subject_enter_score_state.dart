part of 'list_subject_enter_score_bloc.dart';

class ListSubjectEnterScoreState extends Equatable {
  const ListSubjectEnterScoreState();
  
  @override
  List<Object> get props => [];
}

class ListSubjectEnterScoreInitial extends ListSubjectEnterScoreState {}

class  ListAllSubjectPrimaryLoading extends ListSubjectEnterScoreState {}

class  ListAllSubjectPrimaryLoaded extends ListSubjectEnterScoreState {
  final ListSubjectEnterScorePrimaryModel? listAllSubjectPrimaryModel;
  const  ListAllSubjectPrimaryLoaded({required this.listAllSubjectPrimaryModel});
}

class  ListAllSubjectPrimaryError extends ListSubjectEnterScoreState {}
