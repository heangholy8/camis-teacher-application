import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/get_list_enter_score_primary_school_api/list_enter_score_primary_api.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/primary_school_model/list_subject_enter_score_primary_model.dart';
part 'list_subject_enter_score_event.dart';
part 'list_subject_enter_score_state.dart';

class ListSubjectEnterScoreBloc extends Bloc<ListSubjectEnterScoreEvent, ListSubjectEnterScoreState> {
  final GetPrimaryListSubjectApi listAllSubjectPrimaryApi;
  ListSubjectEnterScoreBloc({required this.listAllSubjectPrimaryApi}) : super(ListSubjectEnterScoreInitial()) {
    on<ListAllSubjectEnterScoreEvent>((event, emit) async {
     emit(ListAllSubjectPrimaryLoading());
      try {
        var dataAllSubjectEnterScorePrimary = await listAllSubjectPrimaryApi.getListSubjectEcnterScorePrimarySchoolApi(
          idClass: event.idClass,term: event.semester,month: event.month,type: event.type
        );
        emit(ListAllSubjectPrimaryLoaded(listAllSubjectPrimaryModel: dataAllSubjectEnterScorePrimary));
      } catch (e) {
        print(e.toString());
        emit(ListAllSubjectPrimaryError());
      }
    });
  }
}
