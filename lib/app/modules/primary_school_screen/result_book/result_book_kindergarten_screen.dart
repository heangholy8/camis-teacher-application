import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:khmer_fonts/khmer_fonts.dart';

class ResultBookKindergartenScreen extends StatefulWidget {
  const ResultBookKindergartenScreen({super.key});

  @override
  State<ResultBookKindergartenScreen> createState() => _ResultBookKindergartenScreenState();
}

class _ResultBookKindergartenScreenState extends State<ResultBookKindergartenScreen> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Container(),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        height: 800,
                        margin:const EdgeInsets.only(left: 12,right: 12),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              Container(
                                width: 700,
                                decoration: BoxDecoration(
                                  border: Border.all()
                                ),
                                child: Container(
                                  margin:const EdgeInsets.all(3),
                                  decoration: BoxDecoration(
                                    border: Border.all()
                                  ),
                                  child: Column(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      height: 40,
                                                      decoration:const BoxDecoration(
                                                        border: Border(bottom: BorderSide())
                                                      ),
                                                      child:const Row(
                                                        children: [
                                                          Expanded(child: Text("សប្តាហ៏ទី១",style: TextStyle(
                                                            fontFamily: KhmerFonts.moul,
                                                            fontSize: 20,
                                                            package: 'khmer_fonts',
                                                          ),textAlign: TextAlign.center,)),
                                                        ],
                                                      ),
                                                    ),
                                                    
                                                    Container(
                                                      margin: const EdgeInsets.only(top: 10),
                                                      child: const Text("កុមារបានអភិវឌ្ឍ",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 16,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                    bodyWeekLy(),
                                                    Expanded(
                                                      child: Container(),
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets.symmetric(vertical: 8),
                                                      child:const Text("ប័ណ្ណកុមារសុភាព",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 20,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const VerticalDivider(
                                                thickness: 1,
                                                width: 1,
                                                color: Colorconstand.lightBulma,
                                              ),
                                              Expanded(
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      height: 40,
                                                      decoration:const BoxDecoration(
                                                        border: Border(bottom: BorderSide())
                                                      ),
                                                      child:const Row(
                                                        children: [
                                                          Expanded(child: Text("សប្តាហ៏ទី២",style: TextStyle(
                                                            fontFamily: KhmerFonts.moul,
                                                            fontSize: 20,
                                                            package: 'khmer_fonts',
                                                          ),textAlign: TextAlign.center,)),
                                                        ],
                                                      ),
                                                    ),
                                                    
                                                    Container(
                                                      margin: const EdgeInsets.only(top: 10),
                                                      child: const Text("កុមារបានអភិវឌ្ឍ",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 16,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                    bodyWeekLy(),
                                                    Expanded(
                                                      child: Container(),
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets.symmetric(vertical: 8),
                                                      child:const Text("ប័ណ្ណកុមារសុភាព",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 20,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const Divider(
                                        thickness: 1,
                                        height: 1,
                                        color: Colorconstand.lightBulma,
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      height: 40,
                                                      decoration:const BoxDecoration(
                                                        border: Border(bottom: BorderSide())
                                                      ),
                                                      child:const Row(
                                                        children: [
                                                          Expanded(child: Text("សប្តាហ៏ទី៣",style: TextStyle(
                                                            fontFamily: KhmerFonts.moul,
                                                            fontSize: 20,
                                                            package: 'khmer_fonts',
                                                          ),textAlign: TextAlign.center,)),
                                                        ],
                                                      ),
                                                    ),
                                                    
                                                    Container(
                                                      margin: const EdgeInsets.only(top: 10),
                                                      child: const Text("កុមារបានអភិវឌ្ឍ",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 16,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                    bodyWeekLy(),
                                                    Expanded(
                                                      child: Container(),
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets.symmetric(vertical: 8),
                                                      child:const Text("ប័ណ្ណកុមារសុភាព",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 20,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const VerticalDivider(
                                                thickness: 1,
                                                width: 1,
                                                color: Colorconstand.lightBulma,
                                              ),
                                              Expanded(
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      height: 40,
                                                      decoration:const BoxDecoration(
                                                        border: Border(bottom: BorderSide())
                                                      ),
                                                      child:const Row(
                                                        children: [
                                                          Expanded(child: Text("សប្តាហ៏ទី៤",style: TextStyle(
                                                            fontFamily: KhmerFonts.moul,
                                                            fontSize: 20,
                                                            package: 'khmer_fonts',
                                                          ),textAlign: TextAlign.center,)),
                                                        ],
                                                      ),
                                                    ),
                                                    
                                                    Container(
                                                      margin: const EdgeInsets.only(top: 10),
                                                      child: const Text("កុមារបានអភិវឌ្ឍ",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 16,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                    bodyWeekLy(),
                                                    Expanded(
                                                      child: Container(),
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets.symmetric(vertical: 8),
                                                      child:const Text("ប័ណ្ណកុមារសុភាព",style: TextStyle(
                                                        fontFamily: KhmerFonts.moul,
                                                        fontSize: 20,
                                                        package: 'khmer_fonts',
                                                      ),textAlign: TextAlign.center,)
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(width: 12,),
                              Container(
                                width: 700,
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Expanded(
                                            child: Container(
                                              margin: const EdgeInsets.only(top: 10),
                                              alignment: Alignment.topCenter,
                                              child:const Row(
                                                children: [
                                                  Expanded(child: Text("ប័ណ្ណកុមារឈ្លាសវៃ",style: TextStyle(
                                                              fontFamily: KhmerFonts.moul,
                                                              fontSize: 20,
                                                              package: 'khmer_fonts',
                                                            ),textAlign: TextAlign.center,)),
                                                  Expanded(child: Text("យោបល់របស់គ្រូ",style: TextStyle(
                                                              fontFamily: KhmerFonts.moul,
                                                              fontSize: 20,
                                                              package: 'khmer_fonts',
                                                            ),textAlign: TextAlign.center,)),
                                                ],
                                              ),
                                            )
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            margin:const EdgeInsets.only(left: 12,right: 12),
                                            child: const Column(  
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Text("ថ្ងៃ ",style: ThemsConstands.headline_5_medium_16),
                                                        Text("ច័ន្ទ",style: ThemsConstands.headline_5_semibold_16,),
                                                      ],
                                                    ),
                                                    SizedBox(width: 8,),
                                                    Row(
                                                      children: [
                                                        Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                                        Text("ផលគុន",style: ThemsConstands.headline_5_semibold_16,),
                                                      ],
                                                    ),
                                                    SizedBox(width: 8,),
                                                    Row(
                                                      children: [
                                                        Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                                        Text("ខាល",style: ThemsConstands.headline_5_semibold_16,),
                                                      ],
                                                    ),
                                                    SizedBox(width: 8,),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Text("ព.ស.",style: ThemsConstands.headline_5_medium_16),
                                                        Text("២៥៦៨",style: ThemsConstands.headline_5_semibold_16,),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Text("ភ្នំពេញថ្ងៃទី ",style: ThemsConstands.headline_5_medium_16),
                                                        Text("18",style: ThemsConstands.headline_5_semibold_16,),
                                                      ],
                                                    ),
                                                    SizedBox(width: 8,),
                                                    Row(
                                                      children: [
                                                        Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                                        Text("វិច្ឆិកា",style: ThemsConstands.headline_5_semibold_16,),
                                                      ],
                                                    ),
                                                    SizedBox(width: 8,),
                                                    Row(
                                                      children: [
                                                        Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                                        Text("2024",style: ThemsConstands.headline_5_semibold_16,),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                Text("ឈ្មោះនិងហត្ថលេខា",style: ThemsConstands.headline_4_semibold_18,),
                                                SizedBox(height: 75,)
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all()
                                        ),
                                        child: Container(
                                          margin:const EdgeInsets.all(3),
                                          decoration: BoxDecoration(
                                            border: Border.all()
                                          ),
                                          child: Container(
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        height: 40,
                                                        decoration:const BoxDecoration(
                                                          border: Border(bottom: BorderSide())
                                                        ),
                                                        child:const Row(
                                                          children: [
                                                            Expanded(child: Text("យោបល់របស់មាតាបិតា",style: TextStyle(
                                                              fontFamily: KhmerFonts.moul,
                                                              fontSize: 20,
                                                              package: 'khmer_fonts',
                                                            ),textAlign: TextAlign.center,)),
                                                          ],
                                                        ),
                                                      ),
                                                      TextFormField(
                                                        cursorColor: Colors.black,
                                                        keyboardType: TextInputType.text,
                                                        maxLines: 7,
                                                        textAlign: TextAlign.left,
                                                        style: ThemsConstands.headline_5_medium_16,
                                                        decoration: InputDecoration(
                                                          hintText: "សរសេរនៅទីនេះ",
                                                          hintStyle: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsDisabled),
                                                          border: InputBorder.none,
                                                          focusedBorder: InputBorder.none,
                                                          enabledBorder: InputBorder.none,
                                                          errorBorder: InputBorder.none,
                                                          disabledBorder: InputBorder.none,
                                                          contentPadding:const EdgeInsets.only(left: 15, bottom: 15, top: 15, right: 15),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Container(),
                                                      ),
                                                      Container(
                                                        alignment: Alignment.center,
                                                        margin:const EdgeInsets.only(left: 12,right: 12),
                                                        child: const Column(  
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text("ថ្ងៃ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("ច័ន្ទ",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("ផលគុន",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("ខាល",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                  children: [
                                                                    Text("ព.ស.",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("២៥៦៨",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text("ភ្នំពេញថ្ងៃទី ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("18",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("វិច្ឆិកា",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("2024",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            Text("ហត្ថលេខា",style: ThemsConstands.headline_4_semibold_18,),
                                                            SizedBox(height: 75,)
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                const VerticalDivider(
                                                  thickness: 1,
                                                  width: 1,
                                                  color: Colorconstand.lightBulma,
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        height: 40,
                                                        decoration:const BoxDecoration(
                                                          border: Border(bottom: BorderSide())
                                                        ),
                                                        child:const Row(
                                                          children: [
                                                            Expanded(child: Text("យោបល់របស់មាតាបិតា",style: TextStyle(
                                                              fontFamily: KhmerFonts.moul,
                                                              fontSize: 20,
                                                              package: 'khmer_fonts',
                                                            ),textAlign: TextAlign.center,)),
                                                          ],
                                                        ),
                                                      ),
                                                      TextFormField(
                                                        cursorColor: Colors.black,
                                                        keyboardType: TextInputType.text,
                                                        maxLines: 7,
                                                        textAlign: TextAlign.left,
                                                        style: ThemsConstands.headline_5_medium_16,
                                                        decoration: InputDecoration(
                                                          hintText: "សរសេរនៅទីនេះ",
                                                          hintStyle: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsDisabled),
                                                          border: InputBorder.none,
                                                          focusedBorder: InputBorder.none,
                                                          enabledBorder: InputBorder.none,
                                                          errorBorder: InputBorder.none,
                                                          disabledBorder: InputBorder.none,
                                                          contentPadding:const EdgeInsets.only(left: 15, bottom: 15, top: 15, right: 15),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Container(),
                                                      ),
                                                      Container(
                                                        alignment: Alignment.center,
                                                        margin:const EdgeInsets.only(left: 12,right: 12),
                                                        child: const Column(  
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text("ថ្ងៃ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("ច័ន្ទ",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("ផលគុន",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("ខាល",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                  children: [
                                                                    Text("ព.ស.",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("២៥៦៨",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text("ភ្នំពេញថ្ងៃទី ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("18",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("វិច្ឆិកា",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                                SizedBox(width: 8,),
                                                                Row(
                                                                  children: [
                                                                    Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                                                    Text("2024",style: ThemsConstands.headline_5_semibold_16,),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            Text("ឈ្មោះនិងហត្ថលេខា",style: ThemsConstands.headline_4_semibold_18,),
                                                            SizedBox(height: 75,)
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ), 
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget bodyWeekLy(){
     return Column(
       children: [
        Container(
          margin:const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              const Text("ក)ចិត្តចលភាព៖ ",style: ThemsConstands.headline_5_medium_16),
              Expanded(
                child: Column(
                  children: [
                    TextFormField(
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      style: ThemsConstands.headline_5_medium_16,
                      decoration: InputDecoration(
                        hintText: "សរសេរនៅទីនេះ",
                        hintStyle: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsDisabled),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding:const EdgeInsets.only(left: 10, bottom: 0, top: 0, right: 0),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          margin:const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              const Text("ខ)បញា៖ ",style: ThemsConstands.headline_5_medium_16),
              Expanded(
                child: Column(
                  children: [
                    TextFormField(
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      style: ThemsConstands.headline_5_medium_16,
                      decoration: InputDecoration(
                        hintText: "សរសេរនៅទីនេះ",
                        hintStyle: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsDisabled),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding:const EdgeInsets.only(left: 10, bottom: 0, top: 0, right: 0),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          margin:const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              const Text("គ)-អារម្មណ៏៖ ",style: ThemsConstands.headline_5_medium_16),
              Expanded(
                child: Column(
                  children: [
                    TextFormField(
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      style: ThemsConstands.headline_5_medium_16,
                      decoration: InputDecoration(
                        hintText: "សរសេរនៅទីនេះ",
                        hintStyle: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsDisabled),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding:const EdgeInsets.only(left: 10, bottom: 0, top: 0, right: 0),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          margin:const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              const Text("   -សង្គម៖ ",style: ThemsConstands.headline_5_medium_16),
              Expanded(
                child: Column(
                  children: [
                    TextFormField(
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      style: ThemsConstands.headline_5_medium_16,
                      decoration: InputDecoration(
                        hintText: "សរសេរនៅទីនេះ",
                        hintStyle: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsDisabled),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding:const EdgeInsets.only(left: 10, bottom: 0, top: 0, right: 0),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
       ],
     );
  }
}