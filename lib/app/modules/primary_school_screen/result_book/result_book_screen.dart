import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class ResultBookStudent extends StatefulWidget {
  const ResultBookStudent({super.key});

  @override
  State<ResultBookStudent> createState() => _ResultBookStudentState();
}
class _ResultBookStudentState extends State<ResultBookStudent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Container(),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  Container(
                                    child: Text("AVERAGE".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.left,),
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Text("28.50",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.alertsDecline),textAlign: TextAlign.center,),
                                        DottedBorder(
                                          dashPattern: [1,1],
                                          padding:const EdgeInsets.only(bottom: 0),
                                          borderType: BorderType.Rect,
                                          strokeWidth: 0.5,
                                          color: Colorconstand.lightBlack,
                                          child: Container(
                                            height: 0,
                                          ),  
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(width: 12,),
                            Expanded(
                              child: Row(
                                children: [
                                  Container(
                                    child: Text("RANK".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.left,),
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Text("3/45",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.alertsDecline),textAlign: TextAlign.center,),
                                        DottedBorder(
                                          dashPattern: [1,1],
                                          padding:const EdgeInsets.only(bottom: 0),
                                          borderType: BorderType.Rect,
                                          strokeWidth: 0.5,
                                          color: Colorconstand.lightBlack,
                                          child: Container(
                                            height: 0,
                                          ),  
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.only(left: 12,right: 12,top: 18),
                        child:const Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: SizedBox(),
                            ),
                            SizedBox(width: 8,),
                            Expanded(
                              child: Center(
                                child: Text("ល្អ",style: ThemsConstands.headline_5_semibold_16,),
                              ),
                            ),
                            SizedBox(width: 8,),
                            Expanded(
                              child: Center(
                                child: Text("ល្អបង្គួរ",style: ThemsConstands.headline_5_semibold_16,),
                              ),
                            ),
                            SizedBox(width: 8,),
                            Expanded(
                              child: Center(
                                child: Text("មធ្យម",style: ThemsConstands.headline_5_semibold_16,),
                              ),
                            ),
                            SizedBox(width: 8,),
                            Expanded(
                              child: Center(
                                child: Text("ខ្សោយ",style: ThemsConstands.headline_5_semibold_16,),
                              ),
                            ),
                          ],
                        ),
                      ),
                      ListView.separated(
                        padding:const EdgeInsets.symmetric(horizontal: 12,vertical: 18),
                        itemCount: 14,
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        separatorBuilder: (context, index) {
                          return const SizedBox(height: 16,);
                        },
                        itemBuilder: (context, index) {
                          return Container(
                              child: Row(
                                children: [
                                  const Expanded(
                                    flex: 3,
                                    child: Text("រៀនអាន នឹងមេសូត្រ ដើម្បីអនាគត់",style: ThemsConstands.headline_4_semibold_18,),
                                  ),
                                  const SizedBox(width: 8,),
                                  Expanded(
                                    child: Center(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Colorconstand.lightTrunks)
                                        ),
                                        height: 25,
                                        width:25,
                                        child: const Icon(Icons.check,size: 22,color: Colorconstand.primaryColor,),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 8,),
                                  Expanded(
                                    child: Center(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Colorconstand.lightTrunks)
                                        ),
                                        height: 25,
                                        width:25,
                                        //child: const Icon(Icons.check,size: 22,color: Colorconstand.primaryColor,),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 8,),
                                  Expanded(
                                    child: Center(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Colorconstand.lightTrunks)
                                        ),
                                        height: 25,
                                        width:25,
                                       // child: const Icon(Icons.check,size: 22,color: Colorconstand.primaryColor,),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 8,),
                                  Expanded(
                                    child: Center(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Colorconstand.lightTrunks)
                                        ),
                                        height: 25,
                                        width:25,
                                        //child: const Icon(Icons.check,size: 22,color: Colorconstand.primaryColor,),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                          );
                        },
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  Container(
                                    child: Text("PERMISSION".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.left,),
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Text("2",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.alertsDecline),textAlign: TextAlign.center,),
                                        DottedBorder(
                                          dashPattern: [1,1],
                                          padding:const EdgeInsets.only(bottom: 0),
                                          borderType: BorderType.Rect,
                                          strokeWidth: 0.5,
                                          color: Colorconstand.lightBlack,
                                          child: Container(
                                            height: 0,
                                          ),  
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Text("ដង".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.left,),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(width: 22,),
                            Expanded(
                              child: Row(
                                children: [
                                  Container(
                                    child: Text("ABSENT".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.left,),
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Text("3",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.alertsDecline),textAlign: TextAlign.center,),
                                        DottedBorder(
                                          dashPattern: [1,1],
                                          padding:const EdgeInsets.only(bottom: 0),
                                          borderType: BorderType.Rect,
                                          strokeWidth: 0.5,
                                          color: Colorconstand.lightBlack,
                                          child: Container(
                                            height: 0,
                                          ),  
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Text("ដង".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.left,),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("សម្គាល់ ៖ ",style: ThemsConstands.headline_5_semibold_16,),
                            Expanded(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Text("${"និន្ទេសល្អចាប់ពី: "} ${"៨,០០"}",style: ThemsConstands.headline_5_semibold_16,),
                                      Icon(Icons.arrow_outward_rounded,size: 18,),
                                      Text("- ${"ល្អបង្គួរ: "} ${"៦,៥០"}",style: ThemsConstands.headline_5_semibold_16,),
                                      Icon(Icons.arrow_forward_rounded,size: 18,),
                                      Text("៧,៥៩",style: ThemsConstands.headline_5_semibold_16,),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text("${"មធ្យម: "} ${"៥,០០"}",style: ThemsConstands.headline_5_semibold_16,),
                                      Icon(Icons.arrow_forward_rounded,size: 18,),
                                      Text("៦,៤៩",style: ThemsConstands.headline_5_semibold_16,),
                                      Text("  ${"ខ្សោយ: "} ${"៥,០០"}",style: ThemsConstands.headline_5_semibold_16,),
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        margin:const EdgeInsets.only(left: 12,right: 12),
                        child: const Column(  
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Row(
                                  children: [
                                    Text("ថ្ងៃ ",style: ThemsConstands.headline_5_medium_16),
                                    Text("ច័ន្ទ",style: ThemsConstands.headline_5_semibold_16,),
                                  ],
                                ),
                                SizedBox(width: 8,),
                                Row(
                                  children: [
                                    Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                    Text("ផលគុន",style: ThemsConstands.headline_5_semibold_16,),
                                  ],
                                ),
                                SizedBox(width: 8,),
                                Row(
                                  children: [
                                    Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                    Text("ខាល",style: ThemsConstands.headline_5_semibold_16,),
                                  ],
                                ),
                                SizedBox(width: 8,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text("ព.ស.",style: ThemsConstands.headline_5_medium_16),
                                    Text("២៥៦៨",style: ThemsConstands.headline_5_semibold_16,),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Row(
                                  children: [
                                    Text("ភ្នំពេញថ្ងៃទី ",style: ThemsConstands.headline_5_medium_16),
                                    Text("18",style: ThemsConstands.headline_5_semibold_16,),
                                  ],
                                ),
                                SizedBox(width: 8,),
                                Row(
                                  children: [
                                    Text("ខែ ",style: ThemsConstands.headline_5_medium_16),
                                    Text("វិច្ឆិកា",style: ThemsConstands.headline_5_semibold_16,),
                                  ],
                                ),
                                SizedBox(width: 8,),
                                Row(
                                  children: [
                                    Text("ឆ្នាំ ",style: ThemsConstands.headline_5_medium_16),
                                    Text("2024",style: ThemsConstands.headline_5_semibold_16,),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 12,),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Text("បានឃើញ\nនាយកសាលា",style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Column(
                                children: [
                                  const Text("មូលវិចាររបស់គ្រូប្រចាំថ្នាក់",style: ThemsConstands.headline_5_semibold_16),
                                  TextFormField(
                                    cursorColor: Colors.black,
                                    keyboardType: TextInputType.text,
                                    maxLines: 4,
                                    textAlign: TextAlign.center,
                                    style: ThemsConstands.headline_5_medium_16,
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      contentPadding:EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 12),
                        child: const Column(
                          children: [
                            Text("មតិរបស់មាតាបិតាសិស្ស",style: ThemsConstands.headline_5_semibold_16),
                            SizedBox(height: 6,),
                            Text("ខ្ញុំជាមាតាបិតាសិស្ស សូមអរគុណលោកគ្រូដែរបានជួយបង្ហាត់បង្រៀនកូនរបស់យើងខ្ញុំ",style: ThemsConstands.headline_5_medium_16,textAlign: TextAlign.center,),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}