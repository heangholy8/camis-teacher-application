import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../models/primary_school_model/list_subject_enter_score_primary_model.dart';
import '../../attendance_schedule_screen/state_management/set_exam/bloc/set_exam_bloc.dart';
import '../../grading_screen/bloc/grading_bloc.dart';
import '../../grading_screen/bloc/view_image_bloc.dart';
import '../../grading_screen/view/grading_screen.dart';
import '../../grading_screen/view/result_screen.dart';
import '../list_enter_score_primary/state/list_all_subject_primary/bloc/list_subject_enter_score_bloc.dart';
import 'card_enter_score_primary_widget.dart';

class CardEnterScorePrimary extends StatefulWidget {
  final DataSubjectPrimaryEnterScore dataSubjectPrimaryEnterScore;
  final int activeIndexMonth ;
  String semester;
  bool activeMySchedule;
  int month;
  int typeSemesterOrMonth;
   CardEnterScorePrimary({ super.key,required this.typeSemesterOrMonth, required this.dataSubjectPrimaryEnterScore, required this.activeIndexMonth,required this.activeMySchedule,required this.month,required this.semester});

  @override
  State<CardEnterScorePrimary> createState() => _CardEnterScorePrimaryState();
}

class _CardEnterScorePrimaryState extends State<CardEnterScorePrimary> with TickerProviderStateMixin {
  String classId = "";
  String subjectId = "";
  int? isInstructor;
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return BlocListener<SetExamBloc, SetExamState>(
      listener: (context, state) {
        if(state is SetExamLoading){
          setState(() {
            
          });
        }
        else if(state is SetNoExamLoaded){
            BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                idClass:  classId.toString(), 
                subjectId: subjectId.toString(), 
                type: widget.typeSemesterOrMonth.toString(), 
                month:  widget.month.toString(), 
                semester:  widget.semester.toString(),
                examDate: ""),
            );
            Navigator.push(context, 
                MaterialPageRoute(builder: (context) => GradingScreen(
                isPrimary: true,
                isInstructor: isInstructor == 0 ? false:true,
                myclass: widget.activeMySchedule,
                redirectFormScreen: 1,
                ),
              ),
            );
            BlocProvider.of<ListSubjectEnterScoreBloc>(context).add(ListAllSubjectEnterScoreEvent(semester: widget.semester,type: widget.typeSemesterOrMonth.toString(),month: widget.month.toString(),idClass: classId.toString()));
        }
        else{
          setState(() {
            
          });
        }
      },
      child: Container(
          margin:const EdgeInsets.only(bottom: 25,top: 12),
          child: ListView.builder(
            padding: const EdgeInsets.all(0),
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.dataSubjectPrimaryEnterScore.subjectsGroupTag!.length,
            itemBuilder: (context, indexSubjectGrup) {
              final dataGroup = widget.dataSubjectPrimaryEnterScore.subjectsGroupTag![indexSubjectGrup];
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin:const EdgeInsets.symmetric(horizontal: 18),
                    padding:const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color:  indexSubjectGrup == widget.dataSubjectPrimaryEnterScore.subjectsGroupTag!.length-1?Colorconstand.lage:Colorconstand.primaryColor
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SvgPicture.asset(ImageAssets.subject_tag_icon,width: 16,),
                        const SizedBox(width: 3,),
                        Text( translate=="km"?dataGroup.subjectGroupTagName.toString():dataGroup.subjectGroupTagNameEn.toString(),style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),),
                      ],
                    ),
                  ),
                  ListView.builder(
                      shrinkWrap: true,
                      itemCount: widget.dataSubjectPrimaryEnterScore.subjectsGroupTag![indexSubjectGrup].teachingSubjects!.length,
                      physics: const NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.all(0),
                      itemBuilder: (context, indexChildSubject) {
                        final data = widget.dataSubjectPrimaryEnterScore.subjectsGroupTag![indexSubjectGrup].teachingSubjects![indexChildSubject];
                        return CardSubjectEnterScoreWidget(
                          maginleft: 15,
                          lastIndex: indexSubjectGrup == widget.dataSubjectPrimaryEnterScore.subjectsGroupTag!.length-1?true:false,
                          numClass: widget.dataSubjectPrimaryEnterScore.name.toString(),
                          titleSubject:translate=="km"? data.subjectName.toString():data.subjectNameEn.toString(),
                          present: data.attendance!.isCheckAttendance==0?"--": data.attendance!.present.toString(),
                          absent: data.attendance!.isCheckAttendance==0?"--": data.attendance!.absent.toString(),
                          examDate: data.examDate.toString(),
                          isExamSet: data.id,
                          isApprove: data.isApprove,
                          isCheckScoreEnter:  data.isCheckScoreEnter,
                          isNoExam: data.isNoExam,
                          onPressed: data.isCheckScoreEnter == 1?(){
                            BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                              idClass:  widget.dataSubjectPrimaryEnterScore.classId.toString(), 
                              subjectId:   data.subjectId.toString(), 
                              type: data.type.toString(), 
                              month:  data.month.toString(), 
                              semester:  data.semester.toString(),
                              examDate: ""));
                            BlocProvider.of<ViewImageBloc>(context).add(GetViewImageEvent(data.id!.toString()),);
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> 
                                ResultScreen(isInstructor: widget.dataSubjectPrimaryEnterScore.isInstructor == 0 ? false:true
                              ),
                            )
                          );
                          }:(){
                            if(data.id == 0){
                                  setState(() {
                                    classId = widget.dataSubjectPrimaryEnterScore.classId.toString();
                                    subjectId = data.subjectId.toString();
                                    isInstructor = widget.dataSubjectPrimaryEnterScore.isInstructor;
                                  });
                                    BlocProvider.of<SetExamBloc>(context).add(SetNoExam(idClass: widget.dataSubjectPrimaryEnterScore.classId.toString(), idsubject:  data.subjectId.toString(), semester: widget.semester.toString(), isNoExam: "2", type:widget.typeSemesterOrMonth.toString(),  month: widget.month.toString(),));
                                }
                            else{
                              BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                                idClass:  widget.dataSubjectPrimaryEnterScore.classId.toString(), 
                                subjectId:   data.subjectId.toString(), 
                                type: data.type.toString(), 
                                month:  data.month.toString(), 
                                semester:  data.semester.toString(),
                                examDate: ""),
                                ); 
                                  Navigator.push(context, 
                                    MaterialPageRoute(builder: (context) => GradingScreen(
                                    isPrimary: true,
                                    isInstructor: widget.dataSubjectPrimaryEnterScore.isInstructor == 0 ? false:true,
                                    myclass: widget.activeMySchedule,
                                    redirectFormScreen: 1,
                                    ),
                                  ),
                                );
          
                            }
                          },
                        );
                      },
                    ),
                    const SizedBox(height: 12,)
                ],
              );
            }
          ),
        ),
    );
  }
}
