import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/widget/custom_icon_title_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/constands/color_constands.dart';
class CardSubjectEnterScoreWidget extends StatelessWidget {
  final String absent;
  final String present;
  final String titleSubject;
  final String numClass;
  final double maginleft;
  final String? examDate;
  final int? isExamSet;
  final int? isNoExam;
  final int? isApprove;
  final int? isCheckScoreEnter;
  final bool? lastIndex;
  void Function()? onPressed;
  CardSubjectEnterScoreWidget({
    Key? key,
    this.examDate,
    this.absent = "-",
    this.maginleft = 0,
    this.present = "-",
    this.onPressed,
    this.isExamSet,
    this.titleSubject = "",
    this.numClass = "",
    required this.lastIndex,
    required this.isApprove,
    required this.isCheckScoreEnter,
    this.isNoExam,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:  EdgeInsets.only(
        left: maginleft,
        top: 12,
        bottom: 2.0,
        right: 16,
      ),
      decoration: BoxDecoration(
          color: lastIndex == false?Colorconstand.primaryColor:Colorconstand.lage, borderRadius: BorderRadius.circular(12)),
      child: Container(
          margin: const EdgeInsets.only(left: 6, right: 1, top: 1, bottom: 1),
          decoration: BoxDecoration(
            color: Colorconstand.neutralWhite,
            borderRadius: BorderRadius.circular(12),
          ),
          child: MaterialButton(
            onPressed: onPressed,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12))),
            padding: const EdgeInsets.all(0),
            child: Container(
                color: Colorconstand.primaryColor.withOpacity(0.06),
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 14.0),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            titleSubject,
                            style: ThemsConstands.headline3_semibold_20
                                .copyWith(
                                    height: 1,
                                    color: Colorconstand.lightBlack),
                          ),
                        ),
                        isCheckScoreEnter == 0 || isCheckScoreEnter == 2?
                        Container(
                          padding:const EdgeInsets.symmetric(vertical: 10,horizontal: 12),
                          decoration: BoxDecoration(
                            color:lastIndex == false?Colorconstand.primaryColor:Colorconstand.lage,
                            borderRadius: const BorderRadius.all(Radius.circular(12)),
                          ),
                          child: Row(
                            children: [
                              Text( isCheckScoreEnter == 0?"SCORE".tr():"IN_PROGRESS".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralWhite),),
                              const SizedBox(width: 5,),
                              const Icon(Icons.arrow_forward,size: 22,color: Colorconstand.neutralWhite,)
                            ],
                          ),
                        ) 
                        :Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              isCheckScoreEnter ==1 && isApprove ==1?"DONE".tr():isCheckScoreEnter ==1 && isApprove == 0?"AWAIT_APPROVE".tr(): isCheckScoreEnter == 2?"IN_PROGRESS".tr(): "NOT_YET_ENTER".tr(),
                              overflow: TextOverflow.ellipsis,
                              style: ThemsConstands.headline_6_semibold_14.copyWith(
                                  color:isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,),
                            ),
                            const SizedBox(width: 8),
                            Icon(
                              isCheckScoreEnter ==1 && isApprove ==1?Icons.check_circle:isCheckScoreEnter ==1 && isApprove == 0?Icons.access_time_filled_rounded:Icons.cancel,
                              size: 18.0,
                              color: isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                )),
          )),
    );
  }
}
