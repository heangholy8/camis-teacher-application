import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/constands/color_constands.dart';
import '../../../help/convert_time_helper.dart';

class CardScheduleSubjectPrimaryWidget extends StatelessWidget {
  final String? subjectColor;
  final int? scheduleType;
  final String? titleSubject;
  final String? tagSubject;
  final String? className;
  final String? timeSubject;
  final String? durationSubject;
  final String? present;
  final String? absent;
  final String? examDate;
  final String? homeworkExDate;
  final String? lessonAbout;
  final bool? isActive;
  final List? homeList;
  final List? lessList;
  final List? teacherAbsentList;
  final VoidCallback? onPressed;
  final int isCheckScoreEnter;
  final int isApprove;
  final bool isSubSubject;
  final String? researchAbout;
  final List? researchList;
  const CardScheduleSubjectPrimaryWidget({
    Key? key,
    required this.researchAbout,
    required this.researchList,
    this.examDate,
    required this.subjectColor,
    this.homeworkExDate,
    this.lessonAbout,
    this.titleSubject,
    this.tagSubject,
    this.className,
    this.absent,
    this.present,
    this.scheduleType,
    this.isActive,
    this.timeSubject,
    this.durationSubject,
    this.lessList,
    this.teacherAbsentList,
    this.homeList,
    required this.isSubSubject,
    this.onPressed,required this.isCheckScoreEnter,required this.isApprove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            isActive == true
                ? Container(
                    margin: const EdgeInsets.only(right: 12.0),
                    width: 20.0,
                    height: 13.0,
                    decoration:const BoxDecoration(
                      color: Colorconstand.primaryColor,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0),
                      ),
                    ),
                  )
                : const SizedBox(width: 30.0),
            Text(
              timeSubject!,
              style: ThemsConstands.button_semibold_16
                  .copyWith(color:  isActive == true ? Colorconstand.primaryColor : Colorconstand.lightBulma),
            ),
            const Spacer(),
            Text(
              convertMinutes(durationSubject!),
              style: ThemsConstands.headline_6_semibold_14
                  .copyWith(color: Colorconstand.lightTrunks),
            ),
            const SizedBox(width: 20.0),
          ],
        ),
        Container(
          margin: isActive ==true ? const EdgeInsets.only(left: 18.0, top: 10, right: 16.0, bottom: 16.0)
              : const EdgeInsets.only( left: 32, top: 14, bottom: 12.0, right: 16),
          decoration: BoxDecoration(
              color: Color(int.parse(subjectColor!)),
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                isActive == true
                  ? BoxShadow(
                      offset: const Offset(0, 10),
                      blurRadius: 19,
                      spreadRadius: 1,
                       color: Color(int.parse(subjectColor!)).withOpacity(0.2))
                  : const BoxShadow()
              ]
            ),
          child: Container(
              margin:const EdgeInsets.only(left: 6, right: 1, top: 1, bottom: 1),
              decoration: BoxDecoration(
                color: Colorconstand.neutralWhite,
                borderRadius: BorderRadius.circular(12),
              ),
              child: MaterialButton(
                onPressed: onPressed,
                shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                padding: const EdgeInsets.all(0),
                child: Container(
                    color: Color(int.parse(subjectColor!)).withOpacity(0.09),
                    padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 14.0),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  isSubSubject==true? Container():Container(
                                    margin:const EdgeInsets.only(bottom: 8),
                                    padding:const EdgeInsets.symmetric(vertical: 3,horizontal: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Colorconstand.primaryColor
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        SvgPicture.asset(ImageAssets.subject_tag_icon,width: 14,),
                                        const SizedBox(width: 3,),
                                        Text(
                                          tagSubject!,
                                          style: ThemsConstands.overline_semibold_12.copyWith(height: 1,color: Colorconstand.neutralWhite),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text(
                                    isSubSubject==true?tagSubject!:titleSubject!,
                                    style: ThemsConstands.headline3_semibold_20.copyWith(height: 1,color: Colorconstand.lightBlack),
                                  ),
                                ],
                              ),
                            ),
                            teacherAbsentList!.isNotEmpty?
                            Container(
                              width: 105,
                              height: 28,
                              decoration: BoxDecoration(
                                color: Color(int.parse(subjectColor!)),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(Icons.timer_sharp,size: 18,color: Colorconstand.neutralWhite,),
                                  const SizedBox(width: 3,),
                                  Text("NOT_TAUGHT".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color:Colorconstand.neutralWhite),overflow: TextOverflow.ellipsis,textAlign: TextAlign.left,),
                                ],
                              ),
                            ) 
                            :const SizedBox()
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 12.0,top: teacherAbsentList!.isEmpty?12:5),
                          child: Divider(
                            height: 1.2,
                            thickness: 1,
                            color:  Colorconstand.primaryColor.withOpacity(0.2),
                          ),
                        ),
                        scheduleType == 2 ? studySubject() : examSubject(),
                        teacherAbsentList!.isEmpty? Container()
                        :Container(
                          margin:const EdgeInsets.only(top: 8),
                          alignment: Alignment.centerLeft,
                          padding:const EdgeInsets.symmetric(vertical: 5,horizontal: 12),
                          decoration: BoxDecoration(
                            color: Colorconstand.neutralDarkGrey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(6)
                          ),
                          child: Row(
                            children: [
                              Text("${"REASON".tr()}៖",style: ThemsConstands.headline_6_semibold_14.copyWith(color:Colorconstand.lightBlack),),
                              Expanded(child: Text(" ${teacherAbsentList![0].title??"---"}",style: ThemsConstands.headline_6_regular_14_20height.copyWith(color:Colorconstand.neutralDarkGrey),overflow: TextOverflow.ellipsis,)),
                            ],
                          ),
                        ),
                      ],
                    )),
              )),
        ),
    
      ],
    );
  }


  Widget studySubject() {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconTitleWidget(
                        icon: ImageAssets.book_notebook,
                        title: "ARTICAL_QUOTES".tr(),
                        subjectColor: Color(int.parse(subjectColor!))),
                    const SizedBox(height: 8.0),
                    Container(
                      // margin:const EdgeInsets.only(left: 22),
                      child: Text(
                        lessList!.isEmpty?"-----":lessonAbout!,
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: ThemsConstands.headline_6_semibold_14
                            .copyWith(color: Colorconstand.lightBulma),
                      ),
                    ),
                  ],
                )),
            Expanded(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconTitleWidget(
                      icon: ImageAssets.HOMEWORK_ICON,
                      title: "RESEARCH".tr(),
                      subjectColor: Color(int.parse(subjectColor!))),
                  const SizedBox(height: 8.0),
                  Container(
                    // margin:const EdgeInsets.only(left: 0),
                    child: Text(
                      researchList!.isEmpty?"-----":researchAbout!,
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style: ThemsConstands.headline_6_semibold_14
                          .copyWith(color: Colorconstand.lightBulma),
                    ),
                  ),
                ],
              )),
            Expanded(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconTitleWidget(
                      icon: ImageAssets.HOMEWORK_ICON,
                      title:"HOMEWORK".tr(),
                      subjectColor: Color(int.parse(subjectColor!))),
                  const SizedBox(height: 8.0),
                  Container(
                    // margin:const EdgeInsets.only(left: 0),
                    child: Text(
                      homeList!.isEmpty?"HAVE_NOT".tr():"HAVE".tr(),
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style: ThemsConstands.headline_6_semibold_14
                          .copyWith(color: Colorconstand.lightBulma),
                    ),
                  ),
                ],
              )),
          ],
        ),
        isActive ==true
            ? const SizedBox(
                height: 16,
              )
            : const SizedBox(),
      ],
    );
  }

  Widget examSubject() {
    return Row(children: [
      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconTitleWidget(
                icon: ImageAssets.current_date,
                title: "EXAM_DAY".tr(),
                subjectColor: Colorconstand.exam),
            const SizedBox(
              height: 8.0,
            ),
            Container(
              margin:const EdgeInsets.only(left: 18),
              child: Text(
                examDate!,
                style: ThemsConstands.headline_6_semibold_14
                    .copyWith(color: Colorconstand.lightBulma),
              ),
            ),
          ],
        ),
      ),
      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconTitleWidget(
              icon: ImageAssets.EXAM_ICON,
              title: "SCORE_RESULT".tr(),
              subjectColor: Colorconstand.exam,
            ),
            const SizedBox( height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(width: 24),
                Text(
                  isCheckScoreEnter ==1 && isApprove ==1?"APPROVE".tr():isCheckScoreEnter ==1 && isApprove == 0?"AWAIT_APPROVE".tr(): isCheckScoreEnter == 2?"IN_PROGRESS".tr(): "NOT_YET_ENTER".tr(),
                  textAlign: TextAlign.right,
                  overflow: TextOverflow.ellipsis,
                  style: ThemsConstands.headline_6_semibold_14.copyWith(color:isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,),
                ),
                const SizedBox(width: 8),
                Icon(
                  Icons.access_time_filled_rounded,
                  size: 18.0,
                  color:isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,
                ),
              ],
            ),
          ],
        ),
      )
    ]);
  }
}

class IconTitleWidget extends StatelessWidget {
  final String icon;
  final String title;
  const IconTitleWidget({
    super.key,
    required this.icon,
    required this.title,
    required this.subjectColor,
  });

  final Color? subjectColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin:const EdgeInsets.only(bottom: 0),
          child: SvgPicture.asset(
            icon,
            height: 18,
            color: subjectColor ?? Colorconstand.primaryColor,
          ),
        ),
        const SizedBox(
          width: 5.0,
        ),
        Text(
          title,
          overflow: TextOverflow.ellipsis,
          style: ThemsConstands.overline_semibold_12
              .copyWith(color: subjectColor ?? Colorconstand.primaryColor),
        ),
      ],
    );
  }
}
