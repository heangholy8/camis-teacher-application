import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';

class CardHomeScheduleCardWidget extends StatelessWidget {
  String? startClass;
  String? endClass;
  String? grade;
  String? subject;
  String? tagSubject;
  String? presentCount;
  String? absentCount;
  VoidCallback? onTap;
  bool? isCheckAttendance;
  bool? isTeacherAbsent;
  bool? isSubSubject;

  CardHomeScheduleCardWidget(
      {Key? key,
      this.startClass,
      this.endClass,
      this.subject,
      this.presentCount,
      this.absentCount,
      this.onTap,
      this.tagSubject,
      this.isCheckAttendance,
      this.isTeacherAbsent,
      this.isSubSubject,
      this.grade})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        right: 8,
      ),
      decoration: BoxDecoration(
        color: Colorconstand.primaryColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
          margin: const EdgeInsets.only(left: 6, right: 1, top: 1, bottom: 1),
          decoration: BoxDecoration(
            color: Colorconstand.neutralWhite,
            borderRadius: BorderRadius.circular(12),
          ),
          child: MaterialButton(
            onPressed:onTap,
             shape:const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12))),
              padding:const EdgeInsets.all(0),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 14.0),
                      child: Column(
                        crossAxisAlignment:isSubSubject==true?CrossAxisAlignment.start:CrossAxisAlignment.center,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin:const EdgeInsets.only(right: 5,top: 5),
                                child:const Icon(Icons.access_time_rounded,size: 22,),
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(top: 5.0),
                                      child: Text(
                                        "$startClass",
                                        style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.lightBlack,height: 1,overflow: TextOverflow.ellipsis),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5.0,
                                    ),
                                    isSubSubject == true?Container():Container(
                                      margin: const EdgeInsets.only(top: 5.0),
                                      padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 6),
                                      decoration: BoxDecoration(
                                        color: Colorconstand.primaryColor,
                                        borderRadius: BorderRadius.circular(12)
                                      ),
                                      child: Row(
                                        children: [
                                          SvgPicture.asset(ImageAssets.subject_tag_icon,width: 12,),
                                          const SizedBox(width: 3,),
                                          Text("$tagSubject",style: ThemsConstands.caption_simibold_12.copyWith(height: 1,color: Colorconstand.neutralWhite),),
                                        ],
                                      )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            margin:EdgeInsets.only(left: isSubSubject==true? 35:0),
                            child: Text(isSubSubject==true?"$tagSubject":"$subject",style: ThemsConstands.button_semibold_16.copyWith(height: 1,color: Colorconstand.primaryColor),)),
                        ],
                      ),
                    ),
                  ),
                  isTeacherAbsent == true?
                    Container(
                      width: 105,
                      height: 28,
                      decoration: BoxDecoration(
                        color: Colorconstand.primaryColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(Icons.timer_sharp,size: 18,color: Colorconstand.neutralWhite,),
                          const SizedBox(width: 3,),
                          Text("NOT_TAUGHT".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color:Colorconstand.neutralWhite),overflow: TextOverflow.ellipsis,textAlign: TextAlign.left,),
                        ],
                      ),
                    )
                    :isCheckAttendance == false? Container(
                      padding:const EdgeInsets.symmetric(vertical: 12,horizontal: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Colorconstand.primaryColor,
                      ),
                      child: Text("PRESENCE".tr(),
                        style: ThemsConstands.headline_6_semibold_14
                            .copyWith(color: Colorconstand.neutralWhite),
                        textAlign: TextAlign.center,
                      ),
                    ): Row(
                     children: [
                      Container(
                        width: 35,
                        padding: const EdgeInsets.symmetric(
                          vertical: 4,
                        ),
                        decoration: BoxDecoration(
                          color: Colorconstand.alertsPositive,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text("$presentCount",
                          style: ThemsConstands.headline_6_semibold_14
                              .copyWith(color: Colorconstand.neutralWhite),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Container(
                        width: 35,
                        padding: const EdgeInsets.symmetric(
                          vertical: 4,
                        ),
                        decoration: BoxDecoration(
                          color: Colorconstand.alertsDecline,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text("$absentCount",
                          style: ThemsConstands.headline_6_semibold_14
                              .copyWith(color: Colorconstand.neutralWhite),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(width: 12,)
                ],
              ),
            ),
    ));
  }
}
