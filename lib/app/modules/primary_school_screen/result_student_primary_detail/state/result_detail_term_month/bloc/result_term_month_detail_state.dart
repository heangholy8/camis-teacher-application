part of 'result_term_month_detail_bloc.dart';

class ResultTermMonthDetailState extends Equatable {
  const ResultTermMonthDetailState();
  
  @override
  List<Object> get props => [];
}

class ResultTermMonthDetailInitial extends ResultTermMonthDetailState {}
class  ResultDetailMonthSemesterPrimaryLoading extends ResultTermMonthDetailState {}

class  ResultDetailMonthSemesterPrimaryLoaded extends ResultTermMonthDetailState {
  final StudentResultMonthSemesterDetailPrimaryModel? resultDetailStudentMonthSemesterPrimaryModel;
  const  ResultDetailMonthSemesterPrimaryLoaded({required this.resultDetailStudentMonthSemesterPrimaryModel});
}

class  ResultDetailMonthSemesterPrimaryError extends ResultTermMonthDetailState {}
