part of 'result_term_month_detail_bloc.dart';

class ResultTermMonthDetailEvent extends Equatable {
  const ResultTermMonthDetailEvent();

  @override
  List<Object> get props => [];
}

class StudentResultDetailMonthSemesterEvent extends ResultTermMonthDetailEvent {
  final String? semester;
  final String? idClass;
  final String? studentId;
  final String? month;
  final String?type;
 const StudentResultDetailMonthSemesterEvent({required this.studentId,required this.type,required this.semester,required this.idClass,required this.month});
}
