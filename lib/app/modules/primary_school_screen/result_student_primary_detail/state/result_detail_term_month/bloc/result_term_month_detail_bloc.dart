import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/primary_school_model/result_detail_month_semester_model.dart';
import '../../../../../../service/api/get_list_result_primary_school/get_result_primary_school_api.dart';
part 'result_term_month_detail_event.dart';
part 'result_term_month_detail_state.dart';

class ResultTermMonthDetailBloc extends Bloc<ResultTermMonthDetailEvent, ResultTermMonthDetailState> {
  final GetPrimaryResultApi resultDetailStudentMonthSemesterPrimaryApi;
  ResultTermMonthDetailBloc({required this.resultDetailStudentMonthSemesterPrimaryApi}) : super(ResultTermMonthDetailInitial()) {
    on<StudentResultDetailMonthSemesterEvent>((event, emit) async{
      emit(ResultDetailMonthSemesterPrimaryLoading());
      try {
        var dataResultDetailMonthSemesterPrimary = await resultDetailStudentMonthSemesterPrimaryApi.getDetailResultPrimarySchoolApi(
          idClass: event.idClass,term: event.semester,month: event.month,type: event.type,studentId: event.studentId
        );
        emit(ResultDetailMonthSemesterPrimaryLoaded(resultDetailStudentMonthSemesterPrimaryModel: dataResultDetailMonthSemesterPrimary));
      } catch (e) {
        print(e);
        emit(ResultDetailMonthSemesterPrimaryError());
      }
    });
  }
}
