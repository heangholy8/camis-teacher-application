import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/primary_school_screen/result_student_primary_detail/state/result_detail_term_month/bloc/result_term_month_detail_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';

class DetailResultPrimaryStudentScreen extends StatefulWidget {
  final String type;
  final String monthName;
  final String studentName;
  final String classname;
  const DetailResultPrimaryStudentScreen({super.key,required this.classname,required this.studentName,required this.monthName,required this.type});

  @override
  State<DetailResultPrimaryStudentScreen> createState() => _DetailResultPrimaryStudentScreenState();
}

class _DetailResultPrimaryStudentScreenState extends State<DetailResultPrimaryStudentScreen> {
  bool commentTeacherShow = true;
  ScrollController? controllerData;
  double offset = 0.0;
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    //========= controllerSchedule offset set ========
    controllerData = ScrollController(initialScrollOffset: offset);
    controllerData!.addListener(() {
      setState(() {
        offset = controllerData!.offset;
      });
    });
    //========= controllerSchedule offset set =========
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                padding:const EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  children: [
                    IconButton(
                      icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 22,),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Expanded(
                      child: Text("${"PETRIOD_RESULT".tr()} ${widget.type == "1"?"MONTH".tr():""}${widget.monthName.tr()}",style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.darkTextsRegular),textAlign: TextAlign.center,),
                    ),
                    IconButton(
                      icon:const Icon(Icons.check_circle,size: 28,color: Colors.transparent,),
                      onPressed: (){
                      },
                    )
                  ],
                ),
              ),
              Expanded(
                child: BlocBuilder<ResultTermMonthDetailBloc, ResultTermMonthDetailState>(
                  builder: (context, state) {
                    if(state is ResultDetailMonthSemesterPrimaryLoading){
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    else if(state is ResultDetailMonthSemesterPrimaryLoaded){
                      var data = state.resultDetailStudentMonthSemesterPrimaryModel!.dataResultStudentDetail;
                      return Column(
                        children: [
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                    color: Colorconstand.primaryColor,
                                    borderRadius: BorderRadius.all(Radius.circular(16))
                                  ),
                                  padding: const EdgeInsets.symmetric(vertical: 3,horizontal: 8),
                                  child: Row(
                                    children: [
                                      const Icon(Icons.person_outline,size: 18,color: Colorconstand.neutralWhite,),
                                      Text(widget.studentName,style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.symmetric(horizontal: 12),
                                  height: 8,width: 8 ,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colorconstand.primaryColor,
                                  ),
                                ),
                                Text(widget.classname,style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.only(top: 18),
                              child: Column(
                                children: [
                                  data!.result == null? 
                                  Container()
                                  :Container(
                                    color: Colorconstand.primaryColor,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(vertical: 18),
                                            alignment: Alignment.center,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                SvgPicture.asset(ImageAssets.book,color: Colorconstand.neutralWhite,width: 35,),
                                                const SizedBox(width: 18,),
                                                Column(
                                                  children: [
                                                    Text("AVERAGE".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralWhite),),
                                                    const SizedBox(height: 12,),
                                                    Row(
                                                      //crossAxisAlignment: CrossAxisAlignment.end,
                                                      children: [
                                                        Text(data.result!.avgScore.toString(),style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),),
                                                        Text(" /${data.result!.maxAvgScore}",style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralWhite),),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 1,
                                          height: 55,
                                          color: Colorconstand.neutralWhite,
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(vertical: 18),
                                            alignment: Alignment.center,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                SvgPicture.asset(ImageAssets.award,color: Colorconstand.neutralWhite,width: 35,),
                                                const SizedBox(width: 18,),
                                                Column(
                                                  children: [
                                                    Text("RANK".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralWhite),),
                                                    const SizedBox(height: 12,),
                                                    Row(
                                                      //crossAxisAlignment: CrossAxisAlignment.end,
                                                      children: [
                                                        Text("${"លេខ".tr()} ${data.result!.rank}",style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),),
                                                        Text(" /${data.result!.totalRank}",style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralWhite),),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                   data.result == null? Container()
                                   : data.result!.teacherComment == null || data.result!.teacherComment == "" ?const SizedBox(): 
                                   AnimatedContainer(
                                    margin:const EdgeInsets.only(bottom: 5),
                                    padding:const EdgeInsets.symmetric(vertical: 12,horizontal: 18),
                                    color: Colorconstand.primaryColor.withOpacity(0.15),
                                    duration: const Duration(milliseconds: 200),
                                    height: offset>10?0:97,
                                    width: MediaQuery.of(context).size.width,
                                    child: offset>10? Container():Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text("មូលវិចាររបស់(គ្រូប្រចាំថ្នាក់) ${data.result!.teacherName.toString()} ៖",style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralDarkGrey),),
                                        const SizedBox(height: 8,),
                                        Text(data.result!.teacherComment.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding:const EdgeInsets.symmetric(horizontal: 18),
                                    color: Colorconstand.neutralGrey,
                                    height: 45,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text("SUBJECT".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,),
                                        ),
                                        Expanded(
                                          child: Row(
                                            children: [
                                              Expanded(child: Text("POINT".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,)),
                                              Expanded(child: Text("GRADE_POINT".tr(),style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,)),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: SingleChildScrollView(
                                      controller: controllerData,
                                      physics:const ClampingScrollPhysics(),
                                      child: Column(
                                        children: [
                                          ListView.builder(
                                            physics:const NeverScrollableScrollPhysics(),
                                            shrinkWrap: true,
                                            padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 0),
                                            itemCount: data.subjectTag!.length,
                                            itemBuilder: (context, indexSubjectTag) {
                                              return Container(
                                                alignment: Alignment.centerLeft,
                                                padding:const EdgeInsets.only(top: 12),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      padding:const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(15),
                                                        color:  indexSubjectTag == data.subjectTag!.length-1?Colorconstand.lage:Colorconstand.primaryColor
                                                      ),
                                                      child: Row(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: [
                                                          SvgPicture.asset(ImageAssets.subject_tag_icon,width: 16,),
                                                          const SizedBox(width: 3,),
                                                          Text(translate =="km"?data.subjectTag![indexSubjectTag].name.toString():data.subjectTag![indexSubjectTag].nameEn.toString(),style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),),
                                                        ],
                                                      ),
                                                    ),
                                                    ListView.separated(
                                                      physics:const NeverScrollableScrollPhysics(),
                                                      shrinkWrap: true,
                                                      padding:const EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                                                      itemCount: data.subjectTag![indexSubjectTag].subject!.length,
                                                      itemBuilder: (context, indexsubject) {
                                                        return Container(
                                                          height: 37,
                                                          child: Row(
                                                            children: [
                                                              Expanded(
                                                                child: Text(translate == "km"?data.subjectTag![indexSubjectTag].subject![indexsubject].name.toString():data.subjectTag![indexSubjectTag].subject![indexsubject].nameEn.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.left,),
                                                              ),
                                                              Expanded(
                                                                child: Row(
                                                                  children: [
                                                                    Expanded(child: Text(data.subjectTag![indexSubjectTag].subject![indexsubject].score.toString() == "null"?"--":data.subjectTag![indexSubjectTag].subject![indexsubject].score.toString(),style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,)),
                                                                    Expanded(child: Text(data.subjectTag![indexSubjectTag].subject![indexsubject].grading.toString() == "null"?"--":translate == "km"?data.subjectTag![indexSubjectTag].subject![indexsubject].grading.toString():data.subjectTag![indexSubjectTag].subject![indexsubject].gradingEn.toString(),style: ThemsConstands.button_semibold_16.copyWith(color: data.subjectTag![indexSubjectTag].subject![indexsubject].grading.toString() == "null"?Colorconstand.primaryColor:data.subjectTag![indexSubjectTag].subject![indexsubject].letterGrade.toString() == "F"
                                                                                                                                                    ? Colorconstand.alertsNotifications
                                                                                                                                                    : data.subjectTag![indexSubjectTag].subject![indexsubject].letterGrade.toString() == "A"
                                                                                                                                                        ? Colorconstand.alertsPositive
                                                                                                                                                        : data.subjectTag![indexSubjectTag].subject![indexsubject].letterGrade.toString() == "B"
                                                                                                                                                            ? Colorconstand.mainColorSecondary
                                                                                                                                                            : Colorconstand.alertsAwaitingText),textAlign: TextAlign.center,)),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                      separatorBuilder: (context, index) {
                                                        return Container(
                                                          margin:const EdgeInsets.symmetric(horizontal: 0),
                                                          height: 1,
                                                          color: Colorconstand.neutralGrey,
                                                        );
                                                      },
                                                    )
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                                          data.result == null?Container()
                                          :Container(
                                            margin: const EdgeInsets.only(top: 12,bottom: 28,left: 28,right: 28),
                                            child:Row(
                                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment:CrossAxisAlignment.center,
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                                                    decoration: const BoxDecoration(color: Colorconstand.subject7, borderRadius: const BorderRadius.all(Radius.circular(70))),
                                                    child: Column(children: [
                                                      Text(
                                                        "PERMISSION".tr(),
                                                        style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralWhite),
                                                      ),
                                                      Text(
                                                        "${data.result!.absenceWithPermission.toString()} ${translate=="km"?"ដង":"Time"}",
                                                        style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),
                                                      )
                                                    ]),
                                                  ),
                                                ),
                                                const SizedBox(width: 18,),
                                                Expanded(
                                                  child: Container(
                                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                                                    decoration: const BoxDecoration(color:Colorconstand.alertsDecline, borderRadius: const BorderRadius.all(Radius.circular(70))),
                                                    child: Column(children: [
                                                      Text(
                                                        "ABSENT".tr(),
                                                        style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralWhite),
                                                      ),
                                                      Text(
                                                      "${data.result!.absenceWithoutPermission.toString()} ${translate=="km"?"ដង":"Time"}",
                                                        style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite),
                                                      )
                                                    ]),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            alignment: Alignment.centerLeft,
                                              padding:const EdgeInsets.only(left: 20),
                                              child:Text(
                                                "NOTED".tr(),
                                                style:ThemsConstands.headline_5_semibold_16,
                                              ),
                                          ),
                                          Container(
                                            margin:const EdgeInsets.only(bottom: 25,left: 45,top: 10),
                                            alignment: Alignment.center,
                                            child: ListView.builder(
                                              padding: const EdgeInsets.all(0),
                                              physics:const NeverScrollableScrollPhysics(),
                                              shrinkWrap:true,
                                              itemCount:data.gradingSystem!.length,
                                              itemBuilder:(context,index) {
                                                return Row(
                                                  crossAxisAlignment:CrossAxisAlignment.center,
                                                  mainAxisSize:MainAxisSize.min,
                                                  mainAxisAlignment:MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      width: 150,
                                                      margin: const EdgeInsets.symmetric(vertical: 5),
                                                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                                      decoration: BoxDecoration(color: index !=data.gradingSystem!.length - 1 ? Colorconstand.primaryColor : const Color(0xFFCE0000), borderRadius: const BorderRadius.all(Radius.circular(25))),
                                                      child: Center(
                                                        child: Text("${"FROM".tr()} ${data.gradingSystem![index].scoreMin!} ${"TO".tr()} ${data.gradingSystem![index].scoreMax!}", style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                                      ),
                                                    ),
                                                    Expanded(child: Text(translate == "km" ? " = ${data.gradingSystem![index].description!}" : " = ${data.gradingSystem![index].descriptionEn!}", style: ThemsConstands.headline_5_semibold_16.copyWith(
                                                                                                                                                    color:data.gradingSystem![index].letterGrade.toString() == "F"
                                                                                                                                                    ? Colorconstand.alertsNotifications
                                                                                                                                                    : data.gradingSystem![index].letterGrade.toString() == "A"
                                                                                                                                                        ? Colorconstand.alertsPositive
                                                                                                                                                        : data.gradingSystem![index].letterGrade.toString() == "B"
                                                                                                                                                            ? Colorconstand.mainColorSecondary
                                                                                                                                                            : Colorconstand.alertsAwaitingText))),
                                                      
                                                    ],
                                                  );
                                                },
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      );
                    }
                    else{
                      return Center(
                        child: EmptyWidget(
                          title: "WE_DETECT".tr(),
                          subtitle: "WE_DETECT_DES".tr(),
                        ),
                      );
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}