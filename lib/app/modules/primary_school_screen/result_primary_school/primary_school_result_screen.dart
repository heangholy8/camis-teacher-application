import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/primary_school_screen/result_primary_school/state/result_primary_state/bloc/result_primary_school_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_screenshot/no_screenshot.dart';
import 'package:page_transition/page_transition.dart';
import 'package:super_tooltip/super_tooltip.dart';
import '../result_student_primary_detail/primary_student_result_detail.dart';
import '../result_student_primary_detail/state/result_detail_term_month/bloc/result_term_month_detail_bloc.dart';

class ViewResultPrimarySchoolScreen extends StatefulWidget {
  final String month;
  final String semesterName;
  final String classId;
  final String monthId;
  final String type;
  const ViewResultPrimarySchoolScreen({super.key,required this.type,required this.monthId,required this.month,required this.classId,required this.semesterName});

  @override
  State<ViewResultPrimarySchoolScreen> createState() => _ViewResultPrimarySchoolScreenState();
}

class _ViewResultPrimarySchoolScreenState extends State<ViewResultPrimarySchoolScreen> {
  final _controller1 = ScrollController();
  final _controller2 = ScrollController();
  final _controllerTooltip = SuperTooltipController();

  final _noScreenshot = NoScreenshot.instance;

  void enableScreenshot() async {
    bool result = await _noScreenshot.screenshotOn();
    debugPrint('Enable Screenshot: $result');
  }

  void disableScreenshot() async {
    bool result = await _noScreenshot.screenshotOff();
    debugPrint('Screenshot Off: $result');
  }
  @override
  void initState() {
    super.initState();
    // disableScreenshot();
    _controller1.addListener(listener1);
    _controller2.addListener(listener2);
  }

  var _flag1 = false;
  var _flag2 = false;

  void listener1() {
    if (_flag2) return;
    _flag1 = true;
    _controller2.jumpTo(_controller1.offset);
    _flag1 = false;
  }

  void listener2() {
    if (_flag1) return;
    _flag2 = true;
    _controller1.jumpTo(_controller2.offset);
    _flag2 = false;
  }

  @override
  void dispose() {
    super.dispose();
    // enableScreenshot();
    _controller1.removeListener(listener1);
    _controller2.removeListener(listener2);
    _controller1.dispose();
    _controller2.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      body: SafeArea(
        top: true,
        bottom: false,
        child: Stack(
          children:[
            Container(
              color: Colorconstand.neutralWhite,
              child: Column(
                children: [
                  Container(
                    padding:const EdgeInsets.symmetric(horizontal: 12),
                    child: Row(
                      children: [
                        IconButton(
                          icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 22,),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Expanded(
                          child: Text("${"PETRIOD_RESULT".tr()} ${widget.type == "1"?"MONTH".tr():""} ${widget.month.tr()}",style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.darkTextsRegular),textAlign: TextAlign.center,),
                        ),
                        IconButton(
                          icon:const Icon(Icons.check_circle,size: 28,color: Colors.transparent,),
                          onPressed: (){
                           // BlocProvider.of<PrintBlocBloc>(context).add(GetResultPDFEvent(semester: widget.semesterName,month: widget.monthId.toString(),classId: widget.classId,column: "1"));
                          },
                        )
                      ],
                    ),
                  ),
                  const SizedBox(height: 8,),
                  Expanded(
                    child: BlocBuilder<ResultPrimarySchoolBloc, ResultPrimarySchoolState>(
                      builder: (context, state) {
                        if(state is ResultAllSubjectPrimaryLoading){
                          return const Center(child: CircularProgressIndicator(),);
                        }
                        else if(state is ResultAllSubjectPrimaryLoaded){
                          var data = state.resultAllSubjectPrimaryModel!.data;
                          var dataNameStudent = data!.data;
                          var dataHeader1AllSubjectPrimary = data.header!.where((element) => element.notEvaluation==false || element.notEvaluation== 0 ).toList();
                          var dataHeader2AllSubjectPrimary = data.header!.where((element) => element.notEvaluation==true).toList();

                          return Column(
                            children: [
                              Container(
                                height: 118,
                                child: Row(
                                  children: [
                                    Container(
                                      height: 118,
                                      width: 140,
                                      color: Colorconstand.neutralGrey.withOpacity(0.5),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 35,
                                            child: Text("N.O".tr(),style: ThemsConstands.overline_semibold_12,textAlign: TextAlign.center,)),
                                          Expanded(child: Text("${"FULL_NAME".tr()} (${dataNameStudent!.length})",style: ThemsConstands.overline_semibold_12,textAlign: TextAlign.center,)),
                                        ],
                                      ),
                                    ), 
                                    Expanded(
                                      child: SingleChildScrollView(
                                      physics:const ClampingScrollPhysics(),
                                      scrollDirection: Axis.horizontal,
                                      controller: _controller1,
                                      child: Row(
                                        children: [
                                          Container(
                                              height: 118,
                                              child: ListView.builder(
                                                padding:const EdgeInsets.all(0),
                                                physics:const NeverScrollableScrollPhysics(),
                                                shrinkWrap: true,
                                                scrollDirection: Axis.horizontal,
                                                itemCount: dataHeader1AllSubjectPrimary.length,
                                                itemBuilder: (context, indexMainSubjextHeader) {
                                                  return Container(
                                                    color: Color(int.parse("0xff${dataHeader1AllSubjectPrimary[indexMainSubjextHeader].color}")),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          height: 30,
                                                          width: 30*(dataHeader1AllSubjectPrimary[indexMainSubjextHeader].subject!.length.toDouble()),
                                                          child: Center(
                                                            child: SuperTooltip(
                                                              showBarrier: true,
                                                              hasShadow: false,
                                                              barrierColor: Colors.transparent,
                                                              content: Text(translate=="km"?dataHeader1AllSubjectPrimary[indexMainSubjextHeader].name.toString():dataHeader1AllSubjectPrimary[indexMainSubjextHeader].nameEn.toString()),
                                                              child: Text(translate=="km"?dataHeader1AllSubjectPrimary[indexMainSubjextHeader].name.toString():dataHeader1AllSubjectPrimary[indexMainSubjextHeader].nameEn.toString(),style: ThemsConstands.overline_semibold_12.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 88,
                                                          child: ListView.builder(
                                                            padding:const EdgeInsets.all(0),
                                                            physics:const NeverScrollableScrollPhysics(),
                                                            scrollDirection: Axis.horizontal,
                                                            shrinkWrap: true,
                                                            itemCount: dataHeader1AllSubjectPrimary[indexMainSubjextHeader].subject!.length,
                                                            itemBuilder: (context, indexChildSubjextHeader) {
                                                              return Container(
                                                                width: 30,
                                                                color:indexChildSubjextHeader%2==0?Colorconstand.neutralGrey.withOpacity(0.57):Colorconstand.neutralGrey.withOpacity(0.67),
                                                                child: Center(
                                                                  child: RotatedBox(
                                                                    quarterTurns: 3,
                                                                    child: Text( translate=="km"?dataHeader1AllSubjectPrimary[indexMainSubjextHeader].subject![indexChildSubjextHeader].subjectName.toString():dataHeader1AllSubjectPrimary[indexMainSubjextHeader].subject![indexChildSubjextHeader].subjectNameEn.toString(),style: ThemsConstands.overline_semibold_12.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,)),
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                            Container(width: dataHeader2AllSubjectPrimary.isEmpty?0:5,),
                                            dataHeader2AllSubjectPrimary.isEmpty?Container():Container(
                                              height: 118,
                                              child: ListView.builder(
                                                padding:const EdgeInsets.all(0),
                                                physics:const NeverScrollableScrollPhysics(),
                                                shrinkWrap: true,
                                                scrollDirection: Axis.horizontal,
                                                itemCount: dataHeader2AllSubjectPrimary.length,
                                                itemBuilder: (context, indexMainSubjextHeader) {
                                                  return Container(
                                                    color: Color(int.parse("0xff${dataHeader2AllSubjectPrimary[indexMainSubjextHeader].color}")),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          height: 30,
                                                          width: 30*(dataHeader2AllSubjectPrimary[indexMainSubjextHeader].subject!.length.toDouble()),
                                                          child: Center(
                                                            child: SuperTooltip(
                                                              showBarrier: true,
                                                              hasShadow: false,
                                                              barrierColor: Colors.transparent,
                                                              content: Text(translate=="km"?dataHeader2AllSubjectPrimary[indexMainSubjextHeader].name.toString():dataHeader2AllSubjectPrimary[indexMainSubjextHeader].nameEn.toString()),
                                                              child: Text(translate=="km"?dataHeader2AllSubjectPrimary[indexMainSubjextHeader].name.toString():dataHeader2AllSubjectPrimary[indexMainSubjextHeader].nameEn.toString(),style: ThemsConstands.overline_semibold_12.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 88,
                                                          child: ListView.builder(
                                                            padding:const EdgeInsets.all(0),
                                                            physics:const NeverScrollableScrollPhysics(),
                                                            scrollDirection: Axis.horizontal,
                                                            shrinkWrap: true,
                                                            itemCount: dataHeader2AllSubjectPrimary[indexMainSubjextHeader].subject!.length,
                                                            itemBuilder: (context, indexChildSubjextHeader) {
                                                              return Container(
                                                                width: 30,
                                                                color:indexChildSubjextHeader%2==0?Colorconstand.neutralGrey.withOpacity(0.57):Colorconstand.neutralGrey.withOpacity(0.67),
                                                                child: Center(
                                                                  child: RotatedBox(
                                                                    quarterTurns: 3,
                                                                    child: Text( translate=="km"?dataHeader2AllSubjectPrimary[indexMainSubjextHeader].subject![indexChildSubjextHeader].subjectName.toString():dataHeader2AllSubjectPrimary[indexMainSubjextHeader].subject![indexChildSubjextHeader].subjectNameEn.toString(),style: ThemsConstands.overline_semibold_12.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,)),
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                        ],
                                      ),
                                      ),
                                    )                                     
                                  ],
                                ),
                              ),
                              Expanded(
                                child: SingleChildScrollView(
                                  physics:const ClampingScrollPhysics(),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: 140,
                                            child: ListView.separated(
                                              padding:const EdgeInsets.all(0),
                                              physics:const ClampingScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount: dataNameStudent.length,
                                              itemBuilder: (context, indexRow) {
                                                return GestureDetector(
                                                  onTap: () {
                                                    BlocProvider.of<ResultTermMonthDetailBloc>(context).add(StudentResultDetailMonthSemesterEvent(idClass: dataNameStudent[indexRow].classId.toString(),month: widget.monthId,type: widget.type,semester: widget.semesterName,studentId:dataNameStudent[indexRow].studentId.toString() ));
                                                    Navigator.push(context,
                                                      PageTransition( type:PageTransitionType.rightToLeft,
                                                        child: DetailResultPrimaryStudentScreen(classname: translate == "km"?dataNameStudent[indexRow].className.toString():dataNameStudent[indexRow].classNameEn.toString(),studentName: translate == "km"?dataNameStudent[indexRow].name.toString():dataNameStudent[indexRow].nameEn.toString()==" "?dataNameStudent[indexRow].name.toString():dataNameStudent[indexRow].nameEn.toString(),monthName: widget.month,type: widget.type,),
                                                      ),
                                                    );
                                                  },
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    height: 40,
                                                    child: Row(
                                                      children: [
                                                        SizedBox(
                                                          width: 35,
                                                          child: Text("${indexRow+1}",style: ThemsConstands.overline_semibold_12,textAlign: TextAlign.center,)),
                                                        Expanded(child: Text(translate=="km"?dataNameStudent[indexRow].name.toString():dataNameStudent[indexRow].nameEn.toString()==" "?dataNameStudent[indexRow].name.toString():dataNameStudent[indexRow].nameEn.toString(),style: ThemsConstands.headline_6_regular_14_20height,textAlign: TextAlign.left,)),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                              separatorBuilder: (context, index) {
                                                return Container(height: 0.3,color: Colorconstand.neutralDarkGrey,);
                                              },
                                            ),
                                          ),
                                          Expanded(
                                            child: SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              physics:const ClampingScrollPhysics(),
                                              controller: _controller2,
                                              child: Column(
                                                children: [
                                                  Container(
                                                    width: (30*data.data![0].result!.length).toDouble()+5,
                                                    child: ListView.separated(
                                                      padding:const EdgeInsets.all(0),
                                                      physics:const ClampingScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount: data.data!.length,
                                                      itemBuilder: (context, indexRow) {
                                                        var dataResult1AllSubjectPrimary = data.data![indexRow].result!.where((element) => element.notEvaluation==false,).toList();
                                                        var dataResult2AllSubjectPrimary = data.data![indexRow].result!.where((element) => element.notEvaluation==true,).toList();
                                                        return Container(
                                                          height: 40,
                                                          child: Row(
                                                            children: [
                                                              ListView.builder(
                                                                padding:const EdgeInsets.all(0),
                                                                physics:const NeverScrollableScrollPhysics(),
                                                                shrinkWrap: true,
                                                                scrollDirection: Axis.horizontal,
                                                                itemCount: dataResult1AllSubjectPrimary.length,
                                                                itemBuilder: (context, indexColumn1) {
                                                                  return GestureDetector(
                                                                    onTap: () {
                                                                      BlocProvider.of<ResultTermMonthDetailBloc>(context).add(StudentResultDetailMonthSemesterEvent(idClass: data.data![indexRow].classId.toString(),month: widget.monthId,type: widget.type,semester: widget.semesterName,studentId:data.data![indexRow].studentId.toString() ));
                                                                      Navigator.push(context,
                                                                        PageTransition( type:PageTransitionType.rightToLeft,
                                                                          child: DetailResultPrimaryStudentScreen(classname: translate == "km"?data.data![indexRow].className.toString():data.data![indexRow].classNameEn.toString(),studentName: translate == "km"?data.data![indexRow].name.toString():data.data![indexColumn1].nameEn.toString(),monthName: widget.month,type: widget.type,),
                                                                        ),
                                                                      );
                                                                    },
                                                                    child: Container(
                                                                      color: Color(int.parse("0xff${dataResult1AllSubjectPrimary[indexColumn1].color}")),
                                                                      width: 30,
                                                                      child: Container(
                                                                        alignment: Alignment.center,
                                                                        color:Colorconstand.neutralGrey.withOpacity(double.parse(dataResult1AllSubjectPrimary[indexColumn1].opacity!.toString())),
                                                                        child: Text(dataResult1AllSubjectPrimary[indexColumn1].display.toString(),style: ThemsConstands.overline_semibold_12.copyWith(color: indexColumn1 == (dataResult1AllSubjectPrimary.length-1)?Colorconstand.alertsDecline:dataResult1AllSubjectPrimary[indexColumn1].subjectId == 0?Colorconstand.primaryColor:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,)),
                                                                    ),
                                                                  );
                                                                },
                                                              ),
                                                              Container(width: dataResult2AllSubjectPrimary.isEmpty?0:5,),
                                                              dataResult2AllSubjectPrimary.isEmpty?Container():
                                                              ListView.builder(
                                                                padding:const EdgeInsets.all(0),
                                                                physics:const NeverScrollableScrollPhysics(),
                                                                shrinkWrap: true,
                                                                scrollDirection: Axis.horizontal,
                                                                itemCount: dataResult2AllSubjectPrimary.length,
                                                                itemBuilder: (context, indexColumn2) {
                                                                  return GestureDetector(
                                                                    onTap: () {
                                                                      BlocProvider.of<ResultTermMonthDetailBloc>(context).add(StudentResultDetailMonthSemesterEvent(idClass: data.data![indexRow].classId.toString(),month: widget.monthId,type: widget.type,semester: widget.semesterName,studentId:data.data![indexRow].studentId.toString() ));
                                                                      Navigator.push(context,
                                                                        PageTransition( type:PageTransitionType.rightToLeft,
                                                                          child: DetailResultPrimaryStudentScreen(classname: translate == "km"?data.data![indexRow].className.toString():data.data![indexRow].classNameEn.toString(),studentName: translate == "km"?data.data![indexRow].name.toString():data.data![indexColumn2].nameEn.toString(),monthName: widget.month,type: widget.type,),
                                                                        ),
                                                                      );
                                                                    },
                                                                    child: Container(
                                                                      color: Color(int.parse("0xff${dataResult2AllSubjectPrimary[indexColumn2].color}")),
                                                                      width: 30,
                                                                      child: Container(
                                                                        alignment: Alignment.center,
                                                                        color:Colorconstand.neutralGrey.withOpacity(double.parse(dataResult2AllSubjectPrimary[indexColumn2].opacity!.toString())),
                                                                        child: Text(dataResult2AllSubjectPrimary[indexColumn2].display.toString(),style: ThemsConstands.overline_semibold_12.copyWith(color: indexColumn2 == (dataResult2AllSubjectPrimary.length-1)?Colorconstand.alertsDecline:dataResult2AllSubjectPrimary[indexColumn2].subjectId == 0?Colorconstand.primaryColor:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,)),
                                                                    ),
                                                                  );
                                                                },
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                      separatorBuilder: (context, index) {
                                                        return Container(height: 0.3,color: Colorconstand.neutralDarkGrey,);
                                                      },
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )
                                        
                                        ],
                                      ),
                                      Container(
                                        padding:const EdgeInsets.only(bottom: 28),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin:const EdgeInsets.only(top: 18,bottom: 10,left: 48,right: 22),
                                              child: Text("TOTAL_AVG_STU".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.left,),
                                            ),
                                            Container(
                                              margin:const EdgeInsets.symmetric(horizontal: 22),
                                              child: ListView.builder(
                                                physics:const NeverScrollableScrollPhysics(),
                                                shrinkWrap: true,
                                                padding:const EdgeInsets.all(0),
                                                itemCount: data.totalAvg!.length,
                                                itemBuilder: (context, indexSumaryTotal) {
                                                  return Container(
                                                    margin:const EdgeInsets.symmetric(vertical: 5),
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          margin:const EdgeInsets.only(right: 12),
                                                          width: 110,
                                                          padding: const EdgeInsets.symmetric(vertical: 3),
                                                          decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(18),
                                                            color: indexSumaryTotal == data.totalAvg!.length-1?Colorconstand.alertsDecline:Colorconstand.primaryColor,
                                                          ),
                                                          child: Text(data.totalAvg![indexSumaryTotal].name.toString(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                                        ),
                                                        Text("${data.totalAvg![indexSumaryTotal].total.toString()} ${"PEOPLE".tr()}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: indexSumaryTotal == data.totalAvg!.length-1?Colorconstand.alertsDecline: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                                        Container(
                                                          margin:const EdgeInsets.symmetric(horizontal: 8),
                                                          height: 5,
                                                          width: 5,
                                                          decoration:BoxDecoration(
                                                            color: indexSumaryTotal == data.totalAvg!.length-1?Colorconstand.alertsDecline:Colorconstand.neutralDarkGrey,
                                                            shape: BoxShape.circle
                                                          ),
                                                        ),
                                                        Text("${"FEMALE".tr()} ${data.totalAvg![indexSumaryTotal].female.toString()} ${"PEOPLE".tr()}",style: ThemsConstands.headline_5_semibold_16.copyWith(color:indexSumaryTotal == data.totalAvg!.length-1?Colorconstand.alertsDecline: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                                      ],
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          );
                        }
                        else{
                          return Center(child: CircularProgressIndicator(),);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}