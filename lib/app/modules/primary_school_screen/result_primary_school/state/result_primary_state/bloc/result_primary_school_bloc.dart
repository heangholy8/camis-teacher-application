import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/get_list_result_primary_school/get_result_primary_school_api.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/primary_school_model/list-all-student-subject-result_primary_model.dart';
part 'result_primary_school_event.dart';
part 'result_primary_school_state.dart';

class ResultPrimarySchoolBloc extends Bloc<ResultPrimarySchoolEvent, ResultPrimarySchoolState> {
  final GetPrimaryResultApi resultAllSubjectPrimaryApi;
  ResultPrimarySchoolBloc({required this.resultAllSubjectPrimaryApi}) : super(ResultPrimarySchoolInitial()) {
    on<ListStudentResultEvent>((event, emit) async {
     emit(ResultAllSubjectPrimaryLoading());
      try {
        var dataResultAlSubjectPrimary = await resultAllSubjectPrimaryApi.getResultPrimarySchoolApi(
          idClass: event.idClass,term: event.semester,month: event.month,type: event.type
        );
        emit(ResultAllSubjectPrimaryLoaded(resultAllSubjectPrimaryModel: dataResultAlSubjectPrimary));
      } catch (e) {
        print(e);
        emit(ResultAllSubjectPrimaryError());
      }
    });
  }
}
