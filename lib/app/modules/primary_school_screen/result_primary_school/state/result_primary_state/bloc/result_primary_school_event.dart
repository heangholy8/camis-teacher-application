part of 'result_primary_school_bloc.dart';

class ResultPrimarySchoolEvent extends Equatable {
  const ResultPrimarySchoolEvent();

  @override
  List<Object> get props => [];
}
class ListStudentResultEvent extends ResultPrimarySchoolEvent {
  final String? semester;
  final String? idClass;
  final String? month;
  final String?type;
 const ListStudentResultEvent({required this.type,required this.semester,required this.idClass,required this.month});
}
