part of 'result_primary_school_bloc.dart';

class ResultPrimarySchoolState extends Equatable {
  const ResultPrimarySchoolState();
  
  @override
  List<Object> get props => [];
}

class ResultPrimarySchoolInitial extends ResultPrimarySchoolState {}

class  ResultAllSubjectPrimaryLoading extends ResultPrimarySchoolState {}

class  ResultAllSubjectPrimaryLoaded extends ResultPrimarySchoolState {
  final ListAllStudentSubjectResultPrimaryModel? resultAllSubjectPrimaryModel;
  const  ResultAllSubjectPrimaryLoaded({required this.resultAllSubjectPrimaryModel});
}

class  ResultAllSubjectPrimaryError extends ResultPrimarySchoolState {}
