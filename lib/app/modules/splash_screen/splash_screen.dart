// ignore_for_file: use_build_context_synchronously
import 'package:camis_teacher_application/app/modules/primary_school_screen/result_book/result_book_kindergarten_screen.dart';
import 'package:camis_teacher_application/app/modules/primary_school_screen/result_book/result_book_screen.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/view/affaire_home_screen.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/routes/app_route.dart';
import 'package:camis_teacher_application/app/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../storages/get_storage.dart';
import '../account_confirm_screen/view/account_confirm_screen.dart';
import '../home_screen/view/home_screen.dart';
import '../home_screen/widget/error_connection_widget.dart';
import '../principle/principle_home_screen/view/principle_home_screen.dart';
import '../select_lang/view/select_language.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final KeyStoragePref keyPref = KeyStoragePref();
  bool? isoffline;
  String? rolrLogin;
  Future checkLogin() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    var token = pref.getString(keyPref.keyTokenPref) ?? "";
    var phoneNumber = pref.getString(keyPref.keyPhonePref) ?? "";
    var selectLang = pref.getString(keyPref.keyLangPref) ?? "";
    var comfirmInfo = pref.getString(keyPref.keyConfirmPref) ?? "";
    if(dataaccess.authUser == null){
    }
    else{
      rolrLogin = dataaccess.authUser!.role.toString();
    } 
    print("fasdfasdf$rolrLogin");
    if (selectLang == "") {
      Future.delayed(const Duration(seconds: 2), () async {
        Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) =>
                  const SelectLanguageScreen(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ),
            (route) => false);
      });
    } else {
      if (token != "") {
        // if (phoneNumber != "") {
          if (comfirmInfo != "") {
            //if (selectedMonitor != "") {
              try {
                final result = await InternetAddress.lookup('google.com');
                if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                  setState(() {
                    isoffline = true;
                    Future.delayed(const Duration(seconds: 2), () async {
                      if(rolrLogin == "6"){
                        Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) =>
                                const AffaireHomeScreen(),
                            transitionDuration: Duration.zero,
                            reverseTransitionDuration: Duration.zero,
                          ),
                          (route) => false);
                      }
                      else if(rolrLogin == "1"){
                        Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                  const PrincipleHomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),(route) => false);
                      }
                      else{
                        Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) =>
                                const HomeScreen(),
                            transitionDuration: Duration.zero,
                            reverseTransitionDuration: Duration.zero,
                          ),
                          (route) => false);
                      }
                      
                    });
                  });
                }
              } on SocketException catch (_) {
                setState(() {
                  isoffline = false;
                  Future.delayed(const Duration(seconds: 2), () async {
                    Navigator.pushAndRemoveUntil(
                        context,
                        PageRouteBuilder(
                          pageBuilder: (context, animation1, animation2) =>
                              const ErroreWidgetConnectionDaskBoard(),
                          transitionDuration: Duration.zero,
                          reverseTransitionDuration: Duration.zero,
                        ),
                        (route) => false);
                  });
                });
              }
            // } else {
            //   Navigator.pushAndRemoveUntil(
            //       context,
            //       PageRouteBuilder(
            //         pageBuilder: (context, animation1, animation2) =>
            //             const SetClassMonitorScreen(),
            //         transitionDuration: Duration.zero,
            //         reverseTransitionDuration: Duration.zero,
            //       ),
            //       (route) => false);
            // }
          } else {
            Future.delayed(const Duration(seconds: 2), () async {
              Navigator.pushAndRemoveUntil(context,
                PageRouteBuilder(pageBuilder: (context, animation1, animation2) =>
                      const AccountConfirmScreen(),
                  transitionDuration: Duration.zero,
                  reverseTransitionDuration: Duration.zero,
                ),
                (route) => false);
            });
          }
        // } else {
        //   Future.delayed(const Duration(seconds: 2), () async {
        //     Navigator.pushReplacementNamed(context, Routes.VERIFICATIONSCREEN);
        //   });
        // }
      } else {
        Future.delayed(const Duration(seconds: 2), () async {
          Navigator.pushNamed(context, Routes.OPTIONLOGINSCREEN);
        });
        // Future.delayed(const Duration(seconds: 2), () async {
        //       Navigator.pushAndRemoveUntil(context,
        //         PageRouteBuilder(pageBuilder: (context, animation1, animation2) =>
        //               const ResultBookKindergartenScreen(),
        //           transitionDuration: Duration.zero,
        //           reverseTransitionDuration: Duration.zero,
        //         ),
        //         (route) => false);
        //     });
      }
    }
  }

  @override
  void initState() {
    setState(() {
      checkLogin();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset(
              "assets/images/Oval.png",
              width: MediaQuery.of(context).size.width / 1.7,
            ),
          ),
          Positioned(
            top: 150,
            left: 0,
            child: Image.asset(
              "assets/images/Oval (1).png",
            ),
          ),
          Positioned(
            bottom: 35,
            left: 0,
            child: Image.asset(
              "assets/images/Oval (3).png",
            ),
          ),
          Positioned(
            bottom: MediaQuery.of(context).size.height / 5,
            right: MediaQuery.of(context).size.width / 4,
            child: Center(
                child: Image.asset(
              "assets/images/Oval (2).png",
            )),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 75),
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text(
                      "Powered by:",
                      style: ThemsConstands.caption_regular_12
                          .copyWith(color: Colors.transparent),
                    ),
                    Text(
                      "CAMIS Solutions",
                      style: ThemsConstands.caption_regular_12
                          .copyWith(color: Colors.transparent),
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 7),
                      child: SvgPicture.asset(
                        ImageAssets.logoCamis,
                        width: 45,
                        height: 45,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 5, left: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "CAMEMIS",
                            style: ThemsConstands.headline_1_semibold_32
                            .copyWith(
                                color: Colorconstand.neutralWhite,
                                fontWeight: FontWeight.w600,
                                fontSize: 40),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Text(
                            "RECOMMENTFORTEACHER".tr(),
                            style: ThemsConstands.caption_regular_12
                                .copyWith(
                              color: Colorconstand.neutralWhite,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      "POWEREDBY".tr(),
                      style: ThemsConstands.caption_regular_12.copyWith(
                        color: Colorconstand.neutralWhite,
                      ),
                    ),
                    Text(
                      "CAMIS Solutions",
                      style: ThemsConstands.caption_regular_12
                          .copyWith(color: Colorconstand.neutralWhite),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 8),
                      height: 30,
                      width: 30,
                      child: const Center(
                        child: CircularProgressIndicator(
                          color: Colorconstand.neutralWhite,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
