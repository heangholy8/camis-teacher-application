import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CardAnounment extends StatelessWidget {
  const CardAnounment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      margin: const EdgeInsets.only(right: 8),
      decoration: BoxDecoration(
        color: Colorconstand.neutralWhite,
        borderRadius: BorderRadius.circular(12),
      ),
      child: MaterialButton(
        onPressed: () {},
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12))),
        padding: const EdgeInsets.all(0),
        child: Stack(
          fit: StackFit.passthrough,
          clipBehavior: Clip.none,
          children: [
            Positioned(
                bottom: 0,
                right: 0,
                child: ClipRRect(
                  borderRadius:
                      BorderRadius.circular(12.0), //add border radius
                  child: SvgPicture.asset(
                    "assets/icons/svg/Red_anounment_back.svg",
                    fit: BoxFit.cover,
                  ),
                )),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                margin: const EdgeInsets.all(14),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(
                          bottom: 4, top: 9, left: 12, right: 12),
                      decoration: BoxDecoration(
                          color: Colorconstand.alertsDecline,
                          borderRadius: BorderRadius.circular(12)),
                      child: Text(
                        "ដំណឹងបន្ទាន់របស់សាលា",
                        style: ThemsConstands.overline_semibold_12
                            .copyWith(color: Colorconstand.neutralWhite),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 8),
                      child: Text(
                        "ប្រកាសបិទសាលាបណ្ដោះអាសន្ន ទប់ស្កាត់ជំងឺ",
                        style: ThemsConstands.button_semibold_16
                            .copyWith(color: Colorconstand.primaryColor),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Container(
                      child: Text(
                        "តាមរយៈលិខិត បានឱ្យដឹងថា ដោយសារមានការឆ្លងជំងឺកូវីដ-១៩ ក្នុងសហគមន៍ នៅក្នុងស្រុកគិរីសាគរ ខេត្តកោះកុង និងដើម្បី ចៀសវាង",
                        style: ThemsConstands.caption_regular_12.copyWith(
                            color: Colorconstand.neutralDarkGrey,
                            height: 1.89),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding:
                                const EdgeInsets.only(bottom: 6, right: 7),
                            child: SvgPicture.asset(
                              ImageAssets.calendar_icon,
                              height: 14,
                              color: Colorconstand.darkTextsPlaceholder,
                            ),
                          ),
                          Expanded(
                              child: Text(
                            "០៨ តុលា ២០២២",
                            style: ThemsConstands.caption_regular_12.copyWith(
                                color: Colorconstand.darkTextsPlaceholder),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              right: 12,
              top: 12,
              child: GestureDetector(
                  onTap: () async {
                    SharedPreferences pref =
                        await SharedPreferences.getInstance();
                    KeyStoragePref key = KeyStoragePref();

                    pref.remove(key.keyPhonePref).then((value) =>
                        debugPrint("PhonePref was removed from cache"));
                  },
                  child: const Icon(Icons.close)),
            )
          ],
        ),
      ),
    );
  }
}
