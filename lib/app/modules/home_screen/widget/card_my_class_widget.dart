import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import '../../../core/constands/color_constands.dart';

class CardMyClass extends StatelessWidget {
  String? startClass;
  String? endClass;
  String? grade;
  String? subject;
  String? presentCount;
  String? absentCount;
  String? subjectColor;
  VoidCallback? onTap;
  bool? isCheckAttendance;
  bool? isTeacherAbsent;

  CardMyClass(
      {Key? key,
      this.startClass,
      this.endClass,
      this.subject,
      this.presentCount,
      this.absentCount,
      this.subjectColor,
      this.onTap,
      this.isCheckAttendance,
      this.isTeacherAbsent,
      this.grade})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        right: 8,
      ),
      decoration: BoxDecoration(
        color: Color(int.parse(subjectColor!)),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
          margin: const EdgeInsets.only(left: 6, right: 1, top: 1, bottom: 1),
          decoration: BoxDecoration(
            color: Colorconstand.neutralWhite,
            borderRadius: BorderRadius.circular(12),
          ),
          child: MaterialButton(
            onPressed:onTap,
             shape:const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
              padding:const EdgeInsets.all(0),
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 14.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 6),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        gradient:const LinearGradient(
                          begin: Alignment(0.49999999999999994, -3.0616171314629196e-17),
                          end: Alignment(0.49999999999999994, 0.9999999999999999),
                          colors: [
                            Color(0xff4d9ef6),
                            Color(0xff2c70c7),
                            Colorconstand.primaryColor
                          ]
                        )
                      ),
                      child: Text(
                        "$grade",
                        textAlign: TextAlign.center,
                        style: ThemsConstands.button_semibold_16
                            .copyWith(color: Colorconstand.neutralWhite),
                      ),
                    ),
                    const SizedBox(
                      width: 9.0,
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("$startClass - $endClass",style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.lightBlack,height: 1,overflow: TextOverflow.ellipsis)),
                            const SizedBox(height: 8.0),
                            Text("$subject",style: ThemsConstands.headline_6_regular_14_20height.copyWith(height: 1,color: Colorconstand.darkTextsPlaceholder)),
                          ],
                        ),
                      ),
                    ),
                    isTeacherAbsent == true?
                      Container(
                        width: 105,
                        height: 28,
                        decoration: BoxDecoration(
                          color: Color(int.parse(subjectColor!)),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.timer_sharp,size: 18,color: Colorconstand.neutralWhite,),
                            const SizedBox(width: 3,),
                            Text("NOT_TAUGHT".tr(),style: ThemsConstands.headline_6_regular_14_20height.copyWith(color:Colorconstand.neutralWhite),overflow: TextOverflow.ellipsis,textAlign: TextAlign.left,),
                          ],
                        ),
                      ): Row(
                      children: [
                        Container(
                          width: 35,
                          padding: const EdgeInsets.symmetric(
                            vertical: 4,
                          ),
                          decoration: BoxDecoration(
                            color: Colorconstand.alertsPositive,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Text(isCheckAttendance==false?"--":"$presentCount",
                            style: ThemsConstands.headline_6_semibold_14
                                .copyWith(color: Colorconstand.neutralWhite),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Container(
                          width: 35,
                          padding: const EdgeInsets.symmetric(
                            vertical: 4,
                          ),
                          decoration: BoxDecoration(
                            color: Colorconstand.alertsDecline,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Text(isCheckAttendance==false?"--": "$absentCount",
                            style: ThemsConstands.headline_6_semibold_14
                                .copyWith(color: Colorconstand.neutralWhite),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
    ));
  }
}
