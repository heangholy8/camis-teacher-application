// ignore_for_file: must_be_immutable
import 'dart:async';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';

class CardAttendanceWidget extends StatelessWidget {
  String className;
  String subjectName;
  String title;
  String time;
  double width;
  VoidCallback onTap;
  int staffPresentStatus;

  CardAttendanceWidget({super.key, 
    required this.className,
    required this.subjectName,
    required this.title,
    required this.time,
    required this.width,
    required this.onTap,
    required this.staffPresentStatus,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(4),
      width: width,
      decoration: BoxDecoration(
        color:staffPresentStatus == 1|| staffPresentStatus == 4 ?Colorconstand.alertsPositive:staffPresentStatus == 0?Colorconstand.primaryColor:staffPresentStatus == 2?Colorconstand.mainColorForecolor:Colors.grey,
        borderRadius: BorderRadius.circular(18),
        boxShadow:[ 
          BoxShadow(
            color: Colors.grey.withOpacity(0.3), //color of shadow
            spreadRadius: 1, //spread radius
            blurRadius: 3, // blur radius
            offset:const Offset(1, 1), // changes position of shadow
          ),
        ],
      ),
      child: MaterialButton(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(18)),
        ),
        onPressed: onTap,
        padding: const EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset(ImageAssets.timer_icon,width: 30,color: Colors.white,),
                  const SizedBox(width: 5,),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(
                        top: 8, bottom: 8, left: 8, right: 8),
                      decoration: BoxDecoration(
                        color: Colorconstand.neutralSecondary,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          staffPresentStatus==4||staffPresentStatus==1? const Icon(Icons.check_rounded,color: Colorconstand.alertsPositive,size: 18,):Container(
                            margin: const EdgeInsets.only(bottom: 0, right: 6),
                            alignment: Alignment.center,
                            height: 5,
                            width: 5,
                            decoration:  BoxDecoration(
                              color:staffPresentStatus == 1|| staffPresentStatus == 4 ?Colorconstand.alertsPositive:staffPresentStatus == 0?Colorconstand.primaryColor:staffPresentStatus == 2?Colorconstand.mainColorForecolor:Colors.grey,
                              shape: BoxShape.circle
                            ),
                          ),
                          Expanded(
                            child: Text(
                              staffPresentStatus==4?"PRESENT".tr(): staffPresentStatus==1? "RECORDINGATTENDANCE".tr():staffPresentStatus==3?"ABSENTS".tr():staffPresentStatus==2?"PERMISSION".tr():"CLICE_SHOW_PRESENCE".tr(),
                              style: ThemsConstands.subtitle1_regular_16.copyWith(
                                color:staffPresentStatus == 1|| staffPresentStatus == 4 ?Colorconstand.alertsPositive:staffPresentStatus == 0?Colorconstand.primaryColor:staffPresentStatus == 2?Colorconstand.mainColorForecolor:Colors.grey,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(
                    left: 10,
                    right: 10,
                  ),
                  alignment: Alignment.centerLeft,
                  child: Text(
                   staffPresentStatus==4?"PRESENT".tr(): staffPresentStatus==1? "RECORDINGATTENDANCE_DONE".tr():staffPresentStatus==3?"ABSENTS".tr():staffPresentStatus==2?"PERMISSION".tr():"CHECKATTENDANCE".tr(),
                    style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const SizedBox(height: 12),
                Container (
                  margin: const EdgeInsets.only(left: 10, right: 10),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    time,
                    style: ThemsConstands.subtitle1_regular_16.copyWith( color:Colorconstand.neutralWhite),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
            
              ],
            ),
            Container(
              margin: const EdgeInsets.only(left: 10, right: 12, top: 0, bottom: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      subjectName,
                      style: ThemsConstands.overline_semibold_12.copyWith(color: Colorconstand.neutralWhite),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,textAlign: TextAlign.left,
                    ),
                  ),
                  // const SizedBox(
                  //   height: 50,
                  //   width: 50,
                  // )
                ],
              ),
          ),
          ],
        ),
      ),
    );
  }
}



