

import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ButttonQuickAction extends StatelessWidget {
  final String namebutton;
  final String imageIcon;
  final VoidCallback onTap;
  const ButttonQuickAction({Key? key, required this.namebutton, required this.imageIcon, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            child: MaterialButton(
              elevation: 1,
              color: Colorconstand.neutralWhite,
              onPressed: onTap,
              padding:const EdgeInsets.all(18),
              shape: const CircleBorder(),
              child: SvgPicture.asset(imageIcon,width: 28,color: Colorconstand.primaryColor,),
            ),
          ),
          const SizedBox(height: 5,),
          Container(
            margin:const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
            child: Text(namebutton,textAlign: TextAlign.center,style: ThemsConstands.headline_6_semibold_14.copyWith(color:Colorconstand.neutralDarkGrey,)),
          )
        ],
      ),
    );
  }
}