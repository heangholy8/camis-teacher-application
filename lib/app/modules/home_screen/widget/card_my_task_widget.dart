import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import '../../../core/thems/thems_constands.dart';

class CardMyTaskWidget extends StatelessWidget {
  String className;
  String subjectName;
  dynamic averageInputScore;
  String taskStatusName;
  int taskStatus;
  String title;
  String taskAbout;
  bool attendance;
  String time;
  String aboutExam;
  double width;
  int eventType;
  dynamic countdownDays;
  VoidCallback onTap;
  CardMyTaskWidget({super.key, 
    required this.attendance,
    required this.averageInputScore,
    required this.countdownDays,
    required this.className,
    required this.subjectName,
    required this.title,
    required this.taskAbout,
    required this.taskStatusName,
    required this.taskStatus,
    required this.time,
    required this.aboutExam,
    required this.width,
    required this.onTap,
    required this.eventType,
  });

  @override
  Widget build(BuildContext context) {
     final translate = context.locale.toString();
    return Container(
      margin: const EdgeInsets.all(4),
      width: width,
      decoration: BoxDecoration(
          color: attendance == true?const Color(0xffDAEBFF)
          :taskStatus==3?const Color(0xFFFFE4E4)
          :taskStatus==4?const Color(0xFF8CBA85)
          :taskStatus == 1?Colorconstand.alertsRedShades
          :taskStatus == 2? countdownDays >2 && countdownDays <=5? Colorconstand.alertsPositiveBg
          :countdownDays == 1 || countdownDays ==2?Colorconstand.alertsAwaitingBg:Colorconstand.alertsPositiveBg:const Color(0xFFBDE9B6),
          borderRadius: BorderRadius.circular(18),
          boxShadow:[ 
               BoxShadow(
                  color: Colors.grey.withOpacity(0.3), //color of shadow
                  spreadRadius: 1, //spread radius
                  blurRadius: 3, // blur radius
                  offset:const Offset(1, 1), // changes position of shadow
                  
               ),
              ],
      ),
      child: MaterialButton(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(18))),
        onPressed: onTap,
        padding: const EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 10, left: 10, right: 10, bottom: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        //shape: BoxShape.circle,
                        borderRadius: BorderRadius.circular(25),
                        gradient: const LinearGradient(
                            begin: Alignment(
                                0.49999999999999994, -3.0616171314629196e-17),
                            end: Alignment(
                                0.49999999999999994, 0.9999999999999999),
                            colors: [
                              Color(0xff4d9ef6),
                              Color(0xff2c70c7),
                              Colorconstand.primaryColor
                            ])),
                    child: Text(
                      className,
                      textAlign: TextAlign.center,
                      style: ThemsConstands.overline_semibold_12
                          .copyWith(color: Colorconstand.lightBackgroundsWhite),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                        top: 8, bottom: 8, left: 8, right: 8),
                    decoration: BoxDecoration(
                        color: Colorconstand.neutralSecondary,
                        border: Border.all(
                            color: attendance == true?const Color(0xFFFFB319)
                            : taskStatus == 1?Colorconstand.supportiveDodoria10
                            : taskStatus == 3?Colorconstand.supportiveDodoria10
                            : taskStatus == 4?Colorconstand.alertsPositive
                            : taskStatus == 2? countdownDays >2 && countdownDays <=6? Colorconstand.alertsPositive
                            : countdownDays == 1 || countdownDays ==2? const Color(0xFFF4A560):Colorconstand.alertsPositive
                            :Colorconstand.alertsPositive,),
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 0, right: 6),
                          alignment: Alignment.center,
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                              color: attendance == true? const Color(0xFFFFB319)
                              : taskStatus == 3 || taskStatus == 1?Colorconstand.supportiveDodoria10
                              : taskStatus == 2? countdownDays >2 && countdownDays <=6? Colorconstand.alertsPositive
                              :countdownDays == 1 || countdownDays ==2?const Color(0xFFF4A560):Colorconstand.alertsPositiveBg:Colorconstand.alertsPositive,
                              shape: BoxShape.circle),
                        ),
                        Text(
                          attendance== true?"IN_PROGRESS".tr(): taskStatus == 0?"SOON".tr():taskStatusName == "REMAINING"? "${countdownDays} ${"NEXT_DAY".tr()}":taskStatusName.tr(),
                          style: ThemsConstands.overline_semibold_12.copyWith(
                              color: attendance == true?const Color(0xFFFFB319)
                              : taskStatus == 3 || taskStatus == 1?Colorconstand.supportiveDodoria10
                              : taskStatus == 2? countdownDays >2 && countdownDays <=6? Colorconstand.alertsPositive
                              :countdownDays == 1 || countdownDays ==2?const Color(0xFFF4A560):Colorconstand.alertsPositiveBg:Colorconstand.alertsPositive,                          
                              height: 0),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                left: 10,
                right: 10,
                top: 5,
              ),
              alignment: Alignment.centerLeft,
              child: Text(
                title,
                style: ThemsConstands.headline_5_semibold_16.copyWith(
                    color: Colorconstand.primaryColor),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                left: 10,
                right: 10,
                top: 0,
              ),
              alignment: Alignment.centerLeft,
              child: Text(
                attendance == true? time :" ${"REGULAR".tr()} ${eventType == 1? "${translate == "km"?"ខែ":""}${checkMonth(int.parse(aboutExam))}":aboutExam.tr()}",
                style: ThemsConstands.headline_6_semibold_14.copyWith(
                    color:Colorconstand.darkTextsRegular),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                  left: 10, right: 12, top: 0, bottom: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      child: Text(
                    subjectName,
                    style: ThemsConstands.overline_semibold_12.copyWith(
                        color: Colorconstand.darkTextsLow),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,textAlign: TextAlign.left,
                  )),
                  attendance ==true?Container():Container(
                    height: 50,
                    width: 50,
                    child: CircularPercentIndicator(
                      radius: 24.0,
                      lineWidth: 4.0,
                      animation: true,
                      percent:averageInputScore>=100? 1.0 :(averageInputScore / 100),
                      center: Text(
                        "${averageInputScore.toString()}%",
                        style:ThemsConstands.caption_regular_12.copyWith(color: averageInputScore>=100?Colorconstand.primaryColor: Colorconstand.primaryColor),textAlign: TextAlign.center,
                      ),
                      circularStrokeCap: CircularStrokeCap.round,
                      progressColor: Colorconstand.primaryColor,
                      backgroundColor: Colorconstand.neutralWhite,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
