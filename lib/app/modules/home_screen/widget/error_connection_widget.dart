import 'dart:async';
import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/view/affaire_home_screen.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/shimmer/shimmer_style.dart';
import '../../../core/thems/thems_constands.dart';
import '../../principle/principle_home_screen/view/principle_home_screen.dart';
import '../view/home_screen.dart';

class ErroreWidgetConnectionDaskBoard extends StatefulWidget {
  const ErroreWidgetConnectionDaskBoard({Key? key}) : super(key: key);

  @override
  State<ErroreWidgetConnectionDaskBoard> createState() => _ErroreWidgetConnectionDaskBoardState();
}

class _ErroreWidgetConnectionDaskBoardState extends State<ErroreWidgetConnectionDaskBoard> {
  GetStoragePref pref = GetStoragePref();
  String? schoolName;
  int? monthlocal;
  int? daylocal;
  String? roleLogin;
  StreamSubscription? internetconnection;
  bool? isoffline;
  void getLocalData() async {
    var schoolname = await pref.getJsonToken;
    var dataAuth = await pref.getJsonToken;
    setState(() {
      schoolName = schoolname.schoolName.toString();
      roleLogin = dataAuth.authUser!.role.toString();
    });
  }

  Future checkInterNet() async{
    try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              isoffline = true;
            });
          }
        } on SocketException catch (_) {
          setState(() {
            isoffline = false;
          });
        }
  }
  @override
  void initState()  {
    getLocalData();
    setState((){
      checkInterNet();
      //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        if (result == ConnectivityResult.none) {
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          setState(() {
            isoffline = true;
            if(roleLogin == "6"){
              Navigator.pushAndRemoveUntil(context,
              PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => const AffaireHomeScreen(),transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,),(route) => false);
            }
            else if(roleLogin == "1"){
              Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) =>
                        const PrincipleHomeScreen(),
                    transitionDuration: Duration.zero,
                    reverseTransitionDuration: Duration.zero,
                  ),(route) => false);
            }
            else{
              Navigator.pushAndRemoveUntil(context,
                PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => const HomeScreen(),transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,),(route) => false);
            }
          });
        } else if (result == ConnectivityResult.wifi) {
          setState(() {
            isoffline = true;
            if(roleLogin == "6"){
              Navigator.pushAndRemoveUntil(context,
              PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => const AffaireHomeScreen(),transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,),(route) => false);
            }
            else if(roleLogin == "1"){
              Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) =>
                        const PrincipleHomeScreen(),
                    transitionDuration: Duration.zero,
                    reverseTransitionDuration: Duration.zero,
                  ),(route) => false);
            }
            else{
              Navigator.pushAndRemoveUntil(context,
                PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => const HomeScreen(),transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,),(route) => false);
            }
        });
        }
      });
      //=============Eend Check internet====================
    });
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
    super.initState();
  }
  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 247, 248, 249),
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Container(
          color: Colorconstand.primaryColor,
          child: Stack(
            children: [
              Positioned(
                right: 0,
                top: 0,
                child: SvgPicture.asset("assets/images/Path_home_1.svg"),
              ),
              
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                  margin: const EdgeInsets.only(top: 37),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 18, vertical: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                margin: const EdgeInsets.only(left: 0),
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: Text(
                                        "$schoolName",
                                        style: ThemsConstands
                                            .subtitle1_regular_16
                                            .copyWith(
                                                color: Colorconstand
                                                    .darkBackgroundsDisabled),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      child: Text(
                                        "${"HELLO".tr()}......",
                                        style: ThemsConstands
                                            .headline3_semibold_20
                                            .copyWith(
                                                color: Colorconstand
                                                    .lightTextsRegular),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Container(
                              width: 64,
                              decoration: BoxDecoration(
                                  color: Colorconstand.neutralWhite
                                      .withOpacity(0.2),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10))),
                              child: Container(
                                padding: const EdgeInsets.only(
                                    top: 3, bottom: 12, left: 5, right: 5),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Text(daylocal.toString(),
                                          style: ThemsConstands
                                              .headline_1_semibold_32
                                              .copyWith(
                                                  color: Colorconstand
                                                      .neutralWhite,
                                                  height: 1.3),
                                          textAlign: TextAlign.center),
                                    ),
                                    Center(
                                        child: Text(
                                      "${(monthlocal == 1 ? "JANUARY" : monthlocal == 2 ? "FEBRUARY" : monthlocal == 3 ? "MARCH" : monthlocal == 4 ? "APRIL" : monthlocal == 5 ? "MAY" : monthlocal == 6 ? "JUNE" : monthlocal == 7 ? "JULY" : monthlocal == 8 ? "AUGUST" : monthlocal == 9 ? "SEPTEMBER" : monthlocal == 10 ? "OCTOBER" : monthlocal == 11 ? "NOVEMBER" : "DECEMBER").tr()}  ",
                                      style: ThemsConstands.subtitle1_regular_16
                                          .copyWith(
                                              color:
                                                  Colorconstand.neutralWhite,
                                              height: 1),
                                      textAlign: TextAlign.center,
                                    ))
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: const BoxDecoration(
                                color: Colorconstand.neutralWhite,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(16),
                                  topRight: Radius.circular(16),
                                ),
                              ),
                          child: Column(
                            children: [
                              Expanded(
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,bottom: 0,left: 0,right: 0,
                                      child: Container(
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(12),
                                                topRight: Radius.circular(12))),
                                        child: SingleChildScrollView(
                                          child: Column(
                                            children: [
                                              Container(
                                                child:const ShimmerDaskBoardTask(),
                                              ),
                                              Container(
                                                child:const ShimmerDaskBoardService(),
                                              ),
                                              Container(
                                                child:const ShimmerDaskBoardQuickType(),
                                              )
                                            ],
                                          ),
                                        )
                                      ),
                                    ),
                                    AnimatedPositioned(
                                      bottom: isoffline == true ? -150 : 0,
                                      left: 0,
                                      right: 0,
                                      duration: const Duration(milliseconds: 500),
                                      child: const NoConnectWidget(),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

  }
}