import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/widget/custom_icon_title_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/constands/color_constands.dart';
class CardExamSubjectWidget extends StatelessWidget {
  final String absent;
  final String present;
  final String titleSubject;
  final String numClass;
  final double maginleft;
  final String? examDate;
  final int? isExamSet;
  final int? isNoExam;
  final int? isApprove;
  final int? isCheckScoreEnter;
  void Function()? onPressed;
  CardExamSubjectWidget({
    Key? key,
    this.examDate,
    this.absent = "-",
    this.maginleft = 0,
    this.present = "-",
    this.onPressed,
    this.isExamSet,
    this.titleSubject = "",
    this.numClass = "",
    required this.isApprove,
    required this.isCheckScoreEnter,
    this.isNoExam,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:  EdgeInsets.only(
        left: maginleft,
        top: 12,
        bottom: 2.0,
        right: 16,
      ),
      decoration: BoxDecoration(
        color: Colorconstand.exam, borderRadius: BorderRadius.circular(12)),
      child: Container(
          margin: const EdgeInsets.only(left: 6, right: 1, top: 1, bottom: 1),
          decoration: BoxDecoration(
            color: Colorconstand.neutralWhite,
            borderRadius: BorderRadius.circular(12),
          ),
          child: MaterialButton(
            onPressed: onPressed,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12))),
            padding: const EdgeInsets.all(0),
            child: Container(
                color: Colorconstand.exam.withOpacity(0.06),
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 14.0),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding:const EdgeInsets.symmetric(vertical: 8,horizontal: 6),
                              decoration:  BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  gradient:const LinearGradient(
                                  begin: Alignment(0.49999999999999994,
                                      -3.0616171314629196e-17),
                                  end: Alignment(
                                      0.49999999999999994, 0.9999999999999999),
                                  colors: [
                                    Color(0xff4d9ef6),
                                    Color(0xff2c70c7),
                                    Colorconstand.primaryColor
                                  ])),
                          child: Text(
                            numClass,
                            textAlign: TextAlign.center,
                            style: ThemsConstands.button_semibold_16
                                .copyWith(color: Colorconstand.neutralWhite),
                          ),
                        ),
                        const SizedBox(
                          width: 9.0,
                        ),
                        Expanded(
                          child: Text(
                            titleSubject,
                            style: ThemsConstands.headline3_semibold_20
                                .copyWith(
                                    height: 1,
                                    color: Colorconstand.exam),
                          ),
                        ),
                        // Container(
                        //   padding: const EdgeInsets.symmetric(
                        //     vertical: 6,horizontal: 14
                        //   ),
                        //   decoration: BoxDecoration(
                        //     color: Colorconstand.alertsPositive,
                        //     borderRadius: BorderRadius.circular(8),
                        //   ),
                        //   child: Text(
                        //     present.toString(),
                        //     style: ThemsConstands.headline_6_semibold_14
                        //         .copyWith(color: Colorconstand.neutralWhite),
                        //     textAlign: TextAlign.center,
                        //   ),
                        // ),
                        // const SizedBox(
                        //   width: 8,
                        // ),
                        // Container(
                        //   padding: const EdgeInsets.symmetric(
                        //     vertical: 6,horizontal: 14
                        //   ),
                        //   decoration: BoxDecoration(
                        //     color: Colorconstand.alertsDecline,
                        //     borderRadius: BorderRadius.circular(8),
                        //   ),
                        //   child: Text(
                        //     absent,
                        //     style: ThemsConstands.headline_6_semibold_14
                        //         .copyWith(color: Colorconstand.neutralWhite),
                        //     textAlign: TextAlign.center,
                        //   ),
                        // )
                        isCheckScoreEnter == 0 || isCheckScoreEnter == 2?
                        Container(
                          padding:const EdgeInsets.symmetric(vertical: 10,horizontal: 12),
                          decoration:const BoxDecoration(
                            color: Colorconstand.primaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                          ),
                          child: Row(
                            children: [
                              Text( isCheckScoreEnter == 0?"SCORE".tr():"IN_PROGRESS".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.neutralWhite),),
                              const SizedBox(width: 5,),
                              const Icon(Icons.arrow_forward,size: 22,color: Colorconstand.neutralWhite,)
                            ],
                          ),
                        ) 
                        :Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              isCheckScoreEnter ==1 && isApprove ==1?"${"APPROVED".tr()} ${"DONE".tr()}":isCheckScoreEnter ==1 && isApprove == 0?"AWAIT_APPROVE".tr(): isCheckScoreEnter == 2?"IN_PROGRESS".tr(): "NOT_YET_ENTER".tr(),
                              overflow: TextOverflow.ellipsis,
                              style: ThemsConstands.headline_6_semibold_14.copyWith(
                                  color:isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,),
                            ),
                            const SizedBox(width: 8),
                            Icon(
                              isCheckScoreEnter ==1 && isApprove ==1?Icons.check_circle:isCheckScoreEnter ==1 && isApprove == 0?Icons.access_time_filled_rounded:Icons.cancel,
                              size: 18.0,
                              color: isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,
                            ),
                          ],
                        ),
                      ],
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.symmetric(vertical: 12.0),
                    //   child: Divider(
                    //     height: 1.2,
                    //     thickness: 0.5,
                    //     color: Colorconstand.primaryColor.withOpacity(0.2),
                    //   ),
                    // ),
                    // isExamSet != 0
                    //     ? examSubject()
                    //     : examSetSubject(),
                  ],
                )),
          )),
    );
  }

  Widget examSubject() {
    return Column(children: [
      Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconTitleWidget(
                    icon: ImageAssets.current_date,
                    title: "EXAM_DAY".tr(),
                    examDate:   
                    Text(
                  isNoExam == 1?"NO_EXAM".tr(): isNoExam==2 ?"": examDate!,
                  style: ThemsConstands.headline_6_semibold_14
                      .copyWith(color: Colorconstand.lightBulma),
                ),
                    subjectColor:Colorconstand.exam),
                const SizedBox(
                  height: 8.0,
                ),
               
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconTitleWidget(
                  icon: ImageAssets.EXAM_ICON,
                  title: "SCORE_RESULT".tr(),
                  subjectColor:Colorconstand.exam, 
                  examDate:  Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      isCheckScoreEnter ==1 && isApprove ==1?"APPROVE".tr():isCheckScoreEnter ==1 && isApprove == 0?"AWAIT_APPROVE".tr(): isCheckScoreEnter == 2?"IN_PROGRESS".tr(): "NOT_YET_ENTER".tr(),
                      overflow: TextOverflow.ellipsis,
                      style: ThemsConstands.headline_6_semibold_14.copyWith(
                          color:isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,),
                    ),
                    const SizedBox(width: 8),
                    Icon(
                      Icons.access_time_filled_rounded,
                      size: 18.0,
                      color: isCheckScoreEnter ==1 && isApprove ==1?Colorconstand.alertsPositive:isCheckScoreEnter ==1 && isApprove == 0?Colorconstand.exam:Colorconstand.alertsDecline,
                    ),
                  ],
                ),
                ),
                const SizedBox(
                  height: 8.0,
                ),
               
              ],
            ),
          )
        
        ],
      )
    ]);
  }

  Widget examSetSubject() {
    return MaterialButton(
      height: 40,
      onPressed: onPressed,
      color: Colorconstand.exam,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.44),
      ),
      padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            ImageAssets.calendar_add,
            height: 18,
            color: Colorconstand.neutralWhite,
          ),
          const SizedBox(
            width: 10.0,
          ),
          Text(
            "SET_EXAM_DATE".tr(),
            style: ThemsConstands.headline_6_semibold_14
                .copyWith(color: Colorconstand.neutralWhite),
          )
        ],
      ),
    );
  }
}
