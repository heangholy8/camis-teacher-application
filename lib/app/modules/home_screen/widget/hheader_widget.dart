// ignore_for_file: must_be_immutable
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/thems/thems_constands.dart';
import '../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../tracking_location_teacher/bloc/tracking_location_bloc.dart';
import '../../tracking_location_teacher/tracking_location_teacher.dart';
import '../helper.dart';

class HHeaderWidget extends StatelessWidget {
  final String schoolName;
  final int gender;
  
  HHeaderWidget({super.key, required this.schoolName,required this.gender});
  DateTime now = DateTime.now();

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 0),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 0),
              alignment: Alignment.centerLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:[
                  Row(
                    children: [
                      InkWell(
                        // onTap: () {
                        //   try {  
                        //     print("sdfdf");
                        //     // TrackingLocation.shared.calculateDuration(startT:"13:10", endT:"13:00", statusNo: 1);
                        //     TrackingLocation.shared.statusAttendance = 0;
                        //     BlocProvider.of<TrackingLocationBloc>(context).add(const GetSchoolLocationEvent());
                        //   } catch (e) {
                        //     print("object$e");
                        //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.toString())));
                        //   }
                        // },
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colorconstand.neutralWhite.withOpacity(0.2),
                            borderRadius: const BorderRadius.all(Radius.circular(10))
                          ),
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(now.day.toString(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.neutralWhite, height: 1.3)),
                                const SizedBox(width: 8,),
                                Text(
                                  translate == "km"
                                      ? monthNamesKh[now.month]
                                      : monthNamesEn[now.month],
                                  style: ThemsConstands.headline_4_medium_18.copyWith(
                                      color: Colorconstand.neutralWhite, height: 1),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 12,),
                      Expanded(
                        child: Text(
                          schoolName.toString(),
                          style: ThemsConstands.subtitle1_regular_16.copyWith(
                              color: Colorconstand.darkBackgroundsDisabled),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BlocConsumer<ProfileUserBloc, ProfileUserState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      if (state is GetUserProfileLoading){
                        return Text("${"HELLO".tr()}......",style: ThemsConstands.headline3_semibold_20
                            .copyWith(color: Colorconstand.lightTextsRegular),);
                      }
                      else if(state is GetUserProfileLoaded){
                        var dataUser = state.profileModel!.data;
                        return Text(
                           gender == 1?"${"HELLO".tr()}${translate=="km"?"លោកគ្រូ":" Teacher"} ${translate == "km"?dataUser!.name:dataUser!.nameEn==" "?dataUser.name:dataUser.nameEn}"
                          :gender == 2?"${"HELLO".tr()}${translate=="km"?"អ្នកគ្រូ":" Teacher"} ${translate == "km"?dataUser!.name:dataUser!.nameEn==" "?dataUser.name:dataUser.nameEn}"
                              :"${"HELLO".tr()} ${translate == "km"?dataUser!.name:dataUser!.nameEn==" "?dataUser.name:dataUser.nameEn}",
                          style: ThemsConstands.headline3_semibold_20
                              .copyWith(color: Colorconstand.lightTextsRegular),
                          textAlign: TextAlign.left,overflow: TextOverflow.ellipsis,
                        );
                      }
                      else{
                        return Text("HELLO".tr(),style: ThemsConstands.headline3_semibold_20
                            .copyWith(color: Colorconstand.lightTextsRegular),);
                      }
                    },
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
      
        ],
      ),
    );
  
  }
}