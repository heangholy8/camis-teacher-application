part of 'daskboard_screen_bloc.dart';

abstract class DaskboardScreenState extends Equatable {
  const DaskboardScreenState();
  
  @override
  List<Object> get props => [];
}

class DaskboardScreenInitial extends DaskboardScreenState {}


class ScheduledDashboardLoading extends DaskboardScreenState {}

class ScheduledDashboardLoaded extends DaskboardScreenState {
  final ScheduleModel? scheduleDaskboardModel;
  const ScheduledDashboardLoaded({required this.scheduleDaskboardModel});
}

class ScheduledDashboardError extends DaskboardScreenState {}