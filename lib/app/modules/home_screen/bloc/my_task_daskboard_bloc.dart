import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../models/home_model/my_task_model.dart';
import '../../../service/api/daskboard_api/daskboard_api.dart';
part 'my_task_daskboard_event.dart';
part 'my_task_daskboard_state.dart';

class MyTaskDaskboardBloc extends Bloc<MyTaskDaskboardEvent, MyTaskDaskboardState> {
  final GetDaskboardApi daskboardApi;
  MyTaskDaskboardBloc({required this.daskboardApi}) : super(MyTaskDaskboardInitial()) {
    on<MyTaskDashboardEvent>((event, emit) async{
      emit(MyTaskDashboardLoading());
      try {
        var dataMyTaskDaskboard = await daskboardApi.getMyTaskDaskboardRequestApi(
          date: event.date,
        );
        emit(MyTaskDashboardLoaded(myTaskDaskboardModel: dataMyTaskDaskboard));
      } catch (e) {
        print(e);
        emit(MyTaskDashboardError());
      }
    });
  }
}
