import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/schedule_model/schedule_model.dart';
import 'package:equatable/equatable.dart';
import '../../../service/api/daskboard_api/daskboard_api.dart';
part 'daskboard_screen_event.dart';
part 'daskboard_screen_state.dart';

class DaskboardScreenBloc extends Bloc<DaskboardScreenEvent, DaskboardScreenState> {
  final GetDaskboardApi daskboardApi;
  DaskboardScreenBloc({required this.daskboardApi}) : super(DaskboardScreenInitial()) {
    on<ScheduledDashboardEvent>((event, emit) async{
      emit(ScheduledDashboardLoading());
      try {
        var dataScheduleDaskboard = await daskboardApi.getScheduleDaskboardRequestApi(
          date: event.date,
        );
        emit(ScheduledDashboardLoaded(scheduleDaskboardModel: dataScheduleDaskboard));
      } catch (e) {
        print(e);
        emit(ScheduledDashboardError());
      }
    });
  }
}
