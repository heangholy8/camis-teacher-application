part of 'daskboard_screen_bloc.dart';

abstract class DaskboardScreenEvent extends Equatable {
  const DaskboardScreenEvent();

  @override
  List<Object> get props => [];
}

class ScheduledDashboardEvent extends DaskboardScreenEvent {
  final String? date;
 const ScheduledDashboardEvent({required this.date,});
}
