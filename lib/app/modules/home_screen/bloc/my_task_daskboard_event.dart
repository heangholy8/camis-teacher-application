part of 'my_task_daskboard_bloc.dart';

abstract class MyTaskDaskboardEvent extends Equatable {
  const MyTaskDaskboardEvent();

  @override
  List<Object> get props => [];
}

class MyTaskDashboardEvent extends MyTaskDaskboardEvent {
  final String? date;
 const MyTaskDashboardEvent({required this.date,});
}
