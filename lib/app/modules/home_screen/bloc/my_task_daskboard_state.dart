part of 'my_task_daskboard_bloc.dart';

abstract class MyTaskDaskboardState extends Equatable {
  const MyTaskDaskboardState();
  
  @override
  List<Object> get props => [];
}

class MyTaskDaskboardInitial extends MyTaskDaskboardState {}

class MyTaskDashboardLoading extends MyTaskDaskboardState {}

class MyTaskDashboardLoaded extends MyTaskDaskboardState {
  final MyTaskDashboardModel? myTaskDaskboardModel;
  const MyTaskDashboardLoaded({required this.myTaskDaskboardModel});
}

class MyTaskDashboardError extends MyTaskDaskboardState {}
