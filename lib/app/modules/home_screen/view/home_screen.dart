// ignore_for_file: avoid_unnecessary_containers
import 'dart:async';
import 'package:camis_teacher_application/app/bloc/check_update_version/bloc/check_version_update_bloc.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/view/grading_screen.dart';
import 'package:camis_teacher_application/app/modules/report_screen/view/report_screen_v2.dart';
import 'package:camis_teacher_application/app/modules/schedule_expanded_screen/view/schedule_expanded_screen.dart';
import 'package:camis_teacher_application/app/modules/tracking_location_teacher/bloc/tracking_location_bloc.dart';
import 'package:camis_teacher_application/app/modules/tracking_location_teacher/tracking_location_teacher.dart';
import 'package:camis_teacher_application/widget/shimmer/shimmer_style.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../widget/comfirm_information/comfirm_phoneNumber.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/widge_update.dart';
import '../../../help/check_platform_device.dart';
import '../../../models/grading_model/grading_model.dart';
import '../../../routes/app_route.dart';
import '../../../service/api/approved_api/approved_api.dart';
import '../../../service/api/grading_api/post_graeding_data_api.dart';
import '../../../service/api/student_requst_permission_api/requst_permission_api.dart';
import '../../../service/api/tracking_teacher_api/tracking_teacher_api.dart';
import '../../../storages/get_storage.dart';
import '../../../storages/key_storage.dart';
import '../../../storages/user_storage.dart';
import '../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../../check_attendance_screen/view/attendance_entry_screen.dart';
import '../../grading_screen/bloc/grading_bloc.dart';
import '../../grading_screen/bloc/view_image_bloc.dart';
import '../../grading_screen/view/result_screen.dart';
import '../../primary_school_screen/widget/card_home_schedule_primary_widget.dart';
import '../../schedule_screen/view/schedule_screen.dart';
import '../../time_line/time_line.dart';
import '../bloc/daskboard_screen_bloc.dart';
import '../bloc/my_task_daskboard_bloc.dart';
import '../e.homscreen.dart';
import '../widget/button_quick_action_widget.dart';
import '../widget/card_attendance_widget.dart';
import '../widget/card_my_task_widget.dart';
import '../widget/hheader_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  
  final KeyStoragePref keyPref = KeyStoragePref();
  UserSecureStroage saveStoragePref = UserSecureStroage();
 ///=========== Update Version ==============
  //////========= Change before update success =========
  bool updateVersion = false;
  //isHasDoTask=false;
  String currentVersionIos = "2.5.0";
  String currentVersionAndroid = "2.5.0";
  ///========= Update success change in postman =========
  String releaseDateVersionIos = "28/Oct/2024";
  String releaseDateVersionAndroid = "28/Oct/2024";
///=========== Update Version ==============
  List teacherAbsentList = [];
  PageController? pagecMyclass;
  PageController? pagecNotification;
  double activeindexNotification = 0;
  double activeindexMyclass = 0;
  String? schoolName;
  bool isPrimary = false;
  String? schoolCode;
  String? phoneNumber;
  String? phoneNumberInStoreLocal;
  String? instructorName;
  int gender = 0;
  int? monthlocal;
  String? monthlocalCheck;
  int? daylocal;
  int? yearchecklocal;

  bool isLoading = false;
  String isInstructor= "";
  
  GradingData data = GradingData();
  List<StudentsData> studentData = [];

  StreamSubscription? internetconnection;
  bool isoffline = true;

  // Local Storage
  final GetStoragePref _getStoragePref = GetStoragePref();

  // Datetime && Timer
  String? formattedDate;
  DateTime now = DateTime.now();
  StreamSubscription? sub;
  bool isDisplay = true;
  bool showComfirmPhone = false;

  void loadCheckScore() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    await _getStoragePref.getInstructor.then((value){
      if(value!=null){
        setState(() {
          isInstructor = value;
        });
      }
    });
    await _getStoragePref.getScoreData.then((value){
      if(value.studentsData!=null){
          setState(() {
            data = value;
            studentData  = data.studentsData!.toList();
            print("value : ${value.studentsData!.length}");
            //isHasDoTask = true;
            var api = PostGradingApi();
              api.postListStudentSubjectExam(
                classID: data.classId!,
                subjectID: data.subjectId!,
                term: data.semester,
                examDate: "",
                type: data.type!,
                month: data.month,
                listStudentScore: studentData,
              ).then((value) {
                setState(() {
                });
                int totalScoreStudent = studentData.where((element) => element.score!=null).length;
                print("total Student : $totalScoreStudent");
                if(isInstructor=="1"&&totalScoreStudent==0){
                  ApprovedApi().postApproved(classId: data.classId, examObjectId: data.id, isApprove: 1, discription: "");
                }
                pref.remove("Data Score").then((value) => print("remove data score successfully"));
                pref.remove("Instructor").then((value) => print("remove instructor successfully"));
              });
          });
      }
    });
  }

  void getPhoneStore() async {
    final KeyStoragePref keyPref = KeyStoragePref();
    final SharedPreferences pref = await SharedPreferences.getInstance();
      phoneNumberInStoreLocal = pref.getString(keyPref.keyPhonePref) ?? "";
    if(phoneNumberInStoreLocal == ""|| phoneNumberInStoreLocal==null){
      setState(() {
        showComfirmPhone = true;
      });
    }
  }

  void getLocalData() async {
    var authStoragePref = await _getStoragePref.getJsonToken;
    setState(() {
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      schoolName = authStoragePref.schoolName.toString();
      isPrimary = authStoragePref.isPrimary!;
      schoolCode =  authStoragePref.schoolCode.toString();
      if(authStoragePref.authUser == null){
      }
      else{
        instructorName = authStoragePref.authUser!.name;
        gender = authStoragePref.authUser!.gender;
        phoneNumber =  authStoragePref.authUser!.phone.toString();
      }
      
      if(monthlocal.toString().length == 1){
        monthlocalCheck = "0$monthlocal";
      }
      else{
        monthlocalCheck = monthlocal.toString();
      }
 
      BlocProvider.of<CheckVersionUpdateBloc>(context).add(CheckVersionUpdate());
      BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: "$daylocal/$monthlocalCheck/$yearchecklocal"));
      BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"$daylocal/$monthlocalCheck/$yearchecklocal"));
      //BlocProvider.of<MyTaskDaskboardBloc>(context).add(MyTaskDashboardEvent(date:"27/03/2023"));
       
    });
  }


  @override
  void initState() {
  TrackingLocation.shared.reUseContext(context);
  TrackingLocation.shared.requestLocationPermission();
  getPhoneStore();
  getLocalData();
  PostActionTeacherApi().actionTeacherApi(feature: "HOME_SCREEN",keyFeature: "OPEN_APP");
     //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true) {
            loadCheckScore();
          }
        });
      });
      //=============Eend Check internet===============
      //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          // setState(() {
            isoffline = false;
          // });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          // setState(() {
            isoffline = true;
          // });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          // setState(() {
            isoffline = true;
          // });
        }
      });
    //=============Eend Check internet====================
    super.initState();
    if(isoffline == true){
      loadCheckScore();
    }
  }
  


  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    pagecMyclass = PageController(viewportFraction: 0.92);
    pagecNotification = PageController(viewportFraction: 0.92);
    return Scaffold(
      bottomNavigationBar: isoffline== false || updateVersion == true || showComfirmPhone == true ?Container(height: 0,):const BottomNavigateBar(
        isActive: 1,
      ),
      backgroundColor: const Color.fromARGB(255, 247, 248, 249),
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Container(
          color: Colorconstand.primaryColor,
          child: Stack(
            children: [
              Positioned(
                right: 0,
                top: 0,
                child: Container(
                  child: SvgPicture.asset("assets/images/Path_home_1.svg"),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: SafeArea(
                  bottom: false,
                  child: Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: Column(
                      children: [
                        // headerWidget(translate: translate),
                        HHeaderWidget(
                          gender: gender,
                          schoolName: schoolName.toString(),
                        ),
                        const SizedBox(height: 22,),
                        Expanded(
                          child: BlocListener<CheckVersionUpdateBloc, CheckVersionUpdateState>(
                            listener: (context, state) {
                              if(state is CheckVersionUpdateLoaded){
                                var data = state.checkUpdateModel.data;
                                var plateform = checkPlatformDevice();
                                if(plateform=="Android"){
                                  if(data!.versionAndroid.toString() == currentVersionAndroid || data.releaseDateAndroid == releaseDateVersionAndroid){
                                      setState(() {
                                        updateVersion = false;
                                      });
                                  }
                                  else{
                                      setState(() {
                                        updateVersion = true;
                                      });
                                  }
                                }
                                if(plateform=="ios"){
                                  if(data!.versionIos.toString() == currentVersionIos || data.releaseDateIos == releaseDateVersionIos){
                                      setState(() {
                                        updateVersion = false;
                                      });
                                  }
                                  else{
                                      setState(() {
                                        updateVersion = true;
                                      });
                                  }
                                }
                              }
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                color: Colorconstand.neutralWhite,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(16),
                                  topRight: Radius.circular(16),
                                ),
                              ),
                              child: Stack(
                                children: [
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    bottom: 0,
                                    child: Container(
                                      child: SvgPicture.asset("assets/images/back_daskboard_left.svg"),
                                    ),
                                  ),
                                  Positioned(
                                    left: 0,
                                    bottom: 0,
                                    child: Container(
                                      child: SvgPicture.asset("assets/images/back_daskboard_buttom.svg"),
                                    ),
                                  ),
                                  Positioned(
                                    top: 0,
                                    left: 0,
                                    right: 0,
                                    bottom: 0,
                                    child: BlurryContainer(
                                      borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16),
                                      ),
                                      padding: const EdgeInsets.all(0),
                                      blur: 35,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colorconstand.neutralWhite.withOpacity(0.5),
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(16),
                                            topRight: Radius.circular(16),
                                          ),
                                        ),
                                        child: Container(
                                          margin: const EdgeInsets.only(top: 5),   
                                          child: SingleChildScrollView(
                                            child: Container(
                                              padding: const EdgeInsets.only(top: 3),                                      
                                              child: Column(
                                                children: [
                                                  taskWidget(),
                                                  classWidget(),
                                                  quickActionWidget(),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              isoffline == false || showComfirmPhone == true
                ?Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: Container(
                    color:const Color(0x55101010),
                  ),
                ):Container(height: 0,),
              showComfirmPhone == true?WidgetComfirmPhoneNumber(
                gender: gender,
                phoneNumber:phoneNumber == "0"?"":phoneNumber.toString(),
                onComfirmSkip: (){
                  setState(() {
                    showComfirmPhone = false;
                    saveStoragePref.savePhoneNumber(keyPhoneNumber: "SKIP");
                  });
                },
                onComfirm: (){
                  setState(() {
                    showComfirmPhone = false;
                    if(phoneNumber == ""||phoneNumber==null || phoneNumber == "0"){
                    }
                    else{
                      saveStoragePref.savePhoneNumber(keyPhoneNumber:phoneNumber.toString());
                    }
                  });
                },
              ):Container(height: 0,),
              AnimatedPositioned(
                bottom: isoffline == true ? -150 : 0,
                left: 0,
                right: 0,
                duration: const Duration(milliseconds: 500),
                child: const NoConnectWidget(),
              ),
              updateVersion==false?Container(): const Positioned(
                //top: 0,left: 0,right: 0,bottom: 0,
                child: WidgetUpdate(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget quickActionWidget() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 14, bottom: 30, top: 22),
          alignment: Alignment.centerLeft,
          child: Text(
            "QUICKACTION".tr(),
            style: ThemsConstands.button_semibold_16
                .copyWith(color: Colorconstand.lightBlack, height: 1),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
            left: 14,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                      Navigator.push(context,
                        PageRouteBuilder(
                          pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 1,role: "2",isPrinciple: false,),
                          transitionDuration: Duration.zero,
                          reverseTransitionDuration: Duration.zero,
                        ),
                      );
                    },
                    namebutton: "TIMELINE".tr(),
                    imageIcon: ImageAssets.timeline_icon,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: () {
                      Navigator.pushNamed(context,Routes.STUDENTLISTSCREEN);
                    },
                    namebutton: "LEDGERSTUDENT".tr(),
                    imageIcon: ImageAssets.profile_2users,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                      Navigator.push(context,
                        PageRouteBuilder(
                          pageBuilder: (context, animation1, animation2) => const ReportScreenV2(),
                          transitionDuration: Duration.zero,
                          reverseTransitionDuration: Duration.zero,
                        ),
                      );
                    },
                    namebutton: "REPORT".tr(),
                    imageIcon: ImageAssets.folder_icon,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget classWidget() {
    final translate = context.locale.toString();
    return BlocBuilder<DaskboardScreenBloc, DaskboardScreenState>(
      builder: (context, state) {
        if (state is ScheduledDashboardLoading) {
          return const ShimmerDaskBoardService();
        } else if (state is ScheduledDashboardLoaded) {
          var data = state.scheduleDaskboardModel!.data;
  
          return data!.isEmpty
            ? Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(left: 14,top: 22),
                  child: Text("MYCLASSE".tr(),
                    style: ThemsConstands.button_semibold_16.copyWith(
                        color: Colorconstand.lightBlack, height: 1),
                  ),
                ),
                const SizedBox(height: 12,),
                EmptyWidget(
                  title: "HOLIDAY".tr(),
                  subtitle: "".tr(),
                ),
              ],
            ) 
            :Column(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 14, top: 25, bottom: 22),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                        left: 14,
                      ),
                      child: Text(
                        "${"MYCLASSE".tr()} (${data.length})",
                        style: ThemsConstands.button_semibold_16.copyWith(
                            color: Colorconstand.lightBlack, height: 1),
                      ),
                    ),
                    GestureDetector(
                      onTap: (() {
                        
                        Navigator.pushReplacement(
                            context, 
                            PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) => ScheduleScreen(isPrimary: isPrimary,),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                            ),
                        );
                      }),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            child: Text(
                              "SEEALL".tr(),
                              style: ThemsConstands.headline_6_semibold_14
                                  .copyWith(
                                color: Colorconstand.primaryColor,
                                height: 1,
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(bottom: 2.5),
                            child: SvgPicture.asset(
                              ImageAssets.chevron_right_icon,
                              width: 18,
                              height: 18,
                              color: Colorconstand.primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 85,
                alignment: Alignment.centerLeft,
                child: PageView.builder(
                    controller: pagecMyclass,
                    itemCount: data.length,
                    onPageChanged: (value) {
                      setState(() {
                        activeindexMyclass = value.toDouble();
                      });
                    },
                    itemBuilder: (context, indexmyclass) {
                      if(data[indexmyclass].activities!.isNotEmpty){
                        teacherAbsentList = data[indexmyclass].activities!.where((element) {
                          return element.activityType==4;
                        }).toList();
                      }
                      return InkWell(
                          onTap: () {
                          },
                          child: Container(

                      ///================= condition check primary school==========================
                            child: isPrimary == true? CardHomeScheduleCardWidget(
                              tagSubject: translate=="km"? data[indexmyclass].subjectGroupTagName:data[indexmyclass].subjectGroupTagNameEn,
                              isTeacherAbsent: teacherAbsentList.isEmpty?false:true,
                              startClass: data[indexmyclass].startTime,
                              endClass: data[indexmyclass].endTime,
                              isSubSubject: data[indexmyclass].isSubSubject,
                              subject:translate=="km"? data[indexmyclass].subjectName:data[indexmyclass].subjectNameEn,
                              grade:data[indexmyclass].className!.toString().replaceAll("ថ្នាក់ទី", "").replaceAll("Class", ""),
                              presentCount: data[indexmyclass].attendance!.present.toString(),
                              absentCount: data[indexmyclass].attendance!.absent.toString(),
                              isCheckAttendance: data[indexmyclass].attendance!.isCheckAttendance == 0?false:true,
                              onTap: () {
                                if(data[indexmyclass].attendance!.isCheckAttendance == 0){
                                  BlocProvider.of<CheckAttendanceBloc>(context).add(GetCheckStudentAttendanceEvent(data[indexmyclass].classId.toString(), data[indexmyclass].scheduleId.toString(), data[indexmyclass].date.toString(),));
                                  Navigator.push(context,
                                    PageTransition(type:PageTransitionType.bottomToTop,child: AttendanceEntryScreen(activeMySchedule: false,routFromScreen: 1,isPrimary: isPrimary,),
                                  ),);
                                }
                                else{
                                    TrackingLocation.shared.checkStaffStatus(status:data[indexmyclass].staffPresentStatus!);
                                    Navigator.push(context,
                                      PageTransition(type:PageTransitionType.bottomToTop,child: ScheduleExpandedScreen(isPrimary:isPrimary,routFromScreen: 1,data: data[indexmyclass],activeMySchedule: false, month: monthlocal.toString(), type: "1",displayCheckAttendance: true,),
                                    ),
                                  );
                                }
                              },
                            ):CardMyClass(
                              isTeacherAbsent: teacherAbsentList.isEmpty?false:true,
                              startClass: data[indexmyclass].startTime,
                              endClass: data[indexmyclass].endTime,
                              subject:translate=="km"? data[indexmyclass].subjectName:data[indexmyclass].subjectNameEn,
                              grade: data[indexmyclass].className,
                              subjectColor: data[indexmyclass].isExam == true?"0xFFEE964B":teacherAbsentList.isNotEmpty?"0xff1b1c1e":"0xff${data[indexmyclass].subjectColor}",
                              presentCount: data[indexmyclass].attendance!.present.toString(),
                              absentCount: data[indexmyclass].attendance!.absent.toString(),
                              isCheckAttendance: data[indexmyclass].attendance!.isCheckAttendance == 0 ? false:true,
                              onTap: () {
                                TrackingLocation.shared.checkStaffStatus(status:data[indexmyclass].staffPresentStatus!);
                                Navigator.push(context,
                                  PageTransition(type:PageTransitionType.bottomToTop,child: ScheduleExpandedScreen(routFromScreen: 1,data: data[indexmyclass],activeMySchedule: true, month: monthlocal.toString(), type: "1",displayCheckAttendance: true,)),
                                );
                              },
                            )
                          ),
                        );
                    }),
              ),
              data.length<=1?Container(height: 5,):Container(
                margin: const EdgeInsets.only(bottom: 5, top: 15),
                child: DotsIndicator(
                  dotsCount: data.length,
                  position: activeindexMyclass,
                  decorator: DotsDecorator(
                    color: Colorconstand.darkBordersDefault,
                    activeColor: Colorconstand.mainColorForecolor,
                    size: const Size(16.0, 6),
                    activeSize: const Size(35.0, 6.0),
                    activeShape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                  ),
                ),
              ),
            ],
          );
        } else {
          return Container(
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(left: 14,top: 22),
                  child: Text("MYCLASSE".tr(),
                    style: ThemsConstands.button_semibold_16.copyWith(
                        color: Colorconstand.lightBlack, height: 1),
                  ),
                ),
                const SizedBox(height: 12,),
                EmptyWidget(
                  title: "WE_DETECT".tr(),
                  subtitle: "WE_DETECT_DES".tr(),
                ),
              ],
            ) 
          );
        }
      },
    );
  }

  Widget taskWidget() {
    final translate = context.locale.toString();
    return BlocConsumer<MyTaskDaskboardBloc, MyTaskDaskboardState>(
      listener: (context, state) {
        if (state is MyTaskDashboardLoaded){
          var datatask = state.myTaskDaskboardModel!.data;
          if(datatask!.isNotEmpty){
             if(datatask[0].type == 4){
              BlocProvider.of<TrackingLocationBloc>(context).add(const GetSchoolLocationEvent());
             }
          }
        }
      },
        builder: (context, state) {
          if(state is MyTaskDashboardLoading){
            return Container(
              child:const ShimmerDaskBoardTask(),
            );
          }
          else if (state is MyTaskDashboardLoaded){
            var data = state.myTaskDaskboardModel!.data;
            return data!.isEmpty ? Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(left: 14,top: 22),
                    child: Text("MYTASKS".tr(),
                      style: ThemsConstands.button_semibold_16.copyWith(
                          color: Colorconstand.lightBlack, height: 1),
                    ),
                  ),
                  const SizedBox(height: 12,),
                  EmptyWidget(
                    title: "NO_TASK".tr(),
                    subtitle: "".tr(),
                  ),
                ],
              ) : Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 14, top: 18, bottom: 18),
                      child: Row (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container (
                            margin: const EdgeInsets.only(
                              left: 14,
                            ),
                            child: Text(
                              "${"MYTASKS".tr()} (${data.length})",
                              style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBlack, height: 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 190,
                      child: ListView.builder(
                        shrinkWrap: true,
                        padding: const EdgeInsets.all(0),
                        scrollDirection: Axis.horizontal,
                        itemCount: data.length ,
                        itemBuilder: (context, index) {
                          return Container(
                            margin:const EdgeInsets.only(left:10),
                            child: data[index].type==4 ?
                              CardAttendanceWidget(
                              staffPresentStatus: data[index].staffPresentStatus!.toInt(),
                              width: MediaQuery.of(context).size.width / 2,
                              title: "TIME_CHECK_PRESENCE".tr(), 
                              className: translate=="km"?data[index].name.toString().replaceAll("ថ្នាក់ទី", ""):data[index].nameEn.toString().replaceAll("ថ្នាក់ទី", "").replaceAll("Class", ""), 
                              subjectName: data[index].isSubSubject==true?translate =="km" ? data[index].subjectGroupTagName.toString() : data[index].subjectGroupTagNameEn.toString():translate =="km" ? data[index].subjectName.toString() : data[index].subjectNameEn.toString(), 
                              time: "${data[index].startTime} - ${data[index].endTime}",
                              onTap: () async{
                                final SharedPreferences pref = await SharedPreferences.getInstance();
                                TrackingLocation.shared.checkStaffStatus(status: data[index].staffPresentStatus!);
                                TrackingLocation.shared.determinePosition(
                                  redirectFrom: 1,
                                  classId: data[index].classId.toString(),
                                  subjectId: data[index].subjectId.toString(),
                                  timeTableSettingID: data[index].timeTableSettingId.toString(),
                                  rDistance: int.parse(pref.getString(keyPref.keySchoolRadius).toString()),
                                  schoolLat:  pref.getString(keyPref.keySchoolLati).toString(), 
                                  schoolLog: pref.getString(keyPref.keySchoolLogti).toString(),
                                );
                                PostActionTeacherApi().actionTeacherApi(feature: "HOME_SCREEN" ,keyFeature: "CHECK_MY_ATTENDANCE");
                              },
                            )
                            :CardMyTaskWidget(
                              countdownDays: data[index].countdownDays,
                              taskStatus: data[index].taskStatus!,
                              width:data.length==1? MediaQuery.of(context).size.width - 30 : MediaQuery.of(context).size.width/2,
                              title: data[index].type==3?"TIME_CHECK_PRESENCE".tr():"EXAM_VERSION".tr(), 
                              attendance: data[index].type==3?true:false, 
                              averageInputScore: data[index].totalStudentsScoreEnteredPercentage, 
                              className: translate=="km"?data[index].name.toString().replaceAll("ថ្នាក់ទី", ""):data[index].nameEn.toString().replaceAll("ថ្នាក់ទី", "").replaceAll("Class", ""), 
                              subjectName:data[index].isSubSubject==true?translate =="km" ? data[index].subjectGroupTagName.toString() : data[index].subjectGroupTagNameEn.toString():translate =="km"? data[index].subjectName.toString():data[index].subjectNameEn.toString(), 
                              taskAbout: "", 
                              taskStatusName:data[index].taskStatusName.toString(),
                              time: "${data[index].startTime}-${data[index].endTime}",
                              eventType: data[index].type!,
                              aboutExam: data[index].type == 1? data[index].month.toString() : data[index].type == 2?data[index].semester.toString():data[index].month.toString(),
                              onTap: () {
                                if(data[index].type==3){
                                  if(data[index].countRequestPermissions! >0){
                                    RequestPermissionApi().seenRequestPermissionApi(
                                      scheduleId: data[index].scheduleId.toString(),
                                      date: data[index].date.toString(),
                                    );
                                  }
                                  BlocProvider.of<CheckAttendanceBloc>(context).add(
                                    GetCheckStudentAttendanceEvent(data[index].classId.toString(), data[index].scheduleId.toString(), data[index].date.toString()),
                                  );
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AttendanceEntryScreen(activeMySchedule: true,isPrimary: isPrimary,),
                                    ),
                                  );
                                }
                                else{
                                  if(data[index].isCheckScoreEnter == 1){
                                    BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(idClass: data[index].classId.toString(),subjectId: data[index].subjectId.toString(),type: data[index].type.toString(),month: data[index].month.toString(),semester: data[index].semester.toString(),examDate: ""));
                                    BlocProvider.of<ViewImageBloc>(context).add(GetViewImageEvent(data[index].id!.toString()));
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> ResultScreen(isInstructor: data[index].isInstructor == 0?false:true )));
                                  }else{
                                      BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(idClass:data[index].classId.toString(), subjectId: data[index].subjectId.toString(),type: data[index].type.toString(),month: data[index].month.toString(),semester: data[index].semester.toString(),examDate: "")); 
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> GradingScreen(
                                        isPrimary: isPrimary,
                                        isInstructor:data[index].isInstructor == 0?false:true,
                                        myclass: false,
                                        redirectFormScreen: 3,
                                        ),
                                      ),
                                    );
                                  }
                                }
                              },
                            ),
                          );
                        },
                      ),
                    ),
                  ],
              );
          }
           else{
            return Container(
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: const EdgeInsets.only(left: 14,top: 22),
                      child: Text("MYTASKS".tr(),
                        style: ThemsConstands.button_semibold_16.copyWith(
                            color: Colorconstand.lightBlack, height: 1),
                      ),
                    ),
                    const SizedBox(height: 12,),
                    EmptyWidget(
                      title: "WE_DETECT".tr(),
                      subtitle: "WE_DETECT_DES".tr(),
                    ),
                  ],
                ) 
              );
          }
        },
      );
  }

  Widget emergencyNotificationWidget({required String translate}) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(
            top: 18,
            bottom: 18,
          ),
          height: 180,
          child: PageView.builder(
            controller: pagecNotification,
            scrollDirection: Axis.horizontal,
            itemCount: 4,
            onPageChanged: (index) {
              setState(() {
                activeindexNotification = index.toDouble();
              });
            },
            itemBuilder: (context, index) {
              return const CardAnounment();
            }),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 16, top: 0),
          child: DotsIndicator(
            dotsCount: 4,
            position: activeindexNotification,
            decorator: DotsDecorator(
              color: Colorconstand.neutralWhite,
              activeColor: Colorconstand.mainColorForecolor,
              size: const Size(16.0, 6),
              activeSize: const Size(35.0, 6.0),
              activeShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
            ),
          ),
        ),
      ],
    );
  }

}
