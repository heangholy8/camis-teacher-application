// import 'package:flutter/material.dart';
// import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
// import 'package:camis_teacher_application/app/models/study_affaire_model/get_class_by_level_model.dart';

// import '../../../service/api/get_current_grades_affaire_api/current_grades_affaire.dart';

// class BeerListView extends StatefulWidget {

//   @override
//   _BeerListViewState createState() => _BeerListViewState();
// }

// class _BeerListViewState extends State<BeerListView> {
//   GetCurrentGradeAffaireApi? getclassByLevelApi;
//   static const _pageSize = 20;

//   final PagingController<int, Datum> _pagingController =
//       PagingController(firstPageKey: 0);

//   @override
//   void initState() {
//     _pagingController.addPageRequestListener((pageKey) {
//       _fetchPage(pageKey);
//     });
//     super.initState();
//   }

//   Future<void> _fetchPage(int pageKey) async {
//     try {
//       final newItems = await getclassByLevelApi?.getClassStudentAffairByLevelApi(level: '7', month: '8', page: '', semester: 'SECOND_SEMESTER', type: '1');
//       final isLastPage = newItems!.data!.length < _pageSize;
//       if (isLastPage) {
//         _pagingController.appendLastPage(newItems.data!);
//       } else {
//         final nextPageKey = pageKey + newItems.data!.length;
//         _pagingController.appendPage(newItems.data!, nextPageKey);
//       }
//     } catch (error) {
//       _pagingController.error = error;
//     }
//   }

//   @override
//   Widget build(BuildContext context) => 
//       // Don't worry about displaying progress or error indicators on screen; the 
//       // package takes care of that. If you want to customize them, use the 
//       // [PagedChildBuilderDelegate] properties.
//       PagedListView<int, Datum>(
//         pagingController: _pagingController,
//         builderDelegate: PagedChildBuilderDelegate<Datum>(
//           itemBuilder: (context, item, index) => ListTile(
//             title: Text(item.subjectId.toString()),
//           ),
//         ),
//       );

//   @override
//   void dispose() {
//     _pagingController.dispose();
//     super.dispose();
//   }
// }