import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../core/resources/asset_resource.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../mixins/toast.dart';
import '../../../../models/student_list_model/list_student_in_class_model.dart';
import '../../../../models/study_affaire_model/get_current_grades/get_current_grades_affaire_model.dart';
import '../../../student_list_screen/widget/body_shimmer_widget.dart';
import '../../../student_list_screen/widget/contact_pairent_widget.dart';

class StudentListInAffireScreen extends StatefulWidget {
    DataAllGradeAffaire dataGrade;
    String classId;
    String classname;
  StudentListInAffireScreen({super.key,required this.dataGrade,required this.classname,required this.classId});

  @override
  State<StudentListInAffireScreen> createState() =>
      _StudentListInAffireScreenState();
}

class _StudentListInAffireScreenState extends State<StudentListInAffireScreen>
    with Toast {
  bool connection = true;
  StreamSubscription? sub;
  bool showDropClass = false;
  String? nameClass;
  bool? dbClick = false;
  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    setState(() {
      nameClass = widget.classname;
    });
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {
        } else {}
      });
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 10, left: 22, right: 22, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            ImageAssets.chevron_left,
                            color: Colorconstand.neutralWhite,
                            width: 32,
                            height: 32,
                          ),
                        ),
                        GestureDetector(
                            onTap: (){
                              setState(() {
                                if(showDropClass == false){
                                  setState(() {
                                    showDropClass = true;
                                  });
                                }
                                else{
                                  setState(() {
                                    showDropClass = false;
                                  });
                                }
                              });
                            },
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "LEDGERSTUDENT".tr(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                                      color: Colorconstand.neutralWhite,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  margin:const EdgeInsets.only(left: 5),
                                  alignment: Alignment.center,
                                  child: Text(
                                    nameClass.toString(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                                      color: Colorconstand.neutralWhite,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  child: Icon(showDropClass==false?Icons.arrow_drop_down_rounded:Icons.arrow_drop_up_rounded,color: Colorconstand.neutralWhite,size: 35,),
                                )
                              ],
                            ),
                          ),
                        
                        const SizedBox(
                          width: 32,
                          height: 32,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colorconstand.neutralWhite,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
                        ),
                        child: Column(
                          children: [
                            bodyWidget(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          //submitButtonWidget(context),
          showDropClass ==true? Positioned(
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    showDropClass = false;
                  });
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey.withOpacity(.5),
                  child:  Center(child: Container(),),
                ),
              ),
            ):Container(),
            AnimatedPositioned(
              duration:const Duration(milliseconds:  200),
              top:showDropClass==true? 90:90,
              left:showDropClass==true? 40:80,
              right:showDropClass==true? 40:80,
              child: AnimatedContainer(
                duration:const Duration(milliseconds:  200),
                height:showDropClass==true? 150:0,
                padding:const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colorconstand.neutralWhite,
                  borderRadius: BorderRadius.circular(20)
                ),
                margin:const EdgeInsets.only(top: 15),
                child: GridView.builder(
                  shrinkWrap: true,
                  padding:const EdgeInsets.all(.0),
                    itemCount: widget.dataGrade.classes!.length,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                    itemBuilder: (BuildContext context, int indexClass) {
                    return Container(
                      margin:const EdgeInsets.only(right: 8,left: 8),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            showDropClass = false;
                            BlocProvider.of<GetStudentInClassBloc>(context).add(GetStudentInClass(idClass: widget.dataGrade.classes![indexClass].classId.toString()));
                            nameClass = widget.dataGrade.classes![indexClass].className.toString();
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 40,width: 40,
                          decoration:const BoxDecoration(
                            color: Colorconstand.primaryColor,
                            shape: BoxShape.circle
                          ),
                          child: Container(
                            child: Text(widget.dataGrade.classes![indexClass].className.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_6_semibold_14.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                          ),
                      
                        ),
                      ),
                    );
                  }
                ),
              ),
            ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }

  Widget bodyWidget() {
    final translate = context.locale.toString();
    return BlocConsumer<GetStudentInClassBloc, GetClassState>(
      listener: (context, state) {
        if (state is GetStudentClassLoading) {
          setState(() {
            dbClick = false;
          });
        } else if (state is GetStudentInClassLoaded) {
          setState(() {
            dbClick = true;
          });
        } else {
          setState(() {
            dbClick = true;
          });
        }
      },
      builder: (context, state) {
        if (state is GetStudentClassLoading) {
          return const BodyShimmerWidget();
        }
        else if (state is GetStudentInClassLoaded) {
          var data = state.studentinclassModel!.data;
          return Expanded(
            child: Stack(
              children: [
                data!.isEmpty
                    ?  Column(
                      children: [
                        Expanded(child: Container()),
                        ScheduleEmptyWidget(
                            title: "NO_STUDENT".tr(),
                            subTitle: "EMPTHY_CONTENT_DES".tr(),
                          ),
                        Expanded(child: Container()),
                      ],
                    )
                    : ListView.separated(
                        shrinkWrap: true,
                        padding: const EdgeInsets.all(0),
                        itemCount: data.length,
                        itemBuilder: (context, indexstu) {
                          return MaterialButton(
                                  onLongPress: (){
                                    detailInfoStudentBottomSheet(context, data, indexstu,translate);
                                  },
                                  onPressed: (){
                                    detailInfoStudentBottomSheet(context, data, indexstu,translate);
                                  },
                                  // onPressed: previewClass[isSelectedClass].isInstructor == 0?null:() {
                                  //   Navigator.push(context,MaterialPageRoute(
                                  //     builder: (context) => ProfileUserScreen(
                                  //         profileMedia: data[indexstu].profileMedia!.fileShow.toString(),
                                  //         firstname: data[indexstu].firstname,
                                  //         firstnameEn: data[indexstu].firstnameEn,
                                  //         lastname: data[indexstu].lastname,
                                  //         lastnameEn: data[indexstu].lastnameEn,
                                  //         gmail: data[indexstu].email,
                                  //         gender: data[indexstu].gender == "ប្រុស" || data[indexstu].gender == "Male"?1:data[indexstu].gender == "ស្រី" || data[indexstu].gender == "Female"?2:0,
                                  //         address: data[indexstu].address,
                                  //         dob: data[indexstu].dob,
                                  //         leadership: data[indexstu].leadership,
                                  //         translate: translate,
                                  //         whereEdit: "profileStudent",
                                  //         dataStudentInfo: data[indexstu],
                                  //       ),
                                  //     ),
                                  //   );
                                  // },
                                  padding: const EdgeInsets.symmetric(  vertical: 8, horizontal: 22),
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width: 70,
                                        height: 70,
                                        child: ClipOval(
                                          child: Image.network(
                                              data[indexstu].profileMedia!.fileShow == "" ||
                                              data[indexstu] .profileMedia!.fileShow == null
                                                ? data[indexstu].profileMedia! .fileThumbnail.toString()
                                                : data[indexstu].profileMedia!.fileShow.toString(),
                                            
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: const EdgeInsets.only(left: 18, right: 0),
                                          child: Text(
                                            translate=="km" ? "${data[indexstu].name}":data[indexstu].nameEn==" "? "NONAME".tr():"${data[indexstu].nameEn}",
                                            style: ThemsConstands.headline_4_medium_18.copyWith(
                                              color: Colorconstand.lightBulma,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ) ;
                        },
                        separatorBuilder: (context, ind) {
                          return Container(
                              margin: const EdgeInsets.only(left: 100),
                              child: const Divider(
                                height: 1,
                                thickness: 0.5,
                              ));
                        },
                      ),
                
              ],
            ),
          );
        } else{
          return EmptyWidget(
                title: "WE_DETECT".tr(),
                subtitle: "WE_DETECT_DES".tr(),
              );
        }
      },
    );
  }

  Future<dynamic> yesnoAlertbox(BuildContext context, {required bool isFull}) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              isFull != true
                  ? SvgPicture.asset(
                      ImageAssets.check_icon,
                      width: 62,
                      color: const Color.fromARGB(255, 0, 185, 96),
                    )
                  : SvgPicture.asset(
                      ImageAssets.warning_icon,
                      width: 62,
                      color: Colorconstand.primaryColor,
                    ),
            ],
          ),
          content: Text(
            isFull == true ? "MESSAGEMAZZER".tr() : "MODIFYMAZZER".tr(),
            textAlign: TextAlign.center,
            style: ThemsConstands.headline3_medium_20_26height
                .copyWith(height: 1.5),
          ),
          actions: <Widget>[
            TextButton(
              child: Text("OKAY".tr(),
                  style: ThemsConstands.button_semibold_16.copyWith(
                      color: isFull != true
                          ? const Color.fromARGB(255, 0, 185, 96)
                          : Colorconstand.primaryColor)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  detailInfoStudentBottomSheet( BuildContext context, List<ClassMonitorSet> datastudent, int indexstu,String translate) {
    return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        context: context,
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 16, horizontal: 28),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("STUDENTINFO".tr(),
                        style: ThemsConstands.headline6_regular_14_24height
                            .copyWith(
                          color: Colorconstand.neutralDarkGrey,
                        )),
                    Text(
                    translate=="km"?  datastudent[indexstu].name.toString():datastudent[indexstu].nameEn==" "?"No Name":datastudent[indexstu].nameEn.toString(),
                      style: ThemsConstands.headline_4_medium_18.copyWith(
                        color: Colorconstand.lightBulma,
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                height: 1,
                thickness: 0.5,
              ),
              ContactPairentWidget(
                subjectName: "",
                phone: datastudent[indexstu].guardianPhone.toString(),
                name: datastudent[indexstu].guardianName.toString(),
                onTap: datastudent[indexstu].guardianPhone == ""
                    ? () {}
                    : () {
                        Navigator.of(context).pop();
                        if (datastudent[indexstu].guardianPhone == "") {
                          showMessage(
                              context: context,
                              title: "NOPHONENUM".tr(),
                              hight: 250.0,
                              width: 25.0);
                        } else {
                          _launchLink( "tel:${datastudent[indexstu].guardianPhone}");
                        }
                      },
                profile:
                    "",
                role: "អាណាព្យាបាល",
              ),
              ContactPairentWidget(
                subjectName: "",
                phone: datastudent[indexstu].fatherPhone.toString(),
                name: datastudent[indexstu].fatherName.toString(),
                onTap: () {
                  Navigator.of(context).pop();
                  if (datastudent[indexstu].fatherPhone == "") {
                    showMessage(
                        context: context,
                        title: "NOPHONENUM".tr(),
                        hight: 250.0,
                        width: 25.0);
                  } else {
                    _launchLink("tel:${datastudent[indexstu].fatherPhone}");
                  }
                },
                profile: "",
                role: "ឪពុក",
              ),
              ContactPairentWidget(
                subjectName: "",
                phone: datastudent[indexstu].motherPhone.toString(),
                name: datastudent[indexstu].motherName.toString(),
                onTap: () {
                  Navigator.of(context).pop();
                  if (datastudent[indexstu].motherPhone == "") {
                    showMessage(
                        context: context,
                        title: "NOPHONENUM".tr(),
                        hight: 250.0,
                        width: 25.0);
                  } else {
                    _launchLink("tel:${datastudent[indexstu].motherPhone}");
                  }
                },
                profile: "",
                role: "ម្ដាយ",
              ),
            ],
          );
        });
  }
}
