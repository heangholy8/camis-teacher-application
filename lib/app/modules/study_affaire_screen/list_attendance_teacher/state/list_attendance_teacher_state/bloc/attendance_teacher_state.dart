part of 'attendance_teacher_bloc.dart';

class AttendanceTeacherState extends Equatable {
  const AttendanceTeacherState();
  
  @override
  List<Object> get props => [];
}

class AttendanceTeacherInitial extends AttendanceTeacherState {}

class  ListAttendanceTeacherLoading extends AttendanceTeacherState {}

class  ListAttendanceTeacherLoaded extends AttendanceTeacherState {
  final AttendanceTeacherListModel? attendanceTeacherListModel;
  const  ListAttendanceTeacherLoaded({required this.attendanceTeacherListModel});
}

class  ListAttendanceTeacherByPrincipleLoaded extends AttendanceTeacherState {
  final AttendanceTeacherListModel? attendanceTeacherListModel;
  const  ListAttendanceTeacherByPrincipleLoaded({required this.attendanceTeacherListModel});
}

class  ListAttendanceTeacherError extends AttendanceTeacherState {}
