import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../models/study_affaire_model/get_daily_attendance/get_daily_attendance_teacher_model.dart';
import '../../../../../../service/api/teacher_attendance_api/get_teacher_attendance_for_study_affair_api.dart';

part 'attendance_teacher_event.dart';
part 'attendance_teacher_state.dart';

class AttendanceTeacherBloc extends Bloc<AttendanceTeacherEvent, AttendanceTeacherState> {
  final GetTeacherAttendanceForStudyAffairApi getTeacherAttendanceForStudyAffairApi;
  AttendanceTeacherBloc({required this.getTeacherAttendanceForStudyAffairApi}) : super(AttendanceTeacherInitial()) {
    on<ListAttendanceTeacherEvent>((event, emit) async{
      emit(ListAttendanceTeacherLoading());
      try {
        var data = await getTeacherAttendanceForStudyAffairApi.getTeacherAttendanceApi(date: event.date);
        emit(ListAttendanceTeacherLoaded(attendanceTeacherListModel: data));
      } catch (e) {
        print(e);
        emit(ListAttendanceTeacherError());
      }
    });
    on<ListAttendanceTeacherByPrincipleEvent>((event, emit) async{
      emit(ListAttendanceTeacherLoading());
      try {
        var data = await getTeacherAttendanceForStudyAffairApi.getTeacherAttendanceByPrincipleApi(date: event.date);
        emit(ListAttendanceTeacherByPrincipleLoaded(attendanceTeacherListModel: data));
      } catch (e) {
        print(e);
        emit(ListAttendanceTeacherError());
      }
    });
  }
}
