part of 'attendance_teacher_bloc.dart';

class AttendanceTeacherEvent extends Equatable {
  const AttendanceTeacherEvent();

  @override
  List<Object> get props => [];
}

class ListAttendanceTeacherEvent extends AttendanceTeacherEvent{
  final String date;
  const ListAttendanceTeacherEvent({required this.date});
}

class ListAttendanceTeacherByPrincipleEvent extends AttendanceTeacherEvent{
  final String date;
  const ListAttendanceTeacherByPrincipleEvent({required this.date});
}
