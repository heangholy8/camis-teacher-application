import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/list_attendance_teacher/widget/expand_menu_visit_widget.dart';
import 'package:camis_teacher_application/widget/navigate_bottom_bar_widget/navigate_bottom_bar_principle.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../../widget/shimmer/shimmer_style.dart';
import '../../../../service/api/principle_api/get_list_class_check_attendance/get_class_check_attendance_api.dart';
import '../../../attendance_schedule_screen/state_management/state_attendance/bloc/get_date_bloc.dart';
import '../../../attendance_schedule_screen/widgets/date_picker_screen.dart';
import '../../../student_list_screen/widget/contact_pairent_widget.dart';
import '../state/list_attendance_teacher_state/bloc/attendance_teacher_bloc.dart';

class ListAttendanceTeacherScreen extends StatefulWidget {
  bool principle;
  ListAttendanceTeacherScreen({super.key,required this.principle});

  @override
  State<ListAttendanceTeacherScreen> createState() => _ListAttendanceTeacherScreenState();
}

class _ListAttendanceTeacherScreenState extends State<ListAttendanceTeacherScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  bool isCheckClass = false;
  TabController? controller;
  String optionClassName = "LIST_TEACHER_ABSENT".tr();
  bool isInstructor = false;
  //=============== End Varible main Tap =====================


  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================

  //============= Calculate Score ===========
  //================================

  //============== Varible scedule Tap ==================
  int? monthlocalfirst;
  int? daylocalfirst;
  int? yearchecklocalfirst;
  int daySelectNoCurrentMonth = 1;
  double offset = 0.0;
  bool showListDay = false;
  String? date;
  int activeIndexDay=0;
  bool firstLoadOnlyIndexDay = true;
  ScrollController? controllerSchedule;

  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

 //============== End Varible scedule Tap ==================

    StreamSubscription? internetconnection;
    bool isoffline = true;

  @override
  void initState() {
    setState(() {

    //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

    //============= controller class change option ========
      _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      date = "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal";
    });
    monthlocalfirst = monthlocal;
    daylocalfirst = daylocal;
    yearchecklocalfirst = yearchecklocal;
    //============== Event call Data =================
    BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent(month: monthlocal.toString(),year:yearchecklocal.toString()));
    if(widget.principle == true){
      BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));
    }
    else{
      BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));
    }
    //============== End Event call Data =================
    super.initState();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
    //========= controllerSchedule offset set ========
    // controllerSchedule = ScrollController(initialScrollOffset: offset);
    // controllerSchedule!.addListener(() {
    //   setState(() {
    //     offset = controllerSchedule!.offset;
    //   });
    // });
    
    //========= controllerSchedule offset set =========
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      bottomNavigationBar: isoffline == false ||  widget.principle == false? Container(height: 0,): const BottomNavigateBarPrinciple(isActive: 2),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: WillPopScope(
                  onWillPop: () async{
                    return widget.principle == true?false:true;
                  },
                  child: Container(
                    margin:const EdgeInsets.only(top: 10),
                    child: Column(
                      children: [
                        //========== Button widget Change class==========
                        GestureDetector(
                          onTap: isInstructor == false ? null :(() {
                            setState(() {
                              _arrowAnimationController!.isCompleted
                                ? _arrowAnimationController!.reverse()
                                : _arrowAnimationController!.forward();
                              if(isCheckClass == false){
                                isCheckClass = true;
                              }
                              else{
                                isCheckClass = false;
                              }
                            });
                          }),
                          child: Container(
                            height: 45,
                            margin:const EdgeInsets.only(left: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                widget.principle == true?Container():IconButton(
                                  splashColor: Colors.transparent,
                                  highlightColor:  Colors.transparent,
                                  padding:const EdgeInsets.all(0),
                                  onPressed: (){Navigator.of(context).pop();}, 
                                  icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 22,color: Colorconstand.neutralWhite,)),
                                Container(
                                  margin:const EdgeInsets.only(left: 10),
                                  child: Text(optionClassName, style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite)),
                                ),
                                const SizedBox(width: 12,),
                                isInstructor == false ? Container() : Align(
                                  alignment: Alignment.centerRight,
                                  child: AnimatedBuilder(
                                    animation: _arrowAnimationController!,
                                    builder: (context, child) => Transform.rotate(
                                      angle: _arrowAnimation!.value,
                                      child: const Icon(
                                        Icons.arrow_drop_down,
                                        size: 28.0,
                                        color: Colorconstand.neutralWhite,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        //========== Button End widget Change class==========
                        const SizedBox(height: 10,),
                        Expanded(
                          child: Container(
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10.0),
                                  topRight: Radius.circular(10.0),
                                ),
                                color: Colorconstand.neutralWhite),
                            child: Column(
                              children: [
                                Expanded(
                                  child: BlocBuilder<GetDateBloc, GetDateState>(
                                    builder: (context, state) {
                                      if (state is GetDateListLoading) {
                                        return Container(
                                          decoration: const BoxDecoration(
                                            color: Color(0xFFF1F9FF),
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(12),
                                              topRight: Radius.circular(12),
                                            ),
                                          ),
                                          child: Column(
                                            children: [
                                              Container(
                                                color: const Color(0x622195F3),
                                                height: 165,
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                  padding: const EdgeInsets.all(0),
                                                  shrinkWrap: true,
                                                  itemBuilder: (BuildContext contex, index) {
                                                    return const ShimmerTimeTable();
                                                  },
                                                  itemCount: 5,
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      } else if (state is GetDateListLoaded) {
                                        var selectDay;
                                        var datamonth = state.listDateModel!.data;
                                        var listMonthData = datamonth!.monthData;
                                        if(datamonth.month.toString() == monthlocalfirst.toString() && yearchecklocal==yearchecklocalfirst){
                                          daySelectNoCurrentMonth = 100;
                                        }
                                    //===================== loop list ======================
                                        var listDayWeek1 = listMonthData!.where((element) {
                                          return element.index! < 7;
                                        },).toList();
                                        var listDayWeek2 = listMonthData.where((element) {
                                          return element.index! >= 7 && element.index! <= 13;
                                        },).toList();
                                        var listDayWeek3 = listMonthData.where((element) {
                                          return element.index! >= 14 && element.index! <=20;
                                        },).toList();
                                        var listDayWeek4 = listMonthData.where((element) {
                                          return element.index! >= 21 && element.index! <=27;
                                        },).toList();
                                        var listDayWeek6 = listMonthData.where((element) {
                                          return element.index! > listMonthData.length-8;
                                        },).toList();
                                        var listDayWeek5 = listMonthData.length<=35?listDayWeek6:listMonthData.where((element) {
                                            return element.index! >= 28 && element.index! <= 34;
                                          },).toList();
                                    //===================== End loop list ======================
                                        return Container(
                                          child: Column(
                                            children: [
                                              Container(
                                                width: double.maxFinite,
                                                decoration: BoxDecoration(
                                                    borderRadius:BorderRadius.circular(8.0),
                                                    color: Colorconstand.neutralSecondBackground,
                                                ),
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: [
                                                //===========  Button Show List Month ===========================
                                                    GestureDetector(
                                                      onTap:() {
                                                        setState(() {
                                                            if(showListDay==true){
                                                                showListDay =false;
                                                            }
                                                            else{
                                                              showListDay = true;
                                                            }
                                                        });
                                                      },
                                                      child:Container(
                                                        width: MediaQuery.of(context).size.width,
                                                        height: 45,
                                                          child: Row(
                                                            children: [
                                                              showListDay==false ?Container():Container(
                                                                margin:const EdgeInsets.only(left: 8),
                                                                child: IconButton(
                                                                  onPressed: (){
                                                                    setState(() {
                                                                      if(yearchecklocal == (yearchecklocalfirst! -1)){}
                                                                      else{
                                                                        setState(() {
                                                                          daylocal = 1;
                                                                          monthlocal = 1;
                                                                          daySelectNoCurrentMonth = 1;
                                                                          yearchecklocal = yearchecklocalfirst! - 1;
                                                                          showListDay = false;
                                                                          date = "01/01/$yearchecklocal";
                                                                        });
                                                                        BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocal.toString(), year: yearchecklocal.toString()));
                                                                        if(widget.principle == true){
                                                                          BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: date.toString()));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: date.toString()));
                                                                        }
                                                                      }
                                                                    });
                                                                  }, 
                                                                  icon: Icon(Icons.arrow_back_ios_new_rounded,size: 18,color: yearchecklocal == yearchecklocalfirst! - 1?Colors.transparent:Colorconstand.primaryColor,))
                                                              ),
                                                              Expanded(
                                                                child: Row(
                                                                mainAxisAlignment:MainAxisAlignment.center,
                                                                children: [
                                                                  offset > 130
                                                                      ? Container(
                                                                          height: 25,width: 25,
                                                                          margin: const EdgeInsets.only(right: 5),
                                                                          padding: const EdgeInsets.all(3),
                                                                          decoration: BoxDecoration(color: Colorconstand.primaryColor, borderRadius: BorderRadius.circular(6)),
                                                                          child: Center(
                                                                            child: Text(
                                                                              daylocal.toString(),
                                                                              style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralWhite),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : Container(),
                                                                  Text(
                                                                    checkMonth(int.parse(datamonth.month!)),
                                                                    style:
                                                                        ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),
                                                                  ),
                                                                  const SizedBox(width: 5,),
                                                                  Text(datamonth.year.toString(),
                                                                      style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor)),
                                                                  const SizedBox(
                                                                      width: 8),
                                                                showListDay==true?Container(): Icon(
                                                                      showListDay ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                      size: 24,
                                                                      color: Colorconstand.primaryColor),
                                                                                                                                        ],
                                                                                                                                      ),
                                                              ),
                                                              showListDay==false?Container():Container(
                                                                margin:const EdgeInsets.only(right: 8),
                                                                child: IconButton(
                                                                  onPressed: (){
                                                                    setState(() {
                          
                                                                      if(yearchecklocal == yearchecklocalfirst!){}
                                                                      else{
                                                                        yearchecklocal = yearchecklocalfirst;
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = daylocalfirst;
                                                                        monthlocal = monthlocalfirst;
                                                                        showListDay = false;
                                                                        BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                                        if(widget.principle == true){
                                                                          BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                                        }
                                                                        else{
                                                                          BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                                        }
                                                                      }
                                                                    });
                                                                  }, 
                                                                  icon:Icon(Icons.arrow_forward_ios_rounded,size: 18,color:yearchecklocal==yearchecklocalfirst?Colors.transparent: Colorconstand.primaryColor,))
                                                              ),
                                                            ],
                                                          ),
                                                          ),
                                                    ),
                                                //=========== End Button Show List Month ===========================
                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: Stack(
                                                  children: [
                                                    Column(
                                                      children: [
                                                        Container(
                                                          height: 0,
                                                          child: Stack(
                                                            children:List.generate(listMonthData.length,(subindex) {
                                                              if(firstLoadOnlyIndexDay==true){
                                                                if(listMonthData[subindex].isActive==true){
                                                                  activeIndexDay = listMonthData[subindex].index!;
                                                                  firstLoadOnlyIndexDay = false;
                                                                }
                                                              }
                                                                return Container();
                                                              }
                                                            )
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: RefreshIndicator(
                                                            onRefresh: () async{
                                                              if(widget.principle == true){
                                                                BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));
                                                              }
                                                              else{
                                                                BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));
                                                              }
                                                            },
                                                            child: Column(
                                                              children: [
                                                                Expanded(
                                                                  child: SingleChildScrollView(
                                                                    physics:const AlwaysScrollableScrollPhysics(),
                                                                    controller: controllerSchedule,
                                                                    child: BlocBuilder<AttendanceTeacherBloc, AttendanceTeacherState>(
                                                                      builder: (context, state) {
                                                                        if(state is ListAttendanceTeacherLoading){
                                                                          return Column(
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            children: [
                                                                              SizedBox(
                                                                                height: MediaQuery.of(context).size.height/2 - 125,
                                                                              ),
                                                                              const SizedBox(
                                                                                height: 30,width: 30,
                                                                                child:CircularProgressIndicator(color: Colorconstand.primaryColor,),
                                                                              ),
                                                                              const SizedBox(),
                                                                            ],
                                                                          );
                                                                        }
                                                                        else if(state is ListAttendanceTeacherByPrincipleLoaded){
                                                                          var dataAttendance = state.attendanceTeacherListModel!.data;
                                                                          return Container(
                                                                            margin:const EdgeInsets.only(top: 100),
                                                                            child: Container(
                                                                              child: ListView.builder(
                                                                                padding:const EdgeInsets.only(top: 16,bottom: 28,),
                                                                                physics:const ScrollPhysics(),
                                                                                shrinkWrap: true,
                                                                                itemCount: dataAttendance!.length,
                                                                                itemBuilder: (context, indexTime) {
                                                                                  return ExpandWidget(
                                                                                    isExpand: dataAttendance[indexTime].isCurrent!,
                                                                                    isClick: true,
                                                                                    colorButtonExpand: Colors.transparent,
                                                                                    titleHead: "${dataAttendance[indexTime].startTime} - ${dataAttendance[indexTime].endTime}",
                                                                                    widget: Container(
                                                                                        child: ListView.builder(
                                                                                          padding:const EdgeInsets.only(top: 12,bottom: 0,left: 12,right: 12),
                                                                                          physics:const ScrollPhysics(),
                                                                                          shrinkWrap: true,
                                                                                          itemCount: dataAttendance[indexTime].grade!.length,
                                                                                          itemBuilder: (context, indexGrade) {
                                                                                            return Column(
                                                                                              children: [
                                                                                                Container(
                                                                                                  padding:const EdgeInsets.symmetric(horizontal: 22,vertical: 5),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  color: Colorconstand.alertsAwaitingBg,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    children: [
                                                                                                      Text("TEACHER_NAME".tr(),style: ThemsConstands.headline_5_semibold_16,),
                                                                                                      Text("${"GRADED".tr()} ${dataAttendance[indexTime].grade![indexGrade].grade}",style: ThemsConstands.headline_5_semibold_16,),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                dataAttendance[indexTime].grade![indexGrade].classes!.isEmpty? 
                                                                                                 Container(
                                                                                                  margin:const EdgeInsets.symmetric(vertical: 8),
                                                                                                  child: Text("NO_CLASS".tr(),style: ThemsConstands.headline_5_semibold_16,))
                                                                                                :ListView.builder(
                                                                                                  padding:const EdgeInsets.only(top: 12,bottom: 6,left: 12,right: 12),
                                                                                                  physics:const ScrollPhysics(),
                                                                                                  shrinkWrap: true,
                                                                                                  itemCount: dataAttendance[indexTime].grade![indexGrade].classes!.length,
                                                                                                  itemBuilder: (context, indexclass) {
                                                                                                    return Container(
                                                                                                      margin:const EdgeInsets.only(bottom: 12),
                                                                                                      child: Row(
                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                        children: [
                                                                                                          Expanded(
                                                                                                            child: MaterialButton(
                                                                                                              padding:const EdgeInsets.all(0),
                                                                                                              onLongPress: widget.principle == true?() {
                                                                                                                setState(() {
                                                                                                                  dataAttendance[indexTime].isCurrent = true;
                                                                                                                  dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="ABSENT"?dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus="PRESENT": dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="PRESENT"? dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus="PERMISSION":dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus="ABSENT";
                                                                                                                  HapticFeedback.vibrate();
                                                                                                                  ScaffoldMessenger.of(context).showSnackBar(
                                                                                                                    SnackBar(backgroundColor: Colorconstand.alertsPositive,
                                                                                                                      duration: const Duration(milliseconds: 1000),
                                                                                                                      content: Text("${"ចុះវត្តមានជំនូសគ្រូ ${dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherName}".tr()} (${dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus.toString().tr()})",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.center,),
                                                                                                                    ),
                                                                                                                  );
                                                                                                                });
                                                                                                                GetClassCheckAttApi().principleCheckAttTeacherApi(
                                                                                                                  actionObject: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus.toString(),
                                                                                                                  timeTableSettingId: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].timeTableSettingId.toString(),
                                                                                                                  classId: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].id.toString(),
                                                                                                                  staffId: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherId.toString(),
                                                                                                                  subjectId: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].subjectId.toString(),
                                                                                                                  date: date.toString(),
                                                                                                                );
                                                                                                                
                                                                                                              }:(){},
                                                                                                              onPressed: () {
                                                                                                                showModalBottomSheet(
                                                                                                                  shape: const RoundedRectangleBorder(
                                                                                                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                                                                                                                  ),
                                                                                                                  context: context,
                                                                                                                  builder: (context) {
                                                                                                                    return Column(
                                                                                                                      mainAxisSize: MainAxisSize.min,
                                                                                                                      children: [
                                                                                                                        Container(
                                                                                                                          padding:const EdgeInsets.symmetric(vertical: 16, horizontal: 28),
                                                                                                                          child: Text("CONTACT".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey,)),
                                                                                                                        ),
                                                                                                                        const Divider(
                                                                                                                          height: 1,
                                                                                                                          thickness: 0.5,
                                                                                                                        ),
                                                                                                                        ContactPairentWidget(
                                                                                                                          subjectName: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].subjectName.toString(),
                                                                                                                          phone: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                                                          name: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherName.toString(),
                                                                                                                          onTap: (){
                                                                                                                             _launchLink("tel:${dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherPhone}");
                                                                                                                          },
                                                                                                                          profile: "",
                                                                                                                          role: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                                                        ),
                                                                                                                        
                                                                                                                      ],
                                                                                                                    );
                                                                                                                  }
                                                                                                                );
                                                                                                              },
                                                                                                              splashColor: Colors.transparent,
                                                                                                              highlightColor: Colors.transparent,
                                                                                                              child: Row(
                                                                                                                children: [
                                                                                                                  Container(
                                                                                                                    margin:const EdgeInsets.only(right: 8),
                                                                                                                    padding:const EdgeInsets.all(5),
                                                                                                                    decoration: BoxDecoration(
                                                                                                                      shape: BoxShape.circle,
                                                                                                                      color: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="PRESENT"? Colorconstand.primaryColor:dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="PERMISSION"?Colorconstand.mainColorForecolor:Colorconstand.neutralGrey,
                                                                                                                    ),
                                                                                                                    child: Icon(dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="PRESENT"?Icons.check_rounded:dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="ABSENT"?Icons.close:Icons.close,size: 18,color:Colorconstand.neutralWhite),
                                                                                                                  ),
                                                                                                                  Expanded(
                                                                                                                    child: Container(
                                                                                                                      child: Text("${ translate=="km"?dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherName:dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherNameEn}",style: ThemsConstands.headline_5_medium_16,),
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                ],
                                                                                                              ),
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            child: Text("${"GRADE".tr()} ${dataAttendance[indexTime].grade![indexGrade].classes![indexclass].name}",style: ThemsConstands.headline_5_medium_16,),
                                                                                                          )
                                                                                                        ],
                                                                                                      ),
                                                                                                    );
                                                                                                  },
                                                                                                )
                                                                                              ],
                                                                                            );
                                                                                          },
                                                                                        ),
                                                                                      ),
                                                                                  );
                                                                                },
                                                                              ),
                                                                            )
                                                                          );
                                                                        }
                                                                        else if(state is ListAttendanceTeacherLoaded){
                                                                          var dataAttendance = state.attendanceTeacherListModel!.data;
                                                                          return Container(
                                                                            margin:const EdgeInsets.only(top: 100),
                                                                            child: Container(
                                                                              child: ListView.builder(
                                                                                padding:const EdgeInsets.only(top: 16,bottom: 28,),
                                                                                physics:const ScrollPhysics(),
                                                                                shrinkWrap: true,
                                                                                itemCount: dataAttendance!.length,
                                                                                itemBuilder: (context, indexTime) {
                                                                                  return ExpandWidget(
                                                                                    isExpand: dataAttendance[indexTime].isCurrent!,
                                                                                    isClick: true,
                                                                                    colorButtonExpand: Colors.transparent,
                                                                                    titleHead: "${dataAttendance[indexTime].startTime} - ${dataAttendance[indexTime].endTime}",
                                                                                    widget: Column(
                                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                                      children: [
                                                                                        Container(
                                                                                          margin:const EdgeInsets.symmetric(horizontal: 18),
                                                                                          child: ListView.builder(
                                                                                            padding:const EdgeInsets.only(top: 12,bottom: 0,left: 12,right: 12),
                                                                                            physics:const ScrollPhysics(),
                                                                                            shrinkWrap: true,
                                                                                            itemCount: dataAttendance[indexTime].grade!.length,
                                                                                            itemBuilder: (context, indexGrade) {
                                                                                              return Column(
                                                                                                children: [
                                                                                                  Container(
                                                                                                    padding:const EdgeInsets.symmetric(horizontal: 22,vertical: 5),
                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                    color: Colorconstand.alertsAwaitingBg,
                                                                                                    child: Row(
                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                      children: [
                                                                                                        Text("TEACHER_NAME".tr(),style: ThemsConstands.headline_5_semibold_16,),
                                                                                                        Text("${"GRADED".tr()} ${dataAttendance[indexTime].grade![indexGrade].grade}",style: ThemsConstands.headline_5_semibold_16,),
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                  dataAttendance[indexTime].grade![indexGrade].classes!.isEmpty? 
                                                                                                   Container(
                                                                                                    margin:const EdgeInsets.symmetric(vertical: 8),
                                                                                                    child: Text("NO_CLASS".tr(),style: ThemsConstands.headline_5_semibold_16,))
                                                                                                  :ListView.builder(
                                                                                                    padding:const EdgeInsets.only(top: 12,bottom: 6,left: 12,right: 12),
                                                                                                    physics:const ScrollPhysics(),
                                                                                                    shrinkWrap: true,
                                                                                                    itemCount: dataAttendance[indexTime].grade![indexGrade].classes!.length,
                                                                                                    itemBuilder: (context, indexclass) {
                                                                                                      return MaterialButton(
                                                                                                        padding:const EdgeInsets.all(0),
                                                                                                        splashColor: Colors.transparent,
                                                                                                        highlightColor: Colors.transparent,
                                                                                                        onPressed: () {
                                                                                                          showModalBottomSheet(
                                                                                                            shape: const RoundedRectangleBorder(
                                                                                                              borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                                                                                                            ),
                                                                                                            context: context,
                                                                                                            builder: (context) {
                                                                                                              return Column(
                                                                                                                mainAxisSize: MainAxisSize.min,
                                                                                                                children: [
                                                                                                                  Container(
                                                                                                                    padding:const EdgeInsets.symmetric(vertical: 16, horizontal: 28),
                                                                                                                    child: Text("CONTACT".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey,)),
                                                                                                                  ),
                                                                                                                  const Divider(
                                                                                                                    height: 1,
                                                                                                                    thickness: 0.5,
                                                                                                                  ),
                                                                                                                  ContactPairentWidget(
                                                                                                                    subjectName: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].subjectName.toString(),
                                                                                                                    phone: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                                                    name: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherName.toString(),
                                                                                                                    onTap: (){
                                                                                                                      _launchLink("tel:${dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherPhone}");
                                                                                                                    },
                                                                                                                    profile: "",
                                                                                                                    role: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherPhone.toString(),
                                                                                                                  ),
                                                                                                                  
                                                                                                                ],
                                                                                                              );
                                                                                                            }
                                                                                                          );
                                                                                                              
                                                                                                        },
                                                                                                        child: Container(
                                                                                                          margin:const EdgeInsets.only(bottom: 12),
                                                                                                          child: Row(
                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                            children: [
                                                                                                              Expanded(
                                                                                                                child: Row(
                                                                                                                  children: [
                                                                                                                    Container(
                                                                                                                      margin:const EdgeInsets.only(right: 8),
                                                                                                                      padding:const EdgeInsets.all(5),
                                                                                                                      decoration: BoxDecoration(
                                                                                                                        shape: BoxShape.circle,
                                                                                                                        color: dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="PRESENT"? Colorconstand.primaryColor:dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="PERMISSION"?Colorconstand.mainColorForecolor:Colorconstand.neutralGrey,
                                                                                                                      ),
                                                                                                                      child: Icon(dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="PRESENT"?Icons.check_rounded:dataAttendance[indexTime].grade![indexGrade].classes![indexclass].staffPresentStatus=="ABSENT"?Icons.close:Icons.close,size: 18,color:Colorconstand.neutralWhite),
                                                                                                                    ),
                                                                                                                    Expanded(
                                                                                                                      child: Container(
                                                                                                                        child: Text("${ translate=="km"?dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherName:dataAttendance[indexTime].grade![indexGrade].classes![indexclass].teacherNameEn}",style: ThemsConstands.headline_5_medium_16,),
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ),
                                                                                                              Container(
                                                                                                                child: Text("${"GRADE".tr()} ${dataAttendance[indexTime].grade![indexGrade].classes![indexclass].name}",style: ThemsConstands.headline_5_medium_16,),
                                                                                                              )
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                      );
                                                                                                    },
                                                                                                  )
                                                                                                ],
                                                                                              );
                                                                                            },
                                                                                          ),
                                                                                        ),
                                                                                        Container(height: 1,color: Colorconstand.neutralGrey,margin: EdgeInsets.only(bottom: 18),),
                                                                                      ],
                                                                                    ),
                                                                                  );
                                                                                },
                                                                              ),
                                                                            )
                                                                          );
                                                                        } 
                                                                        else {
                                                                          return Container(
                                                                            margin:const EdgeInsets.only(top: 150),
                                                                            child: Center(
                                                                              child: EmptyWidget(
                                                                                title: "WE_DETECT".tr(),
                                                                                subtitle: "WE_DETECT_DES".tr(),
                                                                              ),
                                                                            ),
                                                                          );
                                                                        }
                                                                      },
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  padding:const EdgeInsets.only(top: 5,bottom: 10,left: 22,right: 22),
                                                                  child: Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: [
                                                                      Text("NOTED".tr(),style: ThemsConstands.headline_5_semibold_16,),
                                                                      Container(
                                                                        margin:const EdgeInsets.only(top: 5),
                                                                        child: Row(
                                                                          children: [
                                                                            Container(
                                                                              margin:const EdgeInsets.only(right: 8),
                                                                              padding:const EdgeInsets.all(3),
                                                                              decoration:const BoxDecoration(
                                                                                shape: BoxShape.circle,
                                                                                color:Colorconstand.primaryColor,
                                                                              ),
                                                                              child: const Icon(Icons.check,size: 15,color:Colorconstand.neutralWhite),
                                                                            ),
                                                                            Expanded(child: Text("${"RECORDINGATTENDANCE_DONE".tr()} (${"PRESENTLOCATION".tr()})",style: ThemsConstands.headline_6_regular_14_20height,)),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        margin:const EdgeInsets.only(top: 5),
                                                                        child: Row(
                                                                          children: [
                                                                            Container(
                                                                              margin:const EdgeInsets.only(right: 8),
                                                                              padding:const EdgeInsets.all(3),
                                                                              decoration:const BoxDecoration(
                                                                                shape: BoxShape.circle,
                                                                                color:Colorconstand.neutralGrey,
                                                                              ),
                                                                              child: const Icon(Icons.close,size: 15,color:Colorconstand.neutralWhite),
                                                                            ),
                                                                            Expanded(child: Text("NO_YET_ACTIVITY".tr(),style: ThemsConstands.headline_6_regular_14_20height,)),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                          
                                                    //======================= widget Date List =======================================
                                                    AnimatedPositioned(
                                                      top: offset>130?-110:0,
                                                          left: 0,
                                                          right: 0,
                                                          duration: const Duration(milliseconds:300),
                                                      child: Container(
                                                          decoration:
                                                              const BoxDecoration(boxShadow: <BoxShadow>[
                                                            BoxShadow(
                                                                color: Colorconstand.neutralGrey,
                                                                blurRadius: 10.0,
                                                                offset: Offset(5.0, 5.7))
                                                          ], color: Colorconstand.neutralSecondBackground),
                                                          width: MediaQuery.of(context).size.width,
                                                          height:MediaQuery.of(context).size.width>800? 205:97,
                                                          padding: const EdgeInsets.symmetric(vertical:10.0,horizontal:0.0),
                                                          child: PageView.builder(
                                                              controller: PageController(initialPage:monthlocalfirst != monthlocal || yearchecklocal != yearchecklocalfirst ?0:activeIndexDay <=6? 0:activeIndexDay >=7&&activeIndexDay <=13?1:activeIndexDay >=14&&activeIndexDay <=20?2:activeIndexDay >=21&&activeIndexDay <=27?3:activeIndexDay >=28&&activeIndexDay <=34?4:5),
                                                              scrollDirection: Axis.horizontal,
                                                              itemCount:listMonthData.length<=35?5:6,
                                                              itemBuilder: (context, indexDate) {
                                                                return GridView.builder(
                                                                  physics:const NeverScrollableScrollPhysics(),
                                                                  padding:const EdgeInsets.all(0),
                                                                  itemCount: indexDate==0?listDayWeek1.length:indexDate==1?listDayWeek2.length:indexDate==2?listDayWeek3.length:indexDate==3?listDayWeek4.length:indexDate==4?listDayWeek5.length:listDayWeek6.length,
                                                                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 7,childAspectRatio: 0.8),
                                                                  itemBuilder: (context, index) {
                                                                    if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){
                                                                      selectDay = true;
                                                                    }
                                                                    else{
                                                                      selectDay = false;
                                                                    }
                                                                    return indexDate==0? DatePickerWidget(
                                                                      disable: listDayWeek1[index].disable!,
                                                                      eveninday: false,
                                                                      selectedday:listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth?selectDay:listDayWeek1[index].isActive!,
                                                                      day:listDayWeek1[index].shortName.toString().tr(),
                                                                      weekday:listDayWeek1[index].day.toString().tr(),
                                                                      onPressed: () {
                                                                        if(listDayWeek1[index].disable==true){}
                                                                        else{
                                                                          setState(() {
                  
                                                                            if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){}
                                                                            else{
                                                                              daySelectNoCurrentMonth = 100;
                                                                            }
                                                                            daylocal = listDayWeek1[index].day;
                                                                            for (var element in listMonthData) {
                                                                              element.isActive = false;
                                                                            }
                                                                            date = listDayWeek1[index].date;
                                                                            listMonthData[listDayWeek1[index].index!.toInt()].isActive = true; 
                                                                            if(widget.principle == true){
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: listDayWeek1[index].date.toString()));
                                                                            }
                                                                            else{
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: listDayWeek1[index].date.toString()));
                                                                            }
                                                                          });
                                                                        }
                                                                      },
                                                                    ):indexDate==1? DatePickerWidget(
                                                                      disable: listDayWeek2[index].disable!,
                                                                      eveninday: false,
                                                                      selectedday: listDayWeek2[index].isActive!,
                                                                      day:listDayWeek2[index].shortName.toString().tr(),
                                                                      weekday:listDayWeek2[index].day.toString(),
                                                                      onPressed: () {
                                                                        setState(() {
                              
                                                                          daySelectNoCurrentMonth = 100;
                                                                          daylocal = listDayWeek2[index].day;
                                                                          for (var element in listMonthData) {
                                                                            element.isActive = false;
                                                                          }
                                                                          date = listDayWeek2[index].date;
                                                                          listMonthData[listDayWeek2[index].index!.toInt()].isActive = true;                                                                                                
                                                                          if(widget.principle == true){
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: listDayWeek2[index].date.toString()));
                                                                          }
                                                                          else{
                                                                            BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: listDayWeek2[index].date.toString()));
                                                                          }
                                                                    
                                                                        });
                                                                      },
                                                                    ):indexDate==2? DatePickerWidget(
                                                                      disable: listDayWeek3[index].disable!,
                                                                      eveninday: false,
                                                                      selectedday: listDayWeek3[index].isActive!,
                                                                      day:listDayWeek3[index].shortName.toString().tr(),
                                                                      weekday:listDayWeek3[index].day.toString(),
                                                                      onPressed: () {
                                                                        setState(() {
                              
                                                                          daySelectNoCurrentMonth = 100;
                                                                          daylocal = listDayWeek3[index].day;
                                                                          for (var element in listMonthData) {
                                                                            element.isActive = false;
                                                                          }
                                                                          date = listDayWeek3[index].date;
                                                                          listMonthData[listDayWeek3[index].index!.toInt()].isActive = true;  
                                                                          if(widget.principle == true){
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: listDayWeek3[index].date.toString()));
                                                                          }
                                                                          else{
                                                                            BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: listDayWeek3[index].date.toString()));
                                                                          }
                                                                    
                                                                        });
                                                                      },
                                                                    ):indexDate==3? DatePickerWidget(
                                                                      disable: listDayWeek4[index].disable!,
                                                                      eveninday: false,
                                                                      selectedday: listDayWeek4[index].isActive!,
                                                                      day:listDayWeek4[index].shortName.toString().tr(),
                                                                      weekday:listDayWeek4[index].day.toString(),
                                                                      onPressed: () {
                                                                        setState(() {
                              
                                                                          daySelectNoCurrentMonth = 100;
                                                                          daylocal = listDayWeek4[index].day;
                                                                          for (var element in listMonthData) {
                                                                            element.isActive = false;
                                                                          }
                                                                          date = listDayWeek4[index].date;
                                                                          listMonthData[listDayWeek4[index].index!.toInt()].isActive = true;                                                                                                
                                                                          if(widget.principle == true){
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: listDayWeek4[index].date.toString()));
                                                                          }
                                                                          else{
                                                                            BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: listDayWeek4[index].date.toString()));
                                                                          }
                                                                    
                                                                        });
                                                                      },
                                                                    ):indexDate==4? DatePickerWidget(
                                                                      disable: listDayWeek5[index].disable!,
                                                                      eveninday: false,
                                                                      selectedday: listDayWeek5[index].isActive!,
                                                                      day:listDayWeek5[index].shortName.toString().tr(),
                                                                      weekday:listDayWeek5[index].day.toString(),
                                                                      onPressed: () {
                                                                        setState(() {
                              
                                                                          daySelectNoCurrentMonth = 100;
                                                                          daylocal = listDayWeek5[index].day;
                                                                          for (var element in listMonthData) {
                                                                            element.isActive = false;
                                                                          }
                                                                          date = listDayWeek5[index].date;
                                                                          listMonthData[listDayWeek5[index].index!.toInt()].isActive = true;                                                                                                
                                                                          if(widget.principle == true){
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: listDayWeek5[index].date.toString()));
                                                                          }
                                                                          else{
                                                                            BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: listDayWeek5[index].date.toString()));
                                                                          }
                                                                        });
                                                                      },
                                                                    ): DatePickerWidget(
                                                                      disable: listDayWeek6[index].disable!,
                                                                      eveninday: false,
                                                                      selectedday: listDayWeek6[index].isActive!,
                                                                      day:listDayWeek6[index].shortName.toString().tr(),
                                                                      weekday:listDayWeek6[index].day.toString(),
                                                                      onPressed: () {
                                                                        setState(() {
                              
                                                                          daySelectNoCurrentMonth = 100;
                                                                          daylocal = listDayWeek6[index].day;
                                                                          for (var element in listMonthData) {
                                                                            element.isActive = false;
                                                                          }
                                                                          date = listDayWeek6[index].date;
                                                                          listMonthData[listDayWeek6[index].index!.toInt()].isActive = true;
                                                                          if(widget.principle == true){
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: listDayWeek6[index].date.toString()));
                                                                          }
                                                                          else{
                                                                            BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: listDayWeek6[index].date.toString()));
                                                                          }
                                                                        });
                                                                      },
                                                                    );
                                                                  },);
                                                              }),
                                                        ),
                                                    ),
                                                    /// ===================End widget List Day================================
                                                    /// 
                                                /// =============== Baground over when drop month =============
                                                    showListDay == false
                                                    ? Container()
                                                    : Positioned(
                                                        bottom: 0,
                                                        left: 0,
                                                        right: 0,
                                                        top: 0,
                                                        child:
                                                            GestureDetector(
                                                          onTap: (() {
                                                            setState(
                                                                () {
                                                              showListDay =
                                                                  false;
                                                            });
                                                          }),
                                                          child:
                                                              Container(
                                                            color: const Color(
                                                                0x7B9C9595),
                                                          ),
                                                        ),
                                                      ),
                                              /// =============== Baground over when drop month =============
                                              /// 
                                              /// =============== Widget List Change Month =============
                                                    AnimatedPositioned(
                                                        top: showListDay ==
                                                                false
                                                            ? wieght > 800? -400: -170
                                                            : 0,
                                                        left: 0,
                                                        right: 0,
                                                        duration: const Duration(milliseconds:300),
                                                        child:Container(
                                                          color: Colorconstand.neutralWhite,
                                                          child:
                                                              GestureDetector(
                                                            onTap:() {
                                                              setState(() {
                                                                showListDay = false;
                                                              });
                                                            },
                                                            child:Container(
                                                              margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                              child: GridView.builder(
                                                                shrinkWrap: true,
                                                                physics:const ScrollPhysics(),
                                                                padding:const EdgeInsets.all(.0),
                                                                  itemCount: 12,
                                                                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
                                                                  itemBuilder: (BuildContext context, int index) {
                                                                    return GestureDetector(
                                                                      onTap: (() {
                                                                        setState(() {
                                                                          if(monthlocal == index+1){}
                                                                          else{
                                                                            if(index+1==monthlocalfirst && yearchecklocal == yearchecklocalfirst){
                                                                              setState(() {
                                                                                daylocal = daylocalfirst;
                                                                              });
                                                                            }
                                                                            else{
                                                                              setState(() {
                                                                                daylocal = 1;
                                                                              });
                                                                            }
                                                                            daySelectNoCurrentMonth = 1;
                                                                            showListDay = false;
                                                                            monthlocal = (index+1);
                                                                            date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                            BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: (index+1).toString(), year: yearchecklocal.toString()));
                                                                            if(widget.principle == true){
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));
                                                                            }
                                                                            else{
                                                                              BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));
                                                                            }
                                                                          }
                                                                        });
                                                                      }),
                                                                      child: Container(
                                                                        margin:const EdgeInsets.all(6),
                                                                        decoration:BoxDecoration(
                                                                          borderRadius: BorderRadius.circular(12),
                                                                          color: monthlocal == (index+1)? Colorconstand.primaryColor:Colors.transparent,
                                                                        ),
                                                                        height: 20,width: 20,
                                                                        child:  Center(child: Text(((index+1) == 1 ? "JANUARY" : (index+1) == 2 ? "FEBRUARY" : (index+1) == 3 ? "MARCH" : (index+1) == 4 ? "APRIL" : (index+1) == 5 ? "MAY" : (index+1) == 6 ? "JUNE" : (index+1) == 7 ? "JULY" : (index+1) == 8 ? "AUGUST" : (index+1) == 9 ? "SEPTEMBER" : (index+1) == 10 ? "OCTOBER" : (index+1) == 11 ? "NOVEMBER" : "DECEMBER").tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthlocal==(index+1)?Colorconstand.neutralWhite:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                                      ),
                                                                    );
                                                                  },
                                                                ),
                                                            )
                                                          ),
                                                        ),
                                                      ),
                                                  /// =============== End Widget List Change Month =============
                          
                                                //======================= Icon Widget redirect to active date =======================
                                                  AnimatedPositioned(
                                                    bottom:monthlocal == monthlocalfirst && yearchecklocal == yearchecklocalfirst && daylocal == daylocalfirst || showListDay==true ? -60: 40,right: 30,
                                                    duration: const Duration(milliseconds:300),
                                                    child: Container(
                                                      height: 55,width: 55,
                                                      decoration: BoxDecoration(
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colorconstand.neutralGrey.withOpacity(0.7),
                                                            spreadRadius: 4,
                                                            blurRadius: 4,
                                                            offset:const Offset(0, 3), // changes position of shadow
                                                          ),
                                                        ],
                                                        color: Colorconstand.neutralWhite,
                                                        shape: BoxShape.circle
                                                      ),
                                                      child: MaterialButton(
                                                        elevation: 8,
                                                        onPressed: (){
                                                          setState(() {
                
                                                              yearchecklocal = yearchecklocalfirst;
                                                              daySelectNoCurrentMonth = 100;
                                                              daylocal = daylocalfirst;
                                                              monthlocal = monthlocalfirst;
                                                              showListDay = false;
                                                          });
                                                          BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                          if(widget.principle == true){
                                                            BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherByPrincipleEvent(date: "${daylocalfirst! <=9?"0$daylocalfirst":daylocalfirst}/${monthlocalfirst! <=9?"0$monthlocalfirst":monthlocalfirst}/$yearchecklocalfirst"));
                                                          }
                                                          else{
                                                            BlocProvider.of<AttendanceTeacherBloc>(context).add(ListAttendanceTeacherEvent(date: "${daylocalfirst! <=9?"0$daylocalfirst":daylocalfirst}/${monthlocalfirst! <=9?"0$monthlocalfirst":monthlocalfirst}/$yearchecklocalfirst"));
                                                          }
                                                        },
                                                        shape:const CircleBorder(),
                                                        child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                                      ),
                                                    ),
                                                  ),
                          
                                            //======================= End Icon Widget redirect to active date =======================
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      } else {
                                        return Container(
                                          margin:const EdgeInsets.only(top: 50),
                                          child: Center(
                                            child: EmptyWidget(
                                              title: "WE_DETECT".tr(),
                                              subtitle: "WE_DETECT_DES".tr(),
                                            ),
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
        //================= background when pop change class ============
        isCheckClass==true ? AnimatedPositioned(
          duration:const Duration(milliseconds: 4000),
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        child: GestureDetector(
          onTap: () {
          setState(() {
              isCheckClass = false;
              _arrowAnimationController!.isCompleted
            ? _arrowAnimationController!.reverse()
            : _arrowAnimationController!.forward();
          });
        },child: Container(color: Colors.transparent,width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,),
        )):Positioned(top: 0,child: Container(height: 0,)),
        //================= baground when pop change class ============
      

      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
          ],
        ),
      ));

  }
  Widget customBoxWidget({String? title, Color? background}) {
    return Container(
      width: 25,
      padding: const EdgeInsets.symmetric(
        vertical: 3,
      ),
      decoration: BoxDecoration(
        color: background ?? Colorconstand.alertsDecline,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        title.toString(),
        style: ThemsConstands.headline_6_semibold_14
            .copyWith(color: Colorconstand.neutralWhite),
        textAlign: TextAlign.center,
      ),
    );
  }
}
