import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar_study_affair.dart';
import '../../../../../widget/shimmer/shimmer_style.dart';
import '../../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../../attendance_schedule_screen/state_management/state_attendance/bloc/get_date_bloc.dart';
import '../../../attendance_schedule_screen/widgets/date_picker_screen.dart';
import '../../affaire_home_screen/state/daily_attendace/bloc/daily_attendance_affaire_bloc.dart';
import '../widget/daily_attendance_list_check_att_screen_affaire_widget.dart';

class ListCheckAttendanceAffaireScreen extends StatefulWidget {
  const ListCheckAttendanceAffaireScreen({super.key,});

  @override
  State<ListCheckAttendanceAffaireScreen> createState() => _ListCheckAttendanceAffaireScreenState();
}

class _ListCheckAttendanceAffaireScreenState extends State<ListCheckAttendanceAffaireScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  bool isCheckClass = false;
  TabController? controller;
  String optionClassName = "CHECKATTENDANCE".tr();
  bool isInstructor = false;
  //=============== End Varible main Tap =====================


  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================

  //============= Calculate Score ===========
  //================================

  //============== Varible scedule Tap ==================
  int? monthlocalfirst;
  int? daylocalfirst;
  int? yearchecklocalfirst;
  int daySelectNoCurrentMonth = 1;
  double offset = 0.0;
  bool showListDay = false;
  String? date;
  int activeIndexDay=0;
  bool firstLoadOnlyIndexDay = true;
  ScrollController? controllerSchedule;

 //============== End Varible scedule Tap ==================

    StreamSubscription? internetconnection;
    bool isoffline = true;

  @override
  void initState() {
    setState(() {

    //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================

    //============= controller class change option ========
      _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      date = "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal";
    });
    monthlocalfirst = monthlocal;
    daylocalfirst = daylocal;
    yearchecklocalfirst = yearchecklocal;
    //============== Event call Data =================
    BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
    BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent(month: monthlocal.toString(),year:yearchecklocal.toString()));
    //============== End Event call Data =================
    super.initState();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    final wieght = MediaQuery.of(context).size.width;
    //========= controllerSchedule offset set ========
    controllerSchedule = ScrollController(initialScrollOffset: offset);
    controllerSchedule!.addListener(() {
      setState(() {
        offset = controllerSchedule!.offset;
      });
    });
    
    //========= controllerSchedule offset set =========
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      bottomNavigationBar: isoffline == false ? Container(height: 0) : const BottomNavigateBarStudyAffair(isActive: 2),
      // bottomNavigationBar:Container(height: 0),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: Container(
                  margin:const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      //========== Button widget Change class==========
                      GestureDetector(
                        onTap: isInstructor == false ? null :(() {
                          setState(() {
                            _arrowAnimationController!.isCompleted
                              ? _arrowAnimationController!.reverse()
                              : _arrowAnimationController!.forward();
                            if(isCheckClass == false){
                              isCheckClass = true;
                            }
                            else{
                              isCheckClass = false;
                            }
                          });
                        }),
                        child: Container(
                          height: 45,
                          margin:const EdgeInsets.only(left: 18),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                child: Text(optionClassName, style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite)),
                              ),
                              const SizedBox(width: 12,),
                              isInstructor == false ? Container() : Align(
                                alignment: Alignment.centerRight,
                                child: AnimatedBuilder(
                                  animation: _arrowAnimationController!,
                                  builder: (context, child) => Transform.rotate(
                                    angle: _arrowAnimation!.value,
                                    child: const Icon(
                                      Icons.arrow_drop_down,
                                      size: 28.0,
                                      color: Colorconstand.neutralWhite,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      //========== Button End widget Change class==========
                      const SizedBox(height: 10,),
                      Expanded(
                        child: Container(
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Colorconstand.neutralWhite),
                          child: Column(
                            children: [
                              Expanded(
                                child: BlocBuilder<GetDateBloc, GetDateState>(
                                  builder: (context, state) {
                                    if (state is GetDateListLoading) {
                                      return Container(
                                        decoration: const BoxDecoration(
                                          color: Color(0xFFF1F9FF),
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(12),
                                            topRight: Radius.circular(12),
                                          ),
                                        ),
                                        child: Column(
                                          children: [
                                            Container(
                                              color: const Color(0x622195F3),
                                              height: 165,
                                            ),
                                            Expanded(
                                              child: ListView.builder(
                                                padding: const EdgeInsets.all(0),
                                                shrinkWrap: true,
                                                itemBuilder: (BuildContext contex, index) {
                                                  return const ShimmerTimeTable();
                                                },
                                                itemCount: 5,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    } else if (state is GetDateListLoaded) {
                                      var selectDay;
                                      var datamonth = state.listDateModel!.data;
                                      var listMonthData = datamonth!.monthData;
                                      if(datamonth.month.toString() == monthlocalfirst.toString() && yearchecklocal==yearchecklocalfirst){
                                        daySelectNoCurrentMonth = 100;
                                      }
                                  //===================== loop list ======================
                                      var listDayWeek1 = listMonthData!.where((element) {
                                        return element.index! < 7;
                                      },).toList();
                                      var listDayWeek2 = listMonthData.where((element) {
                                        return element.index! >= 7 && element.index! <= 13;
                                      },).toList();
                                      var listDayWeek3 = listMonthData.where((element) {
                                        return element.index! >= 14 && element.index! <=20;
                                      },).toList();
                                      var listDayWeek4 = listMonthData.where((element) {
                                        return element.index! >= 21 && element.index! <=27;
                                      },).toList();
                                      var listDayWeek6 = listMonthData.where((element) {
                                        return element.index! > listMonthData.length-8;
                                      },).toList();
                                      var listDayWeek5 = listMonthData.length<=35?listDayWeek6:listMonthData.where((element) {
                                          return element.index! >= 28 && element.index! <= 34;
                                        },).toList();
                                  //===================== End loop list ======================
                                      return Container(
                                        child: Column(
                                          children: [
                                            Container(
                                              width: double.maxFinite,
                                              decoration: BoxDecoration(
                                                  borderRadius:BorderRadius.circular(8.0),
                                                  color: Colorconstand.neutralSecondBackground,
                                              ),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                              //===========  Button Show List Month ===========================
                                                  GestureDetector(
                                                    onTap:() {
                                                      setState(() {
                                                          if(showListDay==true){
                                                              showListDay =false;
                                                          }
                                                          else{
                                                            showListDay = true;
                                                          }
                                                      });
                                                    },
                                                    child:Container(
                                                      width: MediaQuery.of(context).size.width,
                                                      height: 45,
                                                        child: Row(
                                                          children: [
                                                            showListDay==false ?Container():Container(
                                                              margin:const EdgeInsets.only(left: 8),
                                                              child: IconButton(
                                                                onPressed: (){
                                                                  setState(() {
                                                                    if(yearchecklocal == (yearchecklocalfirst! -1)){}
                                                                    else{
                                                                      setState(() {
                                                                        daylocal = 1;
                                                                        monthlocal = 1;
                                                                        daySelectNoCurrentMonth = 1;
                                                                        yearchecklocal = yearchecklocalfirst! - 1;
                                                                        showListDay = false;
                                                                        date = "01/01/$yearchecklocal";
                                                                      });
                                                                      BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocal.toString(), year: yearchecklocal.toString()));
                                                                      BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: date.toString()));
                                                                    }
                                                                  });
                                                                }, 
                                                                icon: Icon(Icons.arrow_back_ios_new_rounded,size: 18,color: yearchecklocal == yearchecklocalfirst! - 1?Colors.transparent:Colorconstand.primaryColor,))
                                                            ),
                                                            Expanded(
                                                              child: Row(
                                                              mainAxisAlignment:MainAxisAlignment.center,
                                                              children: [
                                                                offset > 130
                                                                    ? Container(
                                                                        height: 25,width: 25,
                                                                        margin: const EdgeInsets.only(right: 5),
                                                                        padding: const EdgeInsets.all(3),
                                                                        decoration: BoxDecoration(color: Colorconstand.primaryColor, borderRadius: BorderRadius.circular(6)),
                                                                        child: Center(
                                                                          child: Text(
                                                                            daylocal.toString(),
                                                                            style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralWhite),
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : Container(),
                                                                Text(
                                                                  checkMonth(int.parse(datamonth.month!)),
                                                                  style:
                                                                      ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),
                                                                ),
                                                                const SizedBox(width: 5,),
                                                                Text(datamonth.year.toString(),
                                                                    style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor)),
                                                                const SizedBox(
                                                                    width: 8),
                                                              showListDay==true?Container(): Icon(
                                                                    showListDay ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                    size: 24,
                                                                    color: Colorconstand.primaryColor),
                                                                                                                                      ],
                                                                                                                                    ),
                                                            ),
                                                            showListDay==false?Container():Container(
                                                              margin:const EdgeInsets.only(right: 8),
                                                              child: IconButton(
                                                                onPressed: (){
                                                                  setState(() {
          
                                                                    if(yearchecklocal == yearchecklocalfirst!){}
                                                                    else{
                                                                      yearchecklocal = yearchecklocalfirst;
                                                                      daySelectNoCurrentMonth = 100;
                                                                      daylocal = daylocalfirst;
                                                                      monthlocal = monthlocalfirst;
                                                                      showListDay = false;
                                                                    BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                                    BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: "$daylocal/${monthlocal! <=9?"0$monthlocal":monthlocal}/$yearchecklocal"));
                                                                    }
                                                                  });
                                                                }, 
                                                                icon:Icon(Icons.arrow_forward_ios_rounded,size: 18,color:yearchecklocal==yearchecklocalfirst?Colors.transparent: Colorconstand.primaryColor,))
                                                            ),
                                                          ],
                                                        ),
                                                        ),
                                                  ),
                                              //=========== End Button Show List Month ===========================
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Stack(
                                                children: [
                                                  Column(
                                                    children: [
                                                      Container(
                                                        height: 0,
                                                        child: Stack(
                                                          children:List.generate(listMonthData.length,(subindex) {
                                                            if(firstLoadOnlyIndexDay==true){
                                                              if(listMonthData[subindex].isActive==true){
                                                                activeIndexDay = listMonthData[subindex].index!;
                                                                firstLoadOnlyIndexDay = false;
                                                              }
                                                            }
                                                              return Container();
                                                            }
                                                          )
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: RefreshIndicator(
                                                          onRefresh: () async{
                                                            BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));                                                                                         
                                                          },
                                                          child: Column(
                                                            children: [
                                                              Expanded(
                                                                child: SingleChildScrollView(
                                                                  physics:const AlwaysScrollableScrollPhysics(),
                                                                  controller: controllerSchedule,
                                                                  child: BlocBuilder<DailyAttendanceAffaireBloc, DailyAttendanceAffaireState>(
                                                                    builder: (context, state) {
                                                                      if(state is GetDailyAttendanceAffaireLoading){
                                                                        return Container(
                                                                          decoration: const BoxDecoration(
                                                                            color: Color(0xFFF1F9FF),
                                                                            borderRadius: BorderRadius.only(
                                                                              topLeft: Radius.circular(12),
                                                                              topRight: Radius.circular(12),
                                                                            ),
                                                                          ),
                                                                          child: Column(
                                                                            children: [
                                                                              ListView.builder(
                                                                                padding: const EdgeInsets.all(0),
                                                                                shrinkWrap: true,
                                                                                itemBuilder: (BuildContext contex, index) {
                                                                                return const ShimmerTimeTable();
                                                                                },
                                                                                itemCount: 5,
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        );
                                                                      }
                                                                      else if(state is DailyAttendanceAffaireLoaded){
                                                                        var dataDailyAttendance = state.dailyAttendanceAffaireModel!.data;
                                                                        return Container(
                                                                          margin:const EdgeInsets.only(top: 100),
                                                                          child: WigetDailyListAttendanceScreenAffaire(dailyDataAttendance:dataDailyAttendance!));
                                                                      } 
                                                                      else {
                                                                        return Container(
                                                                          margin:const EdgeInsets.only(top: 150),
                                                                          child: Center(
                                                                            child: EmptyWidget(
                                                                              title: "WE_DETECT".tr(),
                                                                              subtitle: "WE_DETECT_DES".tr(),
                                                                            ),
                                                                          ),
                                                                        );
                                                                      }
                                                                    },
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                        
                                                  //======================= widget Date List =======================================
                                                  AnimatedPositioned(
                                                    top: offset>130?-110:0,
                                                        left: 0,
                                                        right: 0,
                                                        duration: const Duration(milliseconds:300),
                                                    child: Container(
                                                        decoration:
                                                            const BoxDecoration(boxShadow: <BoxShadow>[
                                                          BoxShadow(
                                                              color: Colorconstand.neutralGrey,
                                                              blurRadius: 10.0,
                                                              offset: Offset(5.0, 5.7))
                                                        ], color: Colorconstand.neutralSecondBackground),
                                                        width: MediaQuery.of(context).size.width,
                                                        height:MediaQuery.of(context).size.width>800? 205:97,
                                                        padding: const EdgeInsets.symmetric(vertical:10.0,horizontal:0.0),
                                                        child: PageView.builder(
                                                            controller: PageController(initialPage:monthlocalfirst != monthlocal || yearchecklocal != yearchecklocalfirst ?0:activeIndexDay <=6? 0:activeIndexDay >=7&&activeIndexDay <=13?1:activeIndexDay >=14&&activeIndexDay <=20?2:activeIndexDay >=21&&activeIndexDay <=27?3:activeIndexDay >=28&&activeIndexDay <=34?4:5),
                                                            scrollDirection: Axis.horizontal,
                                                            itemCount:listMonthData.length<=35?5:6,
                                                            itemBuilder: (context, indexDate) {
                                                              return GridView.builder(
                                                                physics:const NeverScrollableScrollPhysics(),
                                                                padding:const EdgeInsets.all(0),
                                                                itemCount: indexDate==0?listDayWeek1.length:indexDate==1?listDayWeek2.length:indexDate==2?listDayWeek3.length:indexDate==3?listDayWeek4.length:indexDate==4?listDayWeek5.length:listDayWeek6.length,
                                                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 7,childAspectRatio: 0.8),
                                                                itemBuilder: (context, index) {
                                                                  if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){
                                                                    selectDay = true;
                                                                  }
                                                                  else{
                                                                    selectDay = false;
                                                                  }
                                                                  return indexDate==0? DatePickerWidget(
                                                                    disable: listDayWeek1[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday:listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth?selectDay:listDayWeek1[index].isActive!,
                                                                    day:listDayWeek1[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek1[index].day.toString().tr(),
                                                                    onPressed: () {
                                                                      if(listDayWeek1[index].disable==true){}
                                                                      else{
                                                                        setState(() {
                
                                                                          if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){}
                                                                          else{
                                                                            daySelectNoCurrentMonth = 100;
                                                                          }
                                                                          daylocal = listDayWeek1[index].day;
                                                                          for (var element in listMonthData) {
                                                                            element.isActive = false;
                                                                          }
                                                                          date = listDayWeek1[index].date;
                                                                          listMonthData[listDayWeek1[index].index!.toInt()].isActive = true; 
                                                                          BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: listDayWeek1[index].date.toString()));                                                                                             
                                                                        });
                                                                      }
                                                                    },
                                                                  ):indexDate==1? DatePickerWidget(
                                                                    disable: listDayWeek2[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek2[index].isActive!,
                                                                    day:listDayWeek2[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek2[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
              
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek2[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek2[index].date;
                                                                        listMonthData[listDayWeek2[index].index!.toInt()].isActive = true;                                                                                                
                                                                        BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: listDayWeek2[index].date.toString()));                                                                                             
                                                                  
                                                                      });
                                                                    },
                                                                  ):indexDate==2? DatePickerWidget(
                                                                    disable: listDayWeek3[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek3[index].isActive!,
                                                                    day:listDayWeek3[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek3[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
              
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek3[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek3[index].date;
                                                                        listMonthData[listDayWeek3[index].index!.toInt()].isActive = true;  
                                                                        BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: listDayWeek3[index].date.toString()));                                                                                             
                                                                  
                                                                      });
                                                                    },
                                                                  ):indexDate==3? DatePickerWidget(
                                                                    disable: listDayWeek4[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek4[index].isActive!,
                                                                    day:listDayWeek4[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek4[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
              
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek4[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek4[index].date;
                                                                        listMonthData[listDayWeek4[index].index!.toInt()].isActive = true;                                                                                                
                                                                        BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: listDayWeek4[index].date.toString()));                                                                                             
                                                                  
                                                                      });
                                                                    },
                                                                  ):indexDate==4? DatePickerWidget(
                                                                    disable: listDayWeek5[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek5[index].isActive!,
                                                                    day:listDayWeek5[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek5[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
              
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek5[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek5[index].date;
                                                                        listMonthData[listDayWeek5[index].index!.toInt()].isActive = true;                                                                                                
                                                                        BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: listDayWeek5[index].date.toString()));                                                                                             
                                                                      });
                                                                    },
                                                                  ): DatePickerWidget(
                                                                    disable: listDayWeek6[index].disable!,
                                                                    eveninday: false,
                                                                    selectedday: listDayWeek6[index].isActive!,
                                                                    day:listDayWeek6[index].shortName.toString().tr(),
                                                                    weekday:listDayWeek6[index].day.toString(),
                                                                    onPressed: () {
                                                                      setState(() {
              
                                                                        daySelectNoCurrentMonth = 100;
                                                                        daylocal = listDayWeek6[index].day;
                                                                        for (var element in listMonthData) {
                                                                          element.isActive = false;
                                                                        }
                                                                        date = listDayWeek6[index].date;
                                                                        listMonthData[listDayWeek6[index].index!.toInt()].isActive = true;
                                                                        BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: listDayWeek6[index].date.toString()));                                                                                             
                                                                      });
                                                                    },
                                                                  );
                                                                },);
                                                            }),
                                                      ),
                                                  ),
                                                  /// ===================End widget List Day================================
                                                  /// 
                                                  /// =============== Baground over when drop month =============
                                                  showListDay == false
                                                  ? Container()
                                                  : Positioned(
                                                      bottom: 0,
                                                      left: 0,
                                                      right: 0,
                                                      top: 0,
                                                      child:
                                                          GestureDetector(
                                                        onTap: (() {
                                                          setState(
                                                              () {
                                                            showListDay =
                                                                false;
                                                          });
                                                        }),
                                                        child:
                                                            Container(
                                                          color: const Color(
                                                              0x7B9C9595),
                                                        ),
                                                      ),
                                                    ),
                                            /// =============== Baground over when drop month =============
                                            /// 
                                            /// =============== Widget List Change Month =============
                                                  AnimatedPositioned(
                                                      top: showListDay ==
                                                              false
                                                          ? wieght > 800? -400: -170
                                                          : 0,
                                                      left: 0,
                                                      right: 0,
                                                      duration: const Duration(milliseconds:300),
                                                      child:Container(
                                                        color: Colorconstand.neutralWhite,
                                                        child:
                                                            GestureDetector(
                                                          onTap:() {
                                                            setState(() {
                                                              showListDay = false;
                                                            });
                                                          },
                                                          child:Container(
                                                            margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                            child: GridView.builder(
                                                              shrinkWrap: true,
                                                              physics:const ScrollPhysics(),
                                                              padding:const EdgeInsets.all(.0),
                                                                itemCount: 12,
                                                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
                                                                itemBuilder: (BuildContext context, int index) {
                                                                  return GestureDetector(
                                                                    onTap: (() {
                                                                      setState(() {
                                                                        if(monthlocal == index+1){}
                                                                        else{
                                                                          if(index+1==monthlocalfirst && yearchecklocal == yearchecklocalfirst){
                                                                            setState(() {
                                                                              daylocal = daylocalfirst;
                                                                            });
                                                                          }
                                                                          else{
                                                                            setState(() {
                                                                              daylocal = 1;
                                                                            });
                                                                          }
                                                                          daySelectNoCurrentMonth = 1;
                                                                          showListDay = false;
                                                                          monthlocal = (index+1);
                                                                          date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                          BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: (index+1).toString(), year: yearchecklocal.toString()));
                                                                          BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: "${daylocal! <=9?"0$daylocal":daylocal}/${monthlocal! <=9?"0$monthlocal":monthlocal}/${yearchecklocal.toString()}"));                                                                                             
                                                                        }
                                                                      });
                                                                    }),
                                                                    child: Container(
                                                                      margin:const EdgeInsets.all(6),
                                                                      decoration:BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(12),
                                                                        color: monthlocal == (index+1)? Colorconstand.primaryColor:Colors.transparent,
                                                                      ),
                                                                      height: 20,width: 20,
                                                                      child:  Center(child: Text(((index+1) == 1 ? "JANUARY" : (index+1) == 2 ? "FEBRUARY" : (index+1) == 3 ? "MARCH" : (index+1) == 4 ? "APRIL" : (index+1) == 5 ? "MAY" : (index+1) == 6 ? "JUNE" : (index+1) == 7 ? "JULY" : (index+1) == 8 ? "AUGUST" : (index+1) == 9 ? "SEPTEMBER" : (index+1) == 10 ? "OCTOBER" : (index+1) == 11 ? "NOVEMBER" : "DECEMBER").tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthlocal==(index+1)?Colorconstand.neutralWhite:Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                                    ),
                                                                  );
                                                                },
                                                              ),
                                                          )
                                                        ),
                                                      ),
                                                    ),
                                                /// =============== End Widget List Change Month =============
                        
                                              //======================= Icon Widget redirect to active date =======================
                                                AnimatedPositioned(
                                                  bottom:monthlocal == monthlocalfirst && yearchecklocal == yearchecklocalfirst && daylocal == daylocalfirst || showListDay==true ? -60: 40,right: 30,
                                                  duration: const Duration(milliseconds:300),
                                                  child: Container(
                                                    height: 55,width: 55,
                                                    decoration: BoxDecoration(
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colorconstand.neutralGrey.withOpacity(0.7),
                                                          spreadRadius: 4,
                                                          blurRadius: 4,
                                                          offset:const Offset(0, 3), // changes position of shadow
                                                        ),
                                                      ],
                                                      color: Colorconstand.neutralWhite,
                                                      shape: BoxShape.circle
                                                    ),
                                                    child: MaterialButton(
                                                      elevation: 8,
                                                      onPressed: (){
                                                        setState(() {

                                                            yearchecklocal = yearchecklocalfirst;
                                                            daySelectNoCurrentMonth = 100;
                                                            daylocal = daylocalfirst;
                                                            monthlocal = monthlocalfirst;
                                                            showListDay = false;
                                                        });
                                                        BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                        BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: "${daylocalfirst! <=9?"0$daylocalfirst":daylocalfirst}/${monthlocalfirst! <=9?"0$monthlocalfirst":monthlocalfirst}/$yearchecklocalfirst"));                                                                                             
                                                      },
                                                      shape:const CircleBorder(),
                                                      child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                                    ),
                                                  ),
                                                ),
                        
                                          //======================= End Icon Widget redirect to active date =======================
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    } else {
                                      return Container(
                                        margin:const EdgeInsets.only(top: 50),
                                        child: Center(
                                          child: EmptyWidget(
                                            title: "WE_DETECT".tr(),
                                            subtitle: "WE_DETECT_DES".tr(),
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
        //================= background when pop change class ============
        isCheckClass==true ? AnimatedPositioned(
          duration:const Duration(milliseconds: 4000),
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        child: GestureDetector(
          onTap: () {
          setState(() {
              isCheckClass = false;
              _arrowAnimationController!.isCompleted
            ? _arrowAnimationController!.reverse()
            : _arrowAnimationController!.forward();
          });
        },child: Container(color: Colors.transparent,width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,),
        )):Positioned(top: 0,child: Container(height: 0,)),
        //================= baground when pop change class ============
      

      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
          ],
        ),
      ));

  }
  Widget customBoxWidget({String? title, Color? background}) {
    return Container(
      width: 25,
      padding: const EdgeInsets.symmetric(
        vertical: 3,
      ),
      decoration: BoxDecoration(
        color: background ?? Colorconstand.alertsDecline,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        title.toString(),
        style: ThemsConstands.headline_6_semibold_14
            .copyWith(color: Colorconstand.neutralWhite),
        textAlign: TextAlign.center,
      ),
    );
  }
}
