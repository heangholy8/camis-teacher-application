import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../models/study_affaire_model/get_daily_attendance/get_daily_attendance_model.dart';
import '../../../check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../../../check_attendance_screen/view/attendance_entry_screen.dart';

class WigetDailyListAttendanceScreenAffaire extends StatefulWidget {
  final List<DataDailyAttendanceAffaire> dailyDataAttendance;
  const WigetDailyListAttendanceScreenAffaire({required this.dailyDataAttendance,super.key});

  @override
  State<WigetDailyListAttendanceScreenAffaire> createState() => _WigetDailyListAttendanceScreenAffaireState();
}

class _WigetDailyListAttendanceScreenAffaireState extends State<WigetDailyListAttendanceScreenAffaire> {
  @override
  List listDataHolida = [];
  Widget build(BuildContext context) {
    var listData = widget.dailyDataAttendance;
    return listData.isEmpty? 
     Container(
      margin:const EdgeInsets.only(top: 50),
        child: EmptyWidget(
          title: "NO_CLASS_TODAY".tr(),
          subtitle: "CAPTION_NO_CLASS_TODAY".tr(),
        ),
      )
    :ListView.builder(
      padding:const EdgeInsets.only(top: 0,left: 0,right: 0,bottom: 50),
      physics:const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: listData.length,
      itemBuilder: (context, index) {
        return Container(
          margin: const EdgeInsets.only(top: 15),
          child: Column(
            children: [
              Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            width: 21,
                            height: 13,
                            decoration: BoxDecoration(
                              color: listData[index].isCurrent == true? Colorconstand.primaryColor:Colorconstand.neutralGrey,
                              borderRadius:const BorderRadius.only(bottomRight: Radius.circular(6),topRight: Radius.circular(6))
                            ),
                          ),
                          Container(
                            child: Text("${"TIME".tr()} ${listData[index].time.toString()}",style: ThemsConstands.headline_5_semibold_16.copyWith(color: listData[index].isCurrent == true? Colorconstand.primaryColor:Colorconstand.neutralDarkGrey),),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ),
              listData[index].grade!.isEmpty? 
              Container(
                child: EmptyWidget(
                  title: "CAPTION_GRADE".tr(),
                  subtitle: "CAPTION_NO_GRADE".tr(),
                ),
              )
              :ListView.builder(
                padding:const EdgeInsets.all(0),
                physics:const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: listData[index].grade!.length,
                itemBuilder: (context, indexgrade) {
                  return Container(
                    margin:const EdgeInsets.only(top: 15,left: 18,right: 18),
                    height: 85,
                    decoration: BoxDecoration(
                      color: Colorconstand.neutralWhite,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: const [
                        BoxShadow(
                            color: Colorconstand.neutralGrey,
                            blurRadius: 5.0,
                            offset: Offset(0.0, 3.0)
                        )
                      ],
                    ),
                    child: Row(
                      children: [
                        Container(
                          padding:const EdgeInsets.symmetric(vertical: 4),
                          width: 50,
                          decoration:const BoxDecoration(
                            color: Colorconstand.primaryColor,
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15),topLeft: Radius.circular(15))
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text("GRADED".tr().tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color:Colorconstand.neutralWhite,)),
                              ),
                              const SizedBox(height: 8,),
                              Container(
                                child: Text(listData[index].grade![indexgrade].grade.toString().tr(),style: ThemsConstands.headline_2_semibold_24.copyWith(color:Colorconstand.neutralWhite,)),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: listData[index].grade![indexgrade].classes!.isEmpty? 
                          Container(
                              child:Text(
                                "NO_CLASS".tr(),
                                style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBlack),
                                textAlign: TextAlign.center,
                              ),
                            )
                          :Container(
                            margin:const EdgeInsets.symmetric(horizontal: 8),
                            child: ListView.builder(
                              padding:const EdgeInsets.all(0),
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: listData[index].grade![indexgrade].classes!.length,
                              itemBuilder: (context, indexClass) {
                                return Container(
                                  margin:const EdgeInsets.only(right: 8),
                                  child: GestureDetector(
                                    onTap: () {
                                      BlocProvider.of<CheckAttendanceBloc>(context).add( GetCheckStudentAttendanceEvent(listData[index].grade![indexgrade].classes![indexClass].id.toString(), listData[index].grade![indexgrade].classes![indexClass].scheduleId.toString().toString(), listData[indexgrade].date.toString()));
                                      Navigator.push(context, MaterialPageRoute( builder: (context) => const AttendanceEntryScreen( routFromScreen: 6,activeMySchedule: false)));
                                    },
                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 50,width: 50,
                                      decoration: BoxDecoration(
                                        color: listData[index].grade![indexgrade].classes![indexClass].isTakeAttendance == true? Colorconstand.primaryColor:Colorconstand.alertsDecline,
                                        shape: BoxShape.circle
                                      ),
                                      child: Text(listData[index].grade![indexgrade].classes![indexClass].name.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,),
            
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        );
      }
    );
  }
}