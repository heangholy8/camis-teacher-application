part of 'list_subject_in_class_bloc.dart';

abstract class ListSubjectInClassState extends Equatable {
  const ListSubjectInClassState();
  
  @override
  List<Object> get props => [];
}

class ListSubjectInClassInitial extends ListSubjectInClassState {}

class ListsubjectLoading extends ListSubjectInClassState{}

class ListsubjectLoaded extends ListSubjectInClassState{
  final OwnSubjectModel ownsubjectModel;
  const ListsubjectLoaded({required this.ownsubjectModel});
}
class ListsubjectError extends ListSubjectInClassState{
  String? error;
  ListsubjectError({ this.error});
}