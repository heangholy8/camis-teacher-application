part of 'list_subject_in_class_bloc.dart';

abstract class ListSubjectInClassEvent extends Equatable {
  const ListSubjectInClassEvent();

  @override
  List<Object> get props => [];
}
class GetListSubjectEvent extends ListSubjectInClassEvent{
  final String classId;
  const GetListSubjectEvent({required this.classId});
}