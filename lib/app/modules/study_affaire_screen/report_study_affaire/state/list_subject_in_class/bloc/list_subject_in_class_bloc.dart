import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../models/report_model/own_subject_model.dart';
import '../../../../../../service/api/report_api/own_subject_api.dart';

part 'list_subject_in_class_event.dart';
part 'list_subject_in_class_state.dart';

class ListSubjectInClassBloc extends Bloc<ListSubjectInClassEvent, ListSubjectInClassState> {
  final OwnSubjectListApi ownSubjectListApi;
  ListSubjectInClassBloc({required this.ownSubjectListApi}) : super(ListSubjectInClassInitial()) {
    on<GetListSubjectEvent>((event, emit) async {
      emit(ListsubjectLoading());
      try {
        var data = await ownSubjectListApi.getListSubjectApi(classId: event.classId);
        emit(ListsubjectLoaded(ownsubjectModel: data));
      } catch (e) {
        print("error: $e");
        emit(ListsubjectError(error: e.toString()));
      }
    });
  }
}
