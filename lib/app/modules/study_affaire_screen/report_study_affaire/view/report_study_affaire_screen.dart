import 'dart:async';

import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/report_screen/state/attendance_print_state/bloc/attendance_print_bloc.dart';
import 'package:camis_teacher_application/widget/empydata_widget/schedule_empty.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../widget/Popup_Print_widget/view_pdf.dart';
import '../../../../bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import '../../../../models/study_affaire_model/get_current_grades/get_current_grades_affaire_model.dart';
import '../../../report_screen/state/monthly_print_state/print_bloc_bloc.dart';
import '../../../report_screen/state/own_subject_state/bloc/ownsubject_bloc.dart';
import '../../../report_screen/state/semester_print_state/bloc/semester_print_report_bloc.dart';
import '../state/list_subject_in_class/bloc/list_subject_in_class_bloc.dart';

class ReportStudeyAffaireScreen extends StatefulWidget {
    DataAllGradeAffaire dataGrade;
    String classId;
    String classname;
  ReportStudeyAffaireScreen({Key? key,required this.dataGrade,required this.classname,required this.classId}) : super(key: key);

  @override
  State<ReportStudeyAffaireScreen> createState() => _ReportStudeyAffaireScreenState();
}

class _ReportStudeyAffaireScreenState extends State<ReportStudeyAffaireScreen> with Toast{
  bool? classmonitor = false;
  String? idClass;
  String? nameClass;
  String yearAcademyName = "";
  bool connection = true;
  bool showDropClass = false;
  bool isInstructor = true;
  bool isSaveLoading = false;
  StreamSubscription? sub;
  int conditionGetStudent = 1;
  int activeClass = 0;
  String fileName = "";
  List<dynamic> attendanceMonth = [];

  @override
  void initState() {
    setState(() {
      idClass = widget.classId.toString();
      nameClass = widget.classname.toString();
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
          } else {}
        });
      });
      //=============End Check internet====================
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
        backgroundColor: Colorconstand.primaryColor,
        body: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset("assets/images/Oval.png"),
            ),
            Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
              child: SafeArea(
                bottom: false,
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10, left: 22, right: 22, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: SvgPicture.asset(
                              ImageAssets.chevron_left,
                              color: Colorconstand.neutralWhite,
                              width: 32,
                              height: 32,
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                showDropClass = !showDropClass;
                                // if(showDropClass == false){
                                //   setState(() {
                                //     showDropClass = true;
                                //   });
                                // }
                                // else{
                                //   setState(() {
                                //     showDropClass = false;
                                //   });
                                // }
                              });
                            },
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "REPORT".tr(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                                      color: Colorconstand.neutralWhite,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  margin:const EdgeInsets.only(left: 5),
                                  alignment: Alignment.center,
                                  child: Text(
                                    nameClass.toString(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                                      color: Colorconstand.neutralWhite,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  child: Icon(showDropClass==false?Icons.arrow_drop_down_rounded:Icons.arrow_drop_up_rounded,color: Colorconstand.neutralWhite,size: 35,),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 32,
                            height: 32,
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Text(
                        yearAcademyName,
                        style:const TextStyle(color: Colorconstand.neutralWhite,fontSize: 16),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          decoration: const BoxDecoration(
                            color: Colorconstand.neutralWhite,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12),
                            ),
                          ),
                          child: BlocConsumer<ListMonthSemesterBloc, ListMonthSemesterState>(
                              listener: (context, state) {
                                if(state is ListMonthSemesterLoaded){
                                  var resultData = state.monthAndSemesterList!.data;
                                  setState(() {
                                    yearAcademyName = resultData!.isNotEmpty?resultData[0].year.toString():"";
                                  });
                                }
                              },
                              builder: (context, state) {
                                if (state is OwnsubjectLoading) {
                                  return Container(
                                    alignment: Alignment.center,
                                    child: const CircularProgressIndicator());
                                }
                                if(state is ListMonthSemesterLoaded){
                                  var resultData = state.monthAndSemesterList!.data;
                                  attendanceMonth = resultData!.where((element) => element.displayMonth != "SECOND_SEMESTER" && element.displayMonth != "FIRST_SEMESTER").toList();
                                  return SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        isInstructor == true ? Column (
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Padding(
                                                padding: const EdgeInsets.only(left: 15,top: 10),
                                                child: Text(
                                                  "LEARNING_RESULT".tr(),
                                                  style: ThemsConstands
                                                    .headline_5_semibold_16
                                                    .copyWith(color: Colorconstand.darkTextsPlaceholder),
                                                ),
                                              ),
                                            ),
                                            // ShowModalBottomSheet Pint Monthly Result when success
                                            BlocListener<PrintBlocBloc, PrintBlocState>(
                                              listener: (context, state) {
                                                if(state is PrintPDFLoadingState){
                                                  setState(() {
                                                    isSaveLoading = true;
                                                  });
                                                }
                                                else if(state is PrintPDFSuccessState){
                                                  setState(() { 
                                                    isSaveLoading = false;
                                                  });
                                                  showModalBottomSheet(
                                                    isScrollControlled: true,
                                                    enableDrag:false,
                                                    isDismissible: true,
                                                    context: context, builder:(context) {
                                                    return viewPDFReportWidget(state: state.pdfBodyString,fileName: fileName,);
                                                  });
                                                }else{
                                                setState(() {
                                                  isSaveLoading=false;
                                                });
                                                }
                                              },
                                              child: Container(),
                                            ),

                                            // ShowModalBottomSheet Print semester  when success
                                            BlocListener<SemesterPrintReportBloc, SemesterPrintReportState>(
                                              listener: (context, state) {
                                                if(state is PrintReportAcademyLoading){
                                                  setState(() {
                                                    isSaveLoading = true;
                                                  });
                                                }
                                                else if(state is PrintReportSemesterExamLoaded){
                                                  setState(() { 
                                                    isSaveLoading = false;
                                                  });
                                                  showModalBottomSheet(
                                                    isScrollControlled: true,
                                                    enableDrag:false,
                                                    isDismissible: true,
                                                    context: context, builder:(context) {
                                                    return viewPDFReportWidget(state:state.body,fileName: fileName,);
                                                  });
                                                }
                                                else if(state is PrintReportSemesterResultLoaded){
                                                  setState(() { 
                                                    isSaveLoading = false;
                                                  });
                                                  showModalBottomSheet(
                                                    isScrollControlled: true,
                                                    enableDrag:false,
                                                    isDismissible: true,
                                                    context: context, builder:(context) {
                                                    return viewPDFReportWidget(state:state.bodyResultSemester,fileName: fileName,);
                                                  });
                                                }
                                                else{
                                                setState(() {
                                                  isSaveLoading=false;
                                                });
                                                }
                                              },
                                              child: Container(),
                                            ),
                                            Container(
                                              margin:const EdgeInsets.only(top: 16,left: 6),
                                              height: 160,
                                              child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: resultData.length,
                                                scrollDirection:Axis.horizontal,
                                                itemBuilder:(context, index) {
                                                  return resultData[index].displayMonth == "YEAR"?Container(): InkWell(
                                                    onTap: (){
                                                      showAlertLogout(
                                                          onSubmit: () {
                                                            setState(() {
                                                              if(resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"){
                                                                BlocProvider.of<SemesterPrintReportBloc>(context).add(GetSemeterResulEvent(classId: "$idClass",semester:"${resultData[index].displayMonth}",));
                                                                fileName = "LEARNING_RESULT".tr()+ "${resultData[index].displayMonth}".tr();
                                                              }
                                                              else{
                                                                BlocProvider.of<PrintBlocBloc>(context).add(GetResultPDFEvent(classId: "$idClass",semester:"${resultData[index].displayMonth}",month: resultData[index].month.toString(),column: "1"));
                                                              }
                                                            });
                                                            Navigator.of(context).pop();
                                                          },
                                                          title: "តើអ្នកចង់ទាញយកឯកសារមែនទេ!".tr(),
                                                          context: context,
                                                          onSubmitTitle: "DOWNLOAD".tr(),
                                                          bgColorSubmitTitle: Colorconstand.alertsDecline,
                                                          icon: const Icon(Icons.print_outlined,size: 38,color: Colorconstand.neutralWhite,),
                                                          bgColoricon:Colorconstand.mainColorSecondary
                                                      );
                                                    },
                                                    child: Stack(
                                                      children: [
                                                        Container(
                                                          margin: const EdgeInsets .symmetric(horizontal:6),
                                                          height: 150,
                                                          width: 110,
                                                          decoration: BoxDecoration(
                                                            color: resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"? Colorconstand.subject9:Colorconstand.mainColorSecondary,
                                                            borderRadius:const BorderRadius.all(
                                                                Radius.circular(15),
                                                              ),
                                                            ),
                                                        ),
                                                        Positioned(
                                                          top: 1,
                                                          right: 8,
                                                          child:SvgPicture.asset(
                                                            ImageAssets.rectagle,
                                                            color: resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"? Colorconstand.alertsPositive.withOpacity(0.4):Colorconstand.mainColorForecolor,
                                                          ),
                                                        ),
                                                        Positioned(
                                                          left: 15,
                                                          right: 15,
                                                          top: 10,
                                                          child: Row(
                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Container(
                                                                decoration: const BoxDecoration(
                                                                    gradient:
                                                                        LinearGradient(colors: [
                                                                      Color.fromRGBO(241,115,111,1),
                                                                      Color.fromRGBO(134,41,25,1)
                                                                    ]),
                                                                    borderRadius: BorderRadius.all(Radius.circular(100))),
                                                                height:
                                                                    36,
                                                                width: 36,
                                                                child:Center(
                                                                  child:Text(nameClass.toString(),
                                                                    style: ThemsConstands
                                                                        .headline6_medium_14
                                                                        .copyWith(color: Colorconstand.lightTextsRegular),
                                                                  ),
                                                                ),
                                                              ),
                                                              Text(
                                                                resultData[index].displayMonth == "FIRST_SEMESTER"?"ឆ១":resultData[index].displayMonth == "SECOND_SEMESTER"?"ឆ២": convertToKhmerNumeral(int.parse(resultData[index].month.toString())),
                                                                style: ThemsConstands
                                                                    .headline_1_semibold_32
                                                                    .copyWith(
                                                                        color: Colorconstand.mainColorUnderlayer),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Positioned(
                                                          left: 11,
                                                          right: 11,
                                                          bottom: 28.8,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Expanded(
                                                                child: Text(
                                                                  "${resultData[index].displayMonth}".tr(),
                                                                  style: ThemsConstands
                                                                      .headline3_semibold_20
                                                                      .copyWith(
                                                                          color: Colorconstand.neutralWhite),textAlign: resultData[index].displayMonth == "SECOND_SEMESTER"||resultData[index].displayMonth == "FIRST_SEMESTER"? TextAlign.center: TextAlign.left,
                                                                ),
                                                              ),
                                                              SvgPicture.asset(
                                                                  ImageAssets
                                                                      .arrow_circle)
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Padding(
                                                padding: const EdgeInsets.only(left: 15,top: 15),
                                                child: Text(
                                                  "RESULT_SEMESTER_EXAM".tr(),
                                                  style: ThemsConstands
                                                    .headline_5_semibold_16
                                                    .copyWith(color: Colorconstand.darkTextsPlaceholder),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin:const EdgeInsets.only(top: 16,left: 6),
                                              height: 160,
                                              child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: resultData.length,
                                                scrollDirection:Axis.horizontal,
                                                itemBuilder:(context, index) {
                                                  return resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"? InkWell(
                                                    onTap: (){
                                                      showAlertLogout(
                                                          onSubmit: () {
                                                            setState(() {
                                                              BlocProvider.of<SemesterPrintReportBloc>(context).add(GetSemeterExamEvent(classId: "$idClass",semester:"${resultData[index].displayMonth}",));
                                                            });
                                                            Navigator.of(context).pop();
                                                          },
                                                          title: "តើអ្នកចង់ទាញយកឯកសារមែនទេ!".tr(),
                                                          context: context,
                                                          onSubmitTitle: "DOWNLOAD".tr(),
                                                          bgColorSubmitTitle: Colorconstand.alertsDecline,
                                                          icon: const Icon(Icons.print_outlined,size: 38,color: Colorconstand.neutralWhite,),
                                                          bgColoricon:Colorconstand.mainColorSecondary
                                                      );
                                                    },
                                                    child: Stack(
                                                      children: [
                                                        Container(
                                                          margin: const EdgeInsets .symmetric(horizontal:6),
                                                          height: 150,
                                                          width: 110,
                                                          decoration: const BoxDecoration(
                                                            color: Colorconstand.subject9,
                                                            borderRadius:BorderRadius.all(
                                                                Radius.circular(15),
                                                              ),
                                                            ),
                                                        ),
                                                        Positioned(
                                                          top: 1,
                                                          right: 8,
                                                          child:SvgPicture.asset(
                                                            ImageAssets.rectagle,
                                                            color: Colorconstand.alertsPositive.withOpacity(0.4),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          left: 15,
                                                          right: 15,
                                                          top: 10,
                                                          child: Row(
                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Container(
                                                                decoration: const BoxDecoration(
                                                                    gradient:
                                                                        LinearGradient(colors: [Color.fromRGBO(241,115,111,1),
                                                                      Color.fromRGBO(134,41,25,1)
                                                                    ]),
                                                                    borderRadius: BorderRadius.all(Radius.circular(100))),
                                                                height:36,
                                                                width: 36,
                                                                child:Center(
                                                                  child:Text(
                                                                    nameClass.toString(),
                                                                    style: ThemsConstands.headline6_medium_14.copyWith(color: Colorconstand.lightTextsRegular),
                                                                  ),
                                                                ),
                                                              ),
                                                              Text(
                                                                resultData[index].displayMonth == "SECOND_SEMESTER"?"ឆ២":resultData[index].displayMonth == "FIRST_SEMESTER"?"ឆ១":convertToKhmerNumeral(int.parse(resultData[index].month.toString())),
                                                                style: ThemsConstands
                                                                    .headline_1_semibold_32
                                                                    .copyWith(
                                                                        color: Colorconstand.mainColorUnderlayer),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Positioned(
                                                          left: 11,
                                                          right: 11,
                                                          bottom: 28.8,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Expanded(
                                                                child: Text(
                                                                  "${resultData[index].displayMonth}".tr(),
                                                                  style: ThemsConstands
                                                                      .headline3_semibold_20
                                                                      .copyWith(
                                                                          color: Colorconstand.neutralWhite),textAlign: resultData[index].displayMonth == "SECOND_SEMESTER"||resultData[index].displayMonth == "FIRST_SEMESTER"? TextAlign.center: TextAlign.left,
                                                                ),
                                                              ),
                                                              SvgPicture.asset(
                                                                  ImageAssets
                                                                      .arrow_circle)
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ):Container();
                                                },
                                              ),
                                            ),
                                          ],
                                        ): Container(),

                                          // ShowModalBottomSheet Pint Monthly Result when success
                                          BlocListener<PrintBlocBloc, PrintBlocState>(
                                            listener: (context, state) {
                                              if(state is PrintPDFLoadingState){
                                                setState(() {
                                                  isSaveLoading = true;
                                                });
                                              }
                                              else if(state is PrintPDFSubjectSuccessState){
                                                setState(() { 
                                                  isSaveLoading = false;
                                                });
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  enableDrag:false,
                                                  isDismissible: true,
                                                  context: context, builder:(context) {
                                                  return viewPDFReportWidget(state:state.pdfBodySubjectResult,fileName: fileName,);
                                                });
                                              }else{
                                              setState(() {
                                                isSaveLoading=false;
                                              });
                                              }
                                            },
                                            child: Container(),
                                          ),

                                        //=========== Listioner Print Attendance ===================
                                        BlocListener<AttendancePrintBloc, AttendancePrintState>(
                                            listener: (context, state) {
                                              if(state is PrintAttendanceLoadingState){
                                                setState(() {
                                                  isSaveLoading = true;
                                                });
                                              }
                                              else if(state is PrintAttendanceMonthlySuccessState){
                                                setState(() { 
                                                  isSaveLoading = false;
                                                });
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  enableDrag:false,
                                                  isDismissible: true,
                                                  context: context, builder:(context) {
                                                  return viewPDFReportWidget(state:state.pdfAttendanceMonthly,fileName: fileName,);
                                                });
                                              }
                                              else if(state is PrintAttendanceYearlySuccessState){
                                                setState(() { 
                                                  isSaveLoading = false;
                                                });
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  enableDrag:false,
                                                  isDismissible: true,
                                                  context: context, builder:(context) {
                                                  return viewPDFReportWidget(state:state.pdfAttendanceYearly,fileName: fileName,);
                                                });
                                              }
                                              else{
                                              setState(() {
                                                isSaveLoading=false;
                                              });
                                              }
                                            },
                                            child: Container(),
                                          ),
                                          //=========== End Listioner Print Attendance ===================


                                        // Get Subject in Class
                                        BlocBuilder<ListSubjectInClassBloc, ListSubjectInClassState>(
                                          builder: (context, state) {
                                            if (state is ListsubjectLoading) {
                                              return Container(
                                                alignment: Alignment.center,
                                                margin:const EdgeInsets.only(top: 50),
                                                child: const CircularProgressIndicator());
                                            }
                                            if(state is ListsubjectLoaded){
                                              var data = state.ownsubjectModel.data;
                                              return ListView.builder(
                                                padding:const EdgeInsets.all(0),
                                                  physics: const NeverScrollableScrollPhysics(),
                                                  shrinkWrap: true,
                                                  itemCount: data?.length,
                                                  itemBuilder: (context, indexsubject) {
                                                    return data!.isEmpty || data ==[]? const ScheduleEmptyWidget(subTitle:"ទទេ", title: "មិនទាន់មានរបាយការណ៍"): 
                                                    Container(
                                                      margin:const EdgeInsets.only(top: 20),
                                                      child: Column(
                                                        children: [
                                                          Align(
                                                            alignment: Alignment.centerLeft,
                                                            child: Padding(
                                                              padding: const EdgeInsets.only(left: 15),
                                                              child: Text("${"SCORE_RESULT".tr()}-${translate=="km"?data[indexsubject].subjectName.toString():data[indexsubject].subjectNameEn.toString()}",
                                                                style: ThemsConstands.headline_5_semibold_16
                                                                    .copyWith(color: Colorconstand.darkTextsPlaceholder),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:const EdgeInsets.only(top: 16,left: 10),
                                                            height: 160,
                                                            child: ListView.builder(
                                                              shrinkWrap: true,
                                                              itemCount: resultData.length,
                                                              scrollDirection: Axis.horizontal,
                                                              itemBuilder: (context,index) {
                                                                return resultData[index].displayMonth == "YEAR"?Container(): InkWell(
                                                                  onTap: () {
                                                                    showAlertLogout(
                                                                        onSubmit: () {
                                                                          setState(() {
                                                                            BlocProvider.of<PrintBlocBloc>(context).add(GetSubjectResultPDFEvent(classId: "$idClass",term:"${resultData[index].semester}",month: resultData[index].month.toString(),istwocolumn: "2", subjectId: data[indexsubject].subjectId.toString(), type: resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"?"2":'1'));                                                                                            });
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                        title: "តើអ្នកចង់ទាញយកឯកសារមែនទេ!".tr(),
                                                                        context: context,
                                                                        onSubmitTitle: "DOWNLOAD".tr(),
                                                                        bgColorSubmitTitle: Colorconstand.alertsDecline,
                                                                        icon: const Icon(Icons.print_outlined,size: 38,color: Colorconstand.neutralWhite,),
                                                                        bgColoricon:Colorconstand.mainColorSecondary
                                                                    );
                                                                    
                                                                  },
                                                                  child: Stack(
                                                                    children: [
                                                                      Container(
                                                                        height: 150,
                                                                        width:10,
                                                                        decoration:  BoxDecoration(
                                                                            color:resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"?Colorconstand.subject7:Colorconstand.subject9,
                                                                            borderRadius:const BorderRadius.only(topLeft: Radius.circular(15), bottomLeft: Radius.circular(15))),
                                                                      ),
                                                                      Container(
                                                                        margin: const EdgeInsets.symmetric(horizontal:6),
                                                                        height: 150,
                                                                        width:105,
                                                                        decoration: BoxDecoration(
                                                                          color: resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"?Colorconstand.subject9:Colorconstand.mainColorSecondary,
                                                                          borderRadius: const BorderRadius.only(
                                                                              topRight: Radius.circular(15),
                                                                              bottomRight: Radius.circular(15),
                                                                              topLeft: Radius.circular(5),
                                                                              bottomLeft: Radius.circular(5),
                                                                            ),
                                                                          ),
                                                                      ),
                                                                      Positioned(
                                                                        top: 1,right: 8,
                                                                        child:SvgPicture.asset(
                                                                          ImageAssets.rectagle,
                                                                          color:resultData[index].displayMonth == "SECOND_SEMESTER" || resultData[index].displayMonth == "FIRST_SEMESTER"?Colorconstand.alertsPositive.withOpacity(0.4): Colorconstand.mainColorForecolor,
                                                                        ),
                                                                      ),
                                                                      Positioned(
                                                                        left: 15,right: 15,top: 10,
                                                                        child:Row(
                                                                          crossAxisAlignment:CrossAxisAlignment.center,
                                                                          mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            Container(
                                                                              decoration: BoxDecoration(color: Colorconstand.alertsAwaitingBg.withOpacity(0.2), borderRadius:const BorderRadius.all(Radius.circular(6))),
                                                                              height: 36,
                                                                              width: 36,
                                                                              child: Center(
                                                                                child: SvgPicture.asset(
                                                                                  ImageAssets.CLIPBOARDTICK_ICON,
                                                                                  height: 25,
                                                                                  width: 25,
                                                                                  color: Colors.white,
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              resultData[index].displayMonth == "SECOND_SEMESTER"?"ឆ២":resultData[index].displayMonth == "FIRST_SEMESTER"?"ឆ១" :convertToKhmerNumeral(int.parse(resultData[index].month.toString())),
                                                                              style: ThemsConstands.headline_1_semibold_32.copyWith(color: Colorconstand.mainColorUnderlayer),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Positioned(
                                                                        left:11,right:11,
                                                                        bottom:28.8,
                                                                        child:Row(mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            Expanded(
                                                                              child: Text(resultData[index].displayMonth.toString().tr(),
                                                                                style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),textAlign: resultData[index].displayMonth == "SECOND_SEMESTER"||resultData[index].displayMonth == "FIRST_SEMESTER"? TextAlign.center: TextAlign.left,
                                                                              ),
                                                                            ),
                                                                            SvgPicture.asset(ImageAssets.arrow_circle)
                                                                          ],
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                );
                                                              },
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                );
                                            }
                                            return Container();
                                          },
                                        ), 

                                        isInstructor==true? Align(
                                          alignment:
                                              Alignment.centerLeft,
                                          child: Padding(
                                            padding:
                                                const EdgeInsets
                                                    .only(left: 15,top: 8),
                                            child: Text(
                                              "REPORT_STUDENT".tr(),
                                              style: ThemsConstands
                                                  .headline_5_semibold_16
                                                  .copyWith(
                                                      color: Colorconstand
                                                          .darkTextsPlaceholder),
                                            ),
                                          ),
                                        ):Container(),
                                        isInstructor==true?const SizedBox(
                                          height: 19,
                                        ):Container(),
                                        isInstructor==true? Padding(
                                          padding:const EdgeInsets.only(left: 22.0),
                                          child: GestureDetector(
                                            onTap: (){
                                                showModalBottomSheet(
                                                  shape:const RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(18),topRight: Radius.circular(18),),
                                                  ),
                                                  isScrollControlled: true,
                                                  context: context, builder:(context) {
                                                  return StatefulBuilder(
                                                    builder: (context, snapshot) {
                                                      return SingleChildScrollView(
                                                        child: Container(
                                                          decoration: const BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius: BorderRadius.only(topLeft: Radius.circular(18),topRight: Radius.circular(18),)),
                                                          child: Container(
                                                            margin:const EdgeInsets.only(top: 0,bottom: 0),
                                                            child: Column(
                                                              mainAxisSize: MainAxisSize.min,
                                                              children: [
                                                                Container(
                                                                  alignment: Alignment.center,
                                                                  width: MediaQuery.of(context).size.width,
                                                                  height: 55,
                                                                  decoration:const BoxDecoration(color: Colorconstand.primaryColor,borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))),
                                                                  child: Text("ជ្រើសរើសកាលបរិច្ចេត",style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),),
                                                                ),
                                                                Container(
                                                                  alignment: Alignment.center,
                                                                  child: GridView.builder(
                                                                  shrinkWrap: true,
                                                                  physics:const ScrollPhysics(),
                                                                  padding:const EdgeInsets.only(top: 25,left: 28,right: 18),
                                                                    itemCount: attendanceMonth.length,
                                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5),
                                                                    itemBuilder: (BuildContext context, int index) {
                                                                      return GestureDetector(
                                                                        onTap: (){
                                                                          showAlertLogout(
                                                                              onSubmit: () {
                                                                                setState(() {
                                                                                  if(attendanceMonth[index].displayMonth == "YEAR"){
                                                                                  BlocProvider.of<AttendancePrintBloc>(context).add(GetAttendanceYearlyEvent(classId: "$idClass"));
                                                                                  }
                                                                                  else{
                                                                                  BlocProvider.of<AttendancePrintBloc>(context).add(GetAttendanceMonthlyEvent(classId: "$idClass",month: attendanceMonth[index].month.toString(),));
                                                                                  }
                                                                                });
                                                                                Navigator.of(context).pop();
                                                                                Navigator.of(context).pop();
                                                                              },
                                                                              title: "តើអ្នកចង់ទាញយកឯកសារមែនទេ!".tr(),
                                                                              context: context,
                                                                              onSubmitTitle: "DOWNLOAD".tr(),
                                                                              bgColorSubmitTitle: Colorconstand.alertsDecline,
                                                                              icon: const Icon(Icons.print_outlined,size: 38,color: Colorconstand.neutralWhite,),
                                                                              bgColoricon:Colorconstand.mainColorSecondary
                                                                          );
                                                                          
                                                                          
                                                                        },
                                                                        child: Container(
                                                                          child: Text(attendanceMonth[index].displayMonth.toString().tr(),style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.lightBlack),),
                                                                        ),
                                                                      );
                                                                    }
                                                                  )
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    }
                                                  );
                                                });
                                            
                                            },
                                            child: Row(
                                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                              children: [
                                                Container(
                                                  padding:const EdgeInsets.all(12),
                                                  decoration: const BoxDecoration(
                                                      color: Color.fromRGBO(235,241,245,1),
                                                      borderRadius: BorderRadius.all(Radius.circular(12))),
                                                  child: SvgPicture.asset(ImageAssets.folder_icon),
                                                ),
                                                const SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: Text("QUOTE_TAB_PRESENT".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.lightBlack),
                                                  ),
                                                ),
                                                Container(
                                                  margin:const EdgeInsets.symmetric(horizontal: 25),
                                                  child: Container(
                                                    height: 22,width: 22,
                                                    decoration:const BoxDecoration(shape: BoxShape.circle,color: Colorconstand.primaryColor),
                                                    child:const Icon(Icons.arrow_outward_sharp,color: Colorconstand.neutralWhite,size: 16,),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ):Container(),
                                        const SizedBox(
                                          height: 25,
                                        )
                                      ],
                                    ),
                                  );
                              
                                }
                                return Container();
                              
                              },
                            )
                        ),
                      ),
                    )
                  ],
                ),
              )),
            // Checking Loading when click to preview Summary Report
            isSaveLoading ==true? Positioned(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.grey.withOpacity(.5),
                child: const Center(child: CircularProgressIndicator(),),
              ),
            ):Container(),
            showDropClass ==true? Positioned(
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    showDropClass = false;
                  });
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey.withOpacity(.5),
                  child:  Center(child: Container(),),
                ),
              ),
            ):Container(),
            AnimatedPositioned(
              duration:const Duration(milliseconds:  400),
              top:showDropClass==true? 90:70,
              left:showDropClass==true? 40:80,
              right:showDropClass==true? 40:80,
              child: AnimatedContainer(
                duration:const Duration(milliseconds:  400),
                height:showDropClass==true? 150:0,
                padding:const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colorconstand.neutralWhite,
                  borderRadius: BorderRadius.circular(20)
                ),
                margin:const EdgeInsets.only(top: 15),
                child: GridView.builder(
                  shrinkWrap: true,
                  padding:const EdgeInsets.all(.0),
                    itemCount: widget.dataGrade.classes!.length,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                    itemBuilder: (BuildContext context, int indexClass) {
                    return Container(
                      margin:const EdgeInsets.only(right: 8,left: 8),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            showDropClass = false;
                            BlocProvider.of<ListSubjectInClassBloc>(context).add(GetListSubjectEvent(classId: widget.dataGrade.classes![indexClass].classId.toString()));
                            idClass = widget.dataGrade.classes![indexClass].classId.toString();
                            nameClass = widget.dataGrade.classes![indexClass].className.toString().replaceAll("ថ្នាក់ទី", "");
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 40,width: 40,
                          //padding: EdgeInsets.,
                          decoration:const BoxDecoration(
                            color: Colorconstand.primaryColor,
                            shape: BoxShape.circle
                          ),
                          child: Container(
                            child: Text(widget.dataGrade.classes![indexClass].className.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_6_semibold_14.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                          ),
                      
                        ),
                      ),
                    );
                  }
                ),
              ),
            ),
          ],
        ),
    );
  }
  String convertToKhmerNumeral(int number) {
    List<String> khmerNumerals = [
      '១',
      '២',
      '៣',
      '៤',
      '៥',
      '៦',
      '៧',
      '៨',
      '៩',
      '១០',
      '១១',
      '១២',
    ];

  if (number >= 1 && number <= 12) {
    return khmerNumerals[number - 1];
  } else {
    return '';
  }
}

void showMessageNointernet(
    BuildContext context, String title, double width, double hight) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: const Duration(milliseconds: 800),
      width: width,
      elevation: 2.0,
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      content: Wrap(
        children: [
          Container(
            height: hight,
            child: Center(
              child: Text(
                title,
                style: ThemsConstands.headline6_regular_14_24height
                    .copyWith(color: Colorconstand.neutralWhite),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
