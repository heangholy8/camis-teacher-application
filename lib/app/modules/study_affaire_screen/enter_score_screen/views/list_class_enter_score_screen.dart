import 'dart:async';
import 'package:camis_teacher_application/app/bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/models/study_affaire_model/get_class_by_level_model.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/view/grading_screen.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/enter_score_screen/state/bloc/student_affair_level_bloc.dart';
import '../../../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar_study_affair.dart';
import '../../../../service/api/approved_api/approved_api.dart';
import '../../../attendance_schedule_screen/state_management/set_exam/bloc/set_exam_bloc.dart';
import '../../../grading_screen/view/result_screen.dart';
import '../../affaire_home_screen/state/current_grade/bloc/current_grade_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../../widget/shimmer/shimmer_style.dart';
import '../../../../models/grading_model/grading_model.dart';
import '../../../../service/api/grading_api/post_graeding_data_api.dart';
import '../../../../storages/get_storage.dart';
import '../../../grading_screen/bloc/grading_bloc.dart';

class ListClassEnterScoreScreen extends StatefulWidget {
  int activeIndex;

  ListClassEnterScoreScreen({super.key, this.activeIndex = 0});

  @override
  State<ListClassEnterScoreScreen> createState() => _ListClassEnterScoreScreenState();
}

class _ListClassEnterScoreScreenState extends State<ListClassEnterScoreScreen>with TickerProviderStateMixin {

  //=============== Varible main Tap =====================
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  bool isCheckClass = false;
  TabController? controller;
  bool isInstructor = false;
  bool activeMySchedule = true;
  String classIdName = "";
  String activeLevel = "";
  bool isReachedMax = false;
  bool isReadLoading = false;
  bool isChangeTab = true;

  String classId = "";
  String subjectId = "";
  //=============== End Varible main Tap =====================

  //================= change class option ================
  AnimationController? _arrowAnimationController;
  Animation? _arrowAnimation;
  //================= change class option ================
  
  //============= Calculate Score ===========
    bool isLoadingCalculate = false;
    int calculateSuccess = 0;
    String statusCalculate  = "";
  //================================

  //============== variable Exam Tap ====================
    int? monthLocalExam;
    int? yearLocalExam;
    String? monthLocalNameExam;
    String? monthLocalNameExamFirstLoad;
    int? monthLocalExamFirstLoad;
    int? yearLocalExamFirstLoad;
    bool showListMonthExam = false;
    String semesterName = "";
    String semesterNameFristLoad = "";
    String typeSelection = "1";
    int activeIndexMonth = 0;
  //============== verible Exam Tap ====================

    List subjectApprove = [];
    StreamSubscription? internetconnection;
    bool isoffline = true;
    ScrollController scrollController = ScrollController();
    bool _isLast = false;
    bool displayCalculateScoreYear = false;
  //============== Check Save Score local do complet task ============
    GradingData data = GradingData();
    List<StudentsData> studentData = [];
    String isInstructorLocalSave = "";
    bool isLoading = false;
    StreamSubscription? sub;
    final GetStoragePref _getStoragePref = GetStoragePref();
    bool firstLoaded = true;

    final ScrollController _scrollController = ScrollController();

    void loadCheckScore() async{
    var isIntructorPref = await _getStoragePref.getInstructor.then((value){
      if(value!=null){
          setState(() {
            isInstructorLocalSave = value;
          });
      }
    });
    var isLocalDataPref =  await _getStoragePref.getScoreData.then((value){
      if(value.studentsData!=null){
          setState(() {
           // isHasDoTask = true;
            data = value;
            studentData  = data.studentsData!.toList();

            var api = PostGradingApi();
            api.postListStudentSubjectExam(
              classID: data.classId!,
              subjectID: data.subjectId!,
              term: data.semester,
              examDate: "",
              type: data.type!,
              month: data.month,
              listStudentScore: studentData,
            ).then((value) async {
              setState(() {
                // isLoading = false;
                // isHasDoTask = false;
              });
              int totalScoreStudent = studentData.where((element) => element.score!=null).length;
              if(isInstructorLocalSave=="1"&&totalScoreStudent==0){
                ApprovedApi().postApproved(classId: data.classId, examObjectId: data.id, isApprove: 1, discription: "");
              }
              SharedPreferences pref = await SharedPreferences.getInstance();
              pref.remove("Data Score").then((value) => print("remove data score successfully"));
              pref.remove("Instructor").then((value) => print("remove instructor successfully"));
            });
          });
      }
      
    });
  }
//============== End Check Save Score local do complet task ============

  @override
  void initState() {
    print(BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel);
    BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradeAffaire());
    _scrollController.addListener(_scrollListener);
    setState(() {
    //============= Check internet ====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true) {
            loadCheckScore();
          }
        });
      });
    //=============End Check internet===============
  
    //=============Check internet===================
      internetconnection = Connectivity()
        .onConnectivityChanged.listen((ConnectivityResult result) {
          // whenevery connection status is changed.
          if (result == ConnectivityResult.none) {
            //there is no any connection
            setState(() {
              isoffline = false;
            });
          } else if (result == ConnectivityResult.mobile) {
            //connection is mobile data network
            setState(() {
              isoffline = true;
            });
          } else if (result == ConnectivityResult.wifi) {
            //connection is from wifi
            setState(() {
              isoffline = true;
            });
          }
        });
    
    //=============Eend Check internet====================
 
    //============= controller class change option ========
      _arrowAnimationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 300));
      _arrowAnimation =Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController!);
    //============= controller class change option ========
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      monthLocalNameExam = DateFormat.MMMM("en-IN").format(DateTime.now()).toUpperCase();
      //==============  Exam Tap ====================
     monthLocalExam = monthlocal;
     yearLocalExam = yearchecklocal;
     monthLocalExamFirstLoad = monthlocal;
     yearLocalExamFirstLoad = yearchecklocal;
     monthLocalNameExamFirstLoad = monthLocalNameExam;
    //============== Exam Tap =====================
    });
    //============== Event call Data =================
    BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
    //============== End Event call Data =================
    super.initState();
    if(isoffline == true){
      loadCheckScore();
    }
  }

  void _scrollListener() {
    if (_scrollController.position.maxScrollExtent == _scrollController.position.pixels && isReachedMax == false) {
      print("888 - $activeLevel,$semesterName, $monthLocalExam,${BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel}");
      // When the user scrolls to the maximum scroll extent, load more data.
      BlocProvider.of<StudentAffairLevelBloc>(context).add(
        GetStudentAffairClassByLevelEvent(
          month: "$monthLocalExam",
          semester: semesterName,
          type: typeSelection,
          level: BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel,
          page: "1",
          isRefresh: false,
        ),
      );
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final wieght = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      bottomNavigationBar: isoffline==false?Container(height: 0,):const BottomNavigateBarStudyAffair(isActive: 3),
       // bottomNavigationBar:Container(height: 0),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_18_sch),
            ),
            Positioned (
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: SafeArea(
                bottom: false,
                child: Container(
                  margin:const EdgeInsets.only(top: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal:8.0),
                        child: SizedBox(
                          child: Text("SCORE".tr(), style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.neutralWhite)),
                        ),
                      ),
                      const SizedBox(height: 18),
                      Expanded(
                        child: BlocListener<ListMonthSemesterBloc, ListMonthSemesterState>(
                          listener: (context, state) {
                            if (state is ListMonthSemesterLoaded){
                              var data = state.monthAndSemesterList!.data;
                              for (var i = 0; i < data!.length; i++) {
                                if (data[i].displayMonth == monthLocalNameExam) {
                                  setState(() {
                                    activeIndexMonth = i;
                                    semesterName = data[i].semester.toString();
                                    semesterNameFristLoad = data[i].semester.toString();
                                  });
                                  // BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester:  data[i].semester.toString(),type:"1",month: data[i].month.toString()));
                                }
                              }
                            }
                          },
                          child: Container(
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Colorconstand.neutralWhite),
                            child: Column(
                              children: [
                                BlocListener<CurrentGradeBloc, CurrentGradeState>(
                                  listener: (context,state){
                                    if(state is CurrentGradeAffaireLoading){
                                      print("loading");
                                    } 
                                    else if (state is CurrentGradeAffaireLoaded) {
                                      var data = state.currentGradeAffaireModel!.data;
                                      print("loading Active Index: ${widget.activeIndex} ${data![widget.activeIndex].level}");
                                      setState(() {
                                        BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel = data[widget.activeIndex].level.toString();
                                        activeLevel = data[widget.activeIndex].level.toString();
                                      });
                                      // Only when click on bottom nav 
                                      Future.delayed(const Duration(milliseconds: 300),(){
                                         BlocProvider.of<StudentAffairLevelBloc>(context).add(
                                        GetStudentAffairClassByLevelEvent(
                                          month: "$monthLocalExam",
                                          semester: semesterName,
                                          type: "1",
                                          level: "${data[widget.activeIndex].level}",
                                          page: "1",
                                          isRefresh: true,
                                        ),
                                      );
                                      });
                                    }
                                  },
                                  child: Container(),
                                  ),
                                Expanded(
                                  child: BlocBuilder<ListMonthSemesterBloc, ListMonthSemesterState>(
                                    builder: (context, state) {
                                      if (state is ListMonthSemesterLoading) {
                                        return Container(
                                          decoration: const BoxDecoration(
                                            color: Color(0xFFF1F9FF),
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(12),
                                              topRight: Radius.circular(12),
                                            ),
                                          ),
                                          child: Column(
                                            children: [
                                              Container(
                                                color: const Color(0x622195F3),
                                                height: 165,
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                  padding: const EdgeInsets.all(0),
                                                  shrinkWrap: true,
                                                  itemBuilder: (BuildContext contex, index) {
                                                    return const ShimmerTimeTable();
                                                  },
                                                  itemCount: 5,
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      } else if (state is ListMonthSemesterLoaded) {
                                        var datamonth = state.monthAndSemesterList!.data;
                                        return Column(
                                          children: [
                                            Container(
                                              width: double.maxFinite,
                                              decoration: BoxDecoration(
                                                  borderRadius:BorderRadius.circular(4.0),
                                                  color: Colorconstand.mainColorSecondary,
                                              ),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                              //===========  Button Show List Month ===========================
                                                  GestureDetector(
                                                    onTap:() {
                                                      setState(() {
                                                          if(showListMonthExam==true){
                                                            showListMonthExam =false;
                                                          }
                                                          else{
                                                            showListMonthExam = true;
                                                          }
                                                      });
                                                    },
                                                    child:SizedBox(
                                                      width: MediaQuery.of(context).size.width,
                                                      height: 45,
                                                        child: Row(
                                                          children: [
                                                            Expanded(
                                                              child: Row(
                                                              mainAxisAlignment:MainAxisAlignment.center,
                                                              children: [
                                                                Text(
                                                                  "${"$monthLocalNameExam".tr()}  ",
                                                                  style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),
                                                                  ),
                                                                    Text("${datamonth![activeIndexMonth].year}",
                                                                      style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite)
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 8
                                                                    ),
                                                                    showListMonthExam==true?Container(): Icon(
                                                                      showListMonthExam ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                      size: 24,
                                                                      color: Colorconstand.neutralWhite
                                                                    ),
                                                                  ],
                                                                ),
                                                            ),
                                                          ],
                                                        ),
                                                        ),
                                                  ),
                                              //=========== End Button Show List Month ===========================
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Stack(
                                                children: [
                                                     BlocBuilder<CurrentGradeBloc, CurrentGradeState>(                                              
                                                      builder:(context, state) {
                                                        if (state is CurrentGradeAffaireLoading) {
                                                          return Container();
                                                        }else if(state is CurrentGradeAffaireLoaded) {
                                                          var data = state.currentGradeAffaireModel!.data;
                                                          if(firstLoaded==true){
                                                            BlocProvider.of<StudentAffairLevelBloc>(context).add(GetStudentAffairClassByLevelEvent(
                                                              month: "$monthLocalExam",
                                                              semester: semesterName,
                                                              type: "1",
                                                              level: "${data![widget.activeIndex].level}",
                                                              page: "1",
                                                              isRefresh: true,
                                                            ));
                                                            firstLoaded=false;
                                                          }
                                                          return DefaultTabController(
                                                            length: data!.length,
                                                            initialIndex: widget.activeIndex,
                                                            child: Column(
                                                              children: [
                                                                isChangeTab == true?TabBar (
                                                                  padding:const EdgeInsets.all(0),
                                                                  isScrollable: data.length == 4 ? true : false,
                                                                  unselectedLabelColor:Colors.grey,
                                                                  unselectedLabelStyle:ThemsConstands.headline_4_medium_18 ,
                                                                  labelColor: Colorconstand.primaryColor,
                                                                  labelStyle: ThemsConstands.headline_4_medium_18.copyWith(fontWeight: FontWeight.w700),
                                                                  onTap: isChangeTab == true ? (value) {                                                                    // print("$semesterName,$activeLevel ${data[value].level}");
                                                                    setState(() {
                                                                      activeLevel = data[value].level.toString();
                                                                      BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel = data[value].level.toString();
                                                                      isReachedMax = false;
                                                                      isReadLoading = true;
                                                                    });
                                                                    BlocProvider.of<StudentAffairLevelBloc>(context).add(
                                                                      GetStudentAffairClassByLevelEvent(
                                                                        month: "$monthLocalExam",
                                                                        semester: semesterName,
                                                                        type: typeSelection,
                                                                        level: "${data[value].level}",
                                                                        page: "1",
                                                                        isRefresh: true,
                                                                      ),
                                                                    );
                                                                    
                                                                  }:null,
                                                                  tabs: List.generate(data.length, (tabIndex) => 
                                                                    Tab(
                                                                      child: Text("${"GRADED".tr()} ${data[tabIndex].level}",textAlign: TextAlign.center,style: ThemsConstands.headline_5_medium_16,),
                                                                    ),
                                                                  )
                                                                ):Container(),
                                                                const Divider(
                                                                  height: 0,
                                                                  thickness: 1,
                                                                ),
                                                                const SizedBox(height: 8,),
                                                              
                                                                Expanded(
                                                                  child: customeBoxClassWidget()
                                                                )
                                                              ]
                                                            ),
                                                          );
                                                        }else{
                                                          return Container();
                                                        }
                                                      },
                                                    ),
                                                
                                                  // ),
                                              
                                                  /// =============== Baground over when drop month =============
                                                  showListMonthExam == false
                                                  ? Container()
                                                  : Positioned(
                                                      bottom: 0,
                                                      left: 0,
                                                      right: 0,
                                                      top: 0,
                                                      child:
                                                          GestureDetector(
                                                        onTap: (() {
                                                          setState(
                                                            () {
                                                              showListMonthExam =false;
                                                            });
                                                        }),
                                                        child:
                                                            Container(
                                                          color: const Color(
                                                              0x7B9C9595),
                                                        ),
                                                      ),
                                                    ),
                                            /// =============== Baground over when drop month =============
                                            /// 
                                            /// =============== Widget List Change Month =============
                                                AnimatedPositioned(
                                                    top: showListMonthExam == false ? wieght > 800 ? - 700 : -270 : 0,
                                                    left: 0,
                                                    right: 0,
                                                    duration: const Duration(milliseconds:300),
                                                    child:Container(
                                                      color: Colorconstand.neutralWhite,
                                                      child: GestureDetector(
                                                        onTap:() {
                                                          setState(() {
                                                            showListMonthExam = false;
                                                          });
                                                        },
                                                        child:Container(
                                                          margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                          child: GridView.builder(
                                                            shrinkWrap: true,
                                                            physics:const NeverScrollableScrollPhysics(),
                                                            padding:const EdgeInsets.all(.0),
                                                              itemCount: datamonth.length,
                                                              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5),
                                                              itemBuilder: (BuildContext context, int index) {
                                                                return activeMySchedule==true&&datamonth[index].displayMonth=="YEAR"?Container()
                                                                :GestureDetector(
                                                                  onTap: (() {
                                                                    setState(() {
                                                                      _isLast = false;
                                                                      monthLocalNameExam = datamonth[index].displayMonth;
                                                                      showListMonthExam = false;
                                                                      monthLocalExam =  int.parse(datamonth[index].month.toString());
                                                                      semesterName = datamonth[index].semester.toString();
                                                                      if(datamonth[index].displayMonth=="YEAR"){
                                                                        setState(() {
                                                                            displayCalculateScoreYear = true;
                                                                            typeSelection = "3";
                                                                        });
                                                                      }
                                                                      else if(datamonth[index].displayMonth=="FIRST_SEMESTER"||datamonth[index].displayMonth=="SECOND_SEMESTER"){
                                                                        setState(() {
                                                                          displayCalculateScoreYear = false;
                                                                          activeIndexMonth = index - 2;
                                                                          typeSelection = "2";
                                                                        });
                                                                        // if(activeMySchedule==true){
                                                                        //   // BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: datamonth[index].semester,type: "2",month: datamonth[index].month.toString()));
                                                                        // }
                                                                        // else{
                                                                        //   BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: datamonth[index].semester,type: "2",month: datamonth[index].month.toString(),classid: classIdTeacher));
                                                                        // }
                                                                      }
                                                                      else{
                                                                        setState(() {
                                                                          displayCalculateScoreYear = false;
                                                                          activeIndexMonth = index;
                                                                          typeSelection = "1";
                                                                        });
                                                                        // if(activeMySchedule==true){
                                                                        //   BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: datamonth[index].semester,type: "1",month: datamonth[index].month.toString()));
                                                                        // }
                                                                        // else{
                                                                        //   BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: datamonth[index].semester,type: "1",month: datamonth[index].month.toString(),classid: classIdTeacher));
                                                                        // }
                                                                      }
                                                                      // Get StudentAffairLevel Bloc in Month List
                                                                      BlocProvider.of<StudentAffairLevelBloc>(context).add(GetStudentAffairClassByLevelEvent(
                                                                        month: "$monthLocalExam",
                                                                        semester: semesterName,
                                                                        type: typeSelection,
                                                                        level: activeLevel,
                                                                        page: "1",
                                                                        isRefresh: true,
                                                                      ));
                                                                    });
                                                                  }),
                                                                  child: Container(
                                                                    margin:const EdgeInsets.all(8),
                                                                    decoration:BoxDecoration(
                                                                      borderRadius: BorderRadius.circular(12),
                                                                      color: monthLocalNameExam == datamonth[index].displayMonth? Colorconstand.primaryColor:Colors.transparent,
                                                                    ),
                                                                    child:  Center(child: Text("${datamonth[index].displayMonth}".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: monthLocalNameExam == datamonth[index].displayMonth?Colorconstand.neutralWhite
                                                                    : datamonth[index].displayMonth =="FIRST_SEMESTER"||datamonth[index].displayMonth=="YEAR"||datamonth[index].displayMonth=="SECOND_SEMESTER"? Colorconstand.alertsDecline :Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)),
                                                                  ),
                                                                );
                                                              },
                                                            ),
                                                        )
                                                      ),
                                                    ),
                                                  ),
                                              /// =============== End Widget List Change Month =============
                                                    
                                              //======================= Icon Widget redirect to active date =======================
                                                AnimatedPositioned(
                                                  bottom: monthLocalNameExam == monthLocalNameExamFirstLoad || showListMonthExam==true?-60:subjectApprove.isNotEmpty && activeMySchedule == false && _isLast == true?80:30,right: 30,
                                                  duration: const Duration(milliseconds:300),
                                                  child: Container(
                                                    height: 55,width: 55,
                                                    decoration: BoxDecoration(
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colorconstand.neutralGrey.withOpacity(0.7),
                                                          spreadRadius: 4,
                                                          blurRadius: 4,
                                                          offset:const Offset(0, 3), // changes position of shadow
                                                        ),
                                                      ],
                                                      color: Colorconstand.neutralWhite,
                                                      shape: BoxShape.circle
                                                    ),
                                                    child: MaterialButton(
                                                      elevation: 8,
                                                      onPressed: (){
                                                       
                                                        setState(() {
                                                          typeSelection = "1";
                                                          displayCalculateScoreYear = false;
                                                          monthLocalNameExam = monthLocalNameExamFirstLoad;
                                                          // if(activeMySchedule==true){
                                                          //   BlocProvider.of<ExamScheduleBloc>(context).add(GetMyExamScheduleEvent(semester: semesterNameFristLoad,type: "1",month: monthLocalExamFirstLoad.toString()));
                                                          // }
                                                          // else{
                                                          //   BlocProvider.of<ExamScheduleBloc>(context).add(GetClassExamScheduleEvent(semester: semesterNameFristLoad,type: "1",month: monthLocalExamFirstLoad.toString(),classid: classIdTeacher));
                                                          // }
                                                        });
                                                        print("month Local Exam: $monthLocalExam, $monthlocal $monthLocalNameExam");
                                                        BlocProvider.of<StudentAffairLevelBloc>(context).add(GetStudentAffairClassByLevelEvent(
                                                          month: "$monthlocal",
                                                          semester: semesterName,
                                                          type: "1",
                                                          level: activeLevel,
                                                          page: "1",
                                                          isRefresh: true,
                                                        ));
                                                      },
                                                      shape:const CircleBorder(),
                                                      child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                                    ),
                                                  ),
                                                ),   
                                            //======================= End Icon Widget redirect to active date =======================
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      } else {
                                        return Center(
                                          child: EmptyWidget(
                                            title: "WE_DETECT".tr(),
                                            subtitle: "WE_DETECT_DES".tr(),
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

      //================ Internet offilne ========================
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
      //================ Internet offilne ========================
          ],
        ),
      )
    );
  }

  Widget customeBoxClassWidget() {
    return BlocListener<SetExamBloc, SetExamState>(
      listener: (context, state) {
        if(state is SetNoExamLoaded){
          BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
            idClass:  classId.toString(), 
            subjectId: subjectId.toString(), 
            type: typeSelection, 
            month:  "$monthLocalExam", 
            semester:  semesterName,
            examDate: "",
            ),
          );
          Navigator.push(context, 
              MaterialPageRoute(builder: (context) => GradingScreen(
              isPrimary: false,
              isInstructor: true,
              myclass: true,
                ),
              ),
            );
          }
      },
      child: BlocListener<StudentAffairLevelBloc, StudentAffairLevelState>(
          listener: (context, state) {
            if(state is StudentAffairLevelLoading){
              setState(() {
                isChangeTab = false;
              });
            }else if(state is StudentAffairLevelLoaded){
               setState(() {
                isChangeTab = true;
              });
            }else if(state is StudentAffairLevelError) {
               setState(() {
                isChangeTab = true;
              });
            }else{
    
            }
          },
          child: BlocBuilder<StudentAffairLevelBloc, StudentAffairLevelState>(
              builder: (context, state) {
                List<Datum> data = [];
                bool isReadLoadin = false;
                if(state is StudentAffairLevelLoading){
                  data = state.oldData;
                  isReadLoadin = true;
                }else if(state is StudentAffairLevelLoaded) {
                  data = state.studentAffairLevelModel;
                  isReachedMax = state.hasReachedMax;
                } 
                if(state is StudentAffairLevelLoading && state.isFirstFetch) {
                  return Column(
                    children: [
                      Expanded(
                        child: Container(
                          color: Colorconstand.neutralWhite,
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: 3,
                            itemBuilder: (contex, index) {
                              return Container(
                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(18)),
                                margin:const EdgeInsets.symmetric(horizontal: 8,vertical: 18),
                                child: Shimmer.fromColors(
                                  baseColor:const Color(0xFF3688F4),
                                  highlightColor:const Color(0xFF3BFFF5),
                                  child:Container(
                                    margin:const EdgeInsets.only(bottom: 5),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          height:17,
                                          width: MediaQuery.of(context).size.width/2.5,
                                          decoration:  BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            color:const Color(0x622195F3),
                                          ),
                                        ),
                                        const SizedBox(width: 7.0,height: 8,),
                                        Container(
                                          height: 140,
                                          width: MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(  color:const Color(0x622195F3),borderRadius: BorderRadius.circular(4)),
                                        )
                                                ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  );
             
                }
              return data.isEmpty ?
              //  Center(
              //     child: Text("NO_CLASS".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),)
              //   )
              Column(
                    children: [
                      Expanded(
                        child: Container(
                          color: Colorconstand.neutralWhite,
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: 3,
                            itemBuilder: (contex, index) {
                              return Container(
                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(18)),
                                margin:const EdgeInsets.symmetric(horizontal: 8,vertical: 18),
                                child: Shimmer.fromColors(
                                  baseColor:const Color(0xFF3688F4),
                                  highlightColor:const Color(0xFF3BFFF5),
                                  child:Container(
                                    margin:const EdgeInsets.only(bottom: 5),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          height:17,
                                          width: MediaQuery.of(context).size.width/2.5,
                                          decoration:  BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            color:const Color(0x622195F3),
                                          ),
                                        ),
                                        const SizedBox(width: 7.0,height: 8,),
                                        Container(
                                          height: 140,
                                          width: MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(  color:const Color(0x622195F3),borderRadius: BorderRadius.circular(4)),
                                        )
                                                ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  )
             
                : ListView.builder(
                    controller: _scrollController,
                    shrinkWrap: true,
                    itemCount: data.length + (isReadLoadin?1:0),
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    itemBuilder: (context,index){
                      if(index < data.length){
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                          Padding(
                              padding: const EdgeInsets.only(left:8.0,right:8.0,bottom: 8),
                              child: Text(data[index].subjectName.toString(),style: ThemsConstands.headline_4_semibold_18,),
                            ),
                            Container(
                              padding:const EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                color: Colorconstand.mainColorUnderlayer,
                                borderRadius: BorderRadius.circular(20)
                              ),
                              margin: const EdgeInsets.only(top:0,bottom: 12),
                              child: GridView.builder(
                                shrinkWrap: true,
                                physics:const NeverScrollableScrollPhysics(),
                                padding:const EdgeInsets.all(.0),
                                  itemCount: data[index].classes!.length,
                                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount:5,mainAxisSpacing: 0),
                                  itemBuilder: (BuildContext context, int indexClass) {
                                  return Container(
                                    margin:const EdgeInsets.only(right: 8,left: 8),
                                    child: GestureDetector(
                                      onTap: data[index].classes![indexClass].isCheckScoreEnter==1?(){
                                        BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                                            idClass:  data[index].classes![indexClass].id.toString(), 
                                            subjectId: data[index].subjectId.toString(), 
                                            type: data[index].classes![indexClass].type.toString(), 
                                            month:  "$monthLocalExam", 
                                            semester:  semesterName,
                                            examDate: "",
                                            ),
                                          );
                                        Navigator.push(context, MaterialPageRoute(builder: (context)=> 
                                             const ResultScreen(isInstructor: true
                                            ),
                                         )
                                        );
                                      }:() {
                                        if(data[index].classes![indexClass].subjectExam!.id !=0){
                                          BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                                            idClass:  data[index].classes![indexClass].id.toString(), 
                                            subjectId: data[index].subjectId.toString(), 
                                            type: data[index].classes![indexClass].type.toString(), 
                                            month:  "$monthLocalExam", 
                                            semester:  semesterName,
                                            examDate: "",
                                            ),
                                          );
                                        Navigator.push(context, 
                                            MaterialPageRoute(builder: (context) => GradingScreen(
                                            isPrimary: false,
                                            isInstructor: true,
                                            myclass: true,
                                            ),
                                          ),
                                        );
                                        }else{
                                          setState(() {
                                            classId = data[index].classes![indexClass].id.toString();
                                            subjectId = data[index].subjectId.toString();
                                          });
                                          BlocProvider.of<SetExamBloc>(context).add(SetNoExam(idClass: data[index].classes![indexClass].id.toString(), idsubject:  data[index].subjectId.toString(), semester: semesterName, isNoExam: "2", type:typeSelection,  month: monthLocalExam.toString(),));
                                        }
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 34,
                                        width: 34,
                                        decoration: BoxDecoration(
                                          color: data[index].classes![indexClass].isCheckScoreEnter == 1? Colorconstand.primaryColor:data[index].classes![indexClass].isCheckScoreEnter == 2? Colorconstand.alertsAwaitingText:Colorconstand.alertsDecline,
                                          shape: BoxShape.circle
                                        ),
                                        child: Text(data[index].classes![indexClass].className.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                                      ),
                                    ),
                                  );
                                }
                              ),
                            ),
                  
                          ],
                        );
                      }else{
                        return SizedBox(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          child: const Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      }
                  });
               
              },
            ),
        ),
    );
  }
}
