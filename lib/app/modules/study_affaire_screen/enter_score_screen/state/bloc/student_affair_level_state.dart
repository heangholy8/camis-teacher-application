// ignore_for_file: must_be_immutable

part of 'student_affair_level_bloc.dart';

 class StudentAffairLevelState extends Equatable {
  const StudentAffairLevelState();
  
  @override
  List<Object> get props => [];
}

 class StudentAffairLevelInitial extends StudentAffairLevelState {}

 class StudentAffairLevelLoading extends StudentAffairLevelState{
  final List<Datum> oldData;
  bool isFirstFetch;

  StudentAffairLevelLoading({required this.oldData,this.isFirstFetch = false});
 }

class StudentAffairLevelLoaded extends StudentAffairLevelState{
  List<Datum> studentAffairLevelModel;
  final bool hasReachedMax;
  StudentAffairLevelLoaded({required this.hasReachedMax, required this.studentAffairLevelModel});
 }
 
 class StudentAffairLevelError extends StudentAffairLevelState{
  final String error;
  const StudentAffairLevelError({required this.error});
 }
