import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/study_affaire_model/get_class_by_level_model.dart';
import 'package:equatable/equatable.dart';
import '../../../../../service/api/get_current_grades_affaire_api/current_grades_affaire.dart';

part 'student_affair_level_event.dart';
part 'student_affair_level_state.dart';

class StudentAffairLevelBloc extends Bloc<StudentAffairLevelEvent, StudentAffairLevelState> {
   String studentLevel = "";
  final GetCurrentGradeAffaireApi getclassByLevelApi;
  List<Datum> oldData = [];
  int page =1;
  bool isReachedMax = false;

  StudentAffairLevelBloc({required this.getclassByLevelApi}) : super(StudentAffairLevelInitial()) {
    on<GetStudentAffairClassByLevelEvent>((event, emit ) async {
      try {
       
        if(event.isRefresh == false) {
          if(state is StudentAffairLevelLoading) return;
          var currentState = state;
          if(currentState is StudentAffairLevelLoaded){
            oldData = currentState.studentAffairLevelModel;
          }
          emit(StudentAffairLevelLoading(oldData: oldData,isFirstFetch: page == 1));
          var resultData = await getclassByLevelApi.getClassStudentAffairByLevelApi(month: event.month, semester: event.semester, type: event.type, level: event.level, page: "$page");
          page++;
          var data = (state as StudentAffairLevelLoading).oldData;
          data.addAll(resultData.data!);
          if(resultData.hasMorePages !=false) {
            isReachedMax=false;
            emit(StudentAffairLevelLoaded(hasReachedMax: isReachedMax, studentAffairLevelModel: data));
          }else{
            isReachedMax=true;
            emit(StudentAffairLevelLoaded(hasReachedMax: isReachedMax, studentAffairLevelModel: data));
          }
        } else if(event.isRefresh == true) {
          oldData =[];
          page = 1;
          emit(StudentAffairLevelLoading(oldData: oldData,isFirstFetch: page == 1));
          await getclassByLevelApi.getClassStudentAffairByLevelApi(month: event.month, semester: event.semester, type: event.type, level: event.level, page: "$page").then((value) {
            oldData.addAll(value.data!);
            if(value.hasMorePages !=false){
              isReachedMax=false;
              emit(StudentAffairLevelLoaded(hasReachedMax: isReachedMax, studentAffairLevelModel: oldData));
            }else{
              isReachedMax=true;
              emit(StudentAffairLevelLoaded(hasReachedMax: isReachedMax, studentAffairLevelModel: oldData));
            }
            page=2;
          });
        }

      } catch (e) {
        emit(StudentAffairLevelError(error: e.toString()));
      }
    });
  }
}
