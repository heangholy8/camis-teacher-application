part of 'student_affair_level_bloc.dart';

 class StudentAffairLevelEvent extends Equatable {
  const StudentAffairLevelEvent();

  @override
  List<Object> get props => [];
}

class GetStudentAffairClassByLevelEvent extends StudentAffairLevelEvent{
  final String month;
  final String semester;
  final String type;
  final String level;
  final String page;
  final bool isRefresh;
  GetStudentAffairClassByLevelEvent({
    required this.isRefresh,
    required this.month,required this.semester,required this.type,required this.level,required this.page});
}
