import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../bloc/get_list_month_semester/bloc/list_month_semester_bloc.dart';
import '../../../../core/resources/asset_resource.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../student_list_screen/bloc/student_in_class_bloc.dart';
import '../../../teacher_list_screen/bloc/teacher_list_bloc.dart';
import '../../affaire_home_screen/state/current_grade/bloc/current_grade_bloc.dart';
import '../../report_study_affaire/state/list_subject_in_class/bloc/list_subject_in_class_bloc.dart';
import '../../report_study_affaire/view/report_study_affaire_screen.dart';
import '../../student_list_in_affaire/view/student_list_in_affaire.screen.dart';
import '../../teacher_list_in_affaire/view/teacher_list_in_affaire.screen.dart';

class ListClassInGradeScreen extends StatefulWidget {
  final int routFromScreen;
  const ListClassInGradeScreen({super.key,required this.routFromScreen});

  @override
  State<ListClassInGradeScreen> createState() =>
      _ListClassInGradeScreenState();
}

class _ListClassInGradeScreenState extends State<ListClassInGradeScreen>{
  bool connection = true;
  StreamSubscription? sub;
  @override
  void initState() {
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {
          BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradeAffaire());
        } else {}
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 10, left: 22, right: 22, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            ImageAssets.chevron_left,
                            color: Colorconstand.neutralWhite,
                            width: 32,
                            height: 32,
                          ),
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: Text(
                              widget.routFromScreen==1?"LEDGERSTUDENT".tr():widget.routFromScreen==2?"LEDGERTEACHER".tr():"REPORT".tr(),
                              style: ThemsConstands.headline_2_semibold_24 .copyWith(
                                color: Colorconstand.neutralWhite,
                              ),
                              textAlign: TextAlign.center,
                            )),
                        const SizedBox(
                          width: 32,
                          height: 32,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colorconstand.neutralWhite,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
                        ),
                        child:  Column(
                          children: [
                            Container(
                              margin:const EdgeInsets.only(top: 15),
                              child: Text("CHOOSECLASS".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey),),
                            ),
                            Expanded(
                              child: BlocBuilder<CurrentGradeBloc, CurrentGradeState>(
                                builder: (context, state) {
                                  if(state is CurrentGradeAffaireLoading){
                                    return Container(
                                      child:const Center(child: CircularProgressIndicator()),
                                    );
                                  }
                                  else if( state is CurrentGradeAffaireLoaded){
                                    var dataGrade  = state.currentGradeAffaireModel!.data;
                                    return dataGrade!.isEmpty? 
                                    Container(
                                      margin:const EdgeInsets.only(top: 50),
                                        child: EmptyWidget(
                                          title: "CAPTION_GRADE".tr(),
                                          subtitle: "CAPTION_NO_GRADE".tr(),
                                        ),
                                      )
                                    :Container(
                                      margin:const EdgeInsets.only(left: 12,right: 12,top: 10),
                                      child: ListView.builder(
                                        padding:const EdgeInsets.only(bottom: 25),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount: dataGrade.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                margin:const EdgeInsets.only(top: 15,left: 8),
                                                child: Text("${"GRADED".tr()} ${dataGrade[index].level}",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.left,),
                                              ),
                                              Container(
                                                padding:const EdgeInsets.all(8),
                                                decoration: BoxDecoration(
                                                  color: Colorconstand.mainColorUnderlayer,
                                                  borderRadius: BorderRadius.circular(20)
                                                ),
                                                margin:const EdgeInsets.only(top: 15),
                                                child: GridView.builder(
                                                  shrinkWrap: true,
                                                  physics:const NeverScrollableScrollPhysics(),
                                                  padding:const EdgeInsets.all(.0),
                                                    itemCount: dataGrade[index].classes!.length,
                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                                                    itemBuilder: (BuildContext context, int indexClass) {
                                                    return Container(
                                                      margin:const EdgeInsets.only(right: 8,left: 8),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          if(widget.routFromScreen ==2 ){
                                                            Navigator.push(context,MaterialPageRoute(builder: (context) =>  TeacherListInAffireScreen( dataGrade: dataGrade[index],classId: dataGrade[index].classes![indexClass].classId.toString(),classname: dataGrade[index].classes![indexClass].className.toString())));
                                                            BlocProvider.of<TeacherListBloc>(context).add(GetTeacherListEvent(idClass:dataGrade[index].classes![indexClass].classId.toString()));
                                                          }
                                                          if(widget.routFromScreen ==1){
                                                            Navigator.push(context,MaterialPageRoute(builder: (context) =>  StudentListInAffireScreen( dataGrade: dataGrade[index],classId: dataGrade[index].classes![indexClass].classId.toString(),classname: dataGrade[index].classes![indexClass].className.toString())));
                                                            BlocProvider.of<GetStudentInClassBloc>(context).add(GetStudentInClass(idClass: dataGrade[index].classes![indexClass].classId.toString()));
                                                          }
                                                          if(widget.routFromScreen ==3){
                                                            BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
                                                            BlocProvider.of<ListSubjectInClassBloc>(context).add(GetListSubjectEvent(classId: dataGrade[index].classes![indexClass].classId.toString()));
                                                            Navigator.push(context,MaterialPageRoute(builder: (context) => ReportStudeyAffaireScreen( dataGrade: dataGrade[index],classId: dataGrade[index].classes![indexClass].classId.toString(),classname: dataGrade[index].classes![indexClass].className.toString())));
                                                          }
                                                        },
                                                        child: Container(
                                                          alignment: Alignment.center,
                                                          height: 40,width: 40,
                                                          decoration:const BoxDecoration(
                                                            color: Colorconstand.primaryColor,
                                                            shape: BoxShape.circle
                                                          ),
                                                          child: Container(
                                                            child: Text(dataGrade[index].classes![indexClass].className!.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_4_semibold_18.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                                                          ),
                                                      
                                                        ),
                                                      ),
                                                    );
                                                  }
                                                ),
                                              ),
                                          
                                            ],
                                          );
                                        },
                                      ),
                                    );
                                  }
                                  else if(state is CurrentGradePricipleLoaded){
                                    var dataGrade  = state.currentGradeAffaireModel!.data;
                                    return dataGrade!.isEmpty? 
                                    Container(
                                      margin:const EdgeInsets.only(top: 50),
                                        child: EmptyWidget(
                                          title: "CAPTION_GRADE".tr(),
                                          subtitle: "CAPTION_NO_GRADE".tr(),
                                        ),
                                      )
                                    :Container(
                                      margin:const EdgeInsets.only(left: 12,right: 12,top: 10),
                                      child: ListView.builder(
                                        padding:const EdgeInsets.only(bottom: 25),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount: dataGrade.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                margin:const EdgeInsets.only(top: 15,left: 8),
                                                child: Text("${"GRADED".tr()} ${dataGrade[index].level}",style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.left,),
                                              ),
                                              Container(
                                                padding:const EdgeInsets.all(8),
                                                decoration: BoxDecoration(
                                                  color: Colorconstand.mainColorUnderlayer,
                                                  borderRadius: BorderRadius.circular(20)
                                                ),
                                                margin:const EdgeInsets.only(top: 15),
                                                child: GridView.builder(
                                                  shrinkWrap: true,
                                                  physics:const NeverScrollableScrollPhysics(),
                                                  padding:const EdgeInsets.all(.0),
                                                    itemCount: dataGrade[index].classes!.length,
                                                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                                                    itemBuilder: (BuildContext context, int indexClass) {
                                                    return Container(
                                                      margin:const EdgeInsets.only(right: 8,left: 8),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          if(widget.routFromScreen ==2 ){
                                                            Navigator.push(context,MaterialPageRoute(builder: (context) =>  TeacherListInAffireScreen( dataGrade: dataGrade[index],classId: dataGrade[index].classes![indexClass].classId.toString(),classname: dataGrade[index].classes![indexClass].className.toString())));
                                                            BlocProvider.of<TeacherListBloc>(context).add(GetTeacherListEvent(idClass:dataGrade[index].classes![indexClass].classId.toString()));
                                                          }
                                                          if(widget.routFromScreen ==1){
                                                            Navigator.push(context,MaterialPageRoute(builder: (context) =>  StudentListInAffireScreen( dataGrade: dataGrade[index],classId: dataGrade[index].classes![indexClass].classId.toString(),classname: dataGrade[index].classes![indexClass].className.toString())));
                                                            BlocProvider.of<GetStudentInClassBloc>(context).add(GetStudentInClass(idClass: dataGrade[index].classes![indexClass].classId.toString()));
                                                          }
                                                          if(widget.routFromScreen ==3){
                                                            BlocProvider.of<ListMonthSemesterBloc>(context).add(ListMonthSemesterEventData());
                                                            BlocProvider.of<ListSubjectInClassBloc>(context).add(GetListSubjectEvent(classId: dataGrade[index].classes![indexClass].classId.toString()));
                                                            Navigator.push(context,MaterialPageRoute(builder: (context) => ReportStudeyAffaireScreen( dataGrade: dataGrade[index],classId: dataGrade[index].classes![indexClass].classId.toString(),classname: dataGrade[index].classes![indexClass].className.toString())));
                                                          }
                                                        },
                                                        child: Container(
                                                          alignment: Alignment.center,
                                                          height: 40,width: 40,
                                                          decoration:const BoxDecoration(
                                                            color: Colorconstand.primaryColor,
                                                            shape: BoxShape.circle
                                                          ),
                                                          child: Container(
                                                            child: Text(dataGrade[index].classes![indexClass].className!.toString().replaceAll("ថ្នាក់ទី", "",),style: ThemsConstands.headline_4_semibold_18.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                                                          ),
                                                      
                                                        ),
                                                      ),
                                                    );
                                                  }
                                                ),
                                              ),
                                          
                                            ],
                                          );
                                        },
                                      ),
                                    );
                                  }
                                  else{
                                    return Center(
                                      child: EmptyWidget(
                                        title: "WE_DETECT".tr(),
                                        subtitle: "WE_DETECT_DES".tr(),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                                                        
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          //submitButtonWidget(context),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }
}
