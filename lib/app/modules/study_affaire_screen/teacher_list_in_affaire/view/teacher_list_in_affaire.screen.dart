import 'dart:async';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/empydata_widget/schedule_empty.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../core/resources/asset_resource.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../mixins/toast.dart';
import '../../../../models/study_affaire_model/get_current_grades/get_current_grades_affaire_model.dart';
import '../../../student_list_screen/widget/body_shimmer_widget.dart';
import '../../../student_list_screen/widget/contact_pairent_widget.dart';
import '../../../teacher_list_screen/bloc/teacher_list_bloc.dart';

class TeacherListInAffireScreen extends StatefulWidget {
    DataAllGradeAffaire dataGrade;
    String classId;
    String classname;
  TeacherListInAffireScreen({super.key,required this.dataGrade,required this.classname,required this.classId});

  @override
  State<TeacherListInAffireScreen> createState() =>
      _TeacherListInAffireScreenState();
}

class _TeacherListInAffireScreenState extends State<TeacherListInAffireScreen>
    with Toast {
  bool connection = true;
  StreamSubscription? sub;
  bool showDropClass = false;
  String? nameClass;
  bool? dbClick = false;
  Future<void> _launchLink(String url) async {
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    setState(() {
      nameClass = widget.classname;
    });
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {
        } else {}
      });
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Container(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10, left: 22, right: 22, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: SvgPicture.asset(
                              ImageAssets.chevron_left,
                              color: Colorconstand.neutralWhite,
                              width: 32,
                              height: 32,
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                if(showDropClass == false){
                                  setState(() {
                                    showDropClass = true;
                                  });
                                }
                                else{
                                  setState(() {
                                    showDropClass = false;
                                  });
                                }
                              });
                            },
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "LEDGERTEACHER".tr(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                                      color: Colorconstand.neutralWhite,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  margin:const EdgeInsets.only(left: 5),
                                  alignment: Alignment.center,
                                  child: Text(
                                    nameClass.toString(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(
                                      color: Colorconstand.neutralWhite,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  child: Icon(showDropClass==false?Icons.arrow_drop_down_rounded:Icons.arrow_drop_up_rounded,color: Colorconstand.neutralWhite,size: 35,),
                                )
                              ],
                            ),
                          ),
                         const SizedBox( width: 32, height: 32,
                            //child: SvgPicture.asset(ImageAssets.question_circle,color: Colorconstand.neutralWhite,width: 28,height: 28,),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          decoration: const BoxDecoration(
                              color: Colorconstand.neutralWhite,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12))),
                          child:  Column(
                            children: [
                              Expanded(
                                child: BlocBuilder<TeacherListBloc,TeacherListState>(
                                  builder: (context, state) {
                                    if (state is GetTeacherLoading) {
                                      return Column(
                                        children: const[
                                            BodyShimmerWidget(),
                                        ],
                                      );
                                    } else if (state
                                        is GetTeacherLoaded) {
                                      var datateacher = state
                                          .teacherListModel!.data;
                                      return datateacher!.isEmpty
                                          ? Column(
                                            children: [
                                              Expanded(child: Container()),
                                              ScheduleEmptyWidget(
                                                  title: "NOT_YET_AVAILABLE".tr(),
                                                  subTitle: "EMPTHY_CONTENT_DES".tr(),
                                                ),
                                              Expanded(child: Container()),
                                            ],
                                          )
                                          : Container(
                                            margin:const EdgeInsets.only(top: 15),
                                            child: ListView.separated(
                                                  padding:
                                              const EdgeInsets
                                                  .all(0),
                                                  itemCount: datateacher
                                              .length,
                                                  itemBuilder: (context,
                                              indexteacher) {
                                            return ContactPairentWidget(
                                              subjectName: translate=="km"?datateacher[ indexteacher].subjectName.toString():datateacher[ indexteacher].subjectNameEn.toString(),
                                                phone: datateacher[ indexteacher].teacherPhone.toString(),
                                                name: translate =="km"? datateacher[ indexteacher].teacherName .toString():datateacher[ indexteacher].teacherNameEn.toString(),
                                                onTap: () {
                                                  if (datateacher[ indexteacher] .teacherPhone ==  "") {
                                                    showMessage(
                                                        context: context,
                                                        title: "NOPHONENUM".tr(),
                                                        hight: 250.0,
                                                        width:25.0);
                                                  } else {
                                                    String phone = datateacher[indexteacher].teacherPhone!
                                                        .replaceAll( ' ', '');
                                                    _launchLink( "tel:$phone");
                                                  }
                                                },
                                                profile: datateacher[indexteacher].profileMedia!.fileShow ==
                                                      "" ||
                                                  datateacher[indexteacher].profileMedia!.fileShow ==
                                                      null
                                              ? datateacher[
                                                      indexteacher]
                                                  .profileMedia!
                                                  .fileThumbnail
                                                  .toString()
                                              : datateacher[
                                                      indexteacher]
                                                  .profileMedia!
                                                  .fileShow
                                                  .toString(),
                                                role: datateacher[
                                                  indexteacher]
                                              .subjectName
                                              .toString(),
                                              );
                                                  },
                                                  separatorBuilder:
                                              (context, ind) {
                                            return Container(
                                                margin:
                                                    const EdgeInsets
                                                            .only(
                                                        left:
                                                            100),
                                                child:
                                                    const Divider(
                                                  height: 1,
                                                  thickness:
                                                      0.5,
                                                ));
                                                  },
                                                ),
                                          );
                                    } else {
                                      return EmptyWidget(
                                          title: "WE_DETECT".tr(),
                                          subtitle: "WE_DETECT_DES".tr(),
                                        );
                                    }
                                  },
                                ),
                              ),
                            ],
                          )
                        ,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          // connection==true?Container():Positioned(
          //   bottom: 0,left: 0,right: 0,top: 0,
          //   child: Container(
          //     color:const Color(0x7B9C9595),
          //   ),
          // ),
          showDropClass ==true? Positioned(
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    showDropClass = false;
                  });
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey.withOpacity(.5),
                  child:  Center(child: Container(),),
                ),
              ),
            ):Container(),
            AnimatedPositioned(
              duration:const Duration(milliseconds:  200),
              top:showDropClass==true? 90:90,
              left:showDropClass==true? 40:80,
              right:showDropClass==true? 40:80,
              child: AnimatedContainer(
                duration:const Duration(milliseconds:  200),
                height:showDropClass==true? 150:0,
                padding:const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colorconstand.neutralWhite,
                  borderRadius: BorderRadius.circular(20)
                ),
                margin:const EdgeInsets.only(top: 15),
                child: GridView.builder(
                  shrinkWrap: true,
                  padding:const EdgeInsets.all(.0),
                    itemCount: widget.dataGrade.classes!.length,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5,mainAxisSpacing: 0),
                    itemBuilder: (BuildContext context, int indexClass) {
                    return Container(
                      margin:const EdgeInsets.only(right: 8,left: 8),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            showDropClass = false;
                            BlocProvider.of<TeacherListBloc>(context).add(GetTeacherListEvent(idClass:widget.dataGrade.classes![indexClass].classId.toString()));
                            nameClass = widget.dataGrade.classes![indexClass].className.toString();
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 40,width: 40,
                          decoration:const BoxDecoration(
                            color: Colorconstand.primaryColor,
                            shape: BoxShape.circle
                          ),
                          child: Container(
                            child: Text(widget.dataGrade.classes![indexClass].className.toString().replaceAll("ថ្នាក់ទី", ""),style: ThemsConstands.headline_6_semibold_14.copyWith(color:Colorconstand.neutralWhite,),textAlign: TextAlign.center,),
                          ),
                      
                        ),
                      ),
                    );
                  }
                ),
              ),
            ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }
}
