// ignore_for_file: avoid_unnecessary_containers
import 'dart:async';
import 'package:camis_teacher_application/app/bloc/check_update_version/bloc/check_version_update_bloc.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/state/current_grade/bloc/current_grade_bloc.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/state/daily_attendace/bloc/daily_attendance_affaire_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../../widget/comfirm_information/comfirm_phoneNumber.dart';
import '../../../../../widget/empydata_widget/empty_widget.dart';
import '../../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar_study_affair.dart';
import '../../../../../widget/widge_update.dart';
import '../../../../help/check_platform_device.dart';
import '../../../../models/grading_model/grading_model.dart';
import '../../../../service/api/approved_api/approved_api.dart';
import '../../../../service/api/grading_api/post_graeding_data_api.dart';
import '../../../../storages/get_storage.dart';
import '../../../../storages/key_storage.dart';
import '../../../../storages/user_storage.dart';
import '../../../account_confirm_screen/bloc/profile_user_bloc.dart';
import '../../../home_screen/e.homscreen.dart';
import '../../../home_screen/widget/button_quick_action_widget.dart';
import '../../../principle/report/view/principle_report_screen.dart';
import '../../affaire_check_attendance_class_screen/view/list_class_check_attendance_screen.dart';
import '../../enter_score_screen/state/bloc/student_affair_level_bloc.dart';
import '../../enter_score_screen/views/list_class_enter_score_screen.dart';
import '../../list_attendance_teacher/view/list_attendance_teacher_screen.dart';
import '../../list_class_in_grade/view/list_class_in_grade_screen.dart';
import '../widget/daily_attendance_affaire_widget.dart';

class AffaireHomeScreen extends StatefulWidget {
  const AffaireHomeScreen({Key? key}) : super(key: key);

  @override
  State<AffaireHomeScreen> createState() => _AffaireHomeScreenState();
}

class _AffaireHomeScreenState extends State<AffaireHomeScreen> {
  
 ///=========== Update Version ==============
  //////========= Change before update success =========
  final KeyStoragePref keyPref = KeyStoragePref();
  UserSecureStroage saveStoragePref = UserSecureStroage();
  bool updateVersion = false;
  String currentVersionIos = "2.5.0";
  String currentVersionAndroid = "2.5.0";
  ///========= Update success change in postman =========
  String releaseDateVersionIos = "28/Oct/2024";
  String releaseDateVersionAndroid = "28/Oct/2024";
///=========== Update Version ==============

  PageController? pagecMyclass;
  PageController? pagecNotification;
  double activeindexNotification = 0;
  double activeindexMyclass = 0;
  String? schoolName;
  String? instructorName;
  String? phoneNumber;
  String? phoneNumberInStoreLocal;
  int gender = 0;
  int? monthlocal;
  String? monthlocalCheck;
  int? daylocal;
  int? yearchecklocal;

  bool isLoading = false;
  String isInstructor= "";
  
  GradingData data = GradingData();
  List<StudentsData> studentData = [];

  StreamSubscription? internetconnection;
  bool isoffline = true;

  // Local Storage
  final GetStoragePref _getStoragePref = GetStoragePref();

  // Datetime && Timer
  DateTime now = DateTime.now();
  StreamSubscription? sub;
  bool isDisplay = true;
  bool showComfirmPhone = false;

  void loadCheckScore() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    var isIntructorPref = await _getStoragePref.getInstructor.then((value){
      if(value!=null){
          setState(() {
            isInstructor = value;
          });
      }
    });
    var isLocalDataPref =  await _getStoragePref.getScoreData.then((value){
      if(value.studentsData!=null){
          setState(() {
            data = value;
            studentData  = data.studentsData!.toList();
            print("value : ${value.studentsData!.length}");
            //isHasDoTask = true;
            var api = PostGradingApi();
              api.postListStudentSubjectExam(
                classID: data.classId!,
                subjectID: data.subjectId!,
                term: data.semester,
                examDate: "",
                type: data.type!,
                month: data.month,
                listStudentScore: studentData,
              ).then((value) {
                setState(() {
                });
                int totalScoreStudent = studentData.where((element) => element.score!=null).length;
                print("total Student : $totalScoreStudent");
                if(isInstructor=="1"&&totalScoreStudent==0){
                  ApprovedApi().postApproved(classId: data.classId, examObjectId: data.id, isApprove: 1, discription: "");
                }
                pref.remove("Data Score").then((value) => print("remove data score successfully"));
                pref.remove("Instructor").then((value) => print("remove instructor successfully"));
              });
          });
      }
    });
  }

  void getPhoneStore() async {
    final KeyStoragePref keyPref = KeyStoragePref();
    final SharedPreferences pref = await SharedPreferences.getInstance();
      phoneNumberInStoreLocal = pref.getString(keyPref.keyPhonePref) ?? "";
    if(phoneNumberInStoreLocal == ""|| phoneNumberInStoreLocal==null){
      setState(() {
        showComfirmPhone = true;
      });
    }
  }

  void getLocalData() async {

    var authStoragePref = await _getStoragePref.getJsonToken;
    setState(() {
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      schoolName = authStoragePref.schoolName.toString();
      if(authStoragePref.authUser == null){
      }
      else{
        instructorName = authStoragePref.authUser!.name;
        gender = authStoragePref.authUser!.gender;
        phoneNumber =  authStoragePref.authUser!.phone.toString();
      }
      
      if(monthlocal.toString().length == 1){
        monthlocalCheck = "0$monthlocal";
      }
      else{
        monthlocalCheck = monthlocal.toString();
      }
       BlocProvider.of<CheckVersionUpdateBloc>(context).add(CheckVersionUpdate());
       BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
       BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradeAffaire());
       BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: "${daylocal! <=9?"0$daylocal":daylocal}/$monthlocalCheck/$yearchecklocal"));
    });
  }

  @override
  void initState() {
    getPhoneStore();
    getLocalData();
     //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true) {
            loadCheckScore();
          }
        });
      });
      //=============Eend Check internet===============
      //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
    //=============Eend Check internet====================
    super.initState();
     if(isoffline == true){
            loadCheckScore();
      }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    pagecMyclass = PageController(viewportFraction: 0.92);
    pagecNotification = PageController(viewportFraction: 0.92);
    return Scaffold(
      bottomNavigationBar: isoffline==false || updateVersion == true ?Container(height: 0,):const BottomNavigateBarStudyAffair(
        isActive: 1,
      ),
      backgroundColor: const Color.fromARGB(255, 247, 248, 249),
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Container(
          color: Colorconstand.primaryColor,
          child: Stack(
            children: [
              Positioned(
                right: 0,
                top: 0,
                child: Container(
                  child: SvgPicture.asset("assets/images/Path_home_1.svg"),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: SafeArea(
                  bottom: false,
                  child: Container(
                    margin: const EdgeInsets.only(top: 0),
                    child: Column(
                      children: [
                        headerWidget(translate: translate),
                        const SizedBox(height: 22,),
                        Expanded(
                          child: BlocListener<CheckVersionUpdateBloc, CheckVersionUpdateState>(
                            listener: (context, state) {
                              if(state is CheckVersionUpdateLoaded){
                                  var data = state.checkUpdateModel.data;
                                  var plateform = checkPlatformDevice();
                                  if(plateform=="Android"){
                                    if(data!.versionAndroid.toString() == currentVersionAndroid || data.releaseDateAndroid == releaseDateVersionAndroid){
                                        setState(() {
                                          updateVersion = false;
                                        });
                                    }
                                    else{
                                       setState(() {
                                          updateVersion = true;
                                        });
                                    }
                                  }
                                  if(plateform=="ios"){
                                    if(data!.versionIos.toString() == currentVersionIos || data.releaseDateIos == releaseDateVersionIos){
                                        setState(() {
                                          updateVersion = false;
                                        });
                                    }
                                    else{
                                       setState(() {
                                          updateVersion = true;
                                        });
                                    }
                                  }
                                }
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                    color: Colorconstand.neutralWhite,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(16),
                                      topRight: Radius.circular(16),
                                    ),
                                  ),
                              child: Stack(
                                children: [
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    bottom: 0,
                                    child: Container(
                                      child: SvgPicture.asset("assets/images/back_daskboard_left.svg"),
                                    ),
                                  ),
                                  Positioned(
                                    left: 0,
                                    bottom: 0,
                                    child: Container(
                                      child: SvgPicture.asset("assets/images/back_daskboard_buttom.svg"),
                                    ),
                                  ),
                                  Positioned(
                                    top: 0,
                                    left: 0,
                                    right: 0,
                                    bottom: 0,
                                    child: BlurryContainer(
                                      borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16),
                                      ),
                                      padding: const EdgeInsets.all(0),
                                      blur: 35,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colorconstand.neutralWhite.withOpacity(0.5),
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(16),
                                            topRight: Radius.circular(16),
                                          ),
                                        ),
                                        child: Container(
                                          margin: const EdgeInsets.only(top: 5),   
                                          child: RefreshIndicator(
                                            onRefresh: () async {
                                              BlocProvider.of<CurrentGradeBloc>(context).add(GetCurrentDradeAffaire());
                                              BlocProvider.of<DailyAttendanceAffaireBloc>(context).add(GetDailyAttendanceAffaire(date: "${daylocal! <=9?"0$daylocal":daylocal}/$monthlocalCheck/$yearchecklocal"));
                                            },
                                            child: Column(
                                              children: [
                                                Expanded(
                                                  child: SingleChildScrollView(
                                                    physics:const AlwaysScrollableScrollPhysics(),
                                                    child: Container(
                                                      padding: const EdgeInsets.only(top: 3),                                      
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            margin: const EdgeInsets.only(right: 14, top: 10, bottom: 10),
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                Container(
                                                                  margin: const EdgeInsets.only(
                                                                    left: 14,
                                                                  ),
                                                                  child: Text(
                                                                    "សម្រង់វត្តមានតាមកម្រិត".tr(),
                                                                    style: ThemsConstands.headline_5_semibold_16
                                                                        .copyWith(color: Colorconstand.lightBlack, height: 1),
                                                                  ),
                                                                ),
                                                                GestureDetector(
                                                                  onTap: (() {
                                                                  Navigator.pushReplacement(
                                                                    context,
                                                                    PageRouteBuilder(
                                                                      pageBuilder: (context, animation1, animation2) =>
                                                                        const ListCheckAttendanceAffaireScreen(),
                                                                      transitionDuration: Duration.zero,
                                                                      reverseTransitionDuration: Duration.zero,
                                                                    ),
                                                                  );
                                                                }),
                                                                  child: Row(
                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                    children: [
                                                                      Container(
                                                                        child: Text(
                                                                          "SEEALL".tr(),
                                                                          style: ThemsConstands.headline_6_regular_14_20height.copyWith(
                                                                              color: Colorconstand.primaryColor, height: 1),
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        margin: const EdgeInsets.only(bottom: 2.5),
                                                                        child: SvgPicture.asset(
                                                                          ImageAssets.chevron_right_icon,
                                                                          width: 18,
                                                                          height: 18,
                                                                          color: Colorconstand.primaryColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          BlocBuilder<DailyAttendanceAffaireBloc, DailyAttendanceAffaireState>(
                                                            builder: (context, state) {
                                                              if(state is GetDailyAttendanceAffaireLoading){
                                                                return Container(
                                                                  child: ListView.builder(
                                                                    padding:const EdgeInsets.all(0),
                                                                    shrinkWrap: true,
                                                                    itemCount: 3,
                                                                    itemBuilder: (context, index) {
                                                                      return Container(
                                                                        margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 8),
                                                                        child: Shimmer.fromColors(
                                                                           baseColor:const Color(0xFF3688F4),highlightColor:const Color(0xFF3BFFF5),
                                                                          child: Container(height: 75,
                                                                          decoration: BoxDecoration(
                                                                            color:const Color(0x622195F3),
                                                                            borderRadius: BorderRadius.circular(12)
                                                                          ),
                                                                        )
                                                                        ),
                                                                      );
                                                                    },
                                                                  )
                                                                );
                                                              }
                                                              else if(state is DailyAttendanceAffaireLoaded){
                                                                var dataDailyAtt = state.dailyAttendanceAffaireModel!.data;
                                                                return WigetDailyAttendanceAffaire(dailyDataAttendance: dataDailyAtt!,);
                                                              }
                                                              else{
                                                                return Center(
                                                                  child: EmptyWidget(
                                                                    title: "WE_DETECT".tr(),
                                                                    subtitle: "WE_DETECT_DES".tr(),
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                          
                                                          Container(
                                                            margin: const EdgeInsets.only(right: 14, top: 20, bottom: 10),
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                Container(
                                                                  margin: const EdgeInsets.only(
                                                                    left: 14,
                                                                  ),
                                                                  child: Text(
                                                                    "សម្រង់ពិន្ទុតាមកម្រិត".tr(),
                                                                    style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBlack, height: 1),
                                                                  ),
                                                                ),
                                                                GestureDetector(
                                                                  onTap: (() {
                                                                    Navigator.pushReplacement(
                                                                      context,
                                                                      PageRouteBuilder(
                                                                        pageBuilder: (context, animation1, animation2) => ListClassEnterScoreScreen(activeIndex:0),
                                                                        transitionDuration: Duration.zero,
                                                                        reverseTransitionDuration: Duration.zero,
                                                                      ),
                                                                    );
                                                                  }),
                                                                  child: Row(
                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                    children: [
                                                                      Container(
                                                                        child: Text(
                                                                          "SEEALL".tr(),
                                                                          style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.primaryColor, height: 1),
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        margin: const EdgeInsets.only(bottom: 2.5),
                                                                        child: SvgPicture.asset(
                                                                          ImageAssets.chevron_right_icon,
                                                                          width: 18,
                                                                          height: 18,
                                                                          color: Colorconstand.primaryColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          BlocBuilder<CurrentGradeBloc, CurrentGradeState>(
                                                            builder: (context, state) {
                                                              if(state is CurrentGradeAffaireLoading){
                                                                return Container(
                                                                  margin:const EdgeInsets.only(top: 10,left: 18,right: 18),
                                                                  height: 80,
                                                                  child: ListView.builder(
                                                                    scrollDirection: Axis.horizontal,
                                                                    padding:const EdgeInsets.all(0),
                                                                    shrinkWrap: true,
                                                                    itemCount: 3,
                                                                    itemBuilder: (context, index) {
                                                                      return Container(
                                                                        margin:const EdgeInsets.symmetric(horizontal: 8, vertical: 0),
                                                                        child: Shimmer.fromColors(
                                                                           baseColor:const Color(0xFF3688F4),highlightColor:const Color(0xFF3BFFF5),
                                                                          child: Container(
                                                                            height:80, 
                                                                            width:80,
                                                                            decoration: BoxDecoration(
                                                                              color:const Color(0x622195F3),
                                                                              borderRadius: BorderRadius.circular(12)
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      );
                                                                    },
                                                                  )
                                                                );
                                                              }
                                                              else if(state is CurrentGradeAffaireLoaded){
                                                                var dataGrade  = state.currentGradeAffaireModel!.data;
                                                                return dataGrade!.isEmpty ? 
                                                                Container(
                                                                    child: EmptyWidget(
                                                                      title: "CAPTION_GRADE".tr(),
                                                                      subtitle: "CAPTION_NO_GRADE".tr(),
                                                                    ),
                                                                  )
                                                                :Container(
                                                                  height: 80,
                                                                  margin:const EdgeInsets.symmetric(horizontal: 8,vertical: 12),
                                                                  child: ListView.builder(
                                                                    padding:const EdgeInsets.all(0),
                                                                    scrollDirection: Axis.horizontal,
                                                                    shrinkWrap: true,
                                                                    itemCount: dataGrade.length,
                                                                    itemBuilder: (context, index) {
                                                                      return Container(
                                                                        margin:const EdgeInsets.only(right: 8,left: 8),
                                                                        child: GestureDetector(
                                                                          onTap: () {
                                                                            setState(() {
                                                                              BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel = dataGrade[index].level!;
                                                                            });
                                                                          
                                                                            debugPrint("studentLevel: ${BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel}");

                                                                            BlocProvider.of<StudentAffairLevelBloc>(context).add(
                                                                              GetStudentAffairClassByLevelEvent(
                                                                              month: "$monthlocal",
                                                                              semester: "SECOND_SEMESTER",
                                                                              type: "1",
                                                                              level: BlocProvider.of<StudentAffairLevelBloc>(context).studentLevel,
                                                                              page: "1",
                                                                              isRefresh: true
                                                                            ));
                                                                            Navigator.pushReplacement(context,
                                                                              PageRouteBuilder(
                                                                                pageBuilder: (context, animation1, animation2) => ListClassEnterScoreScreen(activeIndex: index,),
                                                                                transitionDuration: Duration.zero,
                                                                                reverseTransitionDuration: Duration.zero,
                                                                              ),
                                                                            );
                                                                          },
                                                                          child: Container(
                                                                            alignment: Alignment.center,
                                                                            height: 80,width: 80,
                                                                            decoration: BoxDecoration(
                                                                              color: Colorconstand.primaryColor,
                                                                              borderRadius: BorderRadius.circular(18)
                                                                            ),
                                                                            child: Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Container(
                                                                                  child: Text("GRADED".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color:Colorconstand.neutralWhite,)),
                                                                                ),
                                                                                const SizedBox(height: 8,),
                                                                                Container(
                                                                                  child: Text(dataGrade[index].level!.toString(),style: ThemsConstands.headline_2_semibold_24.copyWith(color:Colorconstand.neutralWhite,)),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      );
                                                                    },
                                                                  ),
                                                                );
                                                              }
                                                              else{
                                                                return Center(
                                                                  child: EmptyWidget(
                                                                    title: "WE_DETECT".tr(),
                                                                    subtitle: "WE_DETECT_DES".tr(),
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                          quickActionWidget()
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              isoffline == false || showComfirmPhone == true
                ?Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: Container(
                    color:const Color(0x55101010),
                  ),
                ):Container(height: 0,),
                showComfirmPhone == true?WidgetComfirmPhoneNumber(
                  gender: gender,
                  phoneNumber:phoneNumber == "0"?"":phoneNumber.toString(),
                  onComfirmSkip: (){
                    setState(() {
                      showComfirmPhone = false;
                      saveStoragePref.savePhoneNumber(keyPhoneNumber: "SKIP");
                    });
                  },
                  onComfirm: (){
                    setState(() {
                      showComfirmPhone = false;
                      if(phoneNumber == ""||phoneNumber==null || phoneNumber == "0"){
                      }
                      else{
                        saveStoragePref.savePhoneNumber(keyPhoneNumber:phoneNumber.toString());
                      }
                    });
                  },
                ):Container(height: 0,),
              AnimatedPositioned(
                bottom: isoffline == true ? -150 : 0,
                left: 0,
                right: 0,
                duration: const Duration(milliseconds: 500),
                child: const NoConnectWidget(),
              ),
              updateVersion==false?Container(): const Positioned(
                //top: 0,left: 0,right: 0,bottom: 0,
                child: WidgetUpdate(),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget emergencyNotificationWidget({required String translate}) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(
            top: 18,
            bottom: 18,
          ),
          height: 180,
          child: PageView.builder(
              controller: pagecNotification,
              scrollDirection: Axis.horizontal,
              itemCount: 4,
              onPageChanged: (index) {
                setState(() {
                  activeindexNotification = index.toDouble();
                });
              },
              itemBuilder: (context, index) {
                return const CardAnounment();
              }),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 16, top: 0),
          child: DotsIndicator(
            dotsCount: 4,
            position: activeindexNotification,
            decorator: DotsDecorator(
              color: Colorconstand.neutralWhite,
              activeColor: Colorconstand.mainColorForecolor,
              size: const Size(16.0, 6),
              activeSize: const Size(35.0, 6.0),
              activeShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
            ),
          ),
        ),
      ],
    );
  }

  Widget headerWidget({required String translate}) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 0),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 0),
              alignment: Alignment.centerLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: (){
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colorconstand.neutralWhite.withOpacity(0.2),
                              borderRadius: const BorderRadius.all(Radius.circular(10))),
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  child: Text(now.day.toString(),
                                      style: ThemsConstands.headline_4_medium_18.copyWith(
                                          color: Colorconstand.neutralWhite, height: 1.3)),
                                ),
                                const SizedBox(width: 8,),
                                Container(
                                  child: Text(
                                    translate == "km"
                                        ? monthNamesKh[now.month]
                                        : monthNamesEn[now.month],
                                    style: ThemsConstands.headline_4_medium_18.copyWith(
                                        color: Colorconstand.neutralWhite, height: 1),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 12,),
                      Expanded(
                        child: Container(
                          child: Text(
                            schoolName.toString(),
                            style: ThemsConstands.subtitle1_regular_16.copyWith(
                                color: Colorconstand.darkBackgroundsDisabled),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BlocConsumer<ProfileUserBloc, ProfileUserState>(
                    listener: (context, state) {
                      // TODO: implement listener
                    },
                    builder: (context, state) {
                      if (state is GetUserProfileLoading){
                        return Container(
                          child: Text("${"HELLO".tr()}......",style: ThemsConstands.headline3_semibold_20
                              .copyWith(color: Colorconstand.lightTextsRegular),),
                        );
                      }
                      else if(state is GetUserProfileLoaded){
                        var dataUser = state.profileModel!.data;
                        return Container(
                          child: Text(
                            gender == 1?"${"HELLO".tr()}${translate=="km"?"លោកគ្រូ":" Teacher"} ${translate == "km"?dataUser!.name:dataUser!.nameEn}"
                            :gender == 2?"${"HELLO".tr()}${translate=="km"?"អ្នកគ្រូ":" Teacher"} ${translate == "km"?dataUser!.name:dataUser!.nameEn}"
                                :"${"HELLO".tr()} ${translate == "km"?dataUser!.name:dataUser!.nameEn}",
                            style: ThemsConstands.headline3_semibold_20
                                .copyWith(color: Colorconstand.lightTextsRegular),
                            textAlign: TextAlign.left,overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }
                      else{
                        return Container(
                          child: Text("HELLO".tr(),style: ThemsConstands.headline3_semibold_20
                              .copyWith(color: Colorconstand.lightTextsRegular),),
                        );
                      }
                    },
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
      
        ],
      ),
    );
  }
  Widget quickActionWidget() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 14, bottom: 30, top: 22),
          alignment: Alignment.centerLeft,
          child: Text(
            "QUICKACTION".tr(),
            style: ThemsConstands.button_semibold_16
                .copyWith(color: Colorconstand.lightBlack, height: 1),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
            left: 14,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                      // Navigator.push(
                      //       context,
                      //       PageRouteBuilder(
                      //         pageBuilder: (context, animation1, animation2) =>
                      //            const ReportForPricipleAndStudyAffair(),
                      //         transitionDuration: Duration.zero,
                      //         reverseTransitionDuration: Duration.zero,
                      //       ),
                      //     );
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> ReportForPricipleAndStudyAffair(role: "6",)));
                    },
                    namebutton: "REPORT".tr(),
                    imageIcon: ImageAssets.folder_icon,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: () {
                     Navigator.push(context, MaterialPageRoute(builder: (context)=> const ListClassInGradeScreen(routFromScreen: 1,)));
                    },
                    namebutton: "LEDGERSTUDENT".tr(),
                    imageIcon: ImageAssets.profile_2users,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: ButttonQuickAction(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ListAttendanceTeacherScreen(principle: false,),
                        ),
                      );
                    },
                    namebutton: "LIST_TEACHER_ABSENT".tr(),
                    imageIcon: ImageAssets.DOCUMENT_ICON,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
