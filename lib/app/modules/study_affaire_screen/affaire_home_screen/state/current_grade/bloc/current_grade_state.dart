part of 'current_grade_bloc.dart';

abstract class CurrentGradeState extends Equatable {
  const CurrentGradeState();
  
  @override
  List<Object> get props => [];
}
class CurrentGradeAffaireLoading extends CurrentGradeState {}

class CurrentGradeAffaireLoaded extends CurrentGradeState {
  final GetCurrentGradeAffaireModel? currentGradeAffaireModel;
  const CurrentGradeAffaireLoaded({required this.currentGradeAffaireModel});
}

class CurrentGradePricipleLoaded extends CurrentGradeState {
  final GetCurrentGradeAffaireModel? currentGradeAffaireModel;
  const CurrentGradePricipleLoaded({required this.currentGradeAffaireModel});
}

class CurrentGradeAffaireError extends CurrentGradeState {}
class CurrentGradeInitial extends CurrentGradeState {}
