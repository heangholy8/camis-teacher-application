part of 'current_grade_bloc.dart';

abstract class CurrentGradeEvent extends Equatable {
  const CurrentGradeEvent();

  @override
  List<Object> get props => [];
}

class GetCurrentDradeAffaire extends CurrentGradeEvent{}
class GetCurrentDradePriciple extends CurrentGradeEvent{}
