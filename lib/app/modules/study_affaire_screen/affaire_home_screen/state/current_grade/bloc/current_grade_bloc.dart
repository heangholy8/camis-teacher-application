import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/study_affaire_model/get_current_grades/get_current_grades_affaire_model.dart';
import '../../../../../../service/api/get_current_grades_affaire_api/current_grades_affaire.dart';
part 'current_grade_event.dart';
part 'current_grade_state.dart';

class CurrentGradeBloc extends Bloc<CurrentGradeEvent, CurrentGradeState> {
  final GetCurrentGradeAffaireApi currentGradeData;
  CurrentGradeBloc({required this.currentGradeData}) : super(CurrentGradeInitial()) {
    on<GetCurrentDradeAffaire>((event, emit) async{
      emit(CurrentGradeAffaireLoading());
      try {
        var dataGradeAttendance = await currentGradeData.getCurrentGradeAffaireApi();
        emit(CurrentGradeAffaireLoaded(currentGradeAffaireModel: dataGradeAttendance));
      } catch (e) {
        emit(CurrentGradeAffaireError());
      }
    });
    on<GetCurrentDradePriciple>((event, emit) async{
      emit(CurrentGradeAffaireLoading());
      try {
        var dataGradeAttendance = await currentGradeData.getCurrentGradePrincipleApi();
        emit(CurrentGradePricipleLoaded(currentGradeAffaireModel: dataGradeAttendance));
      } catch (e) {
        emit(CurrentGradeAffaireError());
      }
    });
  }
}
