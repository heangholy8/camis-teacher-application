part of 'daily_attendance_affaire_bloc.dart';

abstract class DailyAttendanceAffaireState extends Equatable {
  const DailyAttendanceAffaireState();
  
  @override
  List<Object> get props => [];
}


class GetDailyAttendanceAffaireLoading extends DailyAttendanceAffaireState{}

class DailyAttendanceAffaireLoaded extends DailyAttendanceAffaireState {
  final GetDailyAttendanceAffaireModel? dailyAttendanceAffaireModel;
  const DailyAttendanceAffaireLoaded({required this.dailyAttendanceAffaireModel});
}

class DailyAttendanceAffaireError extends DailyAttendanceAffaireState {}

class DailyAttendanceAffaireInitial extends DailyAttendanceAffaireState {}
