import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../models/study_affaire_model/get_daily_attendance/get_daily_attendance_model.dart';
import '../../../../../../service/api/get_daily_attendance_affaire_api/daily_attendance_affaire.dart';
part 'daily_attendance_affaire_event.dart';
part 'daily_attendance_affaire_state.dart';

class DailyAttendanceAffaireBloc extends Bloc<DailyAttendanceAffaireEvent, DailyAttendanceAffaireState> {
  final GetDailyAtytendanceAffaireApi getDailyAttendanceAffaire;
  DailyAttendanceAffaireBloc({required this.getDailyAttendanceAffaire}) : super(DailyAttendanceAffaireInitial()) {
    on<GetDailyAttendanceAffaire>((event, emit) async{
      emit(GetDailyAttendanceAffaireLoading());
      try {
        var dataDailyAttendance = await getDailyAttendanceAffaire.getDailyAttendanceAffaireApi(date: event.date);
        emit(DailyAttendanceAffaireLoaded(dailyAttendanceAffaireModel: dataDailyAttendance));
      } catch (e) {
        print("fasdfyyy$e");
        emit(DailyAttendanceAffaireError());
      }
    });
  }
}
