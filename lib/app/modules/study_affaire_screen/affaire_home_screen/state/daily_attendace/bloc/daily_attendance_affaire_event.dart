part of 'daily_attendance_affaire_bloc.dart';

abstract class DailyAttendanceAffaireEvent extends Equatable {
  const DailyAttendanceAffaireEvent();

  @override
  List<Object> get props => [];
}

class GetDailyAttendanceAffaire extends DailyAttendanceAffaireEvent{
  final String date;
  const GetDailyAttendanceAffaire({required this.date});
}
