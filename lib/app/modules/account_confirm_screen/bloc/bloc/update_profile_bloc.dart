import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../models/profile_model/update_user_model.dart';
import '../../../../service/api/profile_api/post_user_profile.dart';


part 'update_profile_event.dart';
part 'update_profile_state.dart';

class UpdateProfileBloc extends Bloc<UpdateProfileEvent, UpdateProfileState> {
  final UpdateProfileUserApi upDateProfile;
  UpdateProfileBloc({required this.upDateProfile}) : super(UpdateProfileInitial()) {
    on<UpdateProfileUserEvent>((event, emit) async {
      emit(UpdateProfileLoading());
      try{
         var data = await upDateProfile.updateUserProfileImageRequestApi(image: event.imageFile!);
         emit(UpdateProfileLoaded(infoModel: data));
      }
      catch (e){
        emit(UpdateProfileError());
        print(e);
      }
    });
  }
}
