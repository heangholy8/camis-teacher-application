import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/profile_api/get_user_profile_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../../models/profile_model/profile_model.dart';

part 'profile_user_event.dart';
part 'profile_user_state.dart';

class ProfileUserBloc extends Bloc<ProfileUserEvent, ProfileUserState> {
  final GetProfileUserApi profileUserApi;
  ProfileUserBloc({required this.profileUserApi,}) : super(ProfileUserInitial()) {
    on<GetUserProfileEvent>((event, emit) async {
      emit(GetUserProfileLoading());
      try {
        var datauser = await profileUserApi.getUserProfileRequestApi(event.context);
        emit(GetUserProfileLoaded(profileModel: datauser,));
      } catch (e) {
        print(e);
        emit(GetUserProfileError());
      }
    });
  }
}
