part of 'profile_user_bloc.dart';

abstract class ProfileUserEvent extends Equatable {
  const ProfileUserEvent();

  @override
  List<Object> get props => [];
}

class GetUserProfileEvent extends ProfileUserEvent{
  BuildContext context;
  GetUserProfileEvent({required this.context});
}

