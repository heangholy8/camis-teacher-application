part of 'profile_user_bloc.dart';

abstract class ProfileUserState extends Equatable {
  const ProfileUserState();
  
  @override
  List<Object> get props => [];
}

class ProfileUserInitial extends ProfileUserState {}

class GetUserProfileLoading extends ProfileUserState{}
class GetUserProfileLoaded extends ProfileUserState{
  final ProfileModel? profileModel;
  const GetUserProfileLoaded({required this.profileModel,});
}
class GetUserProfileError extends ProfileUserState{}
