// ignore_for_file: use_build_context_synchronously
import 'dart:async';
import 'dart:io';
import 'package:camis_teacher_application/app/modules/principle/principle_home_screen/view/principle_home_screen.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/affaire_home_screen/view/affaire_home_screen.dart';
import 'package:camis_teacher_application/app/storages/user_storage.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget_custom.dart';
import 'package:camis_teacher_application/widget/internet_connect_widget/internet_connect_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../widget/take_upload_image.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../mixins/toast.dart';
import '../../../routes/app_route.dart';
import '../../../service/api/profile_api/post_user_profile.dart';
import '../../../storages/get_storage.dart';
import '../bloc/bloc/update_profile_bloc.dart';
import '../bloc/profile_user_bloc.dart';

class AccountConfirmScreen extends StatefulWidget {
  const AccountConfirmScreen({Key? key}) : super(key: key);

  @override
  State<AccountConfirmScreen> createState() => _AccountConfirmScreenState();
}

class _AccountConfirmScreenState extends State<AccountConfirmScreen>
    with Toast {
  final GetStoragePref _getStoragePref = GetStoragePref();
  final UserSecureStroage _saveStoragePref = UserSecureStroage();
  final UpdateProfileUserApi updateinfo = UpdateProfileUserApi();
  // final guardinaConfirmAccountModel = GuardinaConfirmAccountModel.generate();
  TextEditingController fullnameteController = TextEditingController();
  final roleGuardianteController = TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController genderController = TextEditingController();
  final TextEditingController roleController = TextEditingController();

  final formKey = GlobalKey<FormState>();
  GetStoragePref _prefs = GetStoragePref();
  final ImagePicker _picker = ImagePicker();
  final scrollController = ScrollController();
  File? _image;
  XFile? file;
  bool loadingcontinu = false;
  String? phoneNumber;
  bool connection = true;
  StreamSubscription? sub;
  bool focusFirstName = false;
  bool focusLastName = false;
  bool focusGender = false;
  bool focusRole = false;

  String? roleLogin;

  void callDataLocal() async{
    var dataAuth = await _prefs.getJsonToken;
    setState(() {
      if(dataAuth.authUser == null){
    }
    else{
      roleLogin = dataAuth.authUser!.role.toString();
    } 
    });
  }

  @override
  void initState() {
    callDataLocal();
    BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
    setState(() {
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("fasdfasdf$roleLogin");
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: WillPopScope(
        onWillPop: () => exit(0),
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(
                "assets/images/Oval.png",
                width: MediaQuery.of(context).size.width / 1.7,
              ),
            ),
            Positioned(
              top: 150,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (1).png",
              ),
            ),
            Positioned(
              bottom: 35,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (3).png",
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height / 5,
              right: MediaQuery.of(context).size.width / 4,
              child: Center(
                  child: Image.asset(
                "assets/images/Oval (2).png",
              )),
            ),
            Container(
              margin: const EdgeInsets.only(
                top: 70,
              ),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                      left: 18,
                    ),
                    child: Text(
                      'INFORMATIONVERIFICATION'.tr(),
                      style: ThemsConstands.headline_2_semibold_24
                          .copyWith(color: Colorconstand.neutralWhite),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 28, left: 22, right: 22),
                    child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            style: const TextStyle(
                              color: Colorconstand.neutralWhite,
                            ), //style for all textspan
                            children: [
                              TextSpan(
                                  text: 'WELCOME'.tr(),
                                  style: ThemsConstands.subtitle1_regular_16),
                              TextSpan(
                                text: "CAMEMIS",
                                style: ThemsConstands.headline_6_semibold_14
                                    .copyWith(
                                        color: Colorconstand.neutralWhite),
                              ),
                              const TextSpan(text: "\n"),
                              TextSpan(
                                  text: 'VERIFYDES'.tr(),
                                  style: ThemsConstands.subtitle1_regular_16),
                            ])),
                  ),
                  /* ----------------------- body ---------------------------------- */
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 30),
                      decoration: const BoxDecoration(
                        color: Colorconstand.neutralSecondary,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          topRight: Radius.circular(12.0)
                        ),
                      ),
                      child: ListView(
                        shrinkWrap: true,
                        controller: scrollController,
                        physics: const ClampingScrollPhysics(),
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        children: [
                          /* ----------------------- Profile Icon ---------------------------------- */
                          GestureDetector(
                            onTap: () {
                              setState(
                                () {
                                  showModalBottomSheet(
                                    backgroundColor: Colors.transparent,
                                    context: context,
                                    builder: (BuildContext bc) {
                                      return ShowPiker(
                                        onPressedCamera: () {
                                          _imgFromCamera();
                                          Navigator.of(context).pop();
                                        },
                                        onPressedGalary: () {
                                          _imgFromGallery();
                                          Navigator.of(context).pop();
                                        },
                                      );
                                    },
                                  );
                                },
                              );
                            },
                            child:BlocConsumer<ProfileUserBloc, ProfileUserState>(
                              listener: (context, state) {
                                if (state is GetUserProfileLoaded) {
                                  var data = state.profileModel!.data;
                                  firstNameController.text = data!.firstname.toString();
                                  lastNameController.text = data.lastname.toString();
                                  genderController.text = data.genderName.toString();
                                  roleController.text = data.teachingSubject!.isEmpty && data.instructorClass!.isEmpty ? "---": 
                                  data.instructorClass!.isEmpty ? 
                                    data.teachingSubject!.isNotEmpty? "${"ISTEACHER".tr()}: ${data.teachingSubject![0].name}" 
                                      : "${"ISINSTRUCTOR".tr()}: ${data.instructorClass![0].className}"
                                        :"${"ISINSTRUCTOR".tr()}: ${data.instructorClass![0].className} && ${"ISTEACHER".tr()}: ${data.teachingSubject![0].name}";                        
                                }
                              }, 
                              builder: ((BuildContext context, state) {
                                if (state is GetUserProfileLoaded) {
                                  var dataProfile = state.profileModel!.data;
                                  return Container(
                                    width: 80,
                                    height: 80,
                                    margin: const EdgeInsets.only(
                                        top: 25, bottom: 0),
                                    alignment: Alignment.center,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        CircleAvatar(
                                          radius: 40, // Image radius
                                          child: ClipOval(
                                            child: _image == null
                                                ? Image.network(
                                                    dataProfile!
                                                        .profileMedia!.fileShow
                                                        .toString(),
                                                    width: 80,
                                                    height: 80,
                                                    fit: BoxFit.cover,
                                                  )
                                                : Image.file(
                                                    _image!,
                                                    width: 80,
                                                    height: 80,
                                                    fit: BoxFit.cover,
                                                  ),
                                          ),
                                        ),
                                        _image == null
                                            ? Container(
                                                width: 80,
                                                height: 80,
                                                decoration: const BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.black12),
                                              )
                                            : const SizedBox(),
                                        _image == null
                                            ? const Center(
                                                child: Icon(
                                                  Icons.photo_camera_outlined,
                                                  color: Colorconstand
                                                      .neutralWhite,
                                                  size: 28,
                                                ),
                                              )
                                            : const SizedBox()
                                      ],
                                    ),
                                  );
                                }
                                return Container();
                              }),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: MediaQuery.of(context).size.width / 4,
                            ),
                            width: 150,
                            child: TextButton(
                              onPressed: () {
                                showModalBottomSheet(
                                  backgroundColor: Colors.transparent,
                                  context: context,
                                  builder: (BuildContext bc) {
                                    return ShowPiker(
                                      onPressedCamera: () {
                                        _imgFromCamera();
                                        Navigator.of(context).pop();
                                      },
                                      onPressedGalary: () {
                                        _imgFromGallery();
                                        Navigator.of(context).pop();
                                      },
                                    );
                                  },
                                );
                              },
                              child: Text(
                                'UPLOADPHOTO'.tr(),
                                style:
                                    ThemsConstands.headline6_medium_14.copyWith(
                                  color: Colorconstand.primaryColor,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                          /* ----------------------- Guardian Name ---------------------------------- */

                          Container(
                            margin: const EdgeInsets.only(top: 16),
                            padding: EdgeInsets.zero,
                            height: 48,
                            decoration: BoxDecoration(
                              color: Colorconstand.lightGohan,
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: Colorconstand.neutralGrey, width: 1),
                            ),
                            child: Focus(
                              onFocusChange: (hasfocus) {},
                              child: TextFormField(
                                readOnly: true,
                                controller: firstNameController,
                                onChanged: ((value) {}),
                                style: ThemsConstands.headline_5_medium_16
                                    .copyWith(color: Colors.black),
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(16.0),
                                  ),
                                  contentPadding: const EdgeInsets.only(
                                      top: 4, bottom: 5, left: 18, right: 18),
                                  labelText: 'FIRSTNAME'.tr(),
                                  labelStyle: ThemsConstands
                                      .subtitle1_regular_16
                                      .copyWith(
                                          color: Colorconstand.lightTrunks),
                                  enabledBorder: const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent)),
                                  focusedBorder: const UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 16),
                            padding: EdgeInsets.zero,
                            height: 48,
                            decoration: BoxDecoration(
                              color: Colorconstand.lightGohan,
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                  color: Colorconstand.neutralGrey, width: 1),
                            ),
                            child: Focus(
                              onFocusChange: (hasfocus) {
                                setState(() {
                                  hasfocus
                                      ? focusLastName = true
                                      : focusLastName = false;
                                });
                              },
                              child: TextFormField(
                                readOnly: true,
                                controller: lastNameController,
                                onChanged: ((value) {}),
                                style: ThemsConstands.headline_5_medium_16
                                    .copyWith(color: Colors.black),
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(16.0)),
                                  contentPadding: const EdgeInsets.only(
                                      top: 4, bottom: 5, left: 18, right: 18),
                                  labelText: 'SURNAME'.tr(),
                                  labelStyle: ThemsConstands
                                      .subtitle1_regular_16
                                      .copyWith(
                                          color: Colorconstand.lightTrunks),
                                  enabledBorder: const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent)),
                                  focusedBorder: const UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 16),
                            padding: EdgeInsets.zero,
                            height: 48,
                            decoration: BoxDecoration(
                              color: Colorconstand.lightGohan,
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                  color: Colorconstand.neutralGrey, width: 1),
                            ),
                            child: Focus(
                              onFocusChange: (hasfocus) {
                                setState(() {
                                  hasfocus
                                      ? focusGender = true
                                      : focusGender = false;
                                });
                              },
                              child: TextFormField(
                                readOnly: true,
                                controller: genderController,
                                onChanged: ((value) {}),
                                style: ThemsConstands.headline_5_medium_16
                                    .copyWith(color: Colors.black),
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(16.0)),
                                  contentPadding: const EdgeInsets.only(
                                      top: 4, bottom: 5, left: 18, right: 18),
                                  labelText: 'GENDER'.tr(),
                                  labelStyle: ThemsConstands
                                      .subtitle1_regular_16
                                      .copyWith(
                                          color: Colorconstand.lightTrunks),
                                  enabledBorder: const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent)),
                                  focusedBorder: const UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 16),
                            padding: EdgeInsets.zero,
                            height: 48,
                            decoration: BoxDecoration(
                              color: Colorconstand.lightGohan,
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                  color: Colorconstand.neutralGrey, width: 1),
                            ),
                            child: Focus(
                              onFocusChange: (hasfocus) {
                                setState(() {
                                  hasfocus
                                      ? focusRole = true
                                      : focusRole = false;
                                });
                              },
                              child: TextFormField(
                                readOnly: true,
                                controller: roleController,
                                onChanged: ((value) {}),
                                style: ThemsConstands.headline_5_medium_16
                                    .copyWith(color: Colors.black),
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(16.0)),
                                  contentPadding: const EdgeInsets.only(
                                      top: 4, bottom: 5, left: 18, right: 18),
                                  labelText: 'ROLE'.tr(),
                                  labelStyle: ThemsConstands
                                      .subtitle1_regular_16
                                      .copyWith(
                                          color: Colorconstand.lightTrunks),
                                  enabledBorder: const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent)),
                                  focusedBorder: const UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16.0 * 2,
                          ),
                          ButtonWidgetCustom(
                            heightButton: 45,
                            buttonColor: Colorconstand.primaryColor,
                            onTap: () async {
                              var data = await _getStoragePref.getJsonToken;
                              if (_image != null) {
                                BlocProvider.of<UpdateProfileBloc>(context).add(
                                    UpdateProfileUserEvent(imageFile: _image));
                                // if (data.authUser!.role == 2) {
                                //   Navigator.pushNamedAndRemoveUntil(
                                //       context,
                                //       Routes.SETMONITIORSCREEN,
                                //       (Route<dynamic> route) => false);
                                //   debugPrint("Instructor Class");
                                // } else {
                                  if(roleLogin == "6"){
                                      Navigator.pushAndRemoveUntil(
                                        context,
                                        PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) =>
                                              const AffaireHomeScreen(),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                        (route) => false);
                                  }
                                  else if(roleLogin == "1"){
                                     Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) =>
                                              const PrincipleHomeScreen(),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),(route) => false);
                                  }
                                  else{
                                    Navigator.pushNamedAndRemoveUntil(
                                      context,
                                      Routes.HOMESCREEN,
                                      (Route<dynamic> route) => false);
                                      debugPrint("Teacher Subject");
                                  }
                                // }
                              } else {
                                // if (data.authUser!.role == 2) {
                                //   Navigator.pushNamedAndRemoveUntil(
                                //       context,
                                //       Routes.SETMONITIORSCREEN,
                                //       (Route<dynamic> route) => false);
                                //   debugPrint("Instructor Class");
                                // } else {
                                  if(roleLogin == "6"){
                                      Navigator.pushAndRemoveUntil(
                                        context,
                                        PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) =>
                                              const AffaireHomeScreen(),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                        (route) => false);
                                  }
                                  else if(roleLogin == "1"){
                                     Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) =>
                                              const PrincipleHomeScreen(),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),(route) => false);
                                  }
                                  else{
                                    Navigator.pushNamedAndRemoveUntil(
                                      context,
                                      Routes.HOMESCREEN,
                                      (Route<dynamic> route) => false);
                                      debugPrint("Teacher Subject");
                                  }
                                // }
                              }
                              _saveStoragePref.saveConfirmInformation(
                                  isConfirmed: "Comfired Success");
                            },
                            panddinHorButton: 22,
                            panddingVerButton: 12,
                            radiusButton: 8,
                            textStyleButton: ThemsConstands.button_semibold_16
                                .copyWith(color: Colorconstand.neutralWhite),
                            title: 'CONTINUE'.tr(),
                          ),
                          const SizedBox(
                            height: 80,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
          ],
        ),
      ),
    );
  }

  void _imgFromCamera() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
    });
  }

  void _imgFromGallery() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
      print("path-path " + file!.path.toString());
    });
  }
}
