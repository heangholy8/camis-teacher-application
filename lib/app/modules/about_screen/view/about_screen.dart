import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../widget/button_widget/button_widget_custom.dart';
import '../../../mixins/toast.dart';
import '../../../storages/remove_storage.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({super.key});

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> with Toast{
  final RemoveStoragePref _removeStorage = RemoveStoragePref();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor.withOpacity(0.8),
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              // Positioned(
              //   top: 0,
              //   right: 0,
              //   child: Image.asset(ImageAssets.Path_22),
              // ),
              // Positioned(
              //   bottom: -10,
              //   left: 0,
              //   child: Image.asset(ImageAssets.Path_21),
              // ),
              // Positioned(
              //   bottom: 0,
              //   right: 0,
              //   child: Image.asset(ImageAssets.Path_19_About),
              // ),
              // Positioned(
              //   top: 0,
              //   bottom: 0,
              //   left: 0,
              //   right: 0,
              //   child: BackdropFilter(
              //     filter: ImageFilter.blur(sigmaX: 60, sigmaY: 60),
              //     // filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
      
              //     child: Container(
              //       color: Colorconstand.primaryColor,
              //       child: Column(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           Container(
              //               margin: const EdgeInsets.only(
              //                   top: 50, left: 26, right: 26, bottom: 20),
              //               child:
              //                   const TopBarOneLineWidget(titleTopBar: "About")),
              //           // AboutList(
              //           //   title: "About Us",
              //           //   subtitle: "ver. 2.234",
              //           //   onPressed: () {},
              //           // ),
              //           // const Divider(
              //           //   indent: 20,
              //           //   endIndent: 20,
              //           //   height: 1,
              //           //   thickness: 1,
              //           // ),
              //           // AboutList(
              //           //   title: "Connect with us!",
              //           //   onPressed: () {},
              //           // ),
              //           // const Divider(
              //           //   indent: 20,
              //           //   endIndent: 20,
              //           //   height: 2,
              //           //   thickness: 0.5,
              //           // ),
              //           // AboutList(
              //           //   title: "Terms of Services",
              //           //   onPressed: () {},
              //           // ),
              //           // const Divider(
              //           //   indent: 20,
              //           //   endIndent: 20,
              //           //   height: 1,
              //           //   thickness: 1,
              //           // ),
              //           // AboutList(
              //           //   title: "Acknowledgement",
              //           //   onPressed: () {},
              //           // ),
              //           // const Divider(
              //           //   indent: 20,
              //           //   endIndent: 20,
              //           //   height: 2,
              //           //   thickness: 1,
              //           // ),
              //         ],
              //       ),
              //     ),
              //   ),
              // ),
              Container(
                 // color: Colorconstand.primaryColor.withOpacity(0.8),
                  child: Container(
                      margin: const EdgeInsets.only(
                          top: 10, left: 26, right: 26, bottom: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: const Icon(
                              Icons.arrow_back_ios_new,
                              size: 24,
                              color: Colorconstand.lightBackgroundsWhite,
                            ),
                          ),
                          Expanded(
                              child: Text(
                            "អំពី CAMEMIS",
                            style: ThemsConstands.headline3_semibold_20
                                .copyWith(
                              color: Colorconstand.lightBackgroundsWhite,
                            ),
                            textAlign: TextAlign.center,
                          )),
                          Container(width: 25,)
                        ],
                      ))),
              Expanded(
                child: Container(
                  color: Colorconstand.primaryColor,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(ImageAssets.logoCamis),
                          const SizedBox(
                            width: 16.38,
                          ),
                          Text(
                            "CAMEMIS",
                            style: ThemsConstands.headline_1_semibold_32.copyWith(
                                color: Colorconstand.lightBackgroundsWhite,
                                fontSize: 36,
                                letterSpacing: 2.8),
                          )
                        ],
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 79),
                          child: Text(
                            "Recommended for Teachers",
                            style: ThemsConstands.caption_regular_12
                                .copyWith(color: Colorconstand.neutralSecondary),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 16.25),
                width: MediaQuery.of(context).size.width,
                color: Colorconstand.primaryColor,
                child: Text(
                  "ver. 2.5.0\n©️2024 CAMIS Solutions. \nAll Rights Reserved.",
                  textAlign: TextAlign.center,
                  style: ThemsConstands.headline6_regular_14_24height
                      .copyWith(color: Colorconstand.neutralWhite),
                ),
              ),
              Container(
                margin:
                    const EdgeInsets.only(top: 10, bottom: 5),
                child: ButtonWidgetCustom(
                  buttonColor: Colorconstand.primaryColor
                      .withOpacity(1),
                  title: 'LOGOUT'.tr(),
                  textStyleButton: ThemsConstands
                      .headline_6_semibold_14
                      .copyWith(
                          color: Colorconstand.neutralWhite),
                  panddingVerButton: 5,
                  onTap: () {
                    showAlertLogout(
                        onSubmit: () {
                          _removeStorage.removeToken(context);
                        },
                        title: "DO_YOU_WANT_TO_LEAVE".tr(),
                        context: context);
                  },
                  weightButton: 140,
                  panddinHorButton: 12,
                  radiusButton: 10,
                ),
              ),
              Container(
                height: 70,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(ImageAssets.globe_icon),
                    const SizedBox(width: 16.38),
                    InkWell(
                      onTap: _launchURL,
                      child: const Text(
                        "Learn more",
                        style: TextStyle(
                          color: Colorconstand.neutralWhite,
                          decoration: TextDecoration.underline,
                          decorationThickness: 1,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _launchURL() async {
    const url = 'https://camis-info.camemis-learn.com/';
    if (await launchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }
}

class AboutList extends StatelessWidget {
  final String title;
  void Function()? onPressed;
  final String subtitle;
  AboutList({Key? key, required this.title, this.onPressed, this.subtitle = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      padding: const EdgeInsets.all(0),
      child: Container(
        margin: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 12),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      title,
                      textAlign: TextAlign.justify,
                      style: ThemsConstands.headline_4_medium_18
                          .copyWith(color: Colorconstand.darkTextsRegular),
                    ),
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 17,
                    color: Colorconstand.darkTextsRegular,
                  )
                ],
              ),
            ),
            subtitle == null
                ? const SizedBox(
                    height: 2.0,
                  )
                : const SizedBox(),
            subtitle == null
                ? Text(
                    subtitle,
                    style: ThemsConstands.caption_regular_12
                        .copyWith(color: Colorconstand.neutralDarkGrey),
                  )
                : const SizedBox(),
            const SizedBox(
              height: 12.0,
            )
          ],
        ),
      ),
    );
  }
}
