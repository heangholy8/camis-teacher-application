// ignore_for_file: unnecessary_null_comparison

import 'package:camis_teacher_application/app/service/api/class_api/get_class_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../models/student_list_model/list_student_in_class_model.dart';
import '../../../service/api/student_in_class_list/get_Student_In_Class_Api.dart';

part 'assignmonitor_event.dart';
part 'assignmonitor_state.dart';

class AssignmonitorBloc extends Bloc<AssignmonitorEvent, AssignmonitorState> {
  final GetClassApi getclass = GetClassApi();
  final GetStudentInClassApi getClassStudent;
  String? gradeNameKh;
  String? gradeNameEn;

  AssignmonitorBloc({required this.getClassStudent})
      : super(AssignmonitorInitial()) {
    on<GetStudentListEvent>((event, emit) async {
      emit(GetStudentInClassLoading());
      try {
        await getclass.getClassRequestApi().then((value) async {
          var classes = value.data!.teachingClasses;
          for (var listClasses in classes!) {
            if (listClasses.isInstructor == 1) {
              var getStudentList =
                  await getClassStudent.getStudentInClassRequestApi(
                      idClass: listClasses.id.toString());
              gradeNameKh = listClasses.name.toString();
              gradeNameEn = listClasses.nameEn.toString();
              emit(GetStudentInClassSuccess(
                  studentinclassModel: getStudentList));
            }
          }
        });
      } catch (e) {
        emit(GetStudentInClassError());
      }
    });
  }
}
