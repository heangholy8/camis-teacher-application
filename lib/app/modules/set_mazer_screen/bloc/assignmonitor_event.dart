part of 'assignmonitor_bloc.dart';

abstract class AssignmonitorEvent extends Equatable {
  const AssignmonitorEvent();

  @override
  List<Object> get props => [];
}

class SelectedMonitorEvent extends AssignmonitorEvent {
  final List<String> listSelected;

  const SelectedMonitorEvent({required this.listSelected});
}

class GetStudentListEvent extends AssignmonitorEvent {
  
  const GetStudentListEvent();
}

