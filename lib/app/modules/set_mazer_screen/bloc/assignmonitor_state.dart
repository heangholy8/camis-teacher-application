part of 'assignmonitor_bloc.dart';

abstract class AssignmonitorState extends Equatable {
  const AssignmonitorState();

  @override
  List<Object> get props => [];
}

class AssignmonitorInitial extends AssignmonitorState {}

class SelectedMonitorState extends AssignmonitorState {}

class SelectedFullMonitorState extends AssignmonitorState {}

/// GetStudentInClass
class GetStudentInClassLoading extends AssignmonitorState {}

class SetClassMonitorLoading extends AssignmonitorState {}

class GetStudentInClassSuccess extends AssignmonitorState {
  final StudentInClassListModel? studentinclassModel;
  const GetStudentInClassSuccess({required this.studentinclassModel});
}

class SetClassMonitorClassSuccess extends AssignmonitorState {}

class SetClassMonitorClassError extends AssignmonitorState {
  final String error;
  const SetClassMonitorClassError({required this.error});
}

class GetStudentInClassError extends AssignmonitorState {}
