part of 'set_class_monitor_bloc.dart';

abstract class SetClassMonitorState extends Equatable {
  const SetClassMonitorState();

  @override
  List<Object> get props => [];
}

class SetClassMonitorInitial extends SetClassMonitorState {}

class SetClassMonitorLoadingState extends SetClassMonitorState {}

class SetClassMonitorSuccessState extends SetClassMonitorState {
  final StudentInClassListModel setClassMonitorModel;
  const SetClassMonitorSuccessState({required this.setClassMonitorModel});
}

class SetClassMonitorErrorState extends SetClassMonitorState {
  final String error;
  const SetClassMonitorErrorState({required this.error});
}
