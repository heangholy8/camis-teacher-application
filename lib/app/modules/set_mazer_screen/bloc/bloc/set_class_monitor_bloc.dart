import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/student_in_class_list/get_Student_In_Class_Api.dart';
import 'package:equatable/equatable.dart';
import '../../../../models/student_list_model/list_student_in_class_model.dart';

part 'set_class_monitor_event.dart';
part 'set_class_monitor_state.dart';

class SetClassMonitorBloc
    extends Bloc<SetClassMonitorEvent, SetClassMonitorState> {
  final GetStudentInClassApi setClassMonitorApi;
  SetClassMonitorBloc({required this.setClassMonitorApi})
      : super(SetClassMonitorInitial()) {
    on<SelectedClassMonitorEvent>((event, emit) async {
      emit(SetClassMonitorLoadingState());
      try {
        var data = await setClassMonitorApi.setClassMonitorApi(
            idClass: event.idClass, listStudent: event.listClassMonitor);
        emit(SetClassMonitorSuccessState(setClassMonitorModel: data));
        print("Successfully");
      } catch (e) {
        emit(SetClassMonitorErrorState(error: e.toString()));
        print("error ${e.toString()}");
      }
    });
  }
}
