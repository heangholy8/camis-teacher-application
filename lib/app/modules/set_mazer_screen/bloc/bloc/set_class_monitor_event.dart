part of 'set_class_monitor_bloc.dart';

abstract class SetClassMonitorEvent extends Equatable {
  const SetClassMonitorEvent();

  @override
  List<Object> get props => [];
}

class SelectedClassMonitorEvent extends SetClassMonitorEvent {
  final String idClass;
  final List<ClassMonitorSet> listClassMonitor;

  const SelectedClassMonitorEvent(
      {required this.idClass, required this.listClassMonitor});
}
