import 'dart:io';

import 'package:flutter/material.dart';

import 'package:shimmer/shimmer.dart';

import '../../../core/constands/color_constands.dart';

class ShimmerMonitorWidget extends StatelessWidget {
  const ShimmerMonitorWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: WillPopScope(
        onWillPop: () => exit(0),
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: Center(
                child: Image.asset(
                  "assets/images/Oval (2).png",
                ),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(
                "assets/images/Oval.png",
                width: MediaQuery.of(context).size.width / 1.7,
              ),
            ),
            Positioned(
              top: 150,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (1).png",
              ),
            ),
            Positioned(
              bottom: 35,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (3).png",
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height / 5,
              right: MediaQuery.of(context).size.width / 4,
              child: Center(
                child: Image.asset(
                  "assets/images/Oval (2).png",
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                top: 100,
              ),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Shimmer.fromColors(
                    baseColor: const Color(0xFF3688F4),
                    highlightColor: const Color(0xFF3BFFF5),
                    child: Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width / 1.6,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: const Color(0x622195F3),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                  Shimmer.fromColors(
                    baseColor: const Color(0xFF3688F4),
                    highlightColor: const Color(0xFF3BFFF5),
                    child: Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width / 4.6,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: const Color(0x622195F3),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 12,
                        left: MediaQuery.of(context).size.width / 4.5,
                        right: MediaQuery.of(context).size.width / 4.5),
                    child: Container(
                      margin: const EdgeInsets.symmetric(
                        horizontal: 20,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(
                        top: 30,
                      ),
                      decoration: const BoxDecoration(
                        color: Colorconstand.neutralSecondary,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          topRight: Radius.circular(12.0),
                        ),
                      ),
                      child:
                      
                       ListView.builder(
                        itemCount: 5,
                        shrinkWrap: true,
                        padding: const EdgeInsets.only(top: 22),
                        itemBuilder: (context, index) {
                          return
                          
                           Shimmer.fromColors(
                            baseColor: const Color(0xFF3688F4),
                            highlightColor: const Color(0xFF3BFFF5),
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 0),
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 60,
                                      height: 60,
                                      child: ClipOval(
                                        child: Image.asset(
                                          "assets/images/blank-profile.png",
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Container(
                                        padding:
                                            const EdgeInsets.only(bottom: 14),
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                                color: Colors.black
                                                    .withOpacity(.2),
                                                width: 0.2),
                                          ),
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 2.0),
                                                child: Container(
                                                  height: 38,
                                                  width: 200,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                    color:
                                                        const Color(0x622195F3),
                                                  ),
                                                )),
                                            Container(
                                              padding: const EdgeInsets.all(28),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                       
                        },
                      ),
                 
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    ;
  }
}
