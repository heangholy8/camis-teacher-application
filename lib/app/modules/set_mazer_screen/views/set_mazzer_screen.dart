// ignore_for_file: sort_child_properties_last

import 'dart:io';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/modules/home_screen/view/home_screen.dart';
import 'package:camis_teacher_application/app/modules/set_mazer_screen/views/shimmer_loading_widget.dart';
import 'package:camis_teacher_application/app/storages/user_storage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../widget/button_widget/button_widget_custom.dart';
import '../../../../widget/classmonitor_tag_button.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/thems/thems_constands.dart';

import '../bloc/assignmonitor_bloc.dart';
import '../bloc/bloc/set_class_monitor_bloc.dart';

class SetClassMonitorScreen extends StatefulWidget {
  const SetClassMonitorScreen({super.key});

  @override
  State<SetClassMonitorScreen> createState() => _SetClassMonitorScreenState();
}

class _SetClassMonitorScreenState extends State<SetClassMonitorScreen> {
  int? selectedIndex;
  List<String> lst = [];
  int selectedLeadership = 0;
  bool isChangedLeadership = true;
  bool isLoading = false;
  bool isPostLoading = false;
  UserSecureStroage pref = UserSecureStroage();
  var secondData;
  String? idClass;

  @override
  void initState() {
    BlocProvider.of<AssignmonitorBloc>(context)
        .add(const GetStudentListEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: WillPopScope(
        onWillPop: () => exit(0),
        child: BlocListener<SetClassMonitorBloc, SetClassMonitorState>(
          listener: (context, state) {
            if (state is SetClassMonitorLoadingState) {
              setState(() {
                isPostLoading = true;
              });
            } else {
              setState(() {
                isPostLoading = false;
              });
            }
          },
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Center(
                  child: Image.asset(
                    "assets/images/Oval (2).png",
                  ),
                ),
              ),
              Positioned(
                top: 0,
                right: 0,
                child: Image.asset(
                  "assets/images/Oval.png",
                  width: MediaQuery.of(context).size.width / 1.7,
                ),
              ),
              Positioned(
                top: 150,
                left: 0,
                child: Image.asset(
                  "assets/images/Oval (1).png",
                ),
              ),
              Positioned(
                bottom: 35,
                left: 0,
                child: Image.asset(
                  "assets/images/Oval (3).png",
                ),
              ),
              Positioned(
                bottom: MediaQuery.of(context).size.height / 5,
                right: MediaQuery.of(context).size.width / 4,
                child: Center(
                  child: Image.asset(
                    "assets/images/Oval (2).png",
                  ),
                ),
              ),
              BlocConsumer<AssignmonitorBloc, AssignmonitorState>(
                listener: (context, state) {
                  if (state is GetStudentInClassSuccess) {
                    var data = state.studentinclassModel!.data;
                    var isClassMonitor =
                        data!.where((element) => element.leadership == 1);
                    setState(() {
                      selectedLeadership = isClassMonitor.length;
                      idClass = data[0].classId.toString();
                    });
                    setState(() {
                      isLoading = false;
                    });
                  } else {
                    setState(() {
                      isLoading = true;
                    });
                  }
                },
                builder: (context, state) {
                  if (state is GetStudentInClassLoading) {
                    return const ShimmerMonitorWidget();
                  } else if (state is GetStudentInClassSuccess) {
                    var data = state.studentinclassModel!.data;
                    secondData = data;
                    debugPrint(data?[0].studentId);
                    return Container(
                      margin: const EdgeInsets.only(
                        top: 100,
                      ),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Text(
                            'ASSIGNCLASS'.tr(),
                            style: ThemsConstands.headline_2_semibold_24
                                .copyWith(color: Colorconstand.neutralWhite),
                          ),
                          Text(
                            translate != "en"
                                ? BlocProvider.of<AssignmonitorBloc>(context)
                                    .gradeNameKh
                                    .toString()
                                : BlocProvider.of<AssignmonitorBloc>(context)
                                    .gradeNameEn
                                    .toString(),
                            style: ThemsConstands.headline_2_semibold_24
                                .copyWith(color: Colorconstand.neutralWhite),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 12,
                                left: MediaQuery.of(context).size.width / 4.5,
                                right: MediaQuery.of(context).size.width / 4.5),
                            child: Text(
                              "ASSIGNCLASSDES".tr(),
                              textAlign: TextAlign.center,
                              style: ThemsConstands.subtitle1_regular_16
                                  .copyWith(color: Colors.white),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.only(
                                top: 30,
                              ),
                              decoration: const BoxDecoration(
                                color: Colorconstand.neutralSecondary,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12.0),
                                  topRight: Radius.circular(12.0),
                                ),
                              ),
                              child: ListView.builder(
                                itemCount: data!.length,
                                shrinkWrap: true,
                                padding: const EdgeInsets.only(top: 12),
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      setState(
                                        () {
                                          if (selectedLeadership <= 4) {
                                            if (data[index].leadership == 0) {
                                              data[index].leadership = 1;
                                              selectedLeadership++;
                                              idClass = data[index]
                                                  .classId
                                                  .toString();

                                              lst.add(selectedLeadership
                                                  .toString());
                                            } else {
                                              data[index].leadership = 0;
                                              if (selectedLeadership >= 0) {
                                                selectedLeadership--;
                                              }
                                            }
                                          } else {
                                            if (data[index].leadership == 0) {
                                              yesnoAlertbox(context);
                                            }
                                            if (isChangedLeadership == true) {
                                              if (data[index].leadership == 1) {
                                                data[index].leadership = 0;
                                                selectedLeadership--;
                                                debugPrint(
                                                    "OPOPOPOPOOPOPOPOPO ");
                                              }
                                            }
                                          }
                                        },
                                      );
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          bottom: index == data.length - 1
                                              ? 100.0
                                              : 0),
                                      child: Container(
                                        padding: const EdgeInsets.all(8),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: 50,
                                              height: 50,
                                              child: ClipOval(
                                                child: Image.network(
                                                      data[index]
                                                          .profileMedia!
                                                          .fileThumbnail
                                                          .toString(),
                                                    
                                                    fit: BoxFit.cover),
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 12,
                                            ),
                                            Expanded(
                                              child: Container(
                                                padding: const EdgeInsets.only(
                                                    bottom: 4),
                                                decoration: BoxDecoration(
                                                  border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.black
                                                            .withOpacity(.2),
                                                        width: 0.2),
                                                  ),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 2.0),
                                                      child: Text(
                                                        translate != "en"
                                                            ? data[index]
                                                                .name
                                                                .toString()
                                                            : data[index]
                                                                .nameEn
                                                                .toString(),
                                                        style: ThemsConstands
                                                            .headline_4_medium_18,
                                                      ),
                                                    ),
                                                    data[index].leadership == 0
                                                        ? const SizedBox(
                                                            height: 54,
                                                            width: 30,
                                                          )
                                                        : ClassMonitorTagWidget(title: "CLASSMONITORE".tr(),),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return const ShimmerMonitorWidget();
                  }
                },
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomLeft,
                        colors: [
                          Colors.white.withOpacity(0.5),
                          Colorconstand.lightBeerusBeerus.withOpacity(.4),
                        ],
                      ),
                    ),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ButtonWidgetCustom(
                          buttonColor: selectedLeadership != 0 ||
                                  isChangedLeadership == true
                              ? Colorconstand.primaryColor
                              : Colorconstand.mainColorUnderlayer,
                          onTap: selectedLeadership != 0 ||
                                  isChangedLeadership == true
                              ? () {
                                  BlocProvider.of<SetClassMonitorBloc>(context)
                                      .add(
                                    SelectedClassMonitorEvent(
                                      idClass: idClass.toString(),
                                      listClassMonitor: secondData,
                                    ),
                                  );

                                  pref.saveSelectClassMonitor(
                                      isSelected: "sdf");
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      PageRouteBuilder(
                                        pageBuilder:
                                            (context, animation1, animation2) =>
                                                const HomeScreen(),
                                        transitionDuration: Duration.zero,
                                        reverseTransitionDuration:
                                            Duration.zero,
                                      ),
                                      (route) => false);
                                }
                              : () {},
                          panddinHorButton: 22,
                          panddingVerButton: 12,
                          radiusButton: 8,
                          textStyleButton:
                              ThemsConstands.button_semibold_16.copyWith(
                            color: Colorconstand.neutralWhite,
                          ),
                          title: 'SUBMIT'.tr(),
                        ),
                        InkWell(
                          onTap: () {
                            pref.saveSelectClassMonitor(isSelected: "sdf");
                            Navigator.pushAndRemoveUntil(
                                context,
                                PageRouteBuilder(
                                  pageBuilder:
                                      (context, animation1, animation2) =>
                                          const HomeScreen(),
                                  transitionDuration: Duration.zero,
                                  reverseTransitionDuration: Duration.zero,
                                ),
                                (route) => false);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Text(
                              "SKIP".tr(),
                              style: ThemsConstands.button_semibold_16,
                            ),
                          ),
                        ),
                        const SizedBox(height: 12,),
                      ],
                    )),
              ),
              isPostLoading
                  ? AnimatedPositioned(
                      duration: const Duration(milliseconds: 500),
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: AnimatedContainer(
                        duration: const Duration(milliseconds: 300),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colorconstand.subject13.withOpacity(0.6),
                        child: const Center(
                          child: CircularProgressIndicator(
                            color: Colorconstand.neutralWhite,
                          ),
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }

  Future<dynamic> yesnoAlertbox(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                ImageAssets.warning_icon,
                width: 52,
                color: Colorconstand.primaryColor,
              ),
            ],
          ),
          content: Text(
            "MESSAGEMAZZER".tr(),
            textAlign: TextAlign.center,
            style: ThemsConstands.headline3_medium_20_26height
                .copyWith(height: 1.5),
          ),
          actions: <Widget>[
            TextButton(
              child:
                  Text("OKAY".tr(), style: ThemsConstands.button_semibold_16),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  isChangedLeadership = true;
                });
              },
            ),
          ],
        );
      },
    );
  }

}
