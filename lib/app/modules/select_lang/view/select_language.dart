import 'dart:io';

import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/select_lang/local_data/local_langModel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../widget/button_widget/button_widget.dart';
import '../../../routes/app_route.dart';
import '../../../storages/user_storage.dart';

class SelectLanguageScreen extends StatefulWidget {
  const SelectLanguageScreen({
    Key? key,
  }) : super(key: key);

  @override
  _SelectLanguageScreen createState() => _SelectLanguageScreen();
}

class _SelectLanguageScreen extends State<SelectLanguageScreen> {
  TextEditingController? _textEditingController = TextEditingController();
  List<Lang> langs = allLangs;
  UserSecureStroage _pref = UserSecureStroage();
  String langsuccess = 'Select-lang-success';

  var khmer = const Locale("en");

  void updateLanguage(Locale locale, BuildContext context) {
    context.setLocale(locale);
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return WillPopScope(
      onWillPop: () => exit(0),
      child: Scaffold(
        body: Stack(
          children: [
            Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 95),
                  color: Colorconstand.primaryColor,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Align(
                        child: Text(
                          'CHOOSE_LANGUADE'.tr().toUpperCase(),
                          style: ThemsConstands.headline3_semibold_20.copyWith(
                            color: Colorconstand.neutralWhite,
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colorconstand.neutralWhite,
                            borderRadius: BorderRadius.circular(15)),
                        margin: const EdgeInsets.only(
                            bottom: 20, left: 17, right: 17, top: 35),
                        child: TextFormField(
                          controller: _textEditingController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              fillColor: Colorconstand.primaryColor,
                              prefixIcon: const Icon(Icons.search),
                              hintText: "SEARCH".tr(),
                              hintStyle: ThemsConstands.headline_5_medium_16),
                          onChanged: searchLangs,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Colorconstand.neutralWhite,
                    child: ListView.builder(
                        padding: const EdgeInsets.all(0),
                        itemCount: langs.length,
                        itemBuilder: (context, index) {
                          final lang = langs[index];
                          return Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: translate == lang.sublang
                                    ? Colorconstand.neutralSecondary
                                    : Colorconstand.neutralWhite,
                                border: Border(
                                    bottom: BorderSide(
                                  color: Colorconstand.neutralDarkGrey
                                      .withOpacity(0.1),
                                  width: 1,
                                ))),
                            height: 85,
                            child: ListTile(
                              leading: Container(
                                height: 50,
                                width: 50,
                                child: ClipOval(
                                    child: langs[index].sublang=="la"?  
                                    Image.asset(lang.image,fit: BoxFit.cover,)
                                    :SvgPicture.asset(lang.image,fit: BoxFit.cover,)),
                              ),
                              title: Text(
                                lang.title,
                                style: translate == langs[index].sublang
                                    ? ThemsConstands.headline_4_medium_18
                                    : ThemsConstands.headline_4_medium_18
                                        .copyWith(
                                            color: Colorconstand.lightBlack),
                              ),
                              trailing: langs[index].sublang == "th" || langs[index].sublang == "la" || langs[index].sublang == "vi"? 
                                  const Text("Comming soon...")
                                : translate == langs[index].sublang
                                  ? const Icon(
                                      Icons.check_circle_outline_sharp,
                                      color: Colorconstand.primaryColor,
                                    )
                                  : const Icon(null),
                              onTap: () {
                                setState(() {
                                  if(langs[index].sublang == "th" || langs[index].sublang == "la" || langs[index].sublang == "vi"){}
                                  else{
                                    updateLanguage( Locale(langs[index].sublang), context);
                                  }
                                });
                              },
                            ),
                          );
                        }),
                  ),
                ),
              ],
            ),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 55,
                  color: Colorconstand.neutralWhite,
                )),
            Positioned(
              bottom: 5,
              left: 0,
              right: 0,
              child: Container(
                alignment: Alignment.bottomCenter,
                height: 95,
                color: Colorconstand.neutralWhite.withOpacity(0.6),
                child: Container(
                  margin: const EdgeInsets.only(
                      bottom: 30, top: 10, left: 25.0, right: 25.0),
                  child: Button_Custom(
                    hightButton: 45,
                    buttonColor: Colorconstand.primaryColor,
                    radiusButton: 13,
                    titleButton: 'CONTINUE'.tr(),
                    titlebuttonColor: Colorconstand.neutralWhite,
                    onPressed: () async {
                      setState(() {
                        Navigator.of(context).pushReplacementNamed(
                          Routes.OPTIONLOGINSCREEN,
                        );
                        _pref.saveSelectLangSuccess(
                            selectLangSuccess: langsuccess);
                      });
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void searchLangs(String query) {
    final suggestions = allLangs.where((Lang) {
      final langTitle = Lang.title.toLowerCase();
      final input = query.toLowerCase();
      return langTitle.contains(input);
    }).toList();
    setState(() {
      langs = suggestions;
    });
  }
}
