import 'dart:async';

import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/help/check_month.dart';
import 'package:camis_teacher_application/app/modules/schedule_expanded_screen/attendance_widget/attendance_expanded_widget.dart';
import 'package:camis_teacher_application/app/storages/get_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../attendance_schedule_screen/state_management/state_month/bloc/month_exam_set_bloc.dart';
import '../../attendance_schedule_screen/view/pop_set_exam_day.dart';
import '../../check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../../check_attendance_screen/view/attendance_entry_screen.dart';
import '../../grading_screen/bloc/grading_bloc.dart';
import '../../grading_screen/view/grading_screen.dart';
import '../../grading_screen/view/result_screen.dart';

class ExamDetailScreen extends StatefulWidget {
  final String examDate;
  final int isCheckScoreEnter;
  final int isNoExam;
  final int typeExam;
  final String semester;
  final String className;
  final String subjectName;
  final String subjectId;
  final String startTime;
  final String endTime;
  final String nameClassMonitor;
  final String absent;
  final String presence;
  final String lateStudent;
  final String permission;
  final String classId;
  final String scheduleId;
  final String date;
  final int month;
  final bool activeMySchedule;
  final int isApprove;
  final int setExamFrom;
  final int isInstructor;
  const ExamDetailScreen({super.key,required this.isInstructor, required this.setExamFrom,required this.examDate, required this.isCheckScoreEnter, required this.isNoExam, required this.typeExam, required this.semester, required this.className, required this.subjectName, required this.startTime, required this.endTime, required this.nameClassMonitor, required this.absent, required this.presence, required this.lateStudent, required this.permission, required this.classId, required this.scheduleId, required this.date, required this.month, required this.activeMySchedule, required this.isApprove, required this.subjectId});

  @override
  State<ExamDetailScreen> createState() => _ExamDetailScreenState();
}

class _ExamDetailScreenState extends State<ExamDetailScreen> {
  final examTitleController = TextEditingController();

  StreamSubscription? sub;
  bool isoffline = true;
  bool isPrimary = false;

  final GetStoragePref _getStoragePref = GetStoragePref();


  void getLocalData() async {
    var authStoragePref = await _getStoragePref.getJsonToken;
    setState(() {
      isPrimary = authStoragePref.isPrimary!;
    });
  }

  @override
  void initState() {
    //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          getLocalData();
          isoffline = (event != ConnectivityResult.none);
          if(isoffline == true){
          }
        });
      });
      //============= Eend Check internet ====================
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,bottom: 0,left: 0,right: 0,
            child: SafeArea(
            bottom: false,
            child: ListView(
              padding: const EdgeInsets.only(left: 15, right: 15),
              scrollDirection: Axis.vertical,
              primary: true,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: SvgPicture.asset(
                        ImageAssets.CLOSE_ICON,
                        height: 24,
                        color: Colorconstand.lightBlack,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 5.0, top: 20),
                  child: Text(
                    widget.subjectName,
                    style: ThemsConstands.headline_2_semibold_24
                        .copyWith(color: Colorconstand.primaryColor),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 5.0, top: 12.0, bottom: 24),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        ImageAssets.LOCATION,
                        height: 18,
                        color: Colorconstand.primaryColor,
                      ),
                      const SizedBox(width: 5.0),
                      Text(widget.className,
                          style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.lightBulma),
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: (){
                          BlocProvider.of<MonthExamSetBloc>(context).add(MonthExamSet(idClass:widget.classId.toString(), idsubject: widget.subjectId.toString()));
                          showModalBottomSheet(
                            isScrollControlled:true,
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(16.0),
                                topRight: Radius.circular(16.0),
                              ),
                            ),
                            context: context,
                            builder: (context) {
                              return PopMonthForSelect(isPrimary: isPrimary,studentDataScore: [],date: widget.date,setExamFrom: widget.setExamFrom,typeSemesterOrMonth:widget.typeExam,classId: widget.classId.toString(),subjectId: widget.subjectId.toString(),activeMySchedule: widget.activeMySchedule,month: widget.month,semester: widget.semester);
                            },
                          );
                      },
                      child: Row(
                        children: [
                          SvgPicture.asset(ImageAssets.calendar_icon,height: 18, color: Colorconstand.primaryColor),
                          const SizedBox(width: 5.0),
                          Text(widget.isNoExam == 0 ? widget.date : widget.isNoExam == 2 ?"បញ្ចូលពិន្ទុតាមក្បាលវ៉ាល់": "NO_EXAM".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                          widget.isNoExam == 1?Container():Text(' • ',
                          style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                          widget.isNoExam == 1?Container(): Text("${widget.startTime}-${widget.endTime}",
                            style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                          ],
                        ),
                      ), 
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 18, bottom: 20),
                  decoration: BoxDecoration(
                      color: Colorconstand.neutralWhite,
                      borderRadius: BorderRadius.circular(8.2),
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0, 5.13),
                          blurRadius: 15.38,
                          spreadRadius: 0,
                          color: Colorconstand.lightBulma.withOpacity(0.15),
                        )
                      ]),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "EXAM_TYPE".tr(),
                            style: ThemsConstands.button_semibold_16
                                .copyWith(color: Colorconstand.lightBulma),
                          ),
                          Text(
                             "${"REGULAR".tr()} ${widget.typeExam == 1? "${translate=="km"?"MONTH".tr():""}${checkMonth(widget.month)}": widget.semester.tr()}",
                            style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.primaryColor),
                          )
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 16.0),
                        child: Divider(
                          color: Colorconstand.neutralGrey,
                          thickness: 0.5,
                          height: 1,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "SCORE_RESULT".tr(),
                            style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma),
                          ),
                          // check complete score 
                          MaterialButton(
                            onPressed: (() {
                            if(widget.isCheckScoreEnter == 1) {
                              BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                                idClass:  widget.classId.toString(), 
                                subjectId:  widget.subjectId.toString(), 
                                type:  widget.typeExam.toString(), 
                                month:  widget.month.toString(), 
                                semester:  widget.semester.toString(),
                                examDate: ""));
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> 
                                  ResultScreen(isInstructor: widget.isInstructor == 0 ? false:true
                                ),
                              ),
                            );

                            }else{
                                BlocProvider.of<GradingBloc>(context).add(GetGradingEvent(
                                idClass:  widget.classId.toString(), 
                                subjectId:  widget.subjectId.toString(), 
                                type:  widget.typeExam.toString(), 
                                month:  widget.month.toString(), 
                                semester:  widget.semester.toString(),
                                examDate: ""),
                                ); 
                                Navigator.pushReplacement(context, 
                                  MaterialPageRoute(builder: (context) => GradingScreen(
                                  isPrimary: isPrimary,
                                  isInstructor: widget.isInstructor == 0 ? false:true,
                                  myclass: widget.activeMySchedule,
                                  redirectFormScreen: widget.setExamFrom,
                                  ),
                                ),
                              );
                            }
                            }),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            padding: const EdgeInsets.only(
                                left: 16, top: 8, bottom: 8, right: 16),
                            color:widget.isCheckScoreEnter ==0?Colorconstand.primaryColor:widget.isCheckScoreEnter ==1 && widget.isApprove == 0?Colorconstand.exam: widget.isCheckScoreEnter == 1 && widget.isApprove == 1?Colorconstand.alertsPositive:Colorconstand.primaryColor,
                            child: Row(
                              children: [
                                Text(
                                  widget.isCheckScoreEnter ==0 && widget.isApprove == 0?"Enter".tr():widget.isCheckScoreEnter == 1 && widget.isApprove == 0?"AWAIT_APPROVE".tr(): widget.isCheckScoreEnter == 1 && widget.isApprove == 1?"APPROVE".tr():"IN_PROGRESS".tr(),
                                  style: ThemsConstands.button_semibold_16
                                      .copyWith(color: Colorconstand.neutralWhite),
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                SvgPicture.asset(
                                  ImageAssets.ARROWRIGHT_ICON,
                                  color: Colorconstand.neutralWhite,
                                  height: 24,
                                )
                              ],
                            ),
                          )
                      
                        ],
                      ),
                    ],
                  ),
                ),
                
                const SizedBox(
                  height: 16.0,
                ),
                widget.isNoExam==1?Container():AttendanceExpandedWidget(
                  onPressed: (){
                    BlocProvider.of<CheckAttendanceBloc>(context).add(
                      GetCheckStudentAttendanceEvent(widget.classId.toString(), widget.scheduleId.toString(), widget.date.toString()),
                    );
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AttendanceEntryScreen( routFromScreen: 2,activeMySchedule: widget.activeMySchedule,),
                      ),
                    );
                  },
                  nameClassMonitor: widget.nameClassMonitor,
                  isCheckattandace: 1,
                  presence: widget.presence,
                  lateStudent: widget.lateStudent,
                  permission: widget.permission,
                  absent: widget.absent,
                ),
          
                const SizedBox(height: 20)
              ],
            ),
                  ),
          ),
        isoffline == true
                  ? Container()
                  : Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      top: 0,
                      child: Container(
                        color: const Color(0x7B9C9595),
                      ),
                    ),
              AnimatedPositioned(
                bottom: isoffline == true ? -150 : 0,
                left: 0,
                right: 0,
                duration: const Duration(milliseconds: 500),
                child: const NoConnectWidget(),
              ),
        ],
      ));
  }
}
