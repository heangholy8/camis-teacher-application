// ignore_for_file: avoid_print

import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';

enum MediaSelecteStatus { initial, loading, success, failure }

class MediaCubit extends Cubit<MediaSelecteStatus> {
  MediaCubit() : super(MediaSelecteStatus.initial);
  List<File> filesMedia = [];

  Future<void> pickImgVdoFileCubit({required List<File> listMedia}) async {
    try {
      emit(MediaSelecteStatus.loading);
      filesMedia = listMedia;
      emit(MediaSelecteStatus.success);
    } catch (e) {
      print("Error selecting Media from Device's $e");
      emit(MediaSelecteStatus.failure);
    }
  }

  Future<void> addMoreListCubit({required List<File> listMedia}) async {
    try {
      emit(MediaSelecteStatus.loading);
      filesMedia.addAll(listMedia);
      emit(MediaSelecteStatus.success);
    } catch (e) {
      print("Error selecting Media from Device's $e");
      emit(MediaSelecteStatus.failure);
    }
  }

  void removeFile({required int index}) {
    try {
     emit(MediaSelecteStatus.loading);
      filesMedia.removeAt(index);
      emit(MediaSelecteStatus.success);
    } catch (e) {
      emit(MediaSelecteStatus.failure);
    }
  }
}



 // List<Media>? media = await ImagesPicker.pick(
      //   count: 10,
      //   pickType: PickType.all,
      //   language: Language.System,
      //   cropOpt: CropOption(
      //     aspectRatio: CropAspectRatio.wh16x9,
      //   ),
      // );
      // for (var item in listMedia!) {
      //   File file = File(item.path);
      //   filesMedia.add(file);
      // }
