// ignore_for_file: public_member_api_docs, sort_constructors_first
class ShareClassModel {
  final String? classNumber;
  final String? teacher;
  bool? valueTick;

  ShareClassModel({
    this.classNumber = "",
    this.teacher = "",
    this.valueTick = false,
  });
}

final ShareClassModelList = <ShareClassModel>[
  ShareClassModel(
    classNumber: "១០B",
  ),
  ShareClassModel(
    classNumber: "១០C",
  ),
  ShareClassModel(
    classNumber: "១១A",
  ),
  ShareClassModel(
    classNumber: "១១B",
  ),
  ShareClassModel(
    classNumber: "១១C",
  ),
];
