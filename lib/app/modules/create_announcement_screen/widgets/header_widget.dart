import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HeaderWidget extends StatelessWidget {
  final String? screenTitle;
  final VoidCallback? onSend;
  final VoidCallback? onBack;
  bool disabled;

   HeaderWidget({super.key, this.screenTitle,this.onSend,this.onBack,this.disabled = true});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
          GestureDetector(
            onTap:onBack?? () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 50),
              child: SvgPicture.asset(ImageAssets.CLOSE_ICON),
            ),
          ),
          Text(
              screenTitle?? 'ការជូនដំណឹង: សំបុត្រថ្នាក់រៀន',
              style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),
          ),
          GestureDetector(
            onTap: onSend?? () {
              
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: SvgPicture.asset(
                ImageAssets.send_outline_icon,
                width: 28,
                color: disabled == true?Colors.grey:Colors.white,
              ),
            ),
          ),
      ],
    );
  }
}
