// ignore_for_file: must_be_immutable
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shimmer/shimmer.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../time_line/class_time_line/post_bloc/bloc/post_classtimeline_bloc_bloc.dart';

class ChoosingClass extends StatefulWidget {
  final Function() onSend;

   ChoosingClass({super.key,required this.onSend});

  @override
  State<ChoosingClass> createState() => _ChoosingClassState();
}

class _ChoosingClassState extends State<ChoosingClass> {
  bool checkedBox = false;
  List<int> isSelected = [];
  bool isLoading =false;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    isSelected.clear();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PostClasstimelineBlocBloc, PostClasstimelineBlocState>(
      listener: (context, state) {
        if(state is PostClassTimelineLoadingState ){
          setState(() {
            isLoading = true;
          });
        }else{
          setState(() {
            isLoading = false;
          });
        }
      },
      child:
       Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:18.0,vertical: 14),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [   
                  IconButton(onPressed: null, icon: Container(),),
                  Text("CHOOSECLASS".tr(),style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center),
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    }, 
                    icon: SvgPicture.asset(ImageAssets.CLOSE_ICON,color: Colorconstand.lightTrunks,)
                 ),
                ],
              ),
            ),
            Expanded(
              child: BlocBuilder<GetClassBloc, GetClassState>(
                builder: (context, state) {
                  if(state is GetClassLoading){
                    return shimmerLoading();
                  }else if(state is GetClassLoaded){
                    if (state.classModel!.data!.isCurrent == true) {
                      var data = state.classModel!.data;
                      return ListView.builder(
                        controller: _scrollController,
                        itemCount:data!.teachingClasses!.length,
                        itemBuilder: (context,index) {
                          return GestureDetector(
                            onTap:(){
                              if (data.teachingClasses![index].isSelected == true) {
                                setState((){
                                  data.teachingClasses![index].isSelected = false;
                                  // print(isSelected.toString());
                                  // listClassed.removeAt(index);
                                  // print("sinatremove $listClassed");
                                  
                                  BlocProvider.of<PostClasstimelineBlocBloc>(context).classId.remove(data.teachingClasses![index].id.toString());
                                  print("remove ${BlocProvider.of<PostClasstimelineBlocBloc>(context).classId}");
                                });
                              }else{
                                setState((){
                                  data.teachingClasses![index].isSelected = true;
                                  //listClassed.add(data.teachingClasses![index].id.toString());
                                  BlocProvider.of<PostClasstimelineBlocBloc>(context).classId.add(data.teachingClasses![index].id.toString());
                                  // print(isSelected.toString());
                                  // print("sinatadd $listClassed");
                                  print("add ${BlocProvider.of<PostClasstimelineBlocBloc>(context).classId}");
                                });
                              }
                            },
                            child: 
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical:8.0,horizontal: 20),
                              child: Column (
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                         CircleAvatar(
                                          radius: 30,
                                          child: Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Text(data.teachingClasses![index].name.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline3_semibold_20,),
                                          ),
                                        ),
                                        const SizedBox(width: 18,),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Text("${"${data.teachingClasses![index].objectType}".tr()} ${data.teachingClasses![index].name}",style: ThemsConstands.headline3_medium_20_26height.copyWith(fontWeight: FontWeight.w900),),
                                                const SizedBox(width: 14,),
                                                data.teachingClasses![index].isInstructor == 1 ? SvgPicture.asset(ImageAssets.ranking_icon):Container(),
                                              ],
                                            ),
                                            const  SizedBox(height: 8,),
                                            Text(data.teachingClasses![index].isInstructor == 1? "ISINSTRUCTOR".tr():"ISTEACHER".tr(),style:ThemsConstands.caption_regular_12,)
                                          ],
                                        ),
                                      ],
                                    ),
                                      Container(
                                        width: 34,
                                        height: 34,
                                        decoration: BoxDecoration(
                                          color: data.teachingClasses![index].isSelected == true ? Colorconstand.primaryColor : null,
                                          border: Border.all(color: Colors.grey),
                                          borderRadius: BorderRadius.circular(8),
                                        ),
                                        child: Icon(Icons.check,size: 28,color: data.teachingClasses![index].isSelected == true ?Colors.white:Colors.transparent,)
                                      ),
                                    ],
                                  ),
                                  const  SizedBox(height: 10,),
                                  Container(width: MediaQuery.of(context).size.width,color: Colors.grey,height: .2,margin: const EdgeInsets.only(left: 65),)
                                ],
                              ),
                            ),
                         
                          );
                        }
                      );
                    }
                  }
                  return Container();
                },
              ),
            ),
            isLoading == false ? InkWell(
              onTap: BlocProvider.of<PostClasstimelineBlocBloc>(context).classId.isNotEmpty ? widget.onSend : null,
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width - 50,
                margin: const EdgeInsets.only(bottom: 18),
                height: 52,
                decoration: BoxDecoration(
                  color: BlocProvider.of<PostClasstimelineBlocBloc>(context).classId.isNotEmpty? Colorconstand.primaryColor:Colorconstand.darkTextsDisabled,
                  borderRadius: BorderRadius.circular(8)
                ),
                child: Text("POST".tr() , style: ThemsConstands.button_semibold_16.copyWith(color: Colors.white),)
              ),
            )
            : const Padding(
                padding: EdgeInsets.symmetric(vertical: 28.0),
                child: CircularProgressIndicator(),
            ),
          ],
        ),
    
    );
  }

  Widget shimmerLoading() {
    return Shimmer.fromColors(
      baseColor:const Color(0xFF3688F4),
      highlightColor:const Color(0xFF3BFFF5),
      child: Column(
        children: [
          Container(
            height: 68,
            width: MediaQuery.of(context).size.width-40,
            decoration: BoxDecoration(
              color: Colorconstand.primaryColor,
              borderRadius: BorderRadius.circular(8)
            ),
            child:const Text("sdf"),
          ),
          const SizedBox(height: 18,),
          Container(
            height: 68,
            width: MediaQuery.of(context).size.width-40,
            decoration: BoxDecoration(
              color: Colorconstand.primaryColor,
              borderRadius: BorderRadius.circular(8)
            ),
            child:const Text("sdf"),
          ),
          const SizedBox(height: 18,),
           Container(
            height: 68,
            width: MediaQuery.of(context).size.width-40,
            decoration: BoxDecoration(
              color: Colorconstand.primaryColor,
              borderRadius: BorderRadius.circular(8)
            ),
            child:const Text("sdf"),
          ),
          const SizedBox(height: 18,),
        ],
      ),
    
    );
  }
}
 