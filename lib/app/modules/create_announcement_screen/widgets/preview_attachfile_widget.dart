// ignore_for_file: iterable_contains_unrelated_type

import 'dart:io';

// Type-Dashbaord: 1=monthly exam, 2 = semester, 3 = check attendance,
// Task-Expiration: task-status = 3 => late,

import 'package:camis_teacher_application/app/modules/create_announcement_screen/widgets/pdf_viewer.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/widgets/preview_vdo_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../cubit/select_media_cubit.dart';

class PreviewAttachFileWidget extends StatefulWidget {
  int imageIndex = 0;
  PreviewAttachFileWidget({required this.imageIndex, super.key});

  @override
  State<PreviewAttachFileWidget> createState() =>
      _PreviewAttachFileWidgetState();
}

class _PreviewAttachFileWidgetState extends State<PreviewAttachFileWidget> {
  int index = 0;
  late PageController pageController;
  List<File> listImageNvdo = [];
  String? _thumbnailPath;
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    pageController = PageController(
      initialPage: widget.imageIndex,
      keepPage: true,
    );
    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  Future<bool> getMediaFromDevice() async {
    List<XFile>? listMedia = await _picker.pickMultiImage();
    // final listMedia = await ImagesPicker.pick(
    //   count: 10,
    //   gif: true,
    //   pickType: PickType.all,
    //   language: Language.System,
    //   maxTime: 3600,
    //   quality: 0.8,
    //   cropOpt: CropOption(aspectRatio: CropAspectRatio.wh16x9),
    // );
    if (listMedia.isNotEmpty) {
      for (var item in listMedia) {
        setState(() {
          listImageNvdo.add(File(item.path));
          BlocProvider.of<MediaCubit>(context).addMoreListCubit(listMedia: listImageNvdo);
        });
      }
    }
    return true;
  }

  Future<bool> getFileFromDevice() async {
    final result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'cvs']);
    if (result != null) {
      List<File> files = result.paths.map((e) => File(e.toString())).toList();
      for (var item in files) {
        setState(() {
          listImageNvdo.add(File(item.path));
          BlocProvider.of<MediaCubit>(context).addMoreListCubit(listMedia: listImageNvdo);
        });
      }
    }
    return true;
  }

  // Future<void> _generateThumbnail(String vdoUrl) async {
  //   final thumbnailPath = await VideoThumbnail.thumbnailFile(
  //     video: vdoUrl,
  //     imageFormat: ImageFormat.JPEG,
  //     maxHeight: 128,
  //     maxWidth: 128,
  //     quality: 25,
  //   );
  //   setState(() {
  //     _thumbnailPath = thumbnailPath;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.black,
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(
                horizontal: 12,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SvgPicture.asset(
                      ImageAssets.chevron_left,
                      height: 28,
                      width: 28,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "${widget.imageIndex + 1} of ${BlocProvider.of<MediaCubit>(context).filesMedia.length.toString()}",
                    style: ThemsConstands.headline3_medium_20_26height
                        .copyWith(color: Colors.white),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                         widget.imageIndex - 1;
                      });
                      BlocProvider.of<MediaCubit>(context)
                            .removeFile(index: index);
                        if (BlocProvider.of<MediaCubit>(context)
                            .filesMedia
                            .isEmpty) {
                          Navigator.pop(context);
                        }
                    
                    },
                    child: SvgPicture.asset(
                      ImageAssets.trash_icon,
                      color: Colorconstand.screenWhite,
                      height: 28,
                      width: 28,
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: BlocBuilder<MediaCubit, MediaSelecteStatus>(
                builder: (context, state) {
                  if (state == MediaSelecteStatus.loading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state == MediaSelecteStatus.success) {
                    var data = BlocProvider.of<MediaCubit>(context).filesMedia;
                    return PageView.builder(
                        controller: pageController,
                        itemCount: data.length,
                        itemBuilder: ((context, index) {
                          final extension =
                              data[index].path.toString().toLowerCase();
                          if (extension.contains('.jpg') ||
                              extension.contains('.png') ||
                              extension.contains('.jpeg') ||
                              extension.contains('.gif')) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Container(
                                padding:
                                    const EdgeInsets.only(top: 20, bottom: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    20,
                                  ),
                                ),
                                child: InteractiveViewer(
                                  boundaryMargin: const EdgeInsets.all(20.0),
                                  minScale: 0.1,
                                  maxScale: 4.0,
                                  child: Image.file(
                                    data[index],
                                    width: 150,
                                    height: 250,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            );
                          } else if (extension.contains('.pdf')||extension.contains('.doc')) {
                            return PdfViewWidget(
                              pdfUrl: data[index],
                            );
                          } else if (extension.contains('.mov') || extension.contains('.mp4')) {
                            return PreviewVdoWidget(
                              videoUrl: File(data[index].path),
                            );
                          }
                          return Container();
                        }),
                        onPageChanged: (value) {
                          setState(() {
                            index = value;
                            widget.imageIndex = value;
                          });
                        });
                  } else {
                    return Container(
                      width: 100,
                      height: 100,
                      color: Colors.black,
                    );
                  }
                },
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.12,
              padding: const EdgeInsets.only(top: 8),
              decoration: BoxDecoration(
                color: const Color(0xFFD9D9D9).withOpacity(.2),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      getMediaFromDevice().then(
                        (value) => pageController.jumpToPage(
                          BlocProvider.of<MediaCubit>(context)
                                  .filesMedia
                                  .length +
                              1,
                        ),
                      );
                      debugPrint("Take Camera");
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          ImageAssets.camera,
                          width: 34,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "IMAGEANDVDO".tr(),
                          style: ThemsConstands.headline_5_medium_16
                              .copyWith(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 28),
                    width: 0.5,
                    color: const Color(0xFF919EAB).withOpacity(.24),
                  ),
                  InkWell(
                    onTap: () {
                      getFileFromDevice().then(
                        (value) => pageController.jumpToPage(
                          BlocProvider.of<MediaCubit>(context)
                                  .filesMedia
                                  .length +
                              1,
                        ),
                      );
                  
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          ImageAssets.additem_icon,
                          width: 38,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "DOC".tr(),
                          style: ThemsConstands.headline_5_medium_16
                              .copyWith(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
