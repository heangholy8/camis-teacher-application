import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PdfViewWidget extends StatefulWidget {
  final File pdfUrl;

  const PdfViewWidget({required this.pdfUrl});

  @override
  _PdfViewWidgetState createState() => _PdfViewWidgetState();
}

class _PdfViewWidgetState extends State<PdfViewWidget> {
  final GlobalKey<SfPdfViewerState> _pdfViewStateKey = GlobalKey();
  late PdfViewerController controller;
  OverlayEntry? _overlayEntry;

  @override
  void initState() {
    super.initState();
    controller = PdfViewerController();
    controller.addListener(({property}) {
      if (controller.scrollOffset.dy > 0) {}
    });
  }

  @override
  Widget build(BuildContext context) {
    return SfPdfViewer.file(
      widget.pdfUrl,
      controller: controller,
      key: _pdfViewStateKey,
      enableDocumentLinkAnnotation: true,
      enableTextSelection: true,
      onPageChanged: (value) {},
      onTextSelectionChanged: (PdfTextSelectionChangedDetails details) {
        if (details.selectedText == null && _overlayEntry != null) {
          _overlayEntry?.remove();
          _overlayEntry = null;
        } else if (details.selectedText != null && _overlayEntry == null) {
          _showContextMenu(context, details);
        }
      },
    );
  }

  void _showContextMenu(
      BuildContext context, PdfTextSelectionChangedDetails details) {
    final OverlayState? _overlayState = Overlay.of(context);
    _overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        top: details.globalSelectedRegion!.center.dy - 55,
        left: details.globalSelectedRegion?.bottomLeft.dx,
        child: ElevatedButton(
          child: const Text(
            'Copy',
            style: TextStyle(fontSize: 17),
          ),
          onPressed: () {
            Clipboard.setData(
              ClipboardData(
                text: details.selectedText!,
              ),
            );
            controller.clearSelection();
          },
          // color: Colors.white,
          // elevation: 10,
        ),
      ),
    );
    _overlayState?.insert(_overlayEntry!);
  }
}
