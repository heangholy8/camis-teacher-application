import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  final File videoUrl;
  final String? linkVideo;

  VideoPlayerWidget({required this.videoUrl,this.linkVideo});

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  late VideoPlayerController _videoPlayerController;
  late ChewieController _chewieController;
  String? _thumbnailPath;

  @override
  void initState() {
    super.initState();
    _videoPlayerController = VideoPlayerController.network(widget.linkVideo.toString());
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      autoPlay: true,
      looping: true,
      showControls: true,
      materialProgressColors: ChewieProgressColors(
        playedColor: Colors.red,
        handleColor: Colors.redAccent,
        backgroundColor: Colors.grey,
        bufferedColor: Colors.lightGreen,
      ),
      placeholder: Container(
        color: Colors.white,
      ),
    );
  }



  @override
  void dispose() {
    super.dispose();
    _videoPlayerController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.lightBulma,
      body: SafeArea(
      child: Column(
        children: [
          Container(
            margin:const EdgeInsets.only(left: 12),
            alignment: Alignment.centerLeft,
            child: IconButton(onPressed: (){Navigator.of(context).pop();}, icon: const Icon(Icons.arrow_back_ios,color: Colorconstand.neutralWhite,size: 28,)),
          ),
          Expanded(
            child: Center(
              child: AspectRatio(
                aspectRatio: _videoPlayerController.value.aspectRatio,
                child: Chewie(
                  controller: _chewieController,
                ),
              ),
            ),
          ),
        ],
      ),
      )
    );
  }
}
