
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TypeAnnouncementCreate extends StatelessWidget {
  String title;
  String svgAsset;
  void Function()? onPressed;
  TypeAnnouncementCreate(
      {Key? key, required this.title, required this.svgAsset, this.onPressed})
      : super(key: key);
 
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Colorconstand.neutralWhite,
      padding: const EdgeInsets.all(10.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      onPressed: onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 2.0, right: 16.0),
            child: SvgPicture.asset(
              svgAsset,
              height: 26.0,
            ),
          ),
          Text(
            title,
            style: ThemsConstands.headline_4_medium_18,
          )
        ],
      ),
    );
  }
}
