
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/data/share_class_model.dart';
import 'package:camis_teacher_application/app/routes/app_route.dart';
import 'package:camis_teacher_application/widget/button_widget/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ShareClassModalBottomSheet extends StatefulWidget {
  ShareClassModalBottomSheet({super.key});

  @override
  State<ShareClassModalBottomSheet> createState() =>
      _ShareClassModalBottomSheetState();
}

class _ShareClassModalBottomSheetState
    extends State<ShareClassModalBottomSheet> {
  final shareClassList = ShareClassModelList;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3.0),
                    color: Colorconstand.lightBeerusBeerus),
                margin: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                width: 48.0,
                height: 6.0,
              ),
              const Padding(
                padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
                child: Text(
                  'ជ្រេីសរេីសថ្នាក់',
                  style: ThemsConstands.headline_4_medium_18,
                ),
              ),
              ListView.separated(
                separatorBuilder: (context, index) {
                  return Divider(
                    color: Colorconstand.lightBlack,
                    indent: MediaQuery.of(context).size.width * 0.20,
                  );
                },
                itemCount: shareClassList.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    padding: const EdgeInsets.only(
                        right: 32.8, bottom: 8.0, top: 8.0),
                    child: Row(
                      children: [
                        Container(
                          width: 45,
                          height: 45,
                          margin:
                              const EdgeInsets.only(left: 21.0, right: 21.0),
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              color: Colorconstand.neutralDarkGrey,
                              shape: BoxShape.circle),
                          child: Text(
                            shareClassList[index].classNumber!,
                            style: ThemsConstands.button_semibold_16.copyWith(
                                color: Colorconstand.lightTextsRegular),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "ថ្នាក់ ${shareClassList[index].classNumber}",
                                style: ThemsConstands.headline_4_medium_18
                                    .copyWith(color: Colorconstand.lightBulma),
                              ),
                              Text(
                                "គ្រូមុខវិជ្ជា ${shareClassList[index].teacher}",
                                style: ThemsConstands.caption_regular_12
                                    .copyWith(
                                        color: Colorconstand.neutralDarkGrey),
                              ),
                            ],
                          ),
                        ),
                        Checkbox(
                          activeColor: Colorconstand.primaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4)),
                          value: shareClassList[index].valueTick,
                          onChanged: (value) {
                            setState(() {
                              shareClassList[index].valueTick = value!;
                              print(shareClassList[index].classNumber);
                            });
                          },
                        )
                      ],
                    ),
                  );
                },
              ),
              Container(
                width: double.maxFinite,
                constraints: const BoxConstraints(minHeight: 48.0),
                margin: const EdgeInsets.all(24.0),
                child: CustomMaterialButton(
                  titleButton: "Post",
                  styleText: ThemsConstands.button_semibold_16
                      .copyWith(color: Colorconstand.lightGoten),
                  colorButton: Colorconstand.primaryColor,
                  onPressed: () {
                    Navigator.pushNamed(context, Routes.NOTIFICATION);
                  },
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
