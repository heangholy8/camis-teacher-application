import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class VideoView extends StatefulWidget {
  final VideoPlayerController videoPlayerController;
  final bool autoplay;
  const VideoView({Key? key, required this.videoPlayerController,required this.autoplay}) : super(key: key);

  @override
  State<VideoView> createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {
  late ChewieController _chewieController;
  @override
  void initState() {
    super.initState();
    setState(() {
     _chewieController = ChewieController(
        deviceOrientationsAfterFullScreen:[DeviceOrientation.portraitUp,DeviceOrientation.portraitDown],
        deviceOrientationsOnEnterFullScreen: [DeviceOrientation.landscapeLeft,DeviceOrientation.landscapeRight],
        autoPlay: widget.autoplay,
        looping: false,
        errorBuilder: (context, errorMessage) {
          return Center(
            child: Text(
              errorMessage,
              style:const TextStyle(color: Colors.white),
            ),
          );
        }, videoPlayerController:widget.videoPlayerController,
        placeholder:const Center(child: CircularProgressIndicator(),)
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Chewie(controller:_chewieController),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _chewieController.dispose();
    _chewieController.pause();
  }
}