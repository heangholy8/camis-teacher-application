import 'package:flutter/material.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../widget/profile_widget.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../react_helper/react_data.dart';

class TimelineCardWidget extends StatelessWidget {
  bool? overlayLoading ;
  final Widget moreBtn;
  final VoidCallback? deletePost;
  final VoidCallback? editPost;
  final Widget? discriptionChild;
  final Widget? viewChild;
  final Widget? displayVoice;
  final Widget? displayFile;
  final String? imageProfile;
  final int? comments;
  final String? reaction;
  final int? totalLike;
  final String? datePost;
  final String? name;
  final String? pathImageReact;
  final String? textReact;
  final Color? colorlabelreact;
  final int? selectedReaction;
  final int? totalReactType;
  final String? imageReact1;
  final String? imageReact2;
  final String? imageReactselectdefoult;
  final String? nameusercomment;
  final String? commenttext;
  final String? commentprofile;
  final VoidCallback? onTapBodycomment;
  final VoidCallback? onTapcomment;
  final Function(String?values,bool isChecked) onReactionChanged; 
  TimelineCardWidget({Key? key, this.overlayLoading,this.editPost, this.deletePost,this.imageReactselectdefoult,this.imageProfile, this.discriptionChild, this.viewChild, this.comments, this.reaction, this.totalLike, required this.onReactionChanged, required this.datePost, this.name, this.pathImageReact, this.textReact, this.colorlabelreact, this.selectedReaction, this.totalReactType, this.imageReact1, this.imageReact2, this.displayVoice, this.displayFile, this.nameusercomment, this.commenttext, this.commentprofile, this.onTapBodycomment, this.onTapcomment, required this.moreBtn,}) : super(key: key);
    @override
  Widget build(BuildContext context) {
    return Stack(
      children:[
        Container(
          padding:EdgeInsets.only(top: 22,bottom: comments==0?16.0 :5.0),
          color: Colorconstand.neutralWhite,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 22.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin:const EdgeInsets.only(right: 9.0),
                          child: Profile(height: 45, imageProfile: imageProfile!, namechild: '', onPressed: () {}, pandding: 0, sizetextname: 0, width: 45),
                        ),
                        Container(
                          alignment: Alignment.bottomCenter,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Align(
                                child: Text(name!,style: ThemsConstands.button_semibold_16.copyWith(color:Colors.black,fontWeight: FontWeight.w500),),
                              ),
                              const SizedBox(height: 2),
                              Align(
                                child: Text("${datePost!} ",style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.neutralDarkGrey),),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
    
                    moreBtn
                  ],
                ),
              ),
              Container(child: discriptionChild),
              Container(child: displayVoice),
              Container(child: viewChild),
              Container( child: displayFile),
              Column(
                children: [
                  comments==0&&totalLike==0?Container():Container(
                    padding:const EdgeInsets.only(left: 22.0,right: 22.0,top: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        totalLike==0?Container():Row(
                          children: [
                           SizedBox(
                             width:totalReactType! <=1? 16:28,
                             child: Stack(
                              children: [
                                Container(
                                  height: 16,width: 16,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(width: 1,color: Colors.white),
                                    color: Colors.white
                                  ),
                                  child: Image.asset(imageReact2!),
                                ),
                                totalReactType! <=1?Container():Positioned(
                                  left: 12,
                                  top: 0,bottom: 0,
                                  child: Container(
                                    height: 16,width: 16,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(width: 1,color: Colors.white),
                                      color: Colors.white
                                    ),
                                    child: Image.asset(imageReact1!),
                                  ),
                                ),
                              ],
                            ),
                           ), 
                           const SizedBox(width: 5,),
                            Text(totalLike.toString(),style:const TextStyle(fontSize: 14.0),),
                            const SizedBox(width: 5.0,),
                            Center(child: Text(reaction!,style:const TextStyle(fontSize: 14.0),))
                          ],
                        ),
                        comments==0?Container():Row(
                          children: [
                            Text(comments.toString(),style:const TextStyle(fontSize: 14,color: Colorconstand.neutralDarkGrey)),
                            const SizedBox(width: 5.0,),
                            const Text('Comment',style: TextStyle(fontSize: 14,color: Colorconstand.neutralDarkGrey)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin:const EdgeInsets.only(top: 12.0),
                    padding:const EdgeInsets.symmetric(horizontal: 12.0),
                    child:const Divider(
                      height: 1,
                      color: Color.fromRGBO(0, 0, 0, 0.38),
                    ),
                  ),
                  Container(
                    padding:const EdgeInsets.symmetric(horizontal: 32.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: ReactionButtonToggle<String>(
                              reactions: reactions,
                              initialReaction: Reaction<String>(
                                value: textReact,
                                icon: Row(
                                  children: <Widget>[
                                    Image.asset(pathImageReact!, height: 20),
                                    const SizedBox(width: 5),
                                    Text(
                                      textReact!,style: TextStyle(color: colorlabelreact,fontSize: 16)
                                    )
                                  ],
                                ),
                              ),
                              selectedReaction: reactions[selectedReaction!],
                              onReactionChanged: onReactionChanged,
                                
                              
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: onTapcomment,
                          child: Container(
                            padding:const EdgeInsets.symmetric(vertical: 12.0),
                            child: Row(
                              children: [
                                SvgPicture.asset(ImageAssets.comment_icon),
                                const SizedBox(
                                  width: 8.0,
                                ),
                                const Align(
                                  child: Text("Comment",style: TextStyle(color: Colorconstand.primaryColor,fontSize: 16),),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin:const EdgeInsets.only(top: 0.0),
                    padding:const EdgeInsets.symmetric(horizontal: 12.0),
                    child:const Divider(
                      height: 1,
                      color: Colors.black38,
                    ),
                  ),
                  comments==0?Container(): InkWell(
                    onTap: onTapBodycomment,
                    child: Container(
                      padding:const EdgeInsets.symmetric(vertical: 12.0,horizontal: 12.0),
                      child: Container(
                        padding:const EdgeInsets.symmetric(vertical: 3.0,horizontal: 6.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Profile(
                             imageProfile: commentprofile!,
                             namechild: '',
                             onPressed: () {  },
                             pandding: 0,
                             sizetextname: 0,
                             width: 40,
                             height: 40,
                            ),
                            Expanded(
                              child: Container(
                                margin:const EdgeInsets.only(left: 8.0,right: 18.0),
                                padding: const EdgeInsets.symmetric(horizontal: 12.0,vertical: 5.0),
                                decoration: BoxDecoration(
                                  borderRadius:const BorderRadius.all(Radius.circular(12)),
                                   color: Colorconstand.neutralGrey.withOpacity(0.4),
                                ),
                               
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(nameusercomment!,style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.lightBlack,fontWeight: FontWeight.w500),),
                                    Text(commenttext!,style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey,),maxLines: 3,overflow: TextOverflow.ellipsis,),
                                  ],
                                ),
                              ),
                            ),
                           
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        overlayLoading == true ?  
          Positioned(
            top: 10,
            bottom: 10,
            left: 0,
            right: 0,
            child: Container(
              color: Colors.black.withOpacity(.2),
            ),
        ):Container(),
      ],
    );
  }
}