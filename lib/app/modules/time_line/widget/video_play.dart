
import 'dart:io';

import 'package:camis_teacher_application/app/modules/time_line/widget/video_view.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  final String? linkVideo;
  const VideoPlayerWidget({
    Key? key,
    required this.linkVideo, required File videoUrl,
  }) : super(key: key);

  @override
  State<VideoPlayerWidget> createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  @override
  void initState() {
    super.initState();
    widget.linkVideo;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
      ),
      body: SafeArea(
        child: Center(
          child: VideoView(autoplay: true,videoPlayerController: VideoPlayerController.network(widget.linkVideo!),),
        ),
      ),
    );
  }
}