// ignore_for_file: must_be_immutable

import 'dart:async';

import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:camis_teacher_application/app/modules/time_line/school_time_line/bloc/school_time_line_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../widget/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../core/constands/color_constands.dart';
import '../../core/resources/asset_resource.dart';
import 'class_time_line/view/class_time_line.dart';
import 'school_time_line/view/school_time_line.dart';
class TimeLineScreen extends StatefulWidget {
  String? role;
  int? selectIndex = 0;
  bool isPrinciple;
  TimeLineScreen({Key? key,this.selectIndex,required this.role,required this.isPrinciple}) : super(key: key);

  @override
  State<TimeLineScreen> createState() => _TimeLineScreenState();
}

class _TimeLineScreenState extends State<TimeLineScreen> {
  String studentID = "";
  String classID = "";

  StreamSubscription? stsub;
  bool connection = true;

  @override
  void initState() {
    super.initState();
     //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
   
   //=============Check internet====================
    setState(() {
      BlocProvider.of<SchoolSocialBloc>(context).add( GetSchooltimeLineEvent(isRefresh: false,isPrinciple: widget.isPrinciple));
      // BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
      BlocProvider.of<GetClassBloc>(context).add(GetClass());
    });
  }

  @override
  void dispose() { 
    stsub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
  return Scaffold(
    bottomNavigationBar: connection == false || widget.role == "1"
          ? Container(height: 0)
          : const BottomNavigateBar(isActive:4),
      body: Container(
        color: Colorconstand.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset("assets/images/Oval.png"),
            ),
            Positioned(
              left: 0,
              top: 50,
              child: SvgPicture.asset( ImageAssets.path20,color: Colorconstand.mainColorSecondary)
            ),
            Positioned(
              top: 0,left: 0,right: 0,bottom: 0,
              child: WillPopScope(
                onWillPop: ()async{
                  return false;
                },
                child: SafeArea(
                  bottom: false,
                  child: Column(
                    children:[
                      widget.role == "1"?Container(
                        margin:const EdgeInsets.only(top: 0.0,bottom: 12.0,left: 12,right: 12),
                        child: Row(
                          children: [
                            IconButton(
                                      onPressed: (){
                                        Navigator.of(context).pop();
                                      }, 
                                    icon:const Icon(Icons.arrow_back_ios_outlined,color: Colorconstand.neutralWhite,size: 22,)
                                    ),
                            Text("SCHOOL_TIMELINE".tr(),style: TextStyle(color: widget.selectIndex != 0?Colorconstand.neutralGrey.withOpacity(0.7): Colorconstand.neutralWhite,fontSize: 18.0),overflow: TextOverflow.ellipsis,),
                          ],
                        ),
                      )
                      :Container(
                        margin:const EdgeInsets.only(top: 0.0,bottom: 12.0,left: 12,right: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children:[
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin:const EdgeInsets.only(left: 0.0),
                                    child:TextButton(
                                      onPressed:connection == false ? null :  () {
                                        setState(() {
                                         widget.selectIndex = 0;
                                        });
                                      },
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            height: 6,width: 6,color: Colors.transparent,
                                          ),
                                          const SizedBox(height: 5.0,),
                                          Text("SCHOOL_TIMELINE".tr(),style: TextStyle(color: widget.selectIndex != 0?Colorconstand.neutralGrey.withOpacity(0.7): Colorconstand.neutralWhite,fontSize: 18.0),overflow: TextOverflow.ellipsis,),
                                          const SizedBox(height: 5.0,),
                                          Container(
                                            decoration: BoxDecoration(
                                              color: widget.selectIndex==0 ? Colors.white:Colors.transparent,
                                              shape: BoxShape.circle
                                            ),
                                            height: 6,
                                            width: 6,
                                          )
                                        ],
                                      )
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin:const EdgeInsets.only(left: 8.0),
                                    child:TextButton(
                                      onPressed: connection == false?null: (){
                                        setState(() {
                                          widget.selectIndex =1;
                                        });
                                      },
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(height: 6,width: 6,color: Colors.transparent),
                                          const SizedBox(height: 5.0,),
                                          Text("CLASS_TIMELINE".tr(),style: TextStyle(color: widget.selectIndex==0?Colorconstand.neutralGrey.withOpacity(0.7): Colorconstand.neutralWhite,fontSize: 18.0),overflow: TextOverflow.ellipsis,),
                                          const SizedBox(height: 5.0,),
                                          Container(
                                            decoration: BoxDecoration(
                                              color:widget.selectIndex==0 ? Colors.transparent:Colorconstand.neutralWhite,
                                              shape: BoxShape.circle
                                            ),
                                            height: 6,
                                            width: 6,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: widget.selectIndex==0 ? SchoolTimeline(isPrinciple: widget.isPrinciple,):const ClassTimelineScreen(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
          ],
        ),
      ),
    );
  }
}
