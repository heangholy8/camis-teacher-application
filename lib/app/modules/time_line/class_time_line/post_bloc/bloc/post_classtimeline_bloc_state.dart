part of 'post_classtimeline_bloc_bloc.dart';

abstract class PostClasstimelineBlocState extends Equatable {
  const PostClasstimelineBlocState();
  
  @override
  List<Object> get props => [];
}

class PostClasstimelineBlocInitial extends PostClasstimelineBlocState {}

class PostClassTimelineLoadingState extends PostClasstimelineBlocState{}

class PostClassTimelineSuccessState extends PostClasstimelineBlocState{
  final bool isSuccess;
  const PostClassTimelineSuccessState({required this.isSuccess});
}

class PostClassTimelineErrorState extends PostClasstimelineBlocState{
  final String error;
  const PostClassTimelineErrorState({required this.error});
}
