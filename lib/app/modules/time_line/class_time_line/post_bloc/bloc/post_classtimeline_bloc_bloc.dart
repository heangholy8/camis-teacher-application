import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../service/api/time_line_api/post_classtimeline_api.dart';

part 'post_classtimeline_bloc_event.dart';
part 'post_classtimeline_bloc_state.dart';

class PostClasstimelineBlocBloc extends Bloc<PostClasstimelineBlocEvent, PostClasstimelineBlocState> {
  List<String> classId = [];
  PostClasstimelineBlocBloc() : super(PostClasstimelineBlocInitial()) {
    on<PostClassTimelineEvent>((event, emit) async{
      emit(PostClassTimelineLoadingState());
      try {        

        await posClassTimelineApi(postText: event.postText,parent: event.parent,attachments: event.attachment,classID: event.classID);
        emit(const PostClassTimelineSuccessState(isSuccess: true));

      } catch (e) {
        
        emit(PostClassTimelineErrorState(error: e.toString())); 
      }
    });
  }
}

