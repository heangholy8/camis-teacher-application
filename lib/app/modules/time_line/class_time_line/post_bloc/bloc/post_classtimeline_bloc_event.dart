part of 'post_classtimeline_bloc_bloc.dart';

abstract class PostClasstimelineBlocEvent extends Equatable {
  const PostClasstimelineBlocEvent();

  @override
  List<Object> get props => [];
}



class PostClassTimelineEvent extends PostClasstimelineBlocEvent{

  final String postText;
  final String parent;
  final List<File> attachment;
  final List<String> classID;
  const PostClassTimelineEvent({ required this.postText, required this.parent, required this.attachment,required this.classID});
}