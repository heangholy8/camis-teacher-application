import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/cubit/select_media_cubit.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/widgets/header_widget.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/widgets/preview_attachfile_widget.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/widgets/type_announcement_create.dart';
import 'package:camis_teacher_application/app/modules/time_line/class_time_line/post_bloc/bloc/post_classtimeline_bloc_bloc.dart';
import 'package:camis_teacher_application/app/modules/time_line/school_time_line/bloc/post_school_timeline/post_school_timeline_bloc.dart';
import 'package:camis_teacher_application/app/modules/time_line/school_time_line/bloc/school_time_line_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:images_picker/images_picker.dart';
import '../../../../../widget/view_document.dart';
import '../../../../models/timeline/school_timeline.dart';
import '../../../../service/api/time_line_api/post_classtimeline_api.dart';
import '../../../student_list_screen/bloc/student_in_class_bloc.dart';
import '../../time_line.dart';
import '../../widget/view_image.dart';

class CreateUpdateSchoolTimeline extends StatefulWidget {
  final bool? hideTitle;
  Datum? data;
  bool update;

  CreateUpdateSchoolTimeline({ required this.update,super.key,this.hideTitle=false,this.data});

  @override
  State<CreateUpdateSchoolTimeline> createState() =>
    _CreateUpdateSchoolTimelineState();
}

class _CreateUpdateSchoolTimelineState extends State<CreateUpdateSchoolTimeline> with Toast{
  final titleAnnouncement = TextEditingController();
  final subtitleAnnouncement = TextEditingController();
  bool checkValue = false;
  List<String> imageExtension = ['.jpg', '.png', '.jpeg', '.gif'];
  List<File> listMultiMedia = [];
  List<AttachmentSchoolTimeline> netWorkMedia = [];
  String? thumbnailPath;
  List<Color> classColor = [const Color.fromARGB(53, 58, 88, 255)];
  bool isSuccess = true;
  bool isError = false;
  var listImageVideo = [];
  bool loadingUpdate=false ;
  List<String> delectAtt = [];

  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();  
    BlocProvider.of<GetClassBloc>(context).add(GetClass());
    if(widget.data!=null){
      subtitleAnnouncement.text = "${widget.data?.postText}";
      netWorkMedia = widget.data!.attachments!;
    }
    setState(() {
      if(widget.update == true){
      listImageVideo = widget.data!.attachments!.where((element) => element.fileType!.contains("image") || element.fileType!.contains("video")).toList();
      }
    });
    
    subtitleAnnouncement.addListener(() {
      setState(() {
        checkValue = subtitleAnnouncement.text.isNotEmpty;
      });
    });
  }

  @override
  void dispose() {
    titleAnnouncement.dispose();
    subtitleAnnouncement.dispose();
    super.dispose();
  }

  Future<void> getMediaFromDevice() async {
    var listMedia;
    if(defaultTargetPlatform == TargetPlatform.android){
       listMedia = await _picker.pickMultipleMedia();
    }
    else{
      listMedia = await ImagesPicker.pick(
        count: 10,
        gif: true,
        pickType: PickType.all,
        language: Language.System,
        maxTime: 3600,
        quality: 0.8,
        cropOpt: CropOption(aspectRatio: CropAspectRatio.wh16x9),
      );
    }
    if (listMedia!.isNotEmpty) {
      for (var item in listMedia) {
        setState(() {
          listMultiMedia.add(File(item.path));
          BlocProvider.of<MediaCubit>(context).pickImgVdoFileCubit(listMedia: listMultiMedia);
        });
      }
    }
  }

  Future<void> getFileFromDevice() async {
    final result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'cvs',"mp4","png","jpge"]);
    if (result != null) {
      List<File> files = result.paths.map((e) => File(e.toString())).toList();
      for (var item in files) {
        setState(() {
          listMultiMedia.add(File(item.path));
          BlocProvider.of<MediaCubit>(context)
              .pickImgVdoFileCubit(listMedia: listMultiMedia);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    return Stack(
      children: 
        [
          Scaffold(
          backgroundColor: Colorconstand.primaryColor,
          body: SafeArea(
            bottom: false,
            child: BlocListener<PostSchoolTimelineBloc, PostSchoolTimelineState>(
              listener: (context, state) {
                if(state is PostSchoolTimelineloading){
                  setState(() {
                    loadingUpdate = true;
                  });
                }else if(state is PostSchoolTimelineLoaded){
                  setState(() {
                    BlocProvider.of<SchoolSocialBloc>(context).add( const GetSchooltimeLineEvent(isRefresh: true,isPrinciple:true));
                    Future.delayed(const Duration(seconds: 5),(){
                      setState(() {
                        loadingUpdate = false;
                        BlocProvider.of<MediaCubit>(context).filesMedia=[];
                        Navigator.push(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 0,role: "1",isPrinciple: true,),
                            transitionDuration: Duration.zero,
                            reverseTransitionDuration: Duration.zero,
                          ),
                        );
                      });
                    });
                  });
                } else if(state is PostSchoolTimelineError) {
                  loadingUpdate = false;
                  Future.delayed(const Duration(milliseconds: 400),(){
                    showAlertLogout(
                      icon:SvgPicture.asset(ImageAssets.warning_icon,width: 48,color: Colors.red,),
                      onSubmit: () {
                        Navigator.pop(context);
                      },
                      title: "ការបង្ហោះមានបញ្ហា! សូមព្យាយាមម្ដងទៀត".tr(),
                      onSubmitTitle: "Try again".tr(),
                      context: context
                    );            
                  });
                  print("errororor");
                }
              },
              child: Stack(
                children:[
                  SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0, bottom: 20),
                        child: HeaderWidget(
                          screenTitle:widget.update==true?"EDIT".tr():"CREATECONTENT".tr(),
                          disabled:subtitleAnnouncement.text.isEmpty? true : false,
                          onBack: () {
                            BlocProvider.of<MediaCubit>(context).filesMedia=[];
                            Navigator.of(context).pop();
                          },
                          onSend: widget.update == true?(){
                            setState(() {
                              loadingUpdate = true;
                            });
                            updateSchoolTimelineApi(
                              postText: subtitleAnnouncement.text.toString(),
                              parent: "0",
                              attachments: BlocProvider.of<MediaCubit>(context).filesMedia,
                              postID: widget.data!.id.toString()
                            ).then((value){
                              if(value == true){
                                setState(() {
                                BlocProvider.of<SchoolSocialBloc>(context).add( const GetSchooltimeLineEvent(isRefresh: true,isPrinciple:true));
                                Future.delayed(const Duration(seconds: 5),(){
                                  setState(() {
                                    loadingUpdate = false;
                                     BlocProvider.of<MediaCubit>(context).filesMedia=[];
                                    Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                        pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 0,role: "1",isPrinciple: true,),
                                        transitionDuration: Duration.zero,
                                        reverseTransitionDuration: Duration.zero,
                                      ),
                                    );
                                  });
                                });
                              });
                              }
                              else{
                                Future.delayed(const Duration(milliseconds: 400),(){
                                  showAlertLogout(
                                    icon:SvgPicture.asset(ImageAssets.warning_icon,width: 48,color: Colors.red,),
                                    onSubmit: () {
                                      Navigator.pop(context);
                                    },
                                    title: "ការបង្ហោះមានបញ្ហា! សូមព្យាយាមម្ដងទៀត".tr(),
                                    onSubmitTitle: "Try again".tr(),
                                    context: context
                                  );            
                                });
                              }
                              
                            });
                          } : (){
                            FocusScope.of(context).unfocus();
                            if(BlocProvider.of<MediaCubit>(context).filesMedia.isEmpty && subtitleAnnouncement.text.isEmpty){
                            } else {
                              BlocProvider.of<PostSchoolTimelineBloc>(context).add(
                                PostSchoolTimelineData(
                                  postText: subtitleAnnouncement.text,
                                  parent: "0", 
                                  attachment: BlocProvider.of<MediaCubit>(context).filesMedia,
                                ),
                              );
                            }
                          },
                        ),
                      ),
                      Expanded(
                        child: Stack(
                          children: [
                            Container(
                              alignment: Alignment.topCenter,
                              padding: const EdgeInsets.only(left: 14.0,  right: 20),
                              width: double.maxFinite,
                              decoration: const BoxDecoration(
                                color: Colorconstand.screenWhite,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10.0),
                                  topRight: Radius.circular(10.0),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      widget.hideTitle == false ? TextField(
                                        maxLines: null,
                                        controller: titleAnnouncement,
                                        keyboardType: TextInputType.text,
                                        style: ThemsConstands.headline3_semibold_20,
                                        decoration: InputDecoration(
                                          hintText: "HINTANN".tr(),
                                          hintStyle: ThemsConstands.headline3_semibold_20,
                                          border: InputBorder.none,
                                        ),
                                      ) : Container(),
                                      const SizedBox(height: 8,),
                                      // const Divider(
                                      //   color: Colorconstand.neutralGrey,
                                      // ),
                                      TextField(
                                        maxLines: null,
                                        controller: subtitleAnnouncement,
                                        keyboardType: TextInputType.text,
                                        decoration: InputDecoration(
                                          contentPadding:const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                                          enabledBorder: const UnderlineInputBorder(      
                                            borderSide: BorderSide(color: Colorconstand.darkBackgroundsDisabled),   
                                          ),  
                                          focusedBorder:  UnderlineInputBorder(
                                            borderSide: BorderSide(color: Colorconstand.primaryColor.withOpacity(.8)),
                                          ),  
                                          hintText: "DESANN".tr(),
                                          hintStyle: ThemsConstands.headline_5_medium_16.copyWith(
                                            color: Colorconstand.lightTrunks,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 18,
                                  ),
                                  Container(
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: BlocBuilder<MediaCubit, MediaSelecteStatus>(
                                        builder: (context, state) {
                                          if (state == MediaSelecteStatus.loading) {
                                            return const Center(
                                              child: CircularProgressIndicator(),
                                            );
                                          } else if (state == MediaSelecteStatus.success) {
                                            var data = BlocProvider.of<MediaCubit>(context) .filesMedia;
                                            return Column(
                                              children: [
                                                GridView.builder(
                                                  shrinkWrap: true,
                                                  physics:const BouncingScrollPhysics(),
                                                  scrollDirection: Axis.vertical,
                                                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                                    crossAxisCount: 3, // Number of columns
                                                    crossAxisSpacing:  6, // Spacing between columns
                                                    mainAxisSpacing: 6, // Spacing between rows
                                                  ),
                                                  itemCount: data.length, // Number of items
                                                  itemBuilder: _buildMediaItem,
                                                ),
                                                // SizedBox(
                                                //   height: 100,
                                                //   width: MediaQuery.of(context).size.width,
                                                // ),
                                              ],
                                            );
                                          } else if (state == MediaSelecteStatus.failure) {
                                            return  Text("Please try again!",style: ThemsConstands.button_semibold_16.copyWith(color: Colors.red),);
                                          }
                                          return Container();
                                        },
                                      ),
                                    ),
                                  ),
                                  widget.update == true? widget.data!.attachments!.isEmpty?Container(): Container(
                                    height: 135,
                                    child: ListView.separated(
                                      separatorBuilder: (context, index) {
                                        return Container(width: 5,);
                                      },
                                      padding:const EdgeInsets.all(0),
                                      scrollDirection: Axis.horizontal,
                                      itemCount: widget.data!.attachments!.length,
                                      itemBuilder: (context, index) {
                                        return Container(
                                          height: 128,
                                          width: 128,
                                          child: Stack(
                                            children: [
                                              widget.data!.attachments![index].fileType!.contains("image") || widget.data!.attachments![index].fileType!.contains("video")?
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                    context,
                                                    PageRouteBuilder(
                                                      pageBuilder: (_, __, ___) =>
                                                          ImageViewDownloads(
                                                        listimagevide: listImageVideo,
                                                        activepage: index,
                                                      ),
                                                      transitionDuration:
                                                          const Duration(seconds: 0),
                                                    ),
                                                  );
                                                },
                                                child: Container(
                                                  child: Image.network(widget.data!.attachments![index].fileThumbnail.toString(),width: 128,height: 128,fit: BoxFit.cover,)
                                                ),
                                              ):GestureDetector(
                                                onTap: () {
                                                  Navigator.of(context)
                                              .push(MaterialPageRoute(
                                                  builder: (
                                                      context,
                                                    ) =>
                                                                ViewDocuments(
                                                                  path: widget.data!.attachments![index].fileShow.toString(),
                                                                  filename:
                                                                      widget.data!.attachments![index].fileName.toString(),
                                                                )));
                                                },
                                                child: Container(
                                                  height: 128,width: 128,
                                                  color: Colorconstand.neutralGrey,
                                                  child: Image.asset(ImageAssets.pdf_icon,width: 44,height: 45,),
                                                ),
                                              ),
                                              Positioned(
                                                top: 4,
                                                right: 4,
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      delectAtt.addAll([widget.data!.attachments![index].id.toString()]);
                                                      delectAttSchoolTimeLine(idAtta:delectAtt).then((value) {
                                                        setState(() {
                                                          delectAtt = [];
                                                          BlocProvider.of<SchoolSocialBloc>(context).add( const GetSchooltimeLineEvent(isRefresh: true,isPrinciple:true));
                                                        });
                                                      });
                                                      widget.data!.attachments!.removeAt(index);
                                                    });
                                                  },
                                                  child: const CircleAvatar(
                                                    radius: 12,
                                                    child: Icon(Icons.close,size: 18)
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ):Container(height: 0,)
                                  
                                ],
                              ),
                            ),
                            buttomKeyboardOpenWidget(!isKeyboard),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                //  isSuccess == false ? Positioned(
                //     top: 0,
                //     bottom: 0,
                //     left: 0,
                //     right: 0,
                //     child: Container(
                //       alignment: Alignment.center,
                //       color: Colorconstand.primaryColor.withOpacity(.2),
                //       child: const CircularProgressIndicator(),
                //     ),
                //   ): Container(),
                ],
              ),
            ),
          ),
        ),

       loadingUpdate == true? Container(
          color: Colorconstand.neutralGrey.withOpacity(0.4),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.height,
          child: Center(child: CircularProgressIndicator()),
        ):Container(height: 0,)
      ],
    );
  }

  Widget _buildMediaItem(BuildContext context, int index) {
    final mediaPath = BlocProvider.of<MediaCubit>(context).filesMedia[index];
    final extension = mediaPath.path.toLowerCase();

    if (extension.contains('.jpg') ||
        extension.contains('.png') ||
        extension.contains('.jpeg')) {
      return InkWell(
        onTap: () {
          Navigator.push( context, MaterialPageRoute(
              builder: (context) => PreviewAttachFileWidget(
                imageIndex: index,
              ),
            )
          );
        },
        child: Stack(
          children:[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                image: DecorationImage(
                  image: FileImage(
                    File(
                      mediaPath.path,
                    ),
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              top: 4,
              right: 4,
              child: InkWell(
                onTap: () {
                  BlocProvider.of<MediaCubit>(context).removeFile(index: index);
                },
                child: const CircleAvatar(
                  radius: 12,
                  child: Icon(Icons.close,size: 18)
                ),
              ),
            )
          ],
        ),
      );
    } else if (extension.contains('.mov') || extension.contains('.mp4')) {
      return InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PreviewAttachFileWidget(
                imageIndex: index,
              ),
            ),
          );
        },
        child: Container(
          padding: const EdgeInsets.all(38),
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(2),
          ),
          child: SvgPicture.asset(
            ImageAssets.play_icon,
          ),
        ),
      );
    } else if (extension.contains('.pdf') || extension.contains('.mp3')) {
      return InkWell(
        onTap: () {
          print("PDF");
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PreviewAttachFileWidget(
                        imageIndex: index,
                      )));
        },
        child: Container(
          padding: const EdgeInsets.all(42),
          decoration: BoxDecoration(
            color: const Color(0xFFE5EBF4),
            borderRadius: BorderRadius.circular(2),
          ),
          child: SvgPicture.asset(
            ImageAssets.filldoc_icon,
          ),
        ),
      );
    } else {
      return Container(
        color: Colors.green,
      );
    }
  }
  Widget buttomKeyboardOpenWidget(bool isKeyboard) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Divider(
            color: Colorconstand.neutralGrey,
          ),
          !isKeyboard
              ? Container(
                  color: Colorconstand.neutralBtnBg,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20.0, top: 8.0, bottom: 10),
                    child: Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() async{
                              isKeyboard = true;
                             await getMediaFromDevice();
                            });
                          },
                          child: SvgPicture.asset(
                            ImageAssets.GALLERY_ICON,
                            height: 28,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24.0),
                          child: GestureDetector(
                            onTap: () async {
                              await getFileFromDevice();
                              
                            },
                            child: SvgPicture.asset(
                              ImageAssets.DOCUMENT_ICON,
                              height: 28,
                            ),
                          ),
                        ),
                       
                      ],
                    ),
                  ),
                )
              : Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 19, vertical: 24),
                  decoration: const BoxDecoration(
                    color: Colorconstand.neutralBtnBg,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      // Pick up Image and Video
                      Expanded(
                        child: TypeAnnouncementCreate(
                          svgAsset: ImageAssets.GALLERY_ICON,
                          title: "IMAGEANDVDO".tr(),
                          onPressed: () {
                            getMediaFromDevice();
                          },
                        ),
                      ),
                      const SizedBox(
                        width: 16.0,
                      ),

                      /// Pick up Document
                      Expanded(
                        child: TypeAnnouncementCreate(
                          title: 'DOC'.tr(),
                          svgAsset: ImageAssets.DOCUMENT_ICON,
                          onPressed: () async {
                            await getFileFromDevice();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
        ],
      ),
    );
  }

  Widget bottomPicAndDocWidget() {
    return Positioned(
        bottom: 0,
        right: 0,
        left: 0,
        child: Container(
          height: 120,
          padding: const EdgeInsets.only(
            left: 19.0,
            right: 19.0,
          ),
          decoration: const BoxDecoration(
            color: Colorconstand.neutralBtnBg,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0)),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: TypeAnnouncementCreate(
                  svgAsset: ImageAssets.GALLERY_ICON,
                  title: "PHOTO".tr(),
                  onPressed: () {
                    getMediaFromDevice();
                  },
                ),
              ),
              const SizedBox(
                width: 16.0,
              ),
              Expanded(
                child: TypeAnnouncementCreate(
                  title: 'ឯកសារ',
                  svgAsset: ImageAssets.DOCUMENT_ICON,
                  onPressed: () {},
                ),
              )
            ],
          ),
        ));
  }
}

