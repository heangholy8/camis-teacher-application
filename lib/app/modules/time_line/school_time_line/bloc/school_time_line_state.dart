// ignore_for_file: must_be_immutable

part of 'school_time_line_bloc.dart';

abstract class SchoolTimeLineState extends Equatable {
  const SchoolTimeLineState();

  @override
  List<Object> get props => [];
}

class SchoolTimeLineInitial extends SchoolTimeLineState {}

class SchoolsocialLoadingState extends SchoolTimeLineState {
  final List<Datum> oldData;
  bool isFirstFetch;
  SchoolsocialLoadingState({required this.oldData, this.isFirstFetch = false});
}

/// SchoolDetailSocial
class SchoolTimeLineLoadingState extends SchoolTimeLineState {
  final String message;

  const SchoolTimeLineLoadingState({
    required this.message,
  });
}

class DetailTimeLineLoaded extends SchoolTimeLineState {
  final GetDetailSchoolTimelineModel? timeLineDetailmodel;
  const DetailTimeLineLoaded({this.timeLineDetailmodel});
}

class SchoolTimeLineErrorState extends SchoolTimeLineState {
  final String error;

  const SchoolTimeLineErrorState({
    required this.error,
  });
}

// SchoolSocial

class SchoolsocialSuccessState extends SchoolTimeLineState {
  final List<Datum> data;
  final bool hasReachedMax;

  const SchoolsocialSuccessState(
      {required this.data, required this.hasReachedMax});
}

class AddmoreLoadingState extends SchoolTimeLineState {
  const AddmoreLoadingState();
}

class SchoolsocialErrorState extends SchoolTimeLineState {
  final String error;
  const SchoolsocialErrorState({required this.error});
}
