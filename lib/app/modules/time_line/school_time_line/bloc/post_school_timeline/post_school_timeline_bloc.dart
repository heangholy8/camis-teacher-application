import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/time_line_api/post_classtimeline_api.dart';
import 'package:equatable/equatable.dart';

part 'post_school_timeline_event.dart';
part 'post_school_timeline_state.dart';

class PostSchoolTimelineBloc extends Bloc<PostSchoolTimelineEvent, PostSchoolTimelineState> {
  PostSchoolTimelineBloc() : super(PostSchoolTimelineInitial()) {
    on<PostSchoolTimelineEvent>((event, emit) {
      // TODO: implement event handler
    });
    on<PostSchoolTimelineData>((event, emit) async {
      emit(PostSchoolTimelineloading());
      try {        
        await posSchoolTimelineApi(postText: event.postText,parent: event.parent,attachments: event.attachment);
        emit(const PostSchoolTimelineLoaded(isSuccess: true));
      } catch (e) {
        emit(PostSchoolTimelineError()); 
      }
    });
  }
}
