part of 'post_school_timeline_bloc.dart';

sealed class PostSchoolTimelineState extends Equatable {
  const PostSchoolTimelineState();
  
  @override
  List<Object> get props => [];
}

final class PostSchoolTimelineInitial extends PostSchoolTimelineState {}

final class PostSchoolTimelineloading extends PostSchoolTimelineState {}
final class PostSchoolTimelineLoaded extends PostSchoolTimelineState {
  final bool isSuccess;
  const PostSchoolTimelineLoaded({required this.isSuccess});
}
final class PostSchoolTimelineError extends PostSchoolTimelineState {}
