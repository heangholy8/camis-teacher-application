part of 'post_school_timeline_bloc.dart';

sealed class PostSchoolTimelineEvent extends Equatable {
  const PostSchoolTimelineEvent();

  @override
  List<Object> get props => [];
}


class PostSchoolTimelineData extends PostSchoolTimelineEvent{

  final String postText;
  final String parent;
  final List<File> attachment;
  const PostSchoolTimelineData({ required this.postText, required this.parent, required this.attachment});
}
