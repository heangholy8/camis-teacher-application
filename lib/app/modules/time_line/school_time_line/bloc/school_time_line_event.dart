part of 'school_time_line_bloc.dart';

abstract class SchoolTimeLineEvent extends Equatable {
  const SchoolTimeLineEvent();

  @override
  List<Object> get props => [];
}

class SchoolTimeLineFetchEvent extends SchoolTimeLineEvent {
  const SchoolTimeLineFetchEvent();
}

class SchoolTimeLineRefreshEvent extends SchoolTimeLineEvent {
  const SchoolTimeLineRefreshEvent();
}



class GetSchooltimeLineEvent extends SchoolTimeLineEvent {
  final bool isRefresh;
  final bool isPrinciple;
  const GetSchooltimeLineEvent({required this.isRefresh,required this.isPrinciple});
}


class GetDetailbyIDPostEvent extends SchoolTimeLineEvent{
  final int postID;
  const GetDetailbyIDPostEvent({required this.postID});

}
