import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/update_student_profile_bloc.dart';
import 'package:camis_teacher_application/app/service/api/auth_api/auth_api.dart';
import 'package:camis_teacher_application/widget/internet_connect_widget/internet_connect_widget.dart';
import 'package:camis_teacher_application/widget/take_upload_image.dart';
import 'package:camis_teacher_application/widget/textFormfiled_custom/textformfiled_custom.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import '../../core/constands/color_constands.dart';
import '../../models/student_list_model/list_student_in_class_model.dart';
import 'bloc/bloc/update_profile_bloc.dart';
import '../account_confirm_screen/bloc/profile_user_bloc.dart';

class ProfileUserScreen extends StatefulWidget {
  final String? profileMedia;
  final String? userRole;
  final String? firstname;
  final String? firstnameEn;
  final String? phone;
  final String? lastname;
  final String? lastnameEn;
  final int? gender;
  final String? address;
  final String? gmail;
  final String? dob;
  final int? role;
  final int? leadership;
  final String? translate;
  final String? whereEdit;
  final String? classId;
  final ClassMonitorSet? dataStudentInfo;
  const ProfileUserScreen({Key? key,this.userRole, this.dataStudentInfo, this.classId,this.leadership,this.whereEdit, this.profileMedia, this.firstname, this.phone, this.lastname, this.gender, this.address, this.gmail, this.dob, this.role, this.firstnameEn, this.lastnameEn, this.translate}) : super(key: key);

  @override
  State<ProfileUserScreen> createState() => _ProfileUserScreenState();
}

class _ProfileUserScreenState extends State<ProfileUserScreen> with Toast {
  bool connection = true;
  bool isExpanded = false;
  bool loadingsumit = false;
  late String textGender;
  StreamSubscription? sub;
  bool isChange = false;
  bool isEdit = false;
  bool cheangePass = false;

  // image variable
  final ImagePicker _picker = ImagePicker();
  File? _image;
  XFile? file;


  TextEditingController newPasswordController = TextEditingController();
  TextEditingController comfirmPasswordController = TextEditingController();
  TextEditingController currentPasswordController = TextEditingController();

  // edit == controller yourSale ===========
  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController firstnameEnController = TextEditingController();
  TextEditingController lastnameEnController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController roleController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  // edit == controller Mather ===========
  TextEditingController mather_firstnameController = TextEditingController();
  TextEditingController mather_lastnameController = TextEditingController();
  TextEditingController mather_firstnameEnController = TextEditingController();
  TextEditingController mather_lastnameEnController = TextEditingController();
  TextEditingController mather_phoneController = TextEditingController();
  TextEditingController mather_job_Controller = TextEditingController();
  TextEditingController mather_emailController = TextEditingController();
  TextEditingController mather_addressController = TextEditingController();

  // edit == controller Father ===========
  TextEditingController father_firstnameController = TextEditingController();
  TextEditingController father_lastnameController = TextEditingController();
  TextEditingController father_firstnameEnController = TextEditingController();
  TextEditingController father_lastnameEnController = TextEditingController();
  TextEditingController father_phoneController = TextEditingController();
  TextEditingController father_job_Controller = TextEditingController();
  TextEditingController father_emailController = TextEditingController();
  TextEditingController father_addressController = TextEditingController();

  String errorSelectGender = "";
  int genderNumber = 0;
  
  @override
  void initState() {
    setState(() {
      genderNumber = widget.gender!.toInt();
      firstnameController.text = widget.firstname.toString();
      lastnameController.text = widget.lastname.toString();
      firstnameEnController.text =widget.firstnameEn.toString();
      lastnameEnController.text = widget.lastnameEn.toString();
      phoneController.text = widget.phone == null?"":widget.phone.toString();
      widget.whereEdit == "profileUser"?roleController.text = widget.role!.toInt() > 0?"${"ISINSTRUCTOR".tr()} & ${"ISTEACHER".tr()}":"ISTEACHER".tr()
      :widget.leadership == 0? roleController.text = "សិស្ស": roleController.text = "ប្រធានថ្នាក់";
      dobController.text = widget.dob.toString();
      emailController.text = widget.gmail.toString();
      addressController.text = widget.address.toString();
      
    if(widget.whereEdit != "profileUser"){
      setState(() {
          isEdit = true;
        });
      //============== Mather controller ================
      mather_firstnameController.text = widget.dataStudentInfo!.motherFirstname.toString();
      mather_lastnameController.text = widget.dataStudentInfo!.motherLastname.toString();
      mather_firstnameEnController.text = widget.dataStudentInfo!.motherFirstnameEn.toString();
      mather_lastnameEnController.text = widget.dataStudentInfo!.motherLastnameEn.toString();
      mather_phoneController.text = widget.dataStudentInfo!.motherPhone.toString();
      mather_job_Controller.text = widget.dataStudentInfo!.motherJob.toString();
      mather_addressController.text = widget.dataStudentInfo!.motherAddress.toString();
      mather_emailController.text = widget.dataStudentInfo!.motherEmail.toString();

      //============== Father controller ================

      father_firstnameController.text = widget.dataStudentInfo!.fatherFirstname.toString();
      father_lastnameController.text = widget.dataStudentInfo!.fatherLastname.toString();
      father_firstnameEnController.text = widget.dataStudentInfo!.fatherFirstnameEn.toString();
      father_lastnameEnController.text = widget.dataStudentInfo!.fatherLastnameEn.toString();
      father_phoneController.text = widget.dataStudentInfo!.fatherPhone.toString();
      father_job_Controller.text = widget.dataStudentInfo!.fatherJob.toString();
      father_addressController.text = widget.dataStudentInfo!.fatherAddress.toString();
      father_emailController.text = widget.dataStudentInfo!.fatherEmail.toString();
    }
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
          } else {}
        });
      });
      //=============Eend Check internet====================
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<UpdateProfileBloc, UpdateProfileState>(
        listener: (context, state) {
          if (state is UpdateProfileLoadingState) {
            setState(() {
              loadingsumit = true;
            });
          } else if (state is UpdateProfileLoadedState) {
            showSuccessDialog(
              title: "UPDATEPROFILE".tr(),
              discription: "",
              context: context,
              callback: () {
                setState(() {
                  Navigator.of(context).pop();
                });
              },
            );
            if(widget.whereEdit == "profileUser"){
              BlocProvider.of<ProfileUserBloc>(context).add(GetUserProfileEvent(context: context));
            }
            setState(() {
              loadingsumit = false;
            });
          }
        },
        child: BlocListener<UpdateStudentProfileBloc, UpdateStudentProfileState>(
          listener: (context, state) {
            if (state is UpdateStudentProfileLoading) {
              setState(() {
                loadingsumit = true;
              });
            } else if (state is UpdateStudentProfileLoaded) {
              showSuccessDialog(
                title: "UPDATEPROFILE".tr(),
                discription: "",
                context: context,
                callback: () {
                  setState(() {
                    BlocProvider.of<GetStudentInClassBloc>(context).add(
                      GetStudentInClass(
                        idClass: widget.classId.toString(),
                      ),
                    );
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  });
                },
              );
              setState(() {
                loadingsumit = false;
              });
            }
            else{
              setState(() {
                loadingsumit = false;
              });
            }
          },
          child: SafeArea(
                  bottom: false,
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: 0,
                        left: 0,
                        child: Image.asset(
                          "assets/images/background_profile_screen.png",
                        ),
                      ),
                      Positioned(
                          top: 0,
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
                            child: Container(
                              color: Colorconstand.neutralWhite.withOpacity(0.3),
                            ),
                          )),
                      Positioned(
                        child: SafeArea(
                          bottom: false,
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.only(
                                   top: 10,  bottom: 25, left: 22,right: 22),
                                child: Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: const Icon(
                                        Icons.arrow_back_ios_new_rounded,
                                        color: Colorconstand.neutralDarkGrey,
                                        size: 22,
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        "MYPROFILE".tr(),
                                        textAlign: TextAlign.center,
                                        style: ThemsConstands.headline3_semibold_20
                                            .copyWith(
                                                color: Colorconstand.lightBlack),
                                      ),
                                    ),
                                    cheangePass == true?Container():GestureDetector(
                                      onTap: (){
                                        setState(() {
                                            isEdit = true;
                                        });
                                      },
                                       child:Text("EDIT".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralDarkGrey),),
                                    ),
                                  ],
                                ),
                              ),
                              const Divider(
                                color: Colorconstand.neutralGrey,
                                height: 1,
                                thickness: 1,
                              ),
                              Expanded(
                                child: SingleChildScrollView(
                                        child: Container(
                                          margin: const EdgeInsets.symmetric( horizontal: 0),
                                          child: Column(
                                            children: [
                                              cheangePass == true?Container():Column(
                                                children: [
                                                  GestureDetector(
                                                    onTap: isEdit==false?(){}:() {
                                                      setState(
                                                        () {
                                                          showModalBottomSheet(
                                                            backgroundColor:
                                                                Colors.transparent,
                                                            context: context,
                                                            builder: (BuildContext bc) {
                                                              return ShowPiker(
                                                                onPressedCamera: () {
                                                                  _imgFromCamera();
                                                                  Navigator.of(context)
                                                                      .pop();
                                                                },
                                                                onPressedGalary: () {
                                                                  _imgFromGallery();
                                                                  Navigator.of(context)
                                                                      .pop();
                                                                },
                                                              );
                                                            },
                                                          );
                                                        },
                                                      );
                                                    },
                                                    child: Container(
                                                        width: 108,
                                                        height: 108,
                                                        margin: const EdgeInsets.only(
                                                            top: 25, bottom: 0),
                                                        alignment: Alignment.center,
                                                        child: Stack(
                                                          alignment: Alignment.center,
                                                          children: [
                                                            CircleAvatar(
                                                              radius: 54, // Image radius
                                                              child: ClipOval(
                                                                child: _image == null
                                                                    ? Image.network(
                                                                        widget.profileMedia.toString(),
                                                                        width: 108,
                                                                        height: 108,
                                                                        fit: BoxFit.cover,
                                                                      )
                                                                    : Image.file(
                                                                        _image!,
                                                                        width: 108,
                                                                        height: 108,
                                                                        fit: BoxFit.cover,
                                                                      ),
                                                              ),
                                                            ),
                                                            _image == null
                                                                ? Container(
                                                                    width: 108,
                                                                    height: 108,
                                                                    decoration:
                                                                        const BoxDecoration(
                                                                            shape: BoxShape
                                                                                .circle,
                                                                            color: Colors
                                                                                .black12),
                                                                  )
                                                                : const SizedBox(),
                                                            _image == null
                                                                ? Center(
                                                                    child: Icon(
                                                                      Icons
                                                                          .photo_camera_outlined,
                                                                      color: isEdit == false?Colors.transparent: Colorconstand
                                                                          .neutralWhite,
                                                                      size: 28,
                                                                    ),
                                                                  )
                                                                : const SizedBox()
                                                          ],
                                                        )),
                                                  ),
                                                  isEdit == false?Container():MaterialButton(
                                                    padding: EdgeInsets.zero,
                                                    onPressed: chooseImage,
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(8.0),
                                                    ),
                                                    splashColor: Colors.transparent,
                                                    highlightColor: Colors.transparent,
                                                    child: Row(
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: [
                                                        SvgPicture.asset(
                                                          ImageAssets.camera,
                                                          color: Colorconstand.primaryColor,
                                                        ),
                                                        const SizedBox(width: 5.0),
                                                        Text(
                                                          "CHANGE_PROFILE".tr(),
                                                          style: ThemsConstands
                                                              .button_semibold_16
                                                              .copyWith(
                                                                  color: Colorconstand
                                                                      .primaryColor),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Container(
                                              margin: const EdgeInsets.symmetric(
                                                  horizontal: 22, vertical: 10),
                                              child: Column(
                                                children: [
                                                  isEdit == false? Column(
                                                    children: [
                                                      Text("${lastnameController.text} ${firstnameController.text}",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.center,),
                                                      Text("${lastnameEnController.text} ${firstnameEnController.text}",style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.left,),
                                                      widget.userRole == "1" || widget.userRole == "6"?Container():Container(
                                                          alignment: Alignment.center,child: TextButton(onPressed: (){
                                                            setState(() {
                                                              isEdit = true;
                                                              cheangePass = true;
                                                            });
                                                          }, child: Text("CHANGE_PASSWORD".tr(),style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.center,),))
                                                    ],
                                                  ):Container(),
                                                  isEdit == false?Container(
                                                    margin:const EdgeInsets.only(top: 0),
                                                    alignment: Alignment.centerLeft,
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        const SizedBox(height: 25,),
                                                        Text("${"GENDER".tr()} :",style: ThemsConstands.headline_5_medium_16,textAlign: TextAlign.left,),
                                                        const SizedBox(height: 12,),
                                                        Text(genderNumber==1?"MALE".tr():"FEMALE".tr(),style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.left,),
                                                        const Divider(height: 18,color: Colorconstand.neutralDarkGrey,),
                                                        
                                                        Text("${"DOB".tr()} :",style: ThemsConstands.headline_5_medium_16,textAlign: TextAlign.left,),
                                                        const SizedBox(height: 12,),
                                                        Text(dobController.text,style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.left,),
                                                        const Divider(height: 18,color: Colorconstand.neutralDarkGrey,),
                                                        
                                                        Text("${"PHONENUMBER".tr()} :",style: ThemsConstands.headline_5_medium_16,textAlign: TextAlign.left,),
                                                        const SizedBox(height: 12,),
                                                        Text(phoneController.text,style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.left,),
                                                        const Divider(height: 18,color: Colorconstand.neutralDarkGrey,),
                                                        
                                                        Text("${"EMAIL".tr()} :",style: ThemsConstands.headline_5_medium_16,textAlign: TextAlign.left,),
                                                        const SizedBox(height: 12,),
                                                        Text(emailController.text,style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.left,),
                                                        const Divider(height: 18,color: Colorconstand.neutralDarkGrey,),
                                                        
                                                        Text("${"ADDRESS".tr()} :",style: ThemsConstands.headline_5_medium_16,textAlign: TextAlign.left,),
                                                        const SizedBox(height: 12,),
                                                        Text(addressController.text,style: ThemsConstands.headline_4_semibold_18,textAlign: TextAlign.left,),
                                                        const Divider(height: 18,color: Colorconstand.neutralDarkGrey,),
                                                        
                                                        Text("${"ROLE".tr()} :",style: ThemsConstands.headline_5_medium_16,textAlign: TextAlign.left,),
                                                        const SizedBox(height: 12,),
                                                        Text(roleController.text,style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.left,),
                                                      ],
                                                    ),
                                                  )
                                                  :Column(
                                                    children: [

                                                      cheangePass == true?Container():Container(
                                                        child: Column(
                                                          children: [
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.text,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: lastnameController,
                                                              onChanged: (String? value) {},
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'LAST_NAME'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.text,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: firstnameController,
                                                              onChanged: (String? value) {},
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'FIRST_NAME'.tr(),
                                                            ),
                                                            
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.text,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: lastnameEnController,
                                                              onChanged: (String? value) {},
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'Last name'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.text,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: firstnameEnController,
                                                              onChanged: (String? value) {},
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'Frist name'.tr(),
                                                            ),
                                                            Row(
                                                              children: [
                                                                Container(
                                                                  margin:const EdgeInsets.only(top: 12,left: 12),
                                                                  alignment: Alignment.centerLeft,
                                                                  child: Text('GENDER'.tr(),style: ThemsConstands.subtitle1_regular_16.copyWith( color: Colorconstand.neutralDarkGrey),),
                                                                ),
                                                                Container(
                                                                  margin:const EdgeInsets.only(top: 12,left: 12),
                                                                  alignment: Alignment.centerLeft,
                                                                  child: Text(errorSelectGender,style: ThemsConstands.subtitle1_regular_16.copyWith( color: Colorconstand.alertsDecline),),
                                                                ),
                                                              ],
                                                            ),
                                                            Row(
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Radio(
                                                                      value:1,
                                                                      groupValue: genderNumber,
                                                                      onChanged: (int? value) {
                                                                        setState(() {
                                                                          errorSelectGender = "";
                                                                          genderNumber = value!;
                                                                        });
                                                                      },
                                                                    ),
                                                                    Text('MALE'.tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                                                  ],
                                                                ),
                                                                const SizedBox(width: 20,),
                                                                Row(
                                                                  children: [
                                                                    Radio(
                                                                      value:2,
                                                                      groupValue: genderNumber,
                                                                      onChanged: (int? value) {
                                                                        setState(() {
                                                                          errorSelectGender = "";
                                                                        genderNumber = value!;
                                                                        });
                                                                      },
                                                                    ),
                                                                    Text('FEMALE'.tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.datetime,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: dobController,
                                                              onChanged: (String? value) {
                                                              },
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'DOB'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.phone,
                                                              enable: false,
                                                              checkFourcus: false,
                                                              controller: phoneController,
                                                              onChanged: (String? value) {},
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'PHONENUMBER'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.emailAddress,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: emailController,
                                                              onChanged: (String? value) {},
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'EMAIL'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.emailAddress,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: addressController,
                                                              onChanged: (String? value) { },
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'ADDRESS'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.emailAddress,
                                                              enable: false,
                                                              checkFourcus: false,
                                                              controller: roleController,
                                                              onChanged: (String? value) { },
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'ROLE'.tr(),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      
                                                      cheangePass == false?Container():Container(
                                                        margin:const EdgeInsets.only(top: 18),
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              alignment: Alignment.centerLeft,
                                                              child: Text('CHANGE_PASSWORD'.tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.left,)),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.emailAddress,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: currentPasswordController,
                                                              onChanged: (String? value) { },
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'CURRENT_PASSWORD'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.emailAddress,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: newPasswordController,
                                                              onChanged: (String? value) { },
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'NEW_PASSWORD'.tr(),
                                                            ),
                                                            TextFormFiledCustom(
                                                              keyboardType: TextInputType.emailAddress,
                                                              enable: true,
                                                              checkFourcus: false,
                                                              controller: comfirmPasswordController,
                                                              onChanged: (String? value) { },
                                                              onFocusChange: (bool? focus) {},
                                                              title: 'COMFIRM_PASSWORD'.tr(),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  widget.whereEdit=="profileUser"?Container():Column(
                                                        children: [
                                                          Container(
                                                            alignment: Alignment.centerLeft,
                                                            margin:const EdgeInsets.only(top: 16,bottom: 5),
                                                            child: Text("MOTHER_INFO".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.darkTextsRegular),textAlign: TextAlign.left,),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: mather_lastnameController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'MATHER_LAST_NAME'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: mather_firstnameController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'MATHER_FIRST_NAME'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: mather_lastnameEnController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'Mother last name'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller:mather_firstnameEnController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'Mother first name'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.phone,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: mather_phoneController,
                                                            onChanged: (String? value) { },
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'MATHER_PHONE'.tr(),
                                                          ),
                                                          
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: mather_addressController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'ADDRESS'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.emailAddress,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: mather_emailController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'EMAIL'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: mather_job_Controller,
                                                            onChanged: (String? value) { },
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'MATHER_JOB'.tr(),
                                                          ),
                                                          Container(
                                                            alignment: Alignment.centerLeft,
                                                            margin:const EdgeInsets.only(top: 16,bottom: 5),
                                                            child: Text("FATHER_INFO".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.darkTextsRegular),textAlign: TextAlign.left,),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller:father_lastnameController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'FATHER_LAST_NAME'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller:father_firstnameController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'FATHER_FIRST_NAME'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: father_lastnameEnController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'Father last name'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller:father_firstnameEnController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'Father first name'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.phone,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: father_phoneController,
                                                            onChanged: (String? value) { },
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'FATHER_PHONE'.tr(),
                                                          ),
                                                          
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.text,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: father_addressController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'ADDRESS'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.emailAddress,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: father_emailController,
                                                            onChanged: (String? value) {},
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'EMAIL'.tr(),
                                                          ),
                                                          TextFormFiledCustom(
                                                            keyboardType: TextInputType.emailAddress,
                                                            enable: true,
                                                            checkFourcus: false,
                                                            controller: father_job_Controller,
                                                            onChanged: (String? value) { },
                                                            onFocusChange: (bool? focus) {},
                                                            title: 'FATHER_JOB'.tr(),
                                                          ),
                                                        ],
                                                      )
                                                    
                                                ],
                                              ),
                                            ),
                                             connection == false || isEdit == false?Container() : Container(
                                                margin: const EdgeInsets.only(
                                                    top: 20, bottom: 20),
                                                child: MaterialButton(
                                                  padding: const EdgeInsets.symmetric(
                                                      horizontal: 50),
                                                  onPressed: connection == false
                                                      ? () {showMessage(color: Colorconstand.neutralDarkGrey,context: context,
                                                              title:"NOCONNECTIVITY".tr(),hight: 25.0,width: 250.0);
                                                        }
                                                      : widget.whereEdit == "profileUser"?() async{

                                                        if(cheangePass == true){
                                                          //==========   Change Password =============
                                                            setState(() {
                                                              loadingsumit = true;
                                                            });
                                                            var changePass =  AuthApi();
                                                            changePass.changePasswordApi(currentPass: currentPasswordController.text, newPass: newPasswordController.text, comfirmPass: comfirmPasswordController.text).then((onValue){
                                                              if(onValue == true){
                                                                setState(() {
                                                                  cheangePass = false;
                                                                  showSuccessDialog(
                                                                    title: "UPDATEPROFILE".tr(),
                                                                    discription: "",
                                                                    context: context,
                                                                    callback: () {
                                                                      setState(() {
                                                                        Navigator.of(context).pop();
                                                                        loadingsumit = false;
                                                                        isEdit = false;
                                                                      });
                                                                    },
                                                                  );
                                                                });
                                                              }
                                                              else{
                                                                showErrorDialog(
                                                                  (){
                                                                    Navigator.of(context).pop();
                                                                  },
                                                                  context,
                                                                  title: "FAIL".tr(),
                                                                  description: "".tr(),
                                                                );
                                                                setState(() {
                                                                  loadingsumit = false;
                                                                });
                                                              }
                                                            });
                                                        //==========  End Change Password =============
                                                        }
                                                        else{
                                                          if (_image != null) {
                                                              BlocProvider.of<UpdateProfileBloc>(context).add(
                                                                UpdateUserProfileWithImageEvent(
                                                                    fname: firstnameController.text.toString(),
                                                                    lname: lastnameController.text.toString(),
                                                                    fnameEn: firstnameEnController.text.toString(),
                                                                    lnameEn: lastnameEnController.text.toString(),
                                                                    gender: genderNumber.toString(),
                                                                    phone:phoneController.text,
                                                                    email: emailController.text,
                                                                    address: addressController.text,
                                                                    dob: dobController.text,
                                                                    image: _image!),
                                                              );
                                                            } else {
                                                              BlocProvider.of<UpdateProfileBloc>(context)
                                                                  .add(UpdateUserProfileEvent(
                                                                  fname: firstnameController.text.toString(),
                                                                  lname: lastnameController.text.toString(),
                                                                  fnameEn: firstnameEnController.text.toString(),
                                                                  lnameEn:lastnameEnController.text.toString(),
                                                                  gender: genderNumber.toString(),
                                                                  phone:phoneController.text,
                                                                  email: emailController.text,
                                                                  address: addressController.text,
                                                                  dob: dobController.text,
                                                                ),
                                                              );
                                                            }
                                                        }
                                                      }
                                                      :() {
                                                        BlocProvider.of<UpdateStudentProfileBloc>(context).add(
                                                                UpdateStudentInfoEvent(
                                                                    fname: firstnameController.text.toString(),
                                                                    lname: lastnameController.text.toString(),
                                                                    fnameEn: firstnameEnController.text.toString(),
                                                                    lnameEn: lastnameEnController.text.toString(),
                                                                    gender: genderNumber,
                                                                    phone:phoneController.text,
                                                                    email: emailController.text,
                                                                    address: addressController.text,
                                                                    dob: dobController.text,
                                                                    image: _image, 
                                                                    fatheraddress: father_addressController.text, 
                                                                    fatheremail: father_emailController.text, 
                                                                    fatherfname: father_firstnameController.text, 
                                                                    fatherfnameEn: father_firstnameEnController.text, 
                                                                    fatherjob: father_job_Controller.text, 
                                                                    fatherlname: father_lastnameController.text, 
                                                                    fatherlnameEn: father_lastnameEnController.text, 
                                                                    fatherphone: father_phoneController.text,
                                                                    motheremail: mather_emailController.text, 
                                                                    motherfname: mather_firstnameController.text, 
                                                                    motherfnameEn: mather_firstnameEnController.text, 
                                                                    motherjob: mather_job_Controller.text, 
                                                                    motherlname: mather_lastnameController.text, 
                                                                    motherlnameEn: mather_lastnameEnController.text, 
                                                                    motherphone: mather_phoneController.text, 
                                                                    motheraddress: mather_addressController.text,
                                                                    classId: widget.dataStudentInfo!.classId.toString(),
                                                                    studentId: widget.dataStudentInfo!.studentId.toString()
                                                                ),
                                                              );
                                                        },
                                                  minWidth: MediaQuery.of(context).size.width - 95,
                                                  height: 50,
                                                  shape: const RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.all(
                                                          Radius.circular(12))),
                                                  color: Colorconstand.primaryColor,
                                                  child: Text(
                                                    "SUBMIT".tr(),
                                                    style: ThemsConstands
                                                        .button_semibold_16
                                                        .copyWith(
                                                      color: Colorconstand.neutralWhite,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )  
                              )
                            ],
                          ),
                        ),
                      ),
                      loadingsumit == false
                          ? Container()
                          : Positioned(
                              top: 0,
                              left: 0,
                              right: 0,
                              bottom: 0,
                              child: Container(
                                color: Colorconstand.subject13.withOpacity(0.6),
                                child: const Center(
                                  child: CircularProgressIndicator(
                                    color: Colorconstand.neutralWhite,
                                  ),
                                ),
                              )),
                      AnimatedPositioned(
                        bottom: connection == true ? -150 : 0,
                        left: 0,
                        right: 0,
                        duration: const Duration(milliseconds: 500),
                        child: const NoConnectWidget(),
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  void chooseImage() {
    setState(
      () {
        showModalBottomSheet(
          backgroundColor: Colors.transparent,
          context: context,
          builder: (BuildContext bc) {
            return ShowPiker(
              onPressedCamera: () {
                _imgFromCamera();
                Navigator.of(context).pop();
              },
              onPressedGalary: () {
                _imgFromGallery();
                Navigator.of(context).pop();
              },
            );
          },
        );
      },
    );
  }

  void _imgFromCamera() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
    });
  }

  void _imgFromGallery() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
      print("path-path " + file!.path.toString());
    });
  }
}
