import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/profile_model/update_user_model.dart';
import 'package:camis_teacher_application/app/service/api/profile_api/update_profile_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'update_profile_event.dart';
part 'update_profile_state.dart';

class UpdateProfileBloc extends Bloc<UpdateProfileEvent, UpdateProfileState> {
  UpdateProfileApi updateProfileApi;
  UpdateProfileBloc({required this.updateProfileApi})
      : super(UpdateProfileInitial()) {
    
    // bloc event update with image
    
    on<UpdateUserProfileWithImageEvent>((event, emit) async {
      emit(UpdateProfileLoadingState());
      try {
        final updateProfile = await updateProfileApi.updateProfileWithImageApi(
          firstname: event.fname,
          lastname: event.lname,
          gender: event.gender,
          phone: event.phone,
          image: event.image,
          email: event.email,
          dob: event.dob,
          address: event.address,
          lastnameEn: event.lnameEn,
          firstnameEn: event.fnameEn,
        );
        emit(UpdateProfileLoadedState(updateModel: updateProfile));
      } catch (e) {
        print(e.toString());
        emit(UpdateProfileErrorState(error: e.toString()));
      }
    });

    // bloc event update with no image
    
    on<UpdateUserProfileEvent>((event, emit) async {
      emit(UpdateProfileLoadingState());
      try {
        final updateProfile = await updateProfileApi.updateUserProfileApi(
          firstname: event.fname,
          lastname: event.lname,
          gender: event.gender,
          phone: event.phone,
          email: event.email,
          dob: event.dob,
          address: event.address,
          lastnameEn: event.lnameEn,
          firstnameEn: event.fnameEn,
        );
        emit(UpdateProfileLoadedState(updateModel: updateProfile));
      } catch (e) {
        print(e.toString());
        emit(UpdateProfileErrorState(error: e.toString()));
      }
    });

  }
}
