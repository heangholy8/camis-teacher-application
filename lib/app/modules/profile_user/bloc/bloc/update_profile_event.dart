part of 'update_profile_bloc.dart';

abstract class UpdateProfileEvent extends Equatable {
  const UpdateProfileEvent();

  @override
  List<Object> get props => [];
}

class UpdateUserProfileEvent extends UpdateProfileEvent {
  final String lname;
  final String fname;
  final String gender;
  final String phone;
  final String lnameEn;
  final String fnameEn;
  final String dob;
  final String email;
  final String address;

  const UpdateUserProfileEvent({
    required this.fname,
    required this.gender,
    required this.lname,
    required this.phone,
    required this.lnameEn, required this.fnameEn,required this.dob,required this.email,required this.address, 
  });
}

class UpdateUserProfileWithImageEvent extends UpdateProfileEvent {
  final String lname;
  final String fname;
  final String gender;
  final String phone;
  final String lnameEn;
  final String fnameEn;
  final String dob;
  final String email;
  final String address;
  final File? image;

  const UpdateUserProfileWithImageEvent(
      {required this.fname,
      required this.gender,
      required this.lname,
      required this.phone,required this.lnameEn, required this.fnameEn,required this.dob,required this.email,required this.address,
      this.image});
}
