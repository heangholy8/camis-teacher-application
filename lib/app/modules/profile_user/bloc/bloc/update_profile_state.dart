part of 'update_profile_bloc.dart';

abstract class UpdateProfileState extends Equatable {
  const UpdateProfileState();

  @override
  List<Object> get props => [];
}

class UpdateProfileInitial extends UpdateProfileState {}

class UpdateProfileLoadingState extends UpdateProfileState {}

class UpdateProfileLoadedState extends UpdateProfileState {
  UpdateUserModel updateModel;

  UpdateProfileLoadedState({required this.updateModel});

  @override
  List<Object> get props => [updateModel];
}

class UpdateProfileErrorState extends UpdateProfileState {
  final String error;

  UpdateProfileErrorState({required this.error});

  @override
  List<Object> get props => [error];
}
