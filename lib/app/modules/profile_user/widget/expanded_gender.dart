import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/widget/expansion/expand_section.dart';
import 'package:flutter/material.dart';

class GenderExpand extends StatefulWidget {
  Widget mainWidget;
  bool isExpand;
  Widget widget;
  bool checkfocusgender;
  GenderExpand({
    Key? key,
    required this.mainWidget,
    this.checkfocusgender = false,
    required this.widget,
    required this.isExpand,
  }) : super(key: key);

  @override
  State<GenderExpand> createState() => _GenderExpandState();
}

class _GenderExpandState extends State<GenderExpand>
    with TickerProviderStateMixin {
  late Animation _arrowAnimation;
  late AnimationController _arrowAnimationController;
  final GlobalKey expansionTileKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    _arrowAnimation =
        Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController);
  }

  void _scrollToSelectedContent({required GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(const Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: const Duration(milliseconds: 200));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: expansionTileKey,
      margin: const EdgeInsets.only(top: 16),
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colorconstand.lightGohan,
        borderRadius: BorderRadius.circular(12),
        border: Border.all(
            color: widget.checkfocusgender == false
                ? Colorconstand.neutralGrey
                : Colorconstand.primaryColor,
            width: widget.checkfocusgender == false ? 1 : 2),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MaterialButton(
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              padding: const EdgeInsets.all(0),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16.0),
                ),
              ),
              onPressed: () {
                setState(() {
                  widget.isExpand = !widget.isExpand;
                  _arrowAnimationController.isCompleted
                      ? _arrowAnimationController.reverse()
                      : _arrowAnimationController.forward();
                  _scrollToSelectedContent(expansionTileKey: expansionTileKey);
                });
              },
              child: widget.mainWidget),
          AnimatedCrossFade(
            firstChild: Container(height: 1, color: Colorconstand.neutralGrey),
            secondChild: const SizedBox(),
            crossFadeState: widget.isExpand
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            duration: const Duration(milliseconds: 400),
          ),
          ExpandedSection(expand: widget.isExpand, child: widget.widget),
        ],
      ),
    );
  }
}
