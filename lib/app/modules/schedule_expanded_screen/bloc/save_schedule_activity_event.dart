part of 'save_schedule_activity_bloc.dart';

abstract class SaveScheduleActivityEvent extends Equatable {
  const SaveScheduleActivityEvent();

  @override
  List<Object> get props => [];
}

class PostScheduleActivityEvent extends SaveScheduleActivityEvent{
  final String activityType;
  final String description;
  final String expiredDate;
  final List<File> attachment;

  const PostScheduleActivityEvent({required this.activityType,required this.description,required this.expiredDate,required this.attachment});

  

}