part of 'save_schedule_activity_bloc.dart';

abstract class SaveScheduleActivityState extends Equatable {
  const SaveScheduleActivityState();
  
  @override
  List<Object> get props => [];
}

class SaveScheduleActivityInitial extends SaveScheduleActivityState {}


class SaveScheduleActLoadingState extends SaveScheduleActivityState{}

class SaveScheduleActSuccessState extends SaveScheduleActivityState{
  final bool isSuccess;
  const SaveScheduleActSuccessState({required this.isSuccess});
}

class SaveScheduleActErrorState extends SaveScheduleActivityState{
  final String error;
  const SaveScheduleActErrorState({required this.error});
}
