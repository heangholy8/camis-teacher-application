import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../check_attendance_screen/cubit_attach/cubit_attachment.dart';

class AssignAttachWidget extends StatefulWidget {
  const AssignAttachWidget({super.key});

  @override
  State<AssignAttachWidget> createState() => _AssignAttachWidgetState();
}

class _AssignAttachWidgetState extends State<AssignAttachWidget> {
  @override
  Widget build(BuildContext context) {
    if(BlocProvider.of<AttachCubit>(context).title.isNotEmpty && BlocProvider.of<AttachCubit>(context).filesMedia.isEmpty ){
      return Container(
        padding: const EdgeInsets.all(22), 
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey.withOpacity(.7)),
          borderRadius: BorderRadius.circular(14),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(BlocProvider.of<AttachCubit>(context).title,style: ThemsConstands.headline_4_medium_18.copyWith(fontWeight: FontWeight.w700),),
                const  SizedBox(height: 8,),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 240,
                  child: Text(BlocProvider.of<AttachCubit>(context).description,style: ThemsConstands.headline6_medium_14, maxLines: 1,overflow: TextOverflow.ellipsis,)),
              ],
            ),    
            SvgPicture.asset(ImageAssets.edit,width: 28)
          ],
        ),
      );
    }else{
      final mediaPath = BlocProvider.of<AttachCubit>(context).filesMedia[0];
      final extension = mediaPath.path.toLowerCase();
      return Container(
        padding:  const EdgeInsets.only(left:24,right:12, bottom: 8,top: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          border: Border.all(color:const Color(0xFF000000).withOpacity(.2))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(BlocProvider.of<AttachCubit>(context).title,style: ThemsConstands.headline_4_medium_18.copyWith(fontWeight: FontWeight.w700),),
                const  SizedBox(height: 12),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 240,
                  child: Text(BlocProvider.of<AttachCubit>(context).description,style: ThemsConstands.headline6_medium_14, maxLines: 1,overflow: TextOverflow.ellipsis,)),
              ],
            ),
            extension.contains('.jpg') || extension.contains('.png') || extension.contains('.jpeg')?
              Container(
                width: 84,
                height: 84,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                    image: DecorationImage(image: FileImage(File(mediaPath.path)),
                    fit: BoxFit.cover,
                  ),
                ),
              ):
              extension.contains('.mov') || extension.contains('.mp4') ?
              Container(
                padding: const EdgeInsets.all(32), 
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(14),
                ),
                child: SvgPicture.asset(ImageAssets.play_icon),
              ): 
              extension.contains('.pdf') ?
              Container(
                padding: const EdgeInsets.all(34),
                decoration: BoxDecoration(
                  color: const Color(0xFFE5EBF4),
                  borderRadius: BorderRadius.circular(14),
                ),
                child: SvgPicture.asset( ImageAssets.filldoc_icon ),
              ):  
              Container(
                padding: const EdgeInsets.all(34),
                decoration: BoxDecoration(
                  color: const Color(0xFFE5EBF4),
                  borderRadius: BorderRadius.circular(14),
                ),
                child: Column(
                  children: [
                  Text(BlocProvider.of<AttachCubit>(context).title),
                  const SizedBox(height: 8,),
                  Text(BlocProvider.of<AttachCubit>(context).description),
                  ],
                )
              ),
          ],
        ),   
      );
    }
  }
}