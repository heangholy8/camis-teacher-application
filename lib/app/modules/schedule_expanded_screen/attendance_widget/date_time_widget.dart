// ignore_for_file: must_be_immutable

import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';


class DateTimeWidget extends StatelessWidget {
  String date;
  Function()? onClear;


  void Function()? onPressed;
  DateTimeWidget({super.key,required this.date,required this.onPressed,this.onClear});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          alignment: Alignment.center,
          child: MaterialButton(
            onPressed: onPressed,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
              side: const BorderSide(color: Colorconstand.neutralGrey),
            ),
            padding: const EdgeInsets.all(18.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(ImageAssets.CALENDARFILL_ICON,height: 24, color: Colors.black),
                const SizedBox(width: 12.0),
                Text(
                  date.isEmpty?"EMPTYDATE".tr():date,
                  style: ThemsConstands.headline_4_medium_18.copyWith(color: date.isEmpty?Colorconstand.neutralDarkGrey:Colors.black),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          bottom: 0,
          child:date.isEmpty?Container():InkWell(
            onTap:onClear,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SvgPicture.asset(ImageAssets.remove_circle),
              ),
            ),
          ),
        )
      ],
    );
  
  }
}