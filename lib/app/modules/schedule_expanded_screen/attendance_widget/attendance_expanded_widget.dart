import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AttendanceExpandedWidget extends StatefulWidget {
  int? isCheckattandace;
  final String nameClassMonitor;
  final String presence;
  final String lateStudent;
  final String absent;
  final int? permissionRequest;
  final String permission;
  VoidCallback? onPressed;
  VoidCallback? onPressedViewRequestPermission;
  String? countPermission;
  AttendanceExpandedWidget(
      {super.key,
      this.isCheckattandace,
      this.onPressed,
      this.presence = "-",
      this.absent = "-",
      this.lateStudent = "-",
      this.permission = "-",
      this.countPermission,
      this.permissionRequest,
      this.onPressedViewRequestPermission,
      this.nameClassMonitor = ""});

  @override
  State<AttendanceExpandedWidget> createState() =>
      _AttendanceExpandedWidgetState();
}

class _AttendanceExpandedWidgetState extends State<AttendanceExpandedWidget> {
  final GlobalKey expansionTileKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 600),
      curve: Curves.easeInOut,
      // alignment: Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.only(
            left: 20.8,
            top: 12.5,
            bottom: widget.isCheckattandace == 1 ? 22.25 : 12.5,
            right: 20.8),
        decoration: BoxDecoration(
          color: Colorconstand.lightBackgroundsWhite,
          borderRadius: BorderRadius.circular(8.2),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 5),
              blurRadius: 15,
              spreadRadius: 0,
              color: Colorconstand.primaryColor.withOpacity(0.15),
            ),
          ],
        ),
        child: Column(
          children: [
            GestureDetector(
              onTap: widget.onPressed,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            SvgPicture.asset(
                              ImageAssets.PEOPLEATTANDANCE,
                              height: 24,
                              color: Colorconstand.primaryColor,
                            ),
                            const SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "STUDENT_ATTENDANCE".tr(),
                              style: ThemsConstands.button_semibold_16
                                  .copyWith(color: Colorconstand.lightBulma),
                            ),
                                    
                          ],
                        ),
                      ),
                      widget.isCheckattandace == 0? 
                        MaterialButton(
                          //minWidth: 140,
                            onPressed: widget.onPressed,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          padding: const EdgeInsets.symmetric(vertical: 6,horizontal: 2),
                            color: Colorconstand.primaryColor,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: [
                                  Text(
                                    "PRESENCE".tr(),
                                    overflow: TextOverflow.ellipsis,
                                    style: ThemsConstands.button_semibold_16
                                        .copyWith(
                                            color: Colorconstand.neutralWhite),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  SvgPicture.asset(
                                    ImageAssets.ARROWRIGHT_ICON,
                                    color: Colorconstand.neutralWhite,
                                    height: 18,
                                  )
                                ],
                              ),
                            ),
                          )
                        : IconButton(
                            onPressed: widget.onPressed,
                            icon: const Icon(
                              Icons.arrow_forward_ios_rounded,
                              color: Colorconstand.primaryColor,
                              size: 24,
                            )
                          ),
                    ],
                  ),
              
                  widget.isCheckattandace == 1
                    ? ListView(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        padding: const EdgeInsets.all(0),
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 34.0, top: 8, bottom: 24.0),
                            child: Text(
                              "${"QUOTED_BY".tr()} ${widget.nameClassMonitor}",
                              style: ThemsConstands.headline_6_regular_14_20height
                                  .copyWith(color: Colorconstand.neutralDarkGrey),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 35.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                ScheduleContentItemWidget(
                                  titleItem: "PRESENCES".tr(),
                                  numberItemAccess: widget.presence,
                                  color: Colorconstand.alertsPositive,
                                ),
                                const Spacer(),
                                ScheduleContentItemWidget(
                                  titleItem: "LATE1".tr(),
                                  numberItemAccess: widget.lateStudent,
                                  color: Colorconstand.alertsAwaitingText,
                                ),
                                const Spacer(),
                                ScheduleContentItemWidget(
                                  titleItem: "PERMISSION".tr(),
                                  numberItemAccess: widget.permission,
                                  color: Colorconstand.subject7,
                                ),
                                const Spacer(),
                                ScheduleContentItemWidget(
                                  titleItem: "ABSENT".tr(),
                                  numberItemAccess: widget.absent,
                                  color: Colorconstand.alertsNotifications,
                                ),
                              ],
                            ),
                          )
                        ],
                      )
                    : const SizedBox(),
                ],
              ),
            ),
            widget.permissionRequest! > 0?GestureDetector(
                    onTap:widget.onPressedViewRequestPermission,
                    child: Column(
                      children: [
                        const SizedBox(height: 10,),
                        const Divider(),
                        const SizedBox(height: 10,),
                        Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  SvgPicture.asset(
                                    ImageAssets.taguser,
                                    height: 24,
                                    color: Colorconstand.primaryColor,
                                  ),
                                  const SizedBox(
                                    width: 12.0,
                                  ),
                                  Text(
                                    "PERMISSIONLETTER".tr(),
                                    style: ThemsConstands.button_semibold_16
                                        .copyWith(color: Colorconstand.lightBulma),
                                  ),
                                ],
                              ),
                            ),
                            MaterialButton(
                              minWidth: 140,
                              onPressed: widget.onPressedViewRequestPermission,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              padding: const EdgeInsets.all(12),
                              color: Colorconstand.primaryColor,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left:18.0),
                                      child: Text(
                                        "CHECKK".tr(),
                                        overflow: TextOverflow.ellipsis,
                                        style: ThemsConstands.button_semibold_16
                                            .copyWith(
                                                color: Colorconstand.neutralWhite),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      width: 24,
                                      height: 24,
                                      // padding: const EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        color: Colorconstand.mainColorForecolor,
                                        borderRadius: BorderRadius.circular(5)
                                      ),
                                      child: Text(widget.permissionRequest! == 0? "" : widget.permissionRequest.toString(),style: ThemsConstands.button_semibold_16.copyWith(color: Colors.white),),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ):Container(height: 0,)
                
          ],
        ),
      ),
    );
  }
}

class ScheduleContentItemWidget extends StatelessWidget {
  final String titleItem;
  final String numberItemAccess;
  Color color;
  ScheduleContentItemWidget(
      {Key? key,
      required this.titleItem,
      required this.numberItemAccess,
      required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          width: 45,
          height: 45,
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(12.0)),
          child: Text(
            numberItemAccess,
            style: ThemsConstands.headline_4_medium_18
                .copyWith(color: Colorconstand.neutralWhite),
          ),
        ),
        const SizedBox(height: 8,),
        Text(
          titleItem,
          style: ThemsConstands.headline_6_semibold_14.copyWith(color: color),
        )
      ],
    );
  }
}
