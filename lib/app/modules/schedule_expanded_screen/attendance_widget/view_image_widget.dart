import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';

import '../../../service/api/schedule_api/set_exam_api.dart';

class ViewImageAttach extends StatefulWidget {
  final int index;
  final File fileImage;
  final bool isFile;
  final String imageNework;
  
  const ViewImageAttach({super.key,required this.index,required this.fileImage,this.isFile = true,this.imageNework = ""});

  @override
  State<ViewImageAttach> createState() => _ViewImageAttachState();
}

class _ViewImageAttachState extends State<ViewImageAttach> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0.0,
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);         
          },
          icon:  SvgPicture.asset(ImageAssets.chevron_left,width: 28,)),
        // title: Text("${listFileLesson.length.toString()},${widget.index}"),
        actions:[
          IconButton(onPressed: (){
            Navigator.pop(context,true);  
          }, icon: SvgPicture.asset(ImageAssets.trash_icon,color: Colors.black),)
        ],
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.topCenter,
          child: InteractiveViewer(
            child:widget.isFile == true?Image.file(widget.fileImage):Image.network(widget.imageNework),
          ),
        ),
      )
    );
  }
}