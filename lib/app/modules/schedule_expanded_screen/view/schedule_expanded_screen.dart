// ignore_for_file: unnecessary_null_comparison

import 'dart:async';
import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/check_attendance_screen/cubit_attach/cubit_attachment.dart';
import 'package:camis_teacher_application/app/modules/schedule_expanded_screen/attendance_widget/attendance_expanded_widget.dart';
import 'package:camis_teacher_application/app/modules/schedule_expanded_screen/attendance_widget/view_image_widget.dart';
import 'package:camis_teacher_application/app/modules/tracking_location_teacher/bloc/tracking_location_bloc.dart';
import 'package:camis_teacher_application/app/service/api/tracking_teacher_api/tracking_teacher_api.dart';
import 'package:camis_teacher_application/widget/textFormfiled_custom/textformfiled_custom.dart';
import 'package:camis_teacher_application/widget/video_play_widget/view_video_youtube.dart';
import 'package:clipboard/clipboard.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../widget/button_widget/button_widget.dart';
import '../../../../widget/expansion/expand_section.dart';
import '../../../../widget/internet_connect_widget/internet_connect_widget.dart';
import '../../../../widget/pdf_view_widget/pdf_view_screen.dart';
import '../../../../widget/video_play_widget/display_video_youtube_learing.dart';
import '../../../../widget/view_document.dart';
import '../../../models/schedule_model/schedule_model.dart';
import '../../../routes/app_route.dart';
import '../../../service/api/schedule_api/post_schedule_activity_api.dart';
import '../../../service/api/schedule_api/set_exam_api.dart';
import '../../../service/api/student_requst_permission_api/requst_permission_api.dart';
import '../../../storages/key_storage.dart';
import '../../attendance_schedule_screen/state_management/set_exam/bloc/set_exam_bloc.dart';
import '../../attendance_schedule_screen/state_management/state_attendance/bloc/get_schedule_bloc.dart';
import '../../check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../../check_attendance_screen/view/attendance_entry_screen.dart';
import '../../home_screen/bloc/daskboard_screen_bloc.dart';
import '../../tracking_location_teacher/tracking_location_teacher.dart';
import '../../view_request_permission_screen/bloc/get_list_permission_request_bloc.dart';
import '../attendance_widget/date_time_widget.dart';


class ScheduleExpandedScreen extends StatefulWidget {
// =============== Screen is check primary school =============
  final bool? isPrimary;
// =============== Screen is check primary school =============
  final Datum data;
  final String month;
  final String type;
  final bool activeMySchedule;
  final bool displayCheckAttendance;
  //=========== routFromScreen = 1? HomeScreen : routFromScreen = 2? ScheduleScreen
  final int routFromScreen;
  const ScheduleExpandedScreen({super.key,required this.displayCheckAttendance,this.isPrimary,required this.type,required this.month,required this.routFromScreen, required this.data, required this.activeMySchedule});

  @override
  State<ScheduleExpandedScreen> createState() => _ScheduleExpandedScreenState();
}

class _ScheduleExpandedScreenState extends State<ScheduleExpandedScreen> with Toast {


  final KeyStoragePref keyPref = KeyStoragePref();

  bool isSetExam = false;
  bool isSent = false;
  DateTime date = DateTime.now();

//================ Homework list =======================
bool isCheckHomework = false;
 List<Activity> homeList = [];
 List<LinkElement> linkListHomeWork= [];
 bool showHomewrokPick = false;
 String expairDate = "";
 String? expired;
 TextEditingController titleAnnouncement = TextEditingController();
 TextEditingController subtitleAnnouncement =TextEditingController();
 List<File> listMultiMediaHomeWork = [];
 List<String> listLink = [];
 TextEditingController linkYoutubeController = TextEditingController();
 bool showWidgetPassLink = false;
 bool showDelect = false;
  

//================ lesson list =======================
  bool postAtticalInKeybord = false;
  bool isCheckLesson = false;
  FocusNode atticalFocusNode = FocusNode();
  TextEditingController titleController = TextEditingController();
  List<Activity> lessList = [];
  List<File> listFileLesson = [];
   bool enabledesController = false;
   bool focusTitle = false;
  bool focusDescription = false;
  bool postArticalSuccess = false;
//================ Home Research =======================
  bool isCheckHomeResearch = false;
  TextEditingController titleHomeResearchController = TextEditingController();
  bool postHomeResearchInKeybord = false;
  FocusNode homeResearchFocusNode = FocusNode();
  bool enabledesHomeResearchController = false;
  List<Activity> homeResearchList = [];
  List<File> listFileHomeResearch = [];
  bool postHomeworkSuccess = false;
  bool focusTitleHomeResearch = false;

//================ teacher absent =======================
  bool isTeacherAbsent = false;
  bool isdisplayTeacherAbsent = false;
  bool isdisplayTeacherAbsentText = false;
  List<Activity> teacherAbsent = [];
  List<File> listTeacherAbsent = [];
  String teacherAbsentTitle = "";
  String teacherAbsentValueText = "";
  FocusNode teacherAbsentFocusNode = FocusNode();
  TextEditingController teacherAbsentController = TextEditingController();


  bool connection = true;
  StreamSubscription? sub;
  bool loadingPostSuccess = true;
  int durationMilli =600;
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    BlocProvider.of<TrackingLocationBloc>(context).add(const GetSchoolLocationEvent());
    if(widget.data.countEequestPermissions! > 0){
      RequestPermissionApi().seenRequestPermissionApi(
        scheduleId: widget.data.scheduleId.toString(),
        date: widget.data.date.toString()
      );
    }
    isSetExam = widget.data.isExam!;
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {

        } else {

        }
      });
    });
    splitTypeOfActivities(activities: widget.data.activities);
    checkIsNotEmptyType();
    checkIsNotEmptyTypeHomework();
    checkIsNotEmptyTypeTeacherAbsent();
    super.initState();
  }
  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    if(postHomeworkSuccess == true && postArticalSuccess == true){
      setState(() {
        Navigator.of(context).pop();
      });
    }
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      body: Stack(
        children: [
          Positioned(
            top: 0,left: 0,right: 0,bottom: 0,
            child: SafeArea(
              bottom: false,
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  setState(() {
                                    listMultiMediaHomeWork = [];
                                    listLink = [];
                                    subtitleAnnouncement.text = "";
                                    titleAnnouncement.text = "";
                                  });
                                  BlocProvider.of<AttachCubit>(context).removeAllFile();
                                  // ============Add Event Get schedule again ============= \\
                                  // ============Add Event Get schedule again ============= \\
                                },
                                icon: SvgPicture.asset(
                                  ImageAssets.CLOSE_ICON,
                                  height: 28,width: 28,
                                  color: Colorconstand.lightBulma,
                                ),
                              ),
                            ),
                              // isSetExam == true? Container() : 
                            AnimatedOpacity(
                              duration: Duration(microseconds: durationMilli),
                              opacity:1,
                              child: Container(
                              alignment: Alignment.centerLeft,
                              child: IconButton(
                                onPressed: null,
                                  icon: SvgPicture.asset(
                                    ImageAssets.INFO_ICON,
                                    height: 28,width: 28,
                                    color:Colorconstand.primaryColor,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        BlocBuilder<TrackingLocationBloc, TrackingLocationState>(
                          builder: (context, state) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 5.0, top: 10),
                                  child: Text(
                                  widget.data.isSubSubject==true?translate == "km" ? widget.data.subjectGroupTagName.toString() : widget.data.subjectGroupTagNameEn.toString()
                                  :translate == "km" ? widget.data.subjectName.toString() : widget.data.subjectNameEn.toString(),
                                    style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.primaryColor),
                                  ),
                                ),
                                widget.displayCheckAttendance==false || (TrackingLocation.shared.teacherStatus == TeacherStatus.insideZoon && widget.data.isCurrent==false) ?Container():GestureDetector(
                                  onTap:()async{
                                    final SharedPreferences pref = await SharedPreferences.getInstance();
                                    TrackingLocation.shared.determinePosition(
                                      redirectFrom: widget.routFromScreen,
                                      classId: widget.data.classId.toString(),
                                      subjectId: widget.data.subjectId.toString(),
                                      timeTableSettingID: widget.data.timeTableSettingId.toString(),
                                      rDistance: int.parse(pref.getString(keyPref.keySchoolRadius).toString()),
                                      schoolLat:  pref.getString(keyPref.keySchoolLati).toString(), 
                                      schoolLog: pref.getString(keyPref.keySchoolLogti).toString(),
                                    );
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(horizontal:14,vertical: 4),
                                    decoration: BoxDecoration(
                                      color: TrackingLocation.shared.teacherStatus == TeacherStatus.insideZoon?Colorconstand.primaryColor:TrackingLocation.shared.teacherStatus == TeacherStatus.outsideZoon?Colors.red:TrackingLocation.shared.teacherStatus == TeacherStatus.absentZoon?Colors.grey:TrackingLocation.shared.teacherStatus == TeacherStatus.ischeckeZoon?Colorconstand.primaryColor.withOpacity(.4):Colors.green,
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                    child: TrackingLocation.shared.teacherStatus == TeacherStatus.insideZoon? Row(
                                      children: [
                                        Text("CHECKATTENDANCEE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colors.white),),
                                        const SizedBox(width: 4,),
                                        SvgPicture.asset(ImageAssets.time_starter,width: 24,),
                                      ],
                                    ):TrackingLocation.shared.teacherStatus == TeacherStatus.outsideZoon?
                                      Row(
                                        children:  [
                                          Text("OUT_LOCATION".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colors.white),),
                                          const SizedBox(width: 4,),
                                          SvgPicture.asset(ImageAssets.fill_close_circle_icon,width: 24,color: Colors.white,),
                                        ],
                                      )
                                    :TrackingLocation.shared.teacherStatus == TeacherStatus.ischeckeZoon?
                                      Row(
                                        children: [
                                          SvgPicture.asset(ImageAssets.tick_circle_icon,width: 24,),
                                          const SizedBox(width: 4,),
                                          Text("PRESENTLOCATION".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),),
                                        ],
                                      ) : TrackingLocation.shared.teacherStatus == TeacherStatus.absentZoon ? Row(
                                        children:[
                                          SvgPicture.asset(ImageAssets.fill_close_circle_icon,width: 24,),
                                          const SizedBox(width: 4,),
                                          Text("ABSENTLOCATIONN".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colors.black),),
                                        ],
                                    ): Row(
                                      children: [
                                        SvgPicture.asset(ImageAssets.time_starter,width: 24,),
                                        const SizedBox(width: 4,),
                                        Text("RECORDINGATTENDANCE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colors.white),),
                                      ],
                                    ),
                                  ),
                                ),
                             
                              ],
                            );
                          },
                        ),
                      
                        Container(
                          padding: const EdgeInsets.only(left: 5.0, top: 12.0, bottom: 24),
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                ImageAssets.LOCATION,
                                height: 18,
                                color: Colorconstand.primaryColor,
                              ),
                              const SizedBox(width: 5.0),
                              Text(translate=="km"?widget.data.className.toString():widget.data.className.toString().replaceAll("ថ្នាក់ទី", "Class"), style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colorconstand.lightBulma)),
                              const Spacer(),
                              SvgPicture.asset( ImageAssets.calendar_icon, height: 18, width: 18, color: Colorconstand.primaryColor),
                              const SizedBox(width: 5.0),
                              Text("${widget.data.date.toString()} • ${widget.data.startTime}-${widget.data.endTime}",
                                style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      scrollDirection: Axis.vertical,
                      primary: true,
                      children: [
                        isTeacherAbsent == true?Container():widget.isPrimary==true?Container() :AttendanceExpandedWidget(
                          permissionRequest:  widget.data.countEequestPermissions,
                          onPressedViewRequestPermission: (){
                            BlocProvider.of<GetListPermissionRequestBloc>(context).add(GetListPermissionRequest(date: widget.data.date.toString(),scheduleId: widget.data.scheduleId.toString()));
                            Navigator.pushNamed(context, Routes.VIEWREQUESTPERMISSION);
                          },
                          onPressed: () {
                            BlocProvider.of<CheckAttendanceBloc>(context).add(GetCheckStudentAttendanceEvent(widget.data.classId.toString(), widget.data.scheduleId.toString(), widget.data.date.toString()));
                            Navigator.pushReplacement(context, MaterialPageRoute( builder: (context) => AttendanceEntryScreen( routFromScreen: widget.routFromScreen,activeMySchedule: widget.activeMySchedule)));
                          },
                          nameClassMonitor: widget.data.attendance!.attendanceTaker.toString(),
                          isCheckattandace: widget.data.attendance!.isCheckAttendance,
                          absent: widget.data.attendance!.absent.toString(),
                          presence: widget.data.attendance!.present.toString(),
                          lateStudent: widget.data.attendance!.late.toString(),
                          permission: widget.data.attendance!.permission.toString(),
                        ),
                        isSetExam == true || isdisplayTeacherAbsent==false? Container(): const SizedBox(height: 16.0),
                        isSetExam == true? Container(): isdisplayTeacherAbsent==false?Container(): AnimatedContainer(
                          duration:  Duration(milliseconds: durationMilli),
                          curve: Curves.easeInOut,
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 20,
                              top: 17.5,
                              bottom: isCheckLesson? 22.25 : 17.5,
                              right: 20,
                            ),
                            decoration: BoxDecoration(
                              color: Colorconstand.lightBackgroundsWhite,
                              borderRadius: BorderRadius.circular(8.2),
                              boxShadow: [
                                BoxShadow(
                                  offset: const Offset(0, 5),
                                  blurRadius: 15,
                                  spreadRadius: 0,
                                  color: Colorconstand.primaryColor.withOpacity(0.15),
                                ),
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    const Icon(Icons.timer_sharp,size: 26,color: Colorconstand.primaryColor,),
                                    const SizedBox(width: 12.0),
                                    Text("REASON_TEACHER_ABSENT".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                                  ]
                                ),
                                isSetExam == true? Container():ExpandedSection(
                                  expand: true,
                                  child: isdisplayTeacherAbsentText == false? 
                                  GestureDetector(
                                    onTap: (){
                                      buttonSheetSetTeacherAbsent();
                                    },
                                    child: Container(
                                      margin:const EdgeInsets.only(top: 10),
                                      height: 45,
                                      width: MediaQuery.of(context).size.width,
                                      padding:const EdgeInsets.all(1),
                                      decoration: BoxDecoration(
                                        color: Colorconstand.neutralDarkGrey,
                                        borderRadius: BorderRadius.circular(8)
                                      ),
                                      child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          color: Colorconstand.neutralWhite,
                                          borderRadius: BorderRadius.circular(8)
                                        ),
                                        child: Text(teacherAbsentController.text,style: ThemsConstands.headline_5_semibold_16,),
                                      ),
                                    ),
                                  )
                                  :Row(
                                    children: [
                                      Expanded(
                                        child: TextFormFiledCustom(
                                          onFocusChange: (t) {
                                          },
                                          onEditingComplete: () {
                                            if(teacherAbsentValueText != teacherAbsentTitle){
                                                postTeacherAbsent();
                                            }
                                          },
                                          keyboardType: TextInputType.text,
                                          enable: true,
                                          maxLines: 1,
                                          focusNode: teacherAbsentFocusNode,
                                          checkFourcus: focusTitle,
                                          controller: teacherAbsentController,
                                          onChanged: (String? value) {
                                            setState(() {
                                              teacherAbsentValueText = value!;
                                            });
                                          },
                                          title: "${"REASON".tr()}....",
                                        ),
                                      ),
                                      const SizedBox(width: 8,),
                                      Container(
                                        margin:const EdgeInsets.only(top: 10),
                                        child: GestureDetector(
                                          onTap: (){
                                            setState(() {
                                              if(teacherAbsentValueText != teacherAbsentTitle){
                                                postTeacherAbsent();
                                              }
                                            });
                                          },
                                          child: isSent == false?Text("SEND".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: teacherAbsentValueText != teacherAbsentTitle ?Colorconstand.primaryColor:Colorconstand.neutralGrey),):Container(width: 20,height: 20,child:const CircularProgressIndicator(),),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                            
                              
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 20),
                        isSetExam == true ? Container() : InkWell(
                          onTap:(){
                            setState(() {
                              showHomewrokPick = true;
                            });
                          },                    
                        child: AnimatedContainer(
                            duration: Duration(milliseconds: durationMilli),
                            curve: Curves.easeInOut,
                            alignment: Alignment.centerLeft,
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 20,
                                top: 17.5,
                                bottom: isCheckHomework ? 22.25 : 17.5,
                                right: 20,
                              ),
                              decoration: BoxDecoration(
                                color: Colorconstand.lightBackgroundsWhite,
                                borderRadius: BorderRadius.circular(8.2),
                                boxShadow: [
                                  BoxShadow(
                                    offset: const Offset(0, 5),
                                    blurRadius: 15,
                                    spreadRadius: 0,
                                    color: Colorconstand.primaryColor.withOpacity(0.15),
                                  ),
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      SvgPicture.asset(
                                        ImageAssets.HOMEWORK_ICON,
                                        height: 24,
                                        color: Colorconstand.primaryColor,
                                      ),
                                      const SizedBox(width: 12.0),
                                      Text("HOMEWORK".tr(), style:ThemsConstands.button_semibold_16.copyWith(color:Colorconstand.lightBulma)),
                                      const Spacer(),
                                      homeList.isEmpty && listMultiMediaHomeWork.isEmpty?const Icon(Icons.chevron_right):IconButton(onPressed: (){
                                        setState(() {
                                          showHomewrokPick = true;
                                        });
                                      }, icon: SvgPicture.asset(ImageAssets.edit,width: 24))
                                    ],
                                  ),
                                  ExpandedSection(
                                    expand: isCheckHomework,
                                    child: Column(
                                      children: [
                                        Container(
                                          height: 82,
                                          width: MediaQuery.of(context).size.width,
                                          padding:const EdgeInsets.all(1),
                                          margin: const EdgeInsets.only(top: 18.0, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Colorconstand.neutralGrey,
                                            borderRadius: BorderRadius.circular(12)
                                          ),
                                          child: Container(
                                            height: 80,
                                             width: MediaQuery.of(context).size.width,
                                            decoration: BoxDecoration(
                                            color: Colorconstand.neutralWhite,
                                            borderRadius: BorderRadius.circular(12)
                                          ),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                    margin:const EdgeInsets.symmetric(horizontal: 18),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          titleAnnouncement.text,
                                                          style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colors.black,),maxLines: 1,overflow: TextOverflow.ellipsis,
                                                        ),
                                                        const SizedBox(height: 8,),
                                                        Text(
                                                          subtitleAnnouncement.text,
                                                          style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),maxLines: 1,overflow: TextOverflow.ellipsis,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                homeList.isEmpty?Container(
                                                  child:listMultiMediaHomeWork.isNotEmpty?Container(
                                                  margin:const EdgeInsets.only(right: 5),
                                                    height: 70,
                                                    width: 70,
                                                  decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(12),
                                                        image: DecorationImage(
                                                          image: FileImage(File(listMultiMediaHomeWork[0].path)),
                                                          fit: BoxFit.cover,
                                                        ),
                                                      ),
                                                  ):Container())
                                                : homeList[0].file!.isNotEmpty && listMultiMediaHomeWork.isNotEmpty?
                                                Container(
                                                  margin:const EdgeInsets.only(right: 5),
                                                    height: 70,
                                                    width: 70,
                                                  decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(12),
                                                        image: DecorationImage(
                                                          image: FileImage(File(listMultiMediaHomeWork[0].path)),
                                                          fit: BoxFit.cover,
                                                        ),
                                                      ),
                                                )
                                                :homeList[0].file!.isEmpty?
                                                Container(
                                                  child: 
                                                  listMultiMediaHomeWork.isNotEmpty?
                                                    listMultiMediaHomeWork[0].path.contains(".pdf")?Container(
                                                      width: 70,
                                                      height: 70,
                                                      margin:const EdgeInsets.only(right: 5),
                                                      decoration: BoxDecoration(
                                                        border: Border.all(),
                                                        borderRadius: BorderRadius.circular(12)
                                                      ),
                                                      child: Image.asset(ImageAssets.pdf_icon,width: 60,),
                                                    )
                                                  :Container(
                                                    margin:const EdgeInsets.only(right: 5),
                                                    height: 70,
                                                    width: 70,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(12),
                                                      image: DecorationImage(
                                                        image: FileImage(File(listMultiMediaHomeWork[0].path)),
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                   ):Container()
                                                  
                                                ):
                                                homeList[0].file![0].fileType!.contains("image")?Container(
                                                  margin:const EdgeInsets.only(right: 5),
                                                  height: 70,
                                                  width: 70,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(12),
                                                      image: DecorationImage(
                                                        image: NetworkImage(homeList[0].file![0].fileShow.toString()),
                                                        fit: BoxFit.cover,
                                                      ),
                                                  ),
                                                ):Container(
                                                    width: 70,
                                                    height: 70,
                                                    margin:const EdgeInsets.only(right: 5),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(),
                                                      borderRadius: BorderRadius.circular(12)
                                                    ),
                                                    child: Image.asset(ImageAssets.pdf_icon,width: 60,),
                                                  ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(top: 18.0, bottom: 8),
                                          alignment: Alignment.center,
                                          child: Text(
                                            "HOMEWORK_DEADLINE".tr(),
                                            style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma,),
                                          ),
                                        ),
                                        const SizedBox(height: 18,),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width,
                                          child: DateTimeWidget(
                                            date: homeList.isEmpty? expairDate:homeList[0].expiredAt.toString(),
                                            onPressed: pickTimeDate,
                                            onClear: (){
                                              setState(() {
                                                homeList[0].expiredAt='';
                                                postActivitiesService(
                                                  date: widget.data.date,
                                                  scheduleID: widget.data.scheduleId.toString(), 
                                                  title: titleAnnouncement.text, 
                                                  des: subtitleAnnouncement.text,
                                                  type: "2",
                                                  expiredDate:null,
                                                  file: []
                                                );
                                              });
                                            },
                                          ),
                                        ),
                                        const SizedBox(height: 8,),
                                        const Divider(),
                                        const SizedBox(height: 8),
                                      ],
                                    ),
                                  )
                               
                                ],
                              ),
                            ),
                          ),
                        ),

                  //========================= End HomeWork ===============================

                 // ======================= HomeWork Research =====================================
                        isSetExam == true? Container(): const SizedBox(
                          height: 16.0,
                        ),
                        isSetExam == true? Container(): AnimatedContainer(
                          duration:  Duration(milliseconds: durationMilli),
                          curve: Curves.easeInOut,
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 20,
                              top: 17.5,
                              bottom: isCheckHomeResearch? 22.25 : 17.5,
                              right: 20,
                            ),
                            decoration: BoxDecoration(
                              color: Colorconstand.lightBackgroundsWhite,
                              borderRadius: BorderRadius.circular(8.2),
                              boxShadow: [
                                BoxShadow(
                                  offset: const Offset(0, 5),
                                  blurRadius: 15,
                                  spreadRadius: 0,
                                  color: Colorconstand.primaryColor.withOpacity(0.15),
                                ),
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    SvgPicture.asset( ImageAssets.search_icon, height: 24,color: Colorconstand.primaryColor),
                                    const SizedBox(width: 12.0),
                                    Text( "RESEARCH".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                                    const Spacer(),
                                    CupertinoSwitch(
                                      value: isCheckHomeResearch,
                                      activeColor: Colorconstand.primaryColor,
                                      onChanged: (value) {
                                        setState(() {
                                          isCheckHomeResearch = value;
                                          if(value == true){
                                            setState(() {
                                              value = false;
                                            });
                                          }
                                          else{
                                            setState(() {
                                              value = true;
                                              if(homeResearchList.isEmpty){
                                                SetExamApi().deleteArticleBySceduleApi(scheduleId: widget.data.scheduleId.toString(),date: widget.data.date.toString(),typeActivity: "5").then((value) {
                                                  setState(() {
                                                    listFileHomeResearch = [];
                                                    titleHomeResearchController.text = "";
                                                    if(widget.routFromScreen == 2){
                                                      callSceduleAgain();
                                                    }
                                                    if(widget.routFromScreen == 1){
                                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                    } 
                                                  });
                                                });
                                              }
                                              else{
                                                  SetExamApi().deleteArticleApi(id: homeResearchList[0].id.toString()).then((value) {
                                                  setState(() {
                                                    homeResearchList[0].file = [];
                                                    listFileHomeResearch = [];
                                                    titleHomeResearchController.text = ""; 
                                                    if(widget.routFromScreen == 2){
                                                      callSceduleAgain();
                                                    }
                                                    if(widget.routFromScreen == 1){
                                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                    } 
                                                  });
                                                });
                                                }
                                            });
                                          }
                                        });
                                      },
                                    ),
                                  ],
                                ),
                                isSetExam == true ? Container(): ExpandedSection(
                                  expand: isCheckHomeResearch,
                                  child: Column(
                                    children: [
                                      TextFormFiledCustom(
                                        focusNode: homeResearchFocusNode,
                                        onEditingComplete:() {
                                          if(titleHomeResearchController.text.trim().isNotEmpty){
                                            print("object ${titleHomeResearchController.text}, ${widget.data.date}, ${widget.data.scheduleId}, ${widget.activeMySchedule}");
                                            postActivitiesService(
                                              date: widget.data.date,
                                              scheduleID: widget.data.scheduleId.toString(), 
                                              title: titleHomeResearchController.text, 
                                              type: "5",
                                              file: []
                                            );
                                          }
                                          final currentFocus = FocusScope.of(context);
                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        onFocusChange:(f){
                                        },
                                        keyboardType: TextInputType.text,
                                        enable: true,
                                        checkFourcus: focusTitleHomeResearch,
                                        controller: titleHomeResearchController,
                                        onChanged: (String? value) {
                                          setState(() {
                                          postHomeResearchInKeybord = true;
                                          if( value != ""){
                                            enabledesHomeResearchController = true;
                                          }
                                          else{
                                            enabledesHomeResearchController = false;
                                          }
                                          });
                                        },
                                        title: "HINTANN".tr(),
                                      ),
                                    const SizedBox(height: 18,),
                                    listFileHomeResearch.isNotEmpty || homeResearchList.isNotEmpty?Container(
                                      child:Row(
                                        children: [
                                          Expanded(
                                            child: SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Container(
                                                height: 70,
                                                child: Row(
                                                  children: [
                                                    homeResearchList.isNotEmpty? Container(
                                                      height: 70,
                                                      child: ListView.builder(
                                                        shrinkWrap: true,
                                                        itemCount:homeResearchList[0].file!.length,
                                                        scrollDirection: Axis.horizontal,
                                                        physics: const BouncingScrollPhysics(),
                                                        itemBuilder: (context,fileIndex){
                                                          return Stack(
                                                            children: [
                                                              InkWell(
                                                                onTap: (){
                                                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: fileIndex,fileImage:File(""),isFile:false,imageNework: homeResearchList[0].file![fileIndex].fileShow.toString(),)))
                                                                  .then((removed){
                                                                     if(removed == true){
                                                                      setState(() {
                                                                          SetExamApi().deleteScheduleAttachApi(scheduleActivityId: homeResearchList[0].id.toString(), attachmentsId: homeResearchList[0].file![fileIndex].id.toString()).then((value) {
                                                                              if(widget.routFromScreen == 2){
                                                                                callSceduleAgain();
                                                                              }
                                                                              if(widget.routFromScreen == 1){
                                                                                BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                                              } 
                                                                          });
                                                                        });
                                                                        setState(() {
                                                                          homeResearchList[0].file!.removeAt(fileIndex);  
                                                                        }); 
                                                                      }else{
                                                                      }
                                                                  });
                                                                },
                                                                child: Container(
                                                                  height: 70,
                                                                  width: 70,
                                                                  margin: const EdgeInsets.only(right:8,bottom: 8),
                                                                  decoration: BoxDecoration(
                                                                    borderRadius: BorderRadius.circular(12),
                                                                    image: DecorationImage(
                                                                      image: NetworkImage(homeResearchList[0].file![fileIndex].fileShow.toString()),
                                                                      fit: BoxFit.cover,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      ),
                                                    ):Container(),
                                                    listFileHomeResearch.isNotEmpty? Container(
                                                      height: 70,
                                                      child: ListView.builder(
                                                        shrinkWrap: true,
                                                        itemCount:listFileHomeResearch.length,
                                                        scrollDirection: Axis.horizontal,
                                                        physics: const BouncingScrollPhysics(),
                                                        itemBuilder: (context,fileIndex){
                                                          return Stack(
                                                            children: [
                                                              InkWell(
                                                                onTap: (){
                                                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: fileIndex,fileImage: listFileHomeResearch[fileIndex],isFile:true)))
                                                                  .then((removed){
                                                                    setState(() {
                                                                      if(removed == true){
                                                                        setState(() {
                                                                          listFileHomeResearch.removeAt(fileIndex);  
                                                                        }); 
                                                                      }else{
                                                                      }
                                                                    });
                                                                  });
                                                                },
                                                                child: Container(
                                                                  width: 70,
                                                                  height: 70,
                                                                  margin: const EdgeInsets.only(right:8,bottom: 8),
                                                                  decoration: BoxDecoration(
                                                                    borderRadius: BorderRadius.circular(12),
                                                                    image: DecorationImage(
                                                                      image: FileImage(File(listFileHomeResearch[fileIndex].path)),
                                                                      fit: BoxFit.cover,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      ),
                                                    ):Container(),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          IconButton(onPressed: (){
                                            getImageFromCamera(activityType: "5" );
                                          }, icon: SvgPicture.asset(ImageAssets.camera,color: Colorconstand.primaryColor,)),
                                          IconButton(onPressed: () {
                                            getMediaFromDevice(activityType: "5" );
                                            
                                          }, icon: SvgPicture.asset(ImageAssets.gallery,color: Colorconstand.primaryColor,)),
                                        ],
                                      ),
                                    ):AnimatedOpacity(
                                        duration: Duration(milliseconds: durationMilli),
                                        opacity: isCheckHomeResearch ? 1 : 0,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            InkWell(
                                              onTap: (){
                                                getImageFromCamera(activityType: "5");
                                              },
                                              child: Container(
                                              padding: const EdgeInsets.all(8),
                                            
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(ImageAssets.camera,color: Colorconstand.primaryColor,),
                                                    const SizedBox(width: 6,),
                                                    Text("TAKE_PHOTO".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor))
                                                  ],
                                                ),
                                              ),
                                            ),
                                            const SizedBox(width: 28),
                                            InkWell(
                                              onTap: (){
                                                getMediaFromDevice(activityType: "5" );
                                              },
                                              child: Container(
                                                padding: const EdgeInsets.all(8),
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(ImageAssets.gallery,color: Colorconstand.primaryColor,),
                                                    const SizedBox(width: 6,),
                                                    Text("ATTACH_PIC".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor,),),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
          // ======================= End HomeWork Research =====================================

          // ======================= ARTICAL_QUOTES =====================================
                        isSetExam == true? Container(): const SizedBox(
                          height: 16.0,
                        ),
                        isSetExam == true? Container(): AnimatedContainer(
                          duration:  Duration(milliseconds: durationMilli),
                          curve: Curves.easeInOut,
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 20,
                              top: 17.5,
                              bottom: isCheckLesson? 22.25 : 17.5,
                              right: 20,
                            ),
                            decoration: BoxDecoration(
                              color: Colorconstand.lightBackgroundsWhite,
                              borderRadius: BorderRadius.circular(8.2),
                              boxShadow: [
                                BoxShadow(
                                  offset: const Offset(0, 5),
                                  blurRadius: 15,
                                  spreadRadius: 0,
                                  color: Colorconstand.primaryColor.withOpacity(0.15),
                                ),
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    SvgPicture.asset( ImageAssets.BOOKNOTED, height: 24,color: Colorconstand.primaryColor),
                                    const SizedBox(width: 12.0),
                                    Text( "ARTICAL_QUOTES".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.lightBulma)),
                                    const Spacer(),
                                    CupertinoSwitch(
                                      value: isCheckLesson,
                                      activeColor: Colorconstand.primaryColor,
                                      onChanged: (value) {
                                        setState(() {
                                          isCheckLesson = value;
                                          if(value == true){
                                            setState(() {
                                              value = false;
                                            });
                                          }
                                          else{
                                            setState(() {
                                              value = true;
                                              if(lessList.isEmpty){
                                                SetExamApi().deleteArticleBySceduleApi(scheduleId: widget.data.scheduleId.toString(),date: widget.data.date.toString(),typeActivity: "1").then((value) {
                                                  setState(() {
                                                    listFileLesson = [];
                                                    titleController.text = "";
                                                    if(widget.routFromScreen == 2){
                                                      callSceduleAgain();
                                                    }
                                                    if(widget.routFromScreen == 1){
                                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                    } 
                                                  });
                                                });
                                              }
                                              else{
                                                  SetExamApi().deleteArticleApi(id: lessList[0].id.toString()).then((value) {
                                                  setState(() {
                                                    lessList[0].file = [];
                                                    listFileLesson = [];
                                                    titleController.text = ""; 
                                                    if(widget.routFromScreen == 2){
                                                      callSceduleAgain();
                                                    }
                                                    if(widget.routFromScreen == 1){
                                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                    } 
                                                  });
                                                });
                                                }
                                            });
                                          }
                                        });
                                      },
                                    ),
                                  ],
                                ),
                                isSetExam == true ? Container(): ExpandedSection(
                                  expand: isCheckLesson,
                                  child: Column(
                                    children: [
                                      TextFormFiledCustom(
                                        focusNode: atticalFocusNode,
                                        onEditingComplete:() {
                                          if(titleController.text.trim().isNotEmpty){
                                            print("object ${titleController.text}, ${widget.data.date}, ${widget.data.scheduleId}, ${widget.activeMySchedule}");
                                            postActivitiesService(
                                              date: widget.data.date,
                                              scheduleID: widget.data.scheduleId.toString(), 
                                              title: titleController.text, 
                                              type: "1",
                                              file: []
                                            );
                                          }
                                          final currentFocus = FocusScope.of(context);
                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        onFocusChange:(f){
                                        },
                                        keyboardType: TextInputType.text,
                                        enable: true,
                                        checkFourcus: focusTitle,
                                        controller: titleController,
                                        onChanged: (String? value) {
                                          setState(() {
                                          postAtticalInKeybord = true;
                                          if( value != ""){
                                            enabledesController = true;
                                          }
                                          else{
                                            enabledesController = false;
                                          }
                                          });
                                        },
                                        title: "HINTANN".tr(),
                                      ),
                                    const SizedBox(height: 18,),
                                    listFileLesson.isNotEmpty || lessList.isNotEmpty?Container(
                                      child:Row(
                                        children: [
                                          Expanded(
                                            child: SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Container(
                                                height: 70,
                                                child: Row(
                                                  children: [
                                                    lessList.isNotEmpty? Container(
                                                      height: 70,
                                                      child: ListView.builder(
                                                        shrinkWrap: true,
                                                        itemCount:lessList[0].file!.length,
                                                        scrollDirection: Axis.horizontal,
                                                        physics: const BouncingScrollPhysics(),
                                                        itemBuilder: (context,fileIndex){
                                                          return Stack(
                                                            children: [
                                                              InkWell(
                                                                onTap: (){
                                                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: fileIndex,fileImage:File(""),isFile:false,imageNework: lessList[0].file![fileIndex].fileShow.toString(),)))
                                                                  .then((removed){
                                                                     if(removed == true){
                                                                      setState(() {
                                                                          SetExamApi().deleteScheduleAttachApi(scheduleActivityId: lessList[0].id.toString(), attachmentsId: lessList[0].file![fileIndex].id.toString()).then((value) {
                                                                              if(widget.routFromScreen == 2){
                                                                                callSceduleAgain();
                                                                              }
                                                                              if(widget.routFromScreen == 1){
                                                                                BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                                              } 
                                                                          });
                                                                        });
                                                                        setState(() {
                                                                          lessList[0].file!.removeAt(fileIndex);  
                                                                        }); 
                                                                      }else{
                                                                      }
                                                                  });
                                                                },
                                                                child: Container(
                                                                  height: 70,
                                                                  width: 70,
                                                                  margin: const EdgeInsets.only(right:8,bottom: 8),
                                                                  decoration: BoxDecoration(
                                                                    borderRadius: BorderRadius.circular(12),
                                                                    image: DecorationImage(
                                                                      image: NetworkImage(lessList[0].file![fileIndex].fileShow.toString()),
                                                                      fit: BoxFit.cover,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      ),
                                                    ):Container(),
                                                    listFileLesson.isNotEmpty? Container(
                                                      height: 70,
                                                      child: ListView.builder(
                                                        shrinkWrap: true,
                                                        itemCount:listFileLesson.length,
                                                        scrollDirection: Axis.horizontal,
                                                        physics: const BouncingScrollPhysics(),
                                                        itemBuilder: (context,fileIndex){
                                                          return Stack(
                                                            children: [
                                                              InkWell(
                                                                onTap: (){
                                                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: fileIndex,fileImage: listFileLesson[fileIndex],isFile:true)))
                                                                  .then((removed){
                                                                    setState(() {
                                                                      if(removed == true){
                                                                        setState(() {
                                                                          listFileLesson.removeAt(fileIndex);  
                                                                        }); 
                                                                      }else{
                                                                      }
                                                                    });
                                                                  });
                                                                },
                                                                child: Container(
                                                                  width: 70,
                                                                  height: 70,
                                                                  margin: const EdgeInsets.only(right:8,bottom: 8),
                                                                  decoration: BoxDecoration(
                                                                    borderRadius: BorderRadius.circular(12),
                                                                    image: DecorationImage(
                                                                      image: FileImage(File(listFileLesson[fileIndex].path)),
                                                                      fit: BoxFit.cover,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      ),
                                                    ):Container(),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          IconButton(onPressed: (){
                                            getImageFromCamera(activityType: "1" );
                                          }, icon: SvgPicture.asset(ImageAssets.camera,color: Colorconstand.primaryColor,)),
                                          IconButton(onPressed: () {
                                            getMediaFromDevice(activityType: "1" );
                                            
                                          }, icon: SvgPicture.asset(ImageAssets.gallery,color: Colorconstand.primaryColor,)),
                                        ],
                                      ),
                                    ):AnimatedOpacity(
                                        duration: Duration(milliseconds: durationMilli),
                                        opacity: isCheckLesson ? 1 : 0,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            InkWell(
                                              onTap: (){
                                                getImageFromCamera(activityType: "1");
                                              },
                                              child: Container(
                                              padding: const EdgeInsets.all(8),
                                            
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(ImageAssets.camera,color: Colorconstand.primaryColor,),
                                                    const SizedBox(width: 6,),
                                                    Text("TAKE_PHOTO".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor))
                                                  ],
                                                ),
                                              ),
                                            ),
                                            const SizedBox(width: 28),
                                            InkWell(
                                              onTap: (){
                                                getMediaFromDevice(activityType: "1" );
                                              },
                                              child: Container(
                                                padding: const EdgeInsets.all(8),
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(ImageAssets.gallery,color: Colorconstand.primaryColor,),
                                                    const SizedBox(width: 6,),
                                                    Text("ATTACH_PIC".tr(), style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor,),),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        // ======================= End ARTICAL_QUOTES =====================================
                  
                        const SizedBox(height: 20),
                        isSetExam == true && isTeacherAbsent == false? Container(
                          height: 200,
                          child: Container(
                            child: Column(
                              children: [
                                Container(
                                  child: SvgPicture.asset(ImageAssets.EXAM_ICON,width: 70,height: 70,color:isSetExam == true?Colorconstand.alertsAwaitingText:Colorconstand.alertsAwaitingBg),
                                ),
                                const SizedBox(height: 12,),
                                Text("EXAM_DAY".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.alertsAwaitingText,)),
                              ],
                            ),
                          ),
                        ):isTeacherAbsent == true?Container(
                          height: 200,
                          child: Container(
                            child: Column(
                              children: [
                                Container(
                                  child:const Icon(Icons.timer_sharp,size: 70,color:Colorconstand.lightBlack),
                                ),
                                const SizedBox(height: 12,),
                                Text("NOT_TAUGHT".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.lightBlack,)),
                              ],
                            ),
                          ),
                        )
                        :Container(height: 100,),
                        isSetExam==true || widget.isPrimary==true?Container()
                        :GestureDetector(
                          child: Container(
                          alignment: Alignment.center,
                          child: GestureDetector(
                            onTap: () {
                              if(isTeacherAbsent == true){
                                setState(() {
                                  isdisplayTeacherAbsent = false;
                                  isTeacherAbsent = false;
                                  isSetExam = widget.data.isExam!;
                                });
                                if(teacherAbsent.isEmpty){
                                  SetExamApi().deleteArticleBySceduleApi(scheduleId: widget.data.scheduleId.toString(),date: widget.data.date.toString(),typeActivity: "4").then((value) {
                                    setState(() {
                                      teacherAbsentValueText = "";
                                      teacherAbsentController.text = "";
                                      teacherAbsentTitle = "";
                                      teacherAbsent = [];
                                    });
                                    setState(() {
                                      if(widget.routFromScreen == 2){
                                        callSceduleAgain();
                                      }
                                      if(widget.routFromScreen == 1){
                                        BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                      } 
                                    });
                                  });
                                }
                                else{
                                  SetExamApi().deleteArticleApi(id: teacherAbsent[0].id.toString()).then((value) {
                                    setState(() {
                                      teacherAbsentValueText = "";
                                      teacherAbsentController.text = "";
                                      teacherAbsentTitle = "";
                                      teacherAbsent = [];
                                    });
                                    setState(() {
                                      if(widget.routFromScreen == 2){
                                                      callSceduleAgain();
                                                    }
                                                    if(widget.routFromScreen == 1){
                                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                    } 
                                    });
                                  });
                                }
                              }
                              else{
                               buttonSheetSetTeacherAbsent();
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: Colorconstand.lightBlack,
                              ),
                              height: 40,
                              width: 270,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:[
                                  const Icon(Icons.timer_sharp,size: 30,color:Colorconstand.neutralWhite),
                                  const SizedBox(width: 10,),
                                  Text(isTeacherAbsent==false?"CHANGE_TO_TEACHER_ABSENT".tr():"CHANGE_TO_TEACHER_PRESENCE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite),),
                                ],
                              ),
                            ),
                          ),
                        ),
                        ),
                        Container(height: 10,),
                        isTeacherAbsent == true || widget.isPrimary==true?Container()
                        :Container(
                          alignment: Alignment.center,
                          child: OutlinedButton(
                            onPressed: () {
                              setState(() {
                                if(isSetExam ==false){
                                  setState(() {
                                    isSetExam = true;
                                  });
                                  BlocProvider.of<SetExamBloc>(context).add(
                                    SetExam(
                                      idClass: widget.data.classId.toString(), 
                                      idsubject: widget.data.subjectId.toString(),
                                      isNoExam: "0", 
                                      type:"1", 
                                      date: widget.data.date, 
                                      scheduleId: widget.data.scheduleId.toString()
                                    )
                                  );
                                }
                                else{
                                  setState(() {
                                    isSetExam = false;
                                  });
                                    if(widget.data.examItem == null){
                                      SetExamApi().deleteExamDateWithSchedule(classid: widget.data.classId.toString(),subjectid: widget.data.subjectId.toString(),type: widget.type,month: widget.month);
                                    }
                                    else{
                                      SetExamApi().deleteExamDate(id: widget.data.examItem!.id.toString());
                                    }
                                }
                                Future.delayed( Duration(milliseconds: durationMilli)).then((value) {
                                  if(widget.routFromScreen == 2){
                                    if(widget.routFromScreen == 2){
                                      callSceduleAgain();
                                    }
                                    if(widget.routFromScreen == 1){
                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                    } 
                                  }
                                  if(widget.routFromScreen == 1){
                                    BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                  }
                                });
                              });
                            },
                            style: OutlinedButton.styleFrom(
                                side: BorderSide(color: isSetExam == true? Colorconstand.alertsDecline:Colorconstand.primaryColor, width: 2),
                                shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                            child: Container(
                              height: 40,
                              width: 240,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:[
                                  SvgPicture.asset(ImageAssets.EXAM_ICON,width: 30,height: 30,color:isSetExam == true?Colorconstand.alertsDecline:Colorconstand.primaryColor),
                                  const SizedBox(width: 8,),
                                  Text( isSetExam == true?'CHANGE_TO_NORMOL'.tr():"SET_EXAM_DATE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: isSetExam == true?Colorconstand.alertsDecline:Colorconstand.primaryColor),),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 25,),
                      ],
                    ),
                  ),
                ],
              ),
              ),
          ),
    //=========== Show widget HomeWork =========================
          AnimatedContainer(
            duration:const Duration(milliseconds: 200),
            height: showHomewrokPick == false?0:MediaQuery.of(context).size.height,
            child: SafeArea(
              bottom: false,
              child: Container(
                padding:const EdgeInsets.only(bottom: 25),
                color: Colorconstand.neutralWhite,
                child: Column(
                  children: [
                    Container(
                        margin:const EdgeInsets.symmetric(vertical: 0,horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                          isKeyboard==true?Container():GestureDetector(
                            onTap: (){
                             setState(() {
                              showHomewrokPick = false;
                              if(homeList.isEmpty){
                                setState(() {
                                titleAnnouncement.text = "";
                                subtitleAnnouncement.text = "";
                                listMultiMediaHomeWork = [];
                                });
                              }
                              else{
                                setState(() {
                                listMultiMediaHomeWork = [];
                                titleAnnouncement.text = homeList[0].title.toString()=="null"?"":homeList[0].title.toString();
                                subtitleAnnouncement.text = homeList[0].description.toString()=="null"?"":homeList[0].description.toString();
                                });
                              }
                             });
                            },
                            child: SvgPicture.asset(ImageAssets.chevron_left,width: 32,)),
                          Expanded(child: Text('HOMEWORK'.tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colors.black),textAlign: TextAlign.center,)),
                          isKeyboard==true?Container():GestureDetector(
                              onTap:() {
                                if(lessList.isEmpty){
                                  SetExamApi().deleteArticleBySceduleApi(scheduleId: widget.data.scheduleId.toString(),date: widget.data.date.toString(),typeActivity: "2").then((value) {
                                    setState(() {
                                    isCheckHomework = false;
                                    showHomewrokPick = false;
                                    titleAnnouncement.text = "";
                                    subtitleAnnouncement.text = "";
                                    listMultiMediaHomeWork = [];
                                    homeList = [];
                                    listLink = [];
                                    showDelect = false;
                                  });
                                  setState(() {
                                    if(widget.routFromScreen == 2){
                                      callSceduleAgain();
                                    }
                                    if(widget.routFromScreen == 1){
                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                    } 
                                  });
                                  });
                                }
                                else{
                                  SetExamApi().deleteArticleApi(id: homeList[0].id.toString()).then((value) {
                                  setState(() {
                                    isCheckHomework = false;
                                    showHomewrokPick = false;
                                    titleAnnouncement.text = "";
                                    subtitleAnnouncement.text = "";
                                    listMultiMediaHomeWork = [];
                                    listLink = [];
                                    homeList[0].file = [];
                                    homeList = [];
                                    showDelect = false;
                                  });
                                  setState(() {
                                    if(widget.routFromScreen == 2){
                                      callSceduleAgain();
                                    }
                                    if(widget.routFromScreen == 1){
                                      BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                    } 
                                  });
                                });
                                }
                              },
                              child: homeList.isEmpty && showDelect == false?Container(width: 30,):Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 0),
                                child: SvgPicture.asset(
                                  ImageAssets.trash_icon,
                                  width: 32,
                                  color: Colorconstand.neutralDarkGrey,
                                ),
                              ),
                            ),                       
                          ],
                        ),
                      ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              alignment: Alignment.topCenter,
                              padding: const EdgeInsets.only(
                                  left: 20.0, top: 26.0, right: 20),
                              width: double.maxFinite,
                              decoration: const BoxDecoration(
                                color: Colorconstand.neutralWhite,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10.0),
                                  topRight: Radius.circular(10.0),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Column(
                                    children: [ 
                                        TextField(
                                          maxLines: null,
                                          controller: titleAnnouncement,
                                          keyboardType: TextInputType.text,
                                          style: ThemsConstands.headline3_semibold_20,
                                          decoration: InputDecoration(
                                            hintText: "${"HINTANN".tr()} *",
                                            hintStyle:ThemsConstands.headline3_semibold_20,
                                            border:  const OutlineInputBorder(
                                              borderRadius: BorderRadius.all(Radius.circular(8)),
                                              borderSide: BorderSide(color: Colorconstand.neutralGrey,)
                                            ),
                                          ),
                                        ),
                                      const Divider(
                                        color: Colorconstand.neutralGrey,
                                      ),
                                      Container(
                                        height: 200,
                                        width: MediaQuery.of(context).size.width,
                                        padding: const EdgeInsets.symmetric(horizontal:12,vertical: 4),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8),
                                          border: Border.all(color: Colorconstand.neutralGrey)
                                        ),
                                        child: TextField(
                                          maxLines: null,
                                          controller: subtitleAnnouncement,
                                          keyboardType: TextInputType.multiline,
                                          decoration: InputDecoration(
                                            hintText: "DESCRIBE_HONEWORK".tr(),
                                            hintStyle: ThemsConstands.headline_4_medium_18
                                                .copyWith(
                                              color: Colorconstand.neutralDarkGrey,
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 18,
                                  ),
                                  listMultiMediaHomeWork.isNotEmpty || homeList.isNotEmpty || listLink.isNotEmpty || linkListHomeWork.isNotEmpty? Container(
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Container(
                                        child: Row(
                                          children: [
                                            listMultiMediaHomeWork.isEmpty?Container():Container(
                                              height: 70,
                                              child: Container(
                                                child: ListView.builder(
                                                  physics:const NeverScrollableScrollPhysics(),
                                                  shrinkWrap: true,
                                                  scrollDirection: Axis.horizontal,
                                                  itemCount: listMultiMediaHomeWork.length,
                                                  itemBuilder: (context, index) {
                                                    return Container(
                                                      height: 70,
                                                        width: 70,
                                                      child: Stack(
                                                        children: [
                                                          GestureDetector(
                                                            onTap: (){
                                                              if(listMultiMediaHomeWork[index].path.contains(".pdf")){
                                                                Navigator.push(context, MaterialPageRoute(builder: (context)=> PdfFileViewScreen(pdfUrlFile: listMultiMediaHomeWork[index],)))
                                                                .then((removed){
                                                                  if(removed == true){
                                                                      setState(() {
                                                                          listMultiMediaHomeWork.removeAt(index);
                                                                        });
                                                                    }else{
                                                                    }
                                                                });
                                                              }
                                                              else{
                                                                Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: index,fileImage: listMultiMediaHomeWork[index],isFile: true,)))
                                                                  .then((removed){
                                                                    if(removed == true){
                                                                        setState(() {
                                                                            listMultiMediaHomeWork.removeAt(index);
                                                                          });
                                                                      }else{
                                                                      }
                                                                  });
                                                                }
                                                            },
                                                          child: listMultiMediaHomeWork[index].path.contains(".pdf")? 
                                                          Container(
                                                            width: 70,
                                                            height: 70,
                                                            margin:const EdgeInsets.only(left: 5),
                                                            decoration: BoxDecoration(
                                                              border: Border.all(),
                                                              borderRadius: BorderRadius.circular(12)
                                                            ),
                                                            child: Image.asset(ImageAssets.pdf_icon,width: 60,),
                                                          )
                                                          : Container(
                                                              margin:const EdgeInsets.only(left: 5),
                                                              decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(12),
                                                                image: DecorationImage(
                                                                  image: FileImage(File(listMultiMediaHomeWork[index].path)),
                                                                  fit: BoxFit.cover,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Positioned(
                                                            right: 2,top: 2,
                                                            child: GestureDetector(
                                                              onTap: () {
                                                                setState(() {
                                                                  listMultiMediaHomeWork.removeAt(index);
                                                                });
                                                              },
                                                              child: Container(
                                                                decoration:const BoxDecoration(
                                                                  color: Colorconstand.primaryColor,
                                                                  shape: BoxShape.circle
                                                                ),
                                                                child:const Icon(Icons.close,size: 22,color: Colorconstand.neutralWhite,)),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                            ),
                                            homeList.isEmpty?Container():
                                            Container(
                                              child:homeList[0].file!.isEmpty?Container():Container(
                                                height: 70,
                                                child: ListView.builder(
                                                  physics:const NeverScrollableScrollPhysics(),
                                                  shrinkWrap: true,
                                                  scrollDirection: Axis.horizontal,
                                                  itemCount: homeList[0].file!.length,
                                                  itemBuilder: (context, index) {
                                                    return Container(
                                                      height: 70,
                                                        width: 70,
                                                      child: Stack(
                                                        children: [
                                                          GestureDetector(
                                                            onTap: (){
                                                              if(homeList[0].file![index].fileType!.contains("image")){
                                                                Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewImageAttach(index: index,fileImage: File(""),isFile: false,imageNework: homeList[0].file![index].fileShow.toString(),)))
                                                                .then((removed){
                                                                  if(removed == true){
                                                                      setState(() {
                                                                          SetExamApi().deleteScheduleAttachApi(scheduleActivityId: homeList[0].id.toString(), attachmentsId: homeList[0].file![index].id.toString()).then((value) {
                                                                              if(widget.routFromScreen == 2){
                                                                                callSceduleAgain();
                                                                              }
                                                                              if(widget.routFromScreen == 1){
                                                                                BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
                                                                              } 
                                                                          });
                                                                          homeList[0].file!.removeAt(index);
                                                                        });
                                                                    }else{
                                                                    }
                                                                });
                                                              }
                                                              else{
                                                                setState(() {
                                                                  Navigator.of(context).push(MaterialPageRoute(builder: (context,) =>ViewDocuments( path: homeList[0].file![index].fileShow.toString(),filename:homeList[0].file![index].fileName.toString(),)));
                                                                });
                                                              }
                                                            },
                                                            child: homeList[0].file![index].fileType!.contains("image")?
                                                            Container(
                                                              margin:const EdgeInsets.only(left: 5),
                                                              decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(12),
                                                                image: DecorationImage(
                                                                  image: NetworkImage(homeList[0].file![index].fileShow.toString()),
                                                                  fit: BoxFit.cover,
                                                                ),
                                                              ),
                                                            )
                                                            :Container(
                                                              width: 70,
                                                              height: 70,
                                                              margin:const EdgeInsets.only(left: 5),
                                                              decoration: BoxDecoration(
                                                                border: Border.all(),
                                                                borderRadius: BorderRadius.circular(12)
                                                              ),
                                                              child: Image.asset(ImageAssets.pdf_icon,width: 60,),
                                                            ),
                                                          ),
                                                          Positioned(
                                                            right: 2,top: 2,
                                                            child: GestureDetector(
                                                              onTap: () {
                                                                setState(() {
                                                                  SetExamApi().deleteScheduleAttachApi(scheduleActivityId: homeList[0].id.toString(), attachmentsId: homeList[0].file![index].id.toString());
                                                                  homeList[0].file!.removeAt(index);
                                                                });
                                                              },
                                                              child: Container(
                                                                decoration:const BoxDecoration(
                                                                  color: Colorconstand.primaryColor,
                                                                  shape: BoxShape.circle
                                                                ),
                                                                child:const Icon(Icons.close,size: 22,color: Colorconstand.neutralWhite,)),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                            ),
                                            listLink.isNotEmpty || linkListHomeWork.isNotEmpty? Container(
                                              child: Container(
                                                child: Row(
                                                  children: [
                                                    listLink.isEmpty?Container():Container(
                                                      height: 70,
                                                      child: Container(
                                                        child: ListView.builder(
                                                          physics:const NeverScrollableScrollPhysics(),
                                                          shrinkWrap: true,
                                                          scrollDirection: Axis.horizontal,
                                                          itemCount: listLink.length,
                                                          itemBuilder: (context, index) {
                                                            return Container(
                                                              height: 70,
                                                               width: 70,
                                                              child: Stack(
                                                                children: [
                                                                  GestureDetector(
                                                                    onTap: (){
                                                                      if(listLink[index].contains("https://youtu.be")){
                                                                         Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewVideoYoutubeScreen(linkVideoYoutube: listLink[index]) ));
                                                                      }
                                                                      else{
                                                                        launchUrl(
                                                                          Uri.parse(listLink[index],),
                                                                          mode: LaunchMode.externalApplication,
                                                                        );
                                                                      }
                                                                    },
                                                                  child:
                                                                  Container(
                                                                    height: 70,
                                                                    width: 70,
                                                                      padding:const EdgeInsets.all(5),
                                                                      margin:const EdgeInsets.only(left: 5),
                                                                      decoration: BoxDecoration(
                                                                        border: Border.all(),
                                                                        borderRadius: BorderRadius.circular(12)
                                                                      ),
                                                                       child: SvgPicture.asset(listLink[index].contains("https://youtu.be")?ImageAssets.youtub_icon:ImageAssets.earth_icon,width: 45,),
                                                                    ),
                                                                  ),
                                                                  Positioned(
                                                                    right: 2,top: 2,
                                                                    child: GestureDetector(
                                                                      onTap: () {
                                                                        setState(() {
                                                                          listLink.removeAt(index);
                                                                        });
                                                                      },
                                                                      child: Container(
                                                                        decoration:const BoxDecoration(
                                                                          color: Colorconstand.primaryColor,
                                                                          shape: BoxShape.circle
                                                                        ),
                                                                        child:const Icon(Icons.close,size: 22,color: Colorconstand.neutralWhite,)),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                    linkListHomeWork.isEmpty?Container():Container(
                                                      height: 70,
                                                      child: Container(
                                                        child: ListView.builder(
                                                          physics:const NeverScrollableScrollPhysics(),
                                                          shrinkWrap: true,
                                                          scrollDirection: Axis.horizontal,
                                                          itemCount: linkListHomeWork.length,
                                                          itemBuilder: (context, index) {
                                                            return Container(
                                                              height: 70,
                                                               width: 70,
                                                              child: Stack(
                                                                children: [
                                                                  GestureDetector(
                                                                    onTap: (){
                                                                      if(linkListHomeWork[index].fileType!.contains("YOUTUBE_LINK")){
                                                                         Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewVideoYoutubeScreen(linkVideoYoutube: linkListHomeWork[index].fileShow.toString(),) ));
                                                                      }
                                                                      else{
                                                                        launchUrl(
                                                                          Uri.parse(linkListHomeWork[index].fileShow.toString(),),
                                                                          mode: LaunchMode.externalApplication,
                                                                        );
                                                                      }
                                                                    },
                                                                  child:
                                                                  Container(
                                                                    height: 70,
                                                                    width: 70,
                                                                      padding:const EdgeInsets.all(5),
                                                                      margin:const EdgeInsets.only(left: 5),
                                                                      decoration: BoxDecoration(
                                                                        border: Border.all(),
                                                                        borderRadius: BorderRadius.circular(12)
                                                                      ),
                                                                       child: SvgPicture.asset(linkListHomeWork[index].fileType!.contains("YOUTUBE_LINK")?ImageAssets.youtub_icon:ImageAssets.earth_icon,width: 45,),
                                                                    ),
                                                                  ),
                                                                  Positioned(
                                                                    right: 2,top: 2,
                                                                    child: GestureDetector(
                                                                      onTap: () {
                                                                        setState(() {
                                                                          SetExamApi().deleteScheduleAttachApi(scheduleActivityId: homeList[0].id.toString(), attachmentsId: linkListHomeWork[index].id.toString());
                                                                          linkListHomeWork.removeAt(index);
                                                                        });
                                                                      },
                                                                      child: Container(
                                                                        decoration:const BoxDecoration(
                                                                          color: Colorconstand.primaryColor,
                                                                          shape: BoxShape.circle
                                                                        ),
                                                                        child:const Icon(Icons.close,size: 22,color: Colorconstand.neutralWhite,)),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ):Container(),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ):Container(),
                                  MediaQuery.of(context).size.width > 420?Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      customSmallBtn(
                                        onTap: (){
                                          getImageFromCameraHomeWork();
                                        },
                                        title: "TAKE_PHOTO".tr(), iconPath: ImageAssets.camera,
                                      ),
                                      customSmallBtn(
                                        onTap: (){
                                          getMediaFromDeviceHome();
                                        },
                                        title: "ATTACH_PIC".tr(), iconPath: ImageAssets.gallery,
                                      ),
                                      customSmallBtn(
                                        onTap: (){
                                          getFileFromDeviceHomework();
                                        },
                                        title: "PDF".tr(), iconPath: ImageAssets.document,
                                      ),
                                      customSmallBtn(
                                        onTap: (){
                                          setState(() {
                                            showWidgetPassLink = true;
                                          });
                                        },
                                        title: "Link".tr(), iconPath: ImageAssets.attachment_icon,
                                      ),
                                    ],
                                  ):Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          customSmallBtn(
                                            onTap: (){
                                              getImageFromCameraHomeWork();
                                            },
                                            title: "TAKE_PHOTO".tr(), iconPath: ImageAssets.camera,
                                          ),
                                          customSmallBtn(
                                            onTap: (){
                                              getMediaFromDeviceHome();
                                            },
                                            title: "ATTACH_PIC".tr(), iconPath: ImageAssets.gallery,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          customSmallBtn(
                                            onTap: (){
                                              getFileFromDeviceHomework();
                                            },
                                            title: "PDF".tr(), iconPath: ImageAssets.document,
                                          ),
                                          customSmallBtn(
                                            onTap: (){
                                              setState(() {
                                                showWidgetPassLink = true;
                                              });
                                            },
                                            title: "Link".tr(), iconPath: ImageAssets.attachment_icon,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  const SizedBox(height: 18,),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    child: DateTimeWidget(
                                      date: homeList.isEmpty? expairDate:homeList[0].expiredAt.toString(),
                                      onPressed: pickTimeDate,
                                      onClear: (){
                                        setState(() {
                                          if(homeList.isEmpty){
                                            expairDate = "";
                                          }
                                          else{
                                            homeList[0].expiredAt='';
                                          }
                                        });
                                      },
                                    ),
                                            
                                  ),
                                  Container(
                                    padding:  const EdgeInsets.symmetric(vertical: 18,horizontal: 18),
                                    width: MediaQuery.of(context).size.width,
                                    decoration: const BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8))
                                    ),
                                    child: MaterialButton(
                                      disabledTextColor: Colorconstand.neutralGrey,
                                      disabledColor: Colorconstand.neutralGrey,
                                      color: Colorconstand.primaryColor,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12),
                                        ),
                                      ),
                                      padding: const EdgeInsets.symmetric( vertical: 18, horizontal: 28),
                                      onPressed: titleAnnouncement.text == ""?null: (){
                                        setState(() {
                                          showHomewrokPick = false;
                                        });
                                        postActivitiesService(
                                          date: widget.data.date,
                                          scheduleID: widget.data.scheduleId.toString(), 
                                          title: titleAnnouncement.text, 
                                          des: subtitleAnnouncement.text,
                                          type: "2",
                                          expiredDate:homeList.isEmpty?"":homeList[0].expiredAt.toString(),
                                          file: listMultiMediaHomeWork,
                                          links:listLink,
                                        );
                                        setState(() {
                                          isCheckHomework = true;
                                          showDelect = true;
                                        });
                                        PostActionTeacherApi().actionTeacherApi(feature: "SCHEDULE_SCREEN" ,keyFeature: "ADD_HOMEWORK");
                                      },
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Text("SAVE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite)),
                                        ],
                                      ),
                                    ),
                                  )
                                ]
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          buttomKeyboardOpenWidget(!isKeyboard),
//=========== End Show widget HomeWork =========================
//=========== Show widget Pass with link =========================
          showWidgetPassLink==false?Container(height: 0,):Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              color: const Color(0x7B9C9595),
              child: Column(
                children: [
                  const Expanded(child: Center(),),
                  Center(
                    child: Container(
                      margin:const EdgeInsets.symmetric(vertical: 18,horizontal: 18),
                      padding:const EdgeInsets.symmetric(vertical: 18,horizontal: 18),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        color: Colorconstand.neutralWhite
                      ),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: IconButton(onPressed: (){setState(() {showWidgetPassLink=false;linkYoutubeController.text="";});}, icon:const Icon(Icons.close)),
                              ),
                              Row(
                                children: [
                                  Container(
                                    alignment: Alignment.centerRight,
                                    child: GestureDetector(onTap: (){
                                      launchUrl(
                                        Uri.parse("https://www.youtube.com"),
                                        mode: LaunchMode.externalApplication,
                                      );
                                    }, child:SvgPicture.asset("assets/images/entypo-social_youtube-with-circle.svg",width: 30,color: Colorconstand.alertsDecline,)),
                                  ),
                                  Container(
                                    alignment: Alignment.centerRight,
                                    child: GestureDetector(onTap: (){
                                      launchUrl(
                                        Uri.parse("https://www.google.com"),
                                        mode: LaunchMode.externalApplication,
                                      );
                                    }, child:SvgPicture.asset("assets/images/flat-color-icons_google.svg",width: 35,)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Button_Custom(
                            hightButton: 45,
                            radiusButton: 12,
                            onPressed: (){
                              FlutterClipboard.paste().then((value) { 
                                if (value.contains("//youtu.be")||value.contains("https://")) {
                                  setState(() {
                                    linkYoutubeController.text = value;
                                  });
                                }
                                else{
                                  setState(() {
                                    linkYoutubeController.text = "";
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        duration:const Duration(milliseconds: 500),
                                        backgroundColor: Colorconstand.neutralDarkGrey,
                                        content: Text("INVALID_CODE".tr(),style: ThemsConstands.headline_4_semibold_18.copyWith(color:Colorconstand.neutralWhite),textAlign: TextAlign.center,),
                                      ),
                                    );
                                  });
                                }
                              });
                            },
                            buttonColor: Colorconstand.neutralGrey,
                            titleButton: "Past link".tr(),
                          ),
                          linkYoutubeController.text.contains("https://")?
                          Container(
                            margin:const EdgeInsets.only(top: 12),
                            child:GestureDetector(
                              onTap: (){
                                launchUrl(
                                  Uri.parse(linkYoutubeController.text,),
                                  mode: LaunchMode.externalApplication,
                                );
                              },
                              child: Text(linkYoutubeController.text,style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.primaryColor),maxLines: 3,overflow: TextOverflow.ellipsis,)),
                          ):Container(),
                          linkYoutubeController.text.contains("//youtu.be")?
                          Container(
                            margin:const EdgeInsets.only(top: 12,bottom: 12),
                            child: VideoYoutubeLearningScreen(linkVideo: linkYoutubeController.text,)):Container(margin:const EdgeInsets.only(top: 12,bottom: 12),),
                          MaterialButton(
                            disabledTextColor: Colorconstand.neutralGrey,
                            disabledColor: Colorconstand.neutralGrey,
                            color: Colorconstand.primaryColor,
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(12),
                              ),
                            ),
                            padding: const EdgeInsets.symmetric( vertical: 18, horizontal: 28),
                            onPressed: linkYoutubeController.text == ""?null: (){
                              setState(() {
                                showWidgetPassLink = false;
                                listLink.add(linkYoutubeController.text);
                                linkYoutubeController.text="";
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("OK".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralWhite)),
                              ],
                            ),
                          )
                        ],
                      )
                    ),
                  ),
                  const Expanded(child: Center(),),
                ],
              ),
            ),
          ),
    //=========== End Show widget Pass with link  =========================

          //================ Internet offilne ========================
              connection == true
                  ? Container()
                  : Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      top: 0,
                      child: Container(
                       
                       color: const Color(0x7B9C9595),
                      ),
                    ),
              AnimatedPositioned(
                bottom: connection == true ? -150 : 0,
                left: 0,
                right: 0,
                duration:  Duration(milliseconds: durationMilli),
                child: const NoConnectWidget(),
              ),
        //================ Internet offilne ========================
        loadingPostSuccess == true
        ? Container()
        : Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              color: const Color(0x7B9C9595),
              child:const Center(
              child: CircularProgressIndicator(color: Colorconstand.primaryColor,),
              ),
            ),
          ),
        ],
      )
    );
  }

 void buttonSheetSetTeacherAbsent (){
     showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16)),
      ),
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          child: Container(
            margin:const EdgeInsets.only(top: 50),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8),)),
            child: Container(
              margin:const EdgeInsets.only(top: 10,bottom: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 10,bottom: 10),
                    child: Text(
                      "SELECTREFER".tr(),
                      style: ThemsConstands.headline_4_semibold_18
                          .copyWith(color: Colorconstand.primaryColor),
                    ),
                  ),
                  const Divider(
                    height: 10,
                    color: Colorconstand.neutralDarkGrey,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 12,top: 10),
                    child: Button_Custom(
                        hightButton: 55,
                      titlebuttonColor: Colorconstand.neutralWhite,
                        radiusButton: 8,
                      buttonColor: Colorconstand.primaryColor,
                      onPressed: () {
                        setState(() {
                          teacherAbsentController.text = "HEALTH_ISSUES".tr();
                          isdisplayTeacherAbsent = true;
                          isTeacherAbsent = true;
                          isSetExam = false;
                          postTeacherAbsent();
                          Navigator.of(context).pop();
                        });
                      }, 
                      titleButton: 'HEALTH_ISSUES'.tr(),
                      widthButton: MediaQuery.of(context).size.width-100,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 12),
                    child: Button_Custom(
                      hightButton: 55,
                      titlebuttonColor: Colorconstand.neutralWhite,
                        radiusButton: 8,
                      buttonColor: Colorconstand.primaryColor,
                      onPressed: () {
                        setState(() {
                          teacherAbsentController.text = "PRIVATE_ISSUES".tr();
                          isdisplayTeacherAbsent = true;
                          isTeacherAbsent = true;
                          isSetExam = false;
                          postTeacherAbsent();
                          Navigator.of(context).pop();
                        });
                      }, 
                      titleButton: 'PRIVATE_ISSUES'.tr(),
                      widthButton: MediaQuery.of(context).size.width-100,
                    ),
                  ),
                  Button_Custom(
                      hightButton: 55,
                    titlebuttonColor: Colorconstand.neutralWhite,
                    radiusButton: 8,
                    buttonColor: Colorconstand.primaryColor,
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop();
                        isdisplayTeacherAbsent = true;
                        isdisplayTeacherAbsentText = true;
                        teacherAbsentFocusNode.requestFocus();
                      });}, 
                    titleButton: 'OTHER'.tr(),
                    widthButton: MediaQuery.of(context).size.width-100,
                  ),
                  Container(
                    height: 50,
                  )
                ]
              )
            )
          )
        );
      }
    );
  }

  void postTeacherAbsent(){
      PostActvityApi().postTeacherAbsentApi(
        title: teacherAbsentController.text,
        activityType: "4",
        date: widget.data.date.toString(),
        scheduleId: widget.data.scheduleId.toString(),
      ).then((value) {
        if(value){
          setState(() {
            isdisplayTeacherAbsentText = false;
            isdisplayTeacherAbsent = true;
            isTeacherAbsent = true;
            if(widget.routFromScreen == 2){
              callSceduleAgain();
            }
            if(widget.routFromScreen == 1){
              BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
            } 
          });
        }
        else{
          setState(() {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              duration: const Duration(milliseconds: 800),
              width: MediaQuery.of(context).size.width-100,
              elevation: 2.0,
              behavior: SnackBarBehavior.floating,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              content: Wrap(
                children: [
                  Container(
                    height: 20,
                    child: Center(
                      child: Text(
                        "${"SAVE".tr()} ${"FAIL".tr()}",
                        style: ThemsConstands.headline_5_semibold_16
                            .copyWith(color: Colorconstand.neutralWhite),
                      ),
                    ),
                  ),
                ],
              ),
            ));                                              });
        }
      });
  }

  void postActivitiesService({ String? title, List<String>? links ,List<File>? file,required String type, String? date, required String scheduleID, String? des, String? expiredDate}){
    PostActvityApi().postActivityApi(
      title: title??" ",
      activityType: type,
      date: widget.data.date,
      description: des,
      scheduleId: scheduleID,
      expiredDate: expiredDate,
      attachments: file,
      links: links
    ).then((value) {
      if(value){
        setState(() {
          if(type=="5"){
            postHomeResearchInKeybord = false;
          }
          else{
           postAtticalInKeybord = false;
          }
        });
        if(widget.routFromScreen == 2){
          callSceduleAgain();
        }
        if(widget.routFromScreen == 1){
          BlocProvider.of<DaskboardScreenBloc>(context).add(ScheduledDashboardEvent(date: widget.data.date));
        } 
      }else{
        print("Unsuccess post activities");
      }
        
    });
  }

  void callSceduleAgain(){
    if(widget.activeMySchedule == true ){                                                                                                
          BlocProvider.of<GetScheduleBloc>(context).add(GetMyScheduleEvent(date: widget.data.date));
    }
    else{
      BlocProvider.of<GetScheduleBloc>(context).add(GetClassScheduleEvent(date: widget.data.date,classid: widget.data.classId.toString()));
    }
  }

  void splitTypeOfActivities({List<Activity>? activities} ){
     if(activities!.isNotEmpty){
      lessList = widget.data.activities!.where((element) { 
        return element.activityType == 1;
      }).toList();
      homeResearchList = widget.data.activities!.where((element) { 
        return element.activityType ==5;
      }).toList();
      homeList = widget.data.activities!.where((element) {
        return element.activityType == 2;
      }).toList();
      teacherAbsent = widget.data.activities!.where((element) {
        return element.activityType == 4;
      }).toList();
      // print("Home list ${lessList.length}");
    }
  }

  void checkIsNotEmptyTypeHomework(){
    if(homeList.isNotEmpty){
      setState(() {
        linkListHomeWork = homeList[0].link!;
        if(homeList.isEmpty){
          setState(() {
             titleAnnouncement.text = "";
             subtitleAnnouncement.text = "";
          });
        }
        else{
          setState(() {
             titleAnnouncement.text = homeList[0].title.toString() == "null"?"":homeList[0].title.toString();
             subtitleAnnouncement.text = homeList[0].description.toString() == "null"?"":homeList[0].description.toString();
          });
        }
      });
    }
  }

  void checkIsNotEmptyTypeTeacherAbsent(){
    if(teacherAbsent.isEmpty){
      teacherAbsentController.text = "";
      setState(() {
        isTeacherAbsent = false;
        isdisplayTeacherAbsent = false;
      });
    }
    else{
      setState(() {
        teacherAbsentValueText = teacherAbsent[0].title==null?"":teacherAbsent[0].title.toString();
        teacherAbsentTitle =  teacherAbsent[0].title==null?"":teacherAbsent[0].title.toString();
        teacherAbsentController.text =  teacherAbsent[0].title==null?"":teacherAbsent[0].title.toString();
        isTeacherAbsent = true;
        isdisplayTeacherAbsent = true;
      });
    }
  }

  void checkIsNotEmptyType(){
    if(lessList.isNotEmpty){
      setState(() {
        if(lessList.isNotEmpty){
          if(lessList[0].title == "មានរូបភាព"){
            titleController.text = "";
          }
          else{
            titleController.text = lessList[0].title.toString();
          }
        }
        else{
          titleController.text = "";
        }
        enabledesController = true;
        isCheckLesson=true;
      });
    }
    if(homeResearchList.isNotEmpty){
      setState(() {
        if(homeResearchList.isNotEmpty){
          if(homeResearchList[0].title == "មានរូបភាព"){
            titleHomeResearchController.text = "";
          }
          else{
            titleHomeResearchController.text = homeResearchList[0].title.toString();
          }
        }
        else{
          titleHomeResearchController.text = "";
        }
        enabledesHomeResearchController = true;
        isCheckHomeResearch=true;
      });
    }
    if(homeList.isNotEmpty) {
      setState(() {
        isCheckHomework = true;    
        showDelect = true;   
      });
    }
  }

  // Add attachment for សម្រង់អត្ថបទ  or HomeWork Research
  Future<void> getImageFromCamera({required String activityType}) async {
    final pickedCamera = await _picker.pickImage(source: ImageSource.camera);
    if (pickedCamera != null) {
      if(activityType == "1"){
        setState(() {
         listFileLesson.add(File(pickedCamera.path));
        });
        postActivitiesService(
          date: widget.data.date.toString(), 
          scheduleID: widget.data.scheduleId.toString(), 
          title: titleController.text==null || titleController.text== "" ?"មានរូបភាព":titleController.text, 
          type: activityType,
          file: listFileLesson
        );
      }
      else if(activityType == "5"){
        setState(() {
         listFileHomeResearch.add(File(pickedCamera.path));
        });
        postActivitiesService(
          date: widget.data.date.toString(), 
          scheduleID: widget.data.scheduleId.toString(), 
          title: titleController.text==null || titleController.text== "" ?"មានរូបភាព":titleController.text, 
          type: activityType,
          file: listFileHomeResearch
        );
      }
    }
  }

  Future<dynamic> getMediaFromDevice({required String activityType}) async {
    // final listMedia = await ImagesPicker.pick(
    //   count: 10,
    //   gif: true,
    //   pickType: PickType.all,
    //   language: Language.English,
    //   maxTime: 3600,
    //   quality: 0.8,
    //   cropOpt: CropOption(aspectRatio: CropAspectRatio.wh16x9),
    // );
    List<XFile>? listMedia = await _picker.pickMultiImage();
    if (listMedia.isNotEmpty) {
      for (var item in listMedia) {
        setState(() {
          if(activityType=="5"){
            listFileHomeResearch.add(File(item.path));
          }
          else{
            listFileLesson.add(File(item.path));
          }
        });
      }
      if(activityType=="5"){
        postActivitiesService(
          date: widget.data.date.toString(), 
          scheduleID: widget.data.scheduleId.toString(), 
          title: titleHomeResearchController.text==null || titleHomeResearchController.text== "" ?"មានរូបភាព":titleHomeResearchController.text, 
          type: activityType,
          file: listFileHomeResearch
        );
      }
      else{
        postActivitiesService(
          date: widget.data.date.toString(), 
          scheduleID: widget.data.scheduleId.toString(), 
          title: titleController.text==null || titleController.text== "" ?"មានរូបភាព":titleController.text, 
          type: activityType,
          file: listFileLesson
        );
      }
      
    }
  }

  //======= HomeWork pick Image ===========

  Future<void> getImageFromCameraHomeWork() async {
    final pickedCamera = await _picker.pickImage(source: ImageSource.camera);
    if (pickedCamera != null) {
      setState(() {
        listMultiMediaHomeWork.add(File(pickedCamera.path));
      }); 
    }
  }

  Future<void> getMediaFromDeviceHome() async {
    List<XFile>? listMedia = await _picker.pickMultiImage();
    // final listMedia = await ImagesPicker.pick(
    //   count: 10,
    //   gif: true,
    //   pickType: PickType.all,
    //   language: Language.System,
    //   maxTime: 3600,
    //   quality: 0.8,
    //   cropOpt: CropOption(aspectRatio: CropAspectRatio.wh16x9),
    // );
    if (listMedia.isNotEmpty) {
      for (var item in listMedia) {
        setState(() {
          listMultiMediaHomeWork.add(File(item.path));
        });
      }
    }
  }

  Future<void> getFileFromDeviceHomework() async {
    final result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'cvs']);
    if (result != null) {
      List<File> files = result.paths.map((e) => File(e.toString())).toList();
      for (var item in files) {
        setState(() {
          listMultiMediaHomeWork.add(File(item.path));
        });
      }
    }
  }

  Widget customSmallBtn({required Function() onTap,required String iconPath,required String title}) {
    return InkWell(
      onTap:onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            SvgPicture.asset(iconPath,color: Colorconstand.primaryColor,),
            const SizedBox(width: 8,),
            Text(title,style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor,fontSize: 18),)
          ],
        ),
      ),
    );
  }


  Future pickTimeDate() async {
    DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: date,
      firstDate: DateTime(2019),
      lastDate: DateTime(2100),
    );
    if (newDate == null) return;
    setState(() {
      if(homeList.isEmpty){
        expairDate = DateFormat('dd/MM/yyyy').format(newDate);
      }
      else{
        homeList[0].expiredAt= DateFormat('dd/MM/yyyy').format(newDate);
      }
      postActivitiesService(
        date: widget.data.date,
        scheduleID: widget.data.scheduleId.toString(), 
        title: titleAnnouncement.text, 
        des: subtitleAnnouncement.text,
        type: "2",
        expiredDate: DateFormat('dd/MM/yyyy').format(newDate),
        file: [],
        links: []
      );
    });
  }

  Widget buttomKeyboardOpenWidget(bool isKeyboard) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 000),
      bottom:0,
      left: 0,
      right: 0,
      child: !isKeyboard ? Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Divider(
            color: Colorconstand.neutralGrey,
          ),
          Container(
            color: Colorconstand.screenWhite,
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 8.0, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isKeyboard = true;
                      });
                    },
                    child: SvgPicture.asset(
                      ImageAssets.keyboard_icon,
                      height: 28,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                    child: GestureDetector(
                      onTap: () async {
                        setState(() {
                        FocusScope.of(context).unfocus();
                         if(postAtticalInKeybord == true){
                          if(titleController.text.trim().isNotEmpty){
                            postActivitiesService(
                              date: widget.data.date,
                              scheduleID: widget.data.scheduleId.toString(), 
                              title: titleController.text, 
                              type: "1",
                              file: []
                            );
                          }
                         }
                         else if(postHomeResearchInKeybord == true){
                           if(titleHomeResearchController.text.trim().isNotEmpty){
                            postActivitiesService(
                              date: widget.data.date,
                              scheduleID: widget.data.scheduleId.toString(), 
                              title: titleHomeResearchController.text, 
                              type: "5",
                              file: []
                            );
                          }
                         }
                        });
                      },
                      child: Text("OFFKEYBOARD".tr())
                    ),
                  ),
                
                ],
              ),
            ),
          ) 

        ],
      ):Container(),
    );
  }

}
