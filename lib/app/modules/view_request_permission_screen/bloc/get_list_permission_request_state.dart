part of 'get_list_permission_request_bloc.dart';

abstract class GetListPermissionRequestState extends Equatable {
  const GetListPermissionRequestState();
  
  @override
  List<Object> get props => [];
}

class GetListPermissionRequestInitial extends GetListPermissionRequestState {}
class GetListPermissionRequestLoading extends GetListPermissionRequestState{}

class GetListPermissionRequestLoaded extends GetListPermissionRequestState{
  final ListPermissionRequestStudentModel? listPermissionRequestStudentModel;
  const GetListPermissionRequestLoaded({required this.listPermissionRequestStudentModel});
}
class GetListPermissionRequestError extends GetListPermissionRequestState{}