import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../models/request_permission_model/list_request_permission_model.dart';
import '../../../service/api/student_requst_permission_api/requst_permission_api.dart';
part 'get_list_permission_request_event.dart';
part 'get_list_permission_request_state.dart';

class GetListPermissionRequestBloc extends Bloc<GetListPermissionRequestEvent, GetListPermissionRequestState> {
  final RequestPermissionApi requestPermissionApi;
  GetListPermissionRequestBloc({required this.requestPermissionApi}) : super(GetListPermissionRequestInitial()) {
    on<GetListPermissionRequest>((event, emit) async{
      emit(GetListPermissionRequestLoading());
      try {
        var data = await requestPermissionApi.getRequestPermissionApi(date: event.date,scheduleId: event.scheduleId);
        emit(GetListPermissionRequestLoaded(listPermissionRequestStudentModel: data));
      } catch (e) {
        print(e);
        emit(GetListPermissionRequestError());
      }
    });
  }
}
