part of 'get_list_permission_request_bloc.dart';

abstract class GetListPermissionRequestEvent extends Equatable {
  const GetListPermissionRequestEvent();

  @override
  List<Object> get props => [];
}
class GetListPermissionRequest extends GetListPermissionRequestEvent{
  final String date;
  final String scheduleId;
  const GetListPermissionRequest({required this.date,required this.scheduleId});
}