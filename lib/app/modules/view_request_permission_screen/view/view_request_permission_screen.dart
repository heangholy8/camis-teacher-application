import 'package:audio_waveforms/audio_waveforms.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/view_request_permission_screen/bloc/get_list_permission_request_bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import '../../../../widget/empydata_widget/empty_widget.dart';
import '../../time_line/widget/view_image.dart';

class ViewPermissionRequestScreen extends StatefulWidget {
  const ViewPermissionRequestScreen({super.key});

  @override
  State<ViewPermissionRequestScreen> createState() => _ViewPermissionRequestScreenState();
}

class _ViewPermissionRequestScreenState extends State<ViewPermissionRequestScreen> {
  final _controller = PageController(viewportFraction: 0.85);
  double activeindexMyclass = 0;
  List<double> waveformData = [];
  String playDuration = "0:00";
  bool isplay = false;
  bool getVoice = true;
  String? path;
  String voicePath = "";
  List listImage = [];
  List listVoice = [];
  late Directory appDirectory;
  late final RecorderController recorderController;
  PlayerController playerController = PlayerController();

  Future<void> _getVoiebyLink(String url) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';
    try {
      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = path!;
        file = File(filePath);
        voicePath = filePath;
        await file.writeAsBytes(bytes);
      }
      else
        filePath = 'Error code: '+response.statusCode.toString();
    }
    catch(ex){
      filePath = 'Can not fetch url';
    }
    Future.delayed(Duration.zero,() async {
      waveformData = await playerController.extractWaveformData(
      path: filePath,
      noOfSamples: 100,);
      await playerController.preparePlayer(
      path: filePath,
      shouldExtractWaveform: true,
      noOfSamples: 100,
      volume: 10.0);
      setState((){
        playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
        getVoice = false;
        isplay = false;
      });
    },);
  }
  void _getDir() async {
    
    appDirectory = await getApplicationDocumentsDirectory();
    path = "${appDirectory.path}/recording.m4a";
    setState(() {});
  }
  
  @override
  void initState() {
    _getDir();
    super.initState();
  }
  @override
  void dispose() {
    playerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.lightBulma,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colorconstand.neutralWhite.withOpacity(0.15),
        child: SafeArea(
          bottom: false,
          child: Container(
            child: Column(
              children: [
                Container(
                  margin:const EdgeInsets.only(bottom: 25,left: 28,right: 28),
                  alignment: Alignment.center,
                  height: 60,
                  child: Row(
                    children: [
                      Container(width: 28,),
                      Expanded(child: Text("PERMISSIONLETTER".tr(),style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),textAlign: TextAlign.center,)),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                          playerController.dispose();
                        }, 
                        child:const Icon(Icons.close_rounded,size: 28,color: Colorconstand.neutralWhite,)
                      )

                    ],
                  ),
                ),
                BlocBuilder<GetListPermissionRequestBloc, GetListPermissionRequestState>(
                  builder: (context, state) {
                    if(state is GetListPermissionRequestLoading){
                      return const Expanded(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                    else if(state is GetListPermissionRequestLoaded){
                      var data = state.listPermissionRequestStudentModel!.data;
                      return Expanded(
                        child: Column(
                          children: [
                            Expanded(
                              child: PageView.builder(
                                controller: _controller,
                                itemCount: data!.length,
                                onPageChanged: (value){
                                  setState((){
                                    listImage = data[value].file!.where((element) {return element.fileType!.contains("image");}).toList();
                                    listVoice = data[value].file!.where((element) {return element.fileType!.contains("audio");}).toList();
                                    activeindexMyclass = value.toDouble();
                                    if(listVoice.isNotEmpty){
                                       _getVoiebyLink(listVoice[0].fileShow.toString());
                                    }
                                    else{
                                      setState(() {
                                        listVoice = [];
                                      });
                                    }
                                  });
                                },
                                itemBuilder: (context, index) {
                                  if(getVoice == true){
                                    listImage = data[0].file!.where((element) {return element.fileType!.contains("image");}).toList();
                                    listVoice = data[0].file!.where((element) {return element.fileType!.contains("audio");}).toList();
                                  }
                                  Future.delayed(Duration.zero,(){
                                    if(listVoice.isNotEmpty){
                                      if(getVoice == true){
                                        _getVoiebyLink(listVoice[0].fileShow.toString());
                                      }
                                    }
                                   });
                                  return Container(
                                    margin:const EdgeInsets.only(right: 10,left: 10),
                                    padding:const EdgeInsets.all(18),
                                    decoration: BoxDecoration(
                                      color: Colorconstand.neutralSecondBackground,
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                    child: SingleChildScrollView(
                                      child: Column(
                                        children: [
                                          Container(
                                           decoration: BoxDecoration(
                                              color: Colorconstand.neutralWhite,
                                              borderRadius: BorderRadius.circular(18),
                                              boxShadow:[
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.5),
                                                  blurRadius: 30.0, // soften the shadow
                                                  spreadRadius: 2.0, //extend the shadow
                                                  offset:const Offset(2.0,2.0,),
                                                )
                                              ],
                                            ),
                                            child: Column(
                                              children: [
                                                Container(
                                                  height: 80,
                                                  margin:const EdgeInsets.symmetric(horizontal: 12),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        child: Row(
                                                          children: [
                                                             const Icon(Icons.account_circle_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                             const SizedBox(width: 5,),
                                                             Text("NAME".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                                          ],
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          alignment: Alignment.centerRight,
                                                          margin:const EdgeInsets.only(top: 0 ),
                                                          child: ListView.builder(
                                                            padding:const EdgeInsets.all(0),
                                                            shrinkWrap: true,
                                                            physics:const NeverScrollableScrollPhysics(),
                                                            itemCount: data[index].studentId!.length,
                                                            itemBuilder: (context, indexStudent){
                                                              return Container( alignment: Alignment.centerRight,child: Text(data[index].studentId![indexStudent].name.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),));
                                                            }
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const Divider(
                                                  height: 0,
                                                  color: Colorconstand.neutralGrey,
                                                ),
                                                Container(
                                                  height: 80,
                                                  margin:const EdgeInsets.symmetric(horizontal: 12),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        child: Row(
                                                          children: [
                                                             const Icon(Icons.date_range_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                             const SizedBox(width: 5,),
                                                             Text("PERMISSIONDATE".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: [
                                                            Text(data[index].startDate.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                                            data[index].startDate == data[index].endDate?Container():Text("ដល់",style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                                            data[index].startDate == data[index].endDate?Container():Text(data[index].endDate.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const Divider(
                                                  height: 0,
                                                  color: Colorconstand.neutralGrey,
                                                ),
                                                data[index].requestTypes == 2?Container():Container(
                                                  margin:const EdgeInsets.symmetric(horizontal: 12),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        child: Row(
                                                          children: [
                                                             const Icon(Icons.access_time,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                             const SizedBox(width: 5,),
                                                             Text("PERMISSION_TIME".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                                          ],
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          padding:const EdgeInsets.all(0),
                                                          alignment: Alignment.centerRight,
                                                          margin:const EdgeInsets.only(top: 35 ),
                                                          child: ListView.builder(
                                                            shrinkWrap: true,
                                                            physics:const NeverScrollableScrollPhysics(),
                                                            itemCount: data[index].schedules!.length,
                                                            itemBuilder: (context, indexSchedule){
                                                              return Container( alignment: Alignment.centerRight,child: Text("${data[index].schedules![indexSchedule].startTime}-${data[index].schedules![indexSchedule].endTime}",style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),));
                                                            }
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                data[index].requestTypes == 2?Container(): const Divider(
                                                  height: 0,
                                                  color: Colorconstand.neutralGrey,
                                                ),
                                                Container(
                                                  height: 80,
                                                  margin:const EdgeInsets.symmetric(horizontal: 12),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        child: Row(
                                                          children: [
                                                             const Icon(Icons.message_outlined,size: 18,color: Colorconstand.neutralDarkGrey,),
                                                             const SizedBox(width: 5,),
                                                             Text("REASON".tr(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralDarkGrey),)
                                                          ],
                                                        ),
                                                      ),
                                                      Expanded(child: Text(data[index].admissionTypeName.toString(),style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.end,overflow: TextOverflow.ellipsis,))
                                                    ],
                                                  ),
                                                ),
                                                const Divider(
                                                  height: 0,
                                                  color: Colorconstand.neutralGrey,
                                                )
                                              ],
                                            ),
                                          ),
                                          listImage.isEmpty && listVoice.isEmpty?Container():Container(
                                            margin:const EdgeInsets.only(top: 22),
                                            decoration: BoxDecoration(
                                              color: Colorconstand.neutralWhite,
                                              borderRadius: BorderRadius.circular(18),
                                              boxShadow:[
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.5),
                                                  blurRadius: 30.0, // soften the shadow
                                                  spreadRadius: 2.0, //extend the shadow
                                                  offset:const Offset(2.0,2.0,),
                                                )
                                              ],
                                            ),
                                            child: Column(
                                              children: [
                                                const SizedBox(height: 18,),
                                                Text("ATTACH_REFERRING_FILE".tr(),style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.neutralDarkGrey),textAlign: TextAlign.center,),
                                                const SizedBox(height: 8,),
                                                listImage.isEmpty?Container():Container(
                                                  height: 80,
                                                  child: Center(
                                                    child: ListView.builder(
                                                      shrinkWrap: true,
                                                      scrollDirection: Axis.horizontal,
                                                      itemCount: listImage.length,
                                                      itemBuilder: (context, indexImage) {
                                                        return GestureDetector(
                                                          onTap: () {
                                                            Navigator.push(
                                                              context,
                                                              PageRouteBuilder(
                                                                pageBuilder: (_, __, ___) =>
                                                                    ImageViewDownloads(
                                                                  listimagevide: listImage,
                                                                  activepage: indexImage,
                                                                ),
                                                                transitionDuration:
                                                                    const Duration(seconds: 0),
                                                              ),
                                                            );
                                                          },
                                                          child: Container(
                                                            margin:const EdgeInsets.symmetric(horizontal: 6,vertical: 12),
                                                            child:ClipRRect(
                                                              borderRadius: BorderRadius.circular(10),
                                                              child: Image(
                                                                image: NetworkImage(listImage[indexImage].fileThumbnail.toString()),fit: BoxFit.cover,height: 60,width: 60,
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                ),
                                                listVoice.isEmpty?Container():Divider(),
                                                listVoice.isEmpty?Container():Container(
                                                  margin:const EdgeInsets.only(bottom: 18),
                                                  child: ListView.builder(
                                                    padding:const EdgeInsets.all(0),
                                                    physics:const NeverScrollableScrollPhysics(),
                                                    itemCount: listVoice.length,
                                                    shrinkWrap: true,
                                                    itemBuilder: (context, indexVoice) {
                                                    return Container(
                                                        height: 50,
                                                        margin:const EdgeInsets.symmetric(horizontal: 28),
                                                        width: MediaQuery.of(context).size.width/1.5,
                                                        decoration: BoxDecoration(color: Colorconstand.neutralGrey.withOpacity(0.5),borderRadius:const BorderRadius.all(Radius.circular(10))),
                                                        child: Row(
                                                          children: [
                                                            GestureDetector(onTap:() async {
                                                              setState(() {
                                                                getVoice = false;
                                                              });
                                                              if(isplay == false){
                                                                setState(() {
                                                                  isplay =true;
                                                                  playerController.startPlayer(finishMode: FinishMode.pause);
                                                                });
                                                              }
                                                              else{
                                                                setState(() {
                                                                  isplay =false;
                                                                  playerController.pausePlayer();
                                                                });
                                                              }
                                                              playerController.onPlayerStateChanged.listen((event) {
                                                                if(event.isPlaying){
                                                                    setState(() {
                                                                      isplay =true;
                                                                    },);
                                                                }else if(event.isPaused){
                                                                    setState(() {
                                                                      isplay =false;
                                                                    },);
                                                                }
                                                              });
                                                              playerController.onCompletion.listen((event) {
                                                                setState(() {
                                                                  playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
                                                                  playerController.setRefresh(false);
                                                                });
                                                              });
                                                              playerController.onCurrentDurationChanged.listen((duration) {
                                                                setState(() {
                                                                  playDuration = (playerController.maxDuration~/1000 - (duration~/1000).toInt()).toMMSS();
                                                                },);
                                                              });
                                                              
                                                            } , child: Icon(isplay? Icons.pause_rounded:Icons.play_arrow_rounded,color: Colorconstand.primaryColor,size: 30,)),
                                                            Container(margin:const EdgeInsets.only(right: 5),child: Text(playDuration,style: ThemsConstands.headline_5_medium_16,)),
                                                            Expanded(
                                                              child: AudioFileWaveforms(
                                                                size: Size(MediaQuery.of(context).size.width, 50.0),
                                                                playerController: playerController,
                                                                enableSeekGesture: true,
                                                                waveformType: WaveformType.long,
                                                                waveformData: waveformData,
                                                                playerWaveStyle: const PlayerWaveStyle(
                                                                  scaleFactor: 200,
                                                                  waveCap: StrokeCap.butt,
                                                                  showSeekLine: false,
                                                                  fixedWaveColor: Colorconstand.neutralDarkGrey,
                                                                  liveWaveColor: Colors.blueAccent,
                                                                  spacing: 6,),
                                                                ),
                                                            ),
                                                          ],
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            Container(
                              height: 120,
                              child:data.length <= 1?Container():Container(
                                margin: const EdgeInsets.only(bottom: 20,),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(onPressed: activeindexMyclass ==0?null:(){
                                      playerController.dispose();
                                        _controller.previousPage(duration: const Duration(milliseconds: 200),curve: Curves.easeIn);
                                    }, icon: Icon(Icons.arrow_back_ios_new_rounded,size: 28,color: activeindexMyclass ==0? Colorconstand.neutralDarkGrey: Colorconstand.neutralWhite,)),
                                    Container(
                                      margin:const EdgeInsets.symmetric(horizontal: 8),
                                      child: DotsIndicator(
                                        dotsCount: data.length,
                                        position: activeindexMyclass,
                                        decorator: DotsDecorator(
                                          color: Colorconstand.darkBordersDefault,
                                          activeColor: Colorconstand.mainColorForecolor,
                                          size: const Size(8.0, 8),
                                          activeSize: const Size(12.0, 12.0),
                                          activeShape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(6.0)),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(4.0)),
                                        ),
                                      ),
                                    ),
                                    IconButton(onPressed: activeindexMyclass ==data.length-1?null: (){
                                      playerController.dispose();
                                      _controller.nextPage(duration: const Duration(milliseconds: 300),curve: Curves.easeIn);
                                    }, icon: Icon(Icons.arrow_forward_ios_rounded,size: 28,color: activeindexMyclass == data.length -1? Colorconstand.neutralDarkGrey: Colorconstand.neutralWhite,)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    else{
                      return Expanded(
                        child: Center(
                          child: EmptyWidget(
                            title: "WE_DETECT".tr(),
                            subtitle: "WE_DETECT_DES".tr(),
                          ),
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}