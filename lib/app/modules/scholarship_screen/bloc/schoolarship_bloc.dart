import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/service/api/schoolar_api/request_schoolartype_api.dart';
import 'package:equatable/equatable.dart';
import '../../../models/schoolarship_model/all_schoolarship_model.dart';
import '../../../models/schoolarship_model/type_schoolarship_model.dart';

part 'schoolarship_event.dart';
part 'schoolarship_state.dart';

class SchoolarshipBloc extends Bloc<SchoolarshipEvent, SchoolarshipState> {
  final GetSchoolarClass schoolarShipApi;

  SchoolarshipBloc({required this.schoolarShipApi}) : super(SchoolarshipInitial()) {
    on<GetSchoolarTypeEvent>((event, emit) async {
      try {
        emit(SchoolarshipLoading());
        var data = await schoolarShipApi.getSchoolarTypeApi();
        emit(SchoolarshipLoaded(schoolarModel: data));
      } catch (e) {
        emit(SchoolarshipError(e.toString()));
      }
    });

    on<CreateRequestEvent>((event,emit) async {
      try {
        emit(RequestSchoolarLoading());
        var data = await schoolarShipApi.postAttachRequestApi(
          studentId: event.studentId, 
          classId: event.classId, 
          schoolyearId: event.schoolyearId, 
          typeSchoolar: event.typeSchoolar.toString(),
          attachment: event.attachment,
          description: event.description,
        );
        if(data.status == true){
          emit(RequestSchoolarLoaded(isSuccess: data.status!));
        } else {
          emit(RequestExistStudentState(status: data.status!));
        }
      } catch (e) {
        emit(RequestSchoolarError(e.toString()));
      }
    });
  }
}
