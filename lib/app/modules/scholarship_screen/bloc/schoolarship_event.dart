// ignore_for_file: must_be_immutable

part of 'schoolarship_bloc.dart';

abstract class SchoolarshipEvent extends Equatable {
  const SchoolarshipEvent();
  @override
  List<Object> get props => [];
}

class GetSchoolarTypeEvent extends SchoolarshipEvent{}


class CreateRequestEvent extends SchoolarshipEvent{
  final String studentId;
  final  String classId;
  final String schoolyearId;
  String? typeSchoolar;
  String? description;  
  List<File>? attachment;

  CreateRequestEvent({required this.studentId,required this.classId,required this.schoolyearId,this.typeSchoolar,this.attachment,this.description});
}
