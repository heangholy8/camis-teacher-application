part of 'schoolarship_bloc.dart';

abstract class SchoolarshipState extends Equatable {
  const SchoolarshipState();
  
  @override
  List<Object> get props => [];
}

class SchoolarshipInitial extends SchoolarshipState {}



/// Get type of Schoolarship
class SchoolarshipLoading extends SchoolarshipState{}
class SchoolarshipLoaded extends SchoolarshipState{
  final TypeofSchoolarModel schoolarModel;
  const SchoolarshipLoaded({required this.schoolarModel});
}
class SchoolarshipError extends SchoolarshipState{
  final String error;
  const SchoolarshipError(this.error);
  
}

// Posting Form Schoolarship
class RequestSchoolarLoading extends SchoolarshipState{}

class RequestSchoolarLoaded extends SchoolarshipState{
 final bool isSuccess;
 const RequestSchoolarLoaded({required this.isSuccess});
}
class RequestExistStudentState extends SchoolarshipState{
  final bool status;
  const RequestExistStudentState({required this.status});
}
class RequestSchoolarError extends SchoolarshipState{
  final String error;
  const RequestSchoolarError(this.error);
}

///// Get All Request Student Schoolarship
class GetAllRequestLoading extends SchoolarshipState{}
class GetAllRequestLoaded extends SchoolarshipState{
  final AllSchoolarModel allSchoolarModel;
  const GetAllRequestLoaded({required this.allSchoolarModel});
}
class GetAllRequestError extends SchoolarshipState{
  final String error;
    const GetAllRequestError(this.error);

}