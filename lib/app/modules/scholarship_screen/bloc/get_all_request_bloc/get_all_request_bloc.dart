import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../models/schoolarship_model/all_schoolarship_model.dart';
import '../../../../service/api/schoolar_api/request_schoolartype_api.dart';

part 'get_all_request_event.dart';
part 'get_all_request_state.dart';

class GetAllRequestBloc extends Bloc<GetAllRequestEvent, GetAllRequestState> {
  final GetSchoolarClass schoolarShipApi;
  GetAllRequestBloc({required this.schoolarShipApi}) : super(GetAllRequestInitial()) {
    on<GetRequestEvent>((event, emit) async{
      try {
        emit(GetRequestLoadingState());
        var data = await schoolarShipApi.getAllStudentSchoolarApi();
        emit(GetRequestSuccessState(requestModel: data));
      } catch (e) {
        emit(GetRequestErrorState());
      }
    });
  }
}
