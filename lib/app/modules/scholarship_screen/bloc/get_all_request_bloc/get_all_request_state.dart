part of 'get_all_request_bloc.dart';

abstract class GetAllRequestState extends Equatable {
  const GetAllRequestState();
  
  @override
  List<Object> get props => [];
}

class GetAllRequestInitial extends GetAllRequestState {}

class GetRequestLoadingState extends GetAllRequestState{}

class GetRequestSuccessState extends GetAllRequestState{
  final AllSchoolarModel requestModel;
  const GetRequestSuccessState({required this.requestModel});

}

class GetRequestErrorState extends GetAllRequestState{}


