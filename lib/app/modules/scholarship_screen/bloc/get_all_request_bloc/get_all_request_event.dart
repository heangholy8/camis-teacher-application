part of 'get_all_request_bloc.dart';

abstract class GetAllRequestEvent extends Equatable {
  const GetAllRequestEvent();

  @override
  List<Object> get props => [];
}


class GetRequestEvent extends GetAllRequestEvent{
  
}