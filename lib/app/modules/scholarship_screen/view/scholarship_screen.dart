// ignore_for_file: prefer_is_empty

import 'dart:ui';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/bloc/student_in_class_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:math' as math;
import '../../../../widget/error_request.dart';
import '../../../../widget/top_bar_widget/top_bar_one_line_widget.dart';
import '../../../models/schoolarship_model/all_schoolarship_model.dart';
import '../../../service/api/schoolar_api/request_schoolartype_api.dart';
import '../bloc/get_all_request_bloc/get_all_request_bloc.dart';
import '../widgets/preview_request_bts.dart';
import '../widgets/selectclass_bts.dart';
import '../widgets/selectstudent_bts.dart';

class ScholarshipScreen extends StatefulWidget {
  const ScholarshipScreen({super.key});

  @override
  State<ScholarshipScreen> createState() => _ScholarshipScreenState();
}

class _ScholarshipScreenState extends State<ScholarshipScreen> with Toast {
  final searchHelpController = TextEditingController();
  bool isExpanded = false;
  ScrollController? controller;
  List listClassIsinstructor = [];
  String classObjectType= "";

  @override
  void initState() {
    BlocProvider.of<GetClassBloc>(context).add(GetClass());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: -220,
            left: 0,
            child: Transform.rotate(
              angle: math.pi / 2,
              child: Image.asset(ImageAssets.Path_20)
            ),
          ),
          Positioned(top: 0, right: 0, child: Image.asset(ImageAssets.Path_22)),
          Positioned(bottom: 0,left: 0, child: Image.asset(ImageAssets.Path_21_help)),
          Positioned( bottom: 0, right: 0, child: Image.asset(ImageAssets.Path_19_help)),
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
              child: SafeArea(
                bottom: false,
                child: ListView(
                  controller: controller,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.only( top: 10, bottom: 19, left: 0, right: 0),
                  scrollDirection: Axis.vertical,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 26, right: 26, bottom: 20),
                      child: TopBarOneLineWidget(titleTopBar: "REQUESTCOM".tr()),
                    ),
                  ],
                ),
              ),
            ),
          ),
          BlocListener<GetClassBloc, GetClassState>(
            listener: (context, state) {
              if(state is GetClassLoaded){
                var dataclass = state.classModel!.data;
                setState(() {
                  listClassIsinstructor = dataclass!.teachingClasses!.where((element)=> element.isInstructor == 1).toList();
                });
              }
            },
            child: SafeArea(
              bottom: false,
              child: Align(
                        alignment: Alignment.center,
                        child: BlocBuilder<GetAllRequestBloc, GetAllRequestState>(
                          builder: (context, state) {
                            if(state is GetRequestLoadingState) {
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            } else if(state is GetRequestSuccessState){
                              var data = state.requestModel.data;
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 28.0),
                                child: data!.isEmpty || data.length == 0 ? 
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(ImageAssets.upload_attach_icon,width: 120),
                                      const  SizedBox(height: 12,),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal:38.0),
                                        child: Column(
                                          children: [
                                            Text("NOREQUEST".tr(),style: ThemsConstands.headline3_medium_20_26height.copyWith(fontWeight: FontWeight.w600),),
                                            const  SizedBox(height: 18,),
                                            Text('DESNOREQUEST'.tr(), style: ThemsConstands.headline_4_medium_18.copyWith(color:const Color(0xFF828c95),height: 1.5),textAlign: TextAlign.center,)
                                          ],
                                        ),
                                      )
                                    ],
                                ): exemptedStudentTable(data: state.requestModel),
                              );
                            }
                            else{
                              return ErrorRequestData(
                                onPressed: (){
                                 BlocProvider.of<GetAllRequestBloc>(context).add(GetRequestEvent());
                                },
                                discription: '', 
                                hidebutton: true, 
                                title: 'WE_DETECT_ERROR'.tr(),
                              );
                            }
                          }
                        ),
                      ),
            ),
          ),
          Positioned(
            bottom: 21,
            left: 20,
            right: 20,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal:18.0,),
              child: MaterialButton(
                  onPressed: listClassIsinstructor.length < 2? (){
                    setState(() {
                            classObjectType = listClassIsinstructor[0].name.toString();
                          });
                          showModalBottomSheet(
                            isScrollControlled:true,
                            useSafeArea: false,
                            context: context, 
                            builder: (context) {
                            return FractionallySizedBox(
                            heightFactor: 0.92,
                              child: SelectStudentBottomSheet(classObject:classObjectType.isEmpty? "No" :classObjectType,isInstructor1: 0,));
                            },
                          );
                          BlocProvider.of<GetStudentInClassBloc>(context).add(GetStudentInClass(idClass:listClassIsinstructor[0].id.toString()));
                    }:(){showModalBottomSheet(
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16), 
                          topRight: Radius.circular(16)
                        ),
                      ),
                      context: context,
                      builder: (context) {
                        return const SelectingClassBottomSheet();
                      }
                    );
                  },
                  highlightColor: Colors.transparent,
                    padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 18),
                  color: Colorconstand.primaryColor,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(80.0)),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ImageAssets.ADD_ICON, height: 24,color: Colors.white,),
                      const SizedBox(width: 11),
                      Text("REQUESTING".tr(), style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colors.white,)),
                    ],
                  ),
                ),
              
            ), 
          ), 
        ],
      ),
    ); 
  }

  Widget exemptedStudentTable({required AllSchoolarModel data ,String? translate}){
    return Container(
      margin: const EdgeInsets.only(top: 70),
       child: Column(
         children: [
           Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromRGBO(126, 187, 253, 0.25)),
              margin: const EdgeInsets.only(
                  left: 20, right: 20, bottom: 10),
              padding: const EdgeInsets.only(
                  left: 15, right: 15, bottom: 20, top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 35,
                    child: Text(
                      "N.O".tr(),
                      style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    width: 105,
                    child: Text(
                      "NAME".tr(),
                      style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "CLASS".tr(),
                      style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "REASON".tr(),
                      style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "STATUS".tr(),
                      style: ThemsConstands.headline_5_semibold_16,textAlign: TextAlign.center,
                    ),
                  ),                  
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding:const EdgeInsets.all(0),
                shrinkWrap: true,
                itemCount: data.data!.length,
                itemBuilder: (context, index) {
                  return InkWell(
              onTap: (){
                showModalBottomSheet(
                  context: context,
                  isScrollControlled:true,
                  builder: (context){
                    return  FractionallySizedBox(
                            heightFactor:  0.65 ,
                              child:  PreviewRequestBottomSheet(
                                className: data.data![index].className,
                                studentName:data.data![index].studentName,
                                requestStatus:data.data![index].adminStatus,
                                typeName: data.data![index].typeName,
                                attachMedia: data.data![index].medias?.toList(),
                              ),
                          );
                        });
                      },
                      onLongPress: (){
                        HapticFeedback.vibrate();
                        showAlertLogout(
                          onSubmit: () {
                            GetSchoolarClass().deleteScolarshipList(id: data.data![index].id.toString());
                            BlocProvider.of<GetAllRequestBloc>(context).add(GetRequestEvent());
                            Navigator.of(context).pop();
                          },
                          title: "REALLY_TO_DELETE".tr(),
                          context: context,
                          onSubmitTitle: "DELETE".tr(),
                          bgColorSubmitTitle: Colorconstand.alertsDecline,
                          icon: const Icon(Icons.delete_outline_rounded,size: 38,color: Colorconstand.neutralWhite,),
                          bgColoricon:Colors.red
                          );
                      },
                      child: bodyCardWidget(data.data, index,context),
                    );  
                },
              ),
            )
         ],
       ),
    );

  }
  Widget bodyCardWidget(List<Datum>? data, int index,BuildContext context) {
    var translate = context.locale.toString();
    return Container(
      margin:const EdgeInsets.symmetric(horizontal: 28),
      child: Row(
        children: [
          Container(
            width: 35,
            padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.center,
            child: Text("${index+1}",maxLines: 2,overflow: TextOverflow.ellipsis),
          ),
          Container(
            width: 105,
            height: 52,
            padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.centerLeft,
            child: Text(translate=="km"?"${data![index].studentName}":"${data![index].studentNameEn}".trim().length == 0 ?"${data[index].studentName}":"${data[index].studentNameEn}",maxLines: 2,overflow: TextOverflow.ellipsis),
          ),
          Expanded(
            child: Container(
              height: 52,
              padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
              alignment: Alignment.center,
              child: Text("${data[index].className}"),
            ),
          ),
          Expanded(
            child: Container(
              height: 52,
              padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
              alignment: Alignment.center,
              child: Text("${data[index].typeName}"),
            ),
          ),
          Expanded(
            child: Container(
              height: 42,
              alignment: Alignment.center,
              child: Chip(
                label: Text(data[index].adminStatus==null?"WAITING".tr():data[index].adminStatus==1?"APPROVED".tr():"REJECTED".tr(),style: ThemsConstands.headline_6_semibold_14.copyWith(color: Colors.white),),
                backgroundColor:data[index].adminStatus==null?const Color(0xFFFFDB80):data[index].adminStatus==1? const Color(0xFFA0FF99):const Color(0xFFFFACAC),
              ),
            ),
          ),
        ],
      ),
    );
  }
}






