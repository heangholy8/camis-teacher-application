import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum ExemptionStatus {initial,loading,success,failure}

class ExemptionCubit extends Cubit<ExemptionStatus>{
  ExemptionCubit():super(ExemptionStatus.initial);

  final List<String> exemptedStudentList = [];
  
  void addExemptionCubit({required String exemptedInfo}){
    try {
      emit(ExemptionStatus.loading);
      exemptedStudentList.add(exemptedInfo);
      emit(ExemptionStatus.success);
    } catch (e) {
      emit(ExemptionStatus.failure);
    }
  }
}