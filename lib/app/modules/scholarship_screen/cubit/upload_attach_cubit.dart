import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';

enum AttachSelectStatus { initial, loading, success, failure}

class AttachSelectImageBloc extends Cubit<AttachSelectStatus> {
  AttachSelectImageBloc() : super(AttachSelectStatus.initial);
  List<File> listAttach = [];
  Future<void> listAttachPickerCubit({required List<File> filepicker}) async {
    try {
      emit(AttachSelectStatus.loading);
      listAttach = filepicker;
      emit(AttachSelectStatus.success);
    } catch (e) {
      emit(AttachSelectStatus.failure);
    }
  }

  Future<void> addMoreAttachPickerCubit(
      {required List<File> moreFilePicker}) async {
    try {
      emit(AttachSelectStatus.loading);
      listAttach.addAll(moreFilePicker);
      emit(AttachSelectStatus.success);
    } catch (e) {
      emit(AttachSelectStatus.failure);
    }
  }

  void removeAllItem()async{
    try {
      emit(AttachSelectStatus.loading);
      listAttach = [];
      listAttach.clear();
      emit(AttachSelectStatus.success);
    } catch (e) {
      emit(AttachSelectStatus.failure);
    }
  }

  void removeAttachItem({required int index}) async {
    try {
      emit(AttachSelectStatus.loading);
      listAttach.removeAt(index);
      emit(AttachSelectStatus.success);
    } catch (e) {
      emit(AttachSelectStatus.failure);
    }
  }
}
