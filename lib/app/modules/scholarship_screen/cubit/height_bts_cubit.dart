import 'package:flutter_bloc/flutter_bloc.dart';



class heightBtsCubit extends Cubit<bool> {
  heightBtsCubit() : super(false);
  double heightFactor = 0.92;
  void dropHeightCubit(){
    heightFactor = 0.5;
  }
}
