// ignore_for_file: must_be_immutable, non_constant_identifier_names
import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../cubit/upload_attach_cubit.dart';

class PreviewAttachWidget extends StatefulWidget {
  int imageIndex;

  PreviewAttachWidget({
    required this.imageIndex,
    super.key,
  });

  @override
  State<PreviewAttachWidget> createState() => _PreviewAttachWidgetState();
}

class _PreviewAttachWidgetState extends State<PreviewAttachWidget> {
  int index = 0;
  late PageController pageController;

  final ImagePicker _picker = ImagePicker();
  List<File> listImage = [];

  Future getImageFromCamera() async {
    final PickedFile = await _picker.pickImage(source: ImageSource.camera);
    if (PickedFile != null) {
      setState(() {
        listImage.add(File(PickedFile.path));
        BlocProvider.of<AttachSelectImageBloc>(context).addMoreAttachPickerCubit(moreFilePicker: listImage);
      });
    }
    return true;
  }

  Future getImageFromGallery() async {
    final pickedFiles = await _picker.pickMultiImage(imageQuality: 50);
    if (pickedFiles != null) {
      setState(() {
        for (var pickedFile in pickedFiles) {
          listImage.add(File(pickedFile.path));
          BlocProvider.of<AttachSelectImageBloc>(context)
              .addMoreAttachPickerCubit(moreFilePicker: listImage);
        }
      });
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    pageController = PageController(
      initialPage: widget.imageIndex,
      keepPage: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.black,
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(
                horizontal: 12,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SvgPicture.asset(
                      ImageAssets.chevron_left,
                      height: 28,
                      width: 28,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "${widget.imageIndex} of ${BlocProvider.of<AttachSelectImageBloc>(context).listAttach.length.toString()}",
                    style: ThemsConstands.headline3_medium_20_26height
                        .copyWith(color: Colors.white),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        BlocProvider.of<AttachSelectImageBloc>(context).removeAttachItem(index: index);
                        if (BlocProvider.of<AttachSelectImageBloc>(context).listAttach.isEmpty) {
                          Navigator.pop(context);
                        }
                      });
                    },
                    child: SvgPicture.asset(
                      ImageAssets.trash_icon,
                      color: Colorconstand.screenWhite,
                      height: 28,
                      width: 28,
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: BlocBuilder<AttachSelectImageBloc, AttachSelectStatus>(
                builder: (context, state) {
                  if (state == AttachSelectStatus.loading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state == AttachSelectStatus.success) {
                    var image = BlocProvider.of<AttachSelectImageBloc>(context).listAttach;
                    return PageView.builder(
                        controller: pageController,
                        itemCount: image.length,
                        itemBuilder: ((context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Container(
                              padding: const EdgeInsets.only(top: 20, bottom: 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: InteractiveViewer(
                                boundaryMargin: const EdgeInsets.all(20.0),
                                minScale: 0.1,
                                maxScale: 4.0,
                                child: Image.file(
                                  image[index],
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                          );
                        }),
                        onPageChanged: (value) {
                          setState(() {
                            index = value;
                            widget.imageIndex = value + 1;
                          });
                        });
                  } else {
                    return Container(
                      width: 100,
                      height: 100,
                      color: Colors.black,
                    );
                  }
                },
              ),
            ),
          
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.12,
              padding: const EdgeInsets.only(top: 8),
              decoration: BoxDecoration(
                color: const Color(0xFFD9D9D9).withOpacity(.2),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      getImageFromCamera().then(
                        (value) => pageController.jumpToPage(
                          BlocProvider.of<AttachSelectImageBloc>(context)
                                  .listAttach
                                  .length +
                              1,
                        ),
                      );
                      debugPrint("Take Camera");
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          ImageAssets.camera,
                          width: 34,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "TAKE_PHOTO".tr(),
                          style: ThemsConstands.headline_5_medium_16
                              .copyWith(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 28),
                    width: 0.5,
                    color: const Color(0xFF919EAB).withOpacity(.24),
                  ),
                  InkWell(
                    onTap: () {
                      getImageFromGallery().then(
                        (value) => pageController.jumpToPage(
                          BlocProvider.of<AttachSelectImageBloc>(context)
                                  .listAttach
                                  .length +
                              1,
                        ),
                      );
                      debugPrint("Add Photo");
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          ImageAssets.additem_icon,
                          width: 38,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Add Photo",
                          style: ThemsConstands.headline_5_medium_16
                              .copyWith(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  
  }
}
