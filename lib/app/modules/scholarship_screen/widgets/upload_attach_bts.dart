import 'dart:io';
import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/scholarship_screen/widgets/preview_attach_widget.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../widget/alert_widget/custom_alertdialog.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../grading_screen/widget/attachfile_bottom_widget.dart';
import '../../grading_screen/widget/custom_attach_widget.dart';

import '../bloc/get_all_request_bloc/get_all_request_bloc.dart';
import '../bloc/schoolarship_bloc.dart';
import '../cubit/upload_attach_cubit.dart';

class UploadAttachBottomSheet extends StatefulWidget {
   String? studentName;
   String? className;
   int? exmeptionType;
   final String studentId;
   final String classId;
   final String schoolyearId;
   final String typeSchoolar;
    String? description;
    int isInstructor1;


   
   UploadAttachBottomSheet({super.key,required this.isInstructor1,this.studentName,this.className,this.exmeptionType, required this.studentId, required this.classId, required this.schoolyearId, required this.typeSchoolar,  this.description});

  @override
  State<UploadAttachBottomSheet> createState() => _UploadAttachBottomSheetState();
}

class _UploadAttachBottomSheetState extends State<UploadAttachBottomSheet> with Toast {
  final ImagePicker _picker = ImagePicker();
  List<File> listImage = [];
  bool _isLoading = false;
  
  void getImageFromCamera() async {
    final PickedFile = await _picker.pickImage(source: ImageSource.camera);
    if (PickedFile != null) {
      setState(() {
        listImage.add(File(PickedFile.path));
        BlocProvider.of<AttachSelectImageBloc>(context).listAttachPickerCubit(filepicker: listImage);
      });
    }
  }

  Future getImageFromGallery() async {
    final pickedFiles = await _picker.pickMultiImage();
    if (pickedFiles.isNotEmpty) {
      setState(() {
        pickedFiles.forEach((pickedFile) {
          listImage.add(File(pickedFile.path));
          BlocProvider.of<AttachSelectImageBloc>(context).listAttachPickerCubit(filepicker: listImage);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.only(top: 35),
      color: Colors.white,
      alignment: Alignment.topCenter,
      height: MediaQuery.of(context).size.height,
      padding: const EdgeInsets.symmetric( vertical: 0),
      child: SingleChildScrollView(
        child: BlocListener<SchoolarshipBloc,SchoolarshipState>(
          listener: (context, state) {
            if(state is RequestSchoolarLoading){
                setState(() {
                  _isLoading = true;
                });
            } else if(state is RequestSchoolarLoaded){  
              setState(() {
                _isLoading = false;
                BlocProvider.of<AttachSelectImageBloc>(context).removeAllItem();
                BlocProvider.of<AttachSelectImageBloc>(context).listAttach.clear();
                BlocProvider.of<GetAllRequestBloc>(context).add(GetRequestEvent());
              });
                if(widget.isInstructor1 == 0){
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                }
                else{
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                }
            }else if(state is RequestExistStudentState) {
              showDialog(
                barrierDismissible: false,
                barrierColor: Colors.black38,
                context: context,
                builder: (context) {
                  return CustomAlertDialog(
                    title: "UNSUCCESS".tr(),
                    subtitle:  "EXISTREQUEST".tr(),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    titleButton: 'ត្រលប់'.tr(),
                    buttonColor: Colors.red,
                    titlebuttonColor: Colorconstand.neutralWhite,
                    child: Icon(
                      Icons.cancel,
                      color: Colors.red,
                      size: MediaQuery.of(context).size.width / 6,
                    ),
                  );
                },
              );
               setState(() {
                _isLoading = false;
              });
            }else{
              setState(() {
                _isLoading = false;
              });
            }
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {
                        setState(() {
                          BlocProvider.of<AttachSelectImageBloc>(context).removeAllItem();
                          BlocProvider.of<AttachSelectImageBloc>(context).listAttach.isEmpty;
                           Navigator.pop(context);
                        });
                      },
                    icon: SvgPicture.asset(ImageAssets.chevron_left,width: 34,),
                    ),
                    Text(
                      "ATTACHMENT".tr(),
                      style: ThemsConstands.headline3_semibold_20.copyWith(fontWeight: FontWeight.bold),
                    ),
                    IconButton(onPressed: (){
                      setState(() {
                        BlocProvider.of<AttachSelectImageBloc>(context).removeAllItem();
                          BlocProvider.of<AttachSelectImageBloc>(context).listAttach.isEmpty;
                      });
                      if(widget.isInstructor1 == 0){
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      }
                      else{
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      }
                    }, 
                    icon: SvgPicture.asset(ImageAssets.CLOSE_ICON,color: Colorconstand.lightTrunks,)),
                  ],
                ),
              ),
              
              Padding(
                padding: const EdgeInsets.only(left:18.0,right: 18.0, bottom: 8,top:4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [   
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child:  Container(
                        decoration:const BoxDecoration(
                      color: Colorconstand.primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(16))
                    ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(widget.className ?? "No",textAlign: TextAlign.center,style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralWhite)),
                        ),
                      ),
                    ),
                    const SizedBox(width: 12), 
                    Text(widget.studentName??"Empty", style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center,),
                  ],
                ),
              ),
              Text(widget.exmeptionType==0 ? "POVERTY".tr() : widget.exmeptionType==1?"TEACHERCHILD".tr():widget.exmeptionType ==2?"SCOUT".tr():"CLASSMONITOR".tr(), style: ThemsConstands.headline3_medium_20_26height.copyWith(color:const Color(0xFF696969)),textAlign: TextAlign.center,),
              const SizedBox(height: 28 ),
              Text(
                "ATTACHMENTTYPE".tr(),
                style: ThemsConstands.headline4_regular_18.copyWith(
                  fontWeight: FontWeight.w600,
                  color: Colorconstand.darkTextsDisabled),
              ),
              const SizedBox(
                height: 20,
              ),
              BlocBuilder<AttachSelectImageBloc,AttachSelectStatus>(
                builder: (context, state) {
                  if (state == AttachSelectStatus.loading) {
                    return const Center(
                      child: CircularProgressIndicator()
                    );
                  } else if (state == AttachSelectStatus.success) {
                    var image = BlocProvider.of<AttachSelectImageBloc>(context).listAttach;
                    return image.isNotEmpty
                        ? Container(
      
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            height: 265,
                            child: ListView.builder(
                              shrinkWrap: true,
                              
                              scrollDirection: Axis.horizontal,
                              itemCount: image.length + 1,
                              itemBuilder: (context, index) {
                                return index != image.length
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                        child: DottedBorder(
                                          color: Colorconstand.neutralGrey,
                                          dashPattern: const <double>[8, 8],
                                          padding: const EdgeInsets.all(5),
                                          radius: const Radius.circular(12),
                                          borderType: BorderType.RRect,
                                          strokeWidth: 1,
                                          borderPadding: const EdgeInsets.all(1),
                                          child: InkWell(
                                            onTap: () {
                                              Navigator.push( context,
                                                animtedRouteBottomToTop(
                                                  topage: PreviewAttachWidget(imageIndex: index),
                                                ),
                                              );
                                            },
                                            child: Stack(
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:BorderRadius.circular(90),
                                                    image: DecorationImage(image: FileImage(
                                                    image[index],
                                                  ),
                                                  )
                                                  ),
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(10.0),
                                                    child: Image.file(
                                                      image[index],
                                                      width: 150,
                                                      height: 250,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  width:150 ,
                                                  decoration: BoxDecoration(
                                                    color: Colors.black.withOpacity(0.4),
                                                    borderRadius: BorderRadius.circular(8.0)
                                                  ),
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment:CrossAxisAlignment.center,
                                                    children: [
                                                      SvgPicture.asset( ImageAssets.eye_icon, color: Colors.white,
                                                      ),
                                                      const SizedBox( height: 10),
                                                      Text(
                                                        "PREVIEW".tr(),
                                                        style: ThemsConstands.headline6_medium_14.copyWith(
                                                          color: Colorconstand.lightBackgroundsWhite,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : imageOptoinWidget(
                                        onCamera: () {
                                          getImageFromCamera();
                                        },
                                        onGallery: () {
                                          getImageFromGallery();
                                        },
                                      );
                              },
                            ),
                          )
                        : imageOptoinWidget(
                            onCamera: () {
                              getImageFromCamera();
                            },
                            onGallery: () {
                              getImageFromGallery();
                            },
                          );
                  } else {
                    return imageOptoinWidget(
                      onCamera: () {
                        getImageFromCamera();
                      },
                      onGallery: () {
                        getImageFromGallery();
                      },
                    );
                  }
                },
              ),
              const SizedBox(height: 18),
              const Divider(),  
              const SizedBox(height: 8),
            _isLoading == false ?  MaterialButton(
                height: 57.18,
                minWidth: MediaQuery.of(context).size.width-40,
                onPressed: BlocProvider.of<AttachSelectImageBloc>(context).listAttach.isNotEmpty? () {
                  BlocProvider.of<SchoolarshipBloc>(context)
                    .add(CreateRequestEvent(
                      studentId: widget.studentId,
                      classId: widget.classId, 
                      schoolyearId: widget.schoolyearId,
                      typeSchoolar: widget.typeSchoolar,
                      description: widget.description,
                      attachment: BlocProvider.of<AttachSelectImageBloc>(context).listAttach.toList(),
                    ),
                  );

                  
                  // BlocProvider.of<ExemptionCubit>(context).exemptedStudentList.add("value");
                  // setState(() {
                  //     BlocProvider.of<AttachSelectImageBloc>(context).removeAllItem();
                  //     BlocProvider.of<AttachSelectImageBloc>(context).listAttach.clear();
                  // });
                
                  // Navigator.of(context).pop();
                  // Navigator.of(context).pop();
                  // Navigator.of(context).pop();
                  // Navigator.of(context).pop();
                
                }:null,
                disabledColor: Colorconstand.mainColorUnderlayer,
                color:  Colorconstand.primaryColor,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.5)),
                child: Text(
                  "REQUESTINGTOCOMPANY".tr(),
                  style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.neutralWhite),
                ),
              ):const CircularProgressIndicator(),
            ],
          ),
        ),
      ),
    );

  }
  Widget imageOptoinWidget({required Function()? onCamera, required Function()? onGallery}) {
    return DottedBorder(
      color: Colorconstand.neutralGrey,
      dashPattern: const <double>[8, 4],
      padding: const EdgeInsets.all(10),
      radius: const Radius.circular(12),
      borderType: BorderType.RRect,
      strokeWidth: 1.5,
      borderPadding: const EdgeInsets.all(1),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: onCamera,
            child: CustomAttachListWidget(
              title: "CAMERA".tr(),
              svg: ImageAssets.camera,
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          InkWell(
            onTap: onGallery,
            child: CustomAttachListWidget(
              title: "GALLERY".tr(),
              svg: ImageAssets.gallery,
            ),
          ),
        ],
      ),
    );
  }

}