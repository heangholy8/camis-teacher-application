// ignore_for_file: unrelated_type_equality_checks

import 'package:camis_teacher_application/app/models/exam_model/post_exam_date.dart';
import 'package:camis_teacher_application/app/models/schoolarship_model/all_schoolarship_model.dart';
import 'package:camis_teacher_application/app/modules/scholarship_screen/widgets/preview_result_widget.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';


class PreviewRequestBottomSheet extends StatelessWidget {
   String? className;
   String? studentName;
   String? typeName;
   List<Media>? attachMedia;
   dynamic requestStatus;
   PreviewRequestBottomSheet({super.key,this.requestStatus, this.className, this.studentName, this.typeName , this.attachMedia});

  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Container(
      color: Colors.white,
      alignment: Alignment.topCenter,
      height: MediaQuery.of(context).size.height,
      padding: const EdgeInsets.only( top: 24),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(width: 50,),
                Text(
                  "ATTACHMENT".tr(),
                  style: ThemsConstands.headline3_semibold_20.copyWith(fontWeight: FontWeight.bold),
                ),
                 IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.close),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left:18.0,right: 18.0, bottom: 8,top:4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [   
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colorconstand.primaryColor,
                        borderRadius: BorderRadius.circular(18)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
                        child: Text(className ?? "NO",textAlign: TextAlign.center,style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.neutralWhite)),
                      ),
                    ),
                  ),
                  const SizedBox(width: 12), 
                  Text(studentName?? "NONAME".tr(), style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center,),
                ],
              ),
            ),
            Text(typeName??"UNKNOWN".tr(), style: ThemsConstands.headline3_medium_20_26height.copyWith(color:const Color(0xFF696969)),textAlign: TextAlign.center,),
            const SizedBox(height: 28 ),
            Text(
              "ATTACHMENTTYPE".tr(),
              style: ThemsConstands.headline4_regular_18.copyWith(
                fontWeight: FontWeight.w600,
                color: Colorconstand.darkTextsDisabled),
            ),
            const SizedBox(
              height: 20,
            ),
             Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              height: 265,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount:  attachMedia!.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: DottedBorder(
                      color: Colorconstand.neutralGrey,
                      dashPattern: const <double>[8, 8],
                      padding: const EdgeInsets.all(5),
                      radius: const Radius.circular(12),
                      borderType: BorderType.RRect,
                      strokeWidth: 1,
                      borderPadding: const EdgeInsets.all(1),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>PreviewResultWidget(imageIndex:index,listImage:attachMedia!)));
                        },
                        child: Stack(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:BorderRadius.circular(90),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Container(),
                              ),
                            ),
                            Container(
                              width:150 ,
                              decoration: BoxDecoration(
                                image: DecorationImage(image: NetworkImage(attachMedia![index].fileThumbnail.toString()),fit: BoxFit.cover),
                                color: Colors.black.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(8.0)
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                      
                },
              ),
            ) ,
            const SizedBox(height: 18),
            const Divider(),  
            const SizedBox(height: 8),

    requestStatus == null ?
        Container(
          width: MediaQuery.of(context).size.width,
          height: 70,
          color: Colorconstand.alertsAwaitingText,
          padding: const EdgeInsets.all(9),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(ImageAssets.more_circle),
              const SizedBox(width: 14,),
              Text("Awaiting Approve",style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colors.white,fontWeight: FontWeight.w600),),
            ],
          ),
        ): requestStatus == 1 ?
            Container(
              width: MediaQuery.of(context).size.width,
              height: 250,
              color: Colorconstand.alertsNotifications,
              padding: const EdgeInsets.all(9),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 70,
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SvgPicture.asset(ImageAssets.reject_icon),
                        const SizedBox(width: 14,),
                        Text("Rejected",style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colors.white,fontWeight: FontWeight.w600),),
                      ],
                    ),
                  ),
                  Text("Reason ៖",style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colors.white,fontWeight: FontWeight.w600),),
                  const SizedBox(height: 4,),
                  Text("ឯកសារយោងមិនត្រឹមត្រូវ។ សូមបង្កើតសំណើរថ្មី ដោយភ្ជាប់ប័ណ្ណយ៊ូវណ្ណដែលចេញដោយនាយកសាលា អោយបានត្រឹមត្រូវ។",style: ThemsConstands.headline4_regular_18.copyWith(color: Colors.white),)
                ],
              ),
            ):Container(),
         
          
          ],
        ),
      ),
    );

  }
}