
import 'package:camis_teacher_application/app/core/resources/asset_resource.dart';
import 'package:camis_teacher_application/app/modules/scholarship_screen/bloc/schoolarship_bloc.dart';
import 'package:camis_teacher_application/app/modules/scholarship_screen/widgets/selectstudent_bts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shimmer/shimmer.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/thems/thems_constands.dart';
import '../../student_list_screen/bloc/student_in_class_bloc.dart';
import '../cubit/height_bts_cubit.dart';

class SelectingClassBottomSheet extends StatefulWidget {
  const SelectingClassBottomSheet({super.key});

  @override
  State<SelectingClassBottomSheet> createState() => _SelectingClassBottomSheetState();
}

class _SelectingClassBottomSheetState extends State<SelectingClassBottomSheet> {
  String classObjectType = "";

  @override
  void initState() {
    super.initState();
    
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return  Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal:18.0,vertical: 14),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [   
              IconButton(onPressed: null, icon: Container(),),
              Text("CHOOSECLASS".tr(),style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center,),
              IconButton(onPressed: (){
                Navigator.pop(context);
              
              }, icon: SvgPicture.asset(ImageAssets.CLOSE_ICON,color: Colorconstand.lightTrunks,)),
            ],
          ),
        ),
        Expanded(
          child: BlocBuilder<GetClassBloc, GetClassState>(
            builder: (context, state) {
              if(state is GetClassLoading){
                return shimmerLoading();
              }
              else if(state is GetClassLoaded){
                if (state.classModel!.data!.isCurrent == true) { 
                  var data = state.classModel!.data;
                  var listClassIsinstructor = data!.teachingClasses!.where((element)=> element.isInstructor == 1).toList();
                  return  ListView.builder(
                    shrinkWrap: true,
                    itemCount:listClassIsinstructor.length,
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context,index) {
                      return  InkWell(
                        onTap: (){ 
                          setState(() {
                            classObjectType = listClassIsinstructor[index].name.toString();
                          });
                          showModalBottomSheet(
                            isScrollControlled:true,
                            useSafeArea: false,
                            context: context, 
                            builder: (context) {
                            return FractionallySizedBox(
                            heightFactor: 0.92,
                              child: SelectStudentBottomSheet(classObject:classObjectType.isEmpty? "No" :classObjectType,isInstructor1: 1,));
                            },
                          );
                          BlocProvider.of<GetStudentInClassBloc>(context).add(GetStudentInClass(idClass:listClassIsinstructor[index].id.toString()));
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical:14.0,horizontal: 21),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      CircleAvatar(
                                        radius: 30,
                                        child: Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: Text( listClassIsinstructor[index].name.toString(), textAlign: TextAlign.center,style: ThemsConstands.headline3_semibold_20,),
                                        ),
                                      ),
                                      const SizedBox(width: 18,),
                                      Column(
                                        children:[
                                          Row(
                                            children:[
                                              Text(" ${ translate == "km"? "ថ្នាក់ ${listClassIsinstructor[index].name}": listClassIsinstructor[index].nameEn}",style: ThemsConstands.headline3_medium_20_26height.copyWith(fontWeight: FontWeight.w900),),
                                              const SizedBox(width: 14,),
                                              listClassIsinstructor[index].isInstructor == 1 ? SvgPicture.asset(ImageAssets.ranking_icon):Container(),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 10,),
                            Container(
                              margin: const EdgeInsets.only(left: 90),
                              width: MediaQuery.of(context).size.width,
                              height: 1,
                              color: Colors.black.withOpacity(.12),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                }
              }
              return Container();
            },
          ),
        ),
      ],
    );
  
  }

  Widget _buildTextFormField() {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18.0),
        child: TextFormField(
          onChanged: (value) {
          },
          maxLines: 1,
          style: ThemsConstands.subtitle1_regular_16,
          decoration: InputDecoration(
            prefixIcon: Padding(
              padding: const EdgeInsets.all(14.0),
              child: SvgPicture.asset(
                ImageAssets.search_icon,
                width: 18,
                height: 18,
              ),
            ),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(color: Color(0xFFE5E5E5)),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(color: Color(0xFFE5E5E5)),
            ),
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
            hintText: "SEACHSTUDENT".tr(),
            hintStyle: ThemsConstands.subtitle1_regular_16.copyWith(
              color: const Color(0xFFE4E4E4),
            ),
          ),
        ),
      );
    
  }

  Widget shimmerLoading() {
    return Shimmer.fromColors(
      baseColor:Colorconstand.baseColor,
      highlightColor:Colorconstand.highlightColor,
      child: Column(
        children: [
          Container(
            height: 68,
            width: MediaQuery.of(context).size.width-40,
            decoration: BoxDecoration(
              color: Colorconstand.primaryColor,
              borderRadius: BorderRadius.circular(8)
            ),
            child:const Text("sdf"),
          ),
          const SizedBox(height: 18,),
          Container(
            height: 68,
            width: MediaQuery.of(context).size.width-40,
            decoration: BoxDecoration(
              color: Colorconstand.primaryColor,
              borderRadius: BorderRadius.circular(8)
            ),
            child:const Text("sdf"),
          ),
          const SizedBox(height: 18,),
           Container(
            height: 68,
            width: MediaQuery.of(context).size.width-40,
            decoration: BoxDecoration(
              color: Colorconstand.primaryColor,
              borderRadius: BorderRadius.circular(8)
            ),
            child:const Text("sdf"),
          ),
          const SizedBox(height: 18,),
        ],
      ),
    );
  }
 
}


