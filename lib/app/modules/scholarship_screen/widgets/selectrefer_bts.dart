import 'package:camis_teacher_application/app/modules/scholarship_screen/widgets/upload_attach_bts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../bloc/schoolarship_bloc.dart';

class SelectReferenceBottomsheet extends StatelessWidget {
   String? classGrade;
   String? studentName;
   String studentId;
   String classId;
   String schoolYearId;
   int isInstructor1;

   SelectReferenceBottomsheet({
    super.key,
     this.classGrade,
     this.studentName,
     required this.studentId,
     required this.classId,
     required this.schoolYearId,required this.isInstructor1
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal:18.0,vertical: 14),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [   
                InkWell(
                  onTap: (){
                    Navigator.pop(context);
               
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(ImageAssets.chevron_left,width: 34,),
                    ],
                  ),
                ),
                  Text("SELECTREFER".tr(),style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center,),
                  IconButton(onPressed: isInstructor1 == 0?  (){
                    Navigator.pop(context);
                    Navigator.pop(context);
                  }:(){
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);
                  }, 
                  icon: SvgPicture.asset(ImageAssets.CLOSE_ICON,color: Colorconstand.lightTrunks,)),
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left:18.0,right: 18.0, bottom: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [   
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      decoration:const BoxDecoration(
                        color: Colorconstand.primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(16))
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(classGrade ?? "No",textAlign: TextAlign.center,style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite)),
                      ),
                    ),
                  ),
                  const SizedBox(width: 12), 
                  Text(studentName??"Empty", style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center,),
                ],
              ),
            ),
          ),
          Expanded(
            child: BlocBuilder<SchoolarshipBloc, SchoolarshipState>(
              builder: (context, state) {
                if(state is SchoolarshipLoading){
                  return Container();
                }else if(state is SchoolarshipLoaded){
                  var data = state.schoolarModel.data;
                  return ListView.builder(
                    shrinkWrap: true,
                    itemCount: data!.length,
                    itemBuilder: (context,index) {
                      return ListTile(
                          onTap: (){
                            showModalBottomSheet(
                              enableDrag: false,
                              isDismissible:false,
                              isScrollControlled: true,
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
                              ),
                              
                              context: context,
                              builder: (context) {
                                return  UploadAttachBottomSheet(
                                  isInstructor1: isInstructor1,
                                  studentId: studentId,
                                  classId: classId,
                                  schoolyearId: schoolYearId,
                                  typeSchoolar: data[index].id.toString(),
                                  className: classGrade,
                                  studentName: studentName,
                                  exmeptionType: index,
                                );
                              }
                            );
                          },
                          tileColor: Colors.white,
                          contentPadding:const EdgeInsets.symmetric(horizontal: 25), 
                          shape: Border(top:const BorderSide(color: Color(0xFFE2E2E2)),
                          bottom: BorderSide(width: index== 3 ? 0:0)
                        ),
                        leading: SvgPicture.asset( data[index].id == 1?ImageAssets.personalcard_icon:data[index].id == 2?ImageAssets.teacherhat_icon:data[index].id == 3?ImageAssets.medal_icon:data[index].id == 4?ImageAssets.star_icon:ImageAssets.CLOSE_ICON,color: Colors.black,),
                        title: Text(data[index].id == 1 ? "POVERTY".tr() : data[index].id == 2 ?"TEACHERCHILD".tr():data[index].id == 3 ?"SCOUT".tr(): data[index].id == 4?"CLASSMONITOR".tr():"UNKNOWN".tr(),style: ThemsConstands.headline_4_medium_18.copyWith(fontWeight: FontWeight.w600),),
                      );
                    }
                  );
                }else{
                  return Container();
                }
              },
            ),
          )
      ],
    );
  }
}