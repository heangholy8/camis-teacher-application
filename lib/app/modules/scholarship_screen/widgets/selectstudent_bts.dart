import 'dart:ui';

import 'package:camis_teacher_application/app/modules/scholarship_screen/widgets/selectrefer_bts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../core/constands/color_constands.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../student_list_screen/bloc/student_in_class_bloc.dart';
import '../bloc/schoolarship_bloc.dart';

class SelectStudentBottomSheet extends StatefulWidget {
  final String classObject ;
  final int isInstructor1;

  const SelectStudentBottomSheet({super.key,required this.classObject,required this.isInstructor1});

  @override
  State<SelectStudentBottomSheet> createState() => _SelectStudentBottomSheetState();
}

class _SelectStudentBottomSheetState extends State<SelectStudentBottomSheet> {

@override
  void initState() {


    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal:18.0,vertical: 14),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [   
            InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Row(
                children: [
                  SvgPicture.asset(ImageAssets.chevron_left,width: 34,),
                  const SizedBox(width: 8),
                  Container(
                    decoration:const BoxDecoration(
                      color: Colorconstand.primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(16))
                    ),
                    child: Padding(
                      padding:  const EdgeInsets.all(4.0),
                      child: Text(widget.classObject, textAlign: TextAlign.center,style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                    ),
                  ),
                ],
              ),
            ),
              Text("CHOOSESTUDENT".tr(),style: ThemsConstands.headline3_semibold_20,textAlign: TextAlign.center,),
              IconButton(onPressed: widget.isInstructor1 == 0? (){
                Navigator.pop(context);
              }:(){
                Navigator.pop(context);
                Navigator.pop(context);
              }, 
              icon: SvgPicture.asset(ImageAssets.CLOSE_ICON,color: Colorconstand.lightTrunks,)),
            ],
          ),
        ),
        const SizedBox(height: 8,),
        // _buildTextFormField(),
        Expanded(
          child: BlocBuilder<GetStudentInClassBloc, GetClassState>(
            builder: (context, state) {
              if(state is GetStudentClassLoading){
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              else if(state is GetStudentInClassLoaded){
                var data = state.studentinclassModel!.data;
                return ListView.separated(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(0),
                  itemCount: state.studentinclassModel!.data!.length,
                  itemBuilder: (context, indexstu) {
                    return MaterialButton(
                      onPressed: () { 
                        BlocProvider.of<SchoolarshipBloc>(context).add(GetSchoolarTypeEvent());
                        showModalBottomSheet(
                          context: context, 
                          builder: (context) {
                            return SelectReferenceBottomsheet(
                              isInstructor1: widget.isInstructor1,
                              studentName: state.studentinclassModel!.data![indexstu].name.toString(),
                              classGrade: widget.classObject.toString(),
                              studentId: state.studentinclassModel!.data![indexstu].studentId.toString(),
                              classId: state.studentinclassModel!.data![indexstu].classId.toString(),
                              schoolYearId:  state.studentinclassModel!.data![indexstu].schoolyear.toString(),
                            );
                          }
                        );                            
                      },
                      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 22),
                      child: Row(
                        children:  [
                         SizedBox(
                          width: 70,
                          height: 70,
                          child: ClipOval(
                            child: Image.network(
                                data?[indexstu].profileMedia!.fileShow == "" ||
                                data![indexstu] .profileMedia!.fileShow == null
                                    ? data![indexstu].profileMedia! .fileThumbnail.toString()
                                    : data[indexstu].profileMedia!.fileShow.toString(),
                              
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        const SizedBox(width: 18),
                        Text("${translate =="km" ? data[indexstu].name : "${data[indexstu].nameEn}".trim().isEmpty?"No Name":data[indexstu].nameEn}",style: ThemsConstands.headline_4_medium_18.copyWith(fontWeight: FontWeight.w600),),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Container(
                        margin: const EdgeInsets.only(left: 110),
                        child: const Divider(
                          height: 1,
                          thickness: 0.5,
                        ),
                    );
                  },
                );
              }else{
                return Container();
              }
            },
          ),
        ) ,  
        const SizedBox(height: 25),
      ],
    );  
  }
  //  Widget _buildTextFormField() {
  //     return Padding(
  //       padding: const EdgeInsets.symmetric(horizontal: 18.0),
  //       child: TextFormField(
  //         onChanged: (value) {
  //         },
  //         maxLines: 1,
  //         style: ThemsConstands.subtitle1_regular_16,
  //         decoration: InputDecoration(
  //           prefixIcon: Padding(
  //             padding: const EdgeInsets.all(14.0),
  //             child: SvgPicture.asset(
  //               ImageAssets.search_icon,
  //               width: 18,
  //               height: 18,
  //             ),
  //           ),
  //           enabledBorder: const OutlineInputBorder(
  //             borderRadius: BorderRadius.all(Radius.circular(12.0)),
  //             borderSide: BorderSide(color: Color(0xFFE5E5E5)),
  //           ),
  //           focusedBorder: const OutlineInputBorder(
  //             borderRadius: BorderRadius.all(Radius.circular(12.0)),
  //             borderSide: BorderSide(color: Color(0xFFE5E5E5)),
  //           ),
  //           contentPadding:
  //               const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
  //           hintText: "SEACHSTUDENT".tr(),
  //           hintStyle: ThemsConstands.subtitle1_regular_16.copyWith(
  //             color: const Color(0xFFE4E4E4),
  //           ),
  //         ),
  //       ),
  //     );
  // }
            
}
