// ignore_for_file: must_be_immutable, non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../models/schoolarship_model/all_schoolarship_model.dart';

class PreviewResultWidget extends StatefulWidget {
  int imageIndex;
  List<Media>? listImage;

  PreviewResultWidget({
    required this.imageIndex,
     this.listImage,
    super.key,
  });

  @override
  State<PreviewResultWidget> createState() => _PreviewResultWidgetState();
}

class _PreviewResultWidgetState extends State<PreviewResultWidget> {
  int index = 0;
  late PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController(
      initialPage: widget.imageIndex,
      keepPage: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.black,
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(
                horizontal: 12,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SvgPicture.asset(
                      ImageAssets.chevron_left,
                      height: 28,
                      width: 28,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "${widget.imageIndex} of ${widget.listImage!.length}",
                    style: ThemsConstands.headline3_medium_20_26height.copyWith(color: Colors.white),
                  ),
                  const SizedBox(height: 28, width: 28),
                ],
              ),
            ),
            Expanded(
              child: PageView.builder(
                controller: pageController,
                itemCount: widget.listImage?.length,
                itemBuilder: ((context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      padding: const EdgeInsets.only(top: 20, bottom: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: InteractiveViewer(
                        boundaryMargin: const EdgeInsets.all(20.0),
                        minScale: 0.1,
                        maxScale: 4.0,
                        child: Image.network(
                         "${widget.listImage?[index].fileShow}",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  );
                }),
                onPageChanged: (value) {
                  setState(() {
                    index = value;
                    if(widget.imageIndex==0){
                      widget.imageIndex = 1;
                    }
                     widget.imageIndex = value + 1;
                    print(" asdfadsf $value");
                  });
                }),
            ),
          ],
        ),
      ),
    );
  
  }
}
