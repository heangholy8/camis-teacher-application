// ignore_for_file: constant_identifier_names

abstract class Routes {
  Routes._();

  static const SPLAHSSCREEN = _Paths.SPLASH;
  static const HOMESCREEN = _Paths.HOME;
  // static const MORESCREEN = _Paths.MORE;
  static const STUDENTLISTSCREEN = _Paths.STUDENTLIST;
  static const ANNOUNCEMENT = _Paths.ANNOUNCEMENT;
  static const NOTIFICATION = _Paths.NOTIFICATION;
  static const HELP = _Paths.HELP;
  static const ABOUT = _Paths.ABOUT;
  static const SCHEDULEEXPANDED = _Paths.SCHEDULEEXPANDED;
  static const EXAMDETAIL = _Paths.EXAMDETAIL;
  static const ATTENDANCESCREEN = _Paths.ATTENDANCE;
  static const GRADINGSCREEN = _Paths.GRADING;
  static const GRADINGSCREENOFFLINE = _Paths.GRADINGOFFLINE;
  static const VERIFICATIONSCREEN = _Paths.VERIFYOTP;
  static const RESULTSCREEN = _Paths.RESULT;
  static const OPTIONLOGINSCREEN = _Paths.LOGIN;
  static const LOGINENTRYSCREEN = _Paths.LOGINCODE;
  static const LOGINQRCODESCREEN = _Paths.LOGINQRCODE;
  static const SCHEDULEEXAM = _Paths.SCHEDULEEXAM;
  static const TEACHERLISTSCREEN = _Paths.TEACHERLIST;
  static const PROFILESCREEN = _Paths.PROFILE;
  static const REPORTSCREEN = _Paths.REPORT;
  static const COMFIRMATIONSCREEN = _Paths.COMFIRMATION;
  static const SETMONITIORSCREEN = _Paths.SETMAZZER;
  static const PREVIEWFILE = _Paths.VIEWFILE;
  static const TEACHERFINANCES = _Paths.TEACHERFINANCE; 
  static const STUDENTFEELIST = _Paths.STUDENTFEELIST;
  static const VIEWREQUESTPERMISSION = _Paths.VIEWREQUESTPERMISSION;

}

abstract class _Paths {
  static const SPLASH = '/splash';
  static const MORE = '/more_screen';
  static const STUDENTLIST = '/student_list_screen';
  static const ANNOUNCEMENT = '/announcement';
  static const NOTIFICATION = '/notification';
  static const HOME = '/';
  static const ABOUT = '/about';
  static const HELP = '/help';
  static const SCHEDULEEXPANDED = '/scheduled_expanded';
  static const EXAMDETAIL = '/exam_detail';
  static const ATTENDANCE = '/attendance';
  static const GRADING = '/grading';
  static const GRADINGOFFLINE = '/grading_offline';
  static const RESULT = '/result';
  static const LOGIN = '/login';
  static const VERIFYOTP = '/login_otp';
  static const LOGINCODE = '/login_code';
  static const LOGINQRCODE = '/login_qr_code';
  static const SCHEDULEEXAM = '/attedndace_schedule';
  static const TEACHERLIST = '/teacher_list';
  static const PROFILE = '/profile_screen';
  static const REPORT = '/report';
  static const COMFIRMATION = '/comfirmation_screen';
  static const SETMAZZER = '/setmonitor_screen';
  static const DOTASK = '/do_task';
  static const VIEWFILE = '/image_show';
  static const TEACHERFINANCE = '/teacher_finance';
  static const STUDENTFEELIST = '/student_fee';
  static const VIEWREQUESTPERMISSION = '/view_permission_request';
}
