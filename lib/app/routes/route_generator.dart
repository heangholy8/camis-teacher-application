import 'package:camis_teacher_application/app/modules/account_confirm_screen/view/account_confirm_screen.dart';
import 'package:camis_teacher_application/app/modules/attendance_schedule_screen/view/attendance_schedule_and_exam_screen.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/widget/preview_file_widget.dart';
import 'package:camis_teacher_application/app/modules/home_screen/view/home_screen.dart';
import 'package:camis_teacher_application/app/modules/about_screen/view/about_screen.dart';
import 'package:camis_teacher_application/app/modules/login_screen/login_screen.dart';
import 'package:camis_teacher_application/app/modules/login_screen/views/code_entry_screen.dart';
import 'package:camis_teacher_application/app/modules/create_announcement_screen/view/create_activities_screen.dart';
import 'package:camis_teacher_application/app/modules/notification_screen/view/notification_screen.dart';
import 'package:camis_teacher_application/app/modules/profile_user/profile_user_screen.dart';
import 'package:camis_teacher_application/app/modules/report_screen/view/report_screen.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/view/grading_screen.dart';
import 'package:camis_teacher_application/app/modules/grading_screen/view/result_screen.dart';
import 'package:camis_teacher_application/app/modules/set_mazer_screen/views/set_mazzer_screen.dart';
import 'package:camis_teacher_application/app/modules/splash_screen/splash_screen.dart';
import 'package:camis_teacher_application/app/modules/student_list_screen/view/list_student_screen.dart';
import 'package:camis_teacher_application/app/modules/teacher_list_screen/view/teacher_list_screen.dart';
import 'package:flutter/material.dart';
import '../modules/login_screen/views/scan_qr_screen.dart';
import '../modules/teacher_commission_screen/views/list_student_fee_screen.dart';
import '../modules/teacher_commission_screen/views/teacher_finance_screen.dart';
import '../modules/view_request_permission_screen/view/view_request_permission_screen.dart';
import 'app_route.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed

    switch (settings.name) {
      case Routes.HOMESCREEN:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
      case Routes.SPLAHSSCREEN:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      // case Routes.MORESCREEN:
      //   return MaterialPageRoute(builder: (_) => const MoreScreen());
      case Routes.STUDENTLISTSCREEN:
        return MaterialPageRoute(builder: (_) => const LedgerStudentListScreen());
      case Routes.NOTIFICATION:
        return MaterialPageRoute(builder: (_) => const NotifictaionScreen());
      case Routes.ANNOUNCEMENT:
        return MaterialPageRoute(builder: (_) =>  CreateActivityScreen(update: false,));
      case Routes.ABOUT:
        return MaterialPageRoute(builder: (_) => const AboutScreen());
      // case Routes.EXAMDETAIL:
      //   return MaterialPageRoute(builder: (_) => const ExamDetailScreen());
      // case Routes.ATTENDANCESCREEN:
      //   return MaterialPageRoute(builder: (_) => const AttendanceEntryScreen());
      case Routes.GRADINGSCREEN:
        return MaterialPageRoute(builder: (_) =>  GradingScreen());
      case Routes.RESULTSCREEN:
        return MaterialPageRoute(builder: (_) => const ResultScreen());
      case Routes.OPTIONLOGINSCREEN:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      // case Routes.VERIFICATIONSCREEN:
      //   return MaterialPageRoute(builder: (_) => InputPhoneNumScreen());
      case Routes.LOGINENTRYSCREEN:
        return MaterialPageRoute(builder: (_) => const LoginEntryScreen());
      case Routes.LOGINQRCODESCREEN:
        return MaterialPageRoute(builder: (_) => QrCodeLoginScreen());
      case Routes.COMFIRMATIONSCREEN:
        return MaterialPageRoute(builder: (_) => AccountConfirmScreen());
      case Routes.SCHEDULEEXAM:
        return MaterialPageRoute(
            builder: (_) => AttendacneScheduleScreen());
      case Routes.TEACHERLISTSCREEN:
        return MaterialPageRoute(builder: (_) => const TeacherListScreen());
      case Routes.PROFILESCREEN:
        return MaterialPageRoute(builder: (_) => const ProfileUserScreen());
      case Routes.REPORTSCREEN:
        return MaterialPageRoute(builder: (_) => const ReportScreen());
      case Routes.SETMONITIORSCREEN:
        return MaterialPageRoute(builder: (_) => const SetClassMonitorScreen());
      case Routes.PREVIEWFILE:
        return MaterialPageRoute(builder: (_) => PreviewFileWidget());   
      case Routes.TEACHERFINANCES:
        return MaterialPageRoute(builder: (_)=> const TeacherFinanceScreen());
      case Routes.STUDENTFEELIST:
        return MaterialPageRoute(builder: (_)=>const StudentFeeListScreen());
      case Routes.VIEWREQUESTPERMISSION:
        return MaterialPageRoute(builder: (_)=>const ViewPermissionRequestScreen());
      default:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
    }
  }
}
