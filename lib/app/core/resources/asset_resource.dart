// ignore_for_file: constant_identifier_names

const String ICONS_PATH_SVG = "assets/icons/svg";
const String ICONS_PATH_PNG = "assets/icons/png";
const String IMAGE_PATH = "assets/images";
const String CLIP_PATH = "assets/clip_images";
const String SVG_PATH = "assets/icons/svg";

class ImageAssets {
  // <<<<<<<<<<<<<<< LOGO >>>>>>>>>>>>>>>>> //
  static const String logoCamis = '$IMAGE_PATH/logo.svg';

  // *************** ICON ***************** ///Users/holy/Desktop/camis-teacher-application/assets/icons/svg/comment-icon.svg
  static const String report_icon_not_fill = '$SVG_PATH/icon-report-not-fill.svg';
  static const String forward_square = '$SVG_PATH/forward-square.svg';
  static const String report_icon_fill_color = '$SVG_PATH/icon-report-fill.svg';
  static const String time_starter = '$SVG_PATH/timer-start.svg';
  static const String cross_pin = '$SVG_PATH/location-cross.svg';
  static const String calendar_tick_icon = '$ICONS_PATH_SVG/calendar-tick.svg';
  static const String hour_time_icon = '$ICONS_PATH_SVG/clock.svg';
  static const String tick_circle_icon = '$ICONS_PATH_SVG/tick-circle.svg';
  static const String fill_close_circle_icon = '$ICONS_PATH_SVG/close-circle_fill.svg';
  static const String timer_icon = '$ICONS_PATH_SVG/timer.svg';
  static const String youtub_icon = '$ICONS_PATH_SVG/icomoon-free_youtube2.svg';
  static const String earth_icon = '$ICONS_PATH_SVG/mdi_earth.svg';
  static const String ICON_COMMENT = '$SVG_PATH/comment-icon.svg';
  static const String ICON_CHARTS_GRAP = '$SVG_PATH/system-uicons_graph-bar.svg';


  static const String taguser = '$SVG_PATH/tag-user.svg';
  static const String award ='$SVG_PATH/award.svg';
  static const String personalcard_icon = '$SVG_PATH/personalcard.svg';
  static const String medal_icon ='$SVG_PATH/medal.svg';
  static const String star_icon ='$SVG_PATH/star.svg';
  static const String teacherhat_icon ='$SVG_PATH/teacher.svg';
  static const String message_edit_icon = '$SVG_PATH/message-edit.svg';
  static const String note_remove = '$SVG_PATH/note_remove.svg';
  static const String share_icon = "$SVG_PATH/ic_outline-share.svg";
  static const String card_tick_icon = "$SVG_PATH/card-tick.svg";
  static const String card_tick_outline_icon = "$SVG_PATH/card-tick_outline.svg";
  static const String card_add_icon = "$SVG_PATH/card_add.svg";
  static const String card_verify_icon = "$SVG_PATH/verify.svg";
  static const String fill_verify_icon = "$SVG_PATH/fill_verify.svg";
  
  static const String upload_attach_icon = '$SVG_PATH/upload_attach.svg';
  static const String round_check_icon = "$SVG_PATH/round_check.svg";
  static const String questionMark = '$SVG_PATH/question_mark.svg';
  static const String chevron_left = '$SVG_PATH/chevron_left.svg';
  static const String location = '$SVG_PATH/location.svg';
  static const String current_date = '$SVG_PATH/current-date.svg';
  static const String clipboard = '$SVG_PATH/clipboard-tick.svg';
  static const String Exam_fill = '$SVG_PATH/exam_icons_fill.svg';
  static const String profile_2users = '$SVG_PATH/profile-2user.svg';
  static const String arrow_right = '$SVG_PATH/arrow-right.svg';
  static const String arrow_left = '$SVG_PATH/arrow-left.svg';
  static const String attach_circlle = '$SVG_PATH/attach-circle.svg';
  static const String folder_icon = '$SVG_PATH/folder-open.svg';
  static const String subject_tag_icon = '$SVG_PATH/subject_tag.svg';
  
  static const String camera = 'assets/icons/svg/camera.svg';
  static const String gallery = '$SVG_PATH/gallery-add.svg';
  static const String more = '$SVG_PATH/more.svg';
  static const String book = '$SVG_PATH/book.svg';
  static const String calender = '$SVG_PATH/calendar.svg';
  static const String document = '$SVG_PATH/document-text.svg';
  static const String print = '$SVG_PATH/printer.svg';
  static const String more_circle = '$SVG_PATH/more-circle.svg';
  static const String edit = '$SVG_PATH/edit.svg';
  static const String absent = '$SVG_PATH/close-circle.svg';
  static const String remove_circle = '$SVG_PATH/remove_circle.svg';
  static const String flash_icon = "assets/icons/svg/flash.svg";
  static const String flash_off_icon = "assets/icons/svg/flash-off.svg";
  static const String keyboard_icon = '$SVG_PATH/keyboard.svg';
  static const String scanner_icon = "assets/icons/svg/scanner.svg";
  static const String cambodia_icon =
      "assets/images/country/Flag_of_Cambodia.svg";
  static const String english_icon =
      "assets/images/country/Flag_of_the_United_Kingdom.svg";
  static const String check_icon = "assets/icons/svg/check_circle.svg";

  static const String warning_icon = "assets/icons/svg/warning_icon.svg";
  static const String large_warning_icon = "$SVG_PATH/warning-2.svg";
  static const String additem_icon = "assets/icons/svg/additem.svg";

  static const String school_icon = "assets/icons/svg/bank.svg";
  static const String eye_off_icon = "assets/icons/svg/eye-off.svg";
  static const String eye_icon = "assets/icons/svg/eye.svg";
  static const String security_icon = "assets/icons/svg/security-user.svg";

  static const String question_circle =
      '$ICONS_PATH_SVG/vuesax_outline_question-circle.svg';
  static const String user_edit =
      '$ICONS_PATH_SVG/vuesax_outline_user-edit.svg';
  static const String people_icon = '$ICONS_PATH_SVG/vuesax_bold_people.svg';
  static const String clipboard_icon =
      '$ICONS_PATH_SVG/vuesax_bold_clipboard-text.svg';
  static const String setting_icon =
      '$ICONS_PATH_SVG/vuesax_bold_setting-2.svg';
  static const String phone_icon = '$ICONS_PATH_SVG/vuesax_bold_call.svg';
  static const String chevron_right_icon = '$ICONS_PATH_SVG/chevron-right.svg';
  static const String home_icon = '$ICONS_PATH_SVG/home-2.svg';
  static const String home_outline_icon =
      '$ICONS_PATH_SVG/vuesax_outline_home-2.svg';
  static const String more_outline_icon =
      '$ICONS_PATH_SVG/81-more-circle-2.svg';
  static const String more_icon = '$ICONS_PATH_SVG/vuesax_bold_more-circle.svg';
  static const String notification_outline_icon =
      '$ICONS_PATH_SVG/vuesax_outline_notification.svg';
  static const String notification_icon =
      '$ICONS_PATH_SVG/notification-bing.svg';
  static const String task_outline_icon =
      '$ICONS_PATH_SVG/vuesax_outline_task-square.svg';
  static const String task_icon = '$ICONS_PATH_SVG/task-square.svg';
  //static const String calendar_tick_icon = '$ICONS_PATH_SVG/calendar-tick.svg';
  static const String ranking_icon = '$ICONS_PATH_SVG/ranking.svg';
  static const String call_outlinr_icon = '$ICONS_PATH_SVG/call.svg';
  static const String setting_outlinr_icon = '$ICONS_PATH_SVG/setting-2.svg';
  static const String ADD_ICON = "$SVG_PATH/add_icon.svg";
  static const String INFORMATIOIN_ICON = "$SVG_PATH/information.svg";
  static const String NOTED_ICON = "$SVG_PATH/note.svg";
  static const String CALENDAR_ICON = "$SVG_PATH/calendar.svg";
  static const String INFO_ICON = "$SVG_PATH/info_circle.svg";
  static const String PEOPLE2_ICON = "$SVG_PATH/profile.svg";
  static const String CLOSE_ICON = "$SVG_PATH/close.svg";
  static const String GALLERY_ICON = "$SVG_PATH/bold_gallery.svg";
  static const String DOCUMENT_ICON = "$SVG_PATH/bold_document.svg";
  static const String FLAG_ICON = "$SVG_PATH/flag.svg";
  static const String CLIPBOARDTICK_ICON = "$SVG_PATH/clipboard-tick.svg";
  static const String PEOPLETASK_ICON = "$SVG_PATH/people_task.svg";
  static const String ANOUNMANT_ICON = "$SVG_PATH/anounment_icon.svg";
  static const String TELEGRAME_ICON = "$SVG_PATH/telegram_icon.svg";
  static const String DOCUMENSs_ICON = "$SVG_PATH/document_icon.svg";
  static const String EXAM_ICON = "$SVG_PATH/exam_icon.svg";
  static const String SEARCH_ICON = "$ICONS_PATH_SVG/search_icon.svg";
  static const String OFFICESPACE = "$ICONS_PATH_SVG/officespace.svg";
  static const String LOCATION = "$ICONS_PATH_SVG/location.svg";
  static const String CAMERA_icon = "$ICONS_PATH_SVG/ic_camera.svg";
  static const String gallary_add = "$ICONS_PATH_SVG/gallery_add.svg";
  static const String voice_cricle= "$ICONS_PATH_SVG/voice_cricle.svg";
  static const String PEOPLEATTANDANCE =
      "$ICONS_PATH_SVG/people_attandance.svg";
  static const String ARROWRIGHT_ICON = "$ICONS_PATH_SVG/arrow_right.svg";
  static const String BOOKNOTED = "$ICONS_PATH_SVG/book.svg";
  static const String HOMEWORK_ICON = "$ICONS_PATH_SVG/homework.svg";
  static const String CALENDARFILL_ICON = "$ICONS_PATH_SVG/calendar_fill.svg";
  static const String SENTMESSAGE_ICON = "$ICONS_PATH_SVG/send.svg";
  static const String booking_icon = "$ICONS_PATH_SVG/book.svg";
  static const String location_icon = "$ICONS_PATH_SVG/location.svg";
  static const String login_code_icon = '$ICONS_PATH_SVG/login_code.svg';
  static const String calendar_icon = "$ICONS_PATH_SVG/calendar-2.svg";
  static const String scan_qr_icon = "$ICONS_PATH_SVG/scan-barcode.svg";
  static const String search_icon = "$ICONS_PATH_SVG/search_icon.svg";
  static const String bank_icon_class = "$ICONS_PATH_SVG/bank_icon_class.svg";
  static const String book_notebook = "$ICONS_PATH_SVG/book_notebook.svg";
  static const String homework_sch = "$ICONS_PATH_SVG/homework_sch.svg";
  static const String calendar_add = "$ICONS_PATH_SVG/calendar_add.svg";
  static const String globe_icon = "$ICONS_PATH_SVG/globe.svg";
  static const String rectagle = "$IMAGE_PATH/Mask Group.svg";
  static const String arrow_circle = "$ICONS_PATH_SVG/arrow-circle-right.svg";
  static const String file = "$ICONS_PATH_SVG/file.svg";
  static const String trash_icon = "$ICONS_PATH_SVG/trash.svg";
  static const String timeline_icon = "$ICONS_PATH_SVG/timeline_icon.svg";
  static const String play_icon = '$ICONS_PATH_SVG/play.svg';
  static const String filldoc_icon = "$ICONS_PATH_SVG/fill_document.svg";
  static const String send_outline_icon = "$ICONS_PATH_SVG/send_outline.svg";
  static const String fill_play_icon = "$ICONS_PATH_SVG/fill_play_icon.svg";
  static const String fill_pause_icon = "$ICONS_PATH_SVG/pause.svg";
  static const String path20 = "$SVG_PATH/Path_20.svg";
  static const String comment_icon = "$SVG_PATH/comment_icon.svg";
  static const String fill_forward_icon =
      "$ICONS_PATH_SVG/forward-15-seconds.svg";
  static const String fill_backward_icon =
      "$ICONS_PATH_SVG/backward-15-seconds.svg";
  static const String reject_icon = "$ICONS_PATH_SVG/slash.svg";

  static const String multidoc_icon ="$SVG_PATH/multi_document.svg";


  static const String attachment_icon = "$SVG_PATH/attach_icon.svg";
  static const String ep_icon = "$SVG_PATH/ep-info.svg";
  static const String switch_icon = "$SVG_PATH/switch_icon.svg";
  static const String fill_timeline_icon = "$ICONS_PATH_SVG/bold_timeline.svg";
  static const String question_icon = "$ICONS_PATH_SVG/question_icon.svg";
  static const String pdf_icon = "$ICONS_PATH_PNG/pdf_icon.png";
  

  // +++++++++++++++ IMAGE ++++++++++++++++ //

  // --------------- CLIP IMAGE -----------------camis-mobile-application/ //
  static const String topbar_status_path = "$CLIP_PATH/topbar_status_path.svg";

  // --------------- Path IMAGE -----------------camis-mobile-application/ //
  static const String Path_18 = "$IMAGE_PATH/Path_18.png";
  static const String Path_19 = "$IMAGE_PATH/Path_19.png";
  static const String Path_20 = "$IMAGE_PATH/Path_20.png";
  static const String Path_21 = "$IMAGE_PATH/Path_21.png";
  static const String Path_22 = "$IMAGE_PATH/Path_22.png";
  static const String Path_19_About = "$IMAGE_PATH/Path_19_about.png";
  static const String Path_19_help = "$IMAGE_PATH/Path_19_help.png";
  static const String Path_21_help = "$IMAGE_PATH/Path_21_help.png";
  static const String Path_18_sch = "$IMAGE_PATH/path_18_sch.png";


  //======== List student payment ========

  static const String student_not_paid_icon ='$SVG_PATH/student-not-paid-status.svg';
  static const String pay_company_icon ='$SVG_PATH/icon_pay_company.svg';
  static const String dollar_circle_icon ='$SVG_PATH/dollar-circle.svg';
  static const String dollar_circle_filed_icon ='$SVG_PATH/dollar-circle_filed.svg';

  ImageAssets(String path_18);
}
