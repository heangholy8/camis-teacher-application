// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

const String _fontkantumruyPro = "kantumruyPro";
const String _fontKantumruyPro_Regular = "KantumruyPro-Regular";
const String _fontKantumruyPro_SemiBold = "KantumruyPro-SemiBold";

class ThemsConstands {
  static final headline3_semibold_20 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontSize: 20,
    fontStyle: FontStyle.normal,
  );
  static const button_semibold_16 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontStyle: FontStyle.normal,
    fontSize: 16,
  );
  static const headline_5_medium_16 = TextStyle(
    fontFamily: _fontkantumruyPro,
    fontStyle: FontStyle.normal,
    fontSize: 16,
  );

  static const headline_1_semibold_32 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontStyle: FontStyle.normal,
    fontSize: 32,
  );
  static const headline_2_semibold_24 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontStyle: FontStyle.normal,
    fontSize: 24,
  );

  static const headline_4_medium_18 = TextStyle(
    fontFamily: _fontKantumruyPro_Regular,
    fontStyle: FontStyle.normal,
    fontSize: 18,
  );
  static const headline4_regular_18 = TextStyle(
    fontFamily: _fontKantumruyPro_Regular,
    fontSize: 18,
    fontStyle: FontStyle.normal,
  );
  static const headline_4_semibold_18 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontStyle: FontStyle.normal,
    fontSize: 18,
  );
  static const headline_6_semibold_14 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontStyle: FontStyle.normal,
    fontSize: 14,
  );

  static const headline_6_regular_14_20height = TextStyle(
    fontFamily: _fontKantumruyPro_Regular,
    fontSize: 14,
    fontStyle: FontStyle.normal,
  );

  static const caption_regular_12 = TextStyle(
    fontFamily: _fontKantumruyPro_Regular,
    fontSize: 12,
    fontStyle: FontStyle.normal,
  );

  static const caption_simibold_12 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontSize: 12,
    fontStyle: FontStyle.normal,
  );

  static const overline_semibold_12 = TextStyle(
      fontFamily: _fontKantumruyPro_SemiBold,
      fontSize: 12,

      fontStyle: FontStyle.normal);

  static const caption_regular_text12 = TextStyle(
    fontFamily: _fontKantumruyPro_Regular,
    fontSize: 12,
    fontStyle: FontStyle.normal,
  );
  static const headline_5_semibold_16 = TextStyle(
    fontFamily: _fontKantumruyPro_SemiBold,
    fontStyle: FontStyle.normal,
    fontSize: 16,
  );

  static const subtitle1_regular_16 = TextStyle(
    fontFamily: _fontKantumruyPro_Regular,
    fontStyle: FontStyle.normal,
    fontSize: 16,
  );

  static const headline3_medium_20_26height = TextStyle(
      fontFamily: _fontkantumruyPro,
      fontSize: 20,
      fontStyle: FontStyle.normal,
      );

  static const headline6_regular_14_24height = TextStyle(
      fontFamily: _fontKantumruyPro_Regular,
      fontSize: 14,

      fontStyle: FontStyle.normal);

  static const headline6_medium_14 = TextStyle(
      fontFamily: _fontkantumruyPro,
      fontSize: 14,
      fontStyle: FontStyle.normal);

}
