import 'package:camis_teacher_application/app/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserSecureStroage with KeyStoragePref {
//==================Set to Storage ================
  void setDeviceToken({required String devicetoken}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keyDevicePref, devicetoken)
          .then((value) => debugPrint("Successfull to store => device token"));
    } catch (e) {
      debugPrint("Fail to store device token: $e");
    }
  }

  void setToken({required String token}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keyTokenPref, token)
          .then((value) => debugPrint("Successfull to store => token json"));
    } catch (e) {
      debugPrint("Fail to store token json: $e");
    }
  }

  void savePhoneNumber({required String keyPhoneNumber}) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keyPhonePref, keyPhoneNumber)
          .then((value) => debugPrint("Successfull to store => Phone Number"));
    } catch (e) {
      debugPrint("Fail to store phone number: $e");
    }
  }

  void saveSchoolLat({required String schoolLat}) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(keySchoolLati, schoolLat).then((value) => debugPrint("Successfull to store => School lat"));
    } catch (e) {
      debugPrint("Fail to store school Lat: $e");
    }
  }

  void saveSchoolLog({required String schoolLog}) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keySchoolLogti, schoolLog)
          .then((value) => debugPrint("Successfull to store => school Log"));
    } catch (e) {
      debugPrint("Fail to store school Log: $e");
    }
  }

  void saveSchoolRadius({required String schoolRadius}) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keySchoolRadius, schoolRadius)
          .then((value) => debugPrint("Successfull to store => school Radius"));
    } catch (e) {
      debugPrint("Fail to store school Radius: $e");
    }
  }

  void saveChangeUIEnterScore({required String keyChangeUiEnterScore}) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keyChangeEnterScorePref, keyChangeUiEnterScore)
          .then((value) => debugPrint("Successfull to Change"));
    } catch (e) {
      debugPrint("Fail to Change: $e");
    }
  }

  void saveSelectLangSuccess({required String selectLangSuccess}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keyLangPref, selectLangSuccess)
          .then((value) => debugPrint("Successfull to store Language: $value"));
    } catch (e) {
      debugPrint("Fail to store language: $e");
    }
  }

  void saveConfirmInformation({required String isConfirmed}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keyConfirmPref, isConfirmed)
          .then((value) => debugPrint("Comfirmed Information Success: $value"));
    } catch (e) {
      debugPrint("Fail comfirmed Information: $e");
    }
  }

  void saveSelectClassMonitor({required String isSelected}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(keyMonitorPref, isSelected).then((value) => debugPrint("Selected Monitor Success: $value"));
    } catch (e) {
      debugPrint("Fail Selected Monitor: $e");
    }
  }

  void setSchoolLocationPref({required String schoolocationModel}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(keySchoolLocationPref, schoolocationModel).then((value) => debugPrint("Successfull to store => token json"));
    } catch (e) {
      debugPrint("Fail to store token json: $e");
    }
  }
}
