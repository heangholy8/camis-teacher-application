mixin class KeyStoragePref {
  static const keyDeviceToken = "devicetoken";
  static const keyToken = "token";
  static const keyPhoneNumber = "keyPhoneNumber";
  static const keySchooldistanceRadius = "schooldistanceRadius";
  static const keySchoolLogtitude = "schoolLatitude";
  static const keySchoolLatitude = "schoolLogtitude";
  static const keySelectLangSuccess = "keySelectLangSuccess";
  static const keyConfirmInfo = "KeyConfirmInformation";
  static const keySelectedMonitor = "KeySelectionClassmonitor";
  static const keyChangeEnterScore = "keyChangeEnterScore";
  static const keySchoolLocation = "KeySchoolLocation";

  String get keyDevicePref => keyDeviceToken;
  String get keyTokenPref => keyToken;
  String get keyPhonePref => keyPhoneNumber;
  String get keySchoolLati => keySchoolLatitude;
  String get keySchoolRadius => keySchooldistanceRadius;
  String get keySchoolLogti => keySchoolLogtitude;
  String get keyLangPref => keySelectLangSuccess;
  String get keyConfirmPref => keyConfirmInfo;
  String get keyMonitorPref => keySelectedMonitor;
  String get keyChangeEnterScorePref => keyChangeEnterScore;
  String get keySchoolLocationPref => keySchoolLocation;

}
