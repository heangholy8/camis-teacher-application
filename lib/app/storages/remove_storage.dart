import 'package:camis_teacher_application/app/modules/login_screen/login_screen.dart';
import 'package:camis_teacher_application/app/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RemoveStoragePref {
  removeToken(context) async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      await pref.remove("token");
      await pref.remove(KeyStoragePref.keySchoolLocation).then((value) => print("school location removed from storage"));
      Navigator.pushAndRemoveUntil(
        context,
        PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) => const LoginScreen(),
        transitionDuration: Duration.zero,
        reverseTransitionDuration: Duration.zero,
      ),
            (route) => false);
    } catch (e) {
      debugPrint("Fail to remove pref $e");
    }
  }
}
