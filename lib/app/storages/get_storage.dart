import 'dart:convert';
import 'package:camis_teacher_application/app/storages/key_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/auth_model/auth_model.dart';
import '../models/grading_model/grading_model.dart';
import '../models/tracking_location_model/school_location_model.dart';

class GetStoragePref with KeyStoragePref{
  Future<String?> getDeviceToken() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var deviceToken = prefs.getString(keyDevicePref.toString()) ?? "";
      if (deviceToken != "") {
        return deviceToken;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<AuthModel> get getJsonToken async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      String? data = pref.getString(keyTokenPref) ?? "";
      AuthModel? authModel;
      if (data != "") {
        var result = json.decode(data.toString());
        authModel = AuthModel.fromJson(result);
        return authModel;
      }
      return authModel ?? AuthModel();
    } catch (e) {
      throw Exception("Get JsonToke Unsuccess");
    }
  }

  Future<String?> getPhoneNumber() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var phoneNumber = prefs.getString(keyPhonePref.toString()) ?? "";
      if (phoneNumber != "") {
        return phoneNumber;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getChangeUiEnterScore() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var uiChange = prefs.getString(keyChangeEnterScorePref.toString()) ?? "";
      if (uiChange != "") {
        return uiChange;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getSelectLangSuccessr() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var selectLangSuccess =
          prefs.getString(keyLangPref.toString()) ?? "";
      if (selectLangSuccess != "") {
        return selectLangSuccess;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getComfirmInfo() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var comfirmaInformation =
          prefs.getString(keyLangPref.toString()) ?? "";
      if (comfirmaInformation != "") {
        return comfirmaInformation;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }


  Future<String?> getSchoolLat() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var schoolLat =
          prefs.getString(keySchoolLati.toString()) ?? "";
      if (schoolLat != "") {
        return schoolLat;
      } else {
        return "";
      }
    } catch (e) {
      return "";
    }
  }


  Future<String?> getSchoolLog() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var schoolLog =
          prefs.getString(keySchoolLogti.toString()) ?? "";
      if (schoolLog != "") {
        return schoolLog;
      } else {
        return "";
      }
    } catch (e) {
      return "";
    }
  }


  Future<String?> getSchoolRadius() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var schoolRadius = prefs.getString(keySchoolRadius.toString()) ?? "";
      if (schoolRadius != "") {
        return schoolRadius;
      } else {
        return "";
      }
    } catch (e) {
      return "";
    }
  }

  Future<String?> getSelectedMonitor() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var selectedMonitoring =
          prefs.getString(keyMonitorPref.toString()) ?? "";
      if (selectedMonitoring != "") {
        return selectedMonitoring;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<GradingData> get getScoreData async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      String? datas = pref.getString("Data Score") ?? "";
      GradingData? gradingModel;
      if (datas.isNotEmpty) {
        print("on progress...............");
        var result = json.decode(datas);
        gradingModel = GradingData.fromJson(result);
        print( "classId : ${gradingModel.classId}");
        return gradingModel;
      }
      return gradingModel ?? GradingData();
    } catch (e) {
      throw Exception("Get Data Unsuccess : $e");
    }
  }
  Future<String?> get getInstructor async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var isInstructor =
          prefs.getString("Instructor");
      if (isInstructor != "") {
        return isInstructor;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<SchoolLocationModel> get getSchoolLocationPref async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      String? data = pref.getString(keySchoolLocationPref) ?? "";
      SchoolLocationModel? schoolLocationModel;
      if (data != "") {
        var result = json.decode(data.toString());
        schoolLocationModel = SchoolLocationModel.fromJson(result);
        return schoolLocationModel;
      }
      return schoolLocationModel ?? SchoolLocationModel();
    } catch (e) {
      throw Exception("Get JsonToke Unsuccess");
    }
  }
}