import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

void saveDataInLocal(String data) async {
     try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref
          .setString("Data Score", data)
          .then((value) => debugPrint("Success to store json in cache"));
    } catch (e) {
      // debugPrint("Unsuccess to store json auth in cache");
    }
}
void saveInstructor(String data) async {
     try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref
          .setString("Instructor", data)
          .then((value) => debugPrint("Success to store instructor in cache"));
    } catch (e) {
      // debugPrint("Unsuccess to store json auth in cache");
    }
}