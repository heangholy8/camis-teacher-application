part of 'list_month_semester_bloc.dart';

abstract class ListMonthSemesterEvent extends Equatable {
  const ListMonthSemesterEvent();

  @override
  List<Object> get props => [];
}

class ListMonthSemesterEventData extends ListMonthSemesterEvent {}
