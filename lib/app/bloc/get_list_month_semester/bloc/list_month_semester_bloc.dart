import 'package:bloc/bloc.dart';
import 'package:camis_teacher_application/app/models/list_month_semester/list_month_semester_model.dart';
import 'package:equatable/equatable.dart';
import '../../../service/api/get_list_month_with_semester/get_list_month_semester_api.dart';
part 'list_month_semester_event.dart';
part 'list_month_semester_state.dart';

class ListMonthSemesterBloc extends Bloc<ListMonthSemesterEvent, ListMonthSemesterState> {
  final GetListMonthSemesterApi getListMonthExamApi;
  ListMonthSemesterBloc({required this.getListMonthExamApi}) : super(ListMonthSemesterInitial()) {
    on<ListMonthSemesterEventData>((event, emit) async{
      emit(ListMonthSemesterLoading());
      try {
        var data = await getListMonthExamApi.getListMonthSemester();
        emit(ListMonthSemesterLoaded(monthAndSemesterList: data));
      } catch (e) {
        print(e);
        emit(ListMonthSemesterError());
      }
    });
  }
}
