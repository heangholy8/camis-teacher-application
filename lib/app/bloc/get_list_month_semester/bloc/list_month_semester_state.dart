part of 'list_month_semester_bloc.dart';

abstract class ListMonthSemesterState extends Equatable {
  const ListMonthSemesterState();
  
  @override
  List<Object> get props => [];
}

class ListMonthSemesterInitial extends ListMonthSemesterState {}
class ListMonthSemesterLoading extends ListMonthSemesterState {}

class ListMonthSemesterLoaded extends ListMonthSemesterState {
  final ListMonthSermesterModel? monthAndSemesterList;
  const ListMonthSemesterLoaded({required this.monthAndSemesterList});
}
class ListMonthSemesterError extends ListMonthSemesterState {}
