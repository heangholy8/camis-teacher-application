part of 'check_version_update_bloc.dart';

abstract class CheckVersionUpdateState extends Equatable {
  const CheckVersionUpdateState();
  
  @override
  List<Object> get props => [];
}

class CheckVersionUpdateInitial extends CheckVersionUpdateState {}

class CheckVersionUpdateLoading extends CheckVersionUpdateState{}

class CheckVersionUpdateLoaded extends CheckVersionUpdateState{
  final CheckVersionUpdateModel checkUpdateModel;
  const CheckVersionUpdateLoaded({required this.checkUpdateModel});
}

class CheckVersionUpdateError extends CheckVersionUpdateState{}
