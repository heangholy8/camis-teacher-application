part of 'check_version_update_bloc.dart';

abstract class CheckVersionUpdateEvent extends Equatable {
  const CheckVersionUpdateEvent();

  @override
  List<Object> get props => [];
}

class CheckVersionUpdate extends CheckVersionUpdateEvent{}