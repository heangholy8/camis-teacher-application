part of 'update_user_info_bloc.dart';

abstract class UpdateUserInfoEvent extends Equatable {
  const UpdateUserInfoEvent();

  @override
  List<Object> get props => [];
}
class ChangeInforData extends UpdateUserInfoEvent {
  final String? fristname;
  final String? lastname;
 const ChangeInforData({this.fristname, this.lastname});
}

class ChangeInforImageData extends UpdateUserInfoEvent {
  final String? fristname;
  final String? lastname;
  final File? image;
 const ChangeInforImageData({this.fristname, this.lastname,this.image, });
}
