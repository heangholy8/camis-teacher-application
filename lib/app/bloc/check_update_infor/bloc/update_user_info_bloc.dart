import 'dart:io';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../models/profile_model/update_user_model.dart';
import '../../../service/api/profile_api/post_user_profile.dart';
part 'update_user_info_event.dart';
part 'update_user_info_state.dart';

class UpdateUserInfoBloc
    extends Bloc<UpdateUserInfoEvent, UpdateUserInfoState> {
  final UpdateProfileUserApi userUpadeData;
  UpdateUserInfoBloc({required this.userUpadeData})
      : super(UpdateUserInfoInitial()) {
    on<ChangeInforImageData>((event, emit) async {
      emit(ChangeLoading());
      try {
        var data = await userUpadeData.updateUserProfileImageRequestApi(
          image: event.image!,
        );
        emit(ChangeInforLoaded(dataInfor: data));
      } catch (e) {
        print(e);
        emit(ChangeError());
      }
    });
    on<ChangeInforData>((event, emit) async {
      emit(ChangeLoading());
      try {
        var data = await userUpadeData.updateUserProfileRequestApi(
          lastname: event.lastname.toString(),
          firstname: event.fristname.toString(),
        );
        emit(ChangeInforLoaded(dataInfor: data));
      } catch (e) {
        print(e);
        emit(ChangeError());
      }
    });
  }
}
