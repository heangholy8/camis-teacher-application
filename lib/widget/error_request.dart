import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../app/core/constands/color_constands.dart';
import '../app/core/thems/thems_constands.dart';

import 'button_widget/button_widget.dart';

class ErrorRequestData extends StatefulWidget {
  final String title;
  final String discription;
  final bool hidebutton;
  final VoidCallback onPressed;
  const ErrorRequestData({Key? key, required this.title, required this.discription, required this.hidebutton, required this.onPressed}) : super(key: key);

  @override
  State<ErrorRequestData> createState() => _ErrorRequestDataState();
}

class _ErrorRequestDataState extends State<ErrorRequestData> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.all(22),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(widget.title,style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.center,),
            Text(widget.discription,style: ThemsConstands.headline_5_medium_16.copyWith(color: Colorconstand.lightBlack),),
            widget.hidebutton==false?Container():Container(
              margin: const EdgeInsets.only(top: 25, bottom: 25),
              child: Button_Custom(
                onPressed: widget.onPressed,
                buttonColor: Colorconstand.primaryColor,
                hightButton: 45,
                radiusButton: 12,
                maginRight: 17.0,
                maginleft: 17.0,
                titleButton: "TRY_AGAIN".tr(),
                titlebuttonColor: Colorconstand.neutralWhite,
              ),
            ),
          ],
        ),
      ),
    );
  }
}