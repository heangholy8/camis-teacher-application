import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class ScanViewWidget extends StatelessWidget {
  const ScanViewWidget({
    Key? key,
    required MobileScannerController mobileScannerController,
    required this.onDetect,
  })  : _mobileScannerController = mobileScannerController,
        super(key: key);

  final MobileScannerController _mobileScannerController;
  final void Function(BarcodeCapture)? onDetect;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 230,
      width: 230,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned.fill(
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: MobileScanner(
                fit: BoxFit.cover,
                controller: _mobileScannerController,
                onDetect: onDetect!,
              ),
            ),
          ),
          Positioned.fill(
            child: Container(
              padding: const EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                // border: Border.all(
                //   color:Colorconstand.neutralWhite,
                //   width: 0,
                // ),
                borderRadius: BorderRadius.circular(12)
              ),
            ),
          ),
          // Positioned(
          //   top: 0,left: 0,bottom: 0,
          //   child: Center(
          //     child: Container(
          //       alignment: Alignment.centerLeft,
          //       color: Color(0x922769BF),
          //         height: 110,width: 3,              
          //     ),
          //   ),
          // ),
          // Positioned(
          //   top: 0,right: 0,bottom: 0,
          //   child: Center(
          //     child: Container(
          //       alignment: Alignment.centerLeft,
          //       color: Color(0x922769BF),
          //         height: 110,width: 3,              
          //     ),
          //   ),
          // ),
          // Positioned(
          //   top: 0,left: 0,right: 0,
          //   child: Center(
          //     child: Container(
          //       alignment: Alignment.centerLeft,
          //       color: Color(0x922769BF),
          //         height: 3,width: 110,              
          //     ),
          //   ),
          // ),
          // Positioned(
          //   bottom: 0,left: 0,right: 0,
          //   child: Center(
          //     child: Container(
          //       alignment: Alignment.centerLeft,
          //       color: Color.fromARGB(71, 45, 122, 230),
          //         height: 3,width: 110,              
          //     ),
          //   ),
          // ),
          Positioned(
            top: -3.7,
            left: -3.7,
            child: SvgPicture.asset("assets/images/1.svg"),
          ),
          Positioned(
            top: -3.7,
            right: -3.7,
            child: SvgPicture.asset("assets/images/2.svg"),
          ),
          Positioned(
            bottom: -3.7,
            right: -3.7,
            child: SvgPicture.asset("assets/images/4.svg"),
          ),
          Positioned(
            bottom: -3.7,
            left: -3.7,
            child: SvgPicture.asset("assets/images/3.svg"),
          ),
          // Positioned(
          //   top: -2,
          //   left: -2,
          //   child: Container(
          //     width: 50.0,
          //     height: 50.0,
          //     decoration:const BoxDecoration(
          //       border: Border(
          //         top:
          //             BorderSide(color: Colorconstand.neutralWhite, width: 2.0),
          //         left: BorderSide(
          //           color: Colorconstand.neutralWhite,
          //           width: 2.0,
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          // Positioned(
          //   top: -2,
          //   right: -2,
          //   child: Container(
          //     width: 50.0,
          //     height: 50.0,
          //     decoration:const BoxDecoration(
          //       border: Border(
          //         top:BorderSide(color: Colorconstand.neutralWhite, width: 2.0),
          //         right: BorderSide(
          //           color: Colorconstand.neutralWhite,
          //           width: 2.0,
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          // Positioned(
          //   bottom: -2,
          //   right: -2,
          //   child: Container(
          //     width: 50.0,
          //     height: 50.0,
          //     decoration: const BoxDecoration(
          //       border: Border(
          //         bottom:
          //             BorderSide(color: Colorconstand.neutralWhite, width: 2.0),
          //         right: BorderSide(
          //           color: Colorconstand.neutralWhite,
          //           width: 2.0,
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          // Positioned(
          //   bottom: -2,
          //   left: -2,
          //   child: Container(
          //     width: 50.0,
          //     height: 50.0,
          //     decoration: const BoxDecoration(
          //       border: Border(
          //         bottom:
          //             BorderSide(color: Colorconstand.primaryColor, width: 2.0),
          //         left:
          //             BorderSide(color: Colorconstand.primaryColor, width: 2.0),
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
