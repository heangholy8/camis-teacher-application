import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';
import '../button_widget/button_widget.dart';

// ignore: must_be_immutable
class CustomAlertDialog extends StatelessWidget {
  final String title;
  final String subtitle;
  String titleButton;
  double? widthButton;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;
  Color? blurColor;
  Widget child;
  CustomAlertDialog(
      {Key? key,
      required this.child,
      required this.title,
      required this.subtitle,
      this.buttonColor,
      this.onPressed,
      required this.titleButton,
      this.titlebuttonColor,
      this.blurColor,
      this.widthButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 5,
      backgroundColor: Colorconstand.neutralWhite,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 2,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 25),
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 6,
              height: MediaQuery.of(context).size.width / 6,
              decoration: BoxDecoration(
                  color: blurColor??Colorconstand.alertsNotifications.withOpacity(0.1),
                  shape: BoxShape.circle),
              child: Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: child,
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 35, left: 17.0, right: 17.0),
                child: Align(
                  child: Text(
                    title,
                    style: ThemsConstands.button_semibold_16.copyWith(
                        color: Colorconstand.lightBulma,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                )),
            Container(
              margin: const EdgeInsets.only(top: 8, left: 17.0, right: 17.0),
              child: Align(
                child: Text(
                  subtitle,
                  style: ThemsConstands.button_semibold_16.copyWith(
                      color: Colorconstand.neutralDarkGrey,
                      fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 25, bottom: 25),
              child: Button_Custom(
                onPressed: onPressed,
                buttonColor: buttonColor,
                hightButton: 45,
                radiusButton: 12,
                maginRight: 17.0,
                maginleft: 17.0,
                titleButton: titleButton,
                titlebuttonColor: titlebuttonColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
