import 'package:flutter/material.dart';

import '../app/core/constands/color_constands.dart';

class Profile extends StatelessWidget {
  String imageProfile;
  double height;
  double width;
  double pandding;
  VoidCallback onPressed;
  String namechild;
  double sizetextname;
   Profile({ Key? key,required this.imageProfile,required this.height,required this.sizetextname,required this.namechild,required this.width,required this.pandding,required this.onPressed }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(pandding),
      height: height,
      width: width,
      decoration:const BoxDecoration(
            color: Colorconstand.neutralWhite,
            shape: BoxShape.circle
          ),
      child: MaterialButton(
        color: Colorconstand.neutralSecondary,
        shape:const CircleBorder(),
        padding:const EdgeInsets.all(0),
        onPressed: onPressed,
        child: Stack(
          children: [
             Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              height: height,
              width: width,
              child: Text(namechild,style: TextStyle(color: Colorconstand.neutralWhite,fontSize:sizetextname))),
            imageProfile!=""?SizedBox(
              height: height,
              width: width,
              child: CircleAvatar(backgroundImage: NetworkImage(imageProfile),backgroundColor: Colors.transparent,))
              :Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              height: height,
              width: width,
              child: Text(namechild,style: TextStyle(color: Colorconstand.neutralWhite,fontSize:sizetextname))),

          ],
        )
      ),
    );
  }
}