import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import '../../app/service/api/isverify_api/update_isverify_api.dart';
import '../../app/storages/user_storage.dart';
import '../button_widget/button_widget.dart';

class WidgetComfirmPhoneNumber extends StatefulWidget {
   String phoneNumber;
   int gender;
   VoidCallback onComfirm;
   VoidCallback onComfirmSkip;
   WidgetComfirmPhoneNumber({super.key,required this.gender,this.phoneNumber = "",required this.onComfirm,required this.onComfirmSkip});

  @override
  State<WidgetComfirmPhoneNumber> createState() => _WidgetComfirmPhoneNumberState();
}

class _WidgetComfirmPhoneNumberState extends State<WidgetComfirmPhoneNumber> {
  TextEditingController controllerText = TextEditingController();
  UserSecureStroage saveStoragePref = UserSecureStroage();
  bool showInputPhone = false;
  @override
  void initState() {
    setState(() {
      controllerText.text = widget.phoneNumber;
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        padding:const EdgeInsets.symmetric(vertical: 18,horizontal: 22),
        decoration:const BoxDecoration(
          color: Colorconstand.neutralWhite,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))
        ),
        child: showInputPhone == true?Column(
          children: [
            Container(
              child: Text("ENTER_PHONENUMBER".tr(),style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,),
            ),
            Container(
              margin:const EdgeInsets.symmetric(vertical: 12),
              child: TextFormField(
              focusNode: FocusNode(),
              controller: controllerText,
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              autofocus: true,
              style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.lightBlack),
                decoration: InputDecoration(
                  hintText: '0123456789',
                  contentPadding:const EdgeInsets.fromLTRB(20.0, 25.0, 20.0, 25.0),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 12),
              child: ButtonOutlineCustom(
                titleButton: "FINISH".tr(),
                buttonColor: Colorconstand.neutralWhite,
                radiusButton: 10,
                onPressed: () {
                  setState(() {
                    print("fasdf${controllerText.text}");
                    widget.phoneNumber = controllerText.text;
                    showInputPhone = false;
                    FocusScope.of(context).unfocus();
                    UpdatingIsverifyApi() .updateIsverifyApi( phoneNumber: controllerText.text) .then((value) => debugPrint("Checking Isverify Successfully $value"));
                    saveStoragePref.savePhoneNumber(keyPhoneNumber:controllerText.text.toString());
                  });
                },
                hightButton: 55,
                maginRight: 10,
                widthButton: MediaQuery.of(context).size.width/2,
                titlebuttonColor: Colorconstand.primaryColor,
                borderColor: Colorconstand.primaryColor,
                sizeText: 18,
                child: Container(
                  margin:const EdgeInsets.only(right: 8),
                  child:const Icon(Icons.check_rounded,color: Colorconstand.primaryColor,size: 25,)),
              ),
            )
          ],
        ):Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 12),
              child:const Icon(Icons.phone_android,size: 78,color: Colorconstand.primaryColor,),
            ),
            Container(
              child: Text(widget.phoneNumber!=""?"IS_TEACHER_PHONENUMBER_CORRECT".tr(): widget.gender == 1?"${translate=="km"?"លោកគ្រូ":"Teacher"}${"TEACHER_NOT_HAVE_PHONENUMBER".tr()}":"${translate=="km"?"អ្នកគ្រូ":"Teacher"}${"TEACHER_NOT_HAVE_PHONENUMBER".tr()}",style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,),
            ),
            widget.phoneNumber !=""?Container(
              margin: const EdgeInsets.only(top: 12),
              child: Text(widget.phoneNumber,style: ThemsConstands.headline_2_semibold_24.copyWith(color: Colorconstand.lightBulma),),
            ):Container(),
             widget.phoneNumber !=""? Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 12),
                    child: ButtonOutlineCustom(
                      titleButton: "EDIT".tr(),
                      buttonColor: Colorconstand.neutralWhite,
                      radiusButton: 10,
                      onPressed: () {
                        setState(() {
                          showInputPhone = true;
                        });
                      },
                      hightButton: 55,
                      maginRight: 10,
                      widthButton: MediaQuery.of(context).size.width/2,
                      titlebuttonColor: Colorconstand.primaryColor,
                      borderColor: Colorconstand.primaryColor,
                      sizeText: 18,
                      child: Container(
                        margin:const EdgeInsets.only(right: 8),
                        child:SvgPicture.asset(ImageAssets.edit,color: Colorconstand.primaryColor,width: 25,)),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 12),
                    child: ButtonOutlineCustom(
                      titleButton: "CORRECT".tr(),
                      buttonColor: Colorconstand.primaryColor,
                      radiusButton: 10,
                      onPressed: widget.onComfirm,
                      hightButton: 55,
                      titlebuttonColor: Colorconstand.neutralWhite,
                      borderColor: Colorconstand.primaryColor,
                      sizeText: 18,
                      child: Container(
                        margin:const EdgeInsets.only(right: 8),
                        child:const Icon(Icons.check_rounded,color: Colorconstand.neutralWhite,size: 25,)),
                    ),
                  ),
                ),
              ],
            ):Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 12),
                    child: ButtonOutlineCustom(
                      titleButton: "SKIP".tr(),
                      buttonColor: Colorconstand.neutralWhite,
                      radiusButton: 10,
                      onPressed: widget.onComfirmSkip,
                      hightButton: 55,
                      maginRight: 10,
                      widthButton: MediaQuery.of(context).size.width/2,
                      titlebuttonColor: Colorconstand.primaryColor,
                      borderColor: Colorconstand.primaryColor,
                      sizeText: 18,
                      child: Container(
                        margin:const EdgeInsets.only(right: 8),
                        child:SvgPicture.asset(ImageAssets.forward_square,color: Colorconstand.primaryColor,width: 25,)),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 12),
                    child: ButtonOutlineCustom(
                      titleButton: "INPUT_NUMBER".tr(),
                      buttonColor: Colorconstand.primaryColor,
                      radiusButton: 10,
                      onPressed: (){
                        setState(() {
                          showInputPhone = true;
                        });
                      },
                      hightButton: 55,
                      titlebuttonColor: Colorconstand.neutralWhite,
                      borderColor: Colorconstand.primaryColor,
                      sizeText: 18,
                      child: Container(
                        margin:const EdgeInsets.only(right: 8),
                        child:SvgPicture.asset(ImageAssets.edit,color: Colorconstand.neutralWhite,width: 25,)),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}