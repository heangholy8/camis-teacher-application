import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';

class CustomMaterialButton extends StatelessWidget {
  void Function()? onPressed;
  TextStyle? styleText;
  String titleButton;
  EdgeInsetsGeometry? padding;
  Color? borderColor;
  Color? colorButton;
  double? hight;
  BorderRadiusGeometry? borderRadius;
  CustomMaterialButton(
      {super.key,
      required this.onPressed,
      required this.titleButton,
      this.padding,
      this.borderRadius,
      this.styleText,
      this.borderColor,
      this.hight,
      this.colorButton});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(
          borderRadius: borderRadius ??BorderRadius.circular(10.0),
          side:
              BorderSide(width: 1.2, color: borderColor ?? Colors.transparent)),
      visualDensity: VisualDensity.compact,
      onPressed: onPressed,
      color: colorButton,
      padding: padding,
      height:hight??50,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Text(
          titleButton,
          textAlign: TextAlign.center,
          style: styleText,
        ),
      ),
    );
  }
}

class Button_Custom extends StatelessWidget {
  String titleButton;
  double? hightButton;
  double? widthButton;
  double radiusButton;
  double maginRight;
  double maginleft;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;

  Button_Custom(
      {Key? key,
      this.titleButton = "",
      this.onPressed,
      this.hightButton,
      this.widthButton,
      this.radiusButton = 0,
      this.maginRight = 0,
      this.maginleft = 0,
      this.buttonColor,
      this.titlebuttonColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthButton,
      margin: EdgeInsets.only(right: maginRight, left: maginleft),
      child: MaterialButton(
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        height: hightButton,
        color: buttonColor,
        disabledColor: Colorconstand.neutralGrey,
        onPressed: onPressed,
        child: Center(
          child: Text(
            titleButton,
            style: ThemsConstands.button_semibold_16.copyWith(
              color: titlebuttonColor,),textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}

class ButtonOutlineCustom extends StatelessWidget {
  String titleButton;
  double? hightButton;
  double? widthButton;
  double radiusButton;
  double maginRight;
  double maginleft;
  Color? titlebuttonColor;
  Color? buttonColor;
  Color? borderColor;
  VoidCallback? onPressed;
  Widget? child;
  double? sizeText;
  ButtonOutlineCustom(
      {Key? key,
      this.borderColor,
      this.titleButton = "",
      this.onPressed,
      this.hightButton,
      this.widthButton,
      this.radiusButton = 0,
      this.maginRight = 0,
      this.maginleft = 0,
      this.buttonColor,
      this.sizeText,
      this.titlebuttonColor,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthButton,
      margin: EdgeInsets.only(right: maginRight, left: maginleft),
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            side: BorderSide(color: borderColor!),
            borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        height: hightButton,
        color: buttonColor,
        onPressed: onPressed,
        child:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            child!,
            Center(
                child: Text(
              titleButton,
              style: TextStyle(
                color: titlebuttonColor,
                fontSize: sizeText,
              ),
            )),
          ],
        ),
      ),
    );
  }
}
