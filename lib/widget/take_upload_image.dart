

import 'dart:io';
import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ShowPiker extends StatelessWidget {
  VoidCallback onPressedCamera;
  VoidCallback onPressedGalary;
  ShowPiker({Key? key,required this.onPressedCamera,required this.onPressedGalary}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
            child: Container(
              height: 200,
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                   Container(
                     height: 45,
                     decoration: BoxDecoration(
                       borderRadius:const BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12)),
                       color: Colors.grey[200],
                       
                     ),
                     child: MaterialButton(
                       shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                       padding:  const EdgeInsets.all(0),
                        child: Align(alignment: Alignment.center,child: Text('TAKE_PHOTO'.tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.primaryColor))),
                        onPressed: onPressedCamera,
                   ),),
                   Container(height: 1,color:const Color(0xFFEFEBEB),),
                   Container(
                     height: 45,
                     decoration: BoxDecoration(
                       borderRadius:const BorderRadius.only(bottomLeft: Radius.circular(12),bottomRight: Radius.circular(12)),
                       color: Colors.grey[200],
                     ),
                     child: MaterialButton(
                       shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                       padding:  const EdgeInsets.all(0),
                        child: Align(alignment: Alignment.center,child: Text('CHOOSESS_PHOTO'.tr(),style: ThemsConstands.subtitle1_regular_16.copyWith(color: Colorconstand.primaryColor))),
                        onPressed: onPressedGalary
                      ),
                   ),
                   Container(
                     height: 45,
                     margin:const EdgeInsets.only(top: 7,bottom: 12),
                     decoration: BoxDecoration(
                       borderRadius: BorderRadius.circular(12),
                       color: Colorconstand.neutralWhite,
                     ),
                     child: MaterialButton(
                       shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                       padding:  const EdgeInsets.all(0),
                        child: Align(alignment: Alignment.center,child: Text('CANCEL'.tr(),style: ThemsConstands.button_semibold_16.copyWith(color: Colorconstand.primaryColor,fontWeight: FontWeight.bold))),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                   ),
                ],
              ),
            ),
          );
  }
}

// Future showPicker(BuildContext context) {
//     return showModalBottomSheet(
//       backgroundColor: Colors.transparent,
//         context: context,
//         builder: (BuildContext bc) {
          
//         });
//   }