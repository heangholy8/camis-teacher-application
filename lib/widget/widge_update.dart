import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';
import '../app/core/resources/asset_resource.dart';
import '../app/core/thems/thems_constands.dart';
import '../app/help/check_platform_device.dart';

class WidgetUpdate extends StatefulWidget {
  const WidgetUpdate({Key? key}) : super(key: key);

  @override
  State<WidgetUpdate> createState() => _WidgetUpdateState();
}

class _WidgetUpdateState extends State<WidgetUpdate> {
  var platform = checkPlatformDevice();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colorconstand.neutralDarkGrey.withOpacity(0.6),
      child: Center(
        child: Container(
               margin:const EdgeInsets.all(42),
                height: 380,
                 decoration: BoxDecoration(
                        color: Colorconstand.neutralWhite,
                        borderRadius: BorderRadius.circular(15)
                      ),
                child: Column(
                  children: [
                    Container(
                      height: 150,
                      decoration:const BoxDecoration(
                        color: Colorconstand.neutralWhite,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight: Radius.circular(15))
                      ),
                      child: Center(
                        child: Container(
                          height: 80,width: 80,
                          decoration:const BoxDecoration(
                            color: Colorconstand.neutralGrey,
                            shape: BoxShape.circle
                          ),
                          padding:const EdgeInsets.all(10),
                          child: SvgPicture.asset(ImageAssets.logoCamis,width: 45,height: 45,color: Colorconstand.primaryColor,),
                        ),
                      ),
                    ),
                    Container(
                      child: Text("Update New!",style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.lightBlack),),
                    ),
                    const SizedBox(height: 22,),
                    Container(
                      margin:const EdgeInsets.symmetric(horizontal: 18),
                      child: Text("UPDATE_VERSION_DISCRIPTION".tr(),style: ThemsConstands.headline4_regular_18.copyWith(color: Colorconstand.lightBlack),textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 28,horizontal: 28),
                      height: 60,
                       child: MaterialButton(
                        color: Colorconstand.mainColorSecondary,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                        padding:const EdgeInsets.all(0),
                        onPressed: (){
                          setState(() {
                            if(platform=="Android"){
                              launchUrl(
                                Uri.parse("market://details?id=com.camemis.teacher_application",),
                                mode: LaunchMode.externalApplication,
                              );
                            }
                            else{
                              launchUrl(
                                Uri.parse("https://apps.apple.com/app/id6447053840",),
                                mode: LaunchMode.externalApplication,
                              );
                            }
                            
                          });
                        },
                        child: Center(
                          child: Text("Update Now",style: ThemsConstands.headline3_semibold_20.copyWith(color: Colorconstand.neutralWhite),),
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}