import 'package:flutter/material.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import 'package:flutter/services.dart';
class VideoYoutubeLearningScreen extends StatefulWidget {
  final String linkVideo;
  const VideoYoutubeLearningScreen({Key? key, required this.linkVideo}) : super(key: key);

  @override
  State<VideoYoutubeLearningScreen> createState() => _VideoYoutubeLearningScreenState();
}

class _VideoYoutubeLearningScreenState extends State<VideoYoutubeLearningScreen> {

  late YoutubePlayerController _controller;
  bool done = false;
  @override
  void initState() {
    setState(() {
        _controller = YoutubePlayerController(
        initialVideoId: YoutubePlayerController.convertUrlToId(widget.linkVideo)!,
        params: const YoutubePlayerParams(
          startAt:  Duration(minutes: 0, seconds: 00),
          showControls: true,
          autoPlay: false,
          enableCaption: false,
          mute:false,
          showFullscreenButton: true,
          showVideoAnnotations:true,
          //useHybridComposition:false,
          //privacyEnhanced:true,
          //strictRelatedVideos:true,
        )
      );
      _controller.onEnterFullscreen = () {
        SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
        ]);
      };
      _controller.onExitFullscreen = () {
        SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
      };
    });
    Future.delayed(const Duration(minutes: 1),(){
      setState(() {
        done = true;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return ClipRRect( 
      borderRadius: BorderRadius.circular(8),
      child: Center(child: YoutubePlayerControllerProvider(
        controller: _controller,
        child: LayoutBuilder(
          builder: (context, snapshot) {
            return YoutubePlayerIFrame(
              aspectRatio: 16 / 9,
              controller: _controller,
            );
          }
        ),
      )),
    );
  }
}