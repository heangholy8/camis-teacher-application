import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';

import 'display_video_youtube_learing.dart';

class ViewVideoYoutubeScreen extends StatefulWidget {
  final String linkVideoYoutube;
  const ViewVideoYoutubeScreen({super.key,required this.linkVideoYoutube});

  @override
  State<ViewVideoYoutubeScreen> createState() => _ViewVideoYoutubeScreenState();
}

class _ViewVideoYoutubeScreenState extends State<ViewVideoYoutubeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.neutralWhite,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: IconButton(onPressed: (){Navigator.of(context).pop();}, icon: const Icon(Icons.arrow_back_ios_new_rounded,color: Colorconstand.lightBlack,)),
            ),
            const SizedBox(height: 28,),
            Container(
              color: Colorconstand.lightBlack,
              child: VideoYoutubeLearningScreen(
               linkVideo: widget.linkVideoYoutube
              ),
            )
          ],
        ),
      ),
    );
  }
}