import 'package:flutter/material.dart';
import '../../app/core/constands/color_constands.dart';
import '../../app/core/thems/thems_constands.dart';

class WidgetDatePickerList extends StatefulWidget {
  const WidgetDatePickerList({Key? key}) : super(key: key);

  @override
  State<WidgetDatePickerList> createState() => _WidgetDatePickerListState();
}

class _WidgetDatePickerListState extends State<WidgetDatePickerList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colorconstand.neutralWhite,
      child: GestureDetector(
        onTap: (){},
        child: Column(
          children: [
            Container(
              margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 8),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      child:const Icon(Icons.arrow_back_ios_new_rounded,size: 20,color:Color(0xFF9899A6)),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Text(
                        "12345",
                        style: ThemsConstands.headline_5_semibold_16
                            .copyWith(
                                color: Colorconstand.primaryColor),textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      child:const Icon(Icons.arrow_forward_ios_rounded,size: 20,color:Color(0xFF9899A6),),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 18,),
            Container(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Text("ច័ន្ទ",textAlign: TextAlign.center,style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralDarkGrey),),
                          const SizedBox(height: 12,),
                          ListView.builder(
                            padding:const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (context, indexMO) {
                              return Container(
                               height: 40,width: 30,
                                child: GestureDetector(
                                  child: Container(
                                    child: Text(indexMO.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16,))),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Text("អង្គារ",textAlign: TextAlign.center,style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralDarkGrey),),
                          const SizedBox(height: 12,),
                          ListView.builder(
                            padding:const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (context, indexTU) {
                              return Container(
                               height: 40,width: 30,
                                child: GestureDetector(
                                  child: Container(
                                    child: Text(indexTU.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16,))),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Text("ពុធ",textAlign: TextAlign.center,style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralDarkGrey),),
                          const SizedBox(height: 12,),
                          ListView.builder(
                            padding:const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (context, indexWE) {
                              return Container(
                                   height: 40,width: 30,
                                    child: GestureDetector(
                                      child: Container(
                                        child: Text(indexWE.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16,))),
                                  );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Text("ព្រហ",textAlign: TextAlign.center,style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralDarkGrey),),
                          const SizedBox(height: 12,),
                          ListView.builder(
                            padding:const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (context, indexTH) {
                              return Container(
                                   height: 40,width: 30,
                                    child: GestureDetector(
                                      child: Container(
                                        child: Text(indexTH.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16,))),
                                  );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Text("សុក្រ",textAlign: TextAlign.center,style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralDarkGrey),),
                          const SizedBox(height: 12,),
                          ListView.builder(
                            padding:const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (context, indexFR) {
                              return Container(
                                   height: 40,width: 30,
                                    child: GestureDetector(
                                      child: Container(
                                        child: Text(indexFR.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16,))),
                                  );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Text("សៅរ៍",textAlign: TextAlign.center,style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.neutralDarkGrey),),
                          const SizedBox(height: 12,),
                          ListView.builder(
                            padding:const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (context, indexSA) {
                              return Container(
                                   height: 40,width: 30,
                                    child: GestureDetector(
                                      child: Container(
                                        child: Text(indexSA.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16,))),
                                  );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Text("អាទិត្យ",textAlign: TextAlign.center,style: ThemsConstands.caption_regular_12.copyWith(color: Colorconstand.lightTrunks),),
                          const SizedBox(height: 12,),
                          ListView.builder(
                            padding:const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (context, indexSU) {
                              return Container(
                                   height: 40,width: 30,
                                    child: GestureDetector(
                                      child: Container(
                                        child: Text(indexSU.toString(),textAlign: TextAlign.center,style: ThemsConstands.headline_5_semibold_16,))),
                                  );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}