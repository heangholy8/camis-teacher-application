import 'package:flutter/material.dart';

import '../app/core/thems/thems_constands.dart';



class DataEmptyWidget extends StatelessWidget {
  final String title;
  final String description;
  const DataEmptyWidget({Key? key, required this.title, required this.description}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Image.asset("assets/images/no_schedule_image.png",width: 200,height: 200,),
          Text(title,style: ThemsConstands.headline3_medium_20_26height.copyWith(color:const Color(0xff1e2123),),),
          Container(
            margin:const EdgeInsets.only(top: 8,left: 32,right: 32),
            child: Text(description,style: ThemsConstands.headline_6_regular_14_20height.copyWith(color:const Color(0xff828c95),),textAlign: TextAlign.center,),
          )
        ],
      ),
    );
  }
}