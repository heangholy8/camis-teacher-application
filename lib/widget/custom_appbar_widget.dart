import 'package:flutter/material.dart';

import '../app/core/thems/thems_constands.dart';

class CustomAppBarWidget extends StatelessWidget {
  final String title;
  final Widget? suffix;
  const CustomAppBarWidget({
    Key? key,
    required this.width,
    required this.title,
    this.suffix,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      padding: const EdgeInsets.only(bottom: 10,left: 10,right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.chevron_left,
                  size: 32,
                ),
              ),
            ],
          ),
          Text(
            title,
            style: ThemsConstands.headline3_semibold_20,
          ),
          Container(width: 35,), 
          //suffix ?? Container(),
        ],
      ),
    );
  }
}
