import 'package:flutter/material.dart';

class PopUpBottom extends StatefulWidget {
  // final Widget child;
  bool pop;
  PopUpBottom({super.key, /* required this.child */ this.pop = false});

  @override
  State<PopUpBottom> createState() => _PopUpBottomState();
}

class _PopUpBottomState extends State<PopUpBottom>
    with TickerProviderStateMixin {
  late AnimationController popAnimateController;

  @override
  void initState() {
    popAnimateController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 800));
    _runExpandCheck();
    super.initState();
  }

  void _runExpandCheck() {
    if (widget.pop) {
      popAnimateController.forward();
    } else {
      popAnimateController.reverse();
    }
  }

  @override
  void dispose() {
    popAnimateController.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(PopUpBottom oldWidget) {
    super.didUpdateWidget(oldWidget);
    _runExpandCheck();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: MaterialButton(
              onPressed: () {
                setState(() {
                  if (widget.pop == false) {
                    widget.pop = true;
                    popAnimateController.forward();
                  } else {
                    widget.pop = false;
                    
                    popAnimateController.reverse();
                  }
                });
              },
              child: const Text("Test"),
            ),
          ),
          SlideTransition(
            position: Tween<Offset>(begin: const Offset(0, 3), end: Offset.zero)
                .animate(popAnimateController),
            child: Container(
                margin: const EdgeInsets.only(bottom: 16),
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: 200,
                  height: 50,
                  color: Colors.black,
                )),
          ),
        ],
      ),
    );
  }
}
