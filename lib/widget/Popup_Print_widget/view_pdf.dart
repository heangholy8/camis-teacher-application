import 'dart:typed_data';

import 'package:camis_teacher_application/app/mixins/toast.dart';
import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
import 'package:no_screenshot/no_screenshot.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import '../../app/help/save_file_helper.dart';
import '../../app/modules/create_announcement_screen/widgets/type_announcement_create.dart';

class viewPDFReportWidget extends StatefulWidget {
  String? state;
  final String fileName;
   viewPDFReportWidget({super.key,required this.state,required this.fileName});

  @override
  State<viewPDFReportWidget> createState() => _viewPDFReportWidgetState();
}

class _viewPDFReportWidgetState extends State<viewPDFReportWidget> with Toast {
  final _noScreenshot = NoScreenshot.instance;

  void enableScreenshot() async {
    bool result = await _noScreenshot.screenshotOn();
    debugPrint('Enable Screenshot: $result');
  }

  void disableScreenshot() async {
    bool result = await _noScreenshot.screenshotOff();
    debugPrint('Screenshot Off: $result');
  }
  void listenForScreenshot() {
    _noScreenshot.screenshotStream.listen((value) {
       print("fskdaflkasdmfld$value");
      if (value.wasScreenshotTaken){
        print("fskdaflkasdmfld");
        showErrorDialog(() {
          Navigator.of(context).pop();
        },context,title:"FAIL".tr(),description: "FAIL_SAVE_ATTENDANCE".tr());
      }
    });
  }
  @override
  void initState() {
    // disableScreenshot();
    super.initState();
    listenForScreenshot();
  }

  @override
  void dispose() {
    // enableScreenshot();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
      return FractionallySizedBox(
        heightFactor: 1,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin:const EdgeInsets.only(right: 8,top: 5),
              child: SizedBox(height: 80,width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(),
                    IconButton(onPressed: (){
                      Navigator.pop(context);
                    }, icon: SvgPicture.asset(ImageAssets.CLOSE_ICON,color: Colors.black,))
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                child: SfPdfViewer.memory(
                  Uint8List.fromList(widget.state!.codeUnits),
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 19, vertical: 24),
              decoration: const BoxDecoration(
                color: Colorconstand.neutralBtnBg,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0),
                ),
              ),
              child: Container(
                width: 250,
                child: TypeAnnouncementCreate(
                  svgAsset: ImageAssets.share_icon,
                  title: "SHARE".tr(),
                  onPressed: () async{
                    await sharePDF(pdfBytes: Uint8List.fromList(widget.state!.codeUnits),context: context,filename: widget.fileName == null || widget.fileName == ""?"របាយការណ៍ ${DateTime.now().millisecond}":"${widget.fileName.toString()} ${DateTime.now().millisecond}");
                  },
                ),
              ),
            ),
        
          ],
        ),
      );
  }
}