import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:flutter/material.dart';

class BuildBottomSheet extends StatelessWidget {
  final Widget? child;
  final Widget? expanded;
  final double? initialChildSize;
  const BuildBottomSheet({ Key? key, this.child,this.expanded,required this.initialChildSize }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      initialChildSize: initialChildSize!,
      minChildSize: 0.2,
      maxChildSize: 0.9,
      builder: (_,controller)=> Container(
        padding:const EdgeInsets.only(top:12.0,),
        
        decoration:const BoxDecoration(
          color: Colorconstand.neutralWhite,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight: Radius.circular(15))
        ),
        child: Column(
          children: [
            Container(
               margin:const EdgeInsets.only(bottom: 12.0),
              height:4,
              width: MediaQuery.of(context).size.width/7,
               decoration: BoxDecoration(
                 color: Colorconstand.neutralDarkGrey.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(2)
                ),
            ),
            Expanded(
              flex: 1,
              child: ListView(
                controller: controller,
                children: [
                  child!,
                  expanded!,
                ],
              ),
            ),
          ],
        ),
      ),
      
    );
  }
}