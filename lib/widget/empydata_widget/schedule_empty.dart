import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';

class ScheduleEmptyWidget extends StatelessWidget {
  final String? title;
  final String? subTitle;
  const ScheduleEmptyWidget({Key? key,required this.subTitle,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Column(
          children: [
            Container(
              child: Image.asset("assets/images/no_schedule_image.png",width: 200,height: 200,),
            ),
            Container(
              child: Text(title!,style: ThemsConstands.headline3_medium_20_26height.copyWith(color:const Color(0xff1e2123),),textAlign: TextAlign.center,),
            ),
            Container(
              margin:const EdgeInsets.only(top: 8,left: 32,right: 32),
              child: Text(subTitle!,style: ThemsConstands.headline_6_regular_14_20height.copyWith(color:const Color(0xff828c95),),textAlign: TextAlign.center,),
            )
          ],
        ),
      ),
    );
  }
}