

import 'package:flutter/material.dart';

import '../../app/core/constands/color_constands.dart';
import '../../app/core/thems/thems_constands.dart';

class EmptyWidget extends StatelessWidget {
  final String title;
  final String subtitle;
  const EmptyWidget({Key? key, required this.title, required this.subtitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 22, vertical: 12),
      child: Column(
        children: [
          Text(
            title,
            style: ThemsConstands.headline_5_semibold_16.copyWith(color: Colorconstand.lightBlack),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 22,
          ),
          subtitle == ""?Container():Text(
            subtitle,
            style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsPlaceholder),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}