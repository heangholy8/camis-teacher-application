import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';

class TopBarOneLineWidget extends StatelessWidget {
  final String titleTopBar;
  const TopBarOneLineWidget({super.key, required this.titleTopBar});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
    GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: const Icon(
        Icons.arrow_back_ios_new,
        size: 24,
        color: Colorconstand.darkTextsRegular,
      ),
    ),
    Expanded(
        child: Text(
      titleTopBar,
      style: ThemsConstands.headline3_semibold_20.copyWith(
        color: Colorconstand.neutralDarkGrey,
      ),
      textAlign: TextAlign.center,
    )),
    Container(width: 30,)
      ],
    );
  }
}
