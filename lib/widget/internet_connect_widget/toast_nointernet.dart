import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../bottom_sheet_widget/model_bottomSheet.dart';


void showtoastInternet(context){
  showModalBottomSheet(
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    context: context,
    isDismissible: true,
    builder: (context) =>BuildBottomSheet(
      initialChildSize: 0.45,
      child: Container(
        margin: const EdgeInsets.only(top: 18.0),
       child: Image.asset(
          "assets/images/no_connection.gif",
          width: MediaQuery.of(context).size.width/3,
          height: MediaQuery.of(context).size.height/3,
        ),
      ),
      expanded: Container(
        alignment: Alignment.center,
        child: Text("មិនមានការតភ្ជាប់អ៊ីនធឺណិត".tr()+"!",style: ThemsConstands.headline_5_medium_16),
      ),
      
    ),
  );
}