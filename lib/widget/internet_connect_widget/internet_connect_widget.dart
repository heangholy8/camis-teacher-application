import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';

class NoConnectWidget extends StatelessWidget {
  const NoConnectWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      decoration:const BoxDecoration(
        color: Colorconstand.neutralWhite,
        boxShadow: [
          BoxShadow(
            color: Color(0xFFD5D2D2),
            blurRadius: 8.0, // soften the shadow
            spreadRadius: 5.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 5  horizontally
              5.0, // Move to bottom 5 Vertically
            ),
          )
        ],
      ),
      child: Row(
        children: [
          Container(
            child: Image.asset("assets/images/NoInternet_connect.png",height: 120,width: 120,),
          ),
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text("គ្មានការតភ្ជាប់អ៊ីនធឺណិត",style: ThemsConstands.headline_4_medium_18.copyWith(color: Colorconstand.lightBlack,)),
                  ),
                  const SizedBox(height: 12,),
                  Container(
                    child: Text("សូមពិនិត្យមើល់អ៊ីនធឺណិតរបស់អ្នក ហើយព្យាយាមម្តងទៀត",style: ThemsConstands.headline_6_regular_14_20height.copyWith(color: Colorconstand.darkTextsPlaceholder,)),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}