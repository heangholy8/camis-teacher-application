import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/home_screen/view/home_screen.dart';
import 'package:camis_teacher_application/app/modules/more_screen/view/more_screen.dart';
import 'package:camis_teacher_application/app/modules/primary_school_screen/list_enter_score_primary/list_subject_enter_score_primary_screen.dart';
import 'package:flutter/material.dart';
import '../../app/modules/list_subject_enter_score_screen/view/list_subject_and_class_enter_score_screen.dart';
import '../../app/modules/schedule_screen/view/schedule_screen.dart';
import '../../app/modules/time_line/time_line.dart';
import '../../app/storages/get_storage.dart';

class BottomNavigateBar extends StatefulWidget {
  final int isActive;
  const BottomNavigateBar({Key? key, required this.isActive}) : super(key: key);

  @override
  State<BottomNavigateBar> createState() => _BottomNavigateBarState();
}

class _BottomNavigateBarState extends State<BottomNavigateBar> {
  String? roleLogin;
  bool isPrimary = false;

  void getLocalData() async{
     GetStoragePref _prefs = GetStoragePref();
     var dataaccess = await _prefs.getJsonToken;
     setState(() {
       if(dataaccess.authUser == null){
        }
        else{
          roleLogin = dataaccess.authUser!.role.toString();
          print("1234567890$roleLogin");
        }
        isPrimary = dataaccess.isPrimary!;
     });
  }
   
  @override
  void initState() {
    getLocalData();
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    return Container(
      constraints: const BoxConstraints(
          maxHeight: 90,
          minHeight: 86,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(12), topRight: Radius.circular(12)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 0.5,
            blurRadius: 9,
            offset: const Offset(2, 2),
          )
        ],
      ),
      child: Column(

        children: [
        //  const BTSScoreWidget(),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 1) {
                          Navigator.pushReplacement(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => const HomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),
                          );
                        
                      } else {}
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 1
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 1
                                ? ImageAssets.home_icon
                                : ImageAssets.home_outline_icon),
                          ),
                          Expanded(
                            child: Text("HOMENAV".tr(),
                                style: ThemsConstands.overline_semibold_12
                                    .copyWith(
                                        color: widget.isActive == 1
                                            ? Colorconstand.primaryColor
                                            : Colorconstand.neutralDarkGrey,
                                        fontWeight: widget.isActive == 1
                                            ? FontWeight.w700
                                            : FontWeight.w400,
                                        height: 1),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 2) {
                            Navigator.pushReplacement(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                 ScheduleScreen(isPrimary: isPrimary,),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),
                          );
                      } else {}
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 2
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 2
                                ? ImageAssets.task_icon
                                : ImageAssets.task_outline_icon),
                          ),
                          Expanded(
                            child: Text("SCHEDULENAV".tr(),
                              style: ThemsConstands.overline_semibold_12
                                .copyWith(
                                  color: widget.isActive == 2 ? Colorconstand.primaryColor : Colorconstand.neutralDarkGrey,
                                  fontWeight: widget.isActive == 2 ? FontWeight.w700 : FontWeight.w400,
                                  height: 1,
                                ),
                            textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 3) {
                    //========= Check is Primary School ==============
                        if(isPrimary == true){
                          Navigator.pushReplacement(
                              context,
                              PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) =>
                                    const ListSubjectEnterScorePrimaryScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                            );
                        }
                        else{
                          
                            Navigator.pushReplacement(
                              context,
                              PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) =>
                                    const ListSubjectEnterScoreScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                            );
                        }
                      } else {}
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 3
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 3
                                ? ImageAssets.Exam_fill
                                : ImageAssets.EXAM_ICON,color:widget.isActive == 3?Colorconstand.primaryColor :Colorconstand.neutralDarkGrey,width: 25,),
                          ),
                          Expanded(
                            child: Text("SCORE".tr(),
                                style: ThemsConstands.overline_semibold_12
                                    .copyWith(
                                        color: widget.isActive == 3
                                            ? Colorconstand.primaryColor
                                            : Colorconstand.neutralDarkGrey,
                                        fontWeight: widget.isActive == 3
                                            ? FontWeight.w700
                                            : FontWeight.w400,
                                        height: 1),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                // Class Timeline Tab
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 4) {
                        Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 0,role: "2",isPrinciple: false,),
                            transitionDuration: Duration.zero,
                            reverseTransitionDuration: Duration.zero,
                          ),
                        );
                      }
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 4
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 4
                                ? ImageAssets.fill_timeline_icon
                                : ImageAssets.timeline_icon),
                          ),
                          Expanded(
                            child: Text(
                              "TIMELINE".tr(),
                              style: ThemsConstands.overline_semibold_12.copyWith(
                                  color: widget.isActive == 4
                                      ? Colorconstand.primaryColor
                                      : Colorconstand.neutralDarkGrey,
                                  fontWeight: widget.isActive == 4
                                      ? FontWeight.w700
                                      : FontWeight.w400,
                                  height: 1),textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                // Expanded(
                //   child: MaterialButton(
                //     padding:const EdgeInsets.all(0),
                //     splashColor: Colors.transparent,
                //     highlightColor: Colors.transparent,
                //     onPressed:(){
                //       if(widget.isActive!=3){
                //       Navigator.pushReplacement(
                //         context,
                //         PageRouteBuilder(
                //             pageBuilder: (context, animation1, animation2) =>const NotifictaionScreen(),
                //             transitionDuration: Duration.zero,
                //             reverseTransitionDuration: Duration.zero,
                //           ),
                //         );
                //       } else {}
                //     },
                //     child: Container(
                //       child: Column(
                //         children: [
                //           Container(
                //             width: 60,
                //             height: 4,
                //             decoration: BoxDecoration(
                //                 color: widget.isActive == 3
                //                     ? Colorconstand.primaryColor
                //                     : Colors.white,
                //                 borderRadius: BorderRadius.circular(6)),
                //           ),
                //           Container(
                //             margin: const EdgeInsets.symmetric(vertical: 12),
                //             child: SvgPicture.asset(widget.isActive == 3
                //                 ? ImageAssets.notification_icon
                //                 : ImageAssets.notification_outline_icon),
                //           ),
                //           Container(
                //             child: Text("ជូនដំណឹង",
                //                 style: ThemsConstands.overline_semibold_12
                //                     .copyWith(
                //                         color: widget.isActive == 3
                //                             ? Colorconstand.primaryColor
                //                             : Colorconstand.neutralDarkGrey,
                //                         fontWeight: widget.isActive == 3
                //                             ? FontWeight.w700
                //                             : FontWeight.w400,
                //                         height: 1)),
                //           ),
                //         ],
                //       ),
                //     ),
                //   ),
                // ),

                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 5) {
                        Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) =>
                              MoreScreen(role: roleLogin,isPrimart: isPrimary,),
                            transitionDuration: Duration.zero,
                            reverseTransitionDuration: Duration.zero,
                          ),
                        );
                      } else {}
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 5
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 5
                                ? ImageAssets.more_icon
                                : ImageAssets.more_outline_icon),
                          ),
                          Expanded(
                            child: Text("MORENAV".tr(),
                                style: ThemsConstands.overline_semibold_12
                                    .copyWith(
                                        color: widget.isActive == 5
                                            ? Colorconstand.primaryColor
                                            : Colorconstand.neutralDarkGrey,
                                        fontWeight: widget.isActive == 5
                                            ? FontWeight.w700
                                            : FontWeight.w400,
                                        height: 1),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
