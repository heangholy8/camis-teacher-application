import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:camis_teacher_application/app/modules/more_screen/view/more_screen.dart';
import 'package:camis_teacher_application/app/modules/principle/list_check_class_input_score_screen/view/check_class_input_score_screen.dart';
import 'package:camis_teacher_application/app/modules/study_affaire_screen/list_attendance_teacher/view/list_attendance_teacher_screen.dart';
import 'package:flutter/material.dart';
import '../../app/modules/principle/principle_home_screen/view/principle_home_screen.dart';

class BottomNavigateBarPrinciple extends StatefulWidget {
  final int isActive;
  const BottomNavigateBarPrinciple({Key? key, required this.isActive}) : super(key: key);

  @override
  State<BottomNavigateBarPrinciple> createState() => _BottomNavigateBarPrincipleState();
}

class _BottomNavigateBarPrincipleState extends State<BottomNavigateBarPrinciple> {
 
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    return Container(
      constraints: const BoxConstraints(
          maxHeight: 90,
          minHeight: 86,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(12), topRight: Radius.circular(12)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 0.5,
            blurRadius: 9,
            offset: const Offset(2, 2),
          )
        ],
      ),
      child: Column(

        children: [
        //  const BTSScoreWidget(),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 1) {
                          Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                  const PrincipleHomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),(route) => false);
                      } else {}
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 1
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 1
                                ? ImageAssets.home_icon
                                : ImageAssets.home_outline_icon),
                          ),
                          Expanded(
                            child: Text("HOMENAV".tr(),
                                style: ThemsConstands.overline_semibold_12
                                    .copyWith(
                                        color: widget.isActive == 1
                                            ? Colorconstand.primaryColor
                                            : Colorconstand.neutralDarkGrey,
                                        fontWeight: widget.isActive == 1
                                            ? FontWeight.w700
                                            : FontWeight.w400,
                                        height: 1),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      Navigator.pushReplacement(
                        context,
                        PageRouteBuilder(
                          pageBuilder: (context, animation1, animation2) =>
                            ListAttendanceTeacherScreen(principle: true),
                          transitionDuration: Duration.zero,
                          reverseTransitionDuration: Duration.zero,
                        ),
                      );
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 2
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 2
                                ? ImageAssets.task_icon
                                : ImageAssets.task_outline_icon),
                          ),
                          Expanded(
                            child: Text("NOT_TAUGHT".tr(),
                              style: ThemsConstands.overline_semibold_12
                                .copyWith(
                                  color: widget.isActive == 2 ? Colorconstand.primaryColor : Colorconstand.neutralDarkGrey,
                                  fontWeight: widget.isActive == 2 ? FontWeight.w700 : FontWeight.w400,
                                  height: 1,
                                ),
                            textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 3) {
                        Navigator.pushReplacement(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                CheckClassInputScoreScreen(role: "6",),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),
                          );
                      } else {}
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 3
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 3
                                ? ImageAssets.Exam_fill
                                : ImageAssets.EXAM_ICON,width: 26,color:  widget.isActive == 3 ? Colorconstand.primaryColor : Colorconstand.neutralDarkGrey,),
                          ),
                          Expanded(
                            child: Text("តារាងស្រង់ពិន្ទុ".tr(),
                              style: ThemsConstands.overline_semibold_12
                                .copyWith(
                                  color: widget.isActive == 3 ? Colorconstand.primaryColor : Colorconstand.neutralDarkGrey,
                                  fontWeight: widget.isActive == 3 ? FontWeight.w700 : FontWeight.w400,
                                  height: 1,
                                ),
                            textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                // Class Timeline Tab
                // Expanded(
                //   child: MaterialButton(
                //     padding: const EdgeInsets.all(0),
                //     splashColor: Colors.transparent,
                //     highlightColor: Colors.transparent,
                //     onPressed: () {
                //       if (widget.isActive != 4) {
                //         Navigator.pushReplacement(
                //           context,
                //           PageRouteBuilder(
                //             pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 0),
                //             transitionDuration: Duration.zero,
                //             reverseTransitionDuration: Duration.zero,
                //           ),
                //         );
                //       }
                //     },
                //     child: Container(
                //       height: 90,
                //       child: Column(
                //         children: [
                //           Container(
                //             width: 60,
                //             height: 4,
                //             decoration: BoxDecoration(
                //                 color: widget.isActive == 4
                //                     ? Colorconstand.primaryColor
                //                     : Colors.white,
                //                 borderRadius: BorderRadius.circular(6)),
                //           ),
                //           Container(
                //             margin: const EdgeInsets.symmetric(vertical: 12),
                //             child: SvgPicture.asset(widget.isActive == 4
                //                 ? ImageAssets.fill_timeline_icon
                //                 : ImageAssets.timeline_icon),
                //           ),
                //           Expanded(
                //             child: Text(
                //               "TIMELINE".tr(),
                //               style: ThemsConstands.overline_semibold_12.copyWith(
                //                   color: widget.isActive == 4
                //                       ? Colorconstand.primaryColor
                //                       : Colorconstand.neutralDarkGrey,
                //                   fontWeight: widget.isActive == 4
                //                       ? FontWeight.w700
                //                       : FontWeight.w400,
                //                   height: 1),textAlign: TextAlign.center,
                //             ),
                //           ),
                //         ],
                //       ),
                //     ),
                //   ),
                // ),
                // const SizedBox(
                //   width: 15,
                // ),
                Expanded(
                  child: MaterialButton(
                    padding: const EdgeInsets.all(0),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onPressed: () {
                      if (widget.isActive != 5) {
                        Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) =>
                                MoreScreen(role: "1",isPrimart: false,),
                            transitionDuration: Duration.zero,
                            reverseTransitionDuration: Duration.zero,
                          ),
                        );
                      } else {}
                    },
                    child: Container(
                      height: 90,
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 5
                                    ? Colorconstand.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 5
                                ? ImageAssets.more_icon
                                : ImageAssets.more_outline_icon),
                          ),
                          Expanded(
                            child: Text("MORENAV".tr(),
                                style: ThemsConstands.overline_semibold_12
                                    .copyWith(
                                        color: widget.isActive == 5
                                            ? Colorconstand.primaryColor
                                            : Colorconstand.neutralDarkGrey,
                                        fontWeight: widget.isActive == 5
                                            ? FontWeight.w700
                                            : FontWeight.w400,
                                        height: 1),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
