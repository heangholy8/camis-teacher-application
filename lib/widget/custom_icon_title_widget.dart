// ignore_for_file: must_be_immutable

import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/core/thems/thems_constands.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IconTitleWidget extends StatelessWidget {
  final String icon;
  final String title;
   Widget? examDate;
   IconTitleWidget({
    super.key,
    required this.icon,
    required this.title,
    required this.subjectColor,
     this.examDate
  });

  final Color? subjectColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset(
          icon,
          height: 18,
          color: subjectColor ?? Colorconstand.primaryColor,
        ),
        const SizedBox(
          width: 8.0,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              overflow: TextOverflow.ellipsis,
              style: ThemsConstands.button_semibold_16
                  .copyWith(color: subjectColor ?? Colorconstand.primaryColor),
            ),
           const SizedBox(height: 8,),
           examDate ?? Container()
            // Text(
            //   examDate,
            //    overflow: TextOverflow.ellipsis,
            //   style: examStyle ?? ThemsConstands.button_semibold_16
            //       .copyWith(color: Colorconstand.lightBulma),
            // ),
          ],
        ),
      ],
    );
  }
}
