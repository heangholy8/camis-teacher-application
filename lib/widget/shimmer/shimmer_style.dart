import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerChild extends StatelessWidget {
  const ShimmerChild({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.symmetric(horizontal: 20,),
      child: Shimmer.fromColors(
        baseColor:const Color(0xFF3688F4),
        highlightColor:const Color(0xFF3BFFF5),
        child:Container(
          margin:const EdgeInsets.only(bottom: 5),
          child: Row(
            children: [
              Container(
                height: 65,
                width: 66,
                decoration: const BoxDecoration(
                  color: Color(0x622195F3),
                  shape: BoxShape.circle,
                ),
              ),
              const SizedBox(width: 10.0,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 20,
                    width: MediaQuery.of(context).size.width/2.5,
                    decoration:  BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color:const Color(0x622195F3),
                    ),
                  ),
                  const SizedBox(height: 10,),
                  Container(
                    height: 20,
                    width: MediaQuery.of(context).size.width/3,
                    decoration:  BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color:const Color(0x622195F3),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ShimmerTimeLine extends StatelessWidget {
  const ShimmerTimeLine({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.only(top: 35,),
      child: Shimmer.fromColors(
        baseColor:const Color(0xFF3688F4),
        highlightColor:const Color(0xFF3BFFF5),
        child:Container(
          margin:const EdgeInsets.only(bottom: 5),
          child: Column(
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 18,),
                child: Row(
                  children: [
                    Container(
                      height: 65,
                      width: 66,
                      decoration: const BoxDecoration(
                        color: Color(0x622195F3),
                        shape: BoxShape.circle,
                      ),
                    ),
                    const SizedBox(width: 7.0,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height:17,
                          width: MediaQuery.of(context).size.width/2.5,
                          decoration:  BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color:const Color(0x622195F3),
                          ),
                        ),
                        const SizedBox(height: 5,),
                        Container(
                          height: 15,
                          width: MediaQuery.of(context).size.width/3,
                          decoration:  BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color:const Color(0x622195F3),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.all(18.0),
                height: 60,
                width: MediaQuery.of(context).size.width,
                decoration:  BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color:const Color(0x622195F3),
                ),
              ),
              Container(
                height: 230,
                width: MediaQuery.of(context).size.width,
                color:const Color(0x622195F3),
              )
                      ],
          ),
        ),
      ),
    );
  }
}

class ShimmerAbsentRequest extends StatelessWidget {
  const ShimmerAbsentRequest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.only(top: 5,),
      child: Shimmer.fromColors(
        baseColor:const Color(0xFF3688F4),
        highlightColor:const Color(0xFF3BFFF5),
        child:Container(
          margin:const EdgeInsets.only(bottom: 5),
          child: Container(
          decoration: BoxDecoration(
             color:const Color(0x622195F3),
            borderRadius: BorderRadius.circular(12.0)
          ),
            height: 150,
            width: MediaQuery.of(context).size.width,
           
          ),
        ),
      ),
    );
  }
}
class ShimmerButtonDropDown extends StatelessWidget {
  const ShimmerButtonDropDown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.only(top: 5,),
      child: Shimmer.fromColors(
        baseColor:const Color(0xFF3688F4),
        highlightColor:const Color(0xFF3BFFF5),
        child:Container(
          margin:const EdgeInsets.only(bottom: 5),
          child: Container(
          decoration: BoxDecoration(
             color:const Color(0x622195F3),
            borderRadius: BorderRadius.circular(12.0)
          ),
            height: 55,
            width: MediaQuery.of(context).size.width,
           
          ),
        ),
      ),
    );
  }
}
class ShimmerTimeTable extends StatelessWidget {
  const ShimmerTimeTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.only(top: 5,),
      child: Shimmer.fromColors(
        baseColor:const Color(0xFF3688F4),
        highlightColor:const Color(0xFF3BFFF5),
        child:Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin:const EdgeInsets.only(top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration:const BoxDecoration(
                      color: Color(0x622195F3),
                      borderRadius: BorderRadius.only(bottomRight: Radius.circular(12.0),topRight:Radius.circular(12.0))
                    ),
                      height: 12,
                      width: 25,
                  ),
                  Expanded(
                    child: Container(
                      margin:const EdgeInsets.only(left: 12.0,right: 22),
                      decoration: BoxDecoration(
                        color:const Color(0x622195F3),
                        borderRadius: BorderRadius.circular(6.0)
                      ),
                        height: 22,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin:const EdgeInsets.symmetric(vertical: 15,horizontal: 22),
              child: Container(
                decoration: BoxDecoration(
                  color:const Color(0x622195F3),
                  borderRadius: BorderRadius.circular(12.0)
                ),
                  height: 160,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
class ShimmerDetailPayment extends StatelessWidget {
  const ShimmerDetailPayment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin:const EdgeInsets.only(top: 5,),
        child: Shimmer.fromColors(
          baseColor:const Color(0xFF3688F4),
          highlightColor:const Color(0xFF3BFFF5),
          child:Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                decoration:const BoxDecoration(
                  color: Color(0x622195F3),
                  borderRadius: BorderRadius.all(Radius.circular(12))
                ),
                  height: 25,
              ),
              const SizedBox(height: 20,),
              Container(
                margin:const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration:const BoxDecoration(
                        color: Color(0x622195F3),
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 25,
                        width: 65,
                    ),
                    Container(
                      decoration:const BoxDecoration(
                        color: Color(0x622195F3),
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 25,
                        width: 105,
                    ),
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.symmetric(vertical: 15,horizontal: 0),
                child: Container(
                  decoration: BoxDecoration(
                    color:const Color(0x622195F3),
                    borderRadius: BorderRadius.circular(12.0)
                  ),
                    height: 300,
                ),
              ),
    
              const SizedBox(height: 15,),
              Container(
                decoration:const BoxDecoration(
                  color: Color(0x622195F3),
                  borderRadius: BorderRadius.all(Radius.circular(12))
                ),
                  height: 200,
                  width: 200,
              ),
              const SizedBox(height: 15,),
    
              Container(
                decoration:const BoxDecoration(
                  color: Color(0x622195F3),
                  borderRadius: BorderRadius.all(Radius.circular(12))
                ),
                  height: 25,
              ),
              const SizedBox(height: 15,),
              Container(
                decoration:const BoxDecoration(
                  color: Color(0x622195F3),
                  borderRadius: BorderRadius.all(Radius.circular(12))
                ),
                  height: 25,
              ),
              const SizedBox(height: 15,),
              Container(
                decoration:const BoxDecoration(
                  color: Color(0x622195F3),
                  borderRadius: BorderRadius.all(Radius.circular(12))
                ),
                  height: 25,
              ),
              Container(
                margin:const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration:const BoxDecoration(
                        color: Color(0x622195F3),
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 25,
                        width: 85,
                    ),
                    const SizedBox(width: 10,),
                    Container(
                      decoration:const BoxDecoration(
                        color: Color(0x622195F3),
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 25,
                        width: 105,
                    ),
                    const SizedBox(width: 10,),
                    Container(
                      decoration:const BoxDecoration(
                        color: Color(0x622195F3),
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 25,
                        width: 105,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ShimmerComment extends StatelessWidget {
  const ShimmerComment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin:const EdgeInsets.only(top: 5,),
        child: Shimmer.fromColors(
          baseColor: Colorconstand.neutralGrey,
          highlightColor:Colorconstand.neutralDarkGrey.withOpacity(0.1),
          child:Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin:const EdgeInsets.only(left: 12.0,top: 12.0,bottom: 12.0),
                child: Row(
                  children: [
                    Container(
                      decoration:const BoxDecoration(
                        color: Colorconstand.neutralGrey,
                        shape: BoxShape.circle
                      ),
                        height: 45,
                        width: 45,
                    ),
                    const SizedBox(width: 12.0,),
                    Container(
                      decoration:const BoxDecoration(
                        color: Colorconstand.neutralGrey,
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 75,
                        width: 150,
      
                    ),
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.only(left: 65,bottom: 12.0),
                child: Row(
                  children: [
                    Container(
                      decoration:const BoxDecoration(
                        color: Colorconstand.neutralGrey,
                        shape: BoxShape.circle
                      ),
                        height: 45,
                        width: 45,
                    ),
                    const SizedBox(width: 12.0,),
                    Container(
                      decoration:const BoxDecoration(
                        color: Colorconstand.neutralGrey,
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 75,
                        width: 100,
      
                    ),
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.only(left: 12.0,top: 12.0,bottom: 12.0),
                child: Row(
                  children: [
                    Container(
                      decoration:const BoxDecoration(
                        color: Colorconstand.neutralGrey,
                        shape: BoxShape.circle
                      ),
                        height: 45,
                        width: 45,
                    ),
                    const SizedBox(width: 12.0,),
                    Container(
                      decoration:const BoxDecoration(
                        color: Colorconstand.neutralGrey,
                        borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                        height: 75,
                        width: 150,
      
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
//========================= Daskboard =============================
class ShimmerDaskBoardService extends StatelessWidget {
  const ShimmerDaskBoardService({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
            baseColor: const Color(0xFF3688F4),
            highlightColor: const Color(0xFF3BFFF5), 
            child: Column(
              children: [
                Container(
                  margin:const EdgeInsets.only(top: 28,left: 18,right: 18),
                  child:Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 15,
                        width: 75,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: const Color(0x622195F3),
                        ),
                      ),
                      Container(
                        height: 15,
                        width: 75,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: const Color(0x622195F3),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin:const EdgeInsets.only(top: 28,left: 18,right: 18),
                  height: 85,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: const Color(0x622195F3),
                  ),
                ),
              ],
            ),
          );
  }
}

class ShimmerDaskBoardTask extends StatelessWidget {
  const ShimmerDaskBoardTask({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
            baseColor: const Color(0xFF3688F4),
            highlightColor: const Color(0xFF3BFFF5), 
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  margin:const EdgeInsets.only(top: 28,left: 18,right: 18),
                  child:Container(
                    height: 15,
                    width: 75,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: const Color(0x622195F3),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(top: 22,left: 18,right: 8),
                        height: 185,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: const Color(0x622195F3),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(top: 22,left: 8,right: 18),
                        height: 185,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: const Color(0x622195F3),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
  }
}

class ShimmerDaskBoardQuickType extends StatelessWidget {
  const ShimmerDaskBoardQuickType({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
            baseColor: const Color(0xFF3688F4),
            highlightColor: const Color(0xFF3BFFF5), 
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  margin:const EdgeInsets.only(top: 28,left: 18,right: 18),
                  child:Container(
                    height: 15,
                    width: 75,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: const Color(0x622195F3),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(top: 22,left: 18,right: 8),
                        height: 80,
                        width: 80,
                        decoration:const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0x622195F3),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(top: 22,left: 8,right: 18),
                        height: 80,
                        width: 80,
                        decoration:const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0x622195F3),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(top: 22,left: 8,right: 18),
                        height: 80,
                        width: 80,
                        decoration:const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0x622195F3),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(top: 22,left: 8,right: 18),
                        height: 80,
                        width: 80,
                        decoration:const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0x622195F3),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
  }
}
//========================= End Daskboard =============================

//================= More =========================

class ShimmerProfileLoading extends StatelessWidget {
  const ShimmerProfileLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Shimmer.fromColors(
          baseColor: const Color(0xFF3688F4),
          highlightColor:
              const Color(0xFF3BFFF5),
          child: Container(
            height: 75,
            width: 75,
            decoration: const BoxDecoration(
                color: Color(0x622195F3),
                shape: BoxShape.circle),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
              top: 19, bottom: 10),
          alignment: Alignment.center,
          child: Shimmer.fromColors(
            baseColor: const Color(0xFF3688F4),
            highlightColor:
                const Color(0xFF3BFFF5),
            child: Container(
              height: 40,
              width: 175,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(16),
                color: const Color(0x622195F3),
              ),
            ),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Shimmer.fromColors(
            baseColor: const Color(0xFF3688F4),
            highlightColor:
                const Color(0xFF3BFFF5),
            child: Container(
              height: 30,
              width: 275,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(16),
                color: const Color(0x622195F3),
              ),
            ),
          ),
        ),
        const SizedBox(height: 12,),
        Container(
          alignment: Alignment.center,
          child: Shimmer.fromColors(
            baseColor: const Color(0xFF3688F4),
            highlightColor:
                const Color(0xFF3BFFF5),
            child: Container(
              height: 35,
              width: 150,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(16),
                color: const Color(0x622195F3),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

//====================== End ===========================

