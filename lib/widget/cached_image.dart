import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CachedImageNetwork extends StatelessWidget {
  final String urlImage;
  const CachedImageNetwork({Key? key, required this.urlImage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: urlImage.toString(),
      fit: BoxFit.cover,
      width: MediaQuery.of(context).size.width,
      placeholder: (context, url) =>  Center(child: Container()),
      errorWidget: (context, url, error) {
        return CachedNetworkImage(imageUrl: urlImage.toString());
      },
    );
  }
}