import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../app/core/constands/color_constands.dart';
import '../app/core/resources/asset_resource.dart';
import '../app/core/thems/thems_constands.dart';

class ClassMonitorTagWidget extends StatelessWidget {
  String title;
   ClassMonitorTagWidget({super.key,required this.title});

  @override
  Widget build(BuildContext context) {
       return Container(
      padding: const EdgeInsets.all(8),
      child: Container(
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: Colorconstand.neutralGrey,
          ),
        ),
        child: Row(
          children: [
            SvgPicture.asset(
              ImageAssets.security_icon,
              width: 24,
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              title,
              style: ThemsConstands.caption_regular_12.copyWith(
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  
  }
}