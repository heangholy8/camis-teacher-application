import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../app/core/constands/color_constands.dart';
import '../../app/core/resources/asset_resource.dart';
import '../../app/modules/create_announcement_screen/widgets/pdf_viewer.dart';

class PdfFileViewScreen extends StatefulWidget {
  final File pdfUrlFile;
  const PdfFileViewScreen({super.key,required this.pdfUrlFile});

  @override
  State<PdfFileViewScreen> createState() => _PdfFileViewScreenState();
}

class _PdfFileViewScreenState extends State<PdfFileViewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(
                horizontal: 12,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SvgPicture.asset(
                      ImageAssets.chevron_left,
                      height: 28,
                      width: 28,
                      color: Colorconstand.neutralDarkGrey,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context,true);
                    },
                    child: SvgPicture.asset(
                      ImageAssets.trash_icon,
                      color: Colorconstand.neutralDarkGrey,
                      height: 28,
                      width: 28,
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(height: 15,),
            Expanded(
              child: PdfViewWidget(pdfUrl: widget.pdfUrlFile),
             ),
          ],
        ),
      ),
    );
  
  }
}