import 'package:camis_teacher_application/app/modules/home_screen/e.homscreen.dart';
import 'package:flutter/material.dart';
class TextFormFiledCustom extends StatelessWidget {
  final Function(String) onChanged;
  final Function(bool) onFocusChange;
  Function()? onEditingComplete;
  final String title;
  FocusNode? focusNode;
  final TextEditingController controller;
  bool? checkFourcus;
  bool? enable;
  final TextInputType keyboardType;
  final int maxLines;

  TextFormFiledCustom({Key? key, this.focusNode,this.onEditingComplete, required this.onChanged, required this.onFocusChange, required this.title, required this.controller,  this.checkFourcus,  this.enable, required this.keyboardType,this.maxLines = 1}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.only(top: 16),
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colorconstand.lightGohan,
        borderRadius:
            BorderRadius.circular(12),
        border: Border.all(
            color:checkFourcus == false
                    ? Colorconstand.neutralGrey: Colorconstand.primaryColor,
            width:checkFourcus == false ? 1: 2),
      ),
      child: Focus(
        onFocusChange: onFocusChange,
        child: TextFormField(
          focusNode: focusNode,
          maxLines: maxLines,
          keyboardType:keyboardType,
          enabled: enable,
          controller:controller,
          onChanged: onChanged,
          style: ThemsConstands.subtitle1_regular_16,
          onEditingComplete: onEditingComplete,
          decoration: InputDecoration(
            border: UnderlineInputBorder(
                borderRadius:
                    BorderRadius.circular(
                        16.0)),
            contentPadding:
                const EdgeInsets.only(
                    top: 8,
                    bottom: 5,
                    left: 18,
                    right: 18),
            labelText: title,
            labelStyle: ThemsConstands
                .subtitle1_regular_16
                .copyWith(
                    color: Colorconstand
                        .lightTrunks,),
            enabledBorder:
                const UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors
                            .transparent)),
            focusedBorder:
                const UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors
                            .transparent)),
          ),
        ),
      ),
    );
  }
}