import 'package:camis_teacher_application/app/core/constands/color_constands.dart';
import 'package:camis_teacher_application/app/help/bloc_provider_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'app/core/constands/build_material_color.dart';
import 'app/routes/app_route.dart';
import 'app/routes/route_generator.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await EasyLocalization.ensureInitialized();
  EasyLocalization.logger.enableBuildModes = [];
  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('km'), Locale('en'),Locale('vi')],
      path: 'assets/translations',
      startLocale: const Locale('km'),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
    );
    return MultiBlocProvider(
      providers: listBlocProvider,
      child: MaterialApp(
        builder: (BuildContext context,Widget? child) {
          //final mediaQueryData = MediaQuery.of(context);
          //final scale = mediaQueryData.textScaleFactor.clamp(1.0);
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaler:const TextScaler.linear(1.0),),
            child: child!,
          );
        },
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        initialRoute: Routes.SPLAHSSCREEN,
        onGenerateRoute: RouteGenerator.generateRoute,
        theme: ThemeData(
          primarySwatch: buildMaterialColor(Colorconstand.primaryColor),
          fontFamily: translate == "km" ? "KantumruyPro" : "KantumruyPro",
        ),
      ),
    );
  }
}
 